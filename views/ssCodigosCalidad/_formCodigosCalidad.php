<?php
/* @var $this SsCodigosCalidadController */
/* @var $model SsCodigosCalidad */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-codigos-calidad-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>


<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

<?php echo $form->errorSummary($modelSSCodigosCalidad); ?>

<div class="form-group">
	<?php echo $form->labelEx($modelSSCodigosCalidad,'nombre_documento_digital'); ?>
	<?php echo $form->textField($modelSSCodigosCalidad,'nombre_documento_digital',array('size'=>60,'maxlength'=>75, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSCodigosCalidad,'nombre_documento_digital'); ?>
</div>

<div class="form-group">
	<?php echo $form->labelEx($modelSSCodigosCalidad,'codigo_calidad'); ?>
	<?php echo $form->textField($modelSSCodigosCalidad,'codigo_calidad',array('size'=>25,'maxlength'=>25, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSCodigosCalidad,'codigo_calidad'); ?>
</div>

<div class="form-group">
	<?php echo $form->labelEx($modelSSCodigosCalidad,'revision'); ?>
	<?php echo $form->textField($modelSSCodigosCalidad,'revision',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSCodigosCalidad,'revision'); ?>
</div>

<br>
<div class="form-group">
	<?php echo CHtml::submitButton($modelSSCodigosCalidad->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::link('Cancelar', array('listaCodigosCalidadServicioSocial'), array('class'=>'btn btn-danger')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->