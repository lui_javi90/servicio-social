<?php
/* @var $this SsCodigosCalidadController */
/* @var $model SsCodigosCalidad */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Configuracion Codigos Calidad',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Configuración Codigos Calidad
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Establecer los codigos de calidad que se mostraran en los reportes
		que conciernen al módulo de Servicio Social.
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-codigos-calidad-servicio-social-grid',
	'dataProvider'=>$modelSSCodigosCalidad->search(),
	'filter'=>$modelSSCodigosCalidad,
	'columns'=>array(
		//'id_codigo_calidad',
		array(
			'name' => 'nombre_documento_digital',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		array(
			'name' => 'codigo_calidad',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-info">'.$data->codigo_calidad.'</span>';
			},
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'name' => 'revision',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-info">'.$data->revision.'</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		//'banner_superior',
		//'banner_inferior',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editCodigosCalidadServicioSocial}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editCodigosCalidadServicioSocial' => array
				(
					'label'=>'Editar Codigos Calidad',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssCodigosCalidad/editarCodigosCalidadServicioSocial", array("id_codigo_calidad"=>$data->id_codigo_calidad))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
	),
)); ?>
