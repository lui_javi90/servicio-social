<?php
/* @var $this SsCodigosCalidadController */
/* @var $model SsCodigosCalidad */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Codigos Calidad Servicio Social' => array('ssCodigosCalidad/listaCodigosCalidadServicioSocial'),
	'Editar Codigos Calidad de Servicio Social',
);
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSCodigosCalidad->isNewRecord) ? "Nuevo Codigos Calidad de Servicio Social" : "Editar Codigos Calidad de Servicio Social"; ?>
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSCodigosCalidad->isNewRecord) ? "Nuevo Codigos Calidad de Servicio Social" : "Editar Codigos Calidad de Servicio Social"; ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formCodigosCalidad', array(
											'modelSSCodigosCalidad'=>$modelSSCodigosCalidad
										)); ?>
			</div>
		</div>
	</div>
</div>