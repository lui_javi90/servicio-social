<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Horarios de los Programas',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horarios de los Programas
		</span>
	</h2>
</div>

<br><br>
<?php //echo CHtml::link('Nuevo Horario', array('nuevoHorariosProgramas'), array('class'=>'btn btn-primary')); ?>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Para editar los horarios de los programas, se puede agregar o quitar algun dia habíl. Tambien se podra 
		cambiar el horario de cierto dia.
    </strong></p>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'horario-dias-habiles-programas-grid',
	'dataProvider'=>$modelSSHorarioDiasHabilesProgramas->searchNoRepetidosXPrograma(),
	'filter'=>$modelSSHorarioDiasHabilesProgramas,
	'columns'=>array(
		//'id_horario',
		array(
			'name' => 'idPrograma.nombre_programa',//id_programa_servicio_social
			'filter' => false,
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'name' => 'Tipo Programa',//idProgramaServicioSocial.id_tipo_programa
			'value'=>function($data)
			{
				return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		/*array(
			'name' => 'idDiaSemana.dia_semana',//id_dia_semana
			'filter' => false,
			'htmlOptions' => array('width'=>'20px')
		),
		array(
			'name' => 'hora_inicio',
			'filter' => false,
			'htmlOptions' => array('width' => '35px')
		),
		array(
			'name' => 'hora_fin',
			'filter' => false,
			'htmlOptions' => array('width'=>'35px')
		),
		array(
			'name' => 'horas_totales',
			'filter' => false,
			'htmlOptions' => array('width'=>'20px')
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detHorarioPrograma}',
			'header'=>'Detalle Horario',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detHorarioPrograma' => array
				(
					'label'=>'Detalle Horario del Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/detalleHorarioPrograma", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editHorariosProgramas}',
			'header'=>'Editar Horario',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editHorariosProgramas' => array
				(
					'label'=>'Editar Horario del Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
	),
)); ?>
