<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */

if($modelSSHorarioDiasHabilesProgramas->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Nuevo Horario del Programa',
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Editar Horario del Programa',
	);
}

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Días Hábiles del Programa
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Horario del Programa
				</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formHorarioDiasHabilesProgramas', array(
							'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
							'lista_dias' => $lista_dias
							)); ?>
			</div>
		</div>
	</div>
</div>
