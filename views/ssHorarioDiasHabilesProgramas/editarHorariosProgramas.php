<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Horarios de los Programas' => array('ssHorarioDiasHabilesProgramas/listaHorariosProgramas'),
    'Editar Horario del Programa'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Horario del Programa
		</span>
	</h2>
</div>

<br><br><br>
<?php echo CHtml::link('Agregar Nuevo Dia', array('nuevoDiaHorarioPrograma','id_programa'=>$id_programa), array('class'=>'btn btn-success right')); ?>

<br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'horario-dias-habiles-programas-grid',
    'dataProvider'=>$modelSSHorarioDiasHabilesProgramas->searchEditarHorarioPrograma($id_programa),
    'filter'=>$modelSSHorarioDiasHabilesProgramas,
    'columns'=>array(
        //'id_horario',
        //'idProgramaServicioSocial.nombre_programa',//id_programa
        array(
            'name' => 'idDiaSemana.dia_semana',//id_dia_semana
            'filter' => false,
            'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
        ),
        array(
            'name' => 'hora_inicio',
            'filter' => false,
            'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
        ),
        array(
            'name' => 'hora_fin',
            'filter' => false,
            'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
        ),
        //'horas_totales',
        array(
			'class'=>'CButtonColumn',
			'template'=>'{editDiaHorarioPrograma}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editDiaHorarioPrograma' => array
				(
					'label'=>'Editar Dia',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarDiaHorarioPrograma", array("id_horario"=>$data->id_horario, "id_programa"=>'.$id_programa.'))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{elimDiaHorarioPrograma}',
			'header'=>'Eliminar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'elimDiaHorarioPrograma' => array
				(
					'label'=>'Eliminar Dia',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/eliminarDiaHorarioPrograma", array("id_horario"=>$data->id_horario, "id_programa"=>'.$id_programa.'))',
					'imageUrl'=>'images/servicio_social/eliminar_32.png',
				),
			),
		),
    ),
)); ?>


