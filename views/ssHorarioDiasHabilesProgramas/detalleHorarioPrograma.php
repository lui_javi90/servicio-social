<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Horarios de los Programas' => array('ssHorarioDiasHabilesProgramas/listaHorariosProgramas'),
    'Detalle del Horario del Programa'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Horario del Programa
		</span>
	</h2>
</div>

<br><br>
<div class="row"><!--Inicio Row Principal-->
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">Información del Programa</h6>
            </div>
            <div class="panel-body">

					<div style="padding-top:10px" class="col-lg-3" align="center">
						<?php
							echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/programa_default.png"/>';
						?>
					</div>
					<div class="col-lg-9">

		            <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('nombre_programa')); ?>:</b>
		            &nbsp;&nbsp;<?php echo CHtml::encode($modelSSProgramas->nombre_programa); ?></p>

		            <p><b>Nombre Empresa:</b>
		            &nbsp;&nbsp;<?php echo $empresa; ?></p>

		            <p><b>Departamento:</b>
		            &nbsp;&nbsp;<?php echo $departamento; ?></p>

		            <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_clasificacion_area_servicio_social')); ?>:</b>
		            &nbsp;&nbsp;<?php echo $clasif_area; ?></p>

		            <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>
		            &nbsp;&nbsp;<?php echo '<span style="font-size:18px" class="label label-success">'.CHtml::encode($modelSSProgramas->horas_totales).'</span>'; ?></p>

		            <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('numero_estudiantes_solicitados')); ?>:</b>
		            &nbsp;&nbsp;<?php echo '<span style="font-size:16px" class="label label-info">'.CHtml::encode($modelSSProgramas->numero_estudiantes_solicitados).'</span>'; ?></p>

            	</div>
        		</div>
    		</div>
		</div>
</div><!--Fin Row Principal-->


<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>

<br>
<?php
for($i = 0; $i < count($modelSSHorarioDiasHabilesProgramas) ; $i++)
{
    //original=zii.widgets.CDetailView
    $this->widget('serviciosocial.components.DetailView4Col', array(
        'data'=>$modelSSHorarioDiasHabilesProgramas[$i],
        'attributes'=>array(
            //'id_horario',
            //'idProgramaServicioSocial.nombre_programa',//id_programa_servicio_social
            array(
                'name'=>'idDiaSemana.dia_semana',//id_dia_semana
                'oneRow'=>true,
            ),
            'hora_inicio',
            'hora_fin',
            //'horas_totales',
        ),
    ));

    echo "<br>";
}?>

<br><br>
<div align="center">
    <?php //echo CHtml::link('Entendido', array('listaHorariosProgramas'), array('class'=>'btn btn-success')); ?>
</div>
