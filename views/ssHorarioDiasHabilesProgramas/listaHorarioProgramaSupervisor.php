<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
	  'Programas Servicio Social' => array('ssProgramas/listaProgramasSupervisorServicioSocial'),
    'Horario Programa'
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>

<br><br><br><br>

<div align="right">
  <?php echo CHtml::link('Agregar Nuevo Día', array('nuevoDiaProgramaSupervisor', 'id_programa'=>$id_programa), array('class'=>'btn btn-success')); ?>
</div>

<br><br><br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Puedes agregar, editar o eliminar dias del horario de tu Programa. Debes tener minímo un día para poder eliminar (No aplica la primera vez).</b>
  </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-horario-dias-habiles-programas-grid',
    'dataProvider'=>$modelSSHorarioDiasHabilesProgramas->searchEditarHorarioPrograma($id_programa),
    'filter'=>$modelSSHorarioDiasHabilesProgramas,
    'columns'=>array(
        //'id_horario',
        //'id_programa',
        //'id_dia_semana',
        array(
          'name' => 'idDiaSemana.dia_semana',
          'filter' => false,
          'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
          'name' => 'hora_inicio',
          'filter' => false,
          'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
          'name' => 'hora_fin',
          'filter' => false,
          'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
          'name' => 'horas_totales',
          'filter' => false,
          'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
    			'class'=>'CButtonColumn',
    			'template'=>'{editDiaProgramaSSocial}',
    			'header'=>'Editar Día',
    			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
    			'buttons'=>array
    			(
    				'editDiaProgramaSSocial' => array
    				(
    					'label'=>'Asignar Numero Alumnos a Programas',
    					'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorarioProgramaSupervisor", array("id_programa"=>$data->id_programa, "id_horario"=>$data->id_horario))',
    					'imageUrl'=>'images/servicio_social/horario_32.png'
    				),
    			),
        ),
        array(
    			'class'=>'CButtonColumn',
    			'template'=>'{elimDiaProgramaSSocial},{noElimDiaPrograma}',
    			'header'=>'Eiminar',
    			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
    			'buttons'=>array
    			(
    				'elimDiaProgramaSSocial' => array
    				(
    					'label'=>'Asignar Numero Alumnos a Programas',
    					'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/eliminarDiaProgramaSupervisor", array("id_horario"=>$data->id_horario, "id_programa"=>$data->id_programa))',
              'imageUrl'=>'images/servicio_social/eliminar_32.png',
              'visible' => function($row, $data)
              {
                $id = $data->id_programa;
                $qrys = "select * from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = '$id' ";
                $rs = Yii::app()->db->createCommand($qrys)->queryAll();

                return (count($rs) > 1) ? true : false;
              }
            ),
            'noElimDiaPrograma' => array
            (
              'label'=>'Opción Bloqueada',
    					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/eliminarDiaProgramaSupervisor", array("id_horario"=>$data->id_horario, "id_programa"=>$data->id_programa))',
              'imageUrl'=>'images/servicio_social/bloquedo_32.png',
              'visible' => function($row, $data)
              {
                $id = $data->id_programa;
                $qrys = "select * from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = '$id' ";
                $rs = Yii::app()->db->createCommand($qrys)->queryAll();

                return (count($rs) == 1) ? true : false;
              }
            )
    			),
    		),
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>
