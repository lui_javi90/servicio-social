<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'horario-dias-habiles-programas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSHorarioDiasHabilesProgramas); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
		<?php echo $form->dropDownList($modelSSHorarioDiasHabilesProgramas,
									'id_dia_semana',
									$lista_dias,
									array('prompt'=>'--Dia de la semana--','class'=>'form-control', 'required'=>'required'
									)); ?>
		<?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
		<?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
		<?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
				'model'=>$modelSSHorarioDiasHabilesProgramas,
				'attribute'=>'hora_inicio',
				'language' => 'es',
				'options' => array(
						'showOn'=>'focus',
						'timeOnly'=>true,
						'showHour'=>true,
						'showMinute'=>true,
						'showSecond'=>true,
						'timeFormat'=>'hh:mm:ss',
				),
				'htmlOptions' => array(
						'style'=>'width:150px;', // styles to be applied
						'maxlength' => '10',    // textField maxlength
						'class' => 'form-control',
						'required' => 'required'
				),
		));?>
		<?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
		<?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
		<?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
				'model'=>$modelSSHorarioDiasHabilesProgramas,
				'attribute'=>'hora_fin',
				'language' => 'es',
				'options' => array(
						'showOn'=>'focus',
						'timeOnly'=>true,
						'showHour'=>true,
						'showMinute'=>true,
						'showSecond'=>true,
						'timeFormat'=>'hh:mm:ss',
				),
				'htmlOptions' => array(
						'style'=>'width:150px;', // styles to be applied
						'maxlength' => '10',    // textField maxlength
						'class' => 'form-control',
						'required' => 'required'
				),
		));?>
		<?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
	</div>

	<div class="form-group">
		<?php //echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'horas_totales'); ?>
		<?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'horas_totales',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php //echo $form->error($modelSSHorarioDiasHabilesProgramas,'horas_totales'); ?>
	</div>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSHorarioDiasHabilesProgramas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaHorariosProgramas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->