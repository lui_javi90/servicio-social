<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
	  'Programas Servicio Social' => array('ssProgramas/listaProgramasSupervisorServicioSocial'),
    'Horario del Programa' => array('ssHorarioDiasHabilesProgramas/listaHorarioProgramaSupervisor', 'id_programa'=>$id_programa),
    'Agregar Nuevo Día'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Agregar Nuevo Día
		</span>
	</h2>
</div>


<br>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">
          Agregar
        </h3>
      </div>
      <div class="panel-body">

        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'ss-horario-dias-habiles-programas-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
            //'htmlOptions' => array('autocomplete'=>off)
        )); ?>

            <p class="note">Campos con <span class="required">*</span> son requeridos.</p>

            <?php echo $form->errorSummary($modelSSHorarioDiasHabilesProgramas); ?>

            <div class="form-group">
          		<?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
          		<?php echo $form->dropDownList($modelSSHorarioDiasHabilesProgramas,
          									'id_dia_semana',
          									$lista_dias,
          									array('prompt'=>'--Dia de la semana--','class'=>'form-control', 'required'=>'required',
          									)); ?>
          		<?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
          	</div>

          	<div class="form-group">
          		<?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
          		<?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
          		<?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
          				'model'=>$modelSSHorarioDiasHabilesProgramas,
          				'attribute'=>'hora_inicio',
          				'language' => 'es',
          				'options' => array(
          						'showOn'=>'focus',
          						'timeOnly'=>true,
          						'showHour'=>true,
          						'showMinute'=>true,
          						'showSecond'=>false,
          						'timeFormat'=>'hh:mm:ss',
          				),
          				'htmlOptions' => array(
          						'style'=>'width:150px;', // styles to be applied
          						'maxlength' => '10',    // textField maxlength
								'class' => 'form-control',
								'required' => 'required',
								'readOnly' => true
          				),
          		));?>
          		<?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
          	</div>

          	<div class="form-group">
          		<?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
          		<?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
          		<?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
          				'model'=>$modelSSHorarioDiasHabilesProgramas,
          				'attribute'=>'hora_fin',
          				'language' => 'es',
          				'options' => array(
          						'showOn'=>'focus',
          						'timeOnly'=>true,
          						'showHour'=>true,
          						'showMinute'=>true,
          						'showSecond'=>false,
          						'timeFormat'=>'hh:mm:ss',
          				),
          				'htmlOptions' => array(
          						'style'=>'width:150px;', // styles to be applied
          						'maxlength' => '10',    // textField maxlength
								'class' => 'form-control',
								'required' => 'required',
								'readOnly' => true
          				),
          		));?>
          		<?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
          	</div>

            <div class="row buttons">
                <?php echo CHtml::submitButton($modelSSHorarioDiasHabilesProgramas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                <?php echo CHtml::link('Cancelar', array('listaHorarioProgramaSupervisor', 'id_programa'=>$id_programa), array('class'=>'btn btn-danger')); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->

      </div>
    </div>
  </div>
</div>


<br><br><br><br><br>
<br><br><br><br><br>