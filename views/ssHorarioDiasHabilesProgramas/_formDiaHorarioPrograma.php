<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'edt-horario-dias-habiles-programas-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'htmlOptions' => array('autocomplete'=>off),
    'enableAjaxValidation'=>false,
)); ?>

    <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

    <?php echo $form->errorSummary($modelSSHorarioDiasHabilesProgramas); ?>

    <div class="form-group">
        <?php if($modelSSHorarioDiasHabilesProgramas->isNewRecord){?>
        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
        <?php echo $form->dropDownList($modelSSHorarioDiasHabilesProgramas,
                                        'id_dia_semana',
                                        $lista_dias_semana,
                                        array('prompt'=>'--Selecciona dia de la semana--', 'class'=>'form-control', 'required'=>'required')
                                        ); ?>
        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
        <?php }else{ ?>
            <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
        <?php echo $form->dropDownList($modelSSHorarioDiasHabilesProgramas,
                                        'id_dia_semana',
                                        $lista_dias_semana,
                                        array('prompt'=>'--Selecciona dia de la semana--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
                                        ); ?>
        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
        <?php } ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_inicio');
        $this->widget('ext.timepicker.EJuiDateTimePicker', array(
            'model'=>$modelSSHorarioDiasHabilesProgramas,
            'attribute'=>'hora_inicio',
            'language' => 'es',
            'options' => array(
                    'showOn'=>'focus',
                    'timeOnly'=>true,
                    'showHour'=>true,
                    'showMinute'=>true,
                    'showSecond'=>false,
                    'timeFormat'=>'hh:mm:ss',
            ),
            'htmlOptions' => array(
                    'style'=>'width:150px;', // styles to be applied
                    'maxlength' => '10',    // textField maxlength
                    'class' => 'form-control',
                    'required' => 'required',
                    'readOnly' => true
            ),
        ));?>
        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_fin');
        $this->widget('ext.timepicker.EJuiDateTimePicker', array(
            'model'=>$modelSSHorarioDiasHabilesProgramas,
            'attribute'=>'hora_fin',
            'language' => 'es',
            'options' => array(
                    'showOn'=>'focus',
                    'timeOnly'=>true,
                    'showHour'=>true,
                    'showMinute'=>true,
                    'showSecond'=>false,
                    'timeFormat'=>'hh:mm:ss',
            ),
            'htmlOptions' => array(
                    'style'=>'width:150px;', // styles to be applied
                    'maxlength' => '10',    // textField maxlength
                    'class' => 'form-control',
                    'required' => 'required',
                    'readOnly' => true
            ),
        ));?>
        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
    </div>

    <br>
    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSHorarioDiasHabilesProgramas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('editarHorariosProgramas','id_programa'=>$id_programa), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
