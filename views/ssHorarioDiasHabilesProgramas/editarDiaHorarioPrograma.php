<?php
/* @var $this HorarioDiasHabilesProgramasController */
/* @var $model HorarioDiasHabilesProgramas */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Horarios de los Programas' => array('ssHorarioDiasHabilesProgramas/listaHorariosProgramas'),
    'Editar Horario del Programa' => array('ssHorarioDiasHabilesProgramas/editarHorariosProgramas/','id_programa'=>$id_programa),
    'Editar Dia del programa'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Editar Dia del Horario del Programa
		</span>
	</h2>
</div>

<br><br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Editar Dia del Horario del Programa
                </h6>
            </div>
            <div class="panel-body">
            <?php $this->renderPartial('_formDiaHorarioPrograma', array(
                                    'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
                                    'lista_dias_semana' => $lista_dias_semana,
                                    'id_programa' => $id_programa
                                    )); ?>
            </div>
        </div>
    </div>
</div>