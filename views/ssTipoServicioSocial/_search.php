<?php
/* @var $this SsTipoServicioSocialController */
/* @var $model SsTipoServicioSocial */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tipo_servicio_social'); ?>
		<?php echo $form->textField($model,'id_tipo_servicio_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_servicio_social'); ?>
		<?php echo $form->textField($model,'tipo_servicio_social',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->