<?php
/* @var $this SsTipoServicioSocialController */
/* @var $model SsTipoServicioSocial */

$this->breadcrumbs=array(
	'Ss Tipo Servicio Socials'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsTipoServicioSocial', 'url'=>array('index')),
	array('label'=>'Manage SsTipoServicioSocial', 'url'=>array('admin')),
);
?>

<h1>Create SsTipoServicioSocial</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>