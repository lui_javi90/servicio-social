<?php
/* @var $this SsTipoServicioSocialController */
/* @var $data SsTipoServicioSocial */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipo_servicio_social')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tipo_servicio_social), array('view', 'id'=>$data->id_tipo_servicio_social)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_servicio_social')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_servicio_social); ?>
	<br />


</div>