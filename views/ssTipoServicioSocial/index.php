<?php
/* @var $this SsTipoServicioSocialController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ss Tipo Servicio Socials',
);

$this->menu=array(
	array('label'=>'Create SsTipoServicioSocial', 'url'=>array('create')),
	array('label'=>'Manage SsTipoServicioSocial', 'url'=>array('admin')),
);
?>

<h1>Ss Tipo Servicio Socials</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
