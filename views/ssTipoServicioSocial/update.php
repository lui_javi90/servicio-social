<?php
/* @var $this SsTipoServicioSocialController */
/* @var $model SsTipoServicioSocial */

$this->breadcrumbs=array(
	'Ss Tipo Servicio Socials'=>array('index'),
	$model->id_tipo_servicio_social=>array('view','id'=>$model->id_tipo_servicio_social),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsTipoServicioSocial', 'url'=>array('index')),
	array('label'=>'Create SsTipoServicioSocial', 'url'=>array('create')),
	array('label'=>'View SsTipoServicioSocial', 'url'=>array('view', 'id'=>$model->id_tipo_servicio_social)),
	array('label'=>'Manage SsTipoServicioSocial', 'url'=>array('admin')),
);
?>

<h1>Update SsTipoServicioSocial <?php echo $model->id_tipo_servicio_social; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>