<?php
/* @var $this SsTipoServicioSocialController */
/* @var $model SsTipoServicioSocial */

$this->breadcrumbs=array(
	'Ss Tipo Servicio Socials'=>array('index'),
	$model->id_tipo_servicio_social,
);

$this->menu=array(
	array('label'=>'List SsTipoServicioSocial', 'url'=>array('index')),
	array('label'=>'Create SsTipoServicioSocial', 'url'=>array('create')),
	array('label'=>'Update SsTipoServicioSocial', 'url'=>array('update', 'id'=>$model->id_tipo_servicio_social)),
	array('label'=>'Delete SsTipoServicioSocial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tipo_servicio_social),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SsTipoServicioSocial', 'url'=>array('admin')),
);
?>

<h1>View SsTipoServicioSocial #<?php echo $model->id_tipo_servicio_social; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tipo_servicio_social',
		'tipo_servicio_social',
	),
)); ?>
