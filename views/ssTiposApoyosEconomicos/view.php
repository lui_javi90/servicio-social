<?php
/* @var $this SsTiposApoyosEconomicosController */
/* @var $model SsTiposApoyosEconomicos */

$this->breadcrumbs=array(
	'Ss Tipos Apoyos Economicoses'=>array('index'),
	$model->id_tipo_apoyo_economico,
);

$this->menu=array(
	array('label'=>'List SsTiposApoyosEconomicos', 'url'=>array('index')),
	array('label'=>'Create SsTiposApoyosEconomicos', 'url'=>array('create')),
	array('label'=>'Update SsTiposApoyosEconomicos', 'url'=>array('update', 'id'=>$model->id_tipo_apoyo_economico)),
	array('label'=>'Delete SsTiposApoyosEconomicos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tipo_apoyo_economico),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SsTiposApoyosEconomicos', 'url'=>array('admin')),
);
?>

<h1>View SsTiposApoyosEconomicos #<?php echo $model->id_tipo_apoyo_economico; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tipo_apoyo_economico',
		'id_apoyo_economico_prestador_servicio_social',
		'tipo_apoyo_economico',
	),
)); ?>
