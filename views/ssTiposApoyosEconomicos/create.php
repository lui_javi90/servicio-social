<?php
/* @var $this SsTiposApoyosEconomicosController */
/* @var $model SsTiposApoyosEconomicos */

$this->breadcrumbs=array(
	'Ss Tipos Apoyos Economicoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsTiposApoyosEconomicos', 'url'=>array('index')),
	array('label'=>'Manage SsTiposApoyosEconomicos', 'url'=>array('admin')),
);
?>

<h1>Create SsTiposApoyosEconomicos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>