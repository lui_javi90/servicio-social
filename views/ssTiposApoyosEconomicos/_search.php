<?php
/* @var $this SsTiposApoyosEconomicosController */
/* @var $model SsTiposApoyosEconomicos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tipo_apoyo_economico'); ?>
		<?php echo $form->textField($model,'id_tipo_apoyo_economico'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_apoyo_economico_prestador_servicio_social'); ?>
		<?php echo $form->textField($model,'id_apoyo_economico_prestador_servicio_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_apoyo_economico'); ?>
		<?php echo $form->textField($model,'tipo_apoyo_economico',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->