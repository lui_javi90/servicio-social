<?php
/* @var $this SsTiposApoyosEconomicosController */
/* @var $data SsTiposApoyosEconomicos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipo_apoyo_economico')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tipo_apoyo_economico), array('view', 'id'=>$data->id_tipo_apoyo_economico)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_apoyo_economico_prestador_servicio_social')); ?>:</b>
	<?php echo CHtml::encode($data->id_apoyo_economico_prestador_servicio_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_apoyo_economico')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_apoyo_economico); ?>
	<br />


</div>