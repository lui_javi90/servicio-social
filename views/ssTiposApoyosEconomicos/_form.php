<?php
/* @var $this SsTiposApoyosEconomicosController */
/* @var $model SsTiposApoyosEconomicos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-tipos-apoyos-economicos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_apoyo_economico_prestador_servicio_social'); ?>
		<?php echo $form->textField($model,'id_apoyo_economico_prestador_servicio_social'); ?>
		<?php echo $form->error($model,'id_apoyo_economico_prestador_servicio_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_apoyo_economico'); ?>
		<?php echo $form->textField($model,'tipo_apoyo_economico',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'tipo_apoyo_economico'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->