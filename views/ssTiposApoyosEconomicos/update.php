<?php
/* @var $this SsTiposApoyosEconomicosController */
/* @var $model SsTiposApoyosEconomicos */

$this->breadcrumbs=array(
	'Ss Tipos Apoyos Economicoses'=>array('index'),
	$model->id_tipo_apoyo_economico=>array('view','id'=>$model->id_tipo_apoyo_economico),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsTiposApoyosEconomicos', 'url'=>array('index')),
	array('label'=>'Create SsTiposApoyosEconomicos', 'url'=>array('create')),
	array('label'=>'View SsTiposApoyosEconomicos', 'url'=>array('view', 'id'=>$model->id_tipo_apoyo_economico)),
	array('label'=>'Manage SsTiposApoyosEconomicos', 'url'=>array('admin')),
);
?>

<h1>Update SsTiposApoyosEconomicos <?php echo $model->id_tipo_apoyo_economico; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>