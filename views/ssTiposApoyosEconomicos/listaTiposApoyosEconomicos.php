<?php
/* @var $this TiposApoyosEconomicosController */
/* @var $model TiposApoyosEconomicos */

$this->breadcrumbs=array(
	'Tipos Apoyos Economicoses'=>array('index'),
	'Manage',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Tipos Apoyos Economicos Servicio Social
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipos-apoyos-economicos-grid',
	'dataProvider'=>$modelSSTiposApoyosEconomicos->search(),
	'filter'=>$modelSSTiposApoyosEconomicos,
	'columns'=>array(
		'id_tipo_apoyo_economico',
		'id_apoyo_economico_prestador_servicio_social',
		'tipo_apoyo_economico',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
