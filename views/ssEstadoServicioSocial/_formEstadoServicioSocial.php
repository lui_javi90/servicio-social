<?php
/* @var $this SsEstadoServicioSocialController */
/* @var $model SsEstadoServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-estado-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

<?php echo $form->errorSummary($modelSSEstadoServicioSocial); ?>

<div class="form-group">
	<?php echo $form->labelEx($modelSSEstadoServicioSocial,'estado'); ?>
	<?php echo $form->textField($modelSSEstadoServicioSocial,'estado',array('size'=>15,'maxlength'=>15, 'class'=>'form-control', 'readOnly'=>true, 'required'=>'required')); ?>
	<?php echo $form->error($modelSSEstadoServicioSocial,'estado'); ?>
</div>

<div class="form-group">
	<?php echo $form->labelEx($modelSSEstadoServicioSocial,'descripcion_estado'); ?>
	<?php echo $form->textField($modelSSEstadoServicioSocial,'descripcion_estado',array('size'=>60,'maxlength'=>250, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSEstadoServicioSocial,'descripcion_estado'); ?>
</div>

<div class="form-group">
	<?php echo CHtml::submitButton($modelSSEstadoServicioSocial->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::link('Cancelar', array('listaEstadoServicioSocial'), array('class'=>'btn btn-danger')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->