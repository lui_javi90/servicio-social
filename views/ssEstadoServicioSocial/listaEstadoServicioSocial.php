<?php
/* @var $this SsEstadoServicioSocialController */
/* @var $model SsEstadoServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Estados del Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Estados del Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Si tienes alguna duda con los estados por los que debe pasar el Servicio Social
	puedes verificarlos a continuación.
    </strong></p>
</div>


<?php
if($oficina == true){
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-estado-servicio-social-grid',
	'dataProvider'=>$modelSSEstadoServicioSocial->search(),
	'filter'=>$modelSSEstadoServicioSocial,
	'columns'=>array(
		//'id_estado_servicio_social',
		array(
			'name' => 'estado',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{

				switch($data->id_estado_servicio_social)
				{
					case 1:
						return '<span style="font-size:18px" class="label label-warning">'.$data->estado.'</span>';
					break;
					case 2:
						return '<span style="font-size:18px" class="label label-info">'.$data->estado.'</span>';
					break;
					case 3:
						return '<span style="font-size:18px" class="label label-info">'.$data->estado.'</span>';
					break;
					case 4:
						return '<span style="font-size:18px" class="label label-warning">'.$data->estado.'</span>';
					break;
					case 5:
						return '<span style="font-size:18px" class="label label-success">'.$data->estado.'</span>';
					break;
					case 6:
						return '<span style="font-size:18px" class="label label-success">'.$data->estado.'</span>';
					break;
					case 7:
						return '<span style="font-size:18px" class="label label-danger">'.$data->estado.'</span>';
					break;
					case 8:
						return '<span style="font-size:18px" class="label label-default">'.$data->estado.'</span>';
					break;
				}
			},
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'name' => 'descripcion_estado',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editEstadoServicioSocial}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
				'editEstadoServicioSocial' => array(
					'label'=>'Editar Configuración',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssEstadoServicioSocial/editarEstadoServicioSocial", array("id_estado_servicio_social"=>$data->id_estado_servicio_social))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				)
			),
		),
	),
));
}else{

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-estado-servicio-social-grid',
	'dataProvider'=>$modelSSEstadoServicioSocial->search(),
	'filter'=>$modelSSEstadoServicioSocial,
	'columns'=>array(
		//'id_estado_servicio_social',
		array(
			'name' => 'estado',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{

				switch($data->id_estado_servicio_social)
				{
					case 1:
						return '<span style="font-size:18px" class="label label-warning">'.$data->estado.'</span>';
					break;
					case 2:
						return '<span style="font-size:18px" class="label label-info">'.$data->estado.'</span>';
					break;
					case 3:
						return '<span style="font-size:18px" class="label label-info">'.$data->estado.'</span>';
					break;
					case 4:
						return '<span style="font-size:18px" class="label label-warning">'.$data->estado.'</span>';
					break;
					case 5:
						return '<span style="font-size:18px" class="label label-success">'.$data->estado.'</span>';
					break;
					case 6:
						return '<span style="font-size:18px" class="label label-success">'.$data->estado.'</span>';
					break;
					case 7:
						return '<span style="font-size:18px" class="label label-danger">'.$data->estado.'</span>';
					break;
				}
			},
			'htmlOptions' => array('width'=>'150px', 'height'=>'40px','class'=>'text-center')
		),
		array(
			'name' => 'descripcion_estado',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px')
		)
	),
));

}
?>
