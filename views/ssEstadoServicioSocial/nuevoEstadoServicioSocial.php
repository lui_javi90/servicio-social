<?php
/* @var $this SsEstadoServicioSocialController */
/* @var $model SsEstadoServicioSocial */

if($modelSSEstadoServicioSocial->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Estados Servicio Social' => array('ssEstadoServicioSocial/listaEstadoServicioSocial'),
		'Nuevo Estado Servicio Social',
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Estados Servicio Social' => array('ssEstadoServicioSocial/listaEstadoServicioSocial'),
		'Editar Estado Servicio Social',
	);
}

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSEstadoServicioSocial->isNewRecord) ? "Nuevo Estados Servicio Social" : "Editar Estados Servicio Social"; ?>
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSEstadoServicioSocial->isNewRecord) ? "Nuevo Estados Servicio Social" : "Editar Estados Servicio Social"; ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formEstadoServicioSocial', array(
											'modelSSEstadoServicioSocial'=>$modelSSEstadoServicioSocial
					)); ?>
			</div>
		</div>
	</div>
</div>