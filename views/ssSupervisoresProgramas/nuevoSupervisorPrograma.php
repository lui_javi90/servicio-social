<?php
/* @var $this SupervisoresProgramasController */
/* @var $model SupervisoresProgramas */

if($modelSSSupervisoresProgramas->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Supervisores Programas' => array('ssSupervisoresProgramas/listaSupervisoresProgramas'),
		'Nuevo Supervisor Programa'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Supervisores Programas' => array('ssSupervisoresProgramas/listaSupervisoresProgramas'),
		'Editar Supervisor Programa'
	);
}

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSSupervisoresProgramas->isNewRecord) ? "Nuevo Supervisor Programa" : "Editar Supervisor Programa"; ?>
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSSupervisoresProgramas->isNewRecord) ? "Nuevo Supervisor Programa" : "Editar Supervisor Programa"; ?>
				</h6>
			</div>
			<div class="panel panel-body">
					<?php $this->renderPartial('_formSupervisorPrograma', array(
												'modelSSSupervisoresProgramas'=>$modelSSSupervisoresProgramas,
												'lista_status' => $lista_status,
												'lista_empresas' => $lista_empresas,
												
												)); ?>
			</div>
		</div>
	</div>
</div>
