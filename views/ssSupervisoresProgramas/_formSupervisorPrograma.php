<?php
/* @var $this SupervisoresProgramasController */
/* @var $model SupervisoresProgramas */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'sup-supervisores-programas-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'htmlOptions' => array('autocomplete'=>'off'),
		'enableAjaxValidation'=>false,
	)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSSupervisoresProgramas); ?>

	<?php if($modelSSSupervisoresProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php
		/*$htmlOptions1 = array('size'=>13,'maxlength'=>13, 'class'=>'form-control', 'required'=>'required',
							'ajax'=>array(
										'url'=>$this->createUrl('rfcEmpleadoReconocido'), //
										'type' => 'POST',
										'update' => '#SsSupervisoresProgramas_rfcSupervisor',
										'success'=>'function(data) {
											$("#SsSupervisoresProgramas_nombre_supervisor").html(data.nombre_sup);
										}'
							),
				
					);*/
		?>
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas, 
									'rfcSupervisor',
									array('size'=>13,'maxlength'=>13, 'class'=>'form-control', 'required'=>'required')
									); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
	</div>
	<?php }else{ ?>
		<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
        <?php echo $form->textField($modelSSSupervisoresProgramas,'rfcSupervisor',array('size'=>13,'maxlength'=>13, 'class'=>'form-control', 'readOnly' => true)); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
		</div>
	<?php }?>

	<?php //if($modelSSSupervisoresProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSSupervisoresProgramas,'nombre_supervisor'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas,
									'nombre_supervisor',
									array('size'=>30,'maxlength'=>30, 'class'=>'form-control', 'required'=>'required',
										'ajax'=>array(
										'type'=>'POST',
										'dataType'=>'json',
										'data' => array(

											'nombre_supervisor'=>'js:$(\'#SsSupervisoresProgramas_nombre_supervisor\').val()', //Valor de campo nombre_supervisor
											'apell_paterno'=>'js:$(\'#SsSupervisoresProgramas_apell_paterno\').val()', //Valor de campo apell_paterno
											'apell_materno'=>'js:$(\'#SsSupervisoresProgramas_apell_materno\').val()', //Valor de campo apell_materno
											'eres_jefe_depto'=>'js:$(\'#SsSupervisoresProgramas_eres_jefe_depto option:selected\').val()', //Valor de campo eres_jefe_depto
										),
										'url'=>CController::createUrl('ssSupervisoresProgramas/actualizaNombreSupervisor'),
										'success'=>'function(data) {
											if(data.es_jefe == true){
												$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", true ); 
											}else{
												$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", false );
											}
									
										}',
										))
									); ?>
		<?php echo $form->error($modelSSSupervisoresProgramas,'nombre_supervisor'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSSupervisoresProgramas,'apell_paterno'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas,
									'apell_paterno',
									array('size'=>20,'maxlength'=>20, 'class'=>'form-control', 'required'=>'required',
										'ajax'=>array(
										'type'=>'POST',
										'dataType'=>'json',
										'data' => array(

											'nombre_supervisor'=>'js:$(\'#SsSupervisoresProgramas_nombre_supervisor\').val()', //Valor de campo nombre_supervisor
											'apell_paterno'=>'js:$(\'#SsSupervisoresProgramas_apell_paterno\').val()', //Valor de campo apell_paterno
											'apell_materno'=>'js:$(\'#SsSupervisoresProgramas_apell_materno\').val()', //Valor de campo apell_materno
											'eres_jefe_depto'=>'js:$(\'#SsSupervisoresProgramas_eres_jefe_depto option:selected\').val()', //Valor de campo eres_jefe_depto
										),
										'url'=>CController::createUrl('ssSupervisoresProgramas/actualizaNombreSupervisor'),
										'success'=>'function(data) {
											if(data.es_jefe == true){
												$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", true ); 
											}else{
												$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", false );
											}
									
										}',
										))
									); ?>
		<?php echo $form->error($modelSSSupervisoresProgramas,'apell_paterno'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSSupervisoresProgramas,'apell_materno'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas,
									'apell_materno',
									array('size'=>20,'maxlength'=>20, 'class'=>'form-control', 'required'=>'required',
										'ajax'=>array(
										'type'=>'POST',
										'dataType'=>'json',
										'data' => array(

											'nombre_supervisor'=>'js:$(\'#SsSupervisoresProgramas_nombre_supervisor\').val()', //Valor de campo nombre_supervisor
											'apell_paterno'=>'js:$(\'#SsSupervisoresProgramas_apell_paterno\').val()', //Valor de campo apell_paterno
											'apell_materno'=>'js:$(\'#SsSupervisoresProgramas_apell_materno\').val()', //Valor de campo apell_materno
											'eres_jefe_depto'=>'js:$(\'#SsSupervisoresProgramas_eres_jefe_depto option:selected\').val()', //Valor de campo eres_jefe_depto
										),
										'url'=>CController::createUrl('ssSupervisoresProgramas/actualizaNombreSupervisor'),
										'success'=>'function(data) {
											if(data.es_jefe == true){
												$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", true ); 
											}else{
												$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", false );
											}
									
										}',
										))
									); ?>
		<?php echo $form->error($modelSSSupervisoresProgramas,'apell_materno'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSSupervisoresProgramas,'cargo_supervisor'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas,'cargo_supervisor',array('size'=>60,'maxlength'=>150, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSSupervisoresProgramas,'cargo_supervisor'); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'grMaxEstSup'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas,
									'grMaxEstSup',
									//array('size'=>5,'maxlength'=>5, 'class'=>'form-control')
									array('size'=>5,'maxlength'=>5, 'class'=>'form-control', 'required'=>'required',
										'ajax'=>array(
										'type'=>'POST',
										'dataType'=>'json',
										'data' => array(

											'grMaxEstSup'=>'js:$(\'#SsSupervisoresProgramas_grMaxEstSup\').val()', //Valor de campo nombre_supervisor
											'eres_jefe_depto'=>'js:$(\'#SsSupervisoresProgramas_eres_jefe_depto option:selected\').val()', //Valor de campo eres_jefe_depto
										),
										'url'=>CController::createUrl('ssSupervisoresProgramas/actualizarMaxGradoAcademico'),
										'success'=>'function(data) {
											if(data.es_jefe == true){
												$("#SsSupervisoresProgramas_gradoMaxEstudios").val(data.cargo).prop( "disabled", true ); 
											}else{
												$("#SsSupervisoresProgramas_gradoMaxEstudios").val(data.cargo).prop( "disabled", false );
											}
									
										}',
										))
									); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'grMaxEstSup'); ?>
    </div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSSupervisoresProgramas,'email_supervisor'); ?>
		<?php echo $form->textField($modelSSSupervisoresProgramas,'email_supervisor',array('size'=>50,'maxlength'=>50, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSSupervisoresProgramas,'email_supervisor'); ?>
	</div>

	<?php if($modelSSSupervisoresProgramas->isNewRecord){?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'id_sexo'); ?>
		<?php echo $form->dropDownList($modelSSSupervisoresProgramas,
									'id_sexo',
									array('1'=>'MASCULINO', '2'=>'FEMENINO'),
									array('prompt'=>'--Elegir Genero--','class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'id_sexo'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'id_sexo'); ?>
		<?php echo $form->dropDownList($modelSSSupervisoresProgramas,
									'id_sexo',
									array('1'=>'MASCULINO', '2'=>'FEMENINO'),
									array('prompt'=>'-- Elegir Genero --','class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'id_sexo'); ?>
	</div>
	<?php } ?>

	<?php if($modelSSSupervisoresProgramas->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'id_unidad_receptora'); ?>
		<?php echo $form->dropDownList($modelSSSupervisoresProgramas,
										'id_unidad_receptora',
										$lista_empresas,
										array('prompt'=>'-- Selecciona Unidad Receptora --', 'class'=>'form-control', 'required'=>'required')
										); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'id_unidad_receptora'); ?>
	</div>
	<?php }else{ ?>
		<?php echo $form->labelEx($modelSSSupervisoresProgramas,'id_unidad_receptora'); ?>
		<?php echo $form->dropDownList($modelSSSupervisoresProgramas,
										'id_unidad_receptora',
										$lista_empresas,
										array('class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
										); ?>
		<?php echo $form->error($modelSSSupervisoresProgramas,'id_unidad_receptora'); ?>
	<?php } ?>

	<?php if(!$modelSSSupervisoresProgramas->isNewRecord){ ?>
		<div class="form-group">
			<?php echo $form->labelEx($modelSSSupervisoresProgramas,'id_status_supervisor'); ?>
			<?php echo $form->dropDownList($modelSSSupervisoresProgramas,
											'id_status_supervisor',
											$lista_status,
											array('prompt'=>'-- Status del Supervisor --', 'class'=>'form-control', 'required'=>'required')
											); ?>
			<?php echo $form->error($modelSSSupervisoresProgramas,'id_status_supervisor'); ?>
		</div>
	<?php }?>

	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'dscDepartamento'); ?>
        <?php echo $form->textField($modelSSSupervisoresProgramas,'dscDepartamento',array('size'=>60,'maxlength'=>250, 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'dscDepartamento'); ?>
    </div>

	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'eres_jefe_depto'); ?>
		<?php echo $form->dropDownList($modelSSSupervisoresProgramas,
										'eres_jefe_depto',
										array('0'=>'NO', '1'=>'SI'),
										array(
                                            'prompt'=>'-- Eres Jefe de Departamento --', 'class'=>'form-control', 'required'=>'required',
                                            'ajax'=>array(
                                            'type'=>'POST',
                                            'dataType'=>'json',
                                            'data' => array(

												'eres_jefe_depto'=>'js:$(\'#SsSupervisoresProgramas_eres_jefe_depto option:selected\').val()', //Valor de campo eres_jefe_depto
												'nombre_supervisor'=>'js:$(\'#SsSupervisoresProgramas_nombre_supervisor\').val()', //Valor de campo nombre_supervisor
												'apell_paterno'=>'js:$(\'#SsSupervisoresProgramas_apell_paterno\').val()', //Valor de campo apell_paterno
												'apell_materno'=>'js:$(\'#SsSupervisoresProgramas_apell_materno\').val()', //Valor de campo apell_materno
												'grMaxEstSup' => 'js:$(\'#SsSupervisoresProgramas_grMaxEstSup\').val()', //Valor de campo 
                                            ),
                                            'url'=>CController::createUrl('ssSupervisoresProgramas/esJefeDepto'),
                                            'success'=>'function(data) {
												if(data.es_jefe == true){
													$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", true );
													$("#SsSupervisoresProgramas_gradoMaxEstudios").val(data.grado).prop( "disabled", true ); 
												}else{
													$("#SsSupervisoresProgramas_nombre_jefe_depto").val(data.name_completo).prop( "disabled", false );
													$("#SsSupervisoresProgramas_gradoMaxEstudios").val(data.grado).prop( "disabled", false ); 
												}
                                        
											}',
											))
										); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'eres_jefe_depto'); ?>
    </div>

	<?php if($modelSSSupervisoresProgramas->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'nombre_jefe_depto'); ?>
        <?php echo $form->textField($modelSSSupervisoresProgramas,'nombre_jefe_depto',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'nombre_jefe_depto'); ?>
    </div>
	<?php }else{?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'nombre_jefe_depto'); ?>
        <?php echo $form->textField($modelSSSupervisoresProgramas,'nombre_jefe_depto',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required', 'disabled' => 'disabled')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'nombre_jefe_depto'); ?>
    </div>
	<?php } ?>

	<!--MAXIMO GRADO DE ESTUDIOS DEL JEFE DEL DEPTO. DE SUPERVISOR EXTERNO PARA LAS CARTAS DE ACEPTACION Y TERMINACION-->
	<div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'gradoMaxEstudios'); ?>
        <?php echo $form->textField($modelSSSupervisoresProgramas,'gradoMaxEstudios',array('size'=>5,'maxlength'=>5, 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'gradoMaxEstudios'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'passwSupervisor'); ?>
        <?php echo $form->passwordField($modelSSSupervisoresProgramas,'passwSupervisor',array('class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSSupervisoresProgramas,'passwSupervisor'); ?>
    </div>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSSupervisoresProgramas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	    <?php echo CHtml::link('Cancelar', array('listaSupervisoresProgramas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
