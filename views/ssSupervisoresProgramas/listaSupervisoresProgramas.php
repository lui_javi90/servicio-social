<?php
/* @var $this SupervisoresProgramasController */
/* @var $model SupervisoresProgramas */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Supervisores Externos',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Supervisores Externos 
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div align="right">
	<?php echo CHtml::link('Nuevo Supervisor Programa', array('nuevoSupervisorPrograma'), array('class'=>'btn btn-success')); ?>
</div>

<br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supervisores-programas-grid',
	'dataProvider'=>$modelSSSupervisoresProgramas->search(),
	'filter'=>$modelSSSupervisoresProgramas,
	'columns'=>array(
		array(
			'name' => 'rfcSupervisor',
			'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
		),
		array(
			'name' => 'nombre_supervisor',
			'value' => function($data)
			{
				$rfc_sup = $data->rfcSupervisor;
				$query =
				"
				Select (nombre_supervisor || ' ' || apell_paterno || ' ' || apell_materno) as name
				from pe_planeacion.ss_supervisores_programas
				where \"rfcSupervisor\" = '$rfc_sup'
				";

				$name = Yii::app()->db->createCommand($query)->queryAll();

				return (count($name) > 0) ? $name[0]['name']: "NO ESPECIFICADO";
			},
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'name' => 'idUnidadReceptora.nombre_unidad_receptora', 
			'filter' => CHtml::activeDropDownList($modelSSSupervisoresProgramas,
												   'id_unidad_receptora', 
												   $empresas,
												   array('prompt'=>'-- Filtrar por empresa --')
												),
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo de Supervisor',
			'value' => function($data)
			{
				return ($data->id_tipo_supervisor == 1) ? "INTERNO" : "EXTERNO";
			},
			'filter' => CHtml::activeDropDownList($modelSSSupervisoresProgramas,
												  'id_unidad_receptora', 
												  array('1'=>'INTERNO', '2'=>'EXTERNO'),
												  array('prompt'=>'-- Filtrar por tipo --')
												),
			'htmlOptions' => array('width'=>'110px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editarSupervisorPrograma}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editarSupervisorPrograma' => array
				(
					'label'=>'Editar Configuración',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSupervisoresProgramas/editarSupervisorPrograma", array("rfcSupervisor"=>$data->rfcSupervisor))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detalleSupervisorPrograma}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detalleSupervisorPrograma' => array
				(
					'label'=>'Editar Configuración',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSupervisoresProgramas/detalleSupervisorPrograma", array("rfcSupervisor"=>$data->rfcSupervisor))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{fotPerfilSupervisorPrograma}',
			'header'=>'Foto Perfil',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'fotPerfilSupervisorPrograma' => array
				(
					'label'=>'Cambiar Foto Perfil',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSupervisoresProgramas/fotoPerfilSupervisorPrograma", array("rfcSupervisor"=>$data->rfcSupervisor))',
					'imageUrl'=>'images/servicio_social/foto_empleado_32.png',
				),
			),
		),
	),
)); ?>
