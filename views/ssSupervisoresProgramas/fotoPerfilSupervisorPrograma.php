<?php 
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Supervisores Programas' => array('ssSupervisoresProgramas/listaSupervisoresProgramas'),
    'Cargar Foto Perfil Supervisor'
);
?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Foto Perfil Supervisor
		</span>
	</h2>
</div>

<br><br>
<div class="row">
    <div class="col-xs-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Cargar Foto Perfil del Supervisor
                </h6>
            </div>
            <div class="panel panel-body">
                    <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'foto-supervisores-programas-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                        //Para que el formulario permita la subida de archivos
                        'htmlOptions' => array('enctype'=>'multipart/form-data')
                    )); ?>

                    <?php echo $form->errorSummary($modelSSSupervisoresProgramas); ?>
                    <p align="center"><strong>Subir foto perfil es OPCIONAL.</strong></p>

                    <br>
                    <p><strong>NOTA:</strong> La foto del supervisor debe cumplir con los requisitos definidos.</p>

                    <p><strong>* El nombre del archivo debe ser de maximo 15 caracteres.</strong></p>
                    <p><strong>* Tamaño maximo de la imagen de 5MB.</strong></p>
                    <p><strong>* Solo formatos .png .jpg jpeg.</strong></p>

                    <br>
                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'foto_supervisor'); ?>
                        <?php echo $form->fileField($modelSSSupervisoresProgramas,'foto_supervisor',array('class'=>'form-control')); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'foto_supervisor'); ?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelSSSupervisoresProgramas->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                        <?php echo CHtml::link('Cancelar', array('listaSupervisoresProgramas'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
            <p align="center"><strong>Foto perfil actual.</strong></p>
            <br>
            
            <?php 
                echo (trim($modelSSSupervisoresProgramas->foto_supervisor) == 'femenino.png' || trim($modelSSSupervisoresProgramas->foto_supervisor) == 'masculino.png') ? '<img class="img-circle" align="center" width="180" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$modelSSSupervisoresProgramas->foto_supervisor. '"/>' : '<img class="img-circle" align="center" width="180" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$modelSSSupervisoresProgramas->rfcSupervisor.'/'.$modelSSSupervisoresProgramas->foto_supervisor.'"/>';
            ?>
            </div>
            <br><br><br>
        </div>
    </div>
</div>
