<?php
/* @var $this SupervisoresProgramasController */
/* @var $model SupervisoresProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Supervisores Programas' => array('ssSupervisoresProgramas/listaSupervisoresProgramas'),
	'Detalle Supervisor',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Supervisor
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Detalle Supervisor
                </h6>
            </div>
            <div class="panel-body">
                <!--Foto Supervisor-->
                <div style="padding-top:50px;" class="col-xs-5" align="center">
                    <?php 
                        echo (trim($modelSSSupervisoresProgramas->foto_supervisor) == "femenino.png" || trim($modelSSSupervisoresProgramas->foto_supervisor) == "masculino.png") ? '<img class="img-circle" align="center" width="250" heigth="200" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$modelSSSupervisoresProgramas->foto_supervisor.'"/>' : '<img class="img-circle" align="center" width="250" heigth="200" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.trim($modelSSSupervisoresProgramas->rfcSupervisor).'/'.$modelSSSupervisoresProgramas->foto_supervisor.'"/>';
                    ?>
                </div>
                <!--Foto Supervisor-->
                <!--Informacion Supervisor-->
                <div class="col-xs-7">
                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('nombre_supervisor')); ?>:</b>
                    <p><?php echo $modelSSSupervisoresProgramas->nombre_supervisor.' '.$modelSSSupervisoresProgramas->apell_paterno.' '.$modelSSSupervisoresProgramas->apell_materno; ?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('rfcSupervisor')); ?>:</b>
                    <p><?php echo $modelSSSupervisoresProgramas->rfcSupervisor; ?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('id_unidad_receptora')); ?>:</b>
                    <p><?php echo $name_empresa; ?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('cargo_supervisor')); ?>:</b>
                    <p><?php echo $modelSSSupervisoresProgramas->cargo_supervisor?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('email_supervisor')); ?>:</b>
                    <p><?php echo $modelSSSupervisoresProgramas->email_supervisor?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('id_tipo_supervisor')); ?>:</b>
                    <p><?php echo ($modelSSSupervisoresProgramas->id_tipo_supervisor == '1') ? "INTERNO": "EXTERNO"; ?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('dscDepartamento')); ?>:</b>
                    <p><?php echo $modelSSSupervisoresProgramas->dscDepartamento?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('eres_jefe_depto')); ?></b>
                    <p><?php echo ($modelSSSupervisoresProgramas->eres_jefe_depto == 1 ) ? "SI" : "NO"; ?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('nombre_jefe_depto')); ?>:</b>
                    <p><?php echo $modelSSSupervisoresProgramas->nombre_jefe_depto?></p>

                    <b><?php echo CHtml::encode($modelSSSupervisoresProgramas->getAttributeLabel('id_status_supervisor')); ?>:</b>
                    <p><?php echo ($modelSSSupervisoresProgramas->id_status_supervisor == '1') ? "ALTA": "BAJA"; ?></p>
                </div>
                <!--Informacion Supervisor-->
            </div>
            <br><br>
            <div align="center">
                <?php echo CHtml::link('Entendido', array('listaSupervisoresProgramas'), array('class'=>'btn btn-success')); ?>
            </div>
            <br><br>
        </div>
    </div>
</div>