<?php
/* @var $this ActividadesServicioSocialController */
/* @var $model ActividadesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Actividades Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Actividades Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXAlumnosAltaServSocial(),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*'id_servicio_social',
        'horas_realizadas',
        'fecha_inicio',
        'fecha_fin',*/
        array(//Filtrar las actividades por el no. control del alumno
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(//Filtrar las actividades por el no. control del alumno
			'header' => 'Nombre del Alumno',
			'value' => function($data)
			{
				$query = "select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = '$data->no_ctrl' ";

				$name = Yii::app()->db->createCommand($query)->queryAll();
				return $name[0]['nmbAlumno'];
			},
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        array(//Filtrar las actividades por el no. control del alumno
			'name' => 'idPrograma.nombre_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
		),
        /*
        'status_servicio_social',
        'id_estado_servicio_social',
        'fecha_registro',
        'fecha_modificacion',
        'id_periodo_servicio_social',
        'año_servicio_social',
        'id_periodo_escolar',
        'id_tipo_servicio_social',
        'calificacion_servicio_social',
        
        array(
			'class'=>'CButtonColumn',
			'template'=>'{detPlanTrabajo}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detPlanTrabajo' => array
				(
					'label'=>'Detalle del Plan de Trabajo',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssActividadesServicioSocial/listaAdminActividadesServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/detalle_32.png',
					//'visible' =>'$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
			),
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impPlanTrabajo}',
			'header'=>'Imprimir',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impPlanTrabajo' => array
				(
					'label'=>'Imprimir Plan de Trabajo',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssActividadesServicioSocial/imprimirPlanDeTrabajoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
					//'visible' =>'$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
			),
		),
    ),
)); ?>

<br><br><br>
<div align="center">
	<?php //echo CHtml::link('Volver al Menú Principal', array('/'), array('class'=>'btn btn-success')); ?>
</div>
