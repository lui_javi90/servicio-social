<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

if($modelServicioSocial->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Lista Servicio Social' => array('servicioSocial/listaServicioSocial'),
		'Nuevo Servicio Social'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Lista Servicio Social' => array('servicioSocial/listaServicioSocial'),
		'Editar Servicio Social'
	);
}

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelServicioSocial->isNewRecord) ? "Nuevo Servicio Social" : "Editar Servicio Social"; ?>
		</span>
	</h2>
</div>

<br>
<div clas="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Nuevo Servicio Social
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formServicioSocial', array(
											'modelServicioSocial'=>$modelServicioSocial,
											'lista_programas' => $lista_programas,
											'lista_estados_servicio' => $lista_estados_servicio
											)); ?>
			</div>
		</div>
	</div>
</div>

