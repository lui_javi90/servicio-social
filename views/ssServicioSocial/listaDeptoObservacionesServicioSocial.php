<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Observaciones Depto Servicio Social',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Observaciones Depto Servicio Social
		</span>
	</h2>
</div>


<br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'observ-depto-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXAlumnosAltaServSocial(),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*array(
            'name' => 'id_servicio_social',
            'filter' => false,
            'htmlOptions' => array('width'=>'10px', 'class'=>'text-center')
        ),*/
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPrograma.nombre_programa',
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idEstadoServicioSocial.estado',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Tipo de Servicio Social',
            'value' => function($data)
            {
                return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{observServicioSocialAlumno}',
			'header'=>'Observación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'observServicioSocialAlumno' => array
				(
					'label'=>'Nueva Observación',
                    'url'=>'Yii::app()->createUrl("serviciosocial/ssObservacionesServicioSocial/nuevaObservacionDeptoServicioSocial", 
                    array("id_servicio_social"=>$data->id_servicio_social, "no_ctrl" => $data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
				),
			),
        ),
        array(
			'class' => 'ComponentObservacionesXAlumnoDepto',
			'header' => 'Observaciones Pendientes de Ver',
			'htmlOptions' => array('width' => '80px', 'class' => 'text-center'),
		),
        /*array(
			'class'=>'CButtonColumn',
			'template'=>'{histServicioSocialAlumno}',
			'header'=>'Historial',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'histServicioSocialAlumno' => array
				(
					'label'=>'Historial Observaciones',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssObservacionesServicioSocial/historialAlumnoObservacionesServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/historial_observaciones_32.png',
				),
			),
        ),*/
    ),
)); ?>
