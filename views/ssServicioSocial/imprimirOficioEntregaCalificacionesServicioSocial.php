<style>
    .hoja{
        background-color: transparent;
    }
    .div1{
        background-color: transparent;
        font-weight: bold;
    }
    .div2{
        background-color: transparent;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 11px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado3{
        line-height: 3pt;
    }
</style>

<div class="hoja">

    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />

    <!--leyenda si es mes patrio o natalcio importante-->
    <br><br>
    <h5 style="font-style: italic;" class="center letra4">2019 <span class="bold">"Año del Caudillo del Sur, Emiliano Zapata"</span></h5>
    <br>
    <!--leyenda si es mes patrio o natalcio importante-->

    <!--fecha, oficio y asunto-->
    <div class="div2 interlineado3">
        <table style="width:70%" class="interlineado3 letra3" align="right">
            <tr class="interlineado3">
                <td align="right"><?php echo "<span class='bold letra1'>GESTIÓN TECNOLÓGICA Y VINCULACIÓN</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "Celaya, Guanajuato,&nbsp;<span style='background-color:black;color:white;' class='bold letra1'>".$fec_reporte."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "CIRCULAR:&nbsp;<span class='bold letra1'>".'102'."/".$anio_genera_rep."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "ASUNTO:&nbsp;<span class='bold letra1'>OFICIO DE CALIFICACIONES DE SERVICIO SOCIAL</span>"; ?></td>
            </tr>
        </table>
    </div>
    <!--fecha, oficio y asunto-->

   <!--Datos del Jefe de Servicios Escolares-->
   <br><br><br>
    <div class="div2 interlineado1">
        <h6 class="bold letra3 interlineado1"><?php echo "ING. ".$jefe_escolares[0]['nmbCompletoEmp']; ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo $jefe_escolares[0]['nmbPuesto']; ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo "INSTITUTO TECNOLÓGICO DE CELAYA"; ?></h6>
        <h6 class="bold letra3 interlineado1">P R E S E N T E</h6>
        <!--<h6 class="bold letra3 interlineado1">PRESENTE</h6>-->
    </div>
    <!--Datos del Jefe de Servicios Escolares-->

    <!--Cuerpo del Oficio-->
    <br><br>
    <p align="justify">
        Por este conducto me dirijo a Usted para enviarle en relación adjunta <span class="bold">( <?php echo "90"." alumnos"; ?> )</span> con las
        calificaciones de término de Servicio Social, recibidos en la oficina de Servicio Social y Desarrollo Comunitario
        del Departamento de Gestión Tecnológica y Vinculación, durante el periodo de <?php echo ""; ?>.<br><br>

        Para registro de la calificación correspondiente en el SII y se ven reflejadas en los Cardex de nuestros alumnos
        para dar seguimiento a sus trámites posteriores.<br><br>

        Le agradezco antemano su gentil atención al presente oficio, enviándole un cordial y afectuoso saludo.
    </p>
    <!--Cuerpo del Oficio-->

    <!--Firma del encargado-->
    <br>
    <div class="interlineado1 div2 left">
        <h6 class="interlineado1 bold letra3">A T E N T A M E N T E</h6>
        <i class="interlineado1 letra3">LA TÉCNICA POR UN MÉXICO MEJOR &reg;</i>
    </div>

    <div class="">

    </div>

    <!--Datos del archivo-->
    <br><br><br>
    <div class="div2 left">
        <h6 class="interlineado1 letra2"><?php echo "ELL/cclm"; ?></h6>
    </div>
    <!--Datos del archivo-->


</div>