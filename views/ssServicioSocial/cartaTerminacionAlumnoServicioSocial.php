<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Carta Terminación Servicio Social',
);

/*JS PARA VALIDAR ENVIO DE LA CARTA DE TERMINACION DE SERVICIO SOCIAL */
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
	confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
	url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#hist-cartaterm-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#hist-cartaterm-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA VALIDAR ENVIO DE LA CARTA DE TERMINACION DE SERVICIO SOCIAL */

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Carta Terminación Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Verifica la Carta de Terminación de tu Servicio Social que completaste y procede a enviarla
        a la oficina de Servicio Social para iniciar el tramite de liberación de tu Servicio Social.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'hist-cartaterm-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXHistoricoAlumno($no_ctrl),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*'id_servicio_social',
        'horas_realizadas',
        'fecha_inicio',
        'fecha_fin',
        'no_ctrl',
        'status_servicio_social',
        'id_estado_servicio_social',
        'fecha_registro',
        'fecha_modificacion',
        'id_periodo_servicio_social',
        'año_servicio_social',
        'id_periodo_escolar',
        'id_tipo_servicio_social',*/
        array(
            'name' => 'idProgramaServicioSocial.nombre_programa', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Tipo Servicio Social', //id_tipo_servicio_social
            'value' => function($data)
            {
                return ($data->id_tipo_servicio_social == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'name' => 'calificacion_servicio_social', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{viewCartaTerminacion}',
			'header'=>'Carta Terminación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'viewCartaTerminacion' => array
				(
					'label'=>'Carta Terminación',
					'url'=>'Yii::app()->createUrl("serviciosocial/servicioSocial/imprimirCartaTerminacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/printer.png',
				),
			),
        ),
        [//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{enviado}, {noenviado}', // buttons here...
			'header' 			   => 'Enviar a Evaluación',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'enviado'   => [
					'label'   => 'Enviar Reporte Bimestral a Evaluación',
					'url'     => 'Yii::app()->createUrl("serviciosocial/reportesBimestral/envioAlumnoReporteBimestral", array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "oper" => 1))', // ?r=controller/approve/id/123
					'imageUrl'=> 'images/aprobado_32.png',
					'visible'=> '$data->envio_alum_evaluacion == 1', // <-- SHOW IF ROW ACTIVE
					'options' => [
						'title'        => 'Enviado',
						'data-confirm' => 'Lo siento pero el Reporte Bimestral ya fue enviado a evaluación.', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
				'noenviado' => [
					'label'   => 'Cancelar envio Reporte Bimestral a Evaluación',
					'url'     => 'Yii::app()->createUrl("serviciosocial/reportesBimestral/envioAlumnoReporteBimestral", array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "oper" => 0))', //
					'imageUrl'=>'images/cancelado_32.png',
					'visible'=> '$data->envio_alum_evaluacion == 0', // <-- SHOW IF ROW INACTIVE
					'options' => [
						'title'        => 'No Enviado',//
						'data-confirm' => 'Confirmar envio de Reporte Bimestral a Revisión?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],//Fin
    ),
)); ?>