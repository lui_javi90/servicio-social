<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

if($modelSSServicioSocial->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Lista Servicio Social' => array('servicioSocial/listaAdminServicioSocial'),
		'Status Servicio Social'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Lista Servicio Social' => array('servicioSocial/listaAdminServicioSocial'),
		'Editar Status Servicio Social'
	);
}
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Información del Alumnos
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Información Servicio Social</h6>
			</div>
			<div class="panel-body">
                <div class="col-xs-3" align="center">
					<!--Es un Web Service-->
                    <img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $modelSSServicioSocial->no_ctrl; ?>" alt="foto alumno" height="200">
                    <!--Es un Web Service-->
                </div>
				<div class="col-xs-9" align="left">
                <p><b>Nombre Alumno:</b>
                &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('no_ctrl')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelSSServicioSocial->no_ctrl; ?></p>

                <p><b><?php echo CHtml::encode($modelHorasTotalesAlumnosServicioSocial->getAttributeLabel('horas_totales_servicio_social')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelHorasTotalesAlumnosServicioSocial->horas_totales_servicio_social;?></p>
                
                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('fecha_inicio')); ?>:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->fecha_inicio != null) ? $modelSSServicioSocial->fecha_inicio : "POR DETERMINAR";?></p>
                
                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('fecha_fin')); ?>:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->fecha_fin != null) ? $modelSSServicioSocial->fecha_inicio : "POR DETERMINAR";?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('horas_realizadas')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelSSServicioSocial->horas_realizadas;?></p>
                
                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('status_servicio_social')); ?>:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->status_servicio_social == 1 ) ? "ACTIVO" : "INACTIVO";?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('id_estado_servicio_social')); ?>:</b>
				&nbsp;&nbsp;<?php echo $estado_servicio_social; ?></p>
				
				<p><b>Calificación del Servicio Social:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->calificacion_servicio_social >=70) ? 
                                "<b><FONT COLOR=\"green\">".$modelSSServicioSocial->calificacion_servicio_social."</FONT></b>" :
                                "<b><FONT COLOR=\"red\">".$modelSSServicioSocial->calificacion_servicio_social."</FONT></b>"; ?>
                </div>
			</div>
		</div>
	</div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información de Servicio Social
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$modelSSServicioSocial,
    'attributes'=>array(
        //'id_servicio_social',
        'horas_realizadas',
		array(
			'name' => 'fecha_inicio',
			'oneRow'=>true,
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_registro = InfoServicioSocial::getFechaInicioServicioSocial($data->id_servicio_social);
                return $fecha_registro[0]['fecha_act'];
			},
		),
		array(
			'name' => 'fecha_fin',
			'oneRow'=>true,
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_registro = InfoServicioSocial::getFechaFinServicioSocial($data->id_servicio_social);
                return $fecha_registro[0]['fecha_act'];
			},
		),
        'no_ctrl',
        //'status_servicio_social',
        'idEstadoServicioSocial.estado',
        'idProgramaServicioSocial.nombre_programa',
		array(
            'name' => 'fecha_registro',
            'oneRow'=>true,
            'value' => function($data)
            {
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_registro = InfoServicioSocial::getFechaRegistroServicioSocial($data->id_servicio_social);
                return $fecha_registro[0]['fecha_act'].' a las '.$fecha_registro[0]['hora'];
            },
        ),
		array(
            'name' => 'fecha_modificacion',
            'oneRow'=>true,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_modificacion = InfoServicioSocial::getFechaModificacionServicioSocial($data->id_servicio_social);
                return $fecha_modificacion[0]['fecha_act'].' a las '.$fecha_modificacion[0]['hora'];
            },
        ),
    ),
)); ?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSServicioSocial->isNewRecord) ? "Status Servicio Social" : "Editar Status Servicio Social"; ?>
		</span>
	</h2>
</div>

<br>
<div clas="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
                    <?php echo ($modelSSServicioSocial->isNewRecord) ? "Status Servicio Social" : "Editar Status Servicio Social"; ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formStatusServicioSocial', array(
                                            'modelSSServicioSocial'=>$modelSSServicioSocial,
                                            'lista_edos_servicio' => $lista_edos_servicio,
											)); ?>
			</div>
		</div>
	</div>
</div>