<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Históricos Servicio Social',
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Históricos Servicio Social
		</span>
	</h2>
</div>

<!--<br><br>
<div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Filtrar por Estado del Servicio Social
                    </h3>
                </div>
                <div class="panel-body">-->
					<!--index.php hace referencia al mismo archivo donde se esta y get es el metodo de envio de datos-->
                    <?php //echo CHtml::beginForm("index.php", "get"); ?>

                    <!--<input type="hidden" name='r' value='serviciosocial/ssServicioSocial/historicoAdminServicioSocial'>

                    <b>Estados:</b>
                    <div class="form-group">
													<?php /*echo CHtml::dropDownList('id_estado_servicio_social',
													'',
													$lista_edos_ssocial,
													array('prompt'=>'--Elige estado Servicio Social--', 'class'=>'form-control')
											);*/?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php //echo CHtml::submitButton("Buscar por estado de Servicio Social", array("class"=>"btn btn-primary")); ?>
                        <?php //echo CHtml::link('Salir de aquí', array('/'), array('class'=>'btn btn-danger')); ?>
                    </div>

                    <?php //echo CHtml::endForm();?>
                </div>
            </div>
        </div>
</div>-->

<br><br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Lista de TODOS los Servicio Sociales que han FINALIZADO los alumnos, es decir, que se han completado
		satisfactoriamente. Tambien se puede visualizar los Servicio Sociales CANCELADOS.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'hist-servicio-social-grid',
	'dataProvider'=>$modelSSServicioSocial->searchXEdoHistoricoServicioSocial($anio, $periodo),
	'filter'=>$modelSSServicioSocial,
	'columns'=>array(
		//'id_servicio_social',
		array(
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
			'name' => 'idPrograma.nombre_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo Servicio Social',
			'name' => 'id_tipo_programa',
			'value' => function($data)
			{
				return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO": "EXTERNO";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'header' => 'Horas Completadas',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->idPrograma->horas_totales.'</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		/*array(
			'name' => 'fecha_inicio',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_actualizacion = InfoServicioSocial::getFechaInicioServicioSocial($data->id_servicio_social);
				return $fecha_actualizacion[0]['fecha_act'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'name' => 'fecha_fin',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_actualizacion = InfoServicioSocial::getFechaFinServicioSocial($data->id_servicio_social);
				return $fecha_actualizacion[0]['fecha_act'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Periodo',
			'filter' => CHtml::activeDropDownList($modelSSServicioSocial,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
			'type' => 'raw',
			'value' => function($data)
			{
				$id = $data->id_programa;

                $qry = "select rf.id_periodo as periodo from pe_planeacion.ss_programas p
                        join pe_planeacion.ss_registro_fechas_servicio_social rf
                        on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                        where p.id_programa = '$id' ";

                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                $per = ($rs[0]['periodo'] == 1) ? "ENERO-JUNIO" : "AGOSTO_DICIEMBRE";

                return '<span style="font-size:14px" class="label label-info">'.$per.'</span>';
			},
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'header' => 'Año',
			'filter' => CHtml::activeDropDownList($modelSSServicioSocial,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
			'type' => 'raw',
			'value' => function($data)
			{
				$id = $data->id_programa;

                $qry = "select rf.anio from pe_planeacion.ss_programas p
                        join pe_planeacion.ss_registro_fechas_servicio_social rf
                        on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                        where p.id_programa = '$id' ";

                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                return '<span style="font-size:14px" class="label label-info">'.$rs[0]['anio'].'</span>';
			},
			'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
		),
		array(
			'header' => 'Estatus',
			'filter' => CHtml::activeDropDownList($modelSSServicioSocial,
													'id_estado_servicio_social',
													$lista_edos_ssocial,
													array('prompt'=>'-- Estatus --')
			),
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->id_estado_servicio_social == 6) ? '<span style="font-size:14px" class="label label-default">FINALIZADO</span>' : '<span style="font-size:14px" class="label label-danger">FINALIZADO</span>';
			},
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		/*'status_servicio_social',
		'fecha_registro',
		'fecha_modificacion',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detalleServicioSocialAlumno}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detalleServicioSocialAlumno' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/detalleAdminHistoricoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{repBimHistServicioSocialAlumno}',
			'header'=>'Reportes Bimestrales',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'repBimHistServicioSocialAlumno' => array
				(
					'label'=>'Reportes Bimestrales',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/repBimHistoricoAdminServicioSocial", array("id_servicio_social"=>$data->id_servicio_social, "no_ctrl" => $data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/historial_docs_servsocial_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{cartHistServicioSocialAlumno}',
			'header'=>'Cartas',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'cartHistServicioSocialAlumno' => array
				(
					'label'=>'Cartas Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/cartasHistoricoAdminServicioSocial", array("id_servicio_social"=>$data->id_servicio_social, "no_ctrl" => $data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/cartas_servsocial_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impPlanTrabajo}',
			'header'=>'Actividades',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impPlanTrabajo' => array
				(
					'label'=>'Imprimir Plan de Trabajo',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssActividadesServicioSocial/imprimirPlanDeTrabajoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
					//'visible' =>'$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
			),
		),
	),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>