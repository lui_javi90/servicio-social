<style>
    .hoja{
        background-color: transparent;
    }
    .div2{
        background-color: transparent;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 7px;
    }
    .letra4{
        font-size: 11px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado2{
        /*letter-spacing: 1pt;       para separar entre letras */
        word-spacing: 1.5pt;        /* para separacion entre palabras */
        line-height: 20pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado3{
        line-height: 10pt;
    }
    .interlineado4{
        /*letter-spacing: 1pt;       para separar entre letras */
        word-spacing: 1.5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    #contenedor{
      margin-left:0px;
      margin-top:0px;
      padding:0 0 0 0;
      width:auto;
      /*background-color: #2196F3;*/
      position:absolute;
  }
  .divcontenido1{

      float:left;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:70%;
      text-align:left;
  }
  .divcontenido2{

      float:right;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:25%;
      text-align:left;
      margin-left:0px;
  }
</style>

<div class="hoja">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />
<?php //echo '<img src="images/encabezados_empresas/'.trim($datos_carta_terminacion[0]['path_carpeta']).'/banner_superior_'.trim($datos_carta_terminacion[0]['no_empresa']).'/'.trim($datos_carta_terminacion[0]['banner_superior']).'"/>'; ?>
    <br><br>
    <h5 style="font-style: italic;" class="center letra4"><span class="bold">"
                                            <?php echo ($leyenda_reporte_pdf != NULL) ? $anio.", ".$leyenda_reporte_pdf : ""; ?>
                                                        "</span></h5>
    <br>
    <!--fecha, oficio y asunto-->
    <div class="div2 interlineado2">
        <table style="width:50%" class="interlineado3 letra3" align="right">
            <tr class="interlineado3">
                <td align="right"><?php echo "Celaya, Gto.&nbsp;<span style='background-color:black;color:white;' class='bold'>".$fec_reporte."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "Oficio No.&nbsp;<span class='bold'>".$datos_carta_terminacion[0]['folio']."/".$datos_carta_terminacion[0]['anio_servicio_social']."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "Asunto:&nbsp;<span class='bold'>CARTA DE TERMINACIÓN</span>"; ?></td>
            </tr>
        </table>
    </div>
    <!--fecha, oficio y asunto-->
    <!--Datos del director del tecnologico, servicio social interno-->
    <br><br>
    <div class="div2">
        <h6 class="bold letra3 interlineado1"><u><?php echo $datos_carta_terminacion[0]['grado_max_estudios_director']." ".$datos_carta_terminacion[0]['director']; ?></u></h6>
        <h6 class="bold letra3 interlineado1">DIRECTOR</h6>
        <h6 class="bold letra3 interlineado1"><?php echo $datos_carta_terminacion[0]['institucion']; ?></h6>
        <h6 class="bold letra3 interlineado1">PRESENTE.</h6>
    </div>
    <!--Datos del director del tecnologico-->
    <br>
    <!--Datos del Jefe de Depto. de Vinculacion y Gestion Tecnologica, servicio social interno-->
    <div class="div2 right">
        <h6 class="bold letra3 interlineado1"><u>AT´N.: <?php echo $datos_carta_terminacion[0]['grado_max_estudios_jefvinculacion']." ".$datos_carta_terminacion[0]['jefe_vinculacion']; ?></u></h6>
        <h6 class="bold letra3 interlineado1">Jefe del Depto. de Gestión Tecnológica y Vinculación</h6>
    </div>
    <!--Datos del Jefe de Depto. de Vinculacion y Gestion Tecnologica-->

    <!--Cuerpo de la carta de terminacion-->
    <br><br>
    <div class="interlineado4 div2 letra3">
    <p ALIGN="justify">
        Por medio del presente me permito informarle que el (a) <u><?php echo "<span class='bold'>"."C. ".$datos_carta_terminacion[0]['nombre_alumno']."</span>";?></u>, con
        número de control <u><?php echo "<span class='bold'>".$datos_carta_terminacion[0]['no_ctrl']."</span>"; ?></u> de la carrera de <u><?php echo "<span class='bold'>".$datos_carta_terminacion[0]['carrera_alumno']."</span>"; ?></u>, realizó su <?php echo "<span class='bold'>Servicio
        Social</span>"; ?> en el área de <u><?php echo "<span class='bold'>".$datos_carta_terminacion[0]['departamentoSupervisorJefe']."</span>"; ?></u>, en el programa de: <u><?php echo "<span class='bold'>".$datos_carta_terminacion[0]['nombre_programa']."</span>"; ?></u>,
        durante el período comprendido del <u><?php echo "<span class='bold'>".$fec_inicio."</span>"; ?></u> al <u><?php echo "<span class='bold'>".$fec_fin."</span>"; ?></u>,
        cubriendo un total de <u><?php echo "<span class='bold'>".$datos_carta_terminacion[0]['horas_totales']." horas."."</span>";?></u>
        <br><br><br>
        Agradeciendo su apoyo, reciba un cordial saludo.
    </p>
    </div>
    <!--Cuerpo de la carta de terminacion-->
    <!--Firma del encargado-->
    <br>
    <div class="interlineado1 div2 left">
        <h6 class="interlineado1 bold letra3">A T E N T A M E N T E</h6>
        <i class="interlineado1 letra3">LA TÉCNICA POR UN MÉXICO MEJOR &reg;</i>
    </div>
    
    <div id="contenedor">
      <div class="divcontenido1 left">
        <?php
        if($datos_carta_terminacion[0]['valida_solicitud_supervisor_programa'] == NULL or $datos_carta_terminacion[0]['id_estado_solicitud_programa_supervisor'] == NULL)
        {
            /*Poner un if que si la firma no viene entonces deje el espacio en blanco*/
            echo "<br><br><br>";
        }else{

            echo "<br><br><br>";
            echo "El Jefe de Departamento valido la solicitud del Alumno el dia <br>"."<span class='bold'>".$fecha_validacion_sol[0]['fecha_act']." a las ".$fecha_validacion_sol[0]['hora']."</span>";
            echo "<h6 class=\"interlineado1 bold letra3\">".$datos_carta_terminacion[0]['nombre_jefe_depto']."</h6>";
        }
        ?>
            <!--<img height="100" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/catalina.jpg" />-->
            <?php echo "<span class='bold'>______________________________________________________________</span>"; ?>
            <h6 class="interlineado1 bold letra2">JEFE DEL DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
            <!--<h6 class="interlineado1 letra3"><?php //echo $datos_carta_terminacion[0]['cargo_supervisor']?></h6>-->
            <!--<h6 class="interlineado1 letra3"><?php //echo "JEFE DEL ".$datos_carta_terminacion[0]['departamentoSupervisorJefe']?></h6>-->
         
      </div>
      <div class="divcontenido2 center">
          <!--<img height="150" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/sello_sep.jpg" />-->
      </div>
    </div>
    <!--Firma del encargado-->
    <!--Codigo calidad al pie de pagina-->
    <br><br><br>
    <div class="div2">
        <h6 class="interlineado1 letra3">C.p. Archivo.</h6>
        <h6 class="interlineado1 letra3">JARB/ftcc*</h6>
    </div>
    <!--Codigo calidad al pie de pagina-->

    
</div>
