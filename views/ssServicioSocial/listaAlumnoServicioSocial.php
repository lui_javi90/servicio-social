<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Lista Servicio Social',

);?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			Podras descargar tu Carta de Terminacion una vez que hayas completado las evaluación y validación de todos tus Reportes Bimestrales de Servicio Social.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alumno-servicio-social-grid',
	'dataProvider'=>$modelSSServicioSocial->searchXAlumno($no_ctrl),
	'filter'=>$modelSSServicioSocial,
	'columns'=>array(
		/*'id_servicio_social',
		array(
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
            'name' => 'Nombre del Alumno',//
            'value' => function($data)
            {
                $query =" SELECT (nombres||' '||apellido_paterno||' '||apellido_materno) as name
                from alumnos_vigentes_inscritos_creditos
                WHERE no_ctrl='$data->no_ctrl'
                ";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return (count($result) > 0) ? $result[0]['name'] : "NO ESPECIFICADO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'230px', 'class'=>'text-center')
        ),*/
		array(
			'name' => 'idPrograma.nombre_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'name' => 'idPrograma.horas_totales',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->idPrograma->horas_totales.'</span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Fecha Inicio',
			'value' => function($data)
			{
				return ($data->idPrograma->fecha_inicio_programa != null) ? $data->idPrograma->fecha_inicio_programa : "POR CONFIRMAR";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'header' => 'Fecha Fin',
			'value' => function($data)
			{
				return ($data->idPrograma->fecha_fin_programa != null) ? $data->idPrograma->fecha_fin_programa : "POR CONFIRMAR";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),*/
		array(
			'name' => 'idEstadoServicioSocial.estado',
			'filter' => false,
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		/*'status_servicio_social',
		'fecha_registro',
		'fecha_modificacion',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detalleServicioSocialAlumno}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detalleServicioSocialAlumno' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/detalleAlumnoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{horarioServicioSocial}',
			'header'=>'Horario <br>Programa',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'horarioServicioSocial' => array
				(
					'label'=>'Horario Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/showHorarioProgramaServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/horario_32.png',
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#horario_programa").dialog("open"); 
						
						return false;
					}',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impCartaAceptacion}',
			'header'=>'Carta Aceptación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				//Vista servicio social interno
				'impCartaAceptacion' => array
				(
					'label'=>'Imprimir Carta de Aceptación Interna',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/imprimirCartaAceptacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' =>'$data->id_estado_servicio_social != 7'
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impCartaPresentacion}',
			'header'=>'Carta Presentación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=> array
			(
				'impCartaPresentacion' => array
				(
					'label'=>'Imprimir Carta Presentanción',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/imprimirCartaPresentacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
		),//
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impCartaTerminacion}, {nodisponible}',
			'header'=>'Carta Terminación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impCartaTerminacion' => array
				(
					'label'=>'Carta Terminación Disponible',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/imprimirCartaTerminacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' =>'$data->id_estado_servicio_social > 3 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
				'nodisponible' => array
				(
					'label'=>'Carta Terminación NO Disponible',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' =>'$data->id_estado_servicio_social < 4 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
			),
		),
	),
)); ?>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'horario_programa',
	'options' => array(
		'title' => 'Horario del Programa',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1200, 
		'height' => 550,
		/*'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),*/
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<br><br><br><br><br>
<br><br><br><br><br>
