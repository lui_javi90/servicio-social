<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Candidatos a Liberar Servicio Social',
);?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Candidatos a Liberar Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br>
<div class="row">

    <!--<div class="col-md-3"></div>-->
    <div class="col-md-12">
        <div align="right" class="panel panel-default">
            <div align="left" class="panel-heading">
                <h4 class="panel-title">
                    <b>Completar Todos los Servicio Sociales:</b>
                </h4>
            </div>
            <div align="left" class="panel-body">

            <!--index.php hace referencia al mismo archivo donde se esta y get es el metodo de envio de datos-->
            <?php echo CHtml::beginForm("index.php", "get"); ?>

            <input type="hidden" name='r' value='serviciosocial/ssServicioSocial/listaCandidatosALiberarServicioSocial'>

            <b>Estado:</b>
            <div class="form-group">
                <?php echo CHtml::dropDownList('id_estado_servicio_social',
                                                '',
                                                $lista_estado,
                                                array('prompt'=>'-- Estado Servicio Social --', 'class'=>'form-control')
                                    );?>
            </div>

            <div align="right" class="form-group">
                <?php echo CHtml::submitButton("Completar Todos", array("class"=>"btn btn-success")); ?>
            </div>

            <?php echo CHtml::endForm();?>
                
            </div>
        </div>
    </div>
    <!--<div class="col-md-3"></div>-->

</div>


<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		En esta lista solo aparecen los Servicio Sociales de los alumnos que tiene evaluados y validados todos sus reportes bimestrales sin excepción alguna.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'cand-lib-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchServicioSocialXReportesEvaluadosYValidados(),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*array(
            'name' => 'id_servicio_social',
            'htmlOptions' => array('width'=>'20px')
        ),*/
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        /*array(
            'name' => 'noCtrl.dscEspecialidad',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),*/
        array(
            'name' => 'idPrograma.nombre_programa', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Horas <br>Completadas',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:18px" class="label label-success">'.$data->idPrograma->horas_totales.'</span>';
            },
            'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estado',
            //'name' => 'idEstadoServicioSocial.estado', //id_estado_servicio_social
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                $edo = $data->idEstadoServicioSocial->estado;

                return ($data->id_estado_servicio_social == 4) ? '<span style="font-size:14px" class="label label-info">'.$edo.'</span>' : 'Desconocido';
            },
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        /*array(
            'header' => 'Tipo Servicio Social', //id_tipo_servicio_social
            'value' => function($data)
            {
                return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),*/
        array(
			'class'=>'CButtonColumn',
			'template'=>'{detLibServicioSocialAlumno}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:80px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detLibServicioSocialAlumno' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/detalleLiberacionServicioSocialAlumno", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{impCartaTerminacion}',
			'header'=>'Carta Terminación',
			'htmlOptions'=>array('width:80px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impCartaTerminacion' => array
				(
					'label'=>'Carta Terminación Disponible',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/ImprimirCartaHistoricaTerminacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' =>'($data->id_estado_servicio_social > 3 AND $data->id_estado_servicio_social != 7)' //Para visualizar su carta de terminación
				),
			),
		),
    array(
			'class'=>'CButtonColumn',
			'template'=>'{libServicioSocialAlumno}',
			'header'=>'Liberar',
			'htmlOptions'=>array('width:80px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'libServicioSocialAlumno' => array
				(
					'label'=>'Liberar Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/liberarServicioSocialAlumno", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/liberar_servsocial_32.png',
				),
			),
		),
    ),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
