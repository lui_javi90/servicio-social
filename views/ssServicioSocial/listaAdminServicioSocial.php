<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Lista Servicio Social',
);

/*JS PARA DAR DE BAJA EL SERVICIO SOCIAL DEL ALUNMO*/
$valSSocialJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA DAR DE BAJA EL SERVICIO SOCIAL DEL ALUNMO*/

/*JS PARA CANCELAR EL SERVICIO SOCIAL DEL ALUMNO*/
$bajSSocialJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA DAR DE BAJA EL SERVICIO SOCIAL DEL ALUMNO*/

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Servicio Social
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Filtrar por Programa de Servicio Social
                    </h3>
                </div>
                <div class="panel-body">
					<!--index.php hace referencia al mismo archivo donde se esta y get es el metodo de envio de datos-->
                    <?php echo CHtml::beginForm("index.php", "get"); ?>

                    <input type="hidden" name='r' value='serviciosocial/ssServicioSocial/listaAdminServicioSocial'>

                    <b>Estados:</b>
                    <div class="form-group">
						<?php echo CHtml::dropDownList('id_programa',
														'',
														$lista_programas_activos,
														array('prompt'=>'-- Selecciona Programa Servicio Social --', 'class'=>'form-control')
											);?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php echo CHtml::submitButton("Buscar Estatus", array("class"=>"btn btn-primary")); ?>
                        <?php echo CHtml::link('Salir de aquí', array('/'), array('class'=>'btn btn-danger')); ?>
                    </div>

                    <?php echo CHtml::endForm();?>
                </div>
            </div>
        </div>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Lista de alumnos que llevarán el Servicio Social, es decir, que fueron
		aceptados en algún programa. Una vez que asignas la fecha de inicio y el tipo se Servicio Social (semestral, anual o unica vez)
		ya NO SE PODRÁ modificar el tipo de servicio social ni la fecha.
    </strong></p>
</div>


<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Una vez CANCELADO el Servicio Social ACTUAL del Alumno no se podrá revocar esta decisión.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'servicio-social-grid',
	'dataProvider'=>$modelSSServicioSocial->searchXProgramasServicioSocial($id_programa),
	'filter'=>$modelSSServicioSocial,
	'columns'=>array(
		//'id_servicio_social',
		array(
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
            'name' => 'Nombre del Alumno',//
            'value' => function($data)
            {
                $query =
                " SELECT \"nmbAlumno\"  from public.\"E_datosAlumno\" WHERE \"nctrAlumno\"='$data->no_ctrl'
                ";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return (count($result) > 0) ? $result[0]['nmbAlumno'] : "NO ESPECIFICADO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'230px', 'class'=>'text-center')
        ),
		array(
			'name' => 'idPrograma.horas_totales',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->idPrograma->horas_totales.'</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'name' => 'idPrograma.fecha_inicio_programa',
			/*'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_actualizacion = InfoServicioSocial::getFechaInicioServicioSocial($data->id_servicio_social);
				return $fecha_actualizacion[0]['fecha_act'];
			},*/
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'name' => 'idPrograma.fecha_fin_programa',
			/*'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_actualizacion = InfoServicioSocial::getFechaFinServicioSocial($data->id_servicio_social);
				return $fecha_actualizacion[0]['fecha_act'];
			},*/
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'name' => 'idEstadoServicioSocial.estado',
			//'filter' => false,
			'filter' => CHtml::activeDropDownList($modelSSServicioSocial,
												 'id_estado_servicio_social',
												 CHtml::listData(
													SsEstadoServicioSocial::model()->findAll(),
													'id_estado_servicio_social',
													'estado'
												 ),
												 array('prompt'=>'--Filtrar por --')
												),
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'name' => 'idPrograma.nombre_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		/*'status_servicio_social',
		'fecha_registro',
		'fecha_modificacion',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detalleServicioSocialAlumno}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detalleServicioSocialAlumno' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/detalleAdminServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{fecServicioSocialAlumno},{fecServicioSocialAsig}',
			'header'=>'Duración',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'fecServicioSocialAlumno' => array
				(
					'label'=>'Asignar Fecha Inicio Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/fechasServicioSocialAlumno", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/fecha_servicio_32.png',
					'visible' => '$data->id_estado_servicio_social == 1 AND $data->id_estado_servicio_social != 7' //Solo se pueda agregar el horario cuando el alumno fue aceptado en el programa
				),
				'fecServicioSocialAsig' => array
				(
					'label'=>'Fechas Servicio Social Asignada',
					'url'=>'#',
					'imageUrl'=>'images/fecha_asignada_32.png',
					'visible' => '$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social != 7' //Solo se pueda agregar el horario cuando el alumno fue aceptado en el programa
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statServicioSocialAlumno}',
			'header'=>'Status',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statServicioSocialAlumno' => array
				(
					'label'=>'Status Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/statusServicioSocialAlumno", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/menu_status_servsocial_32.png',
				),
			),
		),*/
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{altaServicioSocialAlumnoC}, {cancelServicioSocialAlumno}', // buttons here...
			'header' 			   => 'Cancelar Servicio Social',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'altaServicioSocialAlumnoC' => [
					'label'   => 'Estatus ALTA Servicio Social',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssServicioSocial/cancelarServicioSocialActual", array("id_servicio_social"=>$data->id_servicio_social))', // ?r=controller/approve/id/123
					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
					'visible'=> '$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social < 6', // <-- SHOW IF ROW ACTIVE
					'options' => [
						'title'        => 'CANCELAR el Servicio Social ACTUAL',
						'data-confirm' => 'Confirmar para CANCELAR el Servicio Social ACTUAL.',
					],
					'click'   => $valSSocialJs, //Cancelar el Servicio Social ACTUAL
				],
				'cancelServicioSocialAlumno' => [
					'label'   => 'Servicio Social CANCELADO',
					'imageUrl' => 'images/servicio_social/cancelado_32.png',
					'visible' => '$data->id_estado_servicio_social == 7', // <-- SHOW IF ROW INACTIVE
				],
			],
		],//Fin
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{altaServicioSocialAlumnoB}, {bajaServicioSocialAlumno}', // buttons here...
			'header' 			   => 'Baja Servicio Social',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'altaServicioSocialAlumnoB' => [
					'label'   => 'Estatus ALTA Servicio Social',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssServicioSocial/bajaServicioSocialActual", array("id_servicio_social"=>$data->id_servicio_social))', // ?r=controller/approve/id/123
					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
					'visible'=> '$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social < 6', // <-- SHOW IF ROW ACTIVE
					'options' => [
						'title'        => 'Baja del Servicio Social ACTUAL',
						'data-confirm' => 'Confirmar Baja del Servicio Social ACTUAL.',
					],
					'click'   => $bajSSocialJs, //Dar Baja el Servicio Social ACTUAL
				],
				'bajaServicioSocialAlumno' => [
					'label'   => 'Servicio Social Dado de Baja',
					'imageUrl' => 'images/servicio_social/cancelado_32.png',
					'visible' => '$data->id_estado_servicio_social == 8', //Servicio Social Dado de baja
				],
			],
		],//Fin
	),
)); ?>
