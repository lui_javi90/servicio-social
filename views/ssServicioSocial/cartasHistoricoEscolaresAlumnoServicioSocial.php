<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Alumnos Completaron Servicio Social ' => array('ssHistoricoTotalesAlumnos/listaHorasTotalesAlumnosServicioSocial'),
    'Historial Servicio Social Alumno' => array('ssHistoricoTotalesAlumnos/historialHorasTotalesAlumnosServicioSocial', 'no_ctrl'=>$no_ctrl),
    'Cartas Histórico Servicio Social'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Cartas Histórico de Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Cartas de Aceptación, Presentación y Carta de Terminación de los Servicio Sociales que completaste. Así como la Solicitud de Servicio Social.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'hist-cartalum-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXServicioSocial($id_servicio_social),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*'id_servicio_social',
        'horas_realizadas',
        'fecha_inicio',
        'fecha_fin',
        'no_ctrl',
        'status_servicio_social',
        'id_estado_servicio_social',
        'fecha_registro',
        'fecha_modificacion',
        'id_periodo_servicio_social',
        'año_servicio_social',
        'id_periodo_escolar',
        'id_tipo_servicio_social',*/
        array(
            'name' => 'idPrograma.nombre_programa', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'320px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Tipo Servicio Social', //id_tipo_servicio_social
            'value' => function($data)
            {
                return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'name' => 'calificacion_servicio_social', //id_programa_servicio_social
            'filter' => false,
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_servicio_social.'</span>';
			},
            'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{solServicioSocialAlumno}',
			'header'=>'Solicitud al Programa',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'solServicioSocialAlumno' => array
				(
					'label'=>'Solicitud a Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/imprimirSolicitudServicioSocial", array("id_solicitud_programa"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
		),
      array(
			'class'=>'CButtonColumn',
			'template'=>'{impCartaAceptacion}',
			'header'=>'Carta Aceptación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impCartaAceptacion' => array
				(
					'label'=>'Imprimir Carta de Aceptación',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/imprimirCartaAceptacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
          'visible' => '$data->id_estado_servicio_social == 6'
				),
			),
		),
      array(
			'class'=>'CButtonColumn',
			'template'=>'{viewCartaPresentacion}',
			'header'=>'Carta Presentación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'viewCartaPresentacion' => array
				(
					'label'=>'Carta Presentanción',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/imprimirCartaPresentacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
        ),
      array(
			'class'=>'CButtonColumn',
			'template'=>'{viewCartaTerminacion}',
			'header'=>'Carta Terminación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'viewCartaTerminacion' => array
				(
					'label'=>'Imprimir Carta Terminación',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/imprimirCartaTerminacion", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
          'visible' => '$data->id_estado_servicio_social == 6'
				),
			),
		),
    array(
    'class'=>'CButtonColumn',
    'template'=>'{impPlanTrabajo}',
    'header'=>'Actividades',
    'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
    'buttons'=>array
    (
      'impPlanTrabajo' => array
      (
        'label'=>'Imprimir Plan de Trabajo',
        'url'=>'Yii::app()->createUrl("serviciosocial/ssActividadesServicioSocial/imprimirPlanDeTrabajoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
        'imageUrl'=>'images/servicio_social/printer.png',
        'visible' =>'$data->id_estado_servicio_social == 6'
      ),
    ),
  ),
  ),
)); ?>