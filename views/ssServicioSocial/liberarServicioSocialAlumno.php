<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Candidatos a Liberar Servicio Social' => array('ssServicioSocial/listaCandidatosALiberarServicioSocial'),
    'Liberar Servicio Social'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Información Servicio Social
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Información Servicio Social del Alumno</h6>
			</div>
			<div class="panel-body">
                <div style="padding-top:10px" class="col-xs-3" align="center">
                <!--Es un Web Service-->
                    <img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="200">
                <!--Es un Web Service-->
                </div>
				<div class="col-xs-9" align="left">
                <p><b>Nombre Alumno:</b>
                &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                <p><b>Carrera:</b>
                &nbsp;&nbsp;<?php echo $modelEEspecialidad->dscEspecialidad; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('no_ctrl')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelSSServicioSocial->no_ctrl;?></p>

                <p><b>Horas a Completar:</b>
                &nbsp;&nbsp;<?php echo '<span style="font-size:18px" class="label label-success">'.$modelSSProgramas->horas_totales.' horas</span>';?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>
                &nbsp;&nbsp;<?php echo ($modelSSProgramas->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('id_estado_servicio_social')); ?>:</b>
                &nbsp;&nbsp;<?php echo $estado_servicio_social; ?></p>

                <p><b>Calificación Final de Servicio Social:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->calificacion_servicio_social >= 70) ?
                                '<span style="font-size:18px" class="label label-success">'.$modelSSServicioSocial->calificacion_servicio_social.'</span></b>' :
                                '<span style="font-size:18px" class="label label-danger">'.$modelSSServicioSocial->calificacion_servicio_social.'</span></b>'; ?>
        </div>
			</div>
		</div>
	</div>
</div>


<!--Informacion del Servicio Social del Alumno-->
<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Reportes Bimestrales
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'hist-lib-reportes-bimestral-grid',
      'dataProvider'=>$modelSSReportesBimestral->searchXReportesBimestralesServSocialActual($id_servicio_social),
      'filter'=>$modelSSReportesBimestral,
      'columns'=>array(
            /*array(
                'name' => 'id_reporte_bimestral',
                'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
            ),
            array(
                'name' => 'id_servicio_social',
                'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
            ),*/
            array(
                'name' => 'bimestres_correspondiente',
                'filter' => false,
                'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
            ),
            array(
                'header' => 'Bimestre Final', //bimestre_final
                'value' => function($data)
                {
                    return ($data->bimestre_final == true) ? "SI" : "NO";
                },
                'filter' => false,
                'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
            ),
            /*'valida_responsable',
            'valida_oficina_servicio_social',
            'observaciones_reporte_bimestral',
            'fecha_inicio_rep_bim',
            'fecha_fin_rep_bim',
            'envio_alum_evaluacion',*/
			array(
				'name' => 'observaciones_reporte_bimestral',
				'filter' => false,
				'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
			),
            array(
                'name' => 'calificacion_reporte_bimestral',
                'filter' => false,
                'type' => 'raw',
                'value' => function($data)
                {
                    return ($data->calificacion_reporte_bimestral >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_reporte_bimestral.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_reporte_bimestral.'</span>';
                },
                'htmlOptions' => array('width'=>'40px', 'class'=>'text-center')
            ),
            array(
                'class'=>'CButtonColumn',
                'template'=>'{impReporteBimestral}',
                'header'=>'Detalle',
                'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
                'buttons'=>array
                (
                    'impReporteBimestral' => array
                    (
                        'label'=>'Ver Reporte Bimestral',
                        'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/ImprimirAReporteBimestral",
                        array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',
                        'imageUrl'=>'images/servicio_social/printer.png',
                    ),
                ),
            ),
        ),
    )); ?>
<!--Informacion del Servicio Social del Alumno-->

<!--Vista para liberar el Servicio Social del Alumno-->
<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Liberar Servicio Social
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Una vez cambiado el estado del Servicio Social a FINALIZADO se agregara la calificación final del Servicio Social y no se podrá modificar mas.
    </strong></p>
</div>


<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Servicio Social</h6>
			</div>
			<div class="panel-body">
      <?php

          $this->renderPartial('_formLiberarServicioSocial', array('modelSSServicioSocial'=>$modelSSServicioSocial,
 																	'lista_estados' => $lista_estados,
 																	'edo_actual_servicio' => $edo_actual_servicio

                                                              ));?>

			</div>
		</div>
	</div>
</div>
<!--Vista para liberar el Servicio Social del Alumno-->

<br><br><br>
<div align="center">
    <?php //echo CHtml::link('Volver a Lista de Candidatos a Liberar Servicio Social', array('listaCandidatosALiberarServicioSocial'), array('class'=>'btn btn-success')); ?>
</div>
<br><br>
