<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Candidatos a Liberar Servicio Social' => array('ssServicioSocial/listaCandidatosALiberarServicioSocial'),
    'Detalle Servicio Social'
);?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Servicio Social
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Información Servicio Social del Alumno</h6>
			</div>
			<div class="panel-body">
        <div style="padding-top:10px" class="col-xs-3" align="center">
					<!--Es un Web Service-->
          	<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="200">
          <!--Es un Web Service-->
        </div>
				<div class="col-xs-9" align="left">
                <p><b>Nombre Alumno:</b>
                &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                <p><b>Carrera:</b>
                &nbsp;&nbsp;<?php echo $modelEEspecialidad->dscEspecialidad; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('no_ctrl')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelSSServicioSocial->no_ctrl;?></p>

                <p><b>Horas a Completar:</b>
                &nbsp;&nbsp;<?php echo '<span style="font-size:18px" class="label label-success">'.$modelSSProgramas->horas_totales.' horas</span>'; ?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>
                &nbsp;&nbsp;<?php echo ($modelSSProgramas->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('id_estado_servicio_social')); ?>:</b>
                &nbsp;&nbsp;<?php echo $estado_servicio_social; ?></p>

                <p><b>Calificación Final de Servicio Social:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->calificacion_servicio_social >=70) ?
                                '<span style="font-size:18px" class="label label-success">'.$modelSSServicioSocial->calificacion_servicio_social.'</span></b>' :
                                '<span style="font-size:18px" class="label label-danger">'.$modelSSServicioSocial->calificacion_servicio_social.'</span></b>'; ?>
        </div>
			</div>
		</div>
	</div>
</div>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle del Programa
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$modelSSProgramas,
    'attributes'=>array(
    //'id_programa_servicio_social',
    'nombre_programa',
    array(
		'header'=>'Empresa',
		'name' => 'idUnidadReceptora.nombre_unidad_receptora'
	),
    'lugar_realizacion_programa',
    /*array(
		'name'=>'Departamento',
		'value' => function($data)
			{
					$id = $data->rfcSupervisor0->cveDepartamento;
					$query = "select \"dscDepartamento\" from \"H_departamentos\" where \"cveDepartamento\" = '$id' ";
					$nombre = Yii::app()->db->createCommand($query)->queryAll();

					return $nombre[0]['dscDepartamento'];
			},
    ),*/
    'horas_totales',
    'idPeriodoPrograma.periodo_programa',//id_periodo_programa
    /*array(
    'name' => 'rfcSupervisor',
    'value' => function($data)
	    {
	          $rfc_sup = $data->rfcSupervisor;
	          $query =
	          "
	          SELECT (nombre_supervisor||' '|| apell_paterno || ' ' || apell_materno) as name
	          from ss_supervisores_programas
	          where \"rfcSupervisor\" = '$rfc_sup' ";
	          $name = Yii::app()->db->createCommand($query)->queryAll();

	          return (count($name) > 0) ? $name[0]['name'] : "NO ESPECIFICADO";
	    }
    ),*/
    'numero_estudiantes_solicitados',
    'descripcion_objetivo_programa',
    'idTipoServicioSocial.tipo_servicio_social',//id_tipo_servicio_social
    'impacto_social_esperado',
    'beneficiarios_programa',
    'actividades_especificas_realizar',
    'mecanismos_supervision',
    'perfil_estudiante_requerido',
    'idApoyoEconomicoPrestador.descripcion_apoyo_economico',//id_apoyo_economico_prestador_servicio_social
    'idTipoApoyoEconomico.tipo_apoyo_economico',//id_tipo_apoyo_economico
    'apoyo_economico',
    //'fecha_registro_programa_servicio_social',
    //'fecha_modificacion_programa_servicio_social',
    //'id_recibe_capacitacion',
    array(
        'name' => 'id_recibe_capacitacion',
        'oneRow'=>true,
        'value' => function($data)
        {
                return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO";
        },
    ),
    'idClasificacionAreaServicioSocial.clasificacion_area_servicio_social',//id_clasificacion_area_servicio_social
    ),
)); ?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>

<br><br>
<?php
for($i=0; $i < count($modelSSHorarioDiasHabilesProgramas); $i++){
$this->widget('serviciosocial.components.DetailView4Col', array(
    'data'=>$modelSSHorarioDiasHabilesProgramas[$i],
    'attributes'=>array(
        //'id_horario',
        //'id_programa_servicio_social', id_dia_semana
        array(
            'name' => 'idDiaSemana.dia_semana',
            'oneRow'=>true,
        ),
        'hora_inicio',
        'hora_fin',
        //'horas_totales',
    ),
));
echo "<br><br>";
}
?>


<br><br>
<div align="center">
    <?php //echo CHtml::link('Entendido', array('listaCandidatosALiberarServicioSocial'), array('class'=>'btn btn-success')); ?>
</div>
<br><br>
