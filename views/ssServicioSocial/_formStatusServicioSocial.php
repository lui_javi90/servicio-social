<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'status-servicio-social-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'htmlOptions'=> array('autocomplete'=>'off'),
    'enableAjaxValidation'=>false,
)); ?>

    <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

    <?php echo $form->errorSummary($modelSSServicioSocial); ?>

    <div class="form-group">
        <?php echo $form->labelEx($modelSSServicioSocial,'id_estado_servicio_social'); ?>
        <?php echo $form->dropDownList($modelSSServicioSocial,
                                    'id_estado_servicio_social',
                                    $lista_edos_servicio,
                                    array('class'=>'form-control', 'required'=>'required')
                                    ); ?>
        <?php echo $form->error($modelSSServicioSocial,'id_estado_servicio_social'); ?>
    </div>

    <br>
    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSServicioSocial->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar',array('listaAdminServicioSocial'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
