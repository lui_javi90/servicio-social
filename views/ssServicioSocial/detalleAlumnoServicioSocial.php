<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Lista Servicio Social' => array('ssServicioSocial/listaAlumnoServicioSocial'),
    'Detalle Servicio Social'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Servicio Social
		</span>
	</h2>
</div>

<br><br>
<div class="row"><!--Row Principal-->
	<div class="col-xs-12">
		<div class="panel panel-primary">
                <div class="panel-heading">
                    <h6 class="panel-title">Información Servicio Social</h6>
                </div>
                <div class="panel-body">
                    <div class="col-lg-12" align="left">
                        <p><b>Nombre del Alumno:</b>
                        &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                        <p><b>Carrera:</b>
                        &nbsp;&nbsp;<?php echo $carrera; ?></p>

                        <p><b>Semestre:</b>
                        &nbsp;&nbsp;<?php echo $semestre; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('no_ctrl')); ?>:</b>
                        &nbsp;&nbsp;<?php echo $modelSSServicioSocial->no_ctrl;?></p>

                        <p><b>Horas a Liberar de Servicio Social:</b>
                        &nbsp;&nbsp;<?php echo '<span style="font-size:18px" class="label label-success">'.$modelSSProgramas->horas_totales.' horas</span>'; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('id_estado_servicio_social')); ?>:</b>
                        &nbsp;&nbsp;<?php echo $estado_servicio_social; ?></p>

                        <p><b>Calificación del Servicio Social:</b>
                        &nbsp;&nbsp;<?php echo ($modelSSServicioSocial->calificacion_servicio_social >=70) ?
                                        '<span style="font-size:18px" class="label label-success">'.$modelSSServicioSocial->calificacion_servicio_social.'</span></b>' :
                                        '<span style="font-size:18px" class="label label-danger">'.$modelSSServicioSocial->calificacion_servicio_social.'</span></b>'; ?>
                    </div>
			    </div>
		</div>
	</div>
</div><!--Row Principal-->

<!--Detalle del Supervisor (ds) del Programa-->
<!--AQUi va el detalle del Supervisor del Programa-->
<!--Detalle del Supervisor (ds) del Programa-->

<div class="row"><!--Row Principal-->
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle del Programa
		</span>
	</h2>
</div><!--Row Principal-->

<!--Detalle del Programa-->
<br>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$modelSSProgramas,
    'attributes'=>array(
        //'id_programa_servicio_social',
        'nombre_programa',
        array(
            'header' => 'Empresa',
            'name'=>'idUnidadReceptora.nombre_unidad_receptora',
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
        'lugar_realizacion_programa',
        'horas_totales',
        'idPeriodoPrograma.periodo_programa',//id_periodo_programa
        'numero_estudiantes_solicitados',
        'descripcion_objetivo_programa',
        'idTipoServicioSocial.tipo_servicio_social',//id_tipo_servicio_social
        'impacto_social_esperado',
        'beneficiarios_programa',
        'actividades_especificas_realizar',
        'mecanismos_supervision',
        'perfil_estudiante_requerido',
        'idApoyoEconomicoPrestador.descripcion_apoyo_economico',//id_apoyo_economico_prestador_servicio_social
        'idTipoApoyoEconomico.tipo_apoyo_economico',//id_tipo_apoyo_economico
        'apoyo_economico',
        //'fecha_registro_programa_servicio_social',
        //'fecha_modificacion_programa_servicio_social',
        //'id_recibe_capacitacion',
        array(
            'name' => 'id_recibe_capacitacion',
            'oneRow'=>true,
            'value' => function($data)
            {
                return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO";
            },
        ),
        'idClasificacionAreaServicioSocial.clasificacion_area_servicio_social',//id_clasificacion_area_servicio_social
    ),
)); ?>
<!--Detalle del Programa-->



<br>
<div align="center">
    <?php //echo CHtml::link('Volver al menú de Servicio Social', array('listaAlumnoServicioSocial'), array('class'=>'btn btn-success')); ?>
</div>
<br><br>


<br><br><br><br><br>
<br><br><br><br><br>
