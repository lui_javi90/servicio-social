<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Observaciones Supervisor Servicio Social',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Observaciones Supervisor Servicio Social
		</span>
	</h2>
</div>



<br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'observ-sup-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXObservacionesPrograma($rfcSupervisor, $id_tipo_programa, $id_programa),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*array(
            'name' => 'id_servicio_social',
            'filter' => false,
            'htmlOptions' => array('width'=>'10px', 'class'=>'text-center')
        ),*/
        array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return (empty($data->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->no_ctrl, '', array('class'=>'img-circle','style' =>"width:80px;height:80px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPrograma.nombre_programa',
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idEstadoServicioSocial.estado',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{observServicioSocialAlumno}',
			'header'=>'Ver Conversación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'observServicioSocialAlumno' => array
				(
					'label'=>'Nueva Observación',
                    'url'=>'Yii::app()->createUrl("serviciosocial/ssObservacionesServicioSocial/nuevaObservacionSupervisorServicioSocial", array("id_servicio_social"=>$data->id_servicio_social, 
                                                                                                                                                 "no_ctrl" => $data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
				),
			),
        ),
        array(
			'class' => 'ComponentObservacionesXAlumno',
			'header' => 'Observaciones Pendientes',
			'htmlOptions' => array('width' => '80px', 'class' => 'text-center'),
		),
    ),
)); ?>