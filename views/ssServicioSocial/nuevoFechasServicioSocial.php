<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

if($modelSSServicioSocial->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Lista Servicio Social' => array('servicioSocial/listaAdminServicioSocial'),
		'Duración Servicio Social'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Lista Servicio Social' => array('servicioSocial/listaAdminServicioSocial'),
		'Editar Duración Servicio Social'
	);
}
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSServicioSocial->isNewRecord) ? "Duración Servicio Social" : "Editar Duración Servicio Social"; ?>
		</span>
	</h2>
</div>

<br>
<div clas="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
                    <?php echo ($modelSSServicioSocial->isNewRecord) ? "Duración Servicio Social" : "Editar Duración Servicio Social"; ?> 
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formFechasServicioSocial', array(
											'modelSSServicioSocial'=>$modelSSServicioSocial,
											'lista_peridos' => $lista_peridos
											)); ?>
			</div>
		</div>
	</div>
</div>