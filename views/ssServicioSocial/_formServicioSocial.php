<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions'=> array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSServicioSocial); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'horas_realizadas'); ?>
		<?php echo $form->textField($modelSSServicioSocial,'horas_realizadas',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'readOnly'=>true)); ?>
		<?php echo $form->error($modelSSServicioSocial,'horas_realizadas'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'fecha_inicio'); ?>
		<?php echo $form->textField($modelSSServicioSocial,'fecha_inicio'); ?>
		<?php echo $form->error($modelSSServicioSocial,'fecha_inicio'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'fecha_fin'); ?>
		<?php echo $form->textField($modelSSServicioSocial,'fecha_fin'); ?>
		<?php echo $form->error($modelSSServicioSocial,'fecha_fin'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'no_ctrl'); ?>
		<?php echo $form->textField($modelSSServicioSocial,'no_ctrl',array('size'=>8,'maxlength'=>8, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelSSServicioSocial,'no_ctrl'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'status_servicio_social'); ?>
		<?php echo $form->checkBox($modelSSServicioSocial,'status_servicio_social'); ?>
		<?php echo $form->error($modelSSServicioSocial,'status_servicio_social'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'id_estado_servicio_social'); ?>
		<?php echo $form->dropDownList($modelSSServicioSocial,
									  'id_estado_servicio_social',
									  $lista_estados_servicio,
									  array('prompt'=>'--Estado Servicio Social--', 'class'=>'form-control', 'required'=>'required'
									  )); ?>
		<?php echo $form->error($modelSSServicioSocial,'id_estado_servicio_social'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServicioSocial,'id_programa_servicio_social'); ?>
		<?php echo $form->dropDownList($modelSSServicioSocial,
									'id_programa_servicio_social',
									$lista_programas,
									array('prompt'=>'--Selecciona Programa--', 'class'=>'form-control', 'required'=>'required'
									));?>
		<?php echo $form->error($modelSSServicioSocial,'id_programa_servicio_social'); ?>
	</div>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSServicioSocial->isNewRecord ? 'Guardar' : 'Guardar', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaServicioSocial'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->