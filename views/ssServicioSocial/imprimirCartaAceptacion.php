<style>
	.hoja{
        background-color: transparent;
    }
    .div1{
        background-color: transparent;
        font-weight: bold;
    }
    .div2{
        background-color: transparent;
    }
    table, th, td{
        padding: 0px;
        padding-left: 0px;
        padding-bottom: 0px;
        padding-right: 0px;
        text-align: left;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 11px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado2{
        /*letter-spacing: 1pt;       para separar entre letras */
        word-spacing: 1.5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado3{
        line-height: 15pt;
    }
    #contenedor{
      margin-left:0px;
      margin-top:0px;
      padding:0 0 0 0;
      width:auto;
      /*background-color: #2196F3;*/
      position:absolute;
  }
  .divcontenido1{

      float:left;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
  }
  .divcontenido2{

      float:right;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
      margin-left:0px;
  }
</style>

<div class="hoja">

    <!--<img src="https://i.imgur.com/g05TF48.jpg"/>-->
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />
    <?php //echo '<img src="images/encabezados_empresas/'.trim($datos_carta_aceptacion[0]['path_carpeta']).'/banner_superior_'.trim($datos_carta_aceptacion[0]['no_empresa']).'/'.trim($datos_carta_aceptacion[0]['banner_superior']).'"/>'; ?>
    <br><br>
    <h5 style="font-style: italic;" class="center letra4"><span class="bold">"
                                            <?php echo ($leyenda_reporte_pdf != NULL) ? $anio.", ".$leyenda_reporte_pdf : ""; ?>
                                        "</span></h5>
    <br>
    <!--fecha, oficio y asunto-->
    <div class="div2 interlineado3">
        <table style="width:50%;" class="interlineado3 letra3" align="right">
            <tr class="interlineado3">
                <td align="right"><?php echo "CELAYA, GTO.,&nbsp;<span style='background-color:black;color:white;' class='bold'>".$fec_reporte."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "NO. DE OFICIO:&nbsp;<span class='bold'>".$datos_carta_aceptacion[0]['folio']."/".$datos_carta_aceptacion[0]['anio_servicio_social']."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "ASUNTO:&nbsp;<span class='bold letra4'>ACEPTACIÓN DE SERVICIO SOCIAL</span>"; ?></td>
            </tr>
        </table>
    </div>
    <!--fecha, oficio y asunto-->

    <!--Datos del supervisor del programa-->
    <br><br><br>
    <div class="div2 interlineado1">
        <h6 class="bold letra3 interlineado1"><?php echo $datos_carta_aceptacion[0]['grado_max_estudios_director']." ".$datos_carta_aceptacion[0]['director']; ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo $datos_carta_aceptacion[0]['institucion']; ?></h6>
        <h6 class="bold letra3 interlineado1">DIRECTOR</h6>
        <!--<h6 class="bold letra3 interlineado1">PRESENTE</h6>-->
    </div>
    <!--Datos del supervisor del programa-->

    <!--Datos del jefe del depto. de vinculacion-->
    <br>
    <div class="div2 interlineado1 right">
    	<h6 class="bold letra3 interlineado1"><?php echo "AT´N: ".$datos_carta_aceptacion[0]['grado_max_estudios_jefvinculacion']." ".$datos_carta_aceptacion[0]['jefe_vinculacion']; ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo "Departamento de Gestión Tecnológica y Vinculación"; ?></h6>
    </div>
    <!--Datos del jefe del depto. de vinculacion-->

    <!--CUERPO DEL REPORTE-->
    <br><br>
    <div class="interlineado2 div2 letra3">
        <p ALIGN="justify">
            Por medio de la presente me permito informarle que el (a) <?php echo "<span class='bold'>"."C. ".$datos_carta_aceptacion[0]['nombre_alumno']."</span>";?>, estudiante de la carrera de <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['carrera_alumno']."</span>"; ?>, 
            con número de control <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['no_ctrl']."</span>"; ?>, fue aceptado (a) para realizar su <?php echo "<span class='bold'>Servicio Social</span>"; ?> en el Programa <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['nombre_programa']."</span>"; ?>, 
            donde cubrirá un total de <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['horas_totales']." horas"."</span>"; ?> a partir del dia <?php echo "<span class='bold'>".$fecha_inicio."</span>"; ?> al <?php echo "<span class='bold'>".$fecha_fin."</span>"; ?>, laborando un total de <?php echo "<span class='bold'>".$horas_diarias."</span>"; ?> 
            horas diarias, en un lapso mínimo de <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['periodo_programa']."</span>"; ?> meses, no excediéndose de dos años.
            <br><br>
            Sin otro particular por el momento, aprovecho la ocasión para enviarle un cordial saludo.
        </p>
    </div>
    <!--CUERPO DEL REPORTE-->

    <!--Firma del encargado-->
    <br>
    <div class="interlineado1 div2 left">
        <h6 class="interlineado1 bold letra3">A T E N T A M E N T E</h6>
        <i class="interlineado1 letra3">LA TÉCNICA POR UN MÉXICO MEJOR &reg;</i>
    </div>

    <div id="contenedor">
        <div class="divcontenido1 left">
            <!--Si la firma no viene entonces deje el espacio en blanco-->
            <br>
            <?php
            if($datos_carta_aceptacion[0]['validacion_jefesup'] == NULL || $datos_carta_aceptacion[0]['estatus_validacion_validacion_jefesup'] == NULL){
                echo "<br><br><br><br>";
            }else{
            ?>
                <!--<img height="90" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/catalina.jpg" />-->
                <?php 
                    //$rfc_jefe_vinc = "LUMC781102KT1";
                    //echo "<img height='50' src="."items/getFirma.php?nctr_rfc=".$rfc_jefe_vinc.">";
                ?>

                <?php
                    echo "<br>";
                    echo "El Jefe de Departamento valido la solicitud del Alumno el dia <br>"."<span class='bold'>".$fecha_val_sol[0]['fecha_act']." a las ".$fecha_val_sol[0]['hora']."</span>";
                    //echo "<br>";
                ?>
            <?php } ?>
            <h6 class="interlineado1 bold letra3"><?php echo $datos_carta_aceptacion[0]['nombre_jefe_depto']; ?></h6>
            <?php echo "<span class='bold'>______________________________________________</span>"; ?>
            <h6 class="interlineado1 bold letra2"><?php echo "JEFE DEL ".$datos_carta_aceptacion[0]['depto_supjefe']; ?></h6>
        </div>
        <div class="divcontenido2 center">
            <!--<img height="150" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/sello_sep.jpg" />-->
        </div>
    </div>
    <!--Firma del encargado-->

    <!--Datos del archivo-->
    <br>
    <div class="div2 left">
        <h6 class="interlineado1 letra2"><?php echo "C.c.p.-Archivo"; ?></h6>
        <h6 class="interlineado1 letra2"><?php echo "AJRG/iov*"; ?></h6>
    </div>
    <!--Datos del archivo-->
    <!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_inf.jpg" />-->

</div>
