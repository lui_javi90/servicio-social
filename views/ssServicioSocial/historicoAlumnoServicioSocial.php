<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Histórico Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Histórico Servicio Social
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Información del Alumno</h6>
			</div>
			<div class="panel-body">
                <div style="padding-top:30px" class="col-xs-3" align="center">
                    <?php echo (empty($no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$no_ctrl, '', array('class'=>'img-circle','style' =>"width:150px;height:150px;")); ?>
                </div>
				<div class="col-xs-6" align="left">
                    <p><b>Nombre del Alumno:</b>
                    &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                    <p><b>Carrera:</b>
                    &nbsp;&nbsp;<?php echo $carrera; ?></p>

                    <p><b>Semestre:</b>
                    &nbsp;&nbsp;<?php echo $semestre;?></p>

                    <p><b>No. de Control:</b>
                    &nbsp;&nbsp;<?php echo $no_ctrl;?></p>

                    <p><b>Estatus del Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSHistoricoTotalesAlumnos->completo_servicio_social == false) ? "INCOMPLETO" : "COMPLETO";
                                    echo "<b>"." (".($horas_servicio_social - $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social)." hrs. faltantes".")"."</b>";
                                ?></p>

                    <p><b>Total de Horas Completadas de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social == $horas_servicio_social) ?
                                    '<span style="font-size:18px" class="label label-success">'.$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social.'</span></b>' :
                                    '<span style="font-size:18px" class="label label-danger">'.$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social.'</span></b>'; ?>

                    <?php //if($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social != 0){ ?>
                    <p><b>Calificación Final de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social >= 70) ?
                                    '<span style="font-size:18px" class="label label-success">'.$modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social.'</span></b>' :
                                    '<span style="font-size:18px" class="label label-danger">'.$modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social.'</span></b>'; ?>
                    <?php //} ?>
                </div>

                <div style="padding-top:20px" class="col-xs-3" align="center">

                    <?php
                    /*Agregar la pila, cuando este llena aparecera la calificacion*/
                    if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= 480)
                    {
                        if($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social >= 70 AND $modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social < 80)
                        {
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/bronce.png"/>';

                        }else if($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social >= 80 AND $modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social < 90)
                        {
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/plata.png"/>';

                        }else if($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social >= 90 AND $modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social <= 100){
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/oro.png"/>';

                        }else if($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social < 70){

                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/reprobados.png"/>';
                        }
                    }else{

                        /*Agregar la pila, cuando este llena aparecera la calificacion*/
                        if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social == 0){
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/empty.png"/>';
                        }else
                        if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social > 0 AND $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social < ($horas_servicio_social / 4)){
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/middle_empty.png"/>';
                        }else
                        if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= ($horas_servicio_social / 4) AND $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social < ($horas_servicio_social / 2)){
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/middle.png"/>';
                        }else
                        if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= ($horas_servicio_social / 2) AND $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social < $horas_servicio_social){
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/full_middle.png"/>';
                        }else
                        if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= $horas_servicio_social){
                        echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/full.png"/>';
                        }
                    }?>

                </div>
			</div><!--End Body-->
		</div>
	</div>
</div>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Registro Histórico de Servicio Social
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		    Únicamente aparecerán los registros de los Servicio Sociales que completaste.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'hist-alum-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXHistoricoAlumno($no_ctrl),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        /*array(
            'name' => 'id_servicio_social', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'10px')
        ),*/
        array(
            'name' => 'idPrograma.nombre_programa', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Horas Liberadas',
            'name' => 'idPrograma.horas_totales', //horas_realizadas
            'filter' => false,
            'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->idPrograma->horas_totales.'</span>';
			},
            'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
        ),
        /*array(
            'name' => 'fecha_inicio', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_fin', //id_programa_servicio_social
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Periodo', //id_periodo_escolar
            'value' => function($data)
            {
                return ($data->id_periodo_escolar == 1) ? "ENERO-JULIO" : "AGOSTO-DICIEMBRE";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
        ),
        array(
            'name' => 'año_servicio_social', //id_programa_servicio_social
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),*/
        array(
            'header' => 'Tipo Servicio Social', //id_tipo_servicio_social
            'value' => function($data)
            {
                return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
  			'name' => 'idEstadoServicioSocial.estado',
  			'filter' => false,
  			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
  		  ),
        array(
            'header' => 'Calificación Servicio Social',
            //'name' => 'calificacion_servicio_social', //id_programa_servicio_social
            'filter' => false,
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_servicio_social.'</span>';
			},
            'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Periodo',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                $id = $data->id_programa;

                $qry = "select rf.id_periodo as periodo from pe_planeacion.ss_programas p
                        join pe_planeacion.ss_registro_fechas_servicio_social rf
                        on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                        where p.id_programa = '$id' ";

                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                $per = ($rs[0]['periodo'] == 1) ? "ENERO-JUNIO" : "AGOSTO-DICIEMBRE";

                return '<span style="font-size:14px" class="label label-info">'.$per.'</span>';
            },
            'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Año ',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                $id = $data->id_programa;

                $qry = "select rf.anio from pe_planeacion.ss_programas p
                        join pe_planeacion.ss_registro_fechas_servicio_social rf
                        on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                        where p.id_programa = '$id' ";

                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                return '<span style="font-size:14px" class="label label-info">'.$rs[0]['anio'].'</span>';
            },
            'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
        ),
        /*'status_servicio_social',
        'id_estado_servicio_social',
        'no_ctrl',
        'fecha_registro',
        'fecha_modificacion',
        */
        array(
			'class'=>'CButtonColumn',
			'template'=>'{detHistServicioSocialAlumno}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detHistServicioSocialAlumno' => array
				(
					'label'=>'Detalle Histórico',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/detalleHistoricoAlumnoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{repBimHistServicioSocialAlumno}',
			'header'=>'Reportes Bimestrales',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'repBimHistServicioSocialAlumno' => array
				(
					'label'=>'Reportes Bimestrales',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/repBimHistoricoAlumnoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/historial_docs_servsocial_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{cartHistServicioSocialAlumno}',
			'header'=>'Cartas',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'cartHistServicioSocialAlumno' => array
				(
					'label'=>'Cartas Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/cartasHistoricoAlumnoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/cartas_servsocial_32.png',
				),
			),
       ),
    ),
)); ?>

<?php if($hayRegServicioSocialPasados == true){ ?>
<br><br><br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Registro Histórico de Servicios Sociales Anteriores
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		    Servicios Sociales registrados anteriormente.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-servicios-sociales-anteriores-grid',
    'dataProvider'=>$modelSSServiciosSocialesAnteriores->searchXAlumno($no_ctrl),
    //'filter'=>$modelSSServiciosSocialesAnteriores,
    'columns'=>array(
        /*'id_servicio_social_anterior',
        'no_ctrl',*/
        array(
            'name' => 'nombre_programa',
            'htmlOptions' => array('width:400px', 'class'=>'text-center')
        ),
        array(
            'name' => 'nombre_empresa',
            'htmlOptions' => array('width:250px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPeriodoPrograma.periodo_programa',
            'htmlOptions' => array('width:50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'nombre_completo_supervisor',
            'htmlOptions' => array('width:200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'cargo_supervisor',
            'htmlOptions' => array('width:200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'horas_liberadas',
            'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->horas_liberadas.'</span>';
			},
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_inicio_programa',
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_fin_programa',
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Calificación Servicio Social',
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_servicio_social.'</span>';
			},
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
    ),
)); ?>
<?php } ?>

<br><br><br><br><br>
<br><br><br><br><br>

<div align="center">
    <?php //echo CHtml::link('Volver al Menú Principal', array('/'), array('class'=>'btn btn-success')); ?>
</div>
