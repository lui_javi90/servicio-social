<style>
    .hoja{
        background-color: transparent;
    }
    .div1{
        background-color: transparent;
        font-weight: bold;
    }
    .div2{
        background-color: transparent;
    }
    table, th, td{
        padding: 0px;
        padding-left: 0px;
        padding-bottom: 0px;
        padding-right: 0px;
        text-align: left;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 11px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado2{
        /*letter-spacing: 1pt;       para separar entre letras */
        word-spacing: 1.5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado3{
        line-height: 15pt;
    }
    #contenedor{
      margin-left:0px;
      margin-top:0px;
      padding:0 0 0 0;
      width:auto;
      /*background-color: #2196F3;*/
      position:absolute;
  }
  .divcontenido1{

      float:left;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:70%;
      text-align:left;
  }
  .divcontenido2{

      float:right;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:25%;
      text-align:left;
      margin-left:0px;
  }
</style>

<div class="hoja">
    <!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/banner_top_itc.jpg" />-->
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />
    <?php //echo '<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/banner_superior_'.trim($datos_carta_presentacion[0]['no_empresa']).'/'.$banners->banner_superior.'"/>'; ?>
    <br><br>
    <h5 style="font-style: italic;" class="center letra4"><span class="bold">"
                                            <?php echo ($leyenda_reporte_pdf != NULL) ? $anio.", ".$leyenda_reporte_pdf : ""; ?>
                                                        "</span></h5>
    <br>
    <!--fecha, oficio y asunto-->
    <div class="div2 interlineado3">
        <table style="width:50%" class="interlineado3 letra3" align="right">
            <tr class="interlineado3">
                <td align="right"><?php echo "Celaya, Guanajuato,&nbsp;<span style='background-color:black;color:white;' class='bold'>".$fec_reporte."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "Oficio No.&nbsp;<span class='bold'>D.G.T.V./SS Y DC/".$datos_carta_presentacion[0]['folio']."/".$datos_carta_presentacion[0]['anio_servicio_social']."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "Asunto:&nbsp;<span class='bold'>CARTA DE PRESENTACIÓN</span>"; ?></td>
            </tr>
        </table>
    </div>
    <!--fecha, oficio y asunto-->

    <!--Datos del supervisor del programa-->
    <br><br>
    <div class="div2 interlineado1">
        <h6 class="bold letra3 interlineado1"><?php echo strtoupper($datos_carta_presentacion[0]['nombre_jefe_depto']); ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo "JEFE DEL ".strtoupper($datos_carta_presentacion[0]['departamentoSupervisorJefe']); ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo strtoupper($datos_carta_presentacion[0]['empresaSupervisorJefe']); ?></h6>
        <h6 class="bold letra3 interlineado1">PRESENTE</h6>
    </div>
    <!--Datos del supervisor del programa-->

    <!--CUERPO DEL REPORTE-->
    <br><br>
    <div class="interlineado2 div2 letra3">
        <p ALIGN="justify">
            Por éste conducto presentamos a sus finas intenciones al (la) <?php echo "<span class='bold'>"."C. ".strtoupper($datos_carta_presentacion[0]['nombre_alumno'])."</span>";?> con
            número de control <?php echo "<span class='bold'>".$datos_carta_presentacion[0]['no_ctrl']."</span>"; ?> alumno (a) del <?php echo "<span class='bold'>".$datos_carta_presentacion[0]['semestre_alumno']."º semestre"."</span>"; ?> de la carrera de <?php echo "<span class='bold'>".$datos_carta_presentacion[0]['carrera_alumno']."</span>"; ?> quien desea
            realizar su <?php echo "<span class='bold'>Servicio Social</span>"; ?> en esa dependencia, cubriendo un total de <?php echo "<span class='bold'>".$datos_carta_presentacion[0]['horas_totales']." horas"."</span>"; ?> en el programa: <?php echo "<span class='bold'>".strtoupper($datos_carta_presentacion[0]['nombre_programa'])."</span>"; ?>,
            en un período mínimo de <?php echo "<span class='bold'>".$datos_carta_presentacion[0]['periodo_programa']."</span>"; ?> meses y no mayor a dos años.
            <br><br>
            Agradeciendo las atenciones que brinden al portador de la presente, nos es grato ofrecer a ustedes la
            seguridad de nuestra más alta y distinguida consideración.
        </p>
    </div>
    <!--CUERPO DEL REPORTE-->

    <!--Firma del encargado-->
    <br><br>
    <div class="interlineado1 div2 left">
        <h6 class="interlineado1 bold letra3">A T E N T A M E N T E</h6>
        <i class="interlineado1 letra3">LA TÉCNICA POR UN MÉXICO MEJOR &reg;</i>
    </div>
    
    <div id="contenedor">
        <div class="divcontenido1 left">
            <!--<img height="100" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/catalina.jpg" />-->
            <?php
            if($datos_carta_presentacion[0]['validacion_jefesup'] == NULL AND $datos_carta_presentacion[0]['estatus_validacion_validacion_jefesup'] == NULL)
            {
                /*Poner un if que si la firma no viene entonces deje el espacio en blanco*/
                echo "<br><br><br>";
            }else{
               
                echo "<br><br><br>";
                echo "El Jefe de Departamento valido la solicitud del Alumno el dia <br>"."<span class='bold'>".$fecha_validacion_sol[0]['fecha_act']." a las ".$fecha_validacion_sol[0]['hora']."</span>";
                echo "<h6 class=\"interlineado1 bold letra3\">".$datos_carta_presentacion[0]['grado_max_estudios_jefvinculacion']." ".$datos_carta_presentacion[0]['jefe_vinculacion']."</h6>";

            }
               
            ?>
            <?php echo "<span class='bold'>______________________________________________________________</span>"; ?>
            <h6 class="interlineado1 bold letra2">JEFE DEL DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        </div>
        <div class="divcontenido2 center">
            <!--<img height="150" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/sello_sep.jpg" />-->
        </div>
    </div>
    <!--Firma del encargado-->

</div>
