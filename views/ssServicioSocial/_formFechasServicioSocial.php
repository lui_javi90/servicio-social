<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'duracion-servicio-social-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
    'enableAjaxValidation'=>false,
)); ?>

    <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSServicioSocial); ?>
	
	<div class="form-group">
        <?php echo "<strong><p>Periodo Servicio Social <span class='required'>*</span></p></strong>"; ?>
		<?php echo $form->dropDownList($modelSSServicioSocial,
										'id_periodo_servicio_social',
										$lista_peridos,
										array('prompt'=>'--Selecciona Tipo de Servicio Social--', 'class'=>'form-control', 'required'=>'required')
										); ?>
        <?php echo $form->error($modelSSServicioSocial,'id_periodo_servicio_social'); ?>
    </div>

    <div class="form-group">
	<!--AQUI ACTUALIZAMOS LA FECHA FIN DEL SERVICIO SOCIAL-->
	<?php 
	/*$htmlOptions = array('size' => '10', 'maxlength' => '10', 'class'=>'form-control', 'required' => 'required', 'readOnly'=>true,
						'ajax' => array(
								'url' => $this->createUrl('calcularFechaFinServicioSocial'),
								'type' => 'POST',
								'update' => '#ServicioSocial_fecha_fin',
								'dataType'=>'json',
						),
				);*/
	?>
	<!--AQUI ACTUALIZAMOS LA FECHA FIN DEL SERVICIO SOCIAL-->
		<?php echo "<strong><p>Fecha de Inicio <span class='required'>*</span></p></strong>"; ?>
		<?php //echo $form->textField($modelSSServicioSocial,'fecha_inicio');
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $modelSSServicioSocial,
			'attribute' => 'fecha_inicio',
			'language' => 'es',
			'defaultOptions' => array(
				'showOn' => 'focus',
				'showOtherMonths' => true,
				'selectOtherMonths' => true,
				'changeMonth' => true,
				'changeYear' => false,
				'showButtonPanel' => true,
			) ,
			'options'=>array(
				'showAnim' => 'fold', //Animation 'fold', 'slide'
				'dateFormat' => 'yy-mm-dd', // save to db format
				'yearRange'=>'2010:2025',
			),
			'htmlOptions' => array(
				'size' => '10', 
				'maxlength' => '10', 
				'class'=>'form-control', 
				'required' => 'required', 
				'readOnly'=>true
		    )
		));
		?>
		<?php echo $form->error($modelSSServicioSocial,'fecha_inicio'); ?>
	</div>

	<!--LA FECHA SE CALCULARA AUTOMATICAMENTE, SE NECESITA PARA HACER LOS REPORTES BIMESTRALES-->
	<!--<b><p>* La Fecha Fin del Servicio Social se calculara dependiendo del tipo de Servicio Social que elijas y la Fecha de Inicio.</p></b>
	<div class="form-group">-->
	<?php //echo "<strong><p>Fecha Fin <span class='required'>*</span></p></strong>"; ?>
		<?php //echo $form->textField($modelSSServicioSocial, 'fecha_fin');
		/*$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $modelSSServicioSocial,
			'language' => 'es',
			'attribute' => 'fecha_fin',
			'options'=>array(
				'dateFormat' => 'yy-mm-dd', // save to db format
				'yearRange'=>'2010:2025',
			),
			'htmlOptions' => array('class'=>'form-control', 'disabled' => 'disabled',
				//'value' => date('Y-m-d'), // set the default date here
				'size' => '10',         // textField size
				'maxlength' => '10',    // textField maxlength
			),
		));*/
		?>
		<?php //echo $form->error($modelSSServicioSocial,'fecha_fin'); ?>
	</div>
	<!--LA FECHA SE CALCULARA AUTOMATICAMENTE, SE NECESITA PARA HACER LOS REPORTES BIMESTRALES-->

	<br>
    <div class="form-group">
		<?php echo CHtml::submitButton($modelSSServicioSocial->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaAdminServicioSocial'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
