<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Calificaciones Alumnos Servicio Social'
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Calificaciones Alumnos Servicio Social
		</span>
	</h2>
</div>


<br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-historico-totales-alumnos-grid',
    'dataProvider'=>$modelSsHistoricoTotalesAlumnos->search(),
    'filter'=>$modelSsHistoricoTotalesAlumnos,
    'columns'=>array(
        //'no_ctrl',
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        //'horas_totales_servicio_social',
        array(
            'name' => 'horas_totales_servicio_social',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        //'fecha_modificacion_horas_totales',
        array(
            'name' => 'fecha_modificacion_horas_totales',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        //'calificacion_final_servicio_social',
        array(
            'name' => 'calificacion_final_servicio_social',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        //'completo_servicio_social',
        array(
            'name' => 'completo_servicio_social',
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->completo_servicio_social == true) ? '<span style="font-size:14px" class="label label-success">SI</span>' : '<span style="font-size:14px" class="label label-danger">NO</span>';
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        //'firma_digital_alumno',
        /*
        'calificacion_kardex',
        */
        array(
			'class'=>'CButtonColumn',//
			'template'=>'{editCalServicioSocial}',
			'header'=>'Editar Reporte Bimestral',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editCalServicioSocial' => array
				(
					'label'=>'Editar Calificacion Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHistoricoTotalesAlumnos/editCalificacionServicioSocial", array("no_ctrl"=>$data->no_ctrl))',
                    'imageUrl'=>'images/servicio_social/agregar_32.png',
				),
			),
		),
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>