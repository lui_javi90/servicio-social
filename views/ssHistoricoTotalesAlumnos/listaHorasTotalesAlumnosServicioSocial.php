<?php
/* @var $this SsHistoricoTotalesAlumnosController */
/* @var $model SsHistoricoTotalesAlumnos */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Horas totales Alumnos de Servicio Social',
);

/*VALIDAR Y MANDAR CALIFICACION FINAL AL KARDEX */
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#ss-horas-totales-alumnos-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#ss-horas-totales-alumnos-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*VALIDAR Y MANDAR CALIFICACION FINAL AL KARDEX */

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Alumnos Completaron Servicio Social
		</span>
	</h2>
</div>



<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Alumnos que han completado su Servicio Social y se debe pasar su Calificación al Kardex.
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-horas-totales-alumnos-servicio-social-grid',
	'dataProvider'=>$modelSSHistoricoTotalesAlumnos->searchXAlumnosCompletaronServicioSocial(),
	'filter'=>$modelSSHistoricoTotalesAlumnos,
	'columns'=>array(
		array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return (empty($data->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->no_ctrl, '', array('style' =>"width:100px;height:100px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
		array(
			'header' => 'No. de Control',
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name' => 'noCtrl.nmbAlumno',
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'header' => 'Semestre',
			'name' => 'noCtrl.semAlumno',
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'header' => 'Horas Completadas',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->horas_totales_servicio_social.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
		),
		array(
			'header' => 'Calificación Servicio Social',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_final_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_final_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_final_servicio_social.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
		),
		/*array(
			'name' => 'fecha_modificacion_horas_totales',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateHorasTotalesAlumno($data->no_ctrl);
				return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{histHorasTotalesAlumnosServicioSocial}',
			'header'=>'Historial',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'histHorasTotalesAlumnosServicioSocial' => array
				(
					'label'=>'Editar Horas Totales Alumno Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHistoricoTotalesAlumnos/historialHorasTotalesAlumnosServicioSocial", array("no_ctrl"=>$data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/historial_docs_servsocial_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editHorasTotalesAlumnosServicioSocial}',
			'header'=>'Finalizar <br>Servicio Social',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editHorasTotalesAlumnosServicioSocial' => array
				(
					'label'=>'Editar Horas Totales Alumno Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHistoricoTotalesAlumnos/deshabilitarServicioSocialAlumnoCompleto", array("no_ctrl"=>$data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'options' => array(
						//'title'        => 'Enviado',
						'data-confirm' => 'Finalizar Servicio Social ?',
					),
					'click'   => $approveJs, 
				),
			),
		),
		/*[
			'class'                => 'CButtonColumn',
			'template'             => '{enviadaAlKardex}, {noEnviadaAlKardex}', // buttons here...
			'header' 			   => 'Envíar Calificación al Kardex',
			'htmlOptions' 		   => array('width'=>'90px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'noEnviadaAlKardex'   => [
					'label'   => 'Agregar calificación al Kardex',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssHistoricoTotalesAlumnos/enviarCalificacionKardexAlumno", array("no_ctrl"=>$data->no_ctrl))', // ?r=controller/approve/id/123
					'imageUrl'=> 'images/servicio_social/no_aprobado_32.png',
					'visible'=> '$data->calificacion_kardex == false', // <-- <-- La calificacion se enviará al Kardex
					'options' => [
						//'title'        => 'Enviado',
						'data-confirm' => 'Envíar la Calificación al Kardex ?',
					],
					'click'   => $approveJs, 
				],
				'enviadaAlKardex' => [
					'label'   => 'La calificación ya esta en el kardex',
					'url'     => '#', //
					'imageUrl' => 'images/servicio_social/aprobado_32.png',
					'visible' => '$data->calificacion_kardex == true', // <-- La calificacion ya fue enviada al Kardex
				],
			],
		],*/
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>