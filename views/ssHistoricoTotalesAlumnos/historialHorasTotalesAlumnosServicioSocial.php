<?php
/* @var $this SsHistoricoTotalesAlumnosController */
/* @var $model SsHistoricoTotalesAlumnos */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Horas totales Alumnos de Servicio Social' => array('listaHorasTotalesAlumnosServicioSocial'),
    'Historial Servicio Social Alumno'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Historial Servicio Social Alumno
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		    Historial de todo el Servicio Social del Alumno 
    </strong></p>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Información del Alumno
                </h3>
            </div>
            <div class="panel-body">
                <div style="padding-top:10px" class="col-xs-3" align="center">
                    <!--Es un Web Service-->
                    <img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="200">
                    <!--Es un Web Service-->
                </div>
                <div class="col-xs-9" align="left">
                    <p><b>Nombre del Alumno:</b>
                    &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                    <p><b>Carrera:</b>
                    &nbsp;&nbsp;<?php echo $carrera; ?></p>

                    <p><b>Semestre:</b>
                    &nbsp;&nbsp;<?php echo $semestre; ?></p>

                    <p><b>No. de Control:</b>
                    &nbsp;&nbsp;<?php echo $no_ctrl; ?></p>

                    <p><b>Estatus del Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSHistoricoTotalesAlumnos->completo_servicio_social == false) ? "INCOMPLETO" : "COMPLETO";
                                    echo "<b>"." (".($horas_servicio_social - $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social)." hrs. faltantes".")"."</b>";
                                ?></p>

                    <p><b>Total de Horas Completadas de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social == $horas_servicio_social) ?
                                    '<span style="font-size:18px" class="label label-success">'.$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social.'</span>' :
                                    '<span style="font-size:18px" class="label label-danger">'.$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social.'</span>'; ?>

                    <?php //if($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social != 0){ ?>
                    <p><b>Calificación Final de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social >= 70) ?
                                    '<span style="font-size:18px" class="label label-success">'.$modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social.'</span>' :
                                    '<span style="font-size:18px" class="label label-danger">'.$modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social.'</span>'; ?>
                    <?php //} ?>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Servicio Social Completado
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-servicio-social-grid',
    'dataProvider'=>$modelSSServicioSocial->searchXAlumnoCompletoServicioSocial($no_ctrl),
    'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        //'id_servicio_social',
        //'no_ctrl',
        //'id_estado_servicio_social',
        array(
			'name' => 'idPrograma.nombre_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Horas Liberadas',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->idPrograma->horas_totales.'</span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Tipo Servicio Social', //id_tipo_servicio_social
            'value' => function($data)
            {
                return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'name' => 'idEstadoServicioSocial.estado',
			'filter' => false,
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Calificación Servicio Social',
            'filter' => false,
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_servicio_social.'</span>';
			},
            'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{detHistServicioSocialAlumno}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:80px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detHistServicioSocialAlumno' => array
				(
					'label'=>'Detalle Histórico',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHistoricoTotalesAlumnos/detalleProgramaHistoricoAlumno", array("id_servicio_social"=>$data->id_servicio_social, "no_ctrl"=>$data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{repBimHistServicioSocialAlumno}',
			'header'=>'Reportes Bimestrales',
			'htmlOptions'=>array('width:80px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'repBimHistServicioSocialAlumno' => array
				(
					'label'=>'Reportes Bimestrales',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/repBimHistoricoEscolaresAlumnoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social, "no_ctrl" => '.$no_ctrl.'))',
					'imageUrl'=>'images/servicio_social/historial_docs_servsocial_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{cartHistServicioSocialAlumno}',
			'header'=>'Cartas',
			'htmlOptions'=>array('width:80px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'cartHistServicioSocialAlumno' => array
				(
					'label'=>'Cartas Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServicioSocial/cartasHistoricoEscolaresAlumnoServicioSocial", array("id_servicio_social"=>$data->id_servicio_social, "no_ctrl" => $data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/cartas_servsocial_32.png',
				),
			),
       ),
        /*'fecha_registro',
        'fecha_modificacion',
        'calificacion_servicio_social',
        'rfcDirector',
        'rfcSupervisor',
        'cargo_supervisor',
        'empresaSupervisorJefe',
        'nombre_jefe_depto',
        'departamentoSupervisorJefe',
        'rfcJefeOficServicioSocial',
        'rfcJefeDeptoVinculacion',
        'nombre_supervisor',
        */
    ),
)); ?>

<?php if($hayRegServicioSocialPasados == true){ ?>
<br><br><br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Registro Histórico de Servicios Sociales Anteriores
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		    Servicios Sociales registrados anteriormente.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-servicios-sociales-anteriores-grid',
    'dataProvider'=>$modelSSServiciosSocialesAnteriores->searchXAlumno($no_ctrl),
    //'filter'=>$modelSSServiciosSocialesAnteriores,
    'columns'=>array(
        /*'id_servicio_social_anterior',
        'no_ctrl',*/
        array(
            'name' => 'nombre_programa',
            'htmlOptions' => array('width:400px', 'class'=>'text-center')
        ),
        array(
            'name' => 'nombre_empresa',
            'htmlOptions' => array('width:250px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPeriodoPrograma.periodo_programa',
            'htmlOptions' => array('width:50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'nombre_completo_supervisor',
            'htmlOptions' => array('width:200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'cargo_supervisor',
            'htmlOptions' => array('width:200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'horas_liberadas',
            'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->horas_liberadas.'</span>';
			},
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_inicio_programa',
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_fin_programa',
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Calificación Servicio Social',
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_servicio_social.'</span>';
			},
            'htmlOptions' => array('width:30px', 'class'=>'text-center')
        ),
    ),
)); ?>
<?php } ?>

<br><br><br><br>


