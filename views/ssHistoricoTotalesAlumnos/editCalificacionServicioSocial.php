<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Calificaciones Alumnos Servicio Social' => array('ssHistoricoTotalesAlumnos/calificacionesAlumnosServicioSocialTodos'),
    'Editar Calificacion Servicio Social'
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Editar Calificacion Servicio Social <?php echo $modelSsHistoricoTotalesAlumnos->no_ctrl; ?>
		</span>
	</h2>
</div>


<br><br><br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Editar
                </h4>
            </div>
            <div class="panel-body">

                <?php
                /* @var $this SsHistoricoTotalesAlumnosController */
                /* @var $model SsHistoricoTotalesAlumnos */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ss-historico-totales-alumnos-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

                    <?php echo $form->errorSummary($modelSsHistoricoTotalesAlumnos); ?>

                    <div class="row">
                        <?php echo $form->labelEx($modelSsHistoricoTotalesAlumnos,'horas_totales_servicio_social'); ?>
                        <?php echo $form->textField($modelSsHistoricoTotalesAlumnos,'horas_totales_servicio_social',array('size'=>3,'maxlength'=>3, 'class'=>'form-control')); ?>
                        <?php echo $form->error($modelSsHistoricoTotalesAlumnos,'horas_totales_servicio_social'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSsHistoricoTotalesAlumnos,'calificacion_final_servicio_social'); ?>
                        <?php echo $form->textField($modelSsHistoricoTotalesAlumnos,'calificacion_final_servicio_social',array('size'=>3,'maxlength'=>3, 'class'=>'form-control')); ?>
                        <?php echo $form->error($modelSsHistoricoTotalesAlumnos,'calificacion_final_servicio_social'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSsHistoricoTotalesAlumnos,'completo_servicio_social'); ?>
                        <?php echo $form->checkBox($modelSsHistoricoTotalesAlumnos,'completo_servicio_social', array('class'=>'form-control')); ?>
                        <?php echo $form->error($modelSsHistoricoTotalesAlumnos,'completo_servicio_social'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelSsHistoricoTotalesAlumnos->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-success')); ?>
                        <?php echo CHtml::link('Cancelar', array('calificacionesAlumnosServicioSocialTodos'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->


            </div>
        </div>
    </div>
</div>


<br><br><br><br><br>
<br><br><br><br><br>

