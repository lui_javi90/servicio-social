<?php
/* @var $this SsHistoricoTotalesAlumnosController */
/* @var $model SsHistoricoTotalesAlumnos */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Histórico Servicios Sociales Finalizados',
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Histórico Servicios Sociales Finalizados
		</span>
	</h2>
</div>

<br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-historico-totales-alumnos-grid',
    'dataProvider'=>$modelSsHistoricoTotalesAlumnos->searchXServicioSocialCompletadoXAlumno($cve_especialidad),
    'filter'=>$modelSsHistoricoTotalesAlumnos,
    'columns'=>array(
        //'no_ctrl',
        /*array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return (empty($data->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->no_ctrl, '', array('class'=>'img-circle', 'style' =>"width:100px;height:100px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),*/
        array(
            'class' => 'ComponentNameAlumnHistoricoSE',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'header' => 'No. de Control',
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		/*array(
			'name' => 'noCtrl.nmbAlumno',
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
        ),*/
        array(
            'header' => 'Carrera',
            'filter' => CHtml::activeDropDownList($modelSsHistoricoTotalesAlumnos,
                                                  'cve_especialidad',
                                                  $lista_carreras,
                                                  array('prompt'=>' -- Carreras --')

            ),
            'value' => function($data)
            {
                $no_ctrl = $data->no_ctrl;

                $qry_esp = "select \"dscEspecialidad\" as carrera
                            from public.\"E_datosAlumno\" eda
                            join public.\"E_especialidad\" esp
                            on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
                            where eda.\"nctrAlumno\" = '$no_ctrl'           
                ";

                $esp = Yii::app()->db->createCommand($qry_esp)->queryAll();

                return $esp[0]['carrera'];

            },
            'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Semestre',
            //'name' => '',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:14px" class="label label-info">'.$data->noCtrl->semAlumno.'</span>';

            },
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Horas Completadas',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->horas_totales_servicio_social.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Calificación Servicio Social',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->calificacion_final_servicio_social >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_final_servicio_social.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_final_servicio_social.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
		),
        /*array(
            'header' => 'Año',
            'filter' => 
            'value' => function($data)
            {

            },
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Periodo',
            'filter' => 
            'value' => function($data)
            {

            },
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),*/
        array(
			'class'=>'CButtonColumn',
			'template'=>'{histServicioSocialAlumno}',
			'header'=>'Historial',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'histServicioSocialAlumno' => array
				(
					'label'=>'Histórico Alumno Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssHistoricoTotalesAlumnos/historialTotalAlumnoServicioSocial", array("no_ctrl"=>$data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/historial_docs_servsocial_32.png',
				),
			),
		),
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>