<?php

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Agregar Firma Digital',
);
?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Cargar Firma Digital
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		La firma digital servirá para firmar digitalmente tu Solicitud de Servicio Social.
    </strong></p>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Cargar Firma Digital
                </h6>
            </div>
            <div class="panel panel-body">
                <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
					    'id'=>'ss-horas-totales-alumnos-servicio-social-form',
					    // Please note: When you enable ajax validation, make sure the corresponding
					    // controller action is handling ajax validation correctly.
					    // There is a call to performAjaxValidation() commented in generated controller code.
					    // See class documentation of CActiveForm for details on this.
					    'enableAjaxValidation'=>false,
					    //enctype para que el formulario permita la subida de archivos
					    'htmlOptions' => array('autocomplete' => 'off', 'enctype'=>'multipart/form-data')
					)); ?>

                        <?php echo $form->errorSummary($modelSSHistoricoTotalesAlumnos); ?>

                        <p align="center"><strong>Es OBLIGATORIO subir tu firma digital.</strong></p>

                        <br>
                        <p><strong>NOTA:</strong> La firma digital debe cumplir con los requisitos definidos.</p>

                        <p><strong>* El nombre del archivo debe ser tu No. de control.</strong></p>
                        <p><strong>* Tamaño maximo de la imagen de 2MB.</strong></p>
                        <p><strong>* Solo formatos .jpg</strong></p>

                        <br>
                        <div class="form-group">
										        <?php echo $form->labelEx($modelSSHistoricoTotalesAlumnos,'firma_digital_alumno'); ?>
										        <?php echo $form->fileField($modelSSHistoricoTotalesAlumnos,'firma_digital_alumno', array('class'=>'form-control')); ?>
										        <?php echo $form->error($modelSSHistoricoTotalesAlumnos,'firma_digital_alumno'); ?>
										    </div>

                        <br>
												<?php if($modelSSHistoricoTotalesAlumnos->firma_digital_alumno === NULL){?>
                        <div class="form-group">
                            <?php echo CHtml::submitButton($modelSSHistoricoTotalesAlumnos->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                            <?php echo CHtml::link('Cancelar', array('/'), array('class'=>'btn btn-danger')); ?>
                        </div>
											<?php }else{?>
												<div class="form-group">
													<?php echo CHtml::link('Volver al menu', array('/'), array('class'=>'btn btn-success')); ?>
												</div>
											<?php }?>

                    <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong>Firma digital actual</strong></p>
                <br><br>
                <?php
                    echo ($modelSSHistoricoTotalesAlumnos->firma_digital_alumno === NULL) ? '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/firmas_digitales_alumnos/firma_digital_default.png"/>' : '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/firmas_digitales_alumnos/'.$no_ctrl."/".$modelSSHistoricoTotalesAlumnos->firma_digital_alumno.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>
