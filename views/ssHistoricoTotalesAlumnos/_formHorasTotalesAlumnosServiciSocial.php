<?php
/* @var $this SsHistoricoTotalesAlumnosController */
/* @var $model SsHistoricoTotalesAlumnos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-historico-totales-alumnos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

<?php echo $form->errorSummary($modelSSHistoricoTotalesAlumnos); ?>

<div class="form-group">
	<?php echo $form->labelEx($modelSSHistoricoTotalesAlumnos,'horas_totales_servicio_social'); ?>
	<?php echo $form->textField($modelSSHistoricoTotalesAlumnos,'horas_totales_servicio_social',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSHistoricoTotalesAlumnos,'horas_totales_servicio_social'); ?>
</div>

<br>
<div class="form-group">
	<?php echo CHtml::submitButton($modelSSHistoricoTotalesAlumnos->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::link('Cancelar', array('listaHorasTotalesAlumnosServicioSocial'), array('class'=>'btn btn-danger')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->