<?php
/* @var $this SsHistoricoTotalesAlumnosController */
/* @var $model SsHistoricoTotalesAlumnos */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Lista Horas Totales Alumnos Servicio Social' => array('ssHistoricoTotalesAlumnos/listaHorasTotalesAlumnosServicioSocial'),
	'Editar Horas Totales Alumo de Servicio Social',
);
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Horas Totales del Alumno de su Servicio Social
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Información del Alumno
				</h6>
			</div>
			<div class="panel-body">
				<div class="col-xs-3">
					<!--Es un Web Service-->
					<img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $modelAlumnosVigentesInscritosCreditos->no_ctrl; ?>" alt="foto alumno" height="200">
				</div>
				<div class="col-xs-9" align="left">
					<p><b><?php echo CHtml::encode($modelAlumnosVigentesInscritosCreditos->getAttributeLabel('nombres')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelAlumnosVigentesInscritosCreditos->nombres.' '.$modelAlumnosVigentesInscritosCreditos->apellido_paterno.' '.$modelAlumnosVigentesInscritosCreditos->apellido_materno; ?></p>
					
					<p><b><?php echo CHtml::encode($modelAlumnosVigentesInscritosCreditos->getAttributeLabel('no_ctrl')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelAlumnosVigentesInscritosCreditos->no_ctrl; ?></p>

					<p><b><?php echo CHtml::encode($modelAlumnosVigentesInscritosCreditos->getAttributeLabel('dscEspecialidad')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelAlumnosVigentesInscritosCreditos->dscEspecialidad; ?></p>
					
					<p><b><?php echo CHtml::encode($modelAlumnosVigentesInscritosCreditos->getAttributeLabel('semestre')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelAlumnosVigentesInscritosCreditos->semestre; ?></p>
					
					<p><b><?php echo CHtml::encode($modelAlumnosVigentesInscritosCreditos->getAttributeLabel('val_serv_social')); ?>:</b>
					&nbsp;&nbsp;<?php echo ($modelAlumnosVigentesInscritosCreditos->val_serv_social == true) ? "ACTIVADO" : "DESACTIVADO"; ?></p>
					
					<p><b><?php echo CHtml::encode($modelSSHistoricoTotalesAlumnos->getAttributeLabel('horas_totales_servicio_social')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social; ?></p>
				</div>
			</div>	
		</div>
	</div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Horas Totales del Alumo
		</span>
	</h2>
</div>


<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Editar Horas Totales del Alumo</h6>
			</div>
			<div class="panel-body">
				<p><b>Fecha de la Última modificación:</b>
				&nbsp;&nbsp;<span style="color:red;font-weight:bold;font-size: 12pt"><?php echo $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora']; ?></span></p>
				<br>
				<?php $this->renderPartial('_formHorasTotalesAlumnosServiciSocial', array(
										'modelSSHistoricoTotalesAlumnos'=>$modelSSHistoricoTotalesAlumnos
										)); ?>
			</div>
		</div>
	</div>
</div>