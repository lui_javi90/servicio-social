<style>
    hr{
        border: none;
        height: 2px;
        padding: 0px;
        padding-left: 0px;
        /*color: #0091EA;*/
    }
</style>

<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */
$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Unidades Receptoras' => array('ssUnidadesReceptoras/menuEmpresas'),
    'Empresas' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
    'Agregar Banners Empresas',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Cargar Banners de la Empresa <br><br><b><?php echo $modelSSUnidadesReceptoras->nombre_unidad_receptora; ?></b>
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Los banners se agregaran a cada documento que lo requiera como la Carta de Aceptación y Terminación del Alumno del Programa en la Empresa.</b>
  </strong></p>
</div>


<!--Banners-->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
                    Cargar Banners
				</h6>
			</div>
			<div class="panel-body">
                <!--row superior-->
                <?php
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/ValidacionesServicioSocial.php';
                    $fecha = ValidacionesServicioSocial::fechaModificacionDatosEmpresa($modelSSUnidadesReceptoras->id_unidad_receptora);
                    $data = "Última modificación el día ";
                ?>
                <div align="center">
                    <div class="alert alert-success">
                        <p><strong>
                        <b><?php echo ($fecha[0]['fecha_act'] == 'ND') ? "Desconocida" : $data.$fecha[0]['fecha_act']. ' a las '.$fecha[0]['hora']; ?></b>
                        </strong></p>
                    </div>
                </div>
                <!--row superior-->

                <!--row inferior-->
                <br><br>
                <div class="row">
                    
                    <div class="col-lg-4" align="left">
                        <?php $this->renderPartial('_formBannersUnidadReceptora', array(
                                                    'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
                                                    )); ?>
                    </diV>
                    <div class="col-lg-8" align="center">
                        <p align="center"><strong><?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Banner Superior Default" : "Banner Superior actual Empresa" ; ?></strong></p>
                        <br><br>
                        <?php 
                            echo ($modelSSUnidadesReceptoras->banner_superior === null) ? '<img align="center" heigth="250" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_512.png"/>' : '<img align="center" heigth="100" width="600" src="'. Yii::app()->request->baseUrl.'/images/encabezados_empresas/'.trim($modelSSUnidadesReceptoras->path_carpeta).'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.trim($modelSSUnidadesReceptoras->banner_superior).'"/>';
                        ?>

                        <br><br><br><br><br><br><br><hr><br>
                        <p align="center"><strong><?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Banner Inferior Default" : "Banner Inferior actual Empresa" ; ?></strong></p>
                        <br><br>
                        <?php 
                            echo ($modelSSUnidadesReceptoras->banner_inferior === null) ? '<img align="center" heigth="250" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_512.png"/>' : '<img align="center" heigth="100" width="600" src="'. Yii::app()->request->baseUrl.'/images/encabezados_empresas/'.trim($modelSSUnidadesReceptoras->path_carpeta).'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.trim($modelSSUnidadesReceptoras->banner_inferior).'"/>';
                        ?>
                    </div>
                </div>
                <!--row inferior-->
			</div>
		</div>
	</div>
</div>
<!--Banners-->