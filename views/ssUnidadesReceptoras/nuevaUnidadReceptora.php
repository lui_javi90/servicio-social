<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */

if($modelSSUnidadesReceptoras->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Unidades Receptoras' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
		'Nueva Unidad Receptora',
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Unidades Receptoras' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
		'Editar Unidad Receptora',
	);
}

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Unidad Receptora
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Nueva Empresa o Unidad Receptora" : "Editar Empresa o Unidad Receptora"; ?>
				</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formUnidadReceptora', array(
										'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
										'lista_estados' => $lista_estados,
										'lista_municipios' => $lista_municipios
										)); ?>
			
			</div>
		</div>
	</div>
	<div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong><?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Logo Default" : "Logo actual" ; ?></strong></p>
                <br><br>
                <?php 
                    echo ($modelSSUnidadesReceptoras->logo_unidad_receptora === NULL) ? '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/logo_empresa_default.png"/>' : '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/encabezados_empresas/'.$modelSSUnidadesReceptoras->path_carpeta.'/'.$modelSSUnidadesReceptoras->logo_unidad_receptora.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>
