<style>
    hr{
        border: none;
        height: 2px;
        padding: 0px;
        padding-left: 0px;
        /*color: #0091EA;*/
    }
</style>

<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */
$this->breadcrumbs=array(
    'Inicio'=>array('serviciosocial/'),
    'Unidades Receptoras' => array('ssUnidadesReceptoras/menuEmpresas'),
    'Empresas' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
    'Agregar Banners Empresas',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Cargar Banners de la Empresa <br><br><b><?php echo $modelSSUnidadesReceptoras->nombre_unidad_receptora; ?></b>
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Los banners se agregaran a cada documento que lo requiera como la Carta de Aceptación y Terminación del Alumno del Programa en la Empresa.</b>
  </strong></p>
</div>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Agregar Banner Superior 
		</span>
	</h2>
</div>

<!--Banner Superior-->
<br>
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
                    Cargar Banner Superior
				</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formBannerSuperiorUnidadReceptora', array(
										'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
										)); ?>
			
			</div>
		</div>
	</div>
	<div class="col-lg-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong><?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Banner Superior Default" : "Banner Superior actual Empresa" ; ?></strong></p>
                <br><br>
                <?php 
                    echo ($modelSSUnidadesReceptoras->banner_superior == null) ? '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/banner_512.png"/>' : '<img align="center" heigth="100" width="600" src="'. Yii::app()->request->baseUrl.'/empresas/'.$modelSSUnidadesReceptoras->path_carpeta.'/'.$modelSSUnidadesReceptoras->banner_superior.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>
<!--Banner Superior-->

<hr>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Agregar Banner Inferior 
		</span>
	</h2>
</div>

<!--Banner Inferior-->
<br>
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
                    Cargar Banner Inferior
				</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formBannerInferiorUnidadReceptora', array(
										'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
										)); ?>
			
			</div>
		</div>
	</div>
	<div class="col-lg-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong><?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Banner Inferior Default" : "Banner Inferior actual Empresa" ; ?></strong></p>
                <br><br>
                <?php 
                    echo ($modelSSUnidadesReceptoras->banner_inferior == null) ? '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/banner_512.png"/>' : '<img align="center" heigth="100" width="600" src="'. Yii::app()->request->baseUrl.'/empresas/'.$modelSSUnidadesReceptoras->path_carpeta.'/'.$modelSSUnidadesReceptoras->banner_inferior.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>
<!--Banner Inferior-->