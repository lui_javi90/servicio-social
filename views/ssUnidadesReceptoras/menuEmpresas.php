<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Ménu de Empresas'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Ménu de Empresas
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Criterios
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssUnidadesReceptoras/listaUnidadesReceptoras"
						title="Clasificación de Áreas de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;&nbsp;&nbsp;Empresas o Unidades Receptoras
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssUnidadesReceptoras/listaHistoricoUnidadesReceptoras"
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-book"></span>&nbsp;&nbsp;&nbsp;&nbsp;Histórico Empresas o Unidades Receptoras
					</a>
					<?php
					}
					?>

					<?php
					//if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or Yii::app()->user->checkAccess(''))
					//{
					?>
					<!--<a href="index.php?r=serviciosocial/ssUnidadesReceptoras/nuevosBannersEmpresa"
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-picture"></span>&nbsp;&nbsp;&nbsp;&nbsp;Sellos y Banners Empresas o Unidades Receptoras
					</a>-->
					<?php
					//}
					?>

				</div>
			</div>
		</div>
	</div>
</div>