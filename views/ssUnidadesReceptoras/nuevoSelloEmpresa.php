<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */

$this->breadcrumbs=array(
	'Inicio'=>array('serviciosocial/'),
	'Ménu de Empresas' => array('ssUnidadesReceptoras/menuEmpresas'),
	'Empresas' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
	'Agregar Sello Empresa',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Cargar Sello Empresa <br><br><b><?php echo $modelSSUnidadesReceptoras->nombre_unidad_receptora; ?></b>
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>El sello es importante para darle valides a cada documento que lo requiera como la Carta de Aceptación y Terminación del Alumno del Programa en la Empresa.</b>
  </strong></p>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Agregar Sello Empresa" : "Editar Sello Empresa"; ?>
				</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formSelloUnidadReceptora', array(
										'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
										)); ?>
			
			</div>
		</div>
	</div>
	<div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong><?php echo ($modelSSUnidadesReceptoras->isNewRecord) ? "Sello Default" : "Sello actual Empresa" ; ?></strong></p>
                <br><br>
                <?php 
                    echo ($modelSSUnidadesReceptoras->sello_empresa === null) ? '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/logo_empresa_default.png"/>' : '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/empresas/'.$modelSSUnidadesReceptoras->path_carpeta.'/'.$modelSSUnidadesReceptoras->sello_empresa.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>