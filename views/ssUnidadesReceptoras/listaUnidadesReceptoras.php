<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Menu Empresas' => array('ssUnidadesReceptoras/menuEmpresas'),
	'Empresas o Unidades Receptoras',
);

//Validar deshabilitar la empresa
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#unidades-receptoras-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#unidades-receptoras-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//Validar deshabilitar la empresa

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Empresas
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div align="right">
<?php echo CHtml::link('Nueva Unidad Receptora', array('nuevaUnidadReceptora'), array('class'=>'btn btn-success'));?>
</div>

<br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'unidades-receptoras-grid',
	'dataProvider'=>$modelSSUnidadesReceptoras->searchXAlta(),
	'filter'=>$modelSSUnidadesReceptoras,
	'columns'=>array(
		//'id_unidad_receptora',
		array(
			'name' => 'nombre_unidad_receptora',
			'htmlOptions' => array('width'=>'230px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo Empresa',
			'value' => function($data)
			{
				return ($data->id_tipo_empresa == 1) ? "INTERNA" : "EXTERNA";
			},
			'filter' => CHtml::activeDropDownList($modelSSUnidadesReceptoras,
												 'id_tipo_empresa',
												 array('1'=>'INTERNA', '2'=>'EXTERNA'),
												 array('prompt'=>'--Filtrar por tipo Empresa--')
												),
			'htmlOptions' => array('width'=>'190px', 'class'=>'text-center')
		),
		array(
			'name' => 'idMunicipio.nombre',
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'header' => 'Dirección',
			'value' => function($data)
			{
				$id_unidad = $data->id_unidad_receptora;
				$query =
				"
				Select (calle||' '||'#'||numero||' Col. '||colonia) as direccion
				from pe_planeacion.ss_unidades_receptoras
				where id_unidad_receptora = '$id_unidad'
				";

				$direccion = Yii::app()->db->createCommand($query)->queryAll();

				return $direccion[0]['direccion'] ? $direccion[0]['direccion'] : "NO ESPECIFICADO";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{agregarLogoUnidadReceptora}',
			'header'=>'Logotipo',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'agregarLogoUnidadReceptora' => array
				(
					'label'=>'Subir Logo Unidad Receptora',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/editarLogoUnidadesReceptoras", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/servicio_social/upload_image_32.png',
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{agregarSello}',
			'header'=>'Sello Empresa',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'agregarSello' => array
				(
					'label'=>'Agregar Sello de la Empresa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/nuevoSelloEmpresa", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/banners_32.png',
				),
			),
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{agregarBanners}',
			'header'=>'Banners Empresa',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'agregarBanners' => array
				(
					'label'=>'Agregar Sello de la Empresa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/nuevosBannersEmpresa", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/servicio_social/banners_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editarUnidadReceptora}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editarUnidadReceptora' => array
				(
					'label'=>'Editar Unidad Receptora',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/editarUnidadesReceptoras", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detUnidadReceptora}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detUnidadReceptora' => array
				(
					'label'=>'Detalle Unidad Receptora',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/detalleUnidadReceptora", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{quitar}', // buttons here...
			'header' 			   => 'Deshabilitar',
			'htmlOptions' 		   => array('width'=>'75px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'quitar'   => [
					'label'   => 'Deshabilitar Empresa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/deshabilitarUnidadReceptora", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=> 'images/servicio_social/quitar_32.png',
					'visible'=> '$data->id_status_unidad_receptora != 3', //Habilitado si el programa esta dado de ALTA
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => '¿Confirmar para deshabilitar la Empresa?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],//Fin
	),
)); ?>
