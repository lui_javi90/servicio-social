<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Menu Empresas' => array('ssUnidadesReceptoras/menuEmpresas'),
	'Histórico Empresas o Unidades Receptoras',
);


//Validar la habilitacion de la empresa
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#hist-unidades-receptoras-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#hist-unidades-receptoras-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//Validar la habilitacion de la empresa

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Histórico Empresas o Unidades Receptoras
		</span>
	</h2>
</div>

<br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'hist-unidades-receptoras-grid',
	'dataProvider'=>$modelSSUnidadesReceptoras->search(),
	'filter'=>$modelSSUnidadesReceptoras,
	'columns'=>array(
		//'id_unidad_receptora',
		array(
            'header' => 'Muncipio',
			'name' => 'nombre_unidad_receptora',
			'htmlOptions' => array('width'=>'230px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo Empresa',
			'value' => function($data)
			{
				return ($data->id_tipo_empresa == 1) ? "INTERNA" : "EXTERNA";
			},
			'filter' => CHtml::activeDropDownList($modelSSUnidadesReceptoras,
												 'id_tipo_empresa',
												 array('1'=>'INTERNA', '2'=>'EXTERNA'),
												 array('prompt'=>'--Filtrar por tipo--')
			),
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
			'name' => 'idMunicipio.nombre',
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'header' => 'Dirección',
			'value' => function($data)
			{
				$id_unidad = $data->id_unidad_receptora;
				$query =
				"
				Select (calle||' '||'#'||numero||' Col. '||colonia) as direccion
				from pe_planeacion.ss_unidades_receptoras
				where id_unidad_receptora = '$id_unidad'
				";

				$direccion = Yii::app()->db->createCommand($query)->queryAll();

				return $direccion[0]['direccion'] ? $direccion[0]['direccion'] : "NO ESPECIFICADO";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estatus',
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->id_status_unidad_receptora == 1) ? '<span class="label label-success">ACTIVA</span>' : '<span class="label label-danger">INACTIVA</span>';
            },
            'filter' => CHtml::activeDropDownList($modelSSUnidadesReceptoras,
                                                  'id_status_unidad_receptora',
                                                  array('1'=>'ACTIVA', '2'=>'INACTIVA'),
                                                  array('prompt'=>'--Filtrar por estatus--')
            ),
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detUnidadReceptora}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detUnidadReceptora' => array
				(
					'label'=>'Detalle Unidad Receptora',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/detalleUnidadReceptora", array("id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{quitar}, {agregar}', // buttons here...
			'header' 			   => 'Estatus Empresa',
			'htmlOptions' 		   => array('width'=>'70px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'quitar'   => [
					'label'   => 'Deshabilitar Empresa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/deshabilitarUnidadReceptora", array("id_unidad_receptora"=>$data->id_unidad_receptora, "opc" => 2))',
					'imageUrl'=> 'images/servicio_social/quitar_32.png',
					'visible'=> '$data->id_status_unidad_receptora == 1', //Habilitado si el programa esta dado de ALTA
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => '¿Confirma para deshabilitar la Empresa?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
                ],
                'agregar'   => [
					'label'   => 'Habilitar Empresa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssUnidadesReceptoras/deshabilitarUnidadReceptora", array("id_unidad_receptora"=>$data->id_unidad_receptora, "opc" => 1))',
					'imageUrl'=> 'images/servicio_social/add_32.png',
					'visible'=> '$data->id_status_unidad_receptora == 2', //Habilitado si el programa esta dado de ALTA
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => '¿Confirma para habilitar la Empresa?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],//Fin
	),
)); ?>