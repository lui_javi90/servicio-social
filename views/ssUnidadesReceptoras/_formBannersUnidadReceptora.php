<?php
/* @var $this SsUnidadesReceptorasController */
/* @var $model SsUnidadesReceptoras */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'ss-banners-unidades-receptoras-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('autocomplete'=>'off', 'enctype'=>'multipart/form-data')
    )); ?>

    <?php echo $form->errorSummary($modelSSUnidadesReceptoras); ?>

    <p align="center"><strong>Los Banners son OPCIONALES.</strong></p>

    <br>
    <div align="center">
        <p><strong>NOTA:</strong> Los banners deben cumplir con los requisitos definidos.</p>

        <p><strong>* Tamaño maximo de la imagen de 2MB.</strong></p>
        <p><strong>* Solo formatos .jpg o jpeg.</strong></p>
    </div>

    <br>
    <div class="form-group">
        <?php echo $form->labelEx($modelSSUnidadesReceptoras,'banner_superior'); ?>
        <?php echo $form->fileField($modelSSUnidadesReceptoras,'banner_superior',array('class'=>'form-control')); ?>
        <?php echo $form->error($modelSSUnidadesReceptoras,'banner_superior'); ?>
    </div>

    <hr><br><br>
    <div align="center">
        <p><strong>NOTA:</strong> Los banners deben cumplir con los requisitos definidos.</p>
        <p><strong>* Tamaño maximo de la imagen de 2MB.</strong></p>
        <p><strong>* Solo formatos .jpg o jpeg.</strong></p>
    </div>

    <br>
    <div class="form-group">
        <?php echo $form->labelEx($modelSSUnidadesReceptoras,'banner_inferior'); ?>
        <?php echo $form->fileField($modelSSUnidadesReceptoras,'banner_inferior',array('class'=>'form-control')); ?>
        <?php echo $form->error($modelSSUnidadesReceptoras,'banner_inferior'); ?>
    </div>

    <br><br><br>
    <div class="form-group" align="center">
        <?php echo CHtml::submitButton($modelSSUnidadesReceptoras->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Volver a Empresas', array('listaUnidadesReceptoras'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->