<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'unidades-receptoras-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSUnidadesReceptoras); ?>

	<?php if($modelSSUnidadesReceptoras->isNewRecord){?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'nombre_unidad_receptora'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'nombre_unidad_receptora',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'nombre_unidad_receptora'); ?>
	</div>
	<?php }else{?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'nombre_unidad_receptora'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'nombre_unidad_receptora',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'readOnly'=>true)); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'nombre_unidad_receptora'); ?>
	</div>
	<?php }?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'rfc_unidad_responsable'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'rfc_unidad_responsable',array('size'=>13,'maxlength'=>13, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'rfc_unidad_responsable'); ?>
	</div>

	<div class="form-group">
		<?php
		$htmlOptions = array('prompt'=>'--Selecciona estados--','class'=>'form-control', 'required'=>'required',
					'ajax' => array(
						'url' => $this->createUrl('municipiosEstados'),
						'type' => 'POST',
						'update' => '#SsUnidadesReceptoras_id_municipio'
					),
				);
		?>
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'id_estado'); ?>
		<?php echo $form->dropDownList($modelSSUnidadesReceptoras,
										'id_estado',
										$lista_estados,
										$htmlOptions); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'id_estado'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'id_municipio'); ?>
		<?php echo $form->dropDownList($modelSSUnidadesReceptoras,
										'id_municipio',
										$lista_municipios,
										array('prompt'=>'--Selecciona municipios--','class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'id_municipio'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'calle'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'calle',array('size'=>40,'maxlength'=>40, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'calle'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'numero'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'numero',array('size'=>4,'maxlength'=>4, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'numero'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'colonia'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'colonia',array('size'=>40,'maxlength'=>40, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'colonia'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'telefono'); ?>
		<?php echo $form->textField($modelSSUnidadesReceptoras,'telefono',array('size'=>50,'maxlength'=>50, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSUnidadesReceptoras,'telefono'); ?>
	</div>

	<?php if($modelSSUnidadesReceptoras->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSUnidadesReceptoras,'id_tipo_empresa'); ?>
		<?php echo $form->dropDownList($modelSSUnidadesReceptoras,
									   'id_tipo_empresa',
									   array('1'=>'INTERNA', '2'=>'EXTERNA'),
									   array('prompt'=>'--Tipo de Empresa--', 'class'=>'form-control', 'required'=>'required')
									  ); ?>
        <?php echo $form->error($modelSSUnidadesReceptoras,'id_tipo_empresa'); ?>
	</div>
	<?php }else{ ?>
		<?php echo $form->labelEx($modelSSUnidadesReceptoras,'id_tipo_empresa'); ?>
		<?php echo $form->dropDownList($modelSSUnidadesReceptoras,
									   'id_tipo_empresa',
									   array('1'=>'INTERNA', '2'=>'EXTERNA'),
									   array('prompt'=>'--Tipo de Empresa--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
									  ); ?>
        <?php echo $form->error($modelSSUnidadesReceptoras,'id_tipo_empresa'); ?>
	<?php }?>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSUnidadesReceptoras->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaUnidadesReceptoras'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->