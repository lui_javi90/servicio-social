<?php
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Unidades Receptoras' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
    'Detalle Unidad Receptora'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Unidad Receptora
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Información Empresa</h6>
			</div>
			<div class="panel-body">
                <div class="col-xs-4" align="center">
                    <?php 
                        echo ($modelSSUnidadesReceptoras->logo_unidad_receptora != null) ? '<img align="center" width="220" height="300" src="'. Yii::app()->request->baseUrl.'/images/encabezados_empresas/'.$modelSSUnidadesReceptoras->path_carpeta.'/'.$modelSSUnidadesReceptoras->logo_unidad_receptora.'"/>' : '<img align="center" width="220" height="300" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/logo_empresa_default.png"/>';
                    ?>
                </div>
				<div class="col-xs-8" align="left">
                    <br><br><br>
                    <p><b><?php echo CHtml::encode($modelSSUnidadesReceptoras->getAttributeLabel('nombre_unidad_receptora')); ?>:</b>
                    &nbsp;&nbsp;<?php echo $modelSSUnidadesReceptoras->nombre_unidad_receptora; ?></p>

                    <p><b><?php echo CHtml::encode($modelSSUnidadesReceptoras->getAttributeLabel('rfc_unidad_responsable')); ?>:</b>
                    &nbsp;&nbsp;<?php echo $modelSSUnidadesReceptoras->rfc_unidad_responsable; ?></p>

                    <p><b>Estado:</b>
                    &nbsp;&nbsp;<?php echo $municipio.', '.$estado; ?></p>

                    <p><b>Ubicación:</b>
                    &nbsp;&nbsp;<?php echo $modelSSUnidadesReceptoras->calle.' #'.$modelSSUnidadesReceptoras->numero.', '.$modelSSUnidadesReceptoras->colonia; ?></p>

                    <p><b><?php echo CHtml::encode($modelSSUnidadesReceptoras->getAttributeLabel('telefono')); ?>:</b>
                    &nbsp;&nbsp;<?php echo $modelSSUnidadesReceptoras->telefono; ?></p>

                    <p><b>Tipo de Empresa:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSUnidadesReceptoras->id_tipo_empresa == 1) ? "INTERNA" : "EXTERNA"; ?></p>

                     <p><b>Estatus de la Empresa:</b>
                    &nbsp;&nbsp;<?php echo ($modelSSUnidadesReceptoras->id_status_unidad_receptora == 1) ? "ACTIVA" : "INACTIVA"; ?></p>
                </div>
            </div>
            <br><br><br>
            <div align="center">
                <?php //echo CHtml::link('Entendido', array('listaUnidadesReceptoras'), array('class'=>'btn btn-success')); ?>
            </div>
            <br>
		</div>
	</div>
</div>