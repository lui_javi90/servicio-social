<?php
/* @var $this SsUnidadesReceptorasController */
/* @var $model SsUnidadesReceptoras */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'ss-banninf-unidades-receptoras-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('autocomplete'=>'off', 'enctype'=>'multipart/form-data')
    )); ?>

    <?php echo $form->errorSummary($modelSSUnidadesReceptoras); ?>

    <p align="center"><strong>El Banner Superior es OPCIONAL.</strong></p>

    <br>
    <p><strong>NOTA:</strong> El Banner Superior debe cumplir con los requisitos definidos.</p>

    <p><strong>* Tamaño maximo de la imagen de 5MB.</strong></p>
    <p><strong>* Solo formatos .jpg o jpeg.</strong></p>

    <br>
    <div class="form-group">
        <?php echo $form->labelEx($modelSSUnidadesReceptoras,'banner_inferior'); ?>
        <?php echo $form->fileField($modelSSUnidadesReceptoras,'banner_inferior',array('class'=>'form-control')); ?>
        <?php echo $form->error($modelSSUnidadesReceptoras,'banner_inferior'); ?>
    </div>

    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSUnidadesReceptoras->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('listaUnidadesReceptoras'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->