<?php
/* @var $this SsUnidadesReceptorasController */
/* @var $model SsUnidadesReceptoras */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ss-unidades-receptoras-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('autocomplete'=>'off', 'enctype'=>'multipart/form-data')
)); ?>

    <!--<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>-->

    <?php echo $form->errorSummary($modelSSUnidadesReceptoras); ?>

    <p align="center"><strong>El Sello es OBLIGATORIO.</strong></p>

    <br>
    <p><strong>NOTA:</strong> El sello debe cumplir con los requisitos definidos.</p>

    <p><strong>* El nombre del archivo debe ser de maximo 18 caracteres.</strong></p>
    <p><strong>* Tamaño maximo de la imagen de 5MB.</strong></p>
    <p><strong>* Solo formatos .jpg o jpeg.</strong></p>

    <br>
    <div class="form-group">
        <?php echo $form->labelEx($modelSSUnidadesReceptoras,'sello_empresa'); ?>
        <?php echo $form->fileField($modelSSUnidadesReceptoras,'sello_empresa',array('class'=>'form-control')); ?>
        <?php echo $form->error($modelSSUnidadesReceptoras,'sello_empresa'); ?>
    </div>

    <br>
    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSUnidadesReceptoras->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('listaUnidadesReceptoras'), array('class'=>'btn btn-danger')) ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->