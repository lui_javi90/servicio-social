<?php 
/* @var $this UnidadesReceptorasController */
/* @var $model UnidadesReceptoras */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Unidades Receptoras' => array('ssUnidadesReceptoras/listaUnidadesReceptoras'),
    'Cargar Logo Empresa'
);
?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Cargar Logo Empresa <br><br><b><?php echo $modelSSUnidadesReceptoras->nombre_unidad_receptora; ?></b>
		</span>
	</h2>
</div>

<br><br>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Cargar Logo 
                </h6>
            </div>
            <div class="panel panel-body">
                <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'logo-unidades-receptoras-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                        //Para que el formulario permita la subida de archivos
                        'htmlOptions' => array('autocomplete'=>'off', 'enctype'=>'multipart/form-data')
                    )); ?>

                        <?php echo $form->errorSummary($modelSSUnidadesReceptoras); ?>

                        <p align="center"><strong>El logo solo es OPCIONAL.</strong></p>

                        <br>
                        <p><strong>NOTA:</strong> El logo debe cumplir con los requisitos definidos.</p>

                        <p><strong>* El nombre del archivo debe ser de maximo 15 caracteres.</strong></p>
                        <p><strong>* Tamaño maximo de la imagen de 5MB.</strong></p>
                        <p><strong>* Solo formatos .png .jpg o jpeg.</strong></p>
                        
                        <br>
                        <div class="form-group">
                            <?php echo $form->labelEx($modelSSUnidadesReceptoras,'logo_unidad_receptora'); ?>
                            <?php echo $form->fileField($modelSSUnidadesReceptoras,'logo_unidad_receptora',array('class'=>'form-control')); ?>
                            <?php echo $form->error($modelSSUnidadesReceptoras,'logo_unidad_receptora'); ?>
                        </div>

                        <br>
                        <div class="form-group">
                            <?php echo CHtml::submitButton($modelSSUnidadesReceptoras->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                            <?php echo CHtml::link('Cancelar', array('listaUnidadesReceptoras'), array('class'=>'btn btn-danger')); ?>
                        </div>

                    <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Vista Prevía
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong>Logo actual</strong></p>
                <br><br>
                <?php 
                    echo ($modelSSUnidadesReceptoras->logo_unidad_receptora === NULL) ? '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/logo_empresa_default.png"/>' : '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/encabezados_empresas/'.$modelSSUnidadesReceptoras->path_carpeta.'/'.$modelSSUnidadesReceptoras->logo_unidad_receptora.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>