<?php
/* @var $this SsServiciosSocialesAnterioresController */
/* @var $model SsServiciosSocialesAnteriores */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-servicios-sociales-anteriores-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSServiciosSocialesAnteriores); ?>

	<?php if($modelSSServiciosSocialesAnteriores->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'no_ctrl'); ?>
		<?php $this->widget('ext.select2.ESelect2',array('model'=>$modelSSServiciosSocialesAnteriores,
														'attribute'=>'no_ctrl',
														'data' => CHtml::listData(EDatosAlumno::model()->findAll(array('order'=>" \"nctrAlumno\" ASC",'condition'=>" \"nctrAlumno\" is not null AND \"statAlumno\" = 'VI' ", 'params'=>array()))
																,'nctrAlumno','nctrAlumno'),
														'options'=>array(
														'width' => '100%',
													    'placeholder'=>'Buscar Alumno por No. de Control',
													    'allowClear'=>true,
													  ),
														'htmlOptions'=>array(
														'size' => '8',			// textField size
														'maxlength' => '8',	// textField maxlength
														//'class' => 'form-control chosen-select',
														'required' => 'required',
														),
												)); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'no_ctrl'); ?>
	</div>
<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'no_ctrl'); ?>
		<?php $this->widget('ext.select2.ESelect2',array(
														'model'=>$modelSSServiciosSocialesAnteriores,
														'attribute'=>'no_ctrl',
														'data'=>CHtml::listData(EDatosAlumno::model()->findAll(array('order'=>" \"nctrAlumno\" ASC",'condition'=>" \"nctrAlumno\" is not null AND \"statAlumno\" = 'VI' ", 'params'=>array()))
																,'nctrAlumno','nctrAlumno'),
														'options'=>array(
														'width' => '100%',
													    'placeholder'=>'Buscar Alumno por No. de Control',
													    'allowClear'=>true,
													  ),
														'htmlOptions'=>array(
														'size' => '8',			// textField size
														'maxlength' => '8',	// textField maxlength
														//'class' => 'form-control chosen-select',
														'required' => 'required',
														'disabled' => 'disabled'
														),
												)); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'no_ctrl'); ?>
	</div>
<?php } ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'nombre_programa'); ?>
		<?php echo $form->textField($modelSSServiciosSocialesAnteriores,'nombre_programa',array('size'=>60,'maxlength'=>150, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'nombre_programa'); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'nombre_empresa'); ?>
        <?php echo $form->textField($modelSSServiciosSocialesAnteriores,'nombre_empresa',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSServiciosSocialesAnteriores,'nombre_empresa'); ?>
    </div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'id_periodo_programa'); ?>
		<?php echo $form->dropDownList($modelSSServiciosSocialesAnteriores,
																		'id_periodo_programa',
																		$lista_periodos_programas,
																		array('prompt'=>'--Selecionar Periodo Programa--', 'class'=>'form-control', 'required'=>'required')
																	); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'id_periodo_programa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'nombre_completo_supervisor'); ?>
		<?php echo $form->textField($modelSSServiciosSocialesAnteriores,'nombre_completo_supervisor',array('size'=>60,'maxlength'=>80, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'nombre_completo_supervisor'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'cargo_supervisor'); ?>
		<?php echo $form->textField($modelSSServiciosSocialesAnteriores,'cargo_supervisor',array('size'=>60,'maxlength'=>100, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'cargo_supervisor'); ?>
	</div>

	<?php if($modelSSServiciosSocialesAnteriores->isNewRecord){?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'horas_liberadas'); ?>
		<?php echo $form->textField($modelSSServiciosSocialesAnteriores,'horas_liberadas',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'horas_liberadas'); ?>
	</div>
<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'horas_liberadas'); ?>
		<?php echo $form->textField($modelSSServiciosSocialesAnteriores,'horas_liberadas',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')); ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'horas_liberadas'); ?>
	</div>
<?php } ?>

<?php if($modelSSServiciosSocialesAnteriores->isNewRecord){?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'fecha_inicio_programa'); ?>
		<?php //echo $form->textField($modelSSServiciosSocialesAnteriores,'fecha_inicio_programa');
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $modelSSServiciosSocialesAnteriores,
				'attribute' => 'fecha_inicio_programa',
				'language' => 'es',
				'theme' => 'softark',
				'options' => array(
					//'showOn' => 'both',				// also opens with a button
					'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
					'showOtherMonths' => true,		// show dates in other months
					'selectOtherMonths' => true,	// can seelect dates in other months
					//'changeYear' => true,			// can change year
					'changeMonth' => true,			// can change month
					'yearRange' => '2018:2025',		// range of year
					//'showButtonPanel' => true,		// show button panel
				),
				'htmlOptions' => array(
					'size' => '10',			// textField size
					'maxlength' => '10',	// textField maxlength
					'class' => 'form-control',
					'required' => 'required',
					'readOnly' => true
					//'disabled' => 'disabled'
				),
			));  ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'fecha_inicio_programa'); ?>
	</div>
<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'fecha_inicio_programa'); ?>
		<?php //echo $form->textField($modelSSServiciosSocialesAnteriores,'fecha_inicio_programa');
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $modelSSServiciosSocialesAnteriores,
				'attribute' => 'fecha_inicio_programa',
				'language' => 'es',
				'theme' => 'softark',
				'options' => array(
					//'showOn' => 'both',				// also opens with a button
					'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
					'showOtherMonths' => true,		// show dates in other months
					'selectOtherMonths' => true,	// can seelect dates in other months
					//'changeYear' => true,			// can change year
					'changeMonth' => true,			// can change month
					'yearRange' => '2018:2025',		// range of year
					//'showButtonPanel' => true,		// show button panel
				),
				'htmlOptions' => array(
					'size' => '10',			// textField size
					'maxlength' => '10',	// textField maxlength
					'class' => 'form-control',
					'required' => 'required',
					'readOnly' => true,
					'disabled' => 'disabled'
				),
			));  ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'fecha_inicio_programa'); ?>
	</div>
<?php } ?>

<?php if($modelSSServiciosSocialesAnteriores->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'fecha_fin_programa'); ?>
		<?php //echo $form->textField($modelSSServiciosSocialesAnteriores,'fecha_fin_programa');
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $modelSSServiciosSocialesAnteriores,
				'attribute' => 'fecha_fin_programa',
				'language' => 'es',
				'theme' => 'softark',
				'options' => array(
					//'showOn' => 'both',				// also opens with a button
					'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
					'showOtherMonths' => true,		// show dates in other months
					'selectOtherMonths' => true,	// can seelect dates in other months
					//'changeYear' => true,			// can change year
					'changeMonth' => true,			// can change month
					'yearRange' => '2018:2025',		// range of year
					//'showButtonPanel' => true,		// show button panel
				),
				'htmlOptions' => array(
					'size' => '10',			// textField size
					'maxlength' => '10',	// textField maxlength
					'class' => 'form-control',
					'required' => 'required',
					'readOnly' => true
					//'disabled' => 'disabled'
				),
			));  ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'fecha_fin_programa'); ?>
	</div>
<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'fecha_fin_programa'); ?>
		<?php //echo $form->textField($modelSSServiciosSocialesAnteriores,'fecha_fin_programa');
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $modelSSServiciosSocialesAnteriores,
				'attribute' => 'fecha_fin_programa',
				'language' => 'es',
				'theme' => 'softark',
				'options' => array(
					//'showOn' => 'both',				// also opens with a button
					'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
					'showOtherMonths' => true,		// show dates in other months
					'selectOtherMonths' => true,	// can seelect dates in other months
					//'changeYear' => true,			// can change year
					'changeMonth' => true,			// can change month
					'yearRange' => '2018:2025',		// range of year
					//'showButtonPanel' => true,		// show button panel
				),
				'htmlOptions' => array(
					'size' => '10',			// textField size
					'maxlength' => '10',	// textField maxlength
					'class' => 'form-control',
					'required' => 'required',
					'readOnly' => true,
					'disabled' => 'disabled'
				),
			));  ?>
		<?php echo $form->error($modelSSServiciosSocialesAnteriores,'fecha_fin_programa'); ?>
	</div>
<?php } ?>

<?php if($modelSSServiciosSocialesAnteriores->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'calificacion_servicio_social'); ?>
        <?php echo $form->textField($modelSSServiciosSocialesAnteriores,'calificacion_servicio_social',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSServiciosSocialesAnteriores,'calificacion_servicio_social'); ?>
  </div>
<?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSServiciosSocialesAnteriores,'calificacion_servicio_social'); ?>
        <?php echo $form->textField($modelSSServiciosSocialesAnteriores,'calificacion_servicio_social',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')); ?>
        <?php echo $form->error($modelSSServiciosSocialesAnteriores,'calificacion_servicio_social'); ?>
  </div>
<?php } ?>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSServiciosSocialesAnteriores->isNewRecord ? 'Guardar' : 'Guadar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaServiciosSocialesAnteriores'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
