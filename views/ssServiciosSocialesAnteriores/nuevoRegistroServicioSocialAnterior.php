<?php
/* @var $this SsServiciosSocialesAnterioresController */
/* @var $model SsServiciosSocialesAnteriores */

if($modelSSServiciosSocialesAnteriores->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Servicios Sociales Anteriores' => array('SsServiciosSocialesAnteriores/listaServiciosSocialesAnteriores'),
		'Nuevo Registro Servicio Social Anterior'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Servicios Sociales Anteriores' => array('SsServiciosSocialesAnteriores/listaServiciosSocialesAnteriores'),
		'Editar Registro Servicio Social Anterior'
	);
}

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSServiciosSocialesAnteriores->isNewRecord) ? "Nuevo Registro Servicio Social Anterior" : "Editar Registro Servicio Social Anterior" ; ?>
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		<b>Para poder registrar un Servicio Social pasado a un alumno se debe de haber dado de ALTA el alumno para Servicio Social. 
		</b>
    </strong></p>
</div>

<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		<b>Es muy IMPORTANTE establecer correctamente las horas liberadas y la calificación obtenido del Servicio Social pasado del alumno porque afectaran 
		directamente la CALIFICACIÓN FINAL de Servicio Social del alumno. </b>
    </strong></p>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSServiciosSocialesAnteriores->isNewRecord) ? "Nuevo Registro" : "Editar Registro" ; ?>
				</h6>
			</div>
			<div class="panel-body">
					<?php $this->renderPartial('_formRegistroServicioSocialAnterior', array('modelSSServiciosSocialesAnteriores'=>$modelSSServiciosSocialesAnteriores,
																							'lista_periodos_programas' => $lista_periodos_programas
																					)); ?>
			</div>
		</div>
	</div>
</div>
