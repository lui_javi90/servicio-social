<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Servicios Sociales Anteriores' => array('ssServiciosSocialesAnteriores/listaServiciosSocialesAnteriores'),
    'Detalle Servicio Social Anterior'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Servicio Social Anterior
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Información Servicio Social</h6>
			</div>
			<div class="panel-body">
				<div style="padding: 50px; text-align: center;" class="col-lg-4" align="center">
						<!--Es un Web Service-->
						<img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $modelSSServiciosSocialesAnteriores->no_ctrl; ?>" alt="foto alumno" height="200">
						<!--Es un Web Service-->
				</div>
				<div class="col-lg-8" align="left">
                <p><b>Nombre del Alumno:</b>
                &nbsp;&nbsp;<?php echo $nombre_alumno; ?></p>

                <p><b>Carrera:</b>
                &nbsp;&nbsp;<?php echo $carrera; ?></p>

                <p><b>Semestre:</b>
                &nbsp;&nbsp;<?php echo $semestre; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServiciosSocialesAnteriores->getAttributeLabel('no_ctrl')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelSSServiciosSocialesAnteriores->no_ctrl;?></p>

                <p><b>Nombre del Programa:</b>
                &nbsp;&nbsp;<?php echo $modelSSServiciosSocialesAnteriores->nombre_programa; ?></p>

                <p><b>Periodo del Programa:</b>
                &nbsp;&nbsp;<?php
                switch($modelSSServiciosSocialesAnteriores->id_periodo_programa)
                {
                  case 1: echo "SEMESTRAL"; break;
                  case 2: echo "ANUAL"; break;
                  case 3: echo "ESPECIAL"; break;
                }
                ?></p>

                <p><b>Nombre del Supervisor:</b>
                &nbsp;&nbsp;<?php echo $modelSSServiciosSocialesAnteriores->nombre_completo_supervisor; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServiciosSocialesAnteriores->getAttributeLabel('cargo_supervisor')); ?>:</b>
                &nbsp;&nbsp;<?php echo $modelSSServiciosSocialesAnteriores->cargo_supervisor; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServiciosSocialesAnteriores->getAttributeLabel('fecha_inicio_programa')); ?>:</b>
                &nbsp;&nbsp;<?php echo $fec_ini_programa; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServiciosSocialesAnteriores->getAttributeLabel('fecha_fin_programa')); ?>:</b>
                &nbsp;&nbsp;<?php echo $fec_fin_programa; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServiciosSocialesAnteriores->getAttributeLabel('horas_liberadas')); ?>:</b>
                &nbsp;&nbsp;<?php echo '<span style="font-size:18px" class="label label-default">'.$modelSSServiciosSocialesAnteriores->horas_liberadas.'</span>'; ?></p>

                <p><b>Calificación del Servicio Social:</b>
                &nbsp;&nbsp;<?php echo ($modelSSServiciosSocialesAnteriores->calificacion_servicio_social >=70) ?
                                '<b><span style="font-size:18px" class="label label-success">'.$modelSSServiciosSocialesAnteriores->calificacion_servicio_social.'</span></b>' :
                                '<b><span style="font-size:18px" class="label label-danger">'.$modelSSServiciosSocialesAnteriores->calificacion_servicio_social.'</span></b>'; ?>
        </div>
			</div>

        <div align="center">
            <?php //echo CHtml::link('Volver al menú de Servicio Sociales Anteriores', array('listaServiciosSocialesAnteriores'), array('class'=>'btn btn-success')); ?>
        </div>
      <br>
		</div>
	</div>
</div>
