<?php
/* @var $this SsServiciosSocialesAnterioresController */
/* @var $model SsServiciosSocialesAnteriores */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Servicios Sociales Anteriores',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Servicios Sociales Anteriores
		</span>
	</h2>
</div>

<br><br><br>
<div style="float: right;">
	<?php echo ($hayRegistroFechasServicioSocial == true) ? CHtml::link('Registrar Servicio Social', array('nuevoRegistroServicioSocialAnterior'), array('class'=>'btn btn-success')) : ''; ?>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
					En esta parte podrás agregar los Servicios Sociales del alumno, anteriores al Módulo de Servicio Social.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-servicios-sociales-anteriores-grid',
	'dataProvider'=>$modelSSServiciosSocialesAnteriores->search(),
	'filter'=>$modelSSServiciosSocialesAnteriores,
	'columns'=>array(
		//'id_servicio_social_anterior',
		array(
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'name'=>'nombre_programa',
			'filter'=>false,
			'htmlOptions'=> array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'name'=>'idPeriodoPrograma.periodo_programa',
			'filter'=> false,
			'htmlOptions'=>array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name'=>'nombre_completo_supervisor',
			'filter'=> false,
			'htmlOptions'=> array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'name'=>'cargo_supervisor',
			'filter'=> false,
			'htmlOptions'=>array('width'=>'120px', 'class'=>'text-center')
		),
		array(
			'name'=>'horas_liberadas',
			'filter'=> false,
			'htmlOptions'=>array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name'=>'fecha_inicio_programa',
			'filter'=> false,
			'htmlOptions'=>array('width'=>'60px', 'class'=>'text-center')
		),
		array(
			'name'=>'fecha_fin_programa',
			'filter'=> false,
			'htmlOptions'=>array('width'=>'60px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editServSocialAnterior}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editServSocialAnterior' => array
				(
					'label'=>'Editar Servicio Social Anterior',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServiciosSocialesAnteriores/editarRegistroServicioSocialAnterior", array("id_servicio_social_anterior"=>$data->id_servicio_social_anterior))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detServSocialAnterior}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detServSocialAnterior' => array
				(
					'label'=>'Editar Servicio Social Anterior',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssServiciosSocialesAnteriores/detalleServicioSocialAnterior", array("id_servicio_social_anterior"=>$data->id_servicio_social_anterior))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
	),
)); ?>
