<?php
/* @var $this ActividadesServicioSocialController */
/* @var $model ActividadesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Mis Actividades de Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Mis Actividades de Servicio Social
		</span>
	</h2>
</div>

<br><br>
<div class="right">
    <?php echo ($ver_reporte == true) ? CHtml::link('Ver Reporte de Actividades', array('imprimirPlanDeTrabajoServicioSocial','id_servicio_social'=>$id_servicio_social), array('class'=>'btn btn-success')) : "" ; ?>
</div>

<br><br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Las actividades que aparecen a continuación seran establecidas por el Supervisor del Programa
		en el que estas realizando tu Servicio Social. En estas se establece las actividades que estaras
		realizando cada mes que dure tu Servicio Social.
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alum-actividades-servicio-social-grid',
	'dataProvider'=>$modelSSActividadesServicioSocial->searchXActividadesAlumno($id_servicio_social),
	'filter'=>$modelSSActividadesServicioSocial,
	'columns'=>array(
		//'id_actividad_servicio_social',
		array(
			'name' => 'id_mes',
			'type' => 'raw',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetMeses.php';
				$mes = GetMeses::getMes($data->id_mes);

				return '<span style="font-size:14px" class="label label-success center">'.$mes.'</span>';
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name' => 'actividad_mensual',
			'filter' => false,
			'htmlOptions' => array('width'=>'450px', 'class'=>'text-center')
		),
	),
)); ?>

<br><br><br>
<div align="center">
	<?php //echo CHtml::link('Volver al Menú Principal', array('/'), array('class'=>'btn btn-success')); ?>
</div>
