<style>
    .hoja{
        background-color: transparent;
    }
    /*table, th, td, tr
    {
        border: 1px solid black; solid black
        border-collapse: collapse;
        color: #000000;
        height:7%;
        text-align: left;
    }*/
    .table1{
        border: 1px solid black; /*solid black*/
        border-collapse: collapse;
        color: #000000;
        height:7%;
        text-align: left;
    }
    .td1{
        border: 1px solid black;
        color: #000000;
        height:5;
        text-align: left;
    }
    .th1{
        border: 1px solid black;
        color: #000000;
        height:7%
        text-align: left;
    }
    .div2{
        background-color: transparent;
        padding: -4px;
        padding-left: -1px;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
        font-family: itali
    }
    .letra4{
        font-size: 9px;
    }
    .letra4{
        font-size: 9px;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    .mytable{
      border: 1px solid black;
      border-right: 1px solid black;
      border-collapse: collapse;
      color: #000000;
      width: 100%;
      background-color: #FFFFFF;
    }
    .mytable-head {
      border: 1px solid black;
      color: #000000;
      width: 100%;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .mytable-body {
      border: 1px solid black;
      border-top: 0;
      margin-top: 0;
      padding-top: 0;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .border-bug {
      border-left: 2px dotted black;
      border-top: 2px dotted black;
      border-bottom: 2px dotted black;
      border-right: 2px dotted black;
    }
</style>
<div class="hoja">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />
    <!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/banner_top_itc.jpg" />-->
    <?php //echo '<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/banner_superior_'.trim($datos_alum_plan_trabajo[0]['no_empresa']).'/'.$banners->banner_superior.'"/>'; ?>
    <br>
    <!--DATOS DEL ENCABEZADO-->
    <div class="div2">
        <h6 class="center letra1">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="center letra1">OFICINA DE SERVICIO SOCIAL</h6>
        <h6 class="center bold letra1">PLAN DE TRABAJO PARA LA REALIZACIÓN DEL SERVICIO SOCIAL</h6>
        <hr>
    </div>
    <!--DATOS DEL ENCABEZADO-->

    <!--DATOS DEL ALUMNO DEL SERVICIO SOCIAL-->
    <br>
    <div class="div2">
        <table class="mytable-head" align="center">
            <tr class="left" width="100%">
                <td bgcolor="#9E9E9E" class="td1 bold letra1 left" width="28%">
                    Nombre del estudiante:
                </td>
                <td class="td1 letra1 left" width="72%">
                    <?php echo $datos_alum_plan_trabajo[0]['namealumno']; ?>
                </td>
            <tr>
        </table>
        <table class="mytable-head" align="center">
            <tr class="left" width="100%">
                <td bgcolor="#9E9E9E" class="td1 bold letra1 left" width="15%">
                    Carrera:
                </td>
                <td class="td1 letra1 left" width="45%">
                    <?php echo $datos_alum_plan_trabajo[0]['carrera']; ?>
                </td>
                <td bgcolor="#9E9E9E" class="td1 bold letra1 left" width="25%">
                    Numero de Control:
                </td>
                <td class="td1 letra1 left" width="15%">
                    <?php echo $datos_alum_plan_trabajo[0]['no_ctrl']; ?>
                </td>
            <tr>
        </table>
        <table class="mytable-head" align="center">
            <tr class="left" width="100%">
                <td bgcolor="#9E9E9E" class="td1 bold letra1 left" width="28%">
                    Nombre del Programa o Proyecto:
                </td>
                <td class="td1 letra1 left" width="72%">
                    <?php echo $datos_alum_plan_trabajo[0]['nombre_programa']; ?>
                </td>
            <tr>
        </table>
        <table class="mytable-head" align="center">
            <tr class="left" width="100%">
                <td bgcolor="#9E9E9E" class="td1 bold letra1 left" width="18%">
                    Objetivo:
                </td>
                <td class="td1 letra1 left" width="82%">
                    <?php echo $datos_alum_plan_trabajo[0]['descripcion_objetivo_programa']; ?>
                </td>
            <tr>
        </table>
        <table class="mytable-head" align="center">
            <tr class="left" width="100%">
                <td bgcolor="#9E9E9E" class="td1 bold letra1 left" width="20%">
                    Fecha de Inicio:
                </td>
                <td class="td1 letra1 left" width="80%">
                    <?php echo $fecha_inicio; ?>
                </td>
            <tr>
        </table>
    </div>
    <!--DATOS DEL ALUMNO DEL SERVICIO SOCIAL-->

    <!--DATOS ACTIVIDADES DEL SERVICIO SOCIAL-->
    <br><br><br>
    <div class="div2">
        <table class="mytable">
            <tr bgcolor="#9E9E9E" class="bold" width="100%">
                <th align="center" class="th1 letra3" width="20%">
                    Meses:
                </th>
                <th align="center" class="th1 letra3" width="80%">
                    Actividad:
                </th>
            <tr>
        <?php
        require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetMeses.php';
        $contador1 = 0;
        for($i=0 ; $i < count($datos_actividades); $i++){
            $contador1++;
            ?>
            <tr class="left" width="100%">
                <td class="td1 bold letra1 left" width="20%">
                    <?php echo GetMeses::getMes($datos_actividades[$i]['id_mes']); ?>
                </td>
                <td class="td1 bold letra1 left" width="80%">
                    <?php echo $datos_actividades[$i]['actividad_mensual']; ?>
                </td>
            </tr>
        <?php } ?>
        </table>
    </div>
    <!--DATOS ACTIVIDADES DEL SERVICIO SOCIAL-->
</diV>