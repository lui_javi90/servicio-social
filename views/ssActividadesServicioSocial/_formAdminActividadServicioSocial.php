<?php
/* @var $this ActividadesServicioSocialController */
/* @var $model ActividadesServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-actividades-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>"off"),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSActividadesServicioSocial); ?>

	<div class="form-group">
        <?php echo "<b>Mes Correspondiente: </b>".$mes; ?>
    </div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSActividadesServicioSocial,'actividad_mensual'); ?>
		<?php echo $form->textField($modelSSActividadesServicioSocial,'actividad_mensual',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSActividadesServicioSocial,'actividad_mensual'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSActividadesServicioSocial->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('actividadesServicioSocial/listaAdminActividadesServicioSocial','id_servicio_social'=>$modelSSActividadesServicioSocial->id_servicio_social), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
