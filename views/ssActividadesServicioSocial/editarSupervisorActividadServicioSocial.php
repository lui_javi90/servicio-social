<?php
/* @var $this ActividadesServicioSocialController */
/* @var $model ActividadesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Actividades Servicio Social'=>array('ssActividadesServicioSocial/listaSupervisorActividadesServicioSocial','id_servicio_social'=>$modelSSActividadesServicioSocial->id_servicio_social),
	'Agregar Actividades Servicio Social' => array('ssActividadesServicioSocial/listaSupervisorActividadesServicioSocial', 'id_actividad_servicio_social' => $modelSSActividadesServicioSocial->id_actividad_servicio_social, 'id_servicio_social'=>$modelSSActividadesServicioSocial->id_servicio_social),
	'Agregar Actividad',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Agregar Actividad
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Configuración</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formSupervisorActividadServicioSocial', array(
										   'modelSSActividadesServicioSocial'=>$modelSSActividadesServicioSocial
									)); ?>	
			</div>
		</div>
	</div>
</div>