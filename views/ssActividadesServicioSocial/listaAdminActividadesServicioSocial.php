<?php
/* @var $this ActividadesServicioSocialController */
/* @var $model ActividadesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Actividades Servicio Social' => array('ssServicioSocial/listaAdminActividades'),
	'Actividades Alumno Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Actividades Alumno Servicio Social
		</span>
	</h2>
</div>


<br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admin-actividades-servicio-social-grid',
	'dataProvider' => $modelSSActividadesServicioSocial->searchXActividadesAlumno($id_servicio_social),
	'filter' => $modelSSActividadesServicioSocial,
	'columns'=>array(
		/*'id_actividad_servicio_social',
		array(//Filtrar las actividades por el no. control del alumno
			'name' => 'idServicioSocial.no_ctrl',
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),*/
		array(
			'name' => 'id_mes',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetMeses.php';
				$mes = GetMeses::getMes($data->id_mes);

				return $mes;
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name' => 'actividad_mensual',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editPlanTrabajo}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editPlanTrabajo' => array
				(
					'label'=>'Editar Actividad',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssActividadesServicioSocial/editarAdminActividadServicioSocial", array("id_actividad_servicio_social"=>$data->id_actividad_servicio_social, "id_servicio_social"=>'.$id_servicio_social.'))',
					'imageUrl'=>'images/editar_32.png',
					//'visible' =>'$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
			),
		),
	),
)); ?>


