<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Menu Criterios del Reporte Bimestral' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
    'Establecer Orden de los Criterios' => array('ssCriteriosAEvaluar/ordenarCriteriosReporteBimestral'),
    'Asignar Orden al Criterio' 
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            <?php echo ($modelSSCriteriosAEvaluar->isNewRecord) ? 'Nueva Posición del Criterio' : 'Editar Posición del Criterio'; ?>
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
					Posición del Criterio
				</h6>
			</div>
			<div class="panel-body">
			<?php if($modelSSCriteriosAEvaluar->id_tipo_criterio == 1){ ?>
				<?php $this->renderPartial('_formAsignarPosicionCriterio', array(
                                            'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
                                            'posicionesSupervisor' => $posicionesSupervisor,
                                                                    )); ?>
			<?php }else{ ?>

			<?php $this->renderPartial('_formAsignarPosicionCriterio', array(
                                       'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
                                        'posicionesAdmin' => $posicionesAdmin,
                                                                    )); ?>

			<?php } ?>
			</div>
		</div>
	</div>
</div>