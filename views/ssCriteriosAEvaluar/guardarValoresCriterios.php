<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

if($modelSSCriteriosAEvaluar->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
        'Criterios Evaluación Reportes Bimestrales' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
        'Asignar Valores a Criterios' => array('ssCriteriosAEvaluar/asignarValoresCriteriosReporteBimestral'),
		'Nuevo Criterio Reporte Bimestral'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
        'Criterios Evaluación Reportes Bimestrales' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
        'Asignar Valores a Criterios' => array('ssCriteriosAEvaluar/asignarValoresCriteriosReporteBimestral'),
		'Editar Criterio Reporte Bimestral'
	);
}
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSCriteriosAEvaluar->isNewRecord) ? 'Nuevo Valor del Criterio' : 'Editar Valor del Criterio'; ?>
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
					Valor (A) del Criterio
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formValorACriterio', array(
                                                                    'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
                                                                    'valorA' => $valorA
                                                                    )); ?>
			</div>
		</div>
	</div>
</div>