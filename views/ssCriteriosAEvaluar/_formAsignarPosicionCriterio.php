<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ss-criterios-posicion-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'htmlOptions' => array('autocomplete'=>'off'),
    'enableAjaxValidation'=>false,
)); ?>

    <!--<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>-->
    <?php echo $form->errorSummary($modelSSCriteriosAEvaluar); ?>

    <div class="form-group">
        <?php echo "Descripcion del Criterio: "."<b>".$modelSSCriteriosAEvaluar->descripcion_criterio."</b>"; ?>
        <?php echo "<br>"; ?>
        <?php echo "Valor (A) del Criterio: "."<b>".$modelSSCriteriosAEvaluar->valor_a."</b>"; ?>
        <?php echo "<br>"; ?>
        <?php echo ($modelSSCriteriosAEvaluar->id_tipo_criterio == 1) ? "Tipo de Criterio: "."<b>"."CRITERIO DEL SUPERVISOR"."</b>" : "Tipo de Criterio: "."<b>"."CRITERIO DEL DEPARTAMENTO"."</b>"; ?>
    </div>

    <?php if($modelSSCriteriosAEvaluar->id_tipo_criterio == 1){  ?>
    <div class="form-group">
        <?php echo $form->labelEx($modelSSCriteriosAEvaluar,'posicion_criterio'); ?>
        <?php echo $form->dropDownList($modelSSCriteriosAEvaluar,
                                    'posicion_criterio',
                                    $posicionesSupervisor,
                                    array('prompt'=>'--Selecciona Posición--', 'class'=>'form-control', 'required'=>'required')
                                    ); ?>
        <?php echo $form->error($modelSSCriteriosAEvaluar,'posicion_criterio'); ?>
    </div>
    <?php }else{?>
        <div class="form-group">
        <?php echo $form->labelEx($modelSSCriteriosAEvaluar,'posicion_criterio'); ?>
        <?php echo $form->dropDownList($modelSSCriteriosAEvaluar,
                                    'posicion_criterio',
                                    $posicionesAdmin,
                                    array('prompt'=>'--Selecciona Posición--', 'class'=>'form-control', 'required'=>'required')
                                    ); ?>
        <?php echo $form->error($modelSSCriteriosAEvaluar,'posicion_criterio'); ?>
    </div>
    <?php } ?>

    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSCriteriosAEvaluar->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('ordenarCriteriosReporteBimestral'), array('class'=>'btn btn-danger')); ?> 
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->