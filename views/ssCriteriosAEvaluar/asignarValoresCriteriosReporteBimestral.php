<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Menu Criterios del Reporte Bimestral' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
    'Formato de Reporte Bimestral'
);

?>

<script>
    /*$('#asigsup-criterios-aevaluar-grid a.save-ajax-button').live('click', function(e)
    {
        var row = $(this).parent().parent();

        var data = $('input', row).serializeObject();

        $.ajax({
            type: 'POST',
            data: data,
            url: jQuery(this).attr('href'),
            success: function(data, textStatus, jqXHR) {
                console.log(data);
                console.log(textStatus);
                console.log(jqXHR);
            },
            error: function(textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
        return false;
    });

    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };*/
</script>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Criterios del Reporte Bimestral
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Criterios 
                </h6>
            </div>
            <div class="panel-body">

                <div class="row">
                    <h2 class="subTitulo" align="center">
                        <span class="subTitulo_inside">
                            Criterios del Supervisor para el Reporte Bimestral
                        </span>
                    </h2>
                </div>

                <hr>

                <div align="right">
                    <?php //echo CHtml::link('Establecer Valores Default', array('establecerValoresDefault'), array('class'=>'btn btn-danger')); ?>
                </diV>

                <br><br><br>
                <h6 align="center"><b>Porcentaje de Progreso del Valor Total de los Criterios:</b></h6>
                <div class="progress">
                    <?php if($valorATotalCriterios < 100){ ?>
                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $valorATotalCriterios; ?>" aria-valuemin="0" aria-valuemax="100" 
                    style="width: <?php echo $valorATotalCriterios."%"; ?>">

                    <?php echo $valorATotalCriterios."%"; ?>
                    </div>
                    <?php }else{?>
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $valorATotalCriterios; ?>" aria-valuemin="0" aria-valuemax="100" 
                    style="width: <?php echo $valorATotalCriterios."%"; ?>">

                    <?php echo $valorATotalCriterios."%"; ?>
                    </div>
                    <?php } ?>
                </div>

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'asigsup-criterios-aevaluar-grid',
                    'dataProvider'=> $modelSSCriteriosAEvaluar->searchCriteriosXSupervisor(),
                    //'filter'=>$modelSSCriteriosAEvaluar,
                    'columns'=>array(
                        /*array(
                            'name' => 'id_criterio',
                            'htmlOptions' => array('width'=>'5px', 'class'=>'text-center')
                        ),*/
                        array(
                            'name' => 'descripcion_criterio',
                            'filter' => false,
                            'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
                        ),
                        array(
                            'header' => 'Valor (A)',
                            'name'=>'valor_a',
                            'filter' => false,
                            //'value' => 'CHtml::textField("SsCriteriosAEvaluar[valor_a]", $data->valor_a)',
                            'htmlOptions' => array('width'=>'75px', 'class'=>'text-center')
                        ),
                        array(
                            'header' => 'Tipo de Criterio',
                            'value' => function($data)
                            {
                                return ($data->id_tipo_criterio == 1) ? "CRITERIO DEL SUPERVISOR" : "CRITERIO DEL DEPARTAMENTO";
                            },
                            'filter' => false,
                            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
                        ),
                        array(
                            'header' => 'Estatus',
                            'filter' => false,
                            'type' => 'raw',
                            'value' => function($data)
                            {
                                return ($data->status_criterio == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
                            },
                            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                        ),
                        array(
                            'class' => 'CButtonColumn',
                            'template' => '{agregarValorA}, {asignado}',
                            'header'=>'Asignar Valor A',
                            'htmlOptions'=>array('width:90px', 'class'=>'text-center'),
                            'buttons' => array(
                                'agregarValorA' => array
                                (
                                    'label'=>'Agregar Valor del Criterio',
                                    'url' => 'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/guardarValoresCriterios", array("id_criterio"=>$data->id_criterio))',
                                    'imageUrl'=>'images/servicio_social/agregar_32.png',
                                    'visible' => "'$valorATotalCriterios' < 100",
                                ),
                                'asignado' => array
                                (
                                    'label'=>'Valor (A) del Criterio Asignado',
                                    //'url' => '#',
                                    'imageUrl'=>'images/servicio_social/aprobado_32.png',
                                    'visible' => "'$valorATotalCriterios' == 100",
                                ),
                            ),
                        ),
                    ),
                )); ?>

                <div class="row">
                    <h2 class="subTitulo" align="center">
                        <span class="subTitulo_inside">
                            Criterios del Admin para el Reporte Bimestral
                        </span>
                    </h2>
                </div>

                <hr>

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'asigadm-criterios-aevaluar-grid',
                    'dataProvider'=>$modelSSCriteriosAEvaluar->searchCriteriosXAdmin(),
                    'filter'=>$modelSSCriteriosAEvaluar,
                    'columns'=>array(
                        /*array(
                            'name' => 'id_criterio',
                            'htmlOptions' => array('width'=>'5px', 'class'=>'text-center')
                        ),*/
                        array(
                            'name' => 'descripcion_criterio',
                            'filter' => false,
                            'htmlOptions' => array('width'=>'300px', 'class'=>'left-center')
                        ),
                        array(
                            'header' => 'Valor (A)',
                            'name'=>'valor_a',
                            'filter' => false,
                            'htmlOptions' => array('width'=>'75px', 'class'=>'text-center')
                        ),
                        array(
                            'header' => 'Tipo de Criterio',
                            'value' => function($data)
                            {
                                return ($data->id_tipo_criterio == 1) ? "CRITERIO DEL SUPERVISOR" : "CRITERIO DEL DEPARTAMENTO";
                            },
                            'filter' => false,
                            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
                        ),
                        array(
                            'header' => 'Estatus',
                            'filter' => false,
                            'type' => 'raw',
                            'value' => function($data)
                            {
                                return ($data->status_criterio == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
                            },
                            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                        ),
                        array(
                            'class' => 'CButtonColumn',
                            'template' => '{agregarValorA}, {asignado}',
                            'header'=>'Asignar Valor A',
                            'htmlOptions'=>array('width'=>'90px', 'class'=>'text-center'),
                            'buttons' => array(
                                'agregarValorA' => array(
                                    'label'=>'Agregar Valor del Criterio',
                                    'url' => 'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/guardarValoresCriterios", array("id_criterio"=>$data->id_criterio))',
                                    'imageUrl'=>'images/servicio_social/agregar_32.png',
                                    'visible' => "'$valorATotalCriterios' < 100",
                                ),
                                'asignado' => array(
                                    'label'=>'Valor (A) del Criterio Asignado',
                                    //'url' => '#',
                                    'imageUrl'=>'images/servicio_social/aprobado_32.png',
                                    'visible' => "'$valorATotalCriterios' == 100",
                                ),
                            ),
                        ),
                    ),
                )); ?>

                <br><br>
                <?php if($valorATotalCriterios == 100){ ?>
                <div align="center">
                    <!--Vista previa del Reporte Bimestral despues de asignar los valores de los criterios-->
                    <?php //echo CHtml::link('Vista Previa Reporte Bimestral', array('vistaPreviaReporteBimestral'), array('class'=>'btn btn-info')); ?> 
                    <?php echo CHtml::link('Finalizar y Salir', array('menuCriteriosReporteBimestral'), array('class'=>'btn btn-danger')); ?>
                </div>
                <?php } ?>
                <br>
            </div>
        </div>
    </div>
</div>