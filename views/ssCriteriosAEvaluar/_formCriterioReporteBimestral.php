<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-criterios-aevaluar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('autocomplete'=>'off'),
)); ?>

<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

<br>
<?php echo $form->errorSummary($modelSSCriteriosAEvaluar); ?>

<div class="form-group">
	<?php echo $form->labelEx($modelSSCriteriosAEvaluar,'descripcion_criterio'); ?>
	<?php echo $form->textField($modelSSCriteriosAEvaluar,'descripcion_criterio', array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSCriteriosAEvaluar,'descripcion_criterio'); ?>
</div>

<div class="form-group">
	<?php echo $form->labelEx($modelSSCriteriosAEvaluar,'id_tipo_criterio'); ?>
	<?php echo $form->dropDownList($modelSSCriteriosAEvaluar,
								'id_tipo_criterio',
								array('1'=>'CRITERIO DEL SUPERVISOR', '2'=>'CRITERIO DEL DEPARTAMENTO'),
								array('prompt'=>'--Selecciona Tipo de Criterio--', 'class'=>'form-control', 'required'=>'required')
								); ?>
	<?php echo $form->error($modelSSCriteriosAEvaluar,'id_tipo_criterio'); ?>
</div>

<?php if(!$modelSSCriteriosAEvaluar->isNewRecord){ ?>
<div class="form-group">
	<?php echo $form->labelEx($modelSSCriteriosAEvaluar,'status_criterio'); ?>
	<?php echo $form->checkBox($modelSSCriteriosAEvaluar,'status_criterio'); ?>
	<?php echo $form->error($modelSSCriteriosAEvaluar,'status_criterio'); ?>
</div>
<?php } ?>

<div class="form-group">
	<?php echo CHtml::submitButton($modelSSCriteriosAEvaluar->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::link('Cancelar', array('listaCriteriosReporteBimestral'), array('class'=>'btn btn-danger')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
