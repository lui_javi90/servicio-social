<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

if($modelSSCriteriosAEvaluar->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Ménu Criterios del Reporte Bimestral' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
		'Criterios Evaluación Reportes Bimestrales' => array('listaCriteriosReporteBimestral'),
		'Nuevo Criterio Reporte Bimestral'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Criterios Evaluación Reportes Bimestrales' => array('ssCriteriosAEvaluar/listaCriteriosReporteBimestral'),
		'Editar Criterio Reporte Bimestral'
	);
}
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSCriteriosAEvaluar->isNewRecord) ? 'Nuevo Criterio Reporte Bimestral' : 'Editar Criterio Reporte Bimestral'; ?>
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSCriteriosAEvaluar->isNewRecord) ? 'Nuevo Criterio' : 'Editar Criterio'; ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formCriterioReporteBimestral', array('modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar)); ?>
			</div>
		</div>
	</div>
</div>



