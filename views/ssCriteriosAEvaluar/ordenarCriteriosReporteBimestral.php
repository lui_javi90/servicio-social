<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Menu Criterios del Reporte Bimestral' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
    'Establecer Orden de los Criterios'
);

/*JS PARA QUITAR LA POSICION ASIGNADA ACTUALMENTE A UN CRITERIO DEL SUPERVISOR*/
$js_sup = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#sup-criterios-asignar-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#sup-criterios-asignar-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA QUITAR LA POSICION ASIGNADA ACTUALMENTE A UN CRITERIO*/

/*JS PARA QUITAR LA POSICION ASIGNADA ACTUALMENTE A UN CRITERIO DEL ADMIN*/
$js_adm = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#adm-criterios-asignar-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#adm-criterios-asignar-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA QUITAR LA POSICION ASIGNADA ACTUALMENTE A UN CRITERIO DEL ADMIN*/

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Establecer Orden de los Criterios
		</span>
	</h2>
</div>


<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Ordenar Criterios para el Reporte Bimestral
                </h6>
            </div>
            <div class="panel-body">

            <div class="form-group">
                <br>
                <?php //echo CHtml::link('Establecer Posiciones Default', array('establecerPosicionesDefault'), array('class'=>'btn btn-danger left')); ?>
                <!--Solo se mostrara cuando se haya asigando los valores a los criterios ACTIVOS-->
                <?php //echo CHtml::link('Asignar Posiciones Default', array('asignarPosicionesDefault'), array('class'=>'btn btn-warning right')); ?>
                <br><br><br>
            </div>

            <hr>

            <div class="row">
                <h2 class="subTitulo" align="center">
                    <span class="subTitulo_inside">
                        Ordenar Criterios del Supervisor
                    </span>
                </h2>
            </div>

            <hr>

            <br>
            <div class="alert alert-info">
                <p><strong>
                <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                <b>Si no puedes editar el orden de los criterios es probable que no se haya asignado el Valor (A) a cada Criterio ACTIVO.</b>
                </strong></p>
            </div>

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'sup-criterios-asignar-grid',
                'dataProvider'=>$modelSSCriteriosAEvaluar->searchCriteriosXSupervisor(),
                //'filter'=>$modelSSCriteriosAEvaluar,
                'columns'=>array(
                    //'id_criterio',
                    array(
                        'name' => 'posicion_criterio',
                        'value' => function($data)
                        {
                            return ($data->posicion_criterio == NULL) ? 0 : $data->posicion_criterio;
                        },
                        'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
                    ),
                    array(
                        'name' => 'descripcion_criterio',
                        'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
                    ),
                    array(
                        'header' => 'Valor (A)',
                        'name'=>'valor_a',
                        'filter' => false,
                        'htmlOptions' => array('width'=>'75px', 'class'=>'text-center')
                    ),
                    array(
                        'header' => 'Tipo de Criterio',
                        'value' => function($data)
                        {
                            return ($data->id_tipo_criterio == 1) ? "CRITERIO DEL SUPERVISOR" : "CRITERIO DEL DEPARTAMENTO";
                        },
                        'filter' => false,
                        'htmlOptions' => array('width'=>'125px', 'class'=>'text-center')
                    ),
                    array(
                        'header' => 'Estatus',
                        'filter' => false,
                        'type' => 'raw',
                        'value' => function($data)
                        {
                            return ($data->status_criterio == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
                        },
                        'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                    ),
                    [//inicio
                        'class'                => 'CButtonColumn',
                        'template'             => '{quitarPosicionCriterio}, {noCriterioAsignado}', // buttons here...
                        'header' 			   => 'Quitar Posición Asignada',
                        'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
                        'buttons'              => [ // custom buttons options here...
                            'quitarPosicionCriterio' => [
                                'label'   => 'Quitar posición asignada al Criterio',
                                'url'     => 'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/quitarPosicionActualCriterio", array("id_criterio"=>$data->id_criterio))', // ?r=controller/approve/id/123
                                'imageUrl'=> 'images/servicio_social/quitar_32.png',
                                'visible'=> '$data->posicion_criterio != null', // <-- SHOW IF ROW ACTIVE
                                'options' => [
                                    'title'        => 'Quitar posición asignada al Criterio',
                                    'data-confirm' => '¿En verdad quieres quitar la posición al Criterio?',
                                ],
                                'click'   => $js_sup, 
                            ],
                            'noCriterioAsignado' => [
                                'label'   => 'No hay posición asignada al Criterio',
                                //'url'     => '#', //
                                'imageUrl' => 'images/servicio_social/bloquedo_32.png',
                                'visible' => '$data->posicion_criterio == NULL ', // <-- SHOW IF ROW INACTIVE
                            ]
                        ],
                    ],//Fin
                    array(
                        'class' => 'CButtonColumn',
                        'template' => '{asignarPosicionCriterio}, {valorAsignado}, {bloqueado}',
                        'header'=>'Orden Criterio',
                        'htmlOptions'=>array('width'=>'90px', 'class'=>'text-center'),
                        'buttons' => array(
                            'asignarPosicionCriterio' => array(
                                'label'=>'Asignar Posición al Criterio',
                                'url' => 'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/asigarPosicionCriterioSupervisor", array("id_criterio"=>$data->id_criterio))',
                                'imageUrl'=>'images/order_32.png',
                                'visible' => "'$valorATotalCriterios' == 100 AND '$posSup' == false"
                            ),
                            'valorAsignado' => array(
                                'label'=>'Posiciones Asignadas',
                                //'url' => '#',
                                'imageUrl'=>'images/servicio_social/aprobado_32.png',
                                'visible' => "'$valorATotalCriterios' == 100 AND '$posSup' == true"
                            ),
                            'bloqueado' => array(
                                'label'=>'No puedes editar la Posición',
                                //'url' => '#',
                                'imageUrl'=>'images/servicio_social/bloquedo_32.png',
                                'visible' => "'$valorATotalCriterios' != 100"
                            )
                        ),
                    ),
                ),
            )); ?>

            <hr>

             <div class="row">
                <h2 class="subTitulo" align="center">
                    <span class="subTitulo_inside">
                        Ordenar Criterios del Admin
                    </span>
                </h2>
            </div>

            <hr>

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'adm-criterios-asignar-grid',
                    'dataProvider'=>$modelSSCriteriosAEvaluar->searchCriteriosXAdmin(),
                    //'filter'=>$modelSSCriteriosAEvaluar,
                    'columns'=>array(
                        /*array(
                            'name' => 'id_criterio',
                            'htmlOptions' => array('width'=>'5px', 'class'=>'text-center')
                        ),*/
                        array(
                            'name' => 'posicion_criterio',
                            'value' => function($data)
                            {
                                return ($data->posicion_criterio == NULL) ? 0 : $data->posicion_criterio;
                            },
                            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
                        ),
                        array(
                            'name' => 'descripcion_criterio',
                            'filter' => false,
                            'htmlOptions' => array('width'=>'300px', 'class'=>'left-center')
                        ),
                        array(
                            'header' => 'Valor (A)',
                            'name'=>'valor_a',
                            'filter' => false,
                            'htmlOptions' => array('width'=>'75px', 'class'=>'text-center')
                        ),
                        array(
                            'header' => 'Tipo de Criterio',
                            'value' => function($data)
                            {
                                return ($data->id_tipo_criterio == 1) ? "CRITERIO DEL SUPERVISOR" : "CRITERIO DEL DEPARTAMENTO";
                            },
                            'filter' => false,
                            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
                        ),
                        array(
                            'header' => 'Estatus',
                            'filter' => false,
                            'type' => 'raw',
                            'value' => function($data)
                            {
                                return ($data->status_criterio == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
                            },
                            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                        ),
                        [//inicio
                            'class'                => 'CButtonColumn',
                            'template'             => '{quitarPosicionCriterio}, {noCriterioAsignado}', // buttons here...
                            'header' 			   => 'Quitar Posición Asignada',
                            'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
                            'buttons'              => [ // custom buttons options here...
                                'quitarPosicionCriterio'   => [
                                    'label'   => 'Quitar posición asignada al Criterio',
                                    'url'     => 'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/quitarPosicionActualCriterio", array("id_criterio"=>$data->id_criterio))', // ?r=controller/approve/id/123
                                    'imageUrl'=> 'images/servicio_social/quitar_32.png',
                                    'visible'=> '$data->posicion_criterio != null', // <-- SHOW IF ROW ACTIVE
                                    'options' => [
                                        'title'        => 'Quitar posición asignada al Criterio',
                                        'data-confirm' => '¿En verdad quieres quitar la posición al Criterio?',
                                    ],
                                    'click'   => $js_adm, 
                                ],
                                'noCriterioAsignado' => [
                                    'label'   => 'No hay posición asignada al Criterio',
                                    //'url'     => '#', //
                                    'imageUrl' => 'images/servicio_social/bloquedo_32.png',
                                    'visible' => '$data->posicion_criterio == NULL', // <-- SHOW IF ROW INACTIVE
                                ]
                            ],
                        ],//Fin
                        array(
                            'class' => 'CButtonColumn',
                            'template' => '{asignarPosicionCriterio}, {valorAsignado}, {bloqueado}',
                            'header'=>'Orden Criterio',
                            'htmlOptions'=>array('width'=>'90px', 'class'=>'text-center'),
                            'buttons' => array(
                                'asignarPosicionCriterio' => array(
                                    'label'=>'Agregar Posición al Criterio',
                                    'url' => 'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/asigarPosicionCriterioAdmin", array("id_criterio"=>$data->id_criterio))',
                                    'imageUrl'=>'images/servicio_social/order_32.png',
                                    'visible' => "'$valorATotalCriterios' == 100 AND '$posAdm' == false",
                                ),
                                'valorAsignado' => array(
                                    'label'=>'Posiciones Asignadas',
                                    //'url' => '#',
                                    'imageUrl'=>'images/servicio_social/aprobado_32.png',
                                    'visible' => "'$valorATotalCriterios' == 100 AND '$posAdm' == true",
                                ),
                                'bloqueado' => array(
                                    'label'=>'No puedes editar la Posición',
                                    //'url' => '#',
                                    'imageUrl'=>'images/servicio_social/bloquedo_32.png',
                                    'visible' => "'$valorATotalCriterios' != 100",
                                )
                            ),
                        ),
                    ),
                )); ?>

                 <br><br>
                <?php if($valorATotalCriterios == 100 AND $posSup == true AND $posAdm == true){ ?>
                <div align="center">
                    <!--Vista previa del Reporte Bimestral despues de asignar los valores de los criterios-->
                    <?php echo CHtml::link('Vista Previa Reporte Bimestral', array('vistaPreviaReporteBimestral'), array('class'=>'btn btn-info')); ?> 
                    <?php echo CHtml::link('Finalizar y Salir', array('menuCriteriosReporteBimestral'), array('class'=>'btn btn-danger')); ?>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>

