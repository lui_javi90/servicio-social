<style>
.hoja{
        background-color: #FFFFFF;
    }
    table, th, td, tr
    {
        /*border: 1px solid black;*/ /*solid black*/
        border-collapse: collapse;
        color: #000000;
        height:7%;
        text-align: left;
    }
    .table1{
        border: 1px solid black; /*solid black*/
        border-collapse: collapse;
        color: #000000;
        height:7%;
        text-align: left;
    }
    .td1{
        border: 1px solid black;
        color: #000000;
        height:5;
        text-align: left;
    }
    .th1{
        border: 1px solid black;
        color: #000000;
        height:7%
        text-align: left;
    }
    .div2{
        background-color: #FFFFFF;
        padding: -4px;
        padding-left: -1px;
    }
    .div3{
        background-color: #FFFFFF;
        padding: -1px;
        padding-left: -1px;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 9px;
    }
    .letra5{
        font-size: 16px;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    #contenedor{
      margin-left:0px;
      margin-top:0px;
      padding:0 0 0 0;
      width:auto;
      /*background-color: #2196F3;*/
      position:absolute;
  }
  .divcontenido1{

      float:left;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
  }
  .divcontenido2{

      float:right;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
      margin-left:0px;
  }
  /*Firmas del reporte*/
  .divcontenido3{
    float:left;
    /*color:white;*/
    font-size:10px;
    padding:0px;
    border-style:groove;
    width:37%;
    text-align:left;
  }
  .divcontenido4{
    float:left;
    /*color:white;*/
    font-size:10px;
    padding-top:70px;
    padding-left: 20px;
    border-style:groove;
    width:20%;
    text-align:left;

  }
  .divcontenido5{
    float:right;
    /*color:white;*/
    font-size:10px;
    padding:0px;
    border-style:groove;
    width:37%;
    text-align:left;
  }
  /*Firmas del reporte*/
  .mytable {
      border-collapse: collapse;
      width: 100%;
      background-color: #FFFFFF;
    }
    .mytable-head {
      border: 1px solid black;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .mytable-body {
      border: 1px solid black;
      border-top: 0;
      margin-top: 0;
      padding-top: 0;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .border-bug {
      border-left: 2px dotted black;
      border-top: 2px dotted black;
      border-bottom: 2px dotted black;
      border-right: 2px dotted black;
    }
      .wrapper {
      display : flex;
      align-items : center;
    }
    /*Imagen sobre texto del sello*/
    .contenedor3{
    position: relative;
    display: inline-block;
    text-align: center;
    }
    .texto-encima{
        position: absolute;
        top: 10px;
        left: 10px;
    }
    .centrado{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    /*Imagen sobre texto del sello*/
    .verticalText {
        writing-mode: vertical-lr;
        transform: rotate(180deg);
    }  
</style>

<div class="hoja">
    <!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/banner_top_itc.jpg" />-->
    <?php echo '<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/'.$banners->banner_superior.'"/>'; ?>
    <br>
    <div class="div2">
        <h6 class="center letra1">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="center letra1">OFICINA DE SERVICIO SOCIAL</h6>
        <h6 class="center bold letra1">FORMATO DE EVALUACIÓN (PLANES 2018-2019)</h6>
        <hr>
    </div>

    <!--NOMBRE COMPLETO DEL PRESTADOR DE SERVICIO SOCIAL (ALUMNO)-->
    <div class="div2">
    <table class="table" align="left">
        <tr class="left" width="100%">
            <td class="bold letra1 left" width="39%">
                Nombre del Prestador de Servicio Social:
            </td>
            <td class="letra1 left" width="61%">
                <?php echo "FULANITA PEREZ PEREZ"; ?>
            </td>
        <tr>
    </table>
    </div>
    <!--NOMBRE COMPLETO DEL PRESTADOR DE SERVICIO SOCIAL (ALUMNO)-->

    <!--CARRERA Y NO. CONTROL DEL ALUMNO-->
    <div class="div2">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td class="bold letra1 left" width="9%">
                    Carrera:
                </td>
                <td class="letra1 left" width="61%">
                    <?php echo "ING. EN SISTEMAS COMPUTACIONALES"; ?>
                </td>
                <td class="bold letra1 left" width="17%">
                    No. de Control:
                </td>
                <td class="letra1 left" width="13%">
                    <?php echo "00000000"; ?>
                </td>
            </tr>
        </table>
    </div>
    <!--CARRERA Y NO. CONTROL DEL ALUMNO-->

    <!--NOMBRE DEL PROGRAMA-->
    <div class="div2">
    <table class="table" align="left">
        <tr class="left" width="100%">
            <td class="bold letra1 left" width="11%">
                Programa:
            </td>
            <td class="letra1 left" width="89%">
                <?php echo "ORDENAMIENTO DE CRITERIOS DEL REPORTE BIMESTRAL"; ?>
            </td>
        <tr>
    </table>
    </div>
    <!--NOMBRE DEL PROGRAMA-->

    <!--PERIODO DE REALIZACIÓN-->
    <div class="div2">
    <table class="table" align="left">
        <tr class="left" width="100%">
            <td class="bold letra1 left" width="24%">
                Periodo de Realización:
            </td>
            <td class="letra1 left" width="76%">
                <u><?php echo "del "."17 de septiembre de 2018"." al "."20 de Marzo de 2019"; ?></u>
            </td>
        <tr>
    </table>
    </diV>
    <!--PERIODO DE REALIZACIÓN-->

    <!--INDICADOR DE BIMESTRE AL QUE CORRESPONDE EL REPORTE-->
    <div class="div2">
    <table class="table">
        <tr class="left" width="100%" align="left">
            <td class="bold letra1" align="left" width="38%">
                Indique a que bimestre corresponde:
            </td>
            <td class="letra1 th1 left" width="12%">
                &nbsp;Bimestre
            </td>
            <td class="bold letra1 th1 center" width="12%">
                <?php ""; ?>
            </td>
            <td class="letra1 th1 left" width="12%">
                <!--Aqui va una condicion para saber si es el final y mostrar la X-->
                &nbsp;Final&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "<span class='bold'>X</span>"; ?>
            </td>
            <td class="letra1 left" width="26%">
                <!--Espacio vacio-->
            </td>
        <tr>
    </table>
    </div>
    <!--INDICADOR DE BIMESTRE AL QUE CORRESPONDE EL REPORTE-->

    <br>
    <!--TABLA DE CRITERIOS A EVALUAR POR PARTE DEL SUPERVISOR DEL PROGRAMA Y DEPTO. VINCULACION-->
    <div class="div3">
        <table class="mytable-head" width="100%">
            <tr>
                <th class="letra2" align="center">
                    En qué medida el prestador del Servicio Social cumple con lo siguiente:
                </th>
            </tr>
        </table>
        <table class="mytable-body" width="100%">
            <tr class="letra1" width="100%" align="center">
                <!--<th style="border: 1px solid black; border-right: 1px solid #FFFFFF" width="10%"> </th>-->
                <th style="border: 1px solid black;"  align="center" height="30px" width="80%" class="letra3">Criterios a Evaluar</th>
                <th style="border: 1px solid black;" align="center" height="30px" width="10%" class="letra1">A <br>Valor</div>
                <th style="border: 1px solid black;" align="center" height="30px" width="10%" class="letra1">Evaluación</div>
            </tr>
        </table>
        <!--CRITERIOS DEL SUPERVISOR-->
        <table class="mytable-body" width="100%">
            <?php
            $contador1 = 0;
            $tamanio = count($lista_criterios_sup);
            for($i=0 ; $i < $tamanio; $i++){
                $contador1++;
                ?>
            <tr class="letra1" width="100%" align="center">
            <?php if($i == 0){ //Encabezado ?>
                <td style="border: 1px solid black;" rowspan="<?php echo $tamanio; ?>" width="12%" class="center bold letra1">
                    <p>Evaluación <br>por el <br>responsable <br>del <br>programa</p>
                </td>
                <td style="border: 1px solid black;" height="30px" width="68%" align="left" class="letra1"><?php echo $contador1.". ".$lista_criterios_sup[$i]['descripcion_criterio']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="bold letra1"><?php echo $lista_criterios_sup[$i]['valor_a']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="letra1"><?php echo $lista_criterios_sup[$i]['valor_a']; ?></td>
            <?php }else{ //Cuando $i es mayor a cero ?>
                <td style="border: 1px solid black;" height="30px" width="68%" align="left" class="letra1"><?php echo $contador1.". ".$lista_criterios_sup[$i]['descripcion_criterio']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="bold letra1"><?php echo $lista_criterios_sup[$i]['valor_a']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="letra1"><?php echo $lista_criterios_sup[$i]['valor_a']; ?></td>
                <?php } ?>
            </tr>
            <?php } ?>
        </table>
        <!--CRITERIOS DEL SUPERVISOR-->

        <!--CRITERIOS DEL ADMIN-->
        <table class="mytable-body" width="100%">
            <?php
            $contador2 = 0;
            $tamanio2 = count($lista_criterios_vinc);
            $rows = ($tamanio2 + 2);
            for($j=0 ; $j < $tamanio2; $j++){
                $contador2++;
                ?>
            <tr class="letra1" width="100%" align="center">
            <?php if($j == 0){ //Encabezado ?>
                <td style="border: 1px solid black;" rowspan="<?php echo $rows; ?>" width="12%" class="center bold letra1">
                    <p>Evaluación<br>por el Jefe <br>de Oficina <br>de <br>Servicio <br>Social </p>
                </td>
            <?php } ?>
                <td style="border: 1px solid black;" height="30px" width="68%" align="left" class="letra1"><?php echo $contador2.". ".$lista_criterios_vinc[$j]['descripcion_criterio']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="bold letra1"><?php echo $lista_criterios_vinc[$j]['valor_a']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="letra1"><?php echo $lista_criterios_vinc[$j]['valor_a']; ?></td>
            </tr>
            <?php } ?>
             <!--CALIFICACION FINAL-->
             <tr class="letra1" width="100%" align="center">
                <td style="border: 1px solid black; border-right: 1px solid #FFFFFF" height="30px" align="center" class="bold letra2">CALIFICACIÓN FINAL</td>
                <td style="border: 1px solid black;" height="30px" align="center" class="bold letra2"><?php echo ""; ?></td>
                <td style="border: 1px solid black;" height="30px" align="center" class="bold letra2"><?php echo $totalA; ?></td>
            </tr>
            <!--CALIFICACION FINAL-->
            <!--NIVEL DE DESEMPEÑO-->
            <tr class="letra2" width="100%" align="center">
                <td style="border: 1px solid black; border-right: 1px solid #FFFFFF" height="30px" align="center" class="bold letra2 center">NIVEL DE DESEMPEÑO:</td>
                <td style="border: 1px solid black; border-right: 1px solid #FFFFFF" height="30px" align="center" class="bold letra2"> </td>
                <td style="border: 1px solid black;" height="30px" align="center" class="bold letra2"> </td>
            </tr>
            <!--NIVEL DE DESEMPEÑO-->
        </table>
        <!--CRITERIOS DEL ADMIN-->
        <!--OBSERVACIONES DEL REPORTE BIMESTRAL-->
        <table class="mytable-head" width="100%">
            <tr class="letra1" width="100%" height="30%" align="center">
                <th class="letra2" align="left">
                    OBSERVACIONES: <?php echo "<span style=\"font-weight:normal;\">"."Vista Prevía Reporte Bimestral"."</span>"; ?>
                </th>
            </tr>
        </table>
        <!--OBSERVACIONES DEL REPORTE BIMESTRAL-->
    </div>
    <!--TABLA DE CRITERIOS A EVALUAR POR PARTE DEL SUPERVISOR DEL PROGRAMA Y DEPTO. VINCULACION-->

     <!--FIRMAS DEL SUPERVISOR, DEL ENCARGADO DE SERVICIO SOCIAL EN VINCULACION Y EL SELLO DE CIENCIAS BASICAS-->
     <div id="contenedor">
      <div class="divcontenido3 bold center">
            <?php
                echo "<br>";
                echo "<br><br>";
                echo "<br>";
                echo "<span class='letra2'>"."Nombre del Supervisor"."</span>";
                echo "<br>";
                echo "<span class='bold'>____________________________________________________</span>";
                echo "<br>";
                echo "<span class='bold letra2'>"."Puesto Supervisor"."</span>";
            ?>
      </div>
     <?php //echo Yii::app()->request->baseUrl; die(); ?>
      <!--<div style="vertical-align: text-top;" class="divcontenido4 bold center">
          <table class="table border-bug" align="center">
              <tr class="letra1" aling="center" width="100%">
                  <td style="border-bottom: 1px solid white;" class="bold" align="center">
                    <span class="center letra1">-->
                        <!--Sello de la<br>Depedencia<br>Empresa-->
                        <!--<img height="150" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/sello_sep.jpg" />
                    </span>
                  </td>
              </tr>
          </table>
      </diV>-->

      <div class="divcontenido5 bold center">
            <?php
                echo "<br>";
                echo "<br><br>";
                echo "<br>";
                echo "<span class='letra2'>"."CATALINA DEL CARMEN LUNA MEZA"."</span>";
                echo "<br>";
                echo "____________________________________________________";
                echo "<br>";
                echo "<span class='bold letra2'>"."VoBo. Oficina del Servicio Social"."</span>";
            ?>
      </div>
    </div>
    <!--FIRMAS DEL SUPERVISOR, DEL ENCARGADO DE SERVICIO SOCIAL EN VINCULACION Y EL SELLO DE CIENCIAS BASICAS-->

    <br><br><br><br>
    <!--LEGENDA INFERIOR DEL EXPEDIENTE-->
    <div class="div2" align="left">
        <?php echo "<span class='bold letra4'>"."c.c.p. Expediente Oficina de Servicio Social"."</span>"; ?>
    </div>
    <!--LEGENDA INFERIOR DEL EXPEDIENTE-->

</div>