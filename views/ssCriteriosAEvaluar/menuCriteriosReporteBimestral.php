<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Ménu Criterios del Reporte Bimestral'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Ménu del Reporte Bimestral
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Criterios
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssCriteriosAEvaluar/listaCriteriosReporteBimestral"
						title="Clasificación de Áreas de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;&nbsp;&nbsp;Crear Nuevo Criterio para Reporte Bimestral
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssCriteriosAEvaluar/asignarValoresCriteriosReporteBimestral"
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;&nbsp;&nbsp;Asignar Valores a los Criterios para Reporte Bimestral
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssCriteriosAEvaluar/ordenarCriteriosReporteBimestral"
						title="Criterios a Evaluar del Reporte Bimestral" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-random"></span>&nbsp;&nbsp;&nbsp;&nbsp;Ordenar los Criterios para el Reporte Bimestral
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
