<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ss-criteriosA-aevaluar-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'htmlOptions' => array('autocomplete'=>'off'),
    'enableAjaxValidation'=>false,
)); ?>

    <!--<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></stron>-->

    <?php echo $form->errorSummary($modelSSCriteriosAEvaluar); ?>

    <div class="form-group">
        <?php echo "Descripcion del Criterio: "."<b>".$modelSSCriteriosAEvaluar->descripcion_criterio."</b>"; ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($modelSSCriteriosAEvaluar,'valor_a'); ?>
        <?php echo $form->dropDownList($modelSSCriteriosAEvaluar,
                                    'valor_a',
                                    $valorA,
                                    array('prompt'=>'--Selecciona Valor A--', 'class'=>'form-control', 'required'=>'required')
                                    ); ?>
        <?php echo $form->error($modelSSCriteriosAEvaluar,'valor_a'); ?>
    </div>

    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSCriteriosAEvaluar->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('asignarValoresCriteriosReporteBimestral'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->