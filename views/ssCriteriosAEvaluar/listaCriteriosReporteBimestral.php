<?php
/* @var $this SsCriteriosAEvaluarController */
/* @var $model SsCriteriosAEvaluar */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Menu Criterios del Reporte Bimestral' => array('ssCriteriosAEvaluar/menuCriteriosReporteBimestral'),
	'Criterios Evaluación Reportes Bimestrales',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Criterios Evaluación Reportes Bimestrales
		</span>
	</h2>
</div>


<!--Se crea el nuevo criterio-->
<br><br><br><br>
<div align="right">
	<?php //echo CHtml::link('Nuevo Criterio de Evaluación', array('nuevoCriterioReporteBimestral'), array('class'=>'btn btn-success')); ?>
</div>
<!--Se crea el nuevo criterio-->

<!--MUESTRA EL TOTAL DE VALOR (A) DE LOS CRITERIOS ACTIVOS-->
<br><br><br>
<h6 align="center"><b>Porcentaje de Progreso del Valor Total de los Criterios:</b></h6>
<div class="progress">
        <?php if($valorATotalCriterios < 100){ ?>
            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $valorATotalCriterios; ?>" aria-valuemin="0" aria-valuemax="100" 
            style="width: <?php echo $valorATotalCriterios."%"; ?>">
            <?php echo $valorATotalCriterios."%"; ?>
            </div>
        <?php }else{ ?>
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $valorATotalCriterios; ?>" aria-valuemin="0" aria-valuemax="100" 
            style="width: <?php echo $valorATotalCriterios."%"; ?>">
		<?php echo $valorATotalCriterios."%"; ?>
            </div>
    	<?php } ?>
</div>
<!--MUESTRA EL TOTAL DE VALOR (A) DE LOS CRITERIOS ACTIVOS-->


<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-criterios-aevaluar-grid',
	'dataProvider'=>$modelSSCriteriosAEvaluar->search(),
	'filter'=>$modelSSCriteriosAEvaluar,
	'columns'=>array(
		/*array(
			'name' => 'id_criterio',
			'htmlOptions' => array('width'=>'5px', 'class'=>'text-center')
		),*/
		array(
			'name' => 'descripcion_criterio',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'left-center')
		),
		array(
			'name' => 'valor_a',
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo de Criterio',
			'filter' => CHtml::activeDropDownList($modelSSCriteriosAEvaluar,
												  'id_tipo_criterio',
												   array('1'=>'CRITERIO DEL SUPERVISOR', '2'=>'CRITERIO DEL DEPARTAMENTO'),
												   array('prompt'=>' --Filtrar por Tipo de Criterio-- ')
			),
			'value' => function($data)
			{
				return ($data->id_tipo_criterio == 1) ? "CRITERIO DEL SUPERVISOR" : "CRITERIO DEL DEPARTAMENTO";
			},
			'htmlOptions' => array('width'=>'125px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editCriterioReporteBimestral}, {bloqueado}',
			'header'=>'Editar',
			'htmlOptions'=>array('width'=>'75px', 'class'=>'text-center'),
			'buttons'=>array(
				'editCriterioReporteBimestral' => array(
					'label'=>'Editar Criterio',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssCriteriosAEvaluar/editarCriterioReporteBimestral", array("id_criterio"=>$data->id_criterio))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					'visible' => "'$valorATotalCriterios' != 100",
				),
				'bloqueado' => array(
					'label'=>'No puedes editar Criterio',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => "'$valorATotalCriterios' == 100",
				),
			),
		),
		array(
			'header' => 'Estatus',
			'filter' => CHtml::activeDropDownList($modelSSCriteriosAEvaluar,
												'status_criterio',
												array('1'=>'ACTIVO', '0'=>'INACTIVO'),
												array('prompt'=>'--Filtrar por--')
			),
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->status_criterio == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
			},
			'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
		),
	),
)); ?>