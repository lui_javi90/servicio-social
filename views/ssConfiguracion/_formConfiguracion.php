<?php
/* @var $this SsConfiguracionController */
/* @var $model SsConfiguracion */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'ss-configuracion-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'htmlOptions' => array('autocomplete'=>'off'),
		'enableAjaxValidation'=>false,
	)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSConfiguracion); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSConfiguracion,'horas_max_servicio_social'); ?>
		<?php echo $form->textField($modelSSConfiguracion,'horas_max_servicio_social',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSConfiguracion,'horas_max_servicio_social'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSConfiguracion,'porcentaje_creditos_req_servicio_social'); ?>
		<?php echo $form->textField($modelSSConfiguracion,'porcentaje_creditos_req_servicio_social',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSConfiguracion,'porcentaje_creditos_req_servicio_social'); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($modelSSConfiguracion,'texto_leyenda_pdf_doc'); ?>
        <?php echo $form->textField($modelSSConfiguracion,'texto_leyenda_pdf_doc',array('size'=>60,'maxlength'=>200, 'class'=>'form-control')); ?>
        <?php echo $form->error($modelSSConfiguracion,'texto_leyenda_pdf_doc'); ?>
    </div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSConfiguracion->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaConfiguracion'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->