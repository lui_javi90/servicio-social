<?php
/* @var $this SsConfiguracionController */
/* @var $model SsConfiguracion */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Configuración Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Configuración
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Establecer el numero de horas por cumplir de Servicio Social, el
		porcentaje de creditos requeridos y la fecha limite de inscripción del Servicio Social.
    </strong></p>
</div>

<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Fecha de la Última modificación:&nbsp;&nbsp;<?php echo ($fecha_actualizacion[0]['fecha_act'] != null) ? $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'] : "Desconocida"; ?></b>
  </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-configuracion-grid',
	'dataProvider'=>$modelSSConfiguracion->search(),
	'filter'=>$modelSSConfiguracion,
	'columns'=>array(
		//'id_configuracion',
		array(
			'name'=>'horas_max_servicio_social',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-info">'.$data->horas_max_servicio_social.'</span>';
			},
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name'=>'porcentaje_creditos_req_servicio_social',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-info">'.$data->porcentaje_creditos_req_servicio_social.'</span>';
			},
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'header' => 'Leyenda Reportes PDF',
			//'name' => 'texto_leyenda_pdf_doc',
			'filter' => false,
			'value' => function($data)
			{
				return ($data->texto_leyenda_pdf_doc == NULL) ? "Texto por establecer" : $data->texto_leyenda_pdf_doc;
			},
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editarConfiguracion}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editarConfiguracion' => array
				(
					'label'=>'Editar Configuración',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssConfiguracion/editarConfiguracion", array("id_configuracion"=>base64_encode($data->id_configuracion)))',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssConfiguracion/editarConfiguracion", array("id_configuracion"=>$data->id_configuracion))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
