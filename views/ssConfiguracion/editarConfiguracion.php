<?php
/* @var $this ConfiguracionController */
/* @var $model Configuracion */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Configuración'=>array('ssConfiguracion/listaConfiguracion'),
	'Editar Configuración',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Configuración
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Configuración</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formConfiguracion', array('modelSSConfiguracion'=>$modelSSConfiguracion)); ?>
			</div>
		</div>
	</div>
</div>

<br><br><br><br><br>
<br><br><br><br><br>