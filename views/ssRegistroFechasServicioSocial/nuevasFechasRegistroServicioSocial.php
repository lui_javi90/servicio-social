<?php
/* @var $this SsConfiguracionController */
/* @var $model SsConfiguracion */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Fechas Registro de Servicio Social' => array('ssRegistroFechasServicioSocial/listaFechasRegistroServicioSocial'),
	'Nueva Fecha Registro de Servicio Social'
);


?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Nueva Fecha Registro de Servicio Social
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		<b>Solo podrás cambiar una sola vez la Fecha Límite de Inscripción.</b>
    </strong></p>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Nuevo Registro Fechas Servicio Social
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formNuevaFechaRegistroServicioSocial', array(
																		'modelSSRegistroFechasServicioSocial'=>$modelSSRegistroFechasServicioSocial,
																		'periodos_escolares' => $periodos_escolares,
																		
																	)); ?>
			</div>
		</div>
	</div>
</div>