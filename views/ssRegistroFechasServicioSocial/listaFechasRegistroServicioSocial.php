<?php
/* @var $this SsRegistroFechasServicioSocialController */
/* @var $model SsRegistroFechasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Fechas Registro de Servicio Social',
);

/*JS PARA FINALIZAR EL REGISTRO DE SERVICIO SOCIAL ACTUAL */
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#ss-registro-fechas-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#ss-registro-fechas-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA FINALIZAR EL REGISTRO DE SERVICIO SOCIAL ACTUAL */
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Fechas Registro de Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div align="right">
	<!--Si existe un Registro de Servicio Social ACTIVO debe desactivarse primero para poder dar de alta otro REGISTRO-->
	<?php echo ($status_registro == false) ? CHtml::link('Nueva Fecha Servicio Social', array('nuevasFechasRegistroServicioSocial'), array('class'=>'btn btn-success')) : ""; ?>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		<b>Una vez establecida la FECHA DE INICIO ya no se podrá modificar dicha fecha, la FECHA LIMITE es posible modificarla 1 VEZ.</b>
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-registro-fechas-servicio-social-grid',
	'dataProvider'=>$modelSSRegistroFechasServicioSocial->search(),
	'filter'=>$modelSSRegistroFechasServicioSocial,
	'columns'=>array(
		//'id_registro_fechas_ssocial',
		array(
			'header' => 'Fecha Limite',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fec_lim = GetFormatoFecha::getFormatFec($data->fecha_limite_inscripcion);

				return $fec_lim;
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'header' => 'Fecha Inicio',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fec_ini = GetFormatoFecha::getFormatFec($data->fecha_inicio_ssocial);

				return $fec_ini;
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'header' => 'Año',
			'name' => 'anio',
			'filter' => CHtml::activeDropDownList($modelSSRegistroFechasServicioSocial,
												  'anio',
												  $lista_anios,
												  array('prompt'=>'--Filtrar por Año--')
			),
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'header' => 'Estatus Servicio Social',
			'filter' => CHtml::activeDropDownList($modelSSRegistroFechasServicioSocial,
												  'ssocial_actual',
												  array('1'=>'ACTIVO', '0'=>'INACTIVO'),
												  array('prompt'=>'--Filtrar por Estatus--')
			),
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->ssocial_actual == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
			},
			'htmlOptions' => array('width'=>'97px', 'class'=>'text-center')
		),
		array(
			'header' => 'Periodo Escolar',
			'filter' => CHtml::activeDropDownList($modelSSRegistroFechasServicioSocial,
												 'id_periodo',
												 $lista_periodos,
												 array('prompt'=>'--Filtrar por Periodo Escolar--')
			),
			'value' => function($data)
			{
				$query = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$data->id_periodo' ";
				$perido = Yii::app()->db->createCommand($query)->queryAll();

				return $perido[0]['dscPeriodo'];
			},
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		//Aqui va historico por servicio social (pendiente)
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editRegFechasServSocial}, {noEdit}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editRegFechasServSocial' => array(

					'label'=>'Editar Configuración',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssRegistroFechasServicioSocial/editarRegistroFechasServSocial", array("id_registro_fechas_ssocial"=>$data->id_registro_fechas_ssocial))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					'visible' => '$data->ssocial_actual == true'
				),
				'noEdit' => array(

					'label'=>'Ya No puedes editar',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => '$data->ssocial_actual == false'
				),
			),
		),
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{habilitado}, {deshabilitado}', // buttons here...
			'header' 			   => 'Cambiar Estatus',
			'htmlOptions' 		   => array('width'=>'70px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'habilitado'   => [
					'label'   => 'FINALIZAR el Registro de Servicio Social',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssRegistroFechasServicioSocial/statusRegistroServicioSocial", array("id_registro_fechas_ssocial"=>$data->id_registro_fechas_ssocial, "oper" => 0))', // ?r=controller/approve/id/123
					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
					'visible'=> '$data->ssocial_actual == true', // <-- SHOW IF ROW ACTIVE
					'options' => [
						'title'        => 'Finalizar',
						'data-confirm' => 'Confirma para FINALIZAR el registro de Servicio Social Actual?',
					],
					'click'   => $approveJs, 
				],
				'deshabilitado' => [
					'label'   => 'Registro de Servicio Social FINALIZADO',
					//'url'     => '#', //
					'imageUrl' => 'images/servicio_social/cancelado_32.png',
					'visible' => '$data->ssocial_actual == false', // <-- SHOW IF ROW INACTIVE
					/*'options' => [
						'title'        => 'No Enviado',//
						'data-confirm' => 'Confirma para FINALIZAR el registro de Servicio Social Actual?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs,*/ // JS string which processes AJAX request
				],
			],
		],//Fin
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
