<?php
/* @var $this SsRegistroFechasServicioSocialController */
/* @var $model SsRegistroFechasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Fechas Registro de Servicio Social' => array('ssRegistroFechasServicioSocial/listaFechasRegistroServicioSocial'),
	'Editar Fechas Registro de Servicio Social'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Fechas Servicio Social
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Editar Fechas Servicio Social</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formNuevaFechaRegistroServicioSocial', array(
											'modelSSRegistroFechasServicioSocial'=>$modelSSRegistroFechasServicioSocial,
											'periodos_escolares' => $periodos_escolares,
											
											)); ?>
			</div>
		</div>
	</div>
</div>