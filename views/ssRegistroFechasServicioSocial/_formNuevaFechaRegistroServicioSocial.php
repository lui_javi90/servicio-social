<?php
/* @var $this SsRegistroFechasServicioSocialController */
/* @var $model SsRegistroFechasServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-registro-fechas-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions'=> array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSRegistroFechasServicioSocial); ?>

	<!--Si cambiar_fech_limite_insc es false entonces puede modificar la fecha limite de inscripcion-->
	<?php //if($modelSSRegistroFechasServicioSocial->cambiar_fech_limite_insc == false or $modelSSRegistroFechasServicioSocial->cambiar_fech_limite_insc == NULL){ ?>
	<div class="form-group">
    <?php echo $form->labelEx($modelSSRegistroFechasServicioSocial,'fecha_limite_inscripcion'); ?>
		<?php //echo $form->textField($model,'fecha_limite_inscripcion');
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $modelSSRegistroFechasServicioSocial,
			'attribute' => 'fecha_limite_inscripcion',
			'language' => 'es',
			//'theme' => 'softark',
			'options' => array(
				//'showOn' => 'both',				// also opens with a button
				'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
				'showOtherMonths' => true,		// show dates in other months
				'selectOtherMonths' => true,	// can seelect dates in other months
				//'changeYear' => true,			// can change year
				'changeMonth' => true,			// can change month
				'yearRange' => '2018:2025',		// range of year
				//'showButtonPanel' => true,		// show button panel
			),
			'htmlOptions' => array(
				'size' => '10',			// textField size
				'maxlength' => '10',	// textField maxlength
				'class' => 'form-control',
				'required' => 'required',
				'readOnly' => true
			),
		));  ?>
        <?php echo $form->error($modelSSRegistroFechasServicioSocial,'fecha_limite_inscripcion'); ?>
	</div>
	<?php /*}else{ ?>
		<div class="form-group">
    <?php echo $form->labelEx($modelSSRegistroFechasServicioSocial,'fecha_limite_inscripcion'); ?>
		<?php //echo $form->textField($model,'fecha_limite_inscripcion');
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $modelSSRegistroFechasServicioSocial,
			'attribute' => 'fecha_limite_inscripcion',
			'language' => 'es',
			//'theme' => 'softark',
			'options' => array(
				//'showOn' => 'both',				// also opens with a button
				'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
				'showOtherMonths' => true,		// show dates in other months
				'selectOtherMonths' => true,	// can seelect dates in other months
				//'changeYear' => true,			// can change year
				'changeMonth' => true,			// can change month
				'yearRange' => '2018:2025',		// range of year
				//'showButtonPanel' => true,		// show button panel
			),
			'htmlOptions' => array(
				'size' => '10',			// textField size
				'maxlength' => '10',	// textField maxlength
				'class' => 'form-control',
				'required' => 'required',
				'readOnly' => true,
				'disabled' => 'disabled'
			),
		));  ?>
        <?php echo $form->error($modelSSRegistroFechasServicioSocial,'fecha_limite_inscripcion'); ?>
	</div>
	<?php }*/ ?>

	<?php if($modelSSRegistroFechasServicioSocial->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSRegistroFechasServicioSocial,'fecha_inicio_ssocial'); ?>
		<?php //echo $form->textField($model,'fecha_inicio_ssocial');
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $modelSSRegistroFechasServicioSocial,
			'attribute' => 'fecha_inicio_ssocial',
			'language' => 'es',
			//'theme' => 'softark',
			'options' => array(
				//'showOn' => 'both',				// also opens with a button
				'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
				'showOtherMonths' => true,		// show dates in other months
				'selectOtherMonths' => true,	// can seelect dates in other months
				//'changeYear' => true,			// can change year
				'changeMonth' => true,			// can change month
				'yearRange' => '2018:2025',		// range of year
				//'showButtonPanel' => true,		// show button panel
			),
			'htmlOptions' => array(
				'size' => '10',			// textField size
				'maxlength' => '10',	// textField maxlength
				'class' => 'form-control',
				'required' => 'required',
				'readOnly' => true
			),
		));  ?>
        <?php echo $form->error($modelSSRegistroFechasServicioSocial,'fecha_inicio_ssocial'); ?>
	</div>
<?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSRegistroFechasServicioSocial,'fecha_inicio_ssocial'); ?>
		<?php //echo $form->textField($model,'fecha_inicio_ssocial');
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $modelSSRegistroFechasServicioSocial,
			'attribute' => 'fecha_inicio_ssocial',
			'language' => 'es',
			//'theme' => 'softark',
			'options' => array(
				//'showOn' => 'both',				// also opens with a button
				'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
				'showOtherMonths' => true,		// show dates in other months
				'selectOtherMonths' => true,	// can seelect dates in other months
				//'changeYear' => true,			// can change year
				'changeMonth' => true,			// can change month
				'yearRange' => '2018:2025',		// range of year
				//'showButtonPanel' => true,		// show button panel
			),
			'htmlOptions' => array(
				'size' => '10',			// textField size
				'maxlength' => '10',	// textField maxlength
				'class' => 'form-control',
				'required' => 'required',
				'readOnly' => true,
				'disabled' => 'disabled'
			),
		));  ?>
        <?php echo $form->error($modelSSRegistroFechasServicioSocial,'fecha_inicio_ssocial'); ?>
	</div>
<?php }?>

	<?php if($modelSSRegistroFechasServicioSocial->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSRegistroFechasServicioSocial,'id_periodo'); ?>
		<?php echo $form->dropDownList($modelSSRegistroFechasServicioSocial,
										'id_periodo',
										$periodos_escolares,
										array('prompt'=>'--Elige el periodo escolar--', 'class'=>'form-control', 'required'=>'required')
										); ?>
		<?php echo $form->error($modelSSRegistroFechasServicioSocial,'id_periodo'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSRegistroFechasServicioSocial,'id_periodo'); ?>
		<?php echo $form->dropDownList($modelSSRegistroFechasServicioSocial,
										'id_periodo',
										$periodos_escolares,
										array('prompt'=>'--Elige el periodo escolar--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
										); ?>
		<?php echo $form->error($modelSSRegistroFechasServicioSocial,'id_periodo'); ?>
	</div>
	<?php }?>

	<!--<div class="form-group">
        <?php /*echo $form->labelEx($modelSSRegistroFechasServicioSocial,'anio'); ?>
		<?php echo $form->dropDownList($modelSSRegistroFechasServicioSocial,
									'anio',
									array('2018'=>'2018', '2019'=>'2019'),
									array('prompt'=>'--Elige el periodo escolar--', 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSRegistroFechasServicioSocial,'anio');*/ ?>
    </div>-->

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSRegistroFechasServicioSocial->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaFechasRegistroServicioSocial'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
