<?php
/* @var $this SsPeriodosProgramasController */
/* @var $data SsPeriodosProgramas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_periodo_programa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_periodo_programa), array('view', 'id'=>$data->id_periodo_programa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('periodo_programa')); ?>:</b>
	<?php echo CHtml::encode($data->periodo_programa); ?>
	<br />


</div>