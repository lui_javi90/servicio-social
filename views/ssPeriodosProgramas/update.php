<?php
/* @var $this SsPeriodosProgramasController */
/* @var $model SsPeriodosProgramas */

$this->breadcrumbs=array(
	'Ss Periodos Programases'=>array('index'),
	$model->id_periodo_programa=>array('view','id'=>$model->id_periodo_programa),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsPeriodosProgramas', 'url'=>array('index')),
	array('label'=>'Create SsPeriodosProgramas', 'url'=>array('create')),
	array('label'=>'View SsPeriodosProgramas', 'url'=>array('view', 'id'=>$model->id_periodo_programa)),
	array('label'=>'Manage SsPeriodosProgramas', 'url'=>array('admin')),
);
?>

<h1>Update SsPeriodosProgramas <?php echo $model->id_periodo_programa; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>