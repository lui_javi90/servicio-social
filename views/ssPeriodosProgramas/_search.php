<?php
/* @var $this SsPeriodosProgramasController */
/* @var $model SsPeriodosProgramas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_periodo_programa'); ?>
		<?php echo $form->textField($model,'id_periodo_programa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'periodo_programa'); ?>
		<?php echo $form->textField($model,'periodo_programa',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->