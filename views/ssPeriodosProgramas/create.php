<?php
/* @var $this SsPeriodosProgramasController */
/* @var $model SsPeriodosProgramas */

$this->breadcrumbs=array(
	'Ss Periodos Programases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsPeriodosProgramas', 'url'=>array('index')),
	array('label'=>'Manage SsPeriodosProgramas', 'url'=>array('admin')),
);
?>

<h1>Create SsPeriodosProgramas</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>