<?php
/* @var $this SsPeriodosProgramasController */
/* @var $model SsPeriodosProgramas */

$this->breadcrumbs=array(
	'Ss Periodos Programases'=>array('index'),
	$model->id_periodo_programa,
);

$this->menu=array(
	array('label'=>'List SsPeriodosProgramas', 'url'=>array('index')),
	array('label'=>'Create SsPeriodosProgramas', 'url'=>array('create')),
	array('label'=>'Update SsPeriodosProgramas', 'url'=>array('update', 'id'=>$model->id_periodo_programa)),
	array('label'=>'Delete SsPeriodosProgramas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_periodo_programa),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SsPeriodosProgramas', 'url'=>array('admin')),
);
?>

<h1>View SsPeriodosProgramas #<?php echo $model->id_periodo_programa; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_periodo_programa',
		'periodo_programa',
	),
)); ?>
