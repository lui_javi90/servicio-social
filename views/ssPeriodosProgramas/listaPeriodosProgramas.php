<?php
/* @var $this PeriodosProgramasController */
/* @var $model PeriodosProgramas */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Periodos de los programas',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
		Periodos de los programas
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'periodos-programas-grid',
	'dataProvider'=>$modelSSPeriodosProgramas->search(),
	'filter'=>$modelSSPeriodosProgramas,
	'columns'=>array(
		array(
			'name' => 'id_periodo_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'50px')
		),
		array(
			'name' => 'periodo_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'320px')
		),
	),
)); ?>

<br><br>