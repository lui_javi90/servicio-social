<?php
/* @var $this SsEstadosSolicitudProgramaController */
/* @var $data SsEstadosSolicitudPrograma */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_estado_solicitud_programa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_estado_solicitud_programa), array('view', 'id'=>$data->id_estado_solicitud_programa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_solicitud_programa')); ?>:</b>
	<?php echo CHtml::encode($data->estado_solicitud_programa); ?>
	<br />


</div>