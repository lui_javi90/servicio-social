<?php
/* @var $this SsEstadosSolicitudProgramaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ss Estados Solicitud Programas',
);

$this->menu=array(
	array('label'=>'Create SsEstadosSolicitudPrograma', 'url'=>array('create')),
	array('label'=>'Manage SsEstadosSolicitudPrograma', 'url'=>array('admin')),
);
?>

<h1>Ss Estados Solicitud Programas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
