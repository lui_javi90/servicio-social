<?php
/* @var $this SsEstadosSolicitudProgramaController */
/* @var $model SsEstadosSolicitudPrograma */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_estado_solicitud_programa'); ?>
		<?php echo $form->textField($model,'id_estado_solicitud_programa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado_solicitud_programa'); ?>
		<?php echo $form->textField($model,'estado_solicitud_programa',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->