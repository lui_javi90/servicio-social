<?php
/* @var $this SsEstadosSolicitudProgramaController */
/* @var $model SsEstadosSolicitudPrograma */

$this->breadcrumbs=array(
	'Ss Estados Solicitud Programas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsEstadosSolicitudPrograma', 'url'=>array('index')),
	array('label'=>'Manage SsEstadosSolicitudPrograma', 'url'=>array('admin')),
);
?>

<h1>Create SsEstadosSolicitudPrograma</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>