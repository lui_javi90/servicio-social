<?php
/* @var $this SsEstadosSolicitudProgramaController */
/* @var $model SsEstadosSolicitudPrograma */

$this->breadcrumbs=array(
	'Ss Estados Solicitud Programas'=>array('index'),
	$model->id_estado_solicitud_programa,
);

$this->menu=array(
	array('label'=>'List SsEstadosSolicitudPrograma', 'url'=>array('index')),
	array('label'=>'Create SsEstadosSolicitudPrograma', 'url'=>array('create')),
	array('label'=>'Update SsEstadosSolicitudPrograma', 'url'=>array('update', 'id'=>$model->id_estado_solicitud_programa)),
	array('label'=>'Delete SsEstadosSolicitudPrograma', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_estado_solicitud_programa),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SsEstadosSolicitudPrograma', 'url'=>array('admin')),
);
?>

<h1>View SsEstadosSolicitudPrograma #<?php echo $model->id_estado_solicitud_programa; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_estado_solicitud_programa',
		'estado_solicitud_programa',
	),
)); ?>
