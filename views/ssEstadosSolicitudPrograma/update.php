<?php
/* @var $this SsEstadosSolicitudProgramaController */
/* @var $model SsEstadosSolicitudPrograma */

$this->breadcrumbs=array(
	'Ss Estados Solicitud Programas'=>array('index'),
	$model->id_estado_solicitud_programa=>array('view','id'=>$model->id_estado_solicitud_programa),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsEstadosSolicitudPrograma', 'url'=>array('index')),
	array('label'=>'Create SsEstadosSolicitudPrograma', 'url'=>array('create')),
	array('label'=>'View SsEstadosSolicitudPrograma', 'url'=>array('view', 'id'=>$model->id_estado_solicitud_programa)),
	array('label'=>'Manage SsEstadosSolicitudPrograma', 'url'=>array('admin')),
);
?>

<h1>Update SsEstadosSolicitudPrograma <?php echo $model->id_estado_solicitud_programa; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>