<style>
.badge-success {
  background-color: #468847;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-warning {
  background-color: #f89406;
}
.badge-error {
  background-color: #b94a48;
}
.badge-inverse {
  background-color: #333333;
}
</style>
<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	'Servicio Social'
); ?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<b>Módulo Servicio Social</b>
		</span>
	</h2>
</div>

<br>
<?php if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-align-justify"></span>&nbsp;
					<b>Departamento de Gestión Tecnológica y Vinculación</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/eDatosAlumno/altaAlumno"
						title="Alumnos Candidatos Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-screenshot"></span>&nbsp;&nbsp;&nbsp;&nbsp;Alumnos Vigentes
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/eDatosAlumno/listaAlumnosAltaServicioSocial"
						title="Alumnos Candidatos Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;Lista Alumnos Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					//if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social'))
					//{
					?>
					<!--<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasActivos"
						title="Altas Alumnos Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-transfer"></span>&nbsp;&nbsp;&nbsp;&nbsp;Cambio de Supervisor del Programa&nbsp;&nbsp;<span style="font-size:16px" class="label label-danger">Solo Casos realmente Especiales</span>
					</a>-->
					<?php
					//}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/listaCandidatosALiberarServicioSocial"
						title="Liberar Alumnos Servicio Social Actual" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-ok-sign"></span>&nbsp;&nbsp;&nbsp;&nbsp;Alumnos Candidatos a Liberar Servicio Social
						<span class="badge badge-info">
						<?php
							//Para saber cuantos alumnos estan en Liberacion de Servicio Social
							$qry_alum_lib = " select count(id_servicio_social) as tot_lib_alum
												from pe_planeacion.ss_servicio_social
												where id_estado_servicio_social = 4 ";

							$alum_lib = Yii::app()->db->createCommand($qry_alum_lib)->queryAll();

							echo ($alum_lib == NULL OR empty($alum_lib)) ? "0" : $alum_lib[0]['tot_lib_alum'];
						?>
						</span>
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssUnidadesReceptoras/menuEmpresas"
						title="Empresas o Instituciones" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-briefcase"></span>&nbsp;&nbsp;&nbsp;&nbsp;Catálogo de Empresas
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasAdminServicioSocial"
						title="Programas Vigentes de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas de Servicio Social
						<span class="badge badge-success">
						<?php
							$qry_prog_vig = "select count(id_programa) as total_prog_vig 
											from pe_planeacion.ss_programas where id_status_programa = 1";

							$pro_vig = Yii::app()->db->createCommand($qry_prog_vig)->queryAll();

							echo ($pro_vig == NULL OR empty($pro_vig)) ? "0" : $pro_vig[0]['total_prog_vig'];
						?>
						</span>
					</a>
					<?php
					}
					?>

					<?php
					/*if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') )
					{*/
					?>
					<!--<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasSinSupervisorAsignado"
						title="Programas para realizar Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-minus-sign"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas sin Supervisor Asignado
					</a>-->
					<?php
					//}
					?>

					<?php
					/*if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') )
					{*/
					?>
					<!--<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasSinHorarioAsignado"
						title="Programas para realizar Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-dashboard"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas sin Horario Asignado
					</a>-->
					<?php
					//}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/historicoProgramasServicioSocial"
						title="Programas para realizar Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-book"></span>&nbsp;&nbsp;&nbsp;&nbsp;Historico Programas de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssSupervisoresProgramas/listaSupervisoresProgramas"
						title="Supervisores de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;&nbsp;Supervisores Externos Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServiciosSocialesAnteriores/listaServiciosSocialesAnteriores"
						title="Horas totales del Alumno" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;&nbsp;&nbsp;Servicios Sociales Pasados
					</a>
					<?php
					}
					?>

					<?php
					//if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					//{
					?>
					<!--<a href="index.php?r=serviciosocial/ssHorarioDiasHabilesProgramas/listaHorariosProgramas"
						title="Horarios de los Programas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;&nbsp;&nbsp;Horarios de los Programas de Servicio Social
					</a>-->
					<?php
					//}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
						$qry_pend_asig_alum_horas = "select count(p.id_programa) as total_programas from pe_planeacion.ss_programas p
														join pe_planeacion.ss_registro_fechas_servicio_social rfss
														on rfss.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
														where p.id_status_programa = 6 AND rfss.ssocial_actual = true
													";

						$prog_total = Yii::app()->db->createCommand($qry_pend_asig_alum_horas)->queryAll();
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasAsignacionAlumnos"
						title="Asignación de Alumnos a Programas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas de Servicio Social Pendientes de Asignación de número de Alumnos y Horas a Liberar
						<span class="badge badge-warning">
							<?php 
								$total = (empty($prog_total) OR $prog_total == null) ? "0" : $prog_total[0]['total_programas']; 
								echo $total; 
							?>
						</span>
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/listaAdminServicioSocial"
						title="Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					//if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					//{
					?>
					<!--<a href="index.php?r=serviciosocial/ssServicioSocial/listaAdminActividades"
						title="Actividades del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-th-large"></span>&nbsp;&nbsp;&nbsp;&nbsp;Actividades Servicio Social
					</a>-->
					<?php
					//}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/listaDeptoObservacionesServicioSocial"
						title="Observaciones a Servicio Social" class="list-group-item" rel="tooltip">
						<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;&nbsp;&nbsp;Observaciones Recibidas <span class="badge badge-info">
						<?php
								require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoObservaciones.php';
								$rfcJefeOfServSocial = Yii::app()->params['rfcJefeOfServSocial'];
								//$rfcJefeOfServSocial = Yii::app()->user->name;
								$observaciones_recibidas = (InfoObservaciones::getObservacionesSinLeerJefeOfServSocial($rfcJefeOfServSocial) == null ) ? "0" : InfoObservaciones::getObservacionesSinLeerJefeOfServSocial($rfcJefeOfServSocial);

								echo $observaciones_recibidas;
						?></span>
					</a>
					<?php
					}
					?>

					<?php
					//if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					//{
					?>
					<!--<a href="index.php?r=serviciosocial/ssSolicitudProgramaServicioSocial/listaSolicitudAdminProgramaServicioSocial"
						title="Solicitudes de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-inbox"></span>&nbsp;&nbsp;&nbsp;&nbsp;Solicitudes Servicio Social
					</a>-->
					<?php
					//}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/listaAdminReportesBimestrales"
						title="Evaluación de Reportes Bimestrales Interno" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Evaluación Reportes Bimestrales Servicio Social
						<span class="badge badge-error">
							<?php
								//id_tipo_programa = 1 -> INTERNO
								$qry_rep_bim_fal = "select count(rb.id_reporte_bimestral) as rep_bim_pend from pe_planeacion.ss_programas p
													join pe_planeacion.ss_servicio_social ss
													on ss.id_programa = p.id_programa
													join pe_planeacion.ss_reportes_bimestral rb
													on rb.id_servicio_social = ss.id_servicio_social
													where rb.valida_oficina_servicio_social is null AND p.id_tipo_programa = 1 AND
													rb.envio_alum_evaluacion is not null AND (rb.valida_responsable is not null AND 
													rb.\"rfcResponsable\" is not null)";

								$res = Yii::app()->db->createCommand($qry_rep_bim_fal)->queryAll();

								echo ($res === NULL OR empty($res)) ? "0" : $res[0]['rep_bim_pend'];
							?>
						</span>
					</a>
					<?php
					}
					?>

					<!--EVALUACION REPORTES BIMESTRALES SERBVICIO SOCIAL EXTERNO-->
					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/listaAdminReportesBimestralesExterno"
						title="Evaluación de Reportes Bimestrales Externo" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Evaluación Reportes Bimestrales Servicio Social Externo
						<span class="badge badge-default">
						<?php
							/*Obtener el no. de reportes bimestrales externos que faltan por evaluar
							id_tipo_programa = 2 -> Externo*/
							$qry_rep_ext = "select count(rb.id_reporte_bimestral) as total_reportes_ext
										from pe_planeacion.ss_programas p
										join pe_planeacion.ss_servicio_social ss
										on ss.id_programa = p.id_programa
										join pe_planeacion.ss_reportes_bimestral rb
										on rb.id_servicio_social = ss.id_servicio_social
										where rb.valida_oficina_servicio_social is null AND rb.envio_alum_evaluacion is not null AND
										rb.valida_responsable is not null AND p.id_tipo_programa = 2 ";

							$rep_ext = Yii::app()->db->createCommand($qry_rep_ext)->queryAll();

							echo ($rep_ext == NULL AND empty($rep_ext)) ? "0" : $rep_ext[0]['total_reportes_ext'];

						?>
						</span>
					</a>
					<?php
					}
					?>
					<!--EVALUACION REPORTES BIMESTRALES SERVICIO SOCIAL EXTERNO-->

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/listaReportesBimestralesValidadosTodos"
						title="Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;Reportes Bimestrales Validados
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/historicoAdminServicioSocial"
						title="Historicos Servicio Social (Sólo Completados)" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-book"></span>&nbsp;&nbsp;&nbsp;&nbsp;Historicos Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasExtraTemporales"
						title="Servicio Social Especial" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-asterisk"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas de Servicio Social Especial (Programas ExtraTemporales)
					</a>
					<?php
					}
					?>

					<?php
					/*Que esta opcion la tenga el Inge Memo nada mas. Para editar las calificaciones de los reportes */
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/editarCalificacionesCriterios"
						title="Alumnos Candidatos Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-transfer"></span>&nbsp;&nbsp;&nbsp;&nbsp;Editar Calificaciones Reportes Bimestrales
					</a>
					<?php
					}
					?>

					<!--cancelar Solicitudes en estatus pendiente-->
					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssSolicitudProgramaServicioSocial/listaSolicitudesProgramasAlumnos"
						title="Supervisores de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;&nbsp;Lista Solicitudes Estatus Pendientes
					</a>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<!--OPCIONES PARA EL ENCARGADO DEL PROGRAMA-->
<?php if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-user"></span>&nbsp;
					<b>Supervisor del programa</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasSupervisorServicioSocial"
						title="Programas para realizar Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
						$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
						//$rfcSupervisor = Yii::app()->user->name;
						//Solicitudes Pendientes al Programa Actual del Supervisor
						$query = "select count(spss.id_solicitud_programa) as total_solicitudes_programa from public.\"H_empleados\" hemp
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.\"rfcEmpleado\" = hemp.\"rfcEmpleado\"
								join pe_planeacion.ss_programas pss
								on pss.id_programa = rpi.id_programa
								join pe_planeacion.ss_registro_fechas_servicio_social rfss
								on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial
								join pe_planeacion.ss_solicitud_programa_servicio_social spss
								on spss.id_programa = pss.id_programa
								where rpi.\"rfcEmpleado\" = '$rfcSupervisor' and pss.id_status_programa = 1 and
								rfss.ssocial_actual = true and
								(spss.id_estado_solicitud_programa_alumno = 2 and spss.id_estado_solicitud_programa_supervisor = 1)";

						$total = Yii::app()->db->createCommand($query)->queryAll();
					?>
					<a href="index.php?r=serviciosocial/ssSolicitudProgramaServicioSocial/listaSolicitudSupervisorProgramaServicioSocial"
						title="Solicitudes Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-inbox"></span>&nbsp;&nbsp;&nbsp;&nbsp;Solicitudes de Servicio Social <span class="badge badge-warning">
						<?php echo ($total === NULL or empty($total)) ? "0" : $total[0]['total_solicitudes_programa']; ?>
						</span>
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/listaHistoricoProgramasSupervisor"
						title="Mi Histórico Programas de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mi Histórico Programas de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/listaSupervisorReportesBimestrales"
						title="Evaluación de Reportes Bimestrales" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Evaluación de Reportes Bimestrales
						<span class="badge badge-error">
						<?php 
							$rfc_supervisor = Yii::app()->params["rfcSupervisor"];
							//$rfc_supervisor = Yii::app()->user->name;
							$rfcSupervisor = trim($rfc_supervisor);

							$qry_rep_bim_pend = "select count(rb.id_reporte_bimestral) as reportes_bim_sup 
													from pe_planeacion.ss_programas p
													join pe_planeacion.ss_responsable_programa_interno rpi
													on rpi.id_programa = p.id_programa
													join pe_planeacion.ss_servicio_social ss
													on ss.id_programa = p.id_programa
													join pe_planeacion.ss_reportes_bimestral rb
													on rb.id_servicio_social = ss.id_servicio_social
													join pe_planeacion.ss_registro_fechas_servicio_social rfss
													on rfss.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
													where rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND rfss.ssocial_actual = true AND
													rb.envio_alum_evaluacion is not null AND 
													(rb.valida_responsable is null OR rb.\"rfcResponsable\" is null)
												";

							$reportes_por_valida = Yii::app()->db->createCommand($qry_rep_bim_pend)->queryAll();

							echo (empty($reportes_por_valida) or $reportes_por_valida === NULL) ? "0" : $reportes_por_valida[0]['reportes_bim_sup'];

						?>
						</span>
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/listaReportesBimestralesValidadosSupervisor"
						title="Actividades de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;Reportes Bimestrales Validados
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/listaSupervisorActividades"
						title="Actividades de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-th-large"></span>&nbsp;&nbsp;&nbsp;&nbsp;Asignación de Actividades de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/listaSupervisorObservacionesServicioSocial"
						title="Observaciones a Servicio Social" class="list-group-item"
						rel="tooltip">
						<!--<span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Observaciones de Servicio Social-->
						<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;&nbsp;&nbsp;Observaciones Recibidas <span class="badge badge-info">
						<?php
								require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoObservaciones.php';
								$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
								//$rfcSupervisor = Yii::app()->user->name;
								$observaciones_recibidas = (InfoObservaciones::getObservacionesSinLeerSupervisor($rfcSupervisor) == null ) ? "0" : InfoObservaciones::getObservacionesSinLeerSupervisor($rfcSupervisor);

								echo $observaciones_recibidas;
						?></span>
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--OPCIONES PARA EL ENCRAGADO DEL PROGRAMA-->

<!--OPCIONES QUE PODRAN VER LOS ALUMNOS-->
<?php  if(Yii::app()->user->checkAccess('alumno') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-font"></span>&nbsp;
					<b>Alumnos</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/listaAlumnoServicioSocial"
						title="Servicio Social Actual" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mi Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssProgramas/listaProgramasAlumnoServicioSocial"
						title="Programas para realizar Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Programas de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssSolicitudProgramaServicioSocial/listaSolicitudAlumnoProgramaServicioSocial"
						title="Solicitudes a programas para realizar Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-inbox"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mis Solicitudes de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssReportesBimestral/listaAlumnoReportesBimestrales"
						title="Reportes Bimestrales del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mis Reportes Bimestrales
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssActividadesServicioSocial/listaAlumnoActividadesServicioSocial"
						title="Actividades del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-th-large"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mis Actividades de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssObservacionesServicioSocial/historialObservacionesServicioSocial"
						title="Observaciones de Servicio Social" class="list-group-item" rel="tooltip">
						<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;&nbsp;&nbsp;Observaciones Recibidas <span class="badge badge-info">
						<?php
								require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoObservaciones.php';
								$no_ctrl = Yii::app()->params['no_ctrl'];
								//$no_ctrl = Yii::app()->user->name;
								$observaciones_recibidas = (InfoObservaciones::getObservacionesSinLeerAlumno($no_ctrl) == null ) ? "0" : InfoObservaciones::getObservacionesSinLeerAlumno($no_ctrl);

								echo $observaciones_recibidas;
						?></span>
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/historicoAlumnoServicioSocial"
						title="Historico Alumno Servicio Social (Completados)" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-book"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mi Histórico de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('alumno') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/eDatosAlumno/preRegistroAlumnoServicioSocial"
						title="Pre-registro a Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-floppy-saved"></span>&nbsp;&nbsp;&nbsp;&nbsp;Pre-registro a Servicio Social
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--OPCIONES QUE PODRAN VER LOS ALUMNOS-->

<!--OPCIONES QUE PODRAN VER DEPTO. SERVICIOS ESCOLARES-->
<?php if(Yii::app()->user->checkAccess('jefe_departamento_escolares') or Yii::app()->user->checkAccess('escolares_control_escolar') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-folder-close"></span>&nbsp;
					<b>Servicios Escolares</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('jefe_departamento_escolares') or Yii::app()->user->checkAccess('escolares_control_escolar') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssHistoricoTotalesAlumnos/listaHorasTotalesAlumnosServicioSocial"
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;&nbsp;&nbsp;&nbsp;Alumnos Completaron Servicio Social
						<span class="badge badge-success">
						<?php 
							$qry_comp = "select count(id_servicio_social) as total_completados
										from pe_planeacion.ss_servicio_social
										where id_estado_servicio_social = 5 ";

							$ssocial_comp = Yii::app()->db->createCommand($qry_comp)->queryAll();

							echo ($ssocial_comp == NULL OR empty($ssocial_comp)) ? "0" : $ssocial_comp[0]['total_completados'];
						?>
						</span>
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('jefe_departamento_escolares') or Yii::app()->user->checkAccess('escolares_control_escolar') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssHistoricoTotalesAlumnos/historicoHorasTotalesAlumnosServicioSocial"
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Histórico Alumnos Completaron Servicio Social
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--OPCIONES QUE PODRAN VER DEPTO. SERVICIOS ESCOLARES-->

<!--OPCION DE JEFE DE DEPTO. DE GESTION TECNOLOGICA Y VINCULACION DE SERVICIO SOCIAL (ERENDIRA)-->
<?php if(Yii::app()->user->checkAccess('jefe_depto_vinculacion') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-list"></span>&nbsp;
					<b>Jefe Departamento de Gestión Tecnológica Y Vinculación</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or Yii::app()->user->checkAccess('alumno') or Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
						//listaCalificacionesAlumnoServicioSocial
					?>
					<a href="index.php?r=serviciosocial/ssServicioSocial/imprimirOficioEntregaCalificacionesServicioSocial" 
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Lista Calificaciones de Servicio Social
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--OPCION DE JEFE DE DEPTO. DE GESTION TECNOLOGICA Y VINCULACION DE SERVICIO SOCIAL (ERENDIRA)-->


<!--INFORMACION DEL MODULO DE SERVICIO SOCIAL-->
<?php if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or Yii::app()->user->checkAccess('alumno') or Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-info-sign"></span>&nbsp;
					<b>Información del Módulo</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or Yii::app()->user->checkAccess('alumno') or Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssEstadoServicioSocial/listaEstadoServicioSocial"
						title="Estados del Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-flag"></span>&nbsp;&nbsp;&nbsp;&nbsp;Estados del Servicio Social
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--INFORMACION DEL MODULO DE SERVICIO SOCIAL-->


<!--OPCIONES QUE PODRAN VER EL ADMINISTRADOR-->
<?php if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true){ ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-cog"></span>&nbsp;
					<b>Administrador</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssConfiguracion/listaConfiguracion"
						title="Configuración del Módulo de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;&nbsp;&nbsp;Configuración del Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssRegistroFechasServicioSocial/listaFechasRegistroServicioSocial"
						title="Nueva Fecha de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;&nbsp;&nbsp;Nueva Fecha de Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssCriteriosAEvaluar/menuCriteriosReporteBimestral"
						title="Criterios a Evaluar del Reporte Bimestral" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;&nbsp;&nbsp;Configuración del Reporte Bimestral
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssClasificacionArea/listaClasificacionAreaServicioSocial"
						title="Clasificación de Áreas de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-th"></span>&nbsp;&nbsp;&nbsp;&nbsp;Clasificación de Áreas para Servicio Social
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssCodigosCalidad/listaCodigosCalidadServicioSocial"
						title="Codigos Calidad de los Reportes de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-barcode"></span>&nbsp;&nbsp;&nbsp;&nbsp;Códigos de Calidad
					</a>
					<?php
					}
					?>

					<?php
					if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social') or true)
					{
					?>
					<a href="index.php?r=serviciosocial/ssCodigosCalidad/listaCodigosCalidadServicioSocial"
						title="Codigos Calidad de los Reportes de Servicio Social" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;&nbsp;&nbsp;Activar Rol de Servicio Social
					</a>
					<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--OPCIONES QUE PODRAN VER ADMINISTRADOR-->

<!--<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-info-sign"></span>&nbsp;
					<b>Reportes de Servicio Social</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">

				</div>
			</div>
		</div>
	</div>
</div>-->
