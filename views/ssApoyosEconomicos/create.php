<?php
/* @var $this SsApoyosEconomicosController */
/* @var $model SsApoyosEconomicos */

$this->breadcrumbs=array(
	'Ss Apoyos Economicoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsApoyosEconomicos', 'url'=>array('index')),
	array('label'=>'Manage SsApoyosEconomicos', 'url'=>array('admin')),
);
?>

<h1>Create SsApoyosEconomicos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>