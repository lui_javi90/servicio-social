<?php
/* @var $this SsApoyosEconomicosController */
/* @var $model SsApoyosEconomicos */

$this->breadcrumbs=array(
	'Ss Apoyos Economicoses'=>array('index'),
	$model->id_apoyo_economico_prestador_servicio_social=>array('view','id'=>$model->id_apoyo_economico_prestador_servicio_social),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsApoyosEconomicos', 'url'=>array('index')),
	array('label'=>'Create SsApoyosEconomicos', 'url'=>array('create')),
	array('label'=>'View SsApoyosEconomicos', 'url'=>array('view', 'id'=>$model->id_apoyo_economico_prestador_servicio_social)),
	array('label'=>'Manage SsApoyosEconomicos', 'url'=>array('admin')),
);
?>

<h1>Update SsApoyosEconomicos <?php echo $model->id_apoyo_economico_prestador_servicio_social; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>