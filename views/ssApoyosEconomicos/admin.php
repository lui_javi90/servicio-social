<?php
/* @var $this SsApoyosEconomicosController */
/* @var $model SsApoyosEconomicos */

$this->breadcrumbs=array(
	'Ss Apoyos Economicoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SsApoyosEconomicos', 'url'=>array('index')),
	array('label'=>'Create SsApoyosEconomicos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ss-apoyos-economicos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Ss Apoyos Economicoses</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-apoyos-economicos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_apoyo_economico_prestador_servicio_social',
		'descripcion_apoyo_economico',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
