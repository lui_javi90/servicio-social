<?php
/* @var $this SsApoyosEconomicosController */
/* @var $data SsApoyosEconomicos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_apoyo_economico_prestador_servicio_social')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_apoyo_economico_prestador_servicio_social), array('view', 'id'=>$data->id_apoyo_economico_prestador_servicio_social)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion_apoyo_economico')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion_apoyo_economico); ?>
	<br />


</div>