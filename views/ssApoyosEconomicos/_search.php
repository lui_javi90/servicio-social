<?php
/* @var $this SsApoyosEconomicosController */
/* @var $model SsApoyosEconomicos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_apoyo_economico_prestador_servicio_social'); ?>
		<?php echo $form->textField($model,'id_apoyo_economico_prestador_servicio_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion_apoyo_economico'); ?>
		<?php echo $form->textField($model,'descripcion_apoyo_economico',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->