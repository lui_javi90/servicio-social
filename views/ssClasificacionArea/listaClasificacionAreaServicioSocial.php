<?php
/* @var $this SsClasificacionAreaController */
/* @var $model SsClasificacionArea */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Clasificación Área Servicio Social',
);
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Clasificación Área Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div align="center">
	<?php echo CHtml::link('Vista Prevía Solicitud Servicio Social', array('vistaPreviaSolicitudServicioSocial'), array('class'=>'btn btn-info left')); ?>
	<!--Se puede quitar esta restriccion-->
	<?php echo ($status_reg_servicio_social == true) ? '' :CHtml::link('Nueva Área Clasificación', array('nuevaAreaClasificacionServicioSocial'), array('class'=>'btn btn-success right')); ?>
</div>

<br><br><br><br><br><br><br>
<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Las Áreas que aparezcan como INACTIVAS no se mostraran en la parte de "TIPO DE PROGRAMA" en Solicitud de Servicio Social.</b>
  </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ss-clasificacion-area-servicio-social-grid',
	'dataProvider'=>$modelSSClasificacionArea->search(),
	'filter'=>$modelSSClasificacionArea,
	'columns'=>array(
		/*array(
			'name'=> 'id_clasificacion_area_servicio_social',
			'filter' => false,
			'htmlOptions' => array('width'=>'50px')
		),*/
		array(
			'name' => 'clasificacion_area_servicio_social',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		array(
			'header' => 'Estatus del Área',
			'filter' => CHtml::activeDropDownList($modelSSClasificacionArea,
												'status_area',
												array('1'=>'ACTIVO', '0'=>'INACTIVO'),
												array('prompt'=>'-- Filtrar por estatus del Área --')
			),
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->status_area == 1) ? '<span class="label label-success">ACTIVO</span>' : '<span class="label label-danger">INACTIVO</span>';
			},
			'htmlOptions' => array('width'=>'115px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editAreaClasificacionServicioSocial},{noEdit}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editAreaClasificacionServicioSocial' => array(
					'label'=>'Editar Clasificación Área',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssClasificacionArea/editarAreaClasificacionServicioSocial", array("id_clasificacion_area_servicio_social"=>$data->id_clasificacion_area_servicio_social))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					'visible' => "'$status_reg_servicio_social' == false",
				),
				'noEdit' => array(
					'label'=>'No puedes Editar',
					//'url'=>'#',
					'imageUrl' => 'images/servicio_social/quitar_32.png',
					'visible' => "'$status_reg_servicio_social' == true",
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',//
			'template'=>'{elimAreaClasificacionServicioSocial}',
			'header'=>'Eliminar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'elimAreaClasificacionServicioSocial' => array
				(
					'label'=>'Eliminar Clasificación Área',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssClasificacionArea/eliminarAreaClasificacionServicioSocial", array("id_clasificacion_area"=>$data->id_clasificacion_area_servicio_social))',
					'imageUrl'=>'images/eliminar_32.png',
					'click'=>'function(){return confirm("Estas Seguro?");}'
				),
			),
		)*/
	),
)); ?>
