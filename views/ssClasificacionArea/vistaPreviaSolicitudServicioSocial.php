<style>
.hoja{
        background-color: #FFFFFF;
    }
    .div2{
        background-color: #FFFFFF;
        padding: -4px;
        padding-left: -1px;
    }
    .div3{
        background-color: #FFFFFF;
        text-align: justify;
        text-justify: inter-word;
    }
    .div4{
        background-color: #FFFFFF;
        padding: 0px;
        padding-left: 0px; /*-8px*/
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 9px;
    }
    .letra5{
        font-size: 13px;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    .interlineado1{
        line-height: 10pt;
    }
</style>

<div class="hoja">
    <!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/banner_top_itc.jpg" />-->
    <?php echo '<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/'.$banners->banner_superior.'"/>'; ?>
    <br>
    <!--Encabezado del documento-->
    <div class="div2">
        <h6 class="center letra1">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="center letra1">OFICINA DE SERVICIO SOCIAL</h6>
        <h6 class="center bold letra1">SOLICITUD DE SERVICIO SOCIAL</h6>
        <hr>
    </div>
    <!--Encabezado del documento-->

    <div style="letter-spacing: -.01em" class="div3 letra1">
    Con el fin de dar cumplimiento a lo establecido en la Ley Reglamentaria del Artículo 5º Constitucional relativo al ejercicio
    de profesiones, el suscrito:
    </div>

    <!--Datos Personales del Alumno-->
    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="9%">
                    Nombre:
                </td>
                <td style="padding-left:0%" class="letra2 left" width="41%">
                    <?php echo "FULANITA DE TAL" ?>
                </td>
                <td style="padding-left:0%" class="bold letra1 left" width="15%">
                    No. de Control:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="35%">
                    <?php echo "00000000"; ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="10%">
                    Domicilio:
                </td>
                <td style="padding-left:0%" class="letra4 left" width="40%">
                    <?php echo "AVENIDA SIEMPRE VIVA #742" ?>
                </td>
                <td style="padding-left:0%" class="bold letra1 left" width="5%">
                    Tel:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="45%">
                    <?php echo "461 000 0000" ?>
                </td>
            <tr>
        </table>
    </div>
    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="9%">
                    Carrera:
                </td>
                <td style="padding-left:0%" class="letra2 left" width="41%">
                    <?php echo "INGENERÍA BIOQUIMÍCA" ?>
                </td>
                <td style="padding-left:0%" class="bold letra1 left" width="10%">
                    Semestre:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="40%">
                    <?php echo "7"."º"; ?>
                </td>
            <tr>
        </table>
    </div>
    <!--Datos Personales del Alumno-->

    <!--Datos del Programa-->
    <h6 class="left bold letra1"><FONT FACE="times new roman">DATOS DEL PROGRAMA</FONT></h6>

    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="20%">
                    Dependencia Oficial:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="80%">
                    <?php echo "TNM INSTITUTO TECNOLÓGICO DE CELAYA" ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="26%">
                    Responsable del programa:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="74%">
                    <?php echo "ING. MENGANITO DE TAL"; ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="8%">
                    Puesto:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="92%">
                    <?php echo "JEFE DEL DEPARTAMENTO DE FÍSICA"; ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="22%">
                    Nombre del Programa:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="78%">
                    <?php echo "BECAS TEC"; ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div4">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="bold letra1 left" width="11%">
                    Modalidad:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="89%">
                    <?php echo "SEMESTRAL"; ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div3">
        <table class="table" align="center">
            <tr class="left" width="100%">
                <td style="padding-left:2.5%" class="bold letra1 left" width="18%">
                    Fecha de Inicio:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="25%">
                    <?php echo "17 de Septiembre de 2018"; ?>
                </td>
                <td style="padding-left:2%" class="bold letra1 left" width="35%">
                    Fecha Aproximada de Terminación:
                </td>
                <td style="padding-left:0%" class="letra1 left" width="22%">
                    <?php echo "21 de Marzo de 2018"; ?>
                </td>
            <tr>
        </table>
    </div>

    <div class="div3 letra1">
        <span class="bold">Actividades:</span> <?php echo "Administrar y veríficar las documentación de los alumnos que aplican para las becas."; ?>
    </div>

    <h6 class="left bold letra1"><FONT FACE="times new roman">TIPO DE PROGRAMA</FONT></h6>

    <br>
    <div class="div3 letra2 interlineado1">
        <table class="table" align="center">
            <?php
            $entero = (sizeof($lista_areas_activas) % 2); //Para saber si es numero par o no
            echo $entero;
            //die();
            //echo $entero; die();
            $tamanio = ($entero == 0) ? sizeof($lista_areas_activas) / 2 : (int) (sizeof($lista_areas_activas) / 2) + 1;
            //echo "<br>".$tamanio; die();
            $contador1 = 0;
            $contador2 = 0;
            for($i=0; $i < $tamanio; $i++)
            {
                $contador2++;
            ?>
            <tr class="left" width="100%">
                <td style="padding-left:0%" class="letra2 left interlineado1" width="50%">
                    <?php if($lista_areas_activas[$contador1]['clasificacion_area_servicio_social'] != NULL){ ?>
                        (&nbsp;&nbsp;&nbsp;) <?php echo $lista_areas_activas[$contador1]['clasificacion_area_servicio_social']; ?>
                    <?php } ?>
                </td>
                <td style="padding-left:-0.5%" class="letra2 left interlineado1" width="50%">
                    <?php if($lista_areas_activas[$contador2]['clasificacion_area_servicio_social'] != NULL){?>
                        &nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;) <?php echo $lista_areas_activas[$contador2]['clasificacion_area_servicio_social'];
                    }?>
                </td>
            </tr>
            <?php

                $contador1 += 2;
                $contador2 += 1;
            } 
            ?>
        </table>
    </div>
    <!--Datos del Programa-->

    <!--Reglamento-->
    <br>
    <div style="letter-spacing: -.090em" class="div3 bold letra5">
    <FONT FACE="arial">Me comprometo a realizar el Servicio Social acatando el reglamento del Tecnológico Nacional de México y llevarlo a cabo en el lugar y periodos
    manifestados, así como, a participar con mis conocimientos e iniciativa en las actividades que desempeñe, procurando dar una imagen positiva del
    Instituto Tecnológico en el organismo o dependencia oficial, de no hacerlo así, quedo enterado (a) de la cancelación respectiva, la cual procederá
    automáticamente.</FONT>
    </div>
    <!--Reglamento-->

    <!--Fecha de la Solicitud de Servicio Social-->
    <br>
    <div class="div3 right letra1">
        En la ciudad de Celaya, Guanajuato del dia <?php echo "<span class=\"bold\">16 de Septiembre de 2018</span>"; ?>.
    </div>
    <!--Fecha de la Solicitud de Servicio Social-->

    <!--Firma de confirmacion del alumno-->
    <br>
    <div class="div3 center letra1">
        <h6 class="center bold letra1"><FONT FACE="arial">Conformidad</FONT></h6>
        <?php
        echo "<br>";
        if(!true)
        {
        echo "<br>";
        }else{
        echo "El alumno (a) "."<span class=\"bold\">"."FULANITA DE TAL"."</span>"." con No. de Control "."<span class=\"bold\">"."00000000"."</span>"." aceptó las condiciones presentadas el dia "."<span class=\"bold\">"."16 de Septiembre de 2018"." a las "."14:24 PM"."</span>".".";
        echo "<br>";
        }?>
        <?php echo "<span class=\"bold\">_________________________________________________</span><br>"; ?>
        <?php echo "<span class=\"bold\">Nombre y Firma del Estudiante</span>"; ?>
    </div>
    <!--Firma de confirmacion del alumno-->
</div>