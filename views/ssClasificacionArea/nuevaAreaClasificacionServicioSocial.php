<?php
/* @var $this SsClasificacionAreaController */
/* @var $model SsClasificacionArea */

if($modelSSClasificacionArea->isNewRecord){
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Clasificación Áreas Servicio Social' => array('ssClasificacionArea/listaClasificacionAreaServicioSocial'),
		'Nueva Clasificación Area Servicio Social',
	);
}else{

	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Clasificación Áreas Servicio Social' => array('ssClasificacionArea/listaClasificacionAreaServicioSocial'),
		'Editar Clasificación Area Servicio Social',
	);
}
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSClasificacionArea->isNewRecord) ? "Nueva Clasificación Area Servicio Social" : "Editar Clasificación Area Servicio Social"; ?>
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSClasificacionArea->isNewRecord) ? "Nueva Clasificación Area Servicio Social" : "Editar Clasificación Area Servicio Social"; ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formClasificacionAreaServicioSocial', array(
											'modelSSClasificacionArea'=>$modelSSClasificacionArea
											)); ?>
			</div>
		</div>
	</div>
</div>