<?php
/* @var $this SsClasificacionAreaController */
/* @var $model SsClasificacionArea */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-clasificacion-area-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

<?php echo $form->errorSummary($modelSSClasificacionArea); ?>

<div class="form-group">
	<?php echo $form->labelEx($modelSSClasificacionArea,'clasificacion_area_servicio_social'); ?>
	<?php echo $form->textField($modelSSClasificacionArea,'clasificacion_area_servicio_social',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
	<?php echo $form->error($modelSSClasificacionArea,'clasificacion_area_servicio_social'); ?>
</div>

<div class="form-group">
    <?php echo $form->labelEx($modelSSClasificacionArea,'status_area'); ?>
    <?php echo $form->checkBox($modelSSClasificacionArea,'status_area'); ?>
	<?php echo $form->error($modelSSClasificacionArea,'status_area'); ?>
</div>

<br>
<div class="form-group">
	<?php echo CHtml::submitButton($modelSSClasificacionArea->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::link('Cancelar', array('listaClasificacionAreaServicioSocial'), array('class'=>'btn btn-danger')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->