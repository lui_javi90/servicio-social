<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Programas Extratemporales',
);

//Validar la elimimacion del programa
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#programas-extra-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#programas-extra-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//Validar la elimimacion del programa

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Programas ExtraTemporales
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div style="float: right;">
    <?php echo CHtml::link('Nuevo Registro Programa', array('nuevoProgramaExtraTemporal', 'id_registro_fechas_ssocial'=>$id_registro_fechas_ssocial), array('class'=>'btn btn-success')); ?>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Verifica la fecha de caducidad de cada Programa ExtraTemporal, una vez que el Estatus del Programa cambie a
		Caducado será momento de Finalizar el programa.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'programas-extra-social-grid',
	'dataProvider'=>$modelSSProgramas->searchProgramasExtratemporales(),
	'filter'=>$modelSSProgramas,
	'columns'=>array(
		//'id_programa',
		array(
			'name'=>'nombre_programa',
			'htmlOptions' => array('width'=>'350px', 'class'=>'text-center')
		),
		array(
			'header'=>'Empresa',
			'name' => 'idUnidadReceptora.nombre_unidad_receptora',
			'filter' => false,
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		/*array(
			'name'=>'Departamento',
			'value' => function($data)
			{
				$id = $data->rfcSupervisor0->cveDepartamento;
				$query = "select \"dscDepartamento\" from \"H_departamentos\" where \"cveDepartamento\" = '$id' ";
				$nombre = Yii::app()->db->createCommand($query)->queryAll();

				return $nombre[0]['dscDepartamento'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),*/
		array(
			'name'=>'horas_totales',
			'filter' => false,
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
			'name'=>'idPeriodoPrograma.periodo_programa',
			'filter' => false,
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		/*array(//id_supervisor
			'header'=>'Supervisor',
			'value'=>function($data)
			{
				$rfc_sup = $data->rfcSupervisor;
				$query =
				"
				SELECT (nombre_supervisor||' '||apell_paterno||' '|| apell_materno) as name
				FROM ss_supervisores_programas WHERE \"rfcSupervisor\"='$rfc_sup'
				";
				$name_completo_supervisor = Yii::app()->db->createCommand($query)->queryAll();

				return $name_completo_supervisor[0]['name'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'200px')
		),*/
		array(
			'header' => 'Estudiantes Solicitados',
			'name'=>'numero_estudiantes_solicitados',
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'name'=>'lugares_disponibles', //
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		/*'descripcion_objetivo_programa',
		'impacto_social_esperado',
		'beneficiarios_programa',
		'actividades_especificas_realizar',
		'mecanismos_supervision_servicio_social',
		'perfil_estudiante_requerido',
		'id_apoyo_economico_prestador',
		'id_tipo_apoyo_economico',*/
		array(
			'header' => 'Hay Apoyo',
			'filter' => CHtml::activeDropDownList($modelSSProgramas,
												'id_apoyo_economico_prestador',
												array('1'=>'HAY APOYO', '2'=>'NO HAY APOYO'),
												array('prompt'=>'-- Filtrar por --')),
			'value' => function($data)
			{
				return ($data->id_apoyo_economico_prestador == 1) ? "SI" : "NO";
			},
			'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
		),
		/*array(//id_recibe_capacitacion
			'header' => 'Recibe Capacitación',
			'value'=>function($data)
			{
				return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO" ;
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'75px')
		),*/
		/*'fecha_registro_programa_servicio_social',
		'fecha_modificacion_programa_servicio_social',
		'id_clasificacion_area_servicio_social',*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editProgramaServicioSocial}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editProgramaServicioSocial' => array
				(
					'label'=>'Asignar Numero Alumnos a Programas',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/editarProgramaExtraTemporal", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/editar_32.png'
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detAdminProgramaServicioSocial}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detAdminProgramaServicioSocial' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleProgramaExtraTemporal", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png'
				),
			),
		),
		/*[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{eliminar}', // buttons here...
			'header' 			   => 'Eliminar Programa',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'eliminar'   => [
					'label'   => 'Eliminar Programa ExtraTemporal',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssProgramas/eliminarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=> 'images/eliminar_32.png',
					'visible'=> '$data->id_status_programa != 3', //Habilitado si el programa esta dado de ALTA
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Eliminación del Programa ExtraTemporal ?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],*/
		array(
			'header' => 'Estatus',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data, $fecha_actual)
			{
				return ($fecha_actual < $data->fecha_fin_programa) ? '<span style="size:18px" class="label label-success">VIGENTE</span>' : '<span style="size:18px" class="label label-danger">CADUCADO</span>';
			},
			'htmlOptions' => array('width'=>'87px', 'class'=>'text-center')
		),
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{vigente} ,{novigente}', // buttons here...
			'header' 			   => 'Cambiar Estatus',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'vigente'   => [
					'label'   => 'Programa ExtraTemporal Vigente',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssProgramas/finalizarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
					'visible'=> '$data->fecha_fin_programa > '.$fecha_actual.' and $data->id_status_programa != 3 ', //Habilitado si el programa esta dado de ALTA
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Finalización del Programa ExtraTemporal ?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
				'novigente'   => [
					'label'   => 'Programa ExtraTemporal Caducado',
					//'url'     => '#',
					'imageUrl'=> 'images/servicio_social/no_aprobado_32.png',
					'visible'=> '$data->id_status_programa == 3', //Habilitado si el programa esta dado de ALTA
				],
			],
		],//Fin
	),
)); ?>
