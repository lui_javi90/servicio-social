<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */
if($modelSSProgramas->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Programas Servicio Social'=>array('ssProgramas/listaProgramasAdminServicioSocial'),
		'Nuevo Programa',
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Programas Servicio Social'=>array('ssProgramas/listaProgramasAdminServicioSocial'),
		'Editar Programa',
	);
}


?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSProgramas->isNewRecord) ? "Nuevo Programa " : "Editar Programa"; ?>
		</span>
	</h2>
</div>

<?php if(!$modelSSProgramas->isNewRecord){?>
<br><br><br>
<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  	<b>Fecha de la Última modificación:&nbsp;&nbsp;<?php echo $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora']; ?></b>
  </strong></p>
</div>
<?php } ?>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSProgramas->isNewRecord) ? "Nuevo Programa " : "Editar Programa"; ?> 
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formDeptoProgramaServicioSocial', array(
										   'modelSSProgramas'=>$modelSSProgramas,
										   'lista_unidades_receptoras' => $lista_unidades_receptoras,
										   'lista_departamentos' => $lista_departamentos,
										   'lista_periodos_programas' => $lista_periodos_programas,
										   'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
										   'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
										   'lista_tipo_apoyo' => $lista_tipo_apoyo,
										   'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
										   'lista_status' => $lista_status,
											)); ?>
			</div>
		</div>
	</div>
</div>