<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Programas sin Supervisor Asignado'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Lista de Programas sin Supervisor Asignado
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Lista de Programas que NO tiene asigando un Supervisor.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'programas-sin-supervisor-servicio-social-grid',
	'dataProvider'=>$modelSSProgramas->searchProgramasSinSupervisor(),
	'filter'=>$modelSSProgramas,
	'columns'=>array(
		//'id_programa_servicio_social',
		array(
			'name'=>'nombre_programa',
			'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
		),
		/*array(
			'header'=>'Empresa',
			'name' => 'idUnidadReceptora.nombre_unidad_receptora',
			'filter' => false,
			'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
		),*/
		array(
			'name'=>'horas_totales',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->horas_totales.'</span>';
			},
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'name'=>'idPeriodoPrograma.periodo_programa',//tipo_programa
			'filter' => CHtml::activeDropDownList($modelSSProgramas,
												 'id_periodo_programa',
												 $tipo_programa,
												 array('prompt'=>'-- Filtrar por  --')
											   ),
			'htmlOptions' => array('width'=>'140px', 'class'=>'text-center')
		),
		/*array(
			'header'=>'Supervisor',
			'value'=>function($data)
			{
				$rfc_sup = $data->ssResponsableProgramaExternos->rfcSupervisor;
				$query =
				"
				SELECT (nombre_supervisor||' '||apell_paterno||' '|| apell_materno) as name
				FROM pe_planeacion.ss_supervisores_programas WHERE \"rfcSupervisor\"='$rfc_sup'
				";
				$name_completo_supervisor = Yii::app()->db->createCommand($query)->queryAll();

				return $name_completo_supervisor[0]['name'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Estudiantes Solicitados',
			'name'=>'numero_estudiantes_solicitados',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->numero_estudiantes_solicitados.'</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'name'=>'lugares_disponibles', //
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->lugares_disponibles > 0) ? '<span style="font-size:18px" class="label label-success">'.$data->lugares_disponibles.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->lugares_disponibles.'</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{completarRegistroPrograma}',
			'header'=>'Completar Registro',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'completarRegistroPrograma' => array
				(
					'label'=>'Asignar Numero Alumnos a Programas',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/asignacionSupervisoresProgramasDepto", array("id_programa"=>$data->id_programa, "id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/servicio_social/editar_32.png'
				),
			),
		),
	),
)); ?>