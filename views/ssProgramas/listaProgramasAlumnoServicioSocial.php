<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Programas Servicio Social',
);

//Validar la solicitud al programa
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#alum-programas-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#alum-programas-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//Validar la solicitud al programa

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Programas Servicio Social
		</span>
	</h2>
</div>

<?php if($alumnoAltaServicioSocial == true){?>
	<?php if($tomoSsocialMismoPeriodoYAnio == 0 AND $completoServicioSocial == false AND $servicioSocialFueCancelado == 0){ ?>
	<br><br><br><br><br>
	<div class="alert alert-info">
		<p><strong>
			<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			Los programas que aparecen como "DEPARTAMENTO EXTERNO" son los programa externos, es decir, son programas
			que se realizan fuera del Tecnológico.
		</strong></p>
	</div>

	<?php //echo "sds:".$fecha_limite; die(); ?>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'alum-programas-servicio-social-grid',
		'dataProvider'=>$modelSSProgramas->searchProgramasActivos($nombreSupervisor, $apePaterno, $apeMaterno),
		'filter'=>$modelSSProgramas,
		'columns'=>array(
			//'id_programa',
			array( //Foto del Supervisor Interno o Externo en caso que haya subido una la SII
				'header' => 'Supervisor',
				'type'=>'raw',
				'value' => function($data)
				{
					$rfc;
					$foto;
					switch($data->id_tipo_programa)
					{
						case 1:
							$query = "select rpi.\"rfcEmpleado\" as rfc
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_interno rpi
									on rpi.id_programa = pss.id_programa
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpi.superv_principal_int = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$rfc = $result[0]['rfc'];

							//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
							$query2 = "select foto from public.\"H_credencialEmpleado\" where \"rfcEmpleado\" = '$rfc' ";
							$res = Yii::app()->db->createCommand($query2)->queryAll();

							//Guardamos la foto en caso de que hayan subido una
							$foto = $res[0]['foto'];

							return ($foto != NULL) ? CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;")) : CHtml::image("images/servicio_social/masculino2.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
						break;
						case 2:
							$query = "select rpe.\"rfcSupervisor\" as rfc
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_externo rpe
									on rpe.id_programa = pss.id_programa
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpe.superv_principal_ext = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$rfc = $result[0]['rfc'];

							//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
							$query2 = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfc' ";
							$res = Yii::app()->db->createCommand($query2)->queryAll();

							//Guardamos la foto en caso de que hayan subido una
							$foto = $res[0]['foto_supervisor'];

							//Si hay foto guardada en la BD
							if($foto != NULL)
							{
								if(trim($foto) != 'masculino.png' or trim($foto) != 'femenino.png')
									return CHtml::image("images/servicio_social/supervisores/".$rfc."/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
								else
									return CHtml::image("images/servicio_social/supervisores/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

							}else
							{

								if($foto[0]['id_sexo'] == 1)
									return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
								elseif($foto[0]['id_sexo'] == 2)
									return CHtml::image("images/servicio_social/supervisores/femenino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
								else
									return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							}

						break;
					}
				},
			),
			array(
				'name'=>'nombre_programa',
				'filter' => false,
				'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
			),
			/*array(
				'header' => 'Tipo',
				'name'=>'id_tipo_programa',
				'filter' => CHtml::activeDropDownList($modelSSProgramas,
													'id_tipo_programa',
													array('1'=>'INTERNO', '2'=>'EXTERNO'),
													array('prompt'=>'-- Tipo --')
				),
				'type' => 'raw',
				'value' => function($data)
				{
					if($data->id_tipo_programa == 1)
						return '<span style="font-size:18px" class="label label-success">I</span>';
					else
						return '<span style="font-size:18px" class="label label-success">E</span>';
				},
				'htmlOptions' => array('width'=>'12px', 'class'=>'text-center')
			),*/
			array(
				'name' => 'nombreSupervisor',
				'header' => 'Nombre del Supervisor',
				'value' => function($data)
				{
					//Guardamos el Nombre del Supervisor
					$nombres;

					switch($data->id_tipo_programa)
					{
						case 1: //Interno
							//hacemos la consulta para obtener el nombre completo del supervisor del programa con ese id
							$query = "select hemp.\"nmbEmpleado\" as nombres
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_interno rpi
									on rpi.id_programa = pss.id_programa
									join public.\"H_empleados\" hemp
									on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpi.superv_principal_int = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$nombres = $result[0]['nombres'];

							return $nombres;
						break;
						case 2: //Externo
							$query = "select sp.nombre_supervisor as nombres
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_externo rpe
									on rpe.id_programa = pss.id_programa
									join pe_planeacion.ss_supervisores_programas sp
									on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpe.superv_principal_ext = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$nombres = $result[0]['nombres'];

							return $nombres;
						break;
						default:
							return "--";
					}
				},
				'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
			),
			array(
				'name' => 'apePaterno',
				'header' => 'Apellido Paterno',
				'value' => function($data)
				{
					//Guardamos el apellido paterno del supervisor
					$ape_paterno;

					switch($data->id_tipo_programa)
					{
						case 1:
							$query = "select hemp.\"apellPaterno\" as apellido_paterno
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_interno rpi
									on rpi.id_programa = pss.id_programa
									join public.\"H_empleados\" hemp
									on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpi.superv_principal_int = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$ape_paterno = $result[0]['apellido_paterno'];

							return $ape_paterno;
						break;
						case 2:
							$query = "select sp.apell_paterno as apellido_paterno
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_externo rpe
									on rpe.id_programa = pss.id_programa
									join pe_planeacion.ss_supervisores_programas sp
									on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpe.superv_principal_ext = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$ape_paterno = $result[0]['apellido_paterno'];

							return $ape_paterno;
						break;
						default:
							return "--";
					}
				},
				'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
			),
			array(
				'name' => 'apeMaterno',
				'header' => 'Apellido Materno',
				'value' => function($data)
				{
					//Guardamos el apellido materno del supervisor
					$ape_materno;

					switch($data->id_tipo_programa)
					{
						case 1:
							$query = "select hemp.\"apellMaterno\" as apellido_materno
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_interno rpi
									on rpi.id_programa = pss.id_programa
									join public.\"H_empleados\" hemp
									on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpi.superv_principal_int = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$ape_materno = $result[0]['apellido_materno'];

							return $ape_materno;
						break;
						case 2:
							$query = "select sp.apell_materno as apellido_materno
									from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_responsable_programa_externo rpe
									on rpe.id_programa = pss.id_programa
									join pe_planeacion.ss_supervisores_programas sp
									on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
									where pss.id_programa = '$data->id_programa' and
									pss.id_tipo_programa = '$data->id_tipo_programa' and
									rpe.superv_principal_ext = true and
									pss.id_status_programa = 1";

							$result = Yii::app()->db->createCommand($query)->queryAll();
							$ape_materno = $result[0]['apellido_materno'];

							return $ape_materno;
						break;
						default:
							return "--";
					}
				},
				'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
			),
			/*array(
				'header'=>'Empresa',
				'name' => 'idUnidadReceptora.nombre_unidad_receptora',
				'filter' => false,
				'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
			),
			array(
				'name'=>'Departamento',
				'value' => function($data)
				{
					$id = $data->rfcSupervisor0->cveDepartamento;
					$query = "select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = '$id' ";
					$nombre = Yii::app()->db->createCommand($query)->queryAll();

					return $nombre[0]['dscDepartamento'];
				},
				'filter' => false,
				'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
			),*7
			/*array(
				'name'=>'horas_totales',
				'filter' => false,
				'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
			),*/
			array(
				'name'=>'idPeriodoPrograma.periodo_programa',
				'filter' => CHtml::activeDropDownList($modelSSProgramas,
													'id_periodo_programa',
													array('1'=>'SEMESTRAL', '2'=>'ANUAL', '3'=>'ESPECIAL'),
													array('prompt'=>'-- Filtrar por --')
				),
				'htmlOptions' => array('width'=>'170px', 'class'=>'text-center')
			),
			/*array(
				'header'=>'Supervisor',
				'value'=>function($data)
				{
					$rfc_sup = $data->rfcSupervisor;
					$query =
					"
					SELECT (nombre_supervisor||' '||apell_paterno||' '|| apell_materno) as name
					FROM ss_supervisores_programas WHERE \"rfcSupervisor\" ='$rfc_sup'
					";
					$name_completo_supervisor = Yii::app()->db->createCommand($query)->queryAll();

					return $name_completo_supervisor[0]['name'];
				},
				'filter' => false,
				'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
			),*/
			array(
				'name'=>'lugares_disponibles', //numero_estudiantes_solicitados
				'filter' => false,
				'type' => 'raw',
				'value' => function($data)
				{
					return ($data->lugares_disponibles > 0) ? '<span style="font-size:18px" class="label label-success">'.$data->lugares_disponibles.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->lugares_disponibles.'</span>';
				},
				'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
			),
			/*'descripcion_objetivo_programa',
			'impacto_social_esperado',
			'beneficiarios_programa',
			'actividades_especificas_realizar',
			'mecanismos_supervision_servicio_social',
			'perfil_estudiante_requerido',
			'id_apoyo_economico_prestador_servicio_social',
			'id_tipo_apoyo_economico',*/
			array(
				'name'=>'Apoyo',
				'value' => function($data)
				{
					$id = $data->id_apoyo_economico_prestador;

					switch($id)
					{
						case 1: return "SI"; break; case 2: return "NO"; break; case 3: return "OTRO"; break;
					}
				},
				'filter' => CHtml::activeDropDownList($modelSSProgramas,
													'id_apoyo_economico_prestador',
													array('1'=>'HAY APOYO', '2'=>'NO HAY APOYO', '3'=>'OTRO TIPO DE APOYO'),
													array('prompt'=>'-- Filtrar por --')
				),
				'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
			),
			/*array(//id_recibe_capacitacion
				'header' => 'Recibe Capacitación',
				'value'=>function($data)
				{
					return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO" ;
				},
				'filter' => false,
				'htmlOptions' => array('width'=>'75px')
			),*/
			/*'fecha_registro_programa_servicio_social',
			'fecha_modificacion_programa_servicio_social',
			'id_clasificacion_area_servicio_social',*/
			array(
				'class'=>'CButtonColumn',
				'template'=>'{detAlumProgramaServicioSocial}',
				'header'=>'Detalle',
				'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
				'buttons'=>array
				(
					'detAlumProgramaServicioSocial' => array
					(
						'label'=>'Detalle Programa',
						'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleAlumProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
						'imageUrl'=>'images/servicio_social/detalle_32.png'
					),
				),
			),
			[
				'class' => 'CButtonColumn',
				'template' => '{selecProgramaServicioSocial}, {bloqueado}, {haylugaresDisponibles}, {cursando}, {fechaout}', // buttons here...
				'header' => 'Seleccionar Programa',
				'htmlOptions' => array('width'=>'180px','class'=>'text-center'),
				'buttons' => [ // custom buttons options here...
					'selecProgramaServicioSocial' => [
						'label'   => 'Seleccionar Programa',
						'url'     => 'Yii::app()->createUrl("serviciosocial/ssProgramas/seleccionarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
						'imageUrl'=>'images/servicio_social/select_programa_32.png',
						'visible' => ' "'.$hay_datos.'" == false AND "'.$fecha_limite.'" == true AND $data->lugares_disponibles != 0',
						/*'options' => [
							'data-confirm' => 'Confirmar la Solicitud al Programa ?',
						],
						'click'   => $approveJs,*/
					],
					'bloqueado' => [
						'label' => 'Ya enviaste una Solicitud',
						//'url' => '#',
						'imageUrl' => 'images/servicio_social/bloquedo_32.png',
						'visible' => " '$hay_datos' == true AND '$cursando' == false",
					],
					'haylugaresDisponibles' => [
						'label'   => 'Lugares Agotados',
						//'url'     => '#',
						'imageUrl'=> 'images/servicio_social/programa_lleno_32.png',
						'visible' => '$data->lugares_disponibles == 0',
					],
					'cursando' => [
						'label'   => 'Cursando Servicio Social',
						//'url'     => '#',
						'imageUrl'=> 'images/servicio_social/aceptar_32.png',
						'visible' => " '$cursando' == true",
					],
					'fechaout' => [
						'label'   => 'Fuera de Fecha de Registro',
						//'url'     => '#',
						'imageUrl'=> 'images/servicio_social/fechaout_32.png',
						'visible' => " '$fecha_limite' == false ",
					],
				],
			],//Fin
			/*array(
				'class'=>'CButtonColumn',
				'template'=>'{selecProgramaServicioSocial},{bloqueado},{haylugaresDisponibles},{cursando}, {fechaout}',
				'header'=>'Seleccionar Programa',
				'htmlOptions'=>array('width:180px', 'class'=>'text-center'),
				'buttons'=>array
				(
					'selecProgramaServicioSocial' => array
					(
						'label'=>'Seleccionar Programa',
						'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/seleccionarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
						'imageUrl'=>'images/select_programa_32.png',
						'visible' => ' "'.$hay_datos.'" == false AND "'.$fecha_limite.'" == true AND $data->lugares_disponibles != 0',
					),
					'bloqueado' => array
					(
						'label'=>'Ya enviaste una Solicitud',
						'url'=>'#',
						'imageUrl'=>'images/bloquedo_32.png',
						'visible' => " '$hay_datos' == true AND '$cursando' == false",
					),
					'haylugaresDisponibles' => array
					(
						'label'=>'Lugares Agotados',
						'url'=>'#',
						'imageUrl'=>'images/programa_lleno_32.png',
						//'visible' => ' $hay_datos == false '
						'visible' => '$data->lugares_disponibles == 0'
					),
					'cursando' => array
					(
						'label'=>'Cursando Servicio Social',
						'url'=>'#',
						'imageUrl'=>'images/aceptar_32.png',
						'visible' => " '$cursando' == true",
					),
					'fechaout' => array
					(
						'label'=>'Fuera de Fecha de Registro',
						'url'=>'#',
						'imageUrl'=>'images/fechaout_32.png',
						'visible' => " '$fecha_limite' == false ",
					),
				),
			),*/
		),
	)); 

	echo "<br><br><br><br><br>";
	echo "<br><br><br><br><br>";

	
	?>

	<?php }elseif($completoServicioSocial == true){ ?>
		<br><br><br><br><br>
		<div class="alert alert-success">
			<p><strong>
				<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
				Has completado tu Servicio Social Satisfactoriamente, Felicidades !!!.
			</strong></p>
		</div>

		<br>
		<div class="row">
			<div class="col-md-12" align="center">
					<?php
						echo '<img align="center" height="200" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/congrats.png"/>';
					?>
			</div>
		</div>

	<?php }elseif($servicioSocialFueCancelado == 1){ ?>
    <br><br><br><br><br>
		<div class="alert alert-danger">
			<p><strong>
				<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
				No puedes llevar Servicio Social en el mismo Periodo Escolar y Año si fue CANCELADO por el Jefe de Oficina de Servicio Social. Intenta el proximo semestre.
			</strong></p>
		</div>

		<br>
		<div class="row">
			<div class="col-md-12" align="center">
					<?php
						echo '<img align="center" height="200" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/lock.png"/>';
					?>
			</div>
		</div>
	<?php }elseif($tomoSsocialMismoPeriodoYAnio == 1){ ?>
    <br><br><br><br><br>
		<div class="alert alert-danger">
			<p><strong>
				<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
				No puedes realizar el Servicio Social mas de una vez en el mismo Periodo Escolar y Año.
			</strong></p>
		</div>

		<br>
		<div class="row">
			<div class="col-md-12" align="center">
					<?php
						echo '<img align="center" height="200" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/lock.png"/>';
					?>
			</div>
		</div>
  <?php } ?>
<?php }else{ ?>
	<br><br><br><br>
    <div class="alert alert-danger">
        <p><strong>
            <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
            <b>No tienes ACTIVADO el Servicio Social. Asegurate de haber realizado el Pre-registro o acude al Departamento de Vinculación para revisar tu caso.</b>
        </strong></p>
    </div>

    <br><br><br>
    <div align="center">
        <?php echo '<img align="center" heigth="100" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/sad_512.png"/>'; ?>

        <br><br><br><br><br><br><br>
        <?php echo CHtml::link('Volver al Menú Principal', array('/'), array('class'=>'btn btn-success center')); ?>
    </div>
<?php } ?>
<br><br><br>
