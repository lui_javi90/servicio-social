<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Detalle
                </h4>
            </div>
            <div class="panel-body">

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('nombre_programa')); ?>:</b>
                <?php echo CHtml::encode($modelSSProgramas->nombre_programa); ?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('lugar_realizacion_programa')); ?>:</b>
                <?php echo CHtml::encode($modelSSProgramas->lugar_realizacion_programa); ?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>
                <?php echo '<span style="font-size:14px" class="label label-success">'.CHtml::encode($modelSSProgramas->horas_totales).'</span>'; ?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_periodo_programa')); ?>:</b>
                <?php echo CHtml::encode($modelSSProgramas->id_periodo_programa); ?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('numero_estudiantes_solicitados')); ?>:</b>
                <?php echo '<span style="font-size:14px" class="label label-success">'.CHtml::encode($modelSSProgramas->numero_estudiantes_solicitados).'</span>'; ?></p>

                <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('descripcion_objetivo_programa')); ?>:</b>
                <?php echo CHtml::encode($modelSSProgramas->descripcion_objetivo_programa); ?></p>
                
            </div>
        </div>
    </div>
</div>