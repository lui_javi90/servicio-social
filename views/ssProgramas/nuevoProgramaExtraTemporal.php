<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

if($modelSSProgramas->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Programas Extratemporales' => array('ssProgramas/listaProgramasExtraTemporales'),
		'Nuevo Programa ExtraTemporal'
		
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Programas Extratemporales' => array('ssProgramas/listaProgramasExtraTemporales'),
		'Editar Programa ExtraTemporal'
		
	);
}

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSProgramas->isNewRecord) ? "Nuevo Programa ExtraTemporal" : "Editar Programa ExtraTemporal"; ?>
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Los Programas ExtraTemporales por lo regular tienen un duración no mayor a dos meses.
    </strong></p>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSProgramas->isNewRecord) ? "Nuevo Programa ExtraTemporal" : "Editar Programa ExtraTemporal"; ?> 
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formProgramaServicioSocialExtraTemporal', array(
											'modelSSProgramas'=>$modelSSProgramas,
											'lista_unidades_receptoras' => $lista_unidades_receptoras,
											'lista_departamentos' => $lista_departamentos,
											'lista_periodos_programas' => $lista_periodos_programas,
											'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
											'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
											'lista_tipo_apoyo' => $lista_tipo_apoyo,
											'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
											'lista_status' => $lista_status,
										)); ?>
			</div>
		</div>
	</div>
</div>