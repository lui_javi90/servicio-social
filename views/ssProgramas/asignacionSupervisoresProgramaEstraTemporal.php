<?php

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas ExtraTemporales' => array('ssProgramas/listaProgramasExtraTemporales'),
    'Asignar Supervisor al Programa ExtraTemporal'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Asignar Supervisor al Programa
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h6 class="panel-title">
				Agregar Supervisor Programa ExtraTemporal
			</h6>
        </div>
        <div class="panel-body">
        <?php if($modelSSProgramas->id_tipo_programa == 2){ ?>
                <!--BLOQUE PARA AGREGAR SUPERVISOR EXTERNO AL PROGRAMA-->
                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ss-supervisores-programas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('autocomplete'=>'off')
                )); ?>

                <?php echo $form->errorSummary($modelSSSupervisoresProgramas); ?>

                <div class="col-lg-4">
                    <div align="center">
                        <br><br><br>
                        <h4>Elegir un Supervisor para el Programa</h4>
                    </div>

                    <br><br>
                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
                        <?php echo $form->dropDownList($modelSSSupervisoresProgramas,
                                                    'rfcSupervisor',
                                                    $lista_supervisores,
                                                    //array('prompt'=>'-- Supervisores --', 'class'=>'form-control', 'required'=>'required'));
                                                    array(
                                                        'prompt'=>'-- Elegir Supervisor --', 'class'=>'form-control', 'required'=>'required',
                                                        'ajax'=>array(
                                                            'type'=>'POST',
                                                            'dataType'=>'json',
                                                            'data' => array(
                
                                                                'rfcSupervisor'=>'js:$(\'#SsSupervisoresProgramas_rfcSupervisor option:selected\').val()',
                                                            ),
                                                            'url'=>CController::createUrl('ssProgramas/mostrarDetalleSupervisorExterno'),
                                                            'success'=>'function(data) {
                                                                $("#SsSupervisoresProgramas_nombre_supervisor").val(data.nombre);
                                                                $("#SsSupervisoresProgramas_apell_paterno").val(data.apellido_pat);
                                                                $("#SsSupervisoresProgramas_apell_materno").val(data.apellido_mat);
                                                                $("#SsSupervisoresProgramas_cargo_supervisor").val(data.cargo);
                                                                $("#SsSupervisoresProgramas_foto_supervisor").val(data.foto);

                                                                var image = new Image();
                                                                image.src = "/images/supervisores/masculino.png";

                                                            }',
                                                        ))
                                                    );?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
                    </div>

                    <br><br><br><br><br>
                </div>

                <div class="col-lg-8">
                    <h3 align="center">Detalle del Supervisor Seleccionado</h3>

                    <div align="center" id="image">
                        <?php  

                            //die();
                       
                        //echo "data:".$modelSSSupervisoresProgramas->foto_supervisor;
                        //echo '<img align="center" id="var" width="250" heigth="200" src="'. Yii::app()->request->baseUrl.'/images/supervisores/'.$var2.'"/>';
                        ?>
                    </div>

                    <br><br>
                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'nombre_supervisor'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'nombre_supervisor',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'nombre_supervisor'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'apell_paterno'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'apell_paterno',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'apell_paterno'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'apell_materno'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'apell_materno',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'apell_materno'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'cargo_supervisor'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'cargo_supervisor',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'cargo_supervisor'); ?>
                    </div>
                </div>

                <br><br>
                <div align="center" class="form-group">
                    <?php echo CHtml::submitButton($modelSSSupervisoresProgramas->isNewRecord ? 'Agregar Supervisor' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                </div>

                <?php $this->endWidget(); ?>
                <!--BLOQUE PARA AGREGAR SUPERVISOR EXTERNO AL PROGRAMA-->
        <?php }else{ ?>
         <!--BLOQUE PARA AGREGAR SUPERVISOR INTERNO AL PROGRAMA-->
                    <?php
                    /* @var $this HEmpleadosController */
                    /* @var $model HEmpleados */
                    /* @var $form CActiveForm */
                    ?>

                    <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'hempleados-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                    )); ?>

                    <div class="col-lg-4">
                    <div align="center">
                        <br><br><br>
                        <h4>Elegir un Supervisor para el Programa</h4>
                    </div>

                    <br><br>
                        <?php echo $form->errorSummary($modelHEmpleados); ?>
                        <div class="form-group">
                            <p><b>RFC del Supervisor </b><span class="required">*</span></p>
                            <?php echo $form->dropDownList($modelHEmpleados,
                                                        'rfcEmpleado', 
                                                        $lista_supervisores,
                                                        array(
                                                            'prompt'=>'-- Elegir Supervisor --', 'class'=>'form-control chosen-select', 'required'=>'required',
                                                            'ajax'=>array(
                                                                'type'=>'POST',
                                                                'dataType'=>'json',
                                                                'data' => array(
                    
                                                                    'rfcEmpleado'=>'js:$(\'#HEmpleados_rfcEmpleado option:selected\').val()',
                                                                ),
                                                                'url'=>CController::createUrl('ssProgramas/mostrarDetalleSupervisorInterno'),
                                                                'success'=>'function(data) {
                                                                    $("#HEmpleados_nmbEmpleado").val(data.nombre);
                                                                    $("#HEmpleados_apellPaterno").val(data.apellido_pat);
                                                                    $("#HEmpleados_apellMaterno").val(data.apellido_mat);
                                                                    $("#HEmpleados_cveDepartamentoEmp").html(data.departamento);
    
                                                                }',
                                                            ))
                                                        ); ?>
                            <?php echo $form->error($modelHEmpleados,'rfcEmpleado'); ?>
                        </div>
                        <br><br><br><br><br>
                    </div>

                        <div class="col-lg-8">
                            <h3 align="center">Detalle del Supervisor Seleccionado</h3>

                            <div class="form-group">
                                <b>Nombre Completo</b>
                                <?php echo $form->textField($modelHEmpleados,'nmbEmpleado',array('class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                                <?php echo $form->error($modelHEmpleados,'nmbEmpleado'); ?>
                            </div>

                            <div class="form-group">
                                <b>Apellido Paterno</b>
                                <?php echo $form->textField($modelHEmpleados,'apellPaterno',array('class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                                <?php echo $form->error($modelHEmpleados,'apellPaterno'); ?>
                            </div>

                            <div class="form-group">
                                <b>Apellido Materno</b>
                                <?php echo $form->textField($modelHEmpleados,'apellMaterno',array('class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                                <?php echo $form->error($modelHEmpleados,'apellMaterno'); ?>
                            </div>

                             <div class="form-group">
                                <b>Departamento</b>
                                <?php echo $form->dropDownList($modelHEmpleados,
                                                            'cveDepartamentoEmp',
                                                            array(),
                                                            array('class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
                                                            ); ?>
                                <?php echo $form->error($modelHEmpleados,'cveDepartamentoEmp'); ?>
                            </div>

                            <br><br>
                            <?php
                            //Si es falso quiere decir que no ha sido asignado ningun supérvisor al Programa
                            if($haySupervisorAsigando == false){ ?>
                            <div class="form-group">
                                <?php echo CHtml::submitButton($modelHEmpleados->isNewRecord ? 'Agregar Supervisor' : 'Agregar Supervisor', array('class'=>'btn btn-primary')); ?>
                            </div>
                            <?php } ?>
                        </div>

                    <?php $this->endWidget(); ?>

                    </div><!-- form -->

                <!--BLOQUE PARA AGREGAR SUPERVISOR INTERNO AL PROGRAMA-->
        <?php }?>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Supervisor Agregado al Programa ExtraTemporal
		</span>
	</h2>
</div>

<br>
<?php 
    if($modelSSProgramas->id_tipo_programa == 2){

    $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-supervisores-programas-grid',
    'dataProvider'=>$modelSSSupervisoresProgramas->searchXSupervisorAgrEmpresa($modelSSProgramas->id_programa),
    //'filter'=>$modelSSSupervisoresProgramas,
    'columns'=>array(
        array(
            'name' => 'rfcSupervisor',
            'filter' => false,
            'htmlOptions'=> array('width'=>'80px', 'class' => 'text-center')
        ),
        array(
            'name' => 'nombre_supervisor',
            'filter' => false,
            'htmlOptions'=> array('width'=>'150px', 'class' => 'text-center')
        ),
        array(
            'name' => 'apell_paterno',
            'filter' => false,
            'htmlOptions'=> array('width'=>'50px', 'class' => 'text-center')
        ),
        array(
            'name' => 'apell_materno',
            'filter' => false,
            'htmlOptions'=> array('width'=>'50px', 'class' => 'text-center')
        ),
        array(
            'name' => 'dscDepartamento',
            'filter' => false,
            'htmlOptions'=> array('width'=>'150px', 'class' => 'text-center')
        ),
        array(
            'name' => 'cargo_supervisor',
            'filter' => false,
            'htmlOptions'=> array('width'=>'150px', 'class' => 'text-center')
        ),
    ),
)); ?>

<?php }else if($modelSSProgramas->id_tipo_programa == 1){ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'resp-int-grid',
    'dataProvider'=>$modelSSResponsableProgramaInterno->searchSupervisorXPrograma($modelSSProgramas->id_programa),
    //'filter'=>$modelHEmpleados,
    'columns'=>array(
        array(
            'name' => 'rfcEmpleado',
            'filter' => false,
            'htmlOptions'=> array('width'=>'50px', 'class' => 'text-center')
        ),
        array(
            'header' => 'Nombres',
            'value' => function($data)
            {
                $query = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$data->rfcEmpleado' ";
                $name = Yii::app()->db->createCommand($query)->queryAll();

                return $name[0]['nmbEmpleado'];
            },
            'filter' => false,
            'htmlOptions'=> array('width'=>'150px', 'class' => 'text-center')
        ),
        array(
            'header' => 'Apellido Paterno',
            'value' => function($data)
            {
                $query = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$data->rfcEmpleado' ";
                $ape_pat = Yii::app()->db->createCommand($query)->queryAll();

                return $ape_pat[0]['apellPaterno'];
            },
            'filter' => false,
            'htmlOptions'=> array('width'=>'70px', 'class' => 'text-center')
        ),
        array(
            'name' => 'Apellido Materno',
            'value' => function($data)
            {
                $query = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$data->rfcEmpleado' ";
                $ape_mat = Yii::app()->db->createCommand($query)->queryAll();

                return $ape_mat[0]['apellMaterno'];
            },
            'filter' => false,
            'htmlOptions'=> array('width'=>'70px', 'class' => 'text-center')
        ),
        array(
            'header' => 'Departamento',
            'filter' => false,
            'value' => function($data)
            {
                $query = "select \"dscDepartamento\" as depto from public.\"H_empleados\" hemp
                join public.\"H_departamentos\" hdep 
                on hdep.\"cveDepartamento\" = \"cveDepartamentoEmp\"
                where \"rfcEmpleado\" = '$data->rfcEmpleado' ";
                $ape_mat = Yii::app()->db->createCommand($query)->queryAll();

                return $ape_mat[0]['depto'];
            },
            'htmlOptions'=> array('width'=>'250px', 'class' => 'text-center')
        ),
        array(
            'header' => 'Rol Supervisor',
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->superv_principal_int == true) ? '<span class="label label-success">P R I N C I P A L</span>' : '<span class="label label-info">S E C U N D A R I O</span>';
            },
            'htmlOptions'=> array('width'=>'60px', 'class' => 'text-center')
        )
    ),
)); ?>

<?php }?>

<br><br><br>
<hr>
<br>
<div align="center">
    <?php 
        if($haySupervisorAsigando == true)
            echo CHtml::link('Agregar Horario del Programa', array('nuevoHorarioDiasHabilesProgramaExtraTemporal','id_programa' => $modelSSProgramas->id_programa), array('class'=>'btn btn-primary'));
    ?>
</div>
<br><br><br>