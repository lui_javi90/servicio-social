<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
	'Programa de Servicio Social',
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Programa de Servicio Social
		</span>
	</h2>
</div>

<!--VALIDAR QUE NO PUEDA SER SUPERVISOR DE DOS PROGRAMAS SIMULTANEAMENTE O QUE NO PUEDE CREAR PROGRAMAS DESPUES DE LA FECHA DE INICIO DE SERVICIO SOCIAL-->
<?php if($id_registro_fechas_ssocial != 0)
	{
?>
<?php 
		  //id_status_programa = 1 es AlTA
		  //Si estatus es 1 ALTA o No tiene Programa vigente entonces muestra esta pantalla
		  if(($modelProgramas === NULL or empty($modelProgramas)) or $modelProgramas[0]['id_status_programa'] == 1)
		  {
?>
			<br><br><br><br><br>
			<div align="right">
			<?php echo ($asignado == false AND $fec_inicio_servicio_social_actual == true) ? CHtml::link('Nuevo Registro Programa', array('nuevoProgramaServicioSocialSupervisor', 'id_registro_fechas_ssocial'=>$id_registro_fechas_ssocial), array('class'=>'btn btn-success')) : ''; ?>
			<?php //echo CHtml::link('Nuevo Registro Programa', array('nuevoProgramaServicioSocialSupervisor', 'id_registro_fechas_ssocial'=>$id_registro_fechas_ssocial), array('class'=>'btn btn-success')) ?>
			</div>
			<!--VALIDAR QUE NO PUEDA SER SUPERVISOR DE DOS PROGRAMAS SIMULTANEAMENTE O QUE NO PUEDE CREAR PROGRAMAS DESPUES DE LA FECHA DE INICIO DE SERVICIO SOCIAL-->

			<br><br><br>
			<div class="alert alert-danger">
			<p><strong>
			<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			<b>Solo podrá ser supervisor de un Programa. Tendra que finalizar el Programa actual para poder dar de alta otro Programa. Puede tener otro Programa
				con otro supervisor a cargo.</b>
			</strong></p>
			</div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'supervisor-programas-servicio-social-grid',
				'dataProvider'=>$modelSSProgramas->searchProgramaSupervisor($rfcsupervisor, $id_tipo_programa),
				'filter'=>$modelSSProgramas,
				'columns'=>array(
					//'id_programa',
					array(
						'name'=>'nombre_programa',
						'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
					),
					/*array(
						'header'=>'Empresa',
						'name' => 'idUnidadReceptora.nombre_unidad_receptora',
						'filter' => false,
						'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
					),*/
					/*array(
						'name'=>'Departamento',
						'value' => function($data)
						{
							$id = $data->rfcSupervisor0->cveDepartamento;
							$query = "select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = '$id' ";
							$nombre = Yii::app()->db->createCommand($query)->queryAll();

							return $nombre[0]['dscDepartamento'];
						},
						'filter' => false,
						'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
					),*/
					array(
						'name'=>'horas_totales',
						'filter' => false,
						'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
					),
					array(
						'name'=>'idPeriodoPrograma.periodo_programa',
						'filter' => false,
						'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
					),
					/*array(
						'header'=>'Supervisor',
						'value'=>function($data)
						{
							$rfc_sup = $data->rfcSupervisor;
							$query =
							"
							SELECT (nombre_supervisor||' '||apell_paterno||' '|| apell_materno) as name
							FROM pe_planeacion.ss_supervisores_programas WHERE \"rfcSupervisor\"='$rfc_sup'
							";
							$name_completo_supervisor = Yii::app()->db->createCommand($query)->queryAll();

							return $name_completo_supervisor[0]['name'];
						},
						'filter' => false,
						'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
					),*/
					array(
						'name'=>'lugares_disponibles', //numero_estudiantes_solicitados
						'filter' => false,
						'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{editSupProgramaServicioSocial}',
						'header'=>'Editar',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'editSupProgramaServicioSocial' => array
							(
								'label'=>'Asignar Numero Alumnos a Programas',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/editarSupervisorProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/editar_32.png'
							),
						),
					),
				array(
						'class'=>'CButtonColumn',
						'template'=>'{editHorProgramaSSocial}',
						'header'=>'Editar Horario',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'editHorProgramaSSocial' => array
							(
								'label'=>'Asignar Numero Alumnos a Programas',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/listaHorarioProgramaSupervisor", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/horario_32.png'
							),
						),
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{detSupProgramaServicioSocial}',
						'header'=>'Detalle',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'detSupProgramaServicioSocial' => array
							(
								'label'=>'Detalle Programa',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleSupervisorProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/detalle_32.png'
							),
						),
					),
					array(
						'header'=>'Estatus', //numero_estudiantes_solicitados
						'filter' => false,
						'type' => 'raw',
						'value' => function($data)
						{
							$id = trim($data->id_status_programa);

							switch($id)
							{
								case 1:
									return '<span style="font-size:14px;" class="label label-success">ALTA</span>';
								break;
								case 6:
									return '<span style="font-size:14px;" class="label label-warning">PENDIENTE</span>';
								break;
								case 8:
									return '<span style="font-size:14px;" class="label label-info">NO HORARIO</span>';
								break;
							}
						},
						'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
					),
				),
			)); ?>

			<br><br><br><br><br>
			<br><br><br><br><br>
			
		<?php }elseif($modelProgramas[0]['id_status_programa'] == 6){ //Pendiente ?>


			<br><br><br>
			<div class="alert alert-info">
				<p align="center"><strong>
				<b>TU PROGRAMA se encuentra PENDIENTE <br> 
					de asignación de Alumnos Solicitados <br>
					y de las Horas a Liberar. El Jefe de Oficina de <br>
					Servicio Social se encarga de asignar dicha información, <br>
					una vez se hayan asignado visualizaras tu Programa <br>
					y podrá ser seleccionado por los alumnos que así lo requieran <br>
					en la Lista de Programas disponíbles para Servicio Social. <br>
				</b>
				</strong></p>
			</div>

			<br>
			<div class="row"><!--Row Principal-->
				<div class="col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h6 class="panel-title">Detalle del Programa</h6>
						</div>
						<div class="panel-body">

							<div style="padding-top:18px" class="col-lg-3" align="center">
								<?php
									echo '<img align="center" height="180" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/programa_default.png"/>';
								?>
							</div>
							<div class="col-lg-9">
								<?php if($modelProgramas[0]['id_tipo_programa'] == 1){ ?>
									<p><b><?php echo "Nombre del Programa"; ?></b>&nbsp;
									<?php echo $modelProgramas[0]['nombre_programa']; ?></p>

									<p><b><?php echo "Nombre del Supervisor: "; ?></b>&nbsp;
									<?php echo $modelProgramas[0]['nmbEmpleado']." ".$modelProgramas[0]['apellPaterno']." ".$modelProgramas[0]['apellMaterno']; ?></p>

									<p><b>RFC del Supervisor:</b>&nbsp;
									<?php echo $modelProgramas[0]['rfcEmpleado']; ?></p>

									<p><b>Nombre de la Empresa:</b>&nbsp;
									<?php echo "TNM INSTITUTO TECNOLÓGICO DE CELAYA"; ?></p>

									<p><b><?php echo "Periodo del Programa" ?></b>&nbsp;
									<?php 
										switch($modelProgramas[0]['id_periodo_programa'])
										{
											case 1:
												echo "SEMESTRAL";
												break;
											case 2:
												echo "ANUAL";
												break;
											case 3:
												echo "ESPECIAl";
												break;
											default :
												echo "ND";
										}
									?></p>

									<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>&nbsp;
									<?php echo '<span style="font-size:18px" class="label label-success">'.$modelProgramas[0]['horas_totales'].' horas</span>'; ?></p>

									<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>&nbsp;
									<?php echo ($modelProgramas[0]['id_tipo_programa'] == 1) ? '<span style="font-size:16px" class="label label-info">'.'INTERNO'.'</span>' : '<span style="font-size:16px" class="label label-info">'.'EXTERNO'.'</span>'; ?></p>

								<?php }else{ ?>
									<p><b><?php echo "Nombre del Programa"; ?></b>&nbsp;
									<?php echo $modelProgramas[0]['nombre_programa']; ?></p>

									<p><b><?php echo "Nombre del Supervisor: "; ?></b>&nbsp;
									<?php echo $modelProgramas[0]['nombre_supervisor']." ".$modelProgramas[0]['apell_paterno']." ".$modelProgramas[0]['apell_materno']; ?></p>

									<p><b>RFC del Supervisor:</b>&nbsp;
									<?php echo $modelProgramas[0]['rfcSupervisor']; ?></p>

									<p><b>Nombre de la Empresa:</b>&nbsp;
									<?php echo $modelProgramas[0]['nombre_unidad_receptora']; ?></p>

									<p><b><?php echo "Periodo del Programa" ?></b>&nbsp;
									<?php 
										switch($modelProgramas[0]['id_periodo_programa'])
										{
											case 1:
												echo "SEMESTRAL";
												break;
											case 2:
												echo "ANUAL";
												break;
											case 3:
												echo "ESPECIAl";
												break;
											default :
												echo "ND";
										}
									?></p>

									<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>&nbsp;
									<?php echo '<span style="font-size:18px" class="label label-success">'.$modelProgramas[0]['horas_totales'].' horas</span>'; ?></p>

									<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>&nbsp;
									<?php echo ($modelProgramas[0]['id_tipo_programa'] == 1) ? '<span style="font-size:16px" class="label label-info">'.'INTERNO'.'</span>' : '<span style="font-size:16px" class="label label-info">'.'EXTERNO'.'</span>'; ?></p>

								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</div><!--Row Principal-->

			<br><br><br><br><br>
			<div align="center">
				<?php echo CHtml::link('Volver a Menú Principal', array('/serviciosocial'), array('class'=>'btn btn-default')); ?>
			</div>

		<?php }elseif($modelProgramas[0]['id_status_programa'] == 8){ ?>

			<br><br><br><br><br>
			<div class="alert alert-warning">
			<p><strong>
			<b>
			* El Programa fue aceptado y validado, se agregaron las Horas a Liberar y el numero de Alumnos a admitir en tu Programa de Servicio Social.<br>
			* Una vez agregues el Horario que tendrá tu Programa pasará a Estatus ALTA y se mostrará a los Alumnos para iniciar las Inscripciones al mismo.
			</b>
			</strong></p>
			</div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'supervisor-programas-servicio-social-grid',
				'dataProvider'=>$modelSSProgramas->searchProgramaSupervisor($rfcsupervisor, $id_tipo_programa),
				'filter'=>$modelSSProgramas,
				'columns'=>array(
					//'id_programa',
					array(
						'name'=>'nombre_programa',
						'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
					),
					/*array(
						'header'=>'Empresa',
						'name' => 'idUnidadReceptora.nombre_unidad_receptora',
						'filter' => false,
						'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
					),*/
					/*array(
						'name'=>'Departamento',
						'value' => function($data)
						{
							$id = $data->rfcSupervisor0->cveDepartamento;
							$query = "select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = '$id' ";
							$nombre = Yii::app()->db->createCommand($query)->queryAll();

							return $nombre[0]['dscDepartamento'];
						},
						'filter' => false,
						'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
					),*/
					array(
						'name'=>'horas_totales',
						'filter' => false,
						'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
					),
					array(
						'name'=>'idPeriodoPrograma.periodo_programa',
						'filter' => false,
						'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
					),
					/*array(
						'header'=>'Supervisor',
						'value'=>function($data)
						{
							$rfc_sup = $data->rfcSupervisor;
							$query =
							"
							SELECT (nombre_supervisor||' '||apell_paterno||' '|| apell_materno) as name
							FROM pe_planeacion.ss_supervisores_programas WHERE \"rfcSupervisor\"='$rfc_sup'
							";
							$name_completo_supervisor = Yii::app()->db->createCommand($query)->queryAll();

							return $name_completo_supervisor[0]['name'];
						},
						'filter' => false,
						'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
					),*/
					array(
						'name'=>'lugares_disponibles', //numero_estudiantes_solicitados
						'filter' => false,
						'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{editSupProgramaServicioSocial}',
						'header'=>'Editar',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'editSupProgramaServicioSocial' => array
							(
								'label'=>'Asignar Numero Alumnos a Programas',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/editarSupervisorProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/editar_32.png'
							),
						),
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{editHorProgramaSSocial}',
						'header'=>'Editar Horario',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'editHorProgramaSSocial' => array
							(
								'label'=>'Asignar Numero Alumnos a Programas',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/listaHorarioProgramaSupervisor", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/horario_32.png'
							),
						),
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{detSupProgramaServicioSocial}',
						'header'=>'Detalle',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'detSupProgramaServicioSocial' => array
							(
								'label'=>'Detalle Programa',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleSupervisorProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/detalle_32.png'
							),
						),
					),
					array(
						'header'=>'Estatus', //numero_estudiantes_solicitados
						'filter' => false,
						'type' => 'raw',
						'value' => function($data)
						{
							$id = trim($data->id_status_programa);

							switch($id)
							{
								case 1:
									return '<span style="font-size:14px;" class="label label-success">ALTA</span>';
								break;
								case 6:
									return '<span style="font-size:14px;" class="label label-warning">PENDIENTE</span>';
								break;
								case 8:
									return '<span style="font-size:14px;" class="label label-info">NO HORARIO</span>';
								break;
							}
						},
						'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
					),
				),
			)); ?>


			<br><br><br><br><br>
			<br><br><br><br><br>
			
		<?php }elseif($modelProgramas != NULL){ //FINALIZO PROGRAMA
		?>

			<br><br><br><br><br>
			<div class="alert alert-danger">
			<p><strong>
			<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			<b>Solo podrá ser supervisor de un Programa. Tendra que finalizar el Programa actual para poder dar de alta otro Programa. Puede tener otro Programa
				con otro supervisor a cargo.</b>
			</strong></p>
			</div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'supervisor-programas-servicio-social-grid',
				'dataProvider'=>$modelSSProgramas->searchProgramaSupervisor($rfcsupervisor, $id_tipo_programa),
				'filter'=>$modelSSProgramas,
				'columns'=>array(
					//'id_programa',
					array(
						'name'=>'nombre_programa',
						'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
					),
					/*array(
						'header'=>'Empresa',
						'name' => 'idUnidadReceptora.nombre_unidad_receptora',
						'filter' => false,
						'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
					),*/
					/*array(
						'name'=>'Departamento',
						'value' => function($data)
						{
							$id = $data->rfcSupervisor0->cveDepartamento;
							$query = "select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = '$id' ";
							$nombre = Yii::app()->db->createCommand($query)->queryAll();

							return $nombre[0]['dscDepartamento'];
						},
						'filter' => false,
						'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
					),*/
					array(
						'name'=>'horas_totales',
						'filter' => false,
						'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
					),
					array(
						'name'=>'idPeriodoPrograma.periodo_programa',
						'filter' => false,
						'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
					),
					/*array(
						'header'=>'Supervisor',
						'value'=>function($data)
						{
							$rfc_sup = $data->rfcSupervisor;
							$query =
							"
							SELECT (nombre_supervisor||' '||apell_paterno||' '|| apell_materno) as name
							FROM pe_planeacion.ss_supervisores_programas WHERE \"rfcSupervisor\"='$rfc_sup'
							";
							$name_completo_supervisor = Yii::app()->db->createCommand($query)->queryAll();

							return $name_completo_supervisor[0]['name'];
						},
						'filter' => false,
						'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
					),*/
					array(
						'name'=>'lugares_disponibles', //numero_estudiantes_solicitados
						'filter' => false,
						'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{editSupProgramaServicioSocial}',
						'header'=>'Editar',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'editSupProgramaServicioSocial' => array
							(
								'label'=>'Asignar Numero Alumnos a Programas',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/editarSupervisorProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/editar_32.png'
							),
						),
					),
				array(
						'class'=>'CButtonColumn',
						'template'=>'{editHorProgramaSSocial}',
						'header'=>'Editar Horario',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'editHorProgramaSSocial' => array
							(
								'label'=>'Asignar Numero Alumnos a Programas',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/listaHorarioProgramaSupervisor", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/horario_32.png'
							),
						),
					),
					array(
						'class'=>'CButtonColumn',
						'template'=>'{detSupProgramaServicioSocial}',
						'header'=>'Detalle',
						'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
						'buttons'=>array
						(
							'detSupProgramaServicioSocial' => array
							(
								'label'=>'Detalle Programa',
								'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleSupervisorProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
								'imageUrl'=>'images/servicio_social/detalle_32.png'
							),
						),
					),
					array(
						'header'=>'Estatus', //numero_estudiantes_solicitados
						'filter' => false,
						'type' => 'raw',
						'value' => function($data)
						{
							$id = trim($data->id_status_programa);

							switch($id)
							{
								case 1:
									return '<span style="font-size:14px;" class="label label-success">ALTA</span>';
								break;
								case 6:
									return '<span style="font-size:14px;" class="label label-warning">PENDIENTE</span>';
								break;
								case 8:
									return '<span style="font-size:14px;" class="label label-info">NO HORARIO</span>';
								break;
							}
						},
						'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
					),
				),
			)); ?>

			<br><br><br><br><br>
			<br><br><br><br><br>

		<?php } ?>
		

<?php }else{ ?>

		<br><br><br>
		<div class="alert alert-danger">
		<p><strong>
		<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		<b>No hay datos sobre el Registro de Fechas para Servicio Social.</b>
		</strong></p>
		</div>

		<br><br>
		<div align="center">
			<?php echo '<img align="center" width="250" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/lock.png"/>'; ?>
		</div>
		<br><br><br>
<?php } ?>
