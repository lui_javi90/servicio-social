<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas ExtraTemporal' => array('ssProgramas/listaProgramasExtraTemporales'),
    'Detalle del Programa ExtraTemporal'
);

?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información del Programa ExtraTemporal
		</span>
	</h2>
</div>

<br><br>
<?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$modelSSProgramas,
        'attributes'=>array(
            //'id_programa_servicio_social',
            'nombre_programa', 
            array(
                'header'=>'Empresa',
                'name' => 'idUnidadReceptora.nombre_unidad_receptora'
            ),
            'lugar_realizacion_programa',
            /*array(
                'name'=>'Departamento',
                'value' => function($data)
                {
                    $id = $data->rfcSupervisor0->cveDepartamento;
                    $query = "select \"dscDepartamento\" from \"H_departamentos\" where \"cveDepartamento\" = '$id' ";
                    $nombre = Yii::app()->db->createCommand($query)->queryAll();
    
                    return $nombre[0]['dscDepartamento'];
                },
            ),*/
            'horas_totales',
            'idPeriodoPrograma.periodo_programa',//id_periodo_programa
            /*array(
                'name' => 'rfcSupervisor',
                'value' => function($data)
                {
                    $rfc_sup = $data->rfcSupervisor;
                    $query = 
                    "
                    SELECT (nombre_supervisor||' '|| apell_paterno || ' ' || apell_materno) as name
                    from ss_supervisores_programas 
                    where \"rfcSupervisor\" = '$rfc_sup' 
                    ";
                    $name = Yii::app()->db->createCommand($query)->queryAll();
                    
                    return (count($name) > 0) ? $name[0]['name'] : "NO ESPECIFICADO";
                }
            ),*/
            'numero_estudiantes_solicitados',
            'descripcion_objetivo_programa',
            'idTipoServicioSocial.tipo_servicio_social',//id_tipo_servicio_social
            'impacto_social_esperado',
            'beneficiarios_programa',
            'actividades_especificas_realizar',
            'mecanismos_supervision',
            'perfil_estudiante_requerido',
            'idApoyoEconomicoPrestador.descripcion_apoyo_economico',//id_apoyo_economico_prestador_servicio_social
            'idTipoApoyoEconomico.tipo_apoyo_economico',//id_tipo_apoyo_economico
            'apoyo_economico',
            array(
                'name' => 'fecha_registro_programa',
                'oneRow' => true,
                'value' => function($data)
                {
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				    $fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateProgramasReg($data->id_programa);
				    return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
                },
            ),
            array(
                'name' => 'fecha_modificacion_programa',
                'oneRow' => true,
                'value' => function($data)
                {
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				    $fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateProgramasMod($data->id_programa);
				    return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
                },
            ),
            array(
                'name' => 'id_recibe_capacitacion',
                'oneRow'=>true,
                'value' => function($data)
                {
                    return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO";
                },
            ),
            'idClasificacionAreaServicioSocial.clasificacion_area_servicio_social',//id_clasificacion_area_servicio_social
            'lugares_disponibles',
            array(
                'name' => 'id_tipo_programa',
                'oneRow'=>true,
                'value' => function($data)
                {
                    return ($data->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
                },
            ),
            'idStatusPrograma.descripcion_status',
            ),
)); ?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>

<br><br>
<?php
for($i=0; $i < count($modelSSHorarioDiasHabilesProgramas); $i++){
$this->widget('serviciosocial.components.DetailView4Col', array(
    'data'=>$modelSSHorarioDiasHabilesProgramas[$i],
    'attributes'=>array(
        //'id_horario',
        //'id_programa_servicio_social', id_dia_semana
        array(
            'name' => 'idDiaSemana.dia_semana',
            'oneRow'=>true,
        ),
        'hora_inicio',
        'hora_fin',
        //'horas_totales',
    ),
));
echo "<br><br>";
} 
?>

<br><br>
<div align="center">
    <?php echo CHtml::link('Entendido', array('listaProgramasExtraTemporales'), array('class'=>'btn btn-success')); ?>
</div>
<br><br>