<?php
/* @var $this ProgramasServicioSocialController */
/* @var $modelSSProgramas ProgramasServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sup-programas-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSProgramas); ?>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'nombre_programa'); ?>
		<?php echo $form->textField($modelSSProgramas,'nombre_programa',array('size'=>60,'maxlength'=>150, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'nombre_programa'); ?>
	</div>
	<?php }else{?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'nombre_programa'); ?>
		<?php echo $form->textField($modelSSProgramas,'nombre_programa',array('size'=>60,'maxlength'=>150, 'class'=>'form-control', 'readOnly'=>true)); ?>
		<?php echo $form->error($modelSSProgramas,'nombre_programa'); ?>
	</div>
	<?php } ?>

	<?php if($modelSSProgramas->isNewRecord) { ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSProgramas,'id_tipo_programa'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									  'id_tipo_programa',
									  $id_tipo_supervisor,
									  array('class'=>'form-control')
									  ); ?>
        <?php echo $form->error($modelSSProgramas,'id_tipo_programa'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSProgramas,'id_tipo_programa'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									  'id_tipo_programa',
									  $id_tipo_supervisor,
									  array('class'=>'form-control', 'disabled'=>'disabled')
									  ); ?>
        <?php echo $form->error($modelSSProgramas,'id_tipo_programa'); ?>
	</div>
	<?php } ?>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSProgramas,'id_unidad_receptora'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									'id_unidad_receptora',
									$id_empresa,
									array('class'=>'form-control')); ?>
        <?php echo $form->error($modelSSProgramas,'id_unidad_receptora'); ?>
    </div>
    <?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelSSProgramas,'id_unidad_receptora'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									'id_unidad_receptora',
									$id_empresa,
									array('class'=>'form-control', 'disabled'=>'disabled')); ?>
        <?php echo $form->error($modelSSProgramas,'id_unidad_receptora'); ?>
    </div>
	<?php } ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'lugar_realizacion_programa'); ?>
		<?php echo $form->textField($modelSSProgramas,'lugar_realizacion_programa',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'lugar_realizacion_programa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'horas_totales'); ?>
		<?php echo $form->textField($modelSSProgramas,'horas_totales',array('size'=>3,'maxlength'=>3, 'value'=>'0', 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
		<?php echo $form->error($modelSSProgramas,'horas_totales'); ?>
	</div>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_periodo_programa'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
										'id_periodo_programa',
										$lista_periodos_programas,
										array('prompt' => '--Seleccione periodo--', 'class'=>'form-control', 'required'=>'required',
										)); ?>
		<?php echo $form->error($modelSSProgramas,'id_periodo_programa'); ?>
	</div>
	<?php }else{?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_periodo_programa'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
										'id_periodo_programa',
										$lista_periodos_programas,
										array('prompt' => '--Seleccione periodo--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled',
										)); ?>
		<?php echo $form->error($modelSSProgramas,'id_periodo_programa'); ?>
	</div>
	<?php }?>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
		<?php echo $form->textField($modelSSProgramas,'numero_estudiantes_solicitados',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
	</div>
	<?php }else{ ?>
		<?php echo $form->labelEx($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
		<?php echo $form->textField($modelSSProgramas,'numero_estudiantes_solicitados',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
		<?php echo $form->error($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
	<?php } ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'descripcion_objetivo_programa'); ?>
		<?php echo $form->textArea($modelSSProgramas,'descripcion_objetivo_programa',array('size'=>60,'maxlength'=>400, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'descripcion_objetivo_programa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_tipo_servicio_social'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									   'id_tipo_servicio_social',
									   $lista_tipo_servicio_social,
									   array('prompt'=>'--Tipo Servicio Social--', 'class'=>'form-control', 'required'=>'required',
									   )); ?>
		<?php echo $form->error($modelSSProgramas,'id_tipo_servicio_social'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'impacto_social_esperado'); ?>
		<?php echo $form->textArea($modelSSProgramas,'impacto_social_esperado',array('size'=>60,'maxlength'=>300, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'impacto_social_esperado'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'beneficiarios_programa'); ?>
		<?php echo $form->textArea($modelSSProgramas,'beneficiarios_programa',array('size'=>60,'maxlength'=>300, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'beneficiarios_programa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'actividades_especificas_realizar'); ?>
		<?php echo $form->textArea($modelSSProgramas,'actividades_especificas_realizar',array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'actividades_especificas_realizar'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'mecanismos_supervision'); ?>
		<?php echo $form->textField($modelSSProgramas,'mecanismos_supervision',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'mecanismos_supervision'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'perfil_estudiante_requerido'); ?>
		<?php echo $form->textArea($modelSSProgramas,'perfil_estudiante_requerido',array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'perfil_estudiante_requerido'); ?>
	</div>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_apoyo_economico_prestador'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									   'id_apoyo_economico_prestador',
									   $ofrece_apoyo_economico,
									   array(
										'prompt'=>'--Selecciona Tipo Asesor--', 'class'=>'form-control', 'required'=>'required',
										'ajax'=>array(
										'type'=>'POST', 
										'dataType'=>'json',
										'data' => array
											(
												'id_apoyo_economico_prestador'=>'js:$(\'#SsProgramas_id_apoyo_economico_prestador option:selected\').val()',
											),
											'url'=>CController::createUrl('ssProgramas/siHayApoyoEconomico'),
											'success'=>'function(data) {
												$("#SsProgramas_id_tipo_apoyo_economico").html(data.tipos_apoyo);
											}',
												)
											)
									   ); ?>
		<?php echo $form->error($modelSSProgramas,'id_apoyo_economico_prestador'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_apoyo_economico_prestador'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									   'id_apoyo_economico_prestador',
									   $ofrece_apoyo_economico,
									   array(
										'prompt'=>'--Selecciona Tipo Asesor--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled',
										'ajax'=>array(
										'type'=>'POST', 
										'dataType'=>'json',
										'data' => array
											(
												'id_apoyo_economico_prestador'=>'js:$(\'#SsProgramas_id_apoyo_economico_prestador option:selected\').val()',
											),
											'url'=>CController::createUrl('ssProgramas/siHayApoyoEconomico'),
											'success'=>'function(data) {
												$("#SsProgramas_id_tipo_apoyo_economico").html(data.tipos_apoyo);
											}',
												))
									   ); ?>
		<?php echo $form->error($modelSSProgramas,'id_apoyo_economico_prestador'); ?>
	</div>
	<?php } ?>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_tipo_apoyo_economico'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
										'id_tipo_apoyo_economico',
										$lista_tipo_apoyo,
										array(
										'prompt'=>'--Tipo Apoyo Económico--', 'class'=>'form-control', 'required'=>'required',
										'onchange'=>'if($(this).val() == 4){
											$("#SsProgramas_apoyo_economico").val("NINGUNO").prop( "disabled", true ); 
										}else{
											$("#SsProgramas_apoyo_economico").val("").prop( "disabled", false ); 
										}')
										); ?>
		<?php echo $form->error($modelSSProgramas,'id_tipo_apoyo_economico'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_tipo_apoyo_economico'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
										'id_tipo_apoyo_economico',
										$lista_tipo_apoyo,
										array(
										'prompt'=>'--Tipo Apoyo Económico--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled',
										'onchange'=>'if($(this).val() == 4){
											$("#SsProgramas_apoyo_economico").val("NINGUNO").prop( "disabled", true ); 
										}else{
											$("#SsProgramas_apoyo_economico").val("").prop( "disabled", false ); 
										}')
										); ?>
		<?php echo $form->error($modelSSProgramas,'id_tipo_apoyo_economico'); ?>
	</div>
	<?php } ?>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'apoyo_economico'); ?>
		<?php echo $form->textField($modelSSProgramas,'apoyo_economico',array('size'=>60,'maxlength'=>100, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'apoyo_economico'); ?>
	</div>
	<?php }else{ ?>
		<?php echo $form->labelEx($modelSSProgramas,'apoyo_economico'); ?>
		<?php echo $form->textField($modelSSProgramas,'apoyo_economico',array('size'=>60,'maxlength'=>100, 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')); ?>
		<?php echo $form->error($modelSSProgramas,'apoyo_economico'); ?>
	<?php } ?>
	
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_recibe_capacitacion'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									  'id_recibe_capacitacion',
									  array('1'=>'SI', '2'=>'NO'),
									  array('prompt'=>'--Recibe Capacitación--', 'class'=>'form-control', 'required'=>'required')
									  ); ?>
		<?php echo $form->error($modelSSProgramas,'id_recibe_capacitacion'); ?>
	</div>

	<?php if($modelSSProgramas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_clasificacion_area_servicio_social'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									'id_clasificacion_area_servicio_social',
									$lista_clasificacion_areas_serv_social,
									array('prompt'=>'--Elige Clasificación Área--', 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSProgramas,'id_clasificacion_area_servicio_social'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSProgramas,'id_clasificacion_area_servicio_social'); ?>
		<?php echo $form->dropDownList($modelSSProgramas,
									'id_clasificacion_area_servicio_social',
									$lista_clasificacion_areas_serv_social,
									array('prompt'=>'--Elige Clasificación Área--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')); ?>
		<?php echo $form->error($modelSSProgramas,'id_clasificacion_area_servicio_social'); ?>
	</div>
	<?php } ?>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSProgramas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo Chtml::link('Cancelar', array('listaProgramasSupervisorServicioSocial'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->