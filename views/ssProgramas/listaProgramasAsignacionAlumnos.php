<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Asignación Alumnos a Programas Servicio Social',
);

$cancProgJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#asignacion-programas-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#asignacion-programas-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Asignar Numero de Alumnos a los Programas
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Asignación del numero de Alumnos y Horas a Liberar del Programa por el Jefe de Oficina de Servicio Social .
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'asignacion-programas-servicio-social-grid',
    'dataProvider'=>$modelSSProgramas->searchProgramasPendientes(),
    'filter'=>$modelSSProgramas,
    'columns'=>array(
        //'id_programa',
        array(
            'name' => 'nombre_programa',
            'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Empresa',
			'name'=>'idUnidadReceptora.nombre_unidad_receptora',
			'filter' => false,
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
        array(
            'name' => 'lugar_realizacion_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        /*array(
			'name'=>'Departamento',
			'value' => function($data)
			{
				$id = $data->rfcSupervisor0->cveDepartamento;
				$query = "select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = '$id' ";
				$nombre = Yii::app()->db->createCommand($query)->queryAll();

				return $nombre[0]['dscDepartamento'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),*/
        array(
            'name' => 'horas_totales',
            'filter' => false,
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estudiante Solicitados',
            'name' => 'numero_estudiantes_solicitados',
            'filter' => false,
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        /*
        'id_periodo_programa',
        'id_supervisor',
        'descripcion_objetivo_programa',
        'id_tipo_servicio_social',
        'impacto_social_esperado',
        'beneficiarios_programa',
        'actividades_especificas_realizar',
        'mecanismos_supervision_servicio_social',
        'perfil_estudiante_requerido',
        'id_apoyo_economico_prestador_servicio_social',
        'id_tipo_apoyo_economico',
        'apoyo_economico',
        'fecha_registro_programa_servicio_social',
        'fecha_modificacion_programa_servicio_social',
        'id_recibe_capacitacion',
        'id_clasificacion_area_servicio_social',
        */
        array(
			'class'=>'CButtonColumn',
			'template'=>'{asigAlumnosProgramaServicioSocial}',
			'header'=>'Asignar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'asigAlumnosProgramaServicioSocial' => array
				(
					'label'=>'Asignar Lugares y Horas a Liberar del Programas',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/asignarAlumnosProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/asignar_alumnos_32.png'
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{cancelarPrograma}',
			'header'=>'Cambiar <br>Estatus',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'cancelarPrograma' => array
				(
					'label'=>'Cancelar Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/cancelProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/cancelado_32.png',
                    'options' => array(
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Cancelación del Programa?', // custom attribute to hold confirmation message
                    ),
					'click'   => $cancProgJs, // JS string which processes AJAX request
                ),
			),
		),
    ),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
