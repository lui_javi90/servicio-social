<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas Servicio Social' => array('ssProgramas/listaProgramasAdminServicioSocial'),
    'Eliminar Lugares al Programa'
);

?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Eliminar Lugares al Programa
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <b>Información del Programa</b>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row"><!--Row 1-->
                    <div style = "padding-top:33px;" class="col-lg-3" align="center">
                        <?php
                            echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/programa_default.png"/>';
                        ?>
                    </div>
                    <div class="col-lg-9">
                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('nombre_programa')); ?>:</b>&nbsp;
                        <?php echo CHtml::encode($modelSSProgramasActual->nombre_programa); ?></p>

                        <p><b>RFC:</b>&nbsp;
                        <?php echo $rfcSupervisor; ?></p>

                        <p><b><?php echo "Nombre del Supervisor: "; ?></b>&nbsp;
                        <?php echo $nombre_supervisor; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('id_periodo_programa')); ?>:</b>&nbsp;
                        <?php echo $periodo_programa; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('id_tipo_programa')); ?>:</b>&nbsp;
                        <?php echo ($modelSSProgramasActual->id_tipo_programa == 1) ? '<span style="font-size:16px" class="label label-info">'.'INTERNO'.'</span>' : '<span style="font-size:16px" class="label label-info">'.'EXTERNO'.'</span>'; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('horas_totales')); ?>:</b>&nbsp;
                        <?php echo '<span style="font-size:18px" class="label label-success">'.CHtml::encode($modelSSProgramasActual->horas_totales).' horas</span>'; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('fecha_inicio_programa')); ?>:</b>&nbsp;
                        <?php echo $fec_inicio; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('fecha_fin_programa')); ?>:</b>&nbsp;
                        <?php echo $fec_fin; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('numero_estudiantes_solicitados')); ?>:</b>&nbsp;
                        <?php  echo ($est_sol > 0) ? '<span style="font-size:16px" class="label label-success">'.$est_sol.'</span>' : '<span style="font-size:16px" class="label label-danger">'.$est_sol.'</span>'; ?></p>

                        <p><b><?php echo CHtml::encode($modelSSProgramasActual->getAttributeLabel('lugares_disponibles')); ?>:</b>&nbsp;
                        <?php echo ($modelSSProgramasActual->lugares_disponibles > 0) ? '<span style="font-size:16px" class="label label-success">'.$modelSSProgramasActual->lugares_disponibles.'</span>' : '<span style="font-size:16px" class="label label-danger">'.$modelSSProgramasActual->lugares_disponibles.'</span>'; ?></p>

                    </div>
                </div><!--Row 1-->
                <div class="row"><!--Row 2-->
                    <div class="col-lg-12">

                    <br><br>
                    <div class="row">
                        <h2 class="subTitulo" align="center">
                            <span class="subTitulo_inside">
                                Eliminar Lugares
                            </span>
                        </h2>
                    </div>

                    <br><br><br>

                    <div class="alert alert-info">
                        <p><strong>
                            <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                            Solo debes ingresar el número de lugares que quieres eliminar, solo puedes eliminar como máximo el número de lugares disponibles en el Programa.
                        </strong></p>
                    </div>

                        <div class="form"><!--form-->

                            <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'ss-programas-form',
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                                'htmlOptions' => array('autocomplete'=>false)
                            )); ?>

                            <?php echo $form->errorSummary($modelSSProgramasActual); ?>

                            <div class="form-group">
                                <b><?php echo "Número de lugares a Eliminar "; ?><span class="required">*</span></b>
                                <?php echo $form->textField($modelSSProgramasActual,'numero_estudiantes_solicitados',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
                                <?php echo $form->error($modelSSProgramasActual,'numero_estudiantes_solicitados'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo CHtml::submitButton($modelSSProgramasActual->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                                <?php echo CHtml::link('Cancelar', array('listaProgramasAdminServicioSocial'), array('class'=>'btn btn-danger')); ?>
                            </div>

                            <?php $this->endWidget(); ?>

                        </div><!-- form -->

                    </div>
                </div><!--Row 2-->
            </div>
        </div>
    </div>
</div>

<br><br><br>