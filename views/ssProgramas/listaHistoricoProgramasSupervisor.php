<?php
    /* @var $this DefaultController */

    $this->breadcrumbs=array(
        'Servicio Social' => '?r=serviciosocial',
        'Histórico Programas de Servicio Social'
    
    );

    /*Renovar Programa de Servicio Social*/
    $JsRenovarPrograma = 'js:function(__event)
    {
        __event.preventDefault(); // disable default action

        var $this = $(this), // link/button
            confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
            url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

        if(confirm(confirm_message)) // Si se confirma la operacion entonces...
        {
            // perform AJAX request
            $("#ss-historico-programas-supervisor-grid").yiiGridView("update",
            {
                type	: "POST", // important! we only allow POST in filters()
                dataType: "json",
                url		: url,
                success	: function(data)
                {
                    console.log("Success:", data);
                    $("#ss-historico-programas-supervisor-grid").yiiGridView("update"); // refresh gridview via AJAX
                },
                error	: function(xhr)
                {
                    console.log("Error:", xhr);
                }
            });
        }
    }';
        /*Renovar Programa de Servicio Social*/
?>


<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Histórico Programas de Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        - Sino puedes Renovar un Programa, pueder ser que:<br>
		* No hay Registro de Servicio Social Nuevo Vigente. Deberas esperar que se de de Alta el nuevo registro.<br>
        * Ya renovaste o creaste un Programa en el Registro de Servicio Social vigente.
    </strong></p>
</div>

<!--<br><br><br><br>-->
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-historico-programas-supervisor-grid',
    'dataProvider'=>$modelSsProgramas->searchXListaHistoricoProgramasSupervisor($rfcEmpleado, $anio, $periodo),
    'filter'=>$modelSsProgramas,
    'columns'=>array(
        //'id_programa',
        array(
            'name' => 'nombre_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Empresa / Institución',
            'name' => 'idUnidadReceptora.nombre_unidad_receptora',
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'horas_totales',
            'filter' => false,
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPeriodoPrograma.periodo_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Año',
            'filter' => CHtml::activeDropDownList($modelSsProgramas,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $model = SsRegistroFechasServicioSocial::model()->findByPk($data->id_registro_fechas_ssocial);

		        if($model === NULL)
                    throw new CHttpException(404,'No existen datos de ese Periodo y Año.');
                    
                return '<span style="font-size:12px" class="label label-success">'.$model->anio.'</span>';
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Periodo',
            'filter' => CHtml::activeDropDownList($modelSsProgramas,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $model = SsRegistroFechasServicioSocial::model()->findByPk($data->id_registro_fechas_ssocial);

		        if($model === NULL)
                    throw new CHttpException(404,'No existen datos de ese Periodo y Año.');

                $per = ($model->id_periodo == 1) ? "ENERO-JUNIO" : "AGOSTO-DICIEMBRE";
                    
                return '<span style="font-size:12px" class="label label-info">'.$per.'</span>';
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{detProgramaHistorico}',
			'header'=>'Detalle',
			'htmlOptions' => array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detProgramaHistorico' => array
				(
					'label'=>'Detalle Programa Histórico',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleHistoricoSPrograma", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png'
				),
			),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{horarioPrograma}',
            'header'=>'Horario <br>Programa',
            'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
            'buttons'=>array
			(
				'horarioPrograma' => array
				(
                    'label'=>'Renovar Programa',
                    'url' => 'Yii::app()->createUrl("serviciosocial/ssProgramas/showHorarioProgramaHistorico", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/horario_32.png',
                    'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#parmdialog").dialog("open"); 
						
						return false;
					}',
                ),
            ),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{renovarPrograma},{noPuedeRenovPrograma}', //
			'header'=>'Renovar <br>Programa',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'renovarPrograma' => array
				(
					'label'=>'Renovar Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/renovarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/update_32.png',
                    'visible' => function($row, $data)
                    {
                        //Obtenemos el RFC del Supervisor
                        $qry_rfc = "select * from pe_planeacion.ss_responsable_programa_interno 
                                    where id_programa = '$data->id_programa' ";

                        $rfc = Yii::app()->db->createCommand($qry_rfc)->queryAll();

                        $rfcEmpleado = trim($rfc[0]['rfcEmpleado']);

                        $qry = "select p.id_programa from pe_planeacion.ss_programas p
                                join pe_planeacion.ss_registro_fechas_servicio_social rf
                                on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                                where rf.ssocial_actual = true AND p.id_status_programa = 3 ";

                        $rs = Yii::app()->db->createCommand($qry)->queryAll();

                        //Hay registro de servicio social vigente
                        $criteria = new CDbCriteria;
                        $criteria->condition = " ssocial_actual = true ";
                        $model = SsRegistroFechasServicioSocial::model()->find($criteria);

                        //Verificar que no tenga un programa en el registro de servicio social actual
                        $qry_2 = "select * from pe_planeacion.ss_programas p
                                    join pe_planeacion.ss_registro_fechas_servicio_social rf
                                    on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                                    join pe_planeacion.ss_responsable_programa_interno rpi
                                    on rpi.id_programa = p.id_programa
                                    where rf.ssocial_actual = true AND rpi.\"rfcEmpleado\" = '$rfcEmpleado' AND
                                    (p.id_status_programa = 1 OR p.id_status_programa = 8 OR p.id_status_programa = 6)
                                ";
                        
                        $prog_act = Yii::app()->db->createCommand($qry_2)->queryAll();

                        return (count($rs) == 0 AND $model != NULL AND count($prog_act) == 0) ? true : false;
                        //return true;

                    },
                    'options' => array(
						'title'        => 'Renovar Programa',
						'data-confirm' => 'Confirmar la RENOVACIÓN del Programa seleccionado?', // custom attribute to hold confirmation message
                    ),
					'click'   => $JsRenovarPrograma // JS string which processes AJAX request
                ),
                'noPuedeRenovPrograma' => array
                (
                    'label'=>'Renovar Programa Bloqueado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/renovarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/bloquedo_32.png',
                    'visible' => function($row, $data)
                    {
                        //Obtenemos el RFC del Supervisor
                        $qry_rfc = "select * from pe_planeacion.ss_responsable_programa_interno 
                                    where id_programa = '$data->id_programa' ";

                        $rfc = Yii::app()->db->createCommand($qry_rfc)->queryAll();

                        $rfcEmpleado = trim($rfc[0]['rfcEmpleado']);

                        $qry = "select p.id_programa from pe_planeacion.ss_programas p
                                join pe_planeacion.ss_registro_fechas_servicio_social rf
                                on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                                where rf.ssocial_actual = true AND p.id_status_programa = 3";

                        $rs = Yii::app()->db->createCommand($qry)->queryAll();

                        //Hay registro de servicio social vigente
                        $criteria = new CDbCriteria;
                        $criteria->condition = " ssocial_actual = true ";
                        $model = SsRegistroFechasServicioSocial::model()->find($criteria);

                        //Verificar que no tenga un programa en el registro de servicio social actual
                        $qry_2 = "select * from pe_planeacion.ss_programas p
                                    join pe_planeacion.ss_registro_fechas_servicio_social rf
                                    on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                                    join pe_planeacion.ss_responsable_programa_interno rpi
                                    on rpi.id_programa = p.id_programa
                                    where rf.ssocial_actual = true AND rpi.\"rfcEmpleado\" = '$rfcEmpleado' AND
                                    (p.id_status_programa = 1 OR p.id_status_programa = 8 OR p.id_status_programa = 6)
                                ";
                        
                        $prog_act = Yii::app()->db->createCommand($qry_2)->queryAll();

                        return (count($rs) > 0 OR $model === NULL OR count($prog_act) > 0) ? true : false;
                    },
                )
			),
		)
    ),
)); ?>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'parmdialog',
	'options' => array(
		'title' => 'Horario del Programa',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1100, 
		'height' => 550,
		/*'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),*/
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<br><br><br><br><br>
<br><br><br><br><br>