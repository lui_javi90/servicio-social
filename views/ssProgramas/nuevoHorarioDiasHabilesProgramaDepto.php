<?php

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas Servicio Social' => array('ssProgramas/listaProgramasAdminServicioSocial'),
    'Horario Dias Habiles del Programa'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Programa de Servicio Social
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Programa de Servicio Social
                </h6>
            </div>
            <div class="panel-body">
                <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'ss-programas-depto-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                    )); ?>

                    <?php echo $form->errorSummary($modelSSProgramas); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'nombre_programa'); ?>
                        <?php echo $form->textField($modelSSProgramas,'nombre_programa',array('size'=>60,'maxlength'=>150, 'class'=>'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'nombre_programa'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_tipo_programa'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                    'id_tipo_programa',
                                                    array('1'=>'INTERNO', '2'=>'EXTERNO'),
                                                    array('prompt'=>'--Tipo Programa--', 'class'=>'form-control', 'disabled'=>'disabled')
                                                    ); ?>
                        <?php echo $form->error($modelSSProgramas,'id_tipo_programa'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'lugar_realizacion_programa'); ?>
                        <?php echo $form->textField($modelSSProgramas,'lugar_realizacion_programa',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'lugar_realizacion_programa'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'horas_totales'); ?>
                        <?php echo $form->textField($modelSSProgramas,'horas_totales',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'horas_totales'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_periodo_programa'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                        'id_periodo_programa',
                                                        $lista_periodos_programas,
                                                        array('prompt' => '--Seleccione periodo--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled',
                                                        )); ?>
                        <?php echo $form->error($modelSSProgramas,'id_periodo_programa'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
                        <?php echo $form->textField($modelSSProgramas,'numero_estudiantes_solicitados',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'descripcion_objetivo_programa'); ?>
                        <?php echo $form->textArea($modelSSProgramas,'descripcion_objetivo_programa',array('size'=>60,'maxlength'=>400, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'descripcion_objetivo_programa'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_tipo_servicio_social'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                    'id_tipo_servicio_social',
                                                    $lista_tipo_servicio_social,
                                                    array('prompt'=>'--Tipo Servicio Social--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled'
                                                    )); ?>
                        <?php echo $form->error($modelSSProgramas,'id_tipo_servicio_social'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'impacto_social_esperado'); ?>
                        <?php echo $form->textArea($modelSSProgramas,'impacto_social_esperado',array('size'=>60,'maxlength'=>300, 'class'=>'form-control', 'required'=>'required','readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'impacto_social_esperado'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'beneficiarios_programa'); ?>
                        <?php echo $form->textArea($modelSSProgramas,'beneficiarios_programa',array('size'=>60,'maxlength'=>300, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'beneficiarios_programa'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'actividades_especificas_realizar'); ?>
                        <?php echo $form->textArea($modelSSProgramas,'actividades_especificas_realizar',array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'actividades_especificas_realizar'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'mecanismos_supervision'); ?>
                        <?php echo $form->textField($modelSSProgramas,'mecanismos_supervision',array('size'=>60,'maxlength'=>200, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'mecanismos_supervision'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'perfil_estudiante_requerido'); ?>
                        <?php echo $form->textArea($modelSSProgramas,'perfil_estudiante_requerido',array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'perfil_estudiante_requerido'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_apoyo_economico_prestador'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                    'id_apoyo_economico_prestador',
                                                    $ofrece_apoyo_economico,
                                                    array(
                                                        'prompt'=>'--Selecciona Tipo Asesor--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled',
                                                        'ajax'=>array(
                                                        'type'=>'POST', 
                                                        'dataType'=>'json',
                                                        'data' => array
                                                            (
                                                                'id_apoyo_economico_prestador'=>'js:$(\'#SsProgramas_id_apoyo_economico_prestador option:selected\').val()',
                                                            ),
                                                            'url'=>CController::createUrl('ssProgramas/siHayApoyoEconomico'),
                                                            'success'=>'function(data) {
                                                                $("#SsProgramas_id_tipo_apoyo_economico").html(data.tipos_apoyo);
                                                            }',
                                                                ))
                                                    ); ?>
                        <?php echo $form->error($modelSSProgramas,'id_apoyo_economico_prestador'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_tipo_apoyo_economico'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                        'id_tipo_apoyo_economico',
                                                        $lista_tipo_apoyo,
                                                        array(
                                                        'prompt'=>'--Tipo Apoyo Económico--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled',
                                                        'onchange'=>'if($(this).val() == 4){
                                                            $("#SsProgramas_apoyo_economico").val("NINGUNO").prop( "disabled", true ); 
                                                        }else{
                                                            $("#SsProgramas_apoyo_economico").val("").prop( "disabled", false ); 
                                                        }')
                                                        ); ?>
                        <?php echo $form->error($modelSSProgramas,'id_tipo_apoyo_economico'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'apoyo_economico'); ?>
                        <?php echo $form->textField($modelSSProgramas,'apoyo_economico',array('size'=>60,'maxlength'=>100, 'class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSProgramas,'apoyo_economico'); ?>
                    </div>                    

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_recibe_capacitacion'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                    'id_recibe_capacitacion',
                                                    array('1'=>'SI', '2'=>'NO'),
                                                    array('prompt'=>'--Recibe Capacitación--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
                                                    ); ?>
                        <?php echo $form->error($modelSSProgramas,'id_recibe_capacitacion'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSProgramas,'id_clasificacion_area_servicio_social'); ?>
                        <?php echo $form->dropDownList($modelSSProgramas,
                                                    'id_clasificacion_area_servicio_social',
                                                    $lista_clasificacion_areas_serv_social,
                                                    array('prompt'=>'--Elige Clasificación Área--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')); ?>
                        <?php echo $form->error($modelSSProgramas,'id_clasificacion_area_servicio_social'); ?>
                    </div>

                    <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Elije los Días Hábiles del Programa
                </h6>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this HorarioDiasHabilesProgramasController */
                /* @var $model HorarioDiasHabilesProgramas */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'horario-dias-habiles-programas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

                    <?php echo $form->errorSummary($modelSSHorarioDiasHabilesProgramas); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
                        <?php echo $form->dropDownList($modelSSHorarioDiasHabilesProgramas,
                                                    'id_dia_semana',
                                                    $lista_dias,
                                                    array('prompt'=>'--Dia de la semana--','class'=>'form-control', 'required'=>'required'
                                                    )); ?>
                        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
                    </div>
                    
                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
                        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
                        <?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
                                'model'=>$modelSSHorarioDiasHabilesProgramas,
                                'attribute'=>'hora_inicio',
                                'language' => 'es',
                                'options' => array(
                                        'showOn'=>'focus',
                                        'timeOnly'=>true,
                                        'showHour'=>true,
                                        'showMinute'=>true,
                                        //'showSecond'=>true,
                                        'timeFormat'=>'hh:mm:ss',
                                ),
                                'htmlOptions' => array(
                                        'style'=>'width:150px;', // styles to be applied
                                        'maxlength' => '10',    // textField maxlength
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'readOnly' => true
                                ),
                        ));?>
                        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
                        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
                        <?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
                                'model'=>$modelSSHorarioDiasHabilesProgramas,
                                'attribute'=>'hora_fin',
                                'language' => 'es',
                                'options' => array(
                                        'showOn'=>'focus',
                                        'timeOnly'=>true,
                                        'showHour'=>true,
                                        'showMinute'=>true,
                                        //'showSecond'=>true,
                                        'timeFormat'=>'hh:mm:ss',
                                ),
                                'htmlOptions' => array(
                                        'style'=>'width:150px;', // styles to be applied
                                        'maxlength' => '10',    // textField maxlength
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'readOnly' => true
                                ),
                        ));?>
                        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
                    </div>

                    <div class="form-group">
                        <?php //echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'horas_totales'); ?>
                        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'horas_totales',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
                        <?php //echo $form->error($modelSSHorarioDiasHabilesProgramas,'horas_totales'); ?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelSSHorarioDiasHabilesProgramas->isNewRecord ? 'Guardar Horario' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                        <?php echo ($hayRegHorario == true) ? CHtml::link('Terminar Registro', array('listaProgramasAdminServicioSocial'), array('class'=>'btn btn-danger')) : ''; ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Días Hábiles del Programa
		</span>
	</h2>
</div>


<!--SE MUESTRAN LOS DIAS HABILES QUE SE VAN ELIGIENDO PARA EL PROGRAMA-->
<br><br><br>
<div class="row">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'contador-horario-dias-habiles-programas-grid',
        'dataProvider'=>$modelSSHorarioDiasHabilesProgramas->searchEditarHorarioPrograma($modelSSProgramas->id_programa),
        'filter'=>$modelSSHorarioDiasHabilesProgramas,
        'columns'=>array(
            array(
                'class'=>'CButtonColumn',
                'template'=>'{horaProgramaServicioSocial}',
                'header'=>'Horario',
                'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
                'buttons'=>array
                (
                    'horaProgramaServicioSocial' => array
                    (
                        'label'=>'Hora y dia del Programa',
                        'url'=>'#',
                        'imageUrl'=>'images/servicio_social/horario_32.png'
                    ),
                ),
            ),
            //'id_horario',
            /*array(
                'name' => 'id_programa_servicio_social',
                'filter' => false,
                'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
            ),*/
            array(
                'name' => 'idDiaSemana.dia_semana',
                'filter' => false,
                'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
            ),
            array(
                'name' => 'hora_inicio',
                'filter' => false,
                'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
            ),
            array(
                'name' => 'hora_fin',
                'filter' => false,
                'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
            ),
            //Falta calcular las horas dependiendo de los campos hora_inicio y hora_fin
            array(
                'name' => 'horas_totales',
                'filter' => false,
                'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
            )
        ),
    )); ?>
</div>

<br><br>
<div align="center">
    <?php //echo CHtml::link('Terminado', array('/'), array('class'=>'btn btn-success')); ?>
</div>
<!--SE MUESTRAN LOS DIAS HABILES QUE SE VAN ELIGIENDO PARA EL PROGRAMA-->