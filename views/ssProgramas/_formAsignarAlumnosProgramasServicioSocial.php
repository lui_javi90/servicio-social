<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'asignar-programas-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($modelSSProgramas); ?>

	<br>
	<?php if($bandera === false){ ?>
		<div class="form-group">
			<p><?php echo "<b>Numero de Lugares a Agregar: </b>"; ?><span class="required">*</span></p>
			<?php echo $form->textField($modelSSProgramas,'numero_estudiantes_solicitados',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
			<?php echo $form->error($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
		</div>
	<?php }else{?>
		<div class="form-group">
			<?php echo "<b>Numero de Lugares a Agregar:</b>"; ?>
			<?php echo $form->textField($modelSSProgramas,'numero_estudiantes_solicitados',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'readOnly'=>true)); ?>
			<?php echo $form->error($modelSSProgramas,'numero_estudiantes_solicitados'); ?>
		</div>
	<?php } ?>

	<?php if($bandera === false){ ?>
	<div class="form-group">
        <p><b><?php echo "Horas a Liberar: "; ?><span class="required">*</span></b></p>
        <?php echo $form->textField($modelSSProgramas,'horas_totales',array('size'=>3,'maxlength'=>3, 'class'=>'form-control')); ?>
        <?php echo $form->error($modelSSProgramas,'horas_totales'); ?>
    </div>
	<?php }else{ ?>
	<div class="form-group">
		<p><b><?php echo "Horas a Liberar: "; ?><span class="required">*</span></b></p>
        <?php echo $form->textField($modelSSProgramas,'horas_totales',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'readOnly'=>true)); ?>
        <?php echo $form->error($modelSSProgramas,'horas_totales'); ?>
    </div>
	<?php } ?>

	<br>
	<div class="form-group">
		<?php if($bandera === false){ ?>
			<?php echo CHtml::submitButton($modelSSProgramas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php } ?>
		<?php echo Chtml::link('Cancelar', array('listaProgramasAsignacionAlumnos'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
