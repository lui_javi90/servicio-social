<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */
if($modelSSProgramas->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Programas Servicio Social'=>array('ssProgramas/listaProgramasSupervisorServicioSocial'),
		'Nuevo Programa',
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Programas Servicio Social'=>array('ssProgramas/listaProgramasSupervisorServicioSocial'),
		'Editar Programa',
	);
}
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<?php echo ($modelSSProgramas->isNewRecord) ? "Nuevo Programa" : "Editar Programa"; ?>
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-warning">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Las HORAS A LIBERAR y el NUMERO DE ALUMNOS ASIGNADOS AL PROGRAMA son establecidos por el Jefe de la Oficina de Servicio Social. Si necesita un numero
		determinado de alumnos deberá tratarlo con el Jefe de la Oficina de Servicio Social.
    </strong></p>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<?php echo ($modelSSProgramas->isNewRecord) ? "<b>Nuevo Programa</b>" : "<b>Editar Programa</b>"; ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formSupervisorProgramaServicioSocial', array(
										   'modelSSProgramas'=>$modelSSProgramas,
										   'info_empleado' => $info_empleado,
										   'departamento' => $departamento,
										   'cargo' => $cargo,
										   'lista_departamentos' => $lista_departamentos,
										   'lista_periodos_programas' => $lista_periodos_programas,
										   'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
										   'id_tipo_supervisor' => $id_tipo_supervisor,
										   'id_empresa' => $id_empresa,
										   'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
										   'lista_tipo_apoyo' => $lista_tipo_apoyo,
										   'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
										   'fecha_actualizacion' => $fecha_actualizacion,
										   'lista_status' => $lista_status,
										)); ?>
			</div>
		</div>
	</div>
</div>

<br><br><br><br><br>
<br><br><br><br><br>
