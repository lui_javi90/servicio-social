<style>
.boton {
    display:none;
}
</style>
<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas Servicio Social' => array('ssProgramas/listaProgramasAdminServicioSocial'),
    'Detalle del Programa'
);

?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información del Programa
		</span>
	</h2>
</div>

<br>
<div class="row"><!--Row Principal-->
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Detalle del Programa</h6>
			</div>
			<div class="panel-body">

				<div class="col-lg-3" align="center">
					<?php
						echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/programa_default.png"/>';
					?>
				</div>
				<div class="col-lg-9">
					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('nombre_programa')); ?>:</b>&nbsp;
			    	 <?php echo CHtml::encode($modelSSProgramas->nombre_programa); ?></p>

                    <p><b>RFC:</b>&nbsp;
			    	 <?php echo $rfcSupervisor; ?></p>

					<p><b><?php echo "Nombre del Supervisor: "; ?></b>&nbsp;
			    	 <?php echo $nombre_supervisor; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_periodo_programa')); ?>:</b>&nbsp;
			    	 <?php echo $periodo_programa; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>&nbsp;
				     <?php echo '<span style="font-size:18px" class="label label-success">'.CHtml::encode($modelSSProgramas->horas_totales).' horas</span>'; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('fecha_inicio_programa')); ?>:</b>&nbsp;
			    	 <?php echo $fec_inicio; ?></p>

			        <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('fecha_fin_programa')); ?>:</b>&nbsp;
			    	 <?php echo $fec_fin; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>&nbsp;
			    	 <?php echo ($modelSSProgramas->id_tipo_programa == 1) ? '<span style="font-size:16px" class="label label-info">'.'INTERNO'.'</span>' : '<span style="font-size:16px" class="label label-info">'.'EXTERNO'.'</span>'; ?></p>

                    <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('numero_estudiantes_solicitados')); ?>:</b>&nbsp;
                    <?php  echo ($modelSSProgramas->numero_estudiantes_solicitados > 0) ? '<span style="font-size:16px" class="label label-success">'.$modelSSProgramas->numero_estudiantes_solicitados.'</span>' : '<span style="font-size:16px" class="label label-danger">'.$modelSSProgramas->numero_estudiantes_solicitados.'</span>'; ?></p>

                    <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('lugares_disponibles')); ?>:</b>&nbsp;
                    <?php echo ($modelSSProgramas->lugares_disponibles > 0) ? '<span style="font-size:16px" class="label label-success">'.$modelSSProgramas->lugares_disponibles.'</span>' : '<span style="font-size:16px" class="label label-danger">'.$modelSSProgramas->lugares_disponibles.'</span>'; ?></p>

				</div>
			</div>
		</div>
	</div>
</div><!--Row Principal-->

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información Gral. del Programa
		</span>
	</h2>
</div>

<br><br>
<?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$modelSSProgramas,
        'attributes'=>array(
            /*'id_programa_servicio_social',
            'nombre_programa',*/
            array(
                'header'=>'Empresa',
                'name' => 'idUnidadReceptora.nombre_unidad_receptora'
            ),
            'lugar_realizacion_programa',
            /*array(
                'name'=>'Departamento',
                'value' => function($data)
                {
                    $id = $data->rfcSupervisor0->cveDepartamento;
                    $query = "select \"dscDepartamento\" from \"H_departamentos\" where \"cveDepartamento\" = '$id' ";
                    $nombre = Yii::app()->db->createCommand($query)->queryAll();

                    return $nombre[0]['dscDepartamento'];
                },
            ),*/
            'horas_totales',
            //'idPeriodoPrograma.periodo_programa',//id_periodo_programa
            /*array(
                'name' => 'rfcSupervisor',
                'value' => function($data)
                {
                    $rfc_sup = $data->rfcSupervisor;
                    $query =
                    "
                    SELECT (nombre_supervisor||' '|| apell_paterno || ' ' || apell_materno) as name
                    from ss_supervisores_programas
                    where \"rfcSupervisor\" = '$rfc_sup'
                    ";
                    $name = Yii::app()->db->createCommand($query)->queryAll();

                    return (count($name) > 0) ? $name[0]['name'] : "NO ESPECIFICADO";
                }
            ),*/
            'numero_estudiantes_solicitados',
            'descripcion_objetivo_programa',
            'idTipoServicioSocial.tipo_servicio_social',//id_tipo_servicio_social
            'impacto_social_esperado',
            'beneficiarios_programa',
            'actividades_especificas_realizar',
            'mecanismos_supervision',
            'perfil_estudiante_requerido',
            'idApoyoEconomicoPrestador.descripcion_apoyo_economico',//id_apoyo_economico_prestador_servicio_social
            'idTipoApoyoEconomico.tipo_apoyo_economico',//id_tipo_apoyo_economico
            'apoyo_economico',
            /*array(
                'name' => 'fecha_registro_programa',
                'oneRow' => true,
                'value' => function($data)
                {
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
								    $fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateProgramasReg($data->id_programa);
								    return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
                },
            ),
            array(
                'name' => 'fecha_modificacion_programa',
                'oneRow' => true,
                'value' => function($data)
                {
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
								    $fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateProgramasMod($data->id_programa);
								    return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
                },
            ),*/
            array(
                'name' => 'id_recibe_capacitacion',
                'oneRow'=>true,
                'value' => function($data)
                {
                    return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO";
                },
            ),
            'idClasificacionAreaServicioSocial.clasificacion_area_servicio_social',//id_clasificacion_area_servicio_social
            'lugares_disponibles',
            array(
                'name' => 'id_tipo_programa',
                'oneRow'=>true,
                'value' => function($data)
                {
                    return ($data->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
                },
            ),
            'idStatusPrograma.descripcion_status',
            ),
)); ?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>

<br><br>
<?php
for($i=0; $i < count($modelSSHorarioDiasHabilesProgramas); $i++){
$this->widget('serviciosocial.components.DetailView4Col', array(
    'data'=>$modelSSHorarioDiasHabilesProgramas[$i],
    'attributes'=>array(
        //'id_horario',
        //'id_programa_servicio_social', id_dia_semana
        array(
            'name' => 'idDiaSemana.dia_semana',
            'oneRow'=>true,
        ),
        'hora_inicio',
        'hora_fin',
        //'horas_totales',
    ),
));
echo "<br><br>";
}
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Alumnos Registrados en el Programa
		</span>
	</h2>
</div>


<br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-servicio-social-grid',
    'dataProvider'=>$SSServicioSocial->searchListaAlumnossEnPrograma($criteria2), //Filtrar los alumnos inscritos en un determinado Programa
    //'filter'=>$modelSSServicioSocial,
    'columns'=>array(
        //'id_servicio_social',
        array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return (empty($data->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->no_ctrl, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			    //'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'no_ctrl',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:16px" class="label label-success">'.$data->no_ctrl.'</span>';
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Nombre del Alumno',//
            'value' => function($data)
            {
                $query =
                " SELECT \"nmbSoloAlumno\" || ' ' || \"apellPaternoAlu\" || ' ' || \"apellMaternoAlu\" as nombre from public.\"E_datosAlumno\" WHERE \"nctrAlumno\"='$data->no_ctrl' ";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return (count($result) > 0) ? $result[0]['nombre'] : "--";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'330px', 'class'=>'text-center')
        ),
        //'id_estado_servicio_social',
        array(
            'header' => 'Especialidad',
            'filter' => false,
            'value' => function($data)
            {
                $query = "select * from public.\"E_datosAlumno\" eda 
                        join public.\"E_especialidad\" esp
                        on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
                        where eda.\"nctrAlumno\" = '$data->no_ctrl' ";

                $carrera = Yii::app()->db->createCommand($query)->queryAll();

                return (!empty($carrera)) ? $carrera[0]['dscEspecialidad'] : '--';
            },
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idEstadoServicioSocial.estado',
            'filter' => false,
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        /*'id_programa',
        array(
            'name' => 'idPrograma.nombre_programa', 
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),*/
        //'fecha_registro',
        array(
            'name' => 'fecha_registro',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';
				$fecha_actualizacion = InfoServicioSocial::getFechaRegistroServicioSocial($data->id_servicio_social);
				return ($fecha_actualizacion[0]['fecha_act'] != 'ND') ? $fecha_actualizacion[0]['fecha_act']. " a las ".$fecha_actualizacion[0]['hora'] : "--";
            },
            'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
        ),
        /*'fecha_modificacion',
        'calificacion_servicio_social',
        'rfcDirector',
        'rfcSupervisor',
        'cargo_supervisor',
        'empresaSupervisorJefe',
        'nombre_jefe_depto',
        'departamentoSupervisorJefe',
        'rfcJefeOficServicioSocial',
        'rfcJefeDeptoVinculacion',
        'nombre_supervisor',
        array(
            'class'=>'CButtonColumn',
        ),*/
    ),
)); ?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Registrar Alumno en el Programa Actual
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Agregar Alumno al Programa
                </h3>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this EDatosAlumnoController */
                /* @var $model EDatosAlumno */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'edatos-alumno-addlugar-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                        //'htmlOptions' => array('autocomplete'=>off),
                    )); ?>

                    <p class="note">Campos con <span class="required">*</span> son requeridos.</p>

                    <?php echo $form->errorSummary($modelEDatosAlumno); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelEDatosAlumno,'nctrAlumno'); ?>
                        <?php echo $form->textField($modelEDatosAlumno,
                                                    'nctrAlumno',
                                                    array(
                                                        'class'=>'form-control', 'size'=>9,'maxlength'=>8, 'required'=>'required',
                                                        'ajax'=>array(
                                                        'type'=>'POST',
                                                        'dataType'=>'json',
                                                        'data' => array(
            
                                                            'nctrAlumno'=>'js:$(\'#EDatosAlumno_nctrAlumno\').val()',
                                                        ),
                                                        'url'=>CController::createUrl('SsProgramas/validarAddAlumnoPrograma'),
                                                        'success'=>'function(data)
                                                        {
                                                            $("#EDatosAlumno_insAlumno").html(data.inscrito);

                                                            if(data.no_c == "*")
                                                            {
                                                                $(".div_foto_alumno").hide();
                                                                $(".div_nombre").hide();
                                                                $(".div_carrera").hide();
                                                                $(".div_inscrito").hide();
                                                                $(".div_status").hide();
                                                                $(".div_semestre").hide();
                                                                $(".div_creditos_acum").hide();
                                                                $(".boton").hide();
                                                            }else
                                                                if(data.no_c == "+")
                                                            {
                                                                $(".div_foto_alumno").hide();
                                                                $(".div_nombre").hide();
                                                                $(".div_carrera").hide();
                                                                $(".div_inscrito").hide();
                                                                $(".div_status").hide();
                                                                $(".div_semestre").hide();
                                                                $(".div_creditos_acum").hide();
                                                                $(".boton").hide();

                                                            }else{

                                                                $(".div_foto_alumno").show();
                                                                $(".div_nombre").show()
                                                                $(".div_carrera").show();
                                                                $(".div_inscrito").show();
                                                                $(".div_status").show();
                                                                $(".div_semestre").show();
                                                                $(".div_creditos_acum").show();
                                                                $(".boton").show();
                                                                $(".div_foto_alumno").html(\'<img class="img-circle" align="center" heigth="150" width="200" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=\'+ data.no_c +\' " />\');
                                                                $(".div_nombre").html(\'<b><label align="left">Nombre Completo:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.nombre_completo +\'" readonly />\');
                                                                $(".div_carrera").html(\'<b align="left"><label>Carrera:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.carrera +\'" readonly />\');
                                                                $(".div_inscrito").html(\'<b><label align="left">Esta Inscrito:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.inscrito +\'" readonly />\');
                                                                $(".div_status").html(\'<b><label align="left">Estatus del Alumno:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.status +\'" readonly />\');
                                                                $(".div_semestre").html(\'<b><label align="left">Semestre:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.semestre +\'" readonly />\');
                                                                $(".div_vis_cred").show();
                                                                if(data.val_creditos == true){
                                                                $(".div_vis_cred").html(\'<img align="center" heigth="15" width="20" src="images/servicio_social/aceptar_32.png" />\');
                                                                }else{
                                                                    $(".div_vis_cred").html(\'<img align="center" heigth="15" width="20" src="images/servicio_social/rechazar_32.png" />\');
                                                                }
                                                                    $(".div_creditos_acum").html(\'<b><label align="left">Creditos Acumulados:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.creditos_acum +\'" readonly />\');
                                                            }
                                                        }',
                                                        )) 
                                                    ); ?>
                        <?php echo $form->error($modelEDatosAlumno,'nctrAlumno'); ?>
                    </div>

                     <!--Foto-->
                    <hr>
                        <h4 align="center">Información del Alumno</h4>
                    <hr>

                    <div class="div_msg_error col-md-12 center" align="center">
                        <br>
                    </div>

                    <br>
                        <div class="div_foto_alumno col-md-12 center" align="center">
                        <br>
                    </div>

                     <!--Nombre Completo-->
                    <div class="div_nombre col-md-12 center" align="center">
                    </div>

                    <br><br>

                     <!--Carrera-->
                     <div class="div_carrera col-md-12" align="center">
                    </div>

                    <br><br>

                     <!--Inscrito-->
                    <div class="div_inscrito col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Estatus-->
                    <div class="div_status col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Semestre-->
                    <div class="div_semestre col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Creditos-->
                    <div align="center" class="div_vis_cred col-md-12">
                    </div>
                    <div class="div_creditos_acum col-md-12" align="center">
                    </div>

                    <br><br>
                    <div align="center" class="form-group">
                        <?php echo CHtml::submitButton('Inscribir Alumno al Programa', array('class'=>'boton btn btn-success')); ?>
                        <?php //echo CHtml::link('Cancelar', array('/'), array('class'=>'btn btn-danger')); ?>
                    </div>

                    <?php $this->endWidget(); ?>
                </div><!-- form -->

            </div>
        </div>
    </div>
</div>


<br><br><br>
<div align="center">
    <?php echo CHtml::link('Volver a Lista de Programas', array('listaProgramasAdminServicioSocial'), array('class'=>'btn btn-default')); ?>
</div>
<br><br>
