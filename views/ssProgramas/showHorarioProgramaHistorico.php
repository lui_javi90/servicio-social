<br>
<div align="center">
    <h4 class=""><?php echo $modelSSProgramas->nombre_programa; ?></h4>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-horario-dias-habiles-programas-grid',
    'dataProvider'=>$model->searchXPrograma($modelSSProgramas->id_programa),
    //'filter'=>$model,
    'columns'=>array(
        //'id_horario',
        //'id_programa',
        //'id_dia_semana',
        array(
            'name' => 'idDiaSemana.dia_semana',
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'name' => 'hora_inicio',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'hora_fin',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'horas_totales',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
    ),
)); ?>

<br>