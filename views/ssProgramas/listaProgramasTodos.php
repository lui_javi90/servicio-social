<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Cambio de Supervisor del Programa'
);

//Validar la elimimacion del programa
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#ss-programas-todos-finalizar-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#ss-programas-todos-finalizar-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//Validar la elimimacion del programa

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Lista de Programas Activos
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-programas-todos-finalizar-grid',
    'dataProvider'=>$modelSSProgramas->searchListaProgramasTodos(), //listaProgramasTodos -> menos Finalziados y Cancelados
    'filter'=>$modelSSProgramas,
    'columns'=>array(
        //'id_programa',
        array(
			'header' => 'Supervisor',
            'type'=>'raw',
			'value' => function($data)
			{
				$rfc;
				$foto;
				switch($data->id_tipo_programa)
				{
					case 1:
						$query = "select rpi.\"rfcEmpleado\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
                                rpi.superv_principal_int = true
                            ";
						
						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
						$query2 = "select foto from public.\"H_credencialEmpleado\" where \"rfcEmpleado\" = '$rfc' ";
						$res = Yii::app()->db->createCommand($query2)->queryAll();

						//Guardamos la foto en caso de que hayan subido una
						$foto = $res[0]['foto'];

						return ($foto != NULL) ? CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;")) : CHtml::image("images/servicio_social/masculino2.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
					break;
					case 2:
						$query = "select rpe.\"rfcSupervisor\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
                                rpe.superv_principal_ext = true
                            ";
						
						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
						$query2 = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfc' ";
						$res = Yii::app()->db->createCommand($query2)->queryAll();

						//Guardamos la foto en caso de que hayan subido una
						$foto = $res[0]['foto_supervisor'];

						//Si hay foto guardada en la BD
						if($foto != NULL)
						{
							if(trim($foto) != 'masculino.png' or trim($foto) != 'femenino.png')
								return CHtml::image("images/servicio_social/supervisores/".$rfc."/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							else
								return CHtml::image("images/servicio_social/supervisores/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

						}else
						{
		
							if($foto[0]['id_sexo'] == 1)
								return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							elseif($foto[0]['id_sexo'] == 2)
								return CHtml::image("images/servicio_social/supervisores/femenino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							else
								return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
						}
						
					break;
				}
				
			},//Fin Funcion value
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'RFC',
            'name' => 'rfc', 
            'value' => function($data)
            {
                switch($data->id_tipo_programa)
				{
					case 1:
						$query = "select hemp.\"rfcEmpleado\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								join public.\"H_empleados\" hemp
								on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' and 
                                rpi.superv_principal_int = true
                            ";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						return $rfc;
					break;
					case 2:
						$query = "select sp.\"rfcSupervisor\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								join pe_planeacion.ss_supervisores_programas sp
								on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' and 
                                rpe.superv_principal_ext = true
                            ";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						return $rfc;
					break;
					default:
                     return "--";
                }
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'name' => 'nombre_programa',
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        /*array(
            'name' => 'idUnidadReceptora.nombre_unidad_receptora',
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),*/
        array(
            'name' => 'horas_totales',
            'filter' => false,
            'htmlOptions' => array('width'=>'60px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPeriodoPrograma.periodo_programa',
            'filter' => CHtml::activeDropDownList($modelSSProgramas,
                                                    'id_periodo_programa',
                                                    $lista_periodos,
                                                    array('prompt'=>'-- Filtrar por periodo --')
            ),
            'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Tipo de Programa',
            'value' => function($data)
            {
                return ($data->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => CHtml::activeDropDownList($modelSSProgramas,
                                                    'id_tipo_programa',
                                                    array('1'=>'INTERNO', '2'=>'EXTERNO'),
                                                    array('prompt'=>'-- Filtrar por tipo --')
            ),
            'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_inicio_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'85px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_fin_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'85px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idStatusPrograma.descripcion_status',
            'filter' => false,
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'No. Alumnos <br>Programa',
            'filter' => false,
            'value' => function($data)
            {
                $id = $data->id_programa;

                $qry = "select count(ss.id_servicio_social) as total_alumnos
                        from pe_planeacion.ss_programas p
                        join pe_planeacion.ss_servicio_social ss
                        on ss.id_programa = p.id_programa
                        where p.id_programa = '$id' AND
                        (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6)
                    ";
                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                return $rs[0]['total_alumnos'];
            },
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        /*
        'descripcion_objetivo_programa',
        'id_tipo_servicio_social',
        'impacto_social_esperado',
        'beneficiarios_programa',
        'actividades_especificas_realizar',
        'mecanismos_supervision',
        'perfil_estudiante_requerido',
        'id_apoyo_economico_prestador',
        'id_tipo_apoyo_economico',
        'apoyo_economico',
        'fecha_registro_programa',
        'fecha_modificacion_programa',
        'id_recibe_capacitacion',
        'id_clasificacion_area_servicio_social',
        'lugares_disponibles',
        'id_status_programa',
        'fecha_inicio_programa',
        'fecha_fin_programa',
        'id_registro_fechas_ssocial',
        'lugares_ocupados',
        */
        array(
			'class'=>'CButtonColumn',//
			'template'=>'{cancelarPrograma}',
			'header'=>'Cancelar Programa',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'cancelarPrograma' => array
				(
					'label'=>'Eliminar Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/cancelProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
                    'options' => array(
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Cancelar del Programa?', // custom attribute to hold confirmation message
                    ),
					'click'   => $approveJs, // JS string which processes AJAX request
				),
			),
		),
		array(
			'class'=>'CButtonColumn',//
			'template'=>'{finalizarPrograma}',
			'header'=>'Finalizar Programa',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'finalizarPrograma' => array
				(
					'label'=>'Finalizar Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/finalizarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/aprobado_32.png',
                    'options' => array(
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Finalización del Programa?', // custom attribute to hold confirmation message
                    ),
					'click'   => $approveJs, // JS string which processes AJAX request
				),
			),
		)
    ),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>