<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
    'Servicio Social' => '?r=serviciosocial',
    'Histórico Programas de Servicio Social' => array('ssProgramas/listaHistoricoProgramasSupervisor'),
    'Detalle Histórico del Programa'
    

); ?>


<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Detalle Histórico del Programa
		</span>
	</h2>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <b>Información</b>
            </h4>
        </div>
        <div class="panel-body">

            <p><b><?php echo "Supervisor Principal"; ?>:</b>
            <?php echo $supervisor_principal; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('nombre_programa')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->nombre_programa); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_unidad_receptora')); ?>:</b>
            <?php echo $empresa; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_periodo_programa')); ?>:</b>
            <?php echo $periodo_programa; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('horas_totales')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->horas_totales); ?></p>

            <p><b><?php echo "No. de Estudiantes Solicitados"; ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->numero_estudiantes_solicitados); ?></p>

        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <b>Detalle</b>
                </h4>
            </div>
            <div class="panel-body">

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('descripcion_objetivo_programa')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->descripcion_objetivo_programa); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_tipo_servicio_social')); ?>:</b>
            <?php echo $tipo_servicio_social; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('impacto_social_esperado')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->impacto_social_esperado); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('beneficiarios_programa')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->beneficiarios_programa); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('actividades_especificas_realizar')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->actividades_especificas_realizar); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('mecanismos_supervision')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->mecanismos_supervision); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('perfil_estudiante_requerido')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->perfil_estudiante_requerido); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_apoyo_economico_prestador')); ?>:</b>
            <?php echo $prestador_apoyo; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_tipo_apoyo_economico')); ?>:</b>
            <?php echo $tipo_apoyo; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('apoyo_economico')); ?>:</b>
            <?php echo CHtml::encode($modelSsProgramas->apoyo_economico); ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('fecha_registro_programa')); ?>:</b>
            <?php echo $fec_registro_programa[0]['fecha_act']. ' a las '.$fec_registro_programa[0]['hora']; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_recibe_capacitacion')); ?>:</b>
            <?php echo ($modelSsProgramas->id_recibe_capacitacion == 1) ? "SI" : "NO"; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_clasificacion_area_servicio_social')); ?>:</b>
            <?php echo $clasificacion_area; ?></p>

            <p><b><?php echo CHtml::encode($modelSsProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>
            <?php echo ($modelSsProgramas->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO"; ?></p>
                
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <b>Fechas de Inicio:</b>
                </h4>
            </div>
            <div align="center" class="panel-body">
                <?php

                    echo $fec_inicio;

                ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <b>Fechas Fin:</b>
                </h4>
            </div>
            <div align="center" class="panel-body">
                <?php 
                   
                    echo $fec_fin;

                ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <b>Estatus del Programa:</b>
                </h4>
            </div>
            <div align="center" class="panel-body">
                <?php echo $estatus_programa; ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <b>Periodo:</b>
                </h4>
            </div>
            <div align="center" class="panel-body">
                <?php echo ($periodo == 1) ? "ENERO-JUNIO" : "AGOSTO-DICIEMBRE"; ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <b>Año:</b>
                </h4>
            </div>
            <div align="center" class="panel-body">
                <?php echo $anio; ?>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Alumnos se Inscribieron en el Programa
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-servicio-social-grid',
    'dataProvider'=>$modelSsServicioSocial->searchXProgramaHistoricoServicioSocial($modelSsProgramas->id_programa), //Filtrar por id de programa en que llevo 
    'filter'=>$modelSsServicioSocial,
    'columns'=>array(
        //'id_servicio_social',
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return (empty($data->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->no_ctrl, '', array('class'=>'img-circle','style' =>"width:80px;height:80px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        //'id_estado_servicio_social',
        //'id_programa',
        array(
            'name' => 'idPrograma.nombre_programa',
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estado Servicio <br>Social',
            //'name' => 'idEstadoServicioSocial.estado',
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->id_estado_servicio_social == 6) ? '<span style="font-size:14px" class="label label-success">FINALIZADO</span>' : '<span class="label label-danger">CANCELADO</span>';
            },
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Fecha Registro',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoServicioSocial.php';

                $fec_reg = InfoServicioSocial::getFechaRegistroServicioSocial($data->id_servicio_social);

                return $fec_reg[0]['fecha_act'].' a las '.$fec_reg[0]['hora'];
            },
            'htmlOptions' => array('width'=>'180px', 'class'=>'text-center')
        ),
        //'fecha_modificacion',
        /*array(
			'class'=>'CButtonColumn',//
			'template'=>'{detAlumnoServSocialHistorico}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detAlumnoServSocialHistorico' => array
				(
					'label'=>'Eliminar Clasificación Área',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleAlumServSocialHistoricoSPrograma", array("id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		)*/
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>
