<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Cambio de Supervisor del Programa'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Lista de Programas Activos
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-programas-grid',
    'dataProvider'=>$modelSSProgramas->search(),
    'filter'=>$modelSSProgramas,
    'columns'=>array(
        //'id_programa',
        array(
            'name' => 'nombre_programa',
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idUnidadReceptora.nombre_unidad_receptora',
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'horas_totales',
            'filter' => false,
            'htmlOptions' => array('width'=>'60px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPeriodoPrograma.periodo_programa',
            'filter' => CHtml::activeDropDownList($modelSSProgramas,
                                                    'id_periodo_programa',
                                                    $lista_periodos,
                                                    array('prompt'=>'-- Filtrar por periodo --')
            ),
            'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Tipo de Programa',
            'value' => function($data)
            {
                return ($data->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
            },
            'filter' => CHtml::activeDropDownList($modelSSProgramas,
                                                    'id_tipo_programa',
                                                    array('1'=>'INTERNO', '2'=>'EXTERNO'),
                                                    array('prompt'=>'-- Filtrar por tipo --')
            ),
            'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_inicio_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'85px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_fin_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'85px', 'class'=>'text-center')
        ),
        /*
        'descripcion_objetivo_programa',
        'id_tipo_servicio_social',
        'impacto_social_esperado',
        'beneficiarios_programa',
        'actividades_especificas_realizar',
        'mecanismos_supervision',
        'perfil_estudiante_requerido',
        'id_apoyo_economico_prestador',
        'id_tipo_apoyo_economico',
        'apoyo_economico',
        'fecha_registro_programa',
        'fecha_modificacion_programa',
        'id_recibe_capacitacion',
        'id_clasificacion_area_servicio_social',
        'lugares_disponibles',
        'id_status_programa',
        'fecha_inicio_programa',
        'fecha_fin_programa',
        'id_registro_fechas_ssocial',
        'lugares_ocupados',
        */
        array(
			'class'=>'CButtonColumn',//
			'template'=>'{cambiarSupervisorprograma}',
			'header'=>'Cambiar Supervisor',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'cambiarSupervisorprograma' => array
				(
					'label'=>'Eliminar Clasificación Área',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/cambiarDeSupervisorDelPrograma", array("id_programa"=>$data->id_programa, "id_unidad_receptora"=>$data->id_unidad_receptora))',
					'imageUrl'=>'images/editar_persona_32.png',
				),
			),
		)
    ),
)); ?>