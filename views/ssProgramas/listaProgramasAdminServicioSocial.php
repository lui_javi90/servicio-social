<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Programas Servicio Social',
);

//Validar la elimimacion del programa
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#admin-programas-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#admin-programas-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//Validar la elimimacion del programa

/* FINALIZA EL PROGRAMA */
$JsFinalizaPrograma = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#admin-programas-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#admin-programas-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}'
/* FINALIZA EL PROGRAMA */

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Programas Servicio Social
		</span>
	</h2>
</div>

<br>
<!--Si no hay registro actual de Servicio Social no se activa-->
<?php //if($fec_inicio_servicio_social_actual == true){ ?>
<div style="float: right;">
	<?php echo CHtml::link('Nuevo Registro Programa', array('nuevoProgramaServicioSocialDepto', 'id_registro_fechas_ssocial'=>$id_registro_fechas_ssocial), array('class'=>'btn btn-success')); ?>
	<?php
		echo "<br><br>";
		echo CHtml::link('Supervisores Faltantes de Evaluar Reportes', array('ListaExcelSupervisoresFaltanEvaluar'), array('class'=>'btn btn-success'));
		//echo "<br><br>";
	?>
</div>
<?php //} ?>

<br><br><br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Verifica la fecha de CADUCIDAD de cada Programa, una vez que el Estatus del Programa cambie a Caducado será momento de FINALIZAR el programa.
    </strong></p>
</div>

<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Una vez llegada la Fecha de Inicio de Servicio Social NO se podran agregar mas Programas.
    </strong></p>
</div>

<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Cuando se CANCELA un Programa no se podrá revertir la acción.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admin-programas-servicio-social-grid',
	'dataProvider'=>$modelSSProgramas->searchProgramasActivos($nombreSupervisor, $apePaterno, $apeMaterno), //Si se manda nombre para filtrar
	'filter'=>$modelSSProgramas,
	'columns'=>array(
		//'id_programa',
		array(
			'header' => 'Supervisor',
            'type'=>'raw',
			'value' => function($data)
			{
				$rfc;
				$foto;
				switch($data->id_tipo_programa)
				{
					case 1:
						$query = "select rpi.\"rfcEmpleado\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpi.superv_principal_int = true
							";
						
						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
						$query2 = "select foto from public.\"H_credencialEmpleado\" where \"rfcEmpleado\" = '$rfc' ";
						$res = Yii::app()->db->createCommand($query2)->queryAll();

						//Guardamos la foto en caso de que hayan subido una
						$foto = $res[0]['foto'];

						return ($foto != NULL) ? CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;")) : CHtml::image("images/servicio_social/masculino2.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
					break;
					case 2:
						$query = "select rpe.\"rfcSupervisor\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpe.superv_principal_ext = true 
							";
						
						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
						$query2 = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfc' ";
						$res = Yii::app()->db->createCommand($query2)->queryAll();

						//Guardamos la foto en caso de que hayan subido una
						$foto = $res[0]['foto_supervisor'];

						//Si hay foto guardada en la BD
						if($foto != NULL)
						{
							if(trim($foto) != 'masculino.png' or trim($foto) != 'femenino.png')
								return CHtml::image("images/servicio_social/supervisores/".$rfc."/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							else
								return CHtml::image("images/servicio_social/supervisores/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

						}else
						{
		
							if($foto[0]['id_sexo'] == 1)
								return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							elseif($foto[0]['id_sexo'] == 2)
								return CHtml::image("images/servicio_social/supervisores/femenino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							else
								return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
						}
						
					break;
				}
				
			},//Fin Funcion value
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
		array(
			'name'=>'nombre_programa',
			'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Tipo',
			'name'=>'id_tipo_programa',
			'filter' => CHtml::activeDropDownList($modelSSProgramas,
												'id_tipo_programa',
												array('1'=>'INTERNO', '2'=>'EXTERNO'),
												array('prompt'=>'-- Tipo --')
			),
			'type' => 'raw',
			'value' => function($data)
			{
				if($data->id_tipo_programa == 1)
					return '<span style="font-size:18px" class="label label-success">I</span>';
				else
					return '<span style="font-size:18px" class="label label-success">E</span>';
			},
			'htmlOptions' => array('width'=>'12px', 'class'=>'text-center')
		),*/
		array(
			'name' => 'nombreSupervisor',
			'header' => 'Nombre del Supervisor',
			'value' => function($data)
			{
				//Guardamos el Nombre del Supervisor
				$nombres;
			
				switch($data->id_tipo_programa)
				{
					case 1: //Interno
						//hacemos la consulta para obtener el nombre completo del supervisor del programa con ese id
						$query = "select hemp.\"nmbEmpleado\" as nombres
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								join public.\"H_empleados\" hemp
								on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpi.superv_principal_int = true
							";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$nombres = $result[0]['nombres'];
						
						return $nombres;
					break;
					case 2: //Externo
						$query = "select sp.nombre_supervisor as nombres
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								join pe_planeacion.ss_supervisores_programas sp
								on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' 
								and rpe.superv_principal_ext = true
							";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$nombres = $result[0]['nombres'];

						return $nombres;
					break;
					default:
						return "--"; //No hay datos del Programa

				} 

			},
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'header' => 'Apellido Paterno',
			'name' => 'apePaterno',
			'value' => function($data)
			{
				//Guardamos el apellido paterno del supervisor
				$ape_paterno;
				
				switch($data->id_tipo_programa)
				{
					case 1:
						$query = "select hemp.\"apellPaterno\" as apellido_paterno
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								join public.\"H_empleados\" hemp
								on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpi.superv_principal_int = true
							";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$ape_paterno = $result[0]['apellido_paterno'];

						return $ape_paterno;
					break;
					case 2:
						$query = "select sp.apell_paterno as apellido_paterno
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								join pe_planeacion.ss_supervisores_programas sp
								on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpe.superv_principal_ext = true
							";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$ape_paterno = $result[0]['apellido_paterno'];

						return $ape_paterno;
					break;
					default:
					 return "--";
				}
			},
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'header' => 'Apellido Materno',
			'name' => 'apeMaterno',
			'value' => function($data)
			{
				//Guardamos el apellido materno del supervisor
				$ape_materno;

				switch($data->id_tipo_programa)
				{
					case 1:
						$query = "select hemp.\"apellMaterno\" as apellido_materno
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								join public.\"H_empleados\" hemp
								on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpi.superv_principal_int = true
							";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$ape_materno = $result[0]['apellido_materno'];

						return $ape_materno;
					break;
					case 2:
						$query = "select sp.apell_materno as apellido_materno
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								join pe_planeacion.ss_supervisores_programas sp
								on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
								where pss.id_programa = '$data->id_programa' and pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpe.superv_principal_ext = true
							";

						$result = Yii::app()->db->createCommand($query)->queryAll();
						$ape_materno = $result[0]['apellido_materno'];

						return $ape_materno;
					break;
					default:
					 return "--";
				}
			},
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'header' => 'Estudiantes Solicitados',
			'name'=>'numero_estudiantes_solicitados',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->numero_estudiantes_solicitados.'</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'header'=>'Tipo <br>Programa', //
			'filter' => CHtml::activeDropDownList(
								$modelSSProgramas,
								'id_periodo_programa',
								$tipo_programa,
								array('prompt'=>' -- Filtrar por --')

			),
			'type' => 'raw',
			'value' => function($data)
			{
				$model = SsPeriodosProgramas::model()->findByPk($data->id_periodo_programa);

				return ($model->periodo_programa != NULL) ? '<span style="font-size:10px" class="label label-success">'.$model->periodo_programa.'</span>' : '--';
			},
			'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editProgramaServicioSocial}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editProgramaServicioSocial' => array
				(
					'label'=>'Asignar Numero Alumnos a Programas',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/editarDeptoProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/editar_32.png'
				),
			),
		),
		//'fecha_fin_programa',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detAdminProgramaServicioSocial}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detAdminProgramaServicioSocial' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/detalleAdminProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png'
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{addLugPrograma}',
			'header'=>'Agregar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'addLugPrograma' => array
				(
					'label'=>'Agregar Lugares al Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/addLugaresProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/agregarr_32.png'
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delLugPrograma}',
			'header'=>'Quitar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delLugPrograma' => array
				(
					'label'=>'Quitar Lugares al Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/delLugaresProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/completo_32.png'
				),
			),
        ),
		array(
			'header' => 'Estatus',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data, $fecha_actual)
			{
				return ($fecha_actual < $data->fecha_fin_programa) ? '<span style="size:18px" class="label label-success">VIGENTE</span>' : '<span style="size:18px" class="label label-danger">CADUCADO</span>';
			},
			'htmlOptions' => array('width'=>'87px', 'class'=>'text-center')
		),
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{cancelar}, {cancelado}', // buttons here...
			'header' 			   => 'Cancelar Programa',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'cancelar'   => [
					'label'   => 'Cancelar Programa',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssProgramas/cancelarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=> 'images/servicio_social/cancelado_32.png',
					//'visible'=> '$data->fecha_fin_programa > '.$fecha_actual.' and ($data->id_status_programa != 3 and $data->id_status_programa != 4)', //Habilitado si el programa esta dado de ALTA
					'visible' => function($fecha_actual, $data)
					{
						//Si ya se libero al menos un servicio social de ese programa ya no se podra eliminar
						$query = "select * from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_servicio_social ss
									on ss.id_programa = pss.id_programa
									where pss.id_programa = '$data->id_programa' AND 
									(ss.id_estado_servicio_social = 5 OR ss.id_estado_servicio_social = 6)
								";

						$datos = Yii::app()->db->createCommand($query)->queryAll();
						$cond1 = (count($datos) > 0) ? false : true;
						$cond2 = ($data->id_status_programa != 3 OR $data->id_status_programa != 4) ? true : false;
						$cond3 = ($data->fecha_fin_programa > $fecha_actual) ? true : false;

						return ($cond1 == true AND $cond2 == true AND $cond3 == true) ? true : false;
					},
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Cancelación del Programa?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
				'cancelado'   => [
					'label'   => 'No Disponible',
					//'url'     => '#',
					'imageUrl'=> 'images/servicio_social/bloquedo_32.png',
					//'visible'=> '$data->fecha_fin_programa < '.$fecha_actual.' and ($data->id_status_programa = 3 or $data->id_status_programa = 4)',
					'visible' => function($fecha_actual, $data)
					{
						//Si ya se libero al menos un servicio social de ese programa ya no se podra eliminar
						$query = "select * from pe_planeacion.ss_programas pss
									join pe_planeacion.ss_servicio_social ss
									on ss.id_programa = pss.id_programa
									where pss.id_programa = '$data->id_programa' AND 
									(ss.id_estado_servicio_social = 5 OR ss.id_estado_servicio_social = 6)
								";

						$datos = Yii::app()->db->createCommand($query)->queryAll();
						$cond1 = (count($datos) > 0) ? true : false;
						$cond2 = ($data->id_status_programa == 3 OR $data->id_status_programa == 4) ? true : false;
						$cond3 = ($data->fecha_fin_programa < $fecha_actual) ? true : false;

						return ($cond1 == true or $cond2 == true or $cond3 == true) ? true : false;
					},
				],
			],
		],
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{vigente} ,{noDispon}', // buttons here...
			'header' 			   => 'Finalizar Programa',
			'htmlOptions' 		   => array('width'=>'70px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'vigente'   => [
					'label'   => 'Finalizar Programa',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssProgramas/finalizarProgramaServicioSocial", array("id_programa"=>$data->id_programa))',
					'imageUrl'=> 'images/servicio_social/no_aprobado_32.png',
					'visible'=> function($row, $data)
					{
						//Aparece cuando todos lo servicios sociales del programa han finalizado
						$id_p = $data->id_programa;
						$qry = "select * from pe_planeacion.ss_programas p
								join pe_planeacion.ss_servicio_social ss
								on ss.id_programa = p.id_programa
								where p.id_programa = '$id_p' AND (ss.id_estado_servicio_social = 1 or ss.id_estado_servicio_social = 2 or
								ss.id_estado_servicio_social = 3 or ss.id_estado_servicio_social = 4 or ss.id_estado_servicio_social = 5) ";

						$rs = Yii::app()->db->createCommand($qry)->queryAll();

						return (count($rs) == 0) ? true : false;
					},
					'options' => [
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Finalización del Programa?', // custom attribute to hold confirmation message
					],
					'click'   => $JsFinalizaPrograma, // JS string which processes AJAX request
				],
				'noDispon'   => [
					'label'   => 'Programa Vigente',
					//'url'     => '#',
					'imageUrl'=> 'images/servicio_social/quitar_32.png',
					'visible'=> function($row, $data)
					{
						//Aparece cuando el servicio social de al menos alumno aun no ha concluido (FINALIZADO)
						$id_p = $data->id_programa;

						$qry = "select * from pe_planeacion.ss_programas p
								join pe_planeacion.ss_servicio_social ss
								on ss.id_programa = p.id_programa
								where p.id_programa = '$id_p' AND (ss.id_estado_servicio_social = 1 or ss.id_estado_servicio_social = 2 or
								ss.id_estado_servicio_social = 3 or ss.id_estado_servicio_social = 4 or ss.id_estado_servicio_social = 5) ";
						
						$rs = Yii::app()->db->createCommand($qry)->queryAll();

						return (count($rs) > 0) ? true : false;

					}
				],
			],
		],//Fin
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
