<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas Servicio Social' => array('ssProgramas/listaProgramasAlumnoServicioSocial'),
    'Detalle del Programa'
);

?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información del Programa
		</span>
	</h2>
</div>

<br>
<div class="row"><!--Row Principal-->
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Detalle del Programa</h6>
			</div>
			<div class="panel-body">

				<div style="padding-top:20px;" class="col-lg-3" align="center">
					<?php
						echo '<img align="center" height="200" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/programa_default.png"/>';
					?>
				</div>
				<div class="col-lg-9">
					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('nombre_programa')); ?>:</b>&nbsp;
			    	 <?php echo CHtml::encode($modelSSProgramas->nombre_programa); ?></p>

					<p><b><?php echo "Nombre del Supervisor:"; ?></b>&nbsp;
			    	 <?php echo $nombre_supervisor; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_periodo_programa')); ?>:</b>&nbsp;
			    	 <?php echo $periodo_programa; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>&nbsp;
				     <?php echo '<span style="font-size:18px" class="label label-success">'.CHtml::encode($modelSSProgramas->horas_totales).' horas</span>'; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('fecha_inicio_programa')); ?>:</b>&nbsp;
			    	 <?php echo $fec_inicio; ?></p>

			        <p><b><?php echo "Fecha Aproximada de Terminación:"; ?></b>&nbsp;
			    	 <?php echo $fec_fin; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>&nbsp;
			    	 <?php echo ($modelSSProgramas->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO"; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('lugares_disponibles')); ?>:</b>&nbsp;
                         <?php 
                            echo ($modelSSProgramas->lugares_disponibles > 0) ? '<span style="font-size:18px" class="label label-success">'.$modelSSProgramas->lugares_disponibles."</span>" : '<span style="font-size:18px" class="label label-danger">'.$modelSSProgramas->lugares_disponibles."</span>"; 
                         ?></p>

				</div>
			</div>
		</div>
	</div>
</div><!--Row Principal-->

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información Gral. del Programa
		</span>
	</h2>
</div>

<br><br>
<?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$modelSSProgramas,
        'attributes'=>array(
            //'id_programa_servicio_social',
            array(
                'header'=>'Empresa',
                'name' => 'idUnidadReceptora.nombre_unidad_receptora'
            ),
            'lugar_realizacion_programa',
            'horas_totales',
            'idPeriodoPrograma.periodo_programa',//id_periodo_programa
            'numero_estudiantes_solicitados',
			'lugares_ocupados',
			'lugares_disponibles',
            'descripcion_objetivo_programa',
            'idTipoServicioSocial.tipo_servicio_social',//id_tipo_servicio_social
            'impacto_social_esperado',
            'beneficiarios_programa',
            'actividades_especificas_realizar',
            'mecanismos_supervision',
            'perfil_estudiante_requerido',
            'idApoyoEconomicoPrestador.descripcion_apoyo_economico',//id_apoyo_economico_prestador_servicio_social
            'idTipoApoyoEconomico.tipo_apoyo_economico',//id_tipo_apoyo_economico
            'apoyo_economico',
            array(
                'name' => 'idRegistroFechasSsocial.anio',
                'oneRow' => true
            ),
            array(
                'name' => 'idRegistroFechasSsocial.id_periodo',
                'oneRow' => true,
                'value' => function($data)
                {
                    $periodo = $data->idRegistroFechasSsocial->id_periodo;
                    $query = "select * from public.\"E_periodos\"
                    where \"numPeriodo\" = '$periodo' ";
                    $perido = Yii::app()->db->createCommand($query)->queryAll();

                    return $perido[0]['dscPeriodo'];
                },
            ),
            array(
                'name' => 'id_recibe_capacitacion',
                'oneRow'=>true,
                'value' => function($data)
                {
                    return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO";
                },
            ),
            'idClasificacionAreaServicioSocial.clasificacion_area_servicio_social',//id_clasificacion_area_servicio_social
            ),
)); ?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>


<br><br><br>
<div class="alert alert-warning">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        <b>El Horarios del Programa puede estar sujeto a cambios para la disponibilidad del o los Alumnos solicitantes.</b>
    </strong></p>
</div>

<?php
for($i=0; $i < count($modelSSHorarioDiasHabilesProgramas); $i++){
$this->widget('serviciosocial.components.DetailView4Col', array(
    'data'=>$modelSSHorarioDiasHabilesProgramas[$i],
    'attributes'=>array(
        //'id_horario',
        //'id_programa_servicio_social', id_dia_semana
        array(
            'name' => 'idDiaSemana.dia_semana',
            'oneRow'=>true,
        ),
        'hora_inicio',
        'hora_fin',
        //'horas_totales',
    ),
));
echo "<br><br>";
}
?>

<br><br>
<div align="center">
    <?php //echo CHtml::link('Volver al menú de Programas', array('listaProgramasAlumnoServicioSocial'), array('class'=>'btn btn-success')); ?>
</div>
<br><br>
