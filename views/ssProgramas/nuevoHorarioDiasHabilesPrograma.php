<?php

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Programas Servicio Social' => array('ssProgramas/listaProgramasSupervisorServicioSocial'),
    'Horario Dias Habiles del Programa'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Programa de Servicio Social
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Elije los Días Hábiles del Programa
                </h6>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this HorarioDiasHabilesProgramasController */
                /* @var $model HorarioDiasHabilesProgramas */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'horario-dias-habiles-programas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

                    <?php echo $form->errorSummary($modelSSHorarioDiasHabilesProgramas); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
                        <?php echo $form->dropDownList($modelSSHorarioDiasHabilesProgramas,
                                                    'id_dia_semana',
                                                    $lista_dias,
                                                    array('prompt'=>'--Dia de la semana--','class'=>'form-control', 'required'=>'required'
                                                    )); ?>
                        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'id_dia_semana'); ?>
                    </div>
                    
                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
                        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); EJuiDateTimePicker?>
                        <?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
                                'model'=>$modelSSHorarioDiasHabilesProgramas,
                                'attribute'=>'hora_inicio',
                                'language' => 'es',
                                'options' => array(
                                        'showOn'=>'focus',
                                        'timeOnly'=>true,
                                        'showHour'=>true,
                                        'showMinute'=>true,
                                        //'showSecond'=>true,
                                        'timeFormat'=>'hh:mm:ss',
                                ),
                                'htmlOptions' => array(
                                        'style'=>'width:150px;', // styles to be applied
                                        'maxlength' => '10',    // textField maxlength
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'readOnly' => true
                                ),
                        ));?>
                        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_inicio'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
                        <?php //echo $form->textField($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
                        <?php $this->widget('ext.timepicker.EJuiDateTimePicker', array(
                                'model'=>$modelSSHorarioDiasHabilesProgramas,
                                'attribute'=>'hora_fin',
                                'language' => 'es',
                                'options' => array(
                                        'showOn'=>'focus',
                                        'timeOnly'=>true,
                                        'showHour'=>true,
                                        'showMinute'=>true,
                                        //'showSecond'=>true,
                                        'timeFormat'=>'hh:mm:ss',
                                ),
                                'htmlOptions' => array(
                                        'style'=>'width:150px;', // styles to be applied
                                        'maxlength' => '10',    // textField maxlength
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'readOnly' => true
                                ),
                        ));?>
                        <?php echo $form->error($modelSSHorarioDiasHabilesProgramas,'hora_fin'); ?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelSSHorarioDiasHabilesProgramas->isNewRecord ? 'Guardar Horario' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                        <?php 
                            if($hayRegHorario == true)
                                echo CHtml::link('Terminar Registro', array('listaProgramasSupervisorServicioSocial'), array('class'=>'btn btn-danger')); 
                        ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Días Hábiles del Programa
		</span>
	</h2>
</div>


<!--SE MUESTRAN LOS DIAS HABILES QUE SE VAN ELIGIENDO PARA EL PROGRAMA-->
<br><br><br>
<div class="row">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'contador-horario-dias-habiles-programas-grid',
        'dataProvider'=>$modelSSHorarioDiasHabilesProgramas->searchEditarHorarioPrograma($modelSSProgramas->id_programa),
        'filter'=>$modelSSHorarioDiasHabilesProgramas,
        'columns'=>array(
            array(
                'class'=>'CButtonColumn',
                'template'=>'{horaProgramaServicioSocial}',
                'header'=>'Horario',
                'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
                'buttons'=>array
                (
                    'horaProgramaServicioSocial' => array
                    (
                        'label'=>'Hora y dia del Programa',
                        //'url'=>'#',
                        'imageUrl'=>'images/servicio_social/horario_32.png'
                    ),
                ),
            ),
            //'id_horario',
            /*array(
                'name' => 'id_programa_servicio_social',
                'filter' => false,
                'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
            ),*/
            array(
                'name' => 'idDiaSemana.dia_semana',
                'filter' => false,
                'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
            ),
            array(
                'name' => 'hora_inicio',
                'filter' => false,
                'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
            ),
            array(
                'name' => 'hora_fin',
                'filter' => false,
                'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
            ),
            //Falta calcular las horas dependiendo de los campos hora_inicio y hora_fin
            array(
                'name' => 'horas_totales',
                'filter' => false,
                'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
            )
        ),
    )); ?>
</div>

<br><br>
<div align="center">
    <?php //echo CHtml::link('Terminado', array('/'), array('class'=>'btn btn-success')); ?>
</div>
<!--SE MUESTRAN LOS DIAS HABILES QUE SE VAN ELIGIENDO PARA EL PROGRAMA-->


<br><br><br><br><br>
<br><br><br><br><br>