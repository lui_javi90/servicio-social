<?php

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Lista Programas Activos' => array('ssProgramas/listaProgramasActivos'),
    'Cambiar Supervisor del Programa'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Cambiar Supervisor del Programa
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información del Programa
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-lg-3" align="center">
					<?php
					    echo '<img align="center" height="160" src="'. Yii::app()->request->baseUrl.'/images/programa_default.png"/>';
					?>
				</div>
				<div class="col-lg-9">
					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('nombre_programa')); ?>:</b>&nbsp;
			    	 <?php echo CHtml::encode($modelSSProgramas->nombre_programa); ?></p>

					<p><b><?php echo "Nombre del Supervisor: "; ?></b>&nbsp;
			    	 <?php echo $nombre_supervisor; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_periodo_programa')); ?>:</b>&nbsp;
			    	 <?php echo $periodo_programa; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('horas_totales')); ?>:</b>&nbsp;
				     <?php echo CHtml::encode($modelSSProgramas->horas_totales)." horas"; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('fecha_inicio_programa')); ?>:</b>&nbsp;
			    	 <?php echo $fec_inicio; ?></p>

			        <p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('fecha_fin_programa')); ?>:</b>&nbsp;
			    	 <?php echo $fec_fin; ?></p>

					<p><b><?php echo CHtml::encode($modelSSProgramas->getAttributeLabel('id_tipo_programa')); ?>:</b>&nbsp;
			    	 <?php echo ($modelSSProgramas->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO"; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Alumnos Asignados al Programa
		</span>
	</h2>
</div>

<!--ext.widgets.DetailView4Col-->
<?php for($i = 0; $i < count($modelSSServicioSocial) ;$i++){ ?>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$modelSSServicioSocial[$i],
    'attributes'=>array(
        array(
            'type' => 'raw',
            'value' => empty($modelSSServicioSocial[$i]['no_ctrl']) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$modelSSServicioSocial[$i]['no_ctrl'], 'foto', array("width"=>"100px")),
        ),
        'noCtrl.nmbAlumno',
        array(
            'name' => 'noCtrl.cveEspecialidadAlu',
            'value' => function ($data)
            {
                $id = $data->noCtrl->cveEspecialidadAlu;
                $query = "Select \"dscEspecialidad\" as carrera from public.\"E_especialidad\" where \"cveEspecialidad\" = '$id' ";
                $carrera = Yii::app()->db->createCommand($query)->queryAll();

                return $carrera[0]['carrera'];
            },
        ),
        'no_ctrl'
        /*'fecha_registro',
        'fecha_modificacion',
        'rfcDirector',
        'rfcSupervisor',
        'cargo_supervisor',
        'empresaSupervisorJefe',
        'nombre_jefe_depto',
        'departamentoSupervisorJefe',
        'rfcJefeOficServicioSocial',
        'rfcJefeDeptoVinculacion',
        'nombre_supervisor',*/
    ),
));
echo "<br><br>";
} ?>

<br><br><br>
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h6 class="panel-title">
				Cambiar Supervisor del Programa
			</h6>
        </div>
        <div class="panel-body">
            <?php if($modelSSProgramas->id_tipo_programa == 2){ ?>
                <!--BLOQUE PARA AGREGAR SUPERVISOR EXTERNO AL PROGRAMA-->
                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ss-supervisores-programas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('autocomplete'=>'off')
                )); ?>

                <?php echo $form->errorSummary($modelSSSupervisoresProgramas); ?>

                <div class="col-lg-4">
                    <div align="center">
                        <br><br><br>
                        <h4>Elegir un Supervisor para el Programa</h4>
                    </div>

                    <br><br>
                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
                        <?php echo $form->dropDownList($modelSSSupervisoresProgramas,
                                                    'rfcSupervisor',
                                                    $lista_supervisores,
                                                    //array('prompt'=>'-- Supervisores --', 'class'=>'form-control', 'required'=>'required'));
                                                    array(
                                                        'prompt'=>'-- Elegir Supervisor --', 'class'=>'form-control', 'required'=>'required',
                                                        'ajax'=>array(
                                                            'type'=>'POST',
                                                            'dataType'=>'json',
                                                            'data' => array(
                
                                                                'rfcSupervisor'=>'js:$(\'#SsSupervisoresProgramas_rfcSupervisor option:selected\').val()',
                                                            ),
                                                            'url'=>CController::createUrl('ssProgramas/mostrarDetalleSupervisorExterno'),
                                                            'success'=>'function(data) {
                                                                $("#SsSupervisoresProgramas_nombre_supervisor").val(data.nombre);
                                                                $("#SsSupervisoresProgramas_apell_paterno").val(data.apellido_pat);
                                                                $("#SsSupervisoresProgramas_apell_materno").val(data.apellido_mat);
                                                                $("#SsSupervisoresProgramas_cargo_supervisor").val(data.cargo);
                                                                $("#SsSupervisoresProgramas_foto_supervisor").val(data.foto);
                                                            }',
                                                        ))
                                                    );?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'rfcSupervisor'); ?>
                    </div>

                <br><br><br>
                </div>

                <div class="col-lg-8">
                    <h3 align="center">Detalle del Supervisor Seleccionado</h3>

                    <div align="center" id="image">
                        <?php  

                            //die();
                       
                        //echo "data:".$modelSSSupervisoresProgramas->foto_supervisor;
                        //echo '<img align="center" id="var" width="250" heigth="200" src="'. Yii::app()->request->baseUrl.'/images/supervisores/'.$var2.'"/>';
                        ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'nombre_supervisor'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'nombre_supervisor',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'nombre_supervisor'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'apell_paterno'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'apell_paterno',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'apell_paterno'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'apell_materno'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'apell_materno',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'apell_materno'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSSupervisoresProgramas,'cargo_supervisor'); ?>
                        <?php echo $form->textField($modelSSSupervisoresProgramas,'cargo_supervisor',array('class' => 'form-control', 'readOnly'=>true)); ?>
                        <?php echo $form->error($modelSSSupervisoresProgramas,'cargo_supervisor'); ?>
                    </div>

                     <br><br>
                    <div align="center" class="form-group">
                        <?php echo CHtml::submitButton($modelSSSupervisoresProgramas->isNewRecord ? 'Agregar Supervisor' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                        <?php echo CHtml::link('Cancelar', array('listaProgramasActivos'), array('class'=>'btn btn-danger')); ?>
                    </div>
                
                </div>

                <?php $this->endWidget(); ?>
                <!--BLOQUE PARA AGREGAR SUPERVISOR EXTERNO AL PROGRAMA-->
            <?php }else{ ?>
            <!--BLOQUE PARA AGREGAR SUPERVISOR INTERNO AL PROGRAMA-->
                    <?php
                    /* @var $this HEmpleadosController */
                    /* @var $model HEmpleados */
                    /* @var $form CActiveForm */
                    ?>

                    <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'hempleados-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                    )); ?>

                    <div class="col-lg-4">
                    <div align="center">
                        <br><br><br>
                        <h4>Elegir un Supervisor para el Programa</h4>
                    </div>

                    <br><br>
                        <?php echo $form->errorSummary($modelHEmpleados); ?>
                        <div class="form-group">
                            <p><b>RFC del Supervisor </b><span class="required">*</span></p>
                            <?php echo $form->dropDownList($modelHEmpleados,
                                                        'rfcEmpleado', 
                                                        $lista_supervisores,
                                                        array(
                                                            'prompt'=>'-- Elegir Supervisor --', 'class'=>'form-control', 'required'=>'required',
                                                            'ajax'=>array(
                                                                'type'=>'POST',
                                                                'dataType'=>'json',
                                                                'data' => array(
                    
                                                                    'rfcEmpleado'=>'js:$(\'#HEmpleados_rfcEmpleado option:selected\').val()',
                                                                ),
                                                                'url'=>CController::createUrl('ssProgramas/mostrarDetalleSupervisorInterno'),
                                                                'success'=>'function(data) {
                                                                    $("#HEmpleados_nmbEmpleado").val(data.nombre);
                                                                    $("#HEmpleados_apellPaterno").val(data.apellido_pat);
                                                                    $("#HEmpleados_apellMaterno").val(data.apellido_mat);
                                                                    $("#HEmpleados_cveDepartamentoEmp").html(data.departamento);
    
                                                                }',
                                                            ))
                                                        ); ?>
                            <?php echo $form->error($modelHEmpleados,'rfcEmpleado'); ?>
                        </div>
                        <br><br><br><br><br>
                    </div>

                        <div class="col-lg-8">
                            <h3 align="center">Detalle del Supervisor Seleccionado</h3>

                            <div class="form-group">
                                <b>Nombre Completo</b>
                                <?php echo $form->textField($modelHEmpleados,'nmbEmpleado',array('class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                                <?php echo $form->error($modelHEmpleados,'nmbEmpleado'); ?>
                            </div>

                            <div class="form-group">
                                <b>Apellido Paterno</b>
                                <?php echo $form->textField($modelHEmpleados,'apellPaterno',array('class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                                <?php echo $form->error($modelHEmpleados,'apellPaterno'); ?>
                            </div>

                            <div class="form-group">
                                <b>Apellido Materno</b>
                                <?php echo $form->textField($modelHEmpleados,'apellMaterno',array('class'=>'form-control', 'required'=>'required', 'readOnly'=>true)); ?>
                                <?php echo $form->error($modelHEmpleados,'apellMaterno'); ?>
                            </div>

                             <div class="form-group">
                                <b>Departamento</b>
                                <?php echo $form->dropDownList($modelHEmpleados,
                                                            'cveDepartamentoEmp',
                                                            array(),
                                                            array('class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
                                                            ); ?>
                                <?php echo $form->error($modelHEmpleados,'cveDepartamentoEmp'); ?>
                            </div>

                            <br><br>
                            <div class="form-group">
                                <?php echo CHtml::submitButton($modelHEmpleados->isNewRecord ? 'Agregar Supervisor' : 'Agregar Supervisor', array('class'=>'btn btn-primary')); ?>
                                <?php echo CHtml::link('Cancelar', array('listaProgramasActivos'), array('class'=>'btn btn-danger')); ?>
                            </div>
                        </div>

                    <?php $this->endWidget(); ?>

                    </div><!-- form -->

                <!--BLOQUE PARA AGREGAR SUPERVISOR INTERNO AL PROGRAMA-->
                <?php }?>
        </div>
    </div>
</div>
