<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Historico Programas de Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Historico Programas de Servicio Social
		</span>
	</h2>
</div>


<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Solo apareceran los Programas que hayan sido Finalizados o Cancelados.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-programas-grid',
    'dataProvider'=>$modelSSProgramas->searchHistoricoProgramas($anio, $periodo),
    'filter'=>$modelSSProgramas,
    'columns'=>array(
        //'id_programa',
        array(
			'header' => 'Supervisor',
            'type'=>'raw',
			'value' => function($data)
			{
				$rfc;
				$foto;
				switch($data->id_tipo_programa)
				{
					case 1:
						$query = "select rpi.\"rfcEmpleado\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = pss.id_programa
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpi.superv_principal_int = true";
						
						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
						$query2 = "select foto from public.\"H_credencialEmpleado\" where \"rfcEmpleado\" = '$rfc' ";
						$res = Yii::app()->db->createCommand($query2)->queryAll();

						//Guardamos la foto en caso de que hayan subido una
						$foto = $res[0]['foto'];

						return ($foto != NULL) ? CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;")) : CHtml::image("images/servicio_social/masculino2.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
					break;
					case 2:
						$query = "select rpe.\"rfcSupervisor\" as rfc
								from pe_planeacion.ss_programas pss
								join pe_planeacion.ss_responsable_programa_externo rpe
								on rpe.id_programa = pss.id_programa
								where pss.id_programa = '$data->id_programa' and 
								pss.id_tipo_programa = '$data->id_tipo_programa' and 
								rpe.superv_principal_ext = true";
						
						$result = Yii::app()->db->createCommand($query)->queryAll();
						$rfc = $result[0]['rfc'];

						//Obtenemos la foto de Perfil del Supervisor Interno en caso de que tenga en SII
						$query2 = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfc' ";
						$res = Yii::app()->db->createCommand($query2)->queryAll();

						//Guardamos la foto en caso de que hayan subido una
						$foto = $res[0]['foto_supervisor'];

						//Si hay foto guardada en la BD
						if($foto != NULL)
						{
							if(trim($foto) != 'masculino.png' or trim($foto) != 'femenino.png')
								return CHtml::image("images/servicio_social/supervisores/".$rfc."/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							else
								return CHtml::image("images/servicio_social/supervisores/".$foto, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

						}else
						{
		
							if($foto[0]['id_sexo'] == 1)
								return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							elseif($foto[0]['id_sexo'] == 2)
								return CHtml::image("images/servicio_social/supervisores/femenino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
							else
								return CHtml::image("images/servicio_social/supervisores/masculino.png", '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
						}
						
					break;
				}
				
			},//Fin Funcion value
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'nombre_programa',
            'filter' => false,
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
			'header'=>'Empresa',
			'name' => 'idUnidadReceptora.nombre_unidad_receptora',
			'filter' => false,
			'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
		),
        //'lugar_realizacion_programa',
        array(
			'name'=>'idPeriodoPrograma.periodo_programa',//tipo_programa
			'filter' => CHtml::activeDropDownList($modelSSProgramas,
												 'id_periodo_programa',
												 $tipo_programa,
												 array('prompt'=>'-- Filtrar por  --')
											   ),
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
        /*array(
            'name' => 'horas_totales',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'numero_estudiantes_solicitados',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),*/
        array(
            'header' => 'Periodo',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($modelSSProgramas,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'value' => function($data)
            {
                $model = SsRegistroFechasServicioSocial::model()->findByPk($data->id_registro_fechas_ssocial);

                return ($model->id_periodo == 1) ? '<span style="font-size:14px" class="label label-success">ENERO-JUNIO</span>' : '<span style="font-size:14px" class="label label-success">AGOSTO-DICIEMBRE</span>';
            },
            'htmlOptions' => array('width'=>'140px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Año',
            'filter' => CHtml::activeDropDownList($modelSSProgramas,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $model = SsRegistroFechasServicioSocial::model()->findByPk($data->id_registro_fechas_ssocial);

                return '<span style="font-size:14px" class="label label-success">'.$model->anio.'</span>';
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Estatus',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				switch($data->id_status_programa)
				{
					case 3:
						return '<span style="font-size:14px" class="label label-info">FINALIZADO</span>';
					break;
					case 4:
						return '<span style="font-size:14px" class="label label-danger">CANCELADO</span>';
					break;
					default: 
						return '<span style="font-size:14px" class="label label-danger">SIN ESPECIFICAR</span>';
					break;
				}
			},
			'htmlOptions' => array('width'=>'87px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{detAdminProgramaServicioSocial}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detAdminProgramaServicioSocial' => array
				(
					'label'=>'Detalle Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssProgramas/showDetalleProgramaHistServicioSocial", array("id_programa"=>$data->id_programa))',
                    'imageUrl'=>'images/servicio_social/detalle_32.png',
                    'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#det_programa_1").dialog("open"); 
						
						return false;
					}',
				),
			),
		),
    ),
)); ?>


<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'det_programa_1',
	'options' => array(
		'title' => 'Detalle del Programa',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1200, 
		'height' => 750,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<br><br><br><br><br>
<br><br><br><br><br>