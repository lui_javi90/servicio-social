<?php
/* @var $this ObservacionesServicioSocialController */
/* @var $model ObservacionesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Observaciones Alumnos Servicio Social' => array('ssServicioSocial/listaDeptoObservacionesServicioSocial'),
	'Historial Observaciones Servicio Social'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Observaciones Servicio Social
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información del Alumno
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3 text-center" >
					<!--Es un Web Service-->
					<img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno" height="200">
					<!--Es un Web Service-->
				</div>
                <div class="col-xs-9" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['name_alumno']; ?></p>

					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['carrera']; ?></p>

					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['periodo_servsocial'] ?></p>

                    <p><b>Calificación del Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['calificacion_servicio_social'] >=70) ?
                                "<b><FONT COLOR=\"green\">".$servicioSocialAlumno[0]['calificacion_servicio_social']."</FONT></b>" :
                                "<b><FONT COLOR=\"red\">".$servicioSocialAlumno[0]['calificacion_servicio_social']."</FONT></b>"; ?>

                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Historial
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'observ-hist-servicio-social-grid',
	'dataProvider'=>$modelSSObservacionesServicioSocial->searchXHistorialAlumnoServicioSocial($id_servicio_social, $fue_leida),
	'filter'=>$modelSSObservacionesServicioSocial,
	'columns'=>array(
		/*'id_observacion',
		array(
			'name' => 'id_servicio_social',
			'filter' => false,
			'htmlOptions' => array('width'=>'100px')
		),*/
		array(
			'name' => 'observacion',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		array(
			'header' => 'Fecha de Envio', //fecha_registro
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fecha_actualizacion = GetFormatoFecha::getFechaEnvioObservacionServicioSocial($data->id_observacion);

				return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'header' => 'Enviado Por',
			'value' => function($data)
			{
				return ($data->tipo_envio_observacion == 1) ? "OFICINA DEL SERVICIO SOCIAL" : "SUPERVISOR" ;
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
			'header' => 'Vista por el Alumno',
			//'name' => 'fue_leida',
			'value' => function($data)
			{
				return ($data->fue_leida == 0) ? "NO" : "SI";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'name' => 'fecha_leida',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fecha_actualizacion = GetFormatoFecha::getFechaVistaObservacionServicioSocial($data->id_observacion);

				return ($fecha_actualizacion[0]['fecha_act'] != "Sin Ver") ? $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'] : "No Vista";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statObservacionAlumnoServicioSocial}',
			'header'=>'Status',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statObservacionAlumnoServicioSocial' => array
				(
					'label'=>'Entregada',
					//'url'=>'#',
					'imageUrl'=>'images/aprobado_32.png',
				),
			),
		),
	),
)); ?>
