<style>
div.ex1 {
  /*background-color: lightblue;*/
  width: auto;
  height: 650px;
  overflow: auto;
}
.div1 {
    background-color: #EEEEEE;
}
.center{
    text-align: center;
}
</style>
<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Observaciones Recibidas' => array('ssObservacionesServicioSocial/historialObservacionesServicioSocial'),
    'Detalle de la Observación'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Observaciones Servicio Social
		</span>
	</h2>
</div>

<!--Para sacar la fecha en que fue vista la Observacion-->
<?php 
	//require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
	//$fecha_vio_obs = GetFormatoFecha::getFechaVistaObservacionServicioSocial($modelSSObservacionesServicioSocial->id_observacion);
?>
<!--Para sacar la fecha en que fue vista la Observacion-->

<br><br><br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Todos los mensajes enviados y recibidos unicamente seran vistos por el Alumno y su Supervisor de Programa.</b>
  </strong></p>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Historial de Conversación</h6>
			</div>
			<div class="panel-body ex1">
			<?php for($i = 0 ; $i < count($modelSSObservacionesServicioSocial); $i++){?>
					<!--Fecha de envio de la Observacion-->
					<div align="center">
						<?php 
						require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
						$fecha_env = GetFormatoFecha::getFechaEnvioObservacionServicioSocial($modelSSObservacionesServicioSocial[$i]['id_observacion']);
						echo ($fecha_env[0]['fecha_act'] != "Sin Ver") ? '<b><span style="font-size:16px" class="label label-default">'."Enviado el ".$fecha_env[0]['fecha_act'].' a las '.$fecha_env[0]['hora'].'</span></b>' : '<b><span style="font-size:16px" class="label label-default">...</span></b>'; 
						
						?>
					</div>
					<br>
					<b><?php
						if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'] != NULL)
						{
							switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'])
							{
								case 1 : echo "Jefe Oficina Servicio Social dice:"; break; //En esta parte no se utiliza 
								case 2 : echo "Supervisor ".$nombre_supervisor." dice:"; break;
								case 3 : echo "Alumno (a) ".$servicioSocialAlumno[0]['name_alumno']." dice :"; break;
							}
						}else
						if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'] != NULL)
						{
							switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'])
							{
								case 1 : echo "Jefe Oficina Servicio Social dice:"; break; //En esta parte no se utiliza 
								case 2 : echo "Supervisor ".$nombre_supervisor." dice:"; break;
								case 3 : echo "Alumno (a) ".$servicioSocialAlumno[0]['name_alumno']." dice :"; break;
							}
						}
						
						?></b>
					<br><br>
					<div class="row">
						<div align="center" class="col-xs-2">
							<?php
							if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'] != NULL)
							{
								if($id_tipo_programa == 1)
								{
									switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'])
									{//Inicio de Switch 1

										case 1: 
										//Foto del Admin
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/jefe_ssocial_512.png"/>'; 
											break;
										//Foto del Admin
										case 2:
										//Foto del Supervisor
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisor_512.png"/>'; 
											break;
										//Foto del Supervisor
										?>
										<?php case 3: ?>
											<!--Foto del Alumno-->
											<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="120">
											<!--Foto del Alumno-->
										<?php break;

									}//Fin de Switch 1

								}else if($id_tipo_programa == 2)
								{
									//EXTERNO
									switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'])
									{//Inicio de Switch 2
										case 1: 
										//Foto del Admin
											echo '<img  class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/jefe_ssocial_512.png"/>'; 
											break;
										//Foto del Admin
										case 2:
										//Foto del Supervisor
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$emisor_observ.'/'.$foto_emisor.'"/>'; 
											break;
										//Foto del Supervisor
										?>
										<?php case 3: ?>
											<!--Foto del Alumno-->
											<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="120">
											<!--Foto del Alumno-->
										<?php break;

									}//Fin de Switch de 2
								}
								
							}else
							if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'] != NULL)
							{
								if($id_tipo_programa == 1)
								{
									switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'])
									{//Inicio Switch 1
										case 1: 
										//Foto del Admin
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/jefe_ssocial_512.png"/>'; 
											break;
										//Foto del Admin
										case 2:
										//Foto del Supervisor
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisor_512.png"/>'; 
											break;
										//Foto del Supervisor
										?>
										<?php case 3: ?>
											<!--Foto del Alumno-->
											<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="120">
											<!--Foto del Alumno-->
										<?php break; 
									}//Fin Switch 1

								}else if($id_tipo_programa == 2)
								{
									switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'])
									{//Inicio Switch 2
										case 1: 
										//Foto del Admin
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/jefe_ssocial_512.png"/>'; 
											break;
										//Foto del Admin
										case 2:
										//Foto del Supervisor
											echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$emisor_observ.'/'.$foto_emisor.'"/>'; 
											break;
										//Foto del Supervisor
										?>
										<?php case 3: ?>
											<!--Foto del Alumno-->
											<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="120">
											<!--Foto del Alumno-->
										<?php break; 
									}//Fin Switch 2
								}
							}
							
							?>
						</div>
						<div class="col-xs-10">
							<div align="center" class="jumbotron">
								<p><?php
									echo $modelSSObservacionesServicioSocial[$i]['observacion']; 
								?></p>
							</div>
						</div>
					</div>
				<div align="center">
						<?php 
						require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
						$fecha_vist = GetFormatoFecha::getFechaVistaObservacionServicioSocial($modelSSObservacionesServicioSocial[$i]['id_observacion']);
						echo ($fecha_vist[0]['fecha_act'] != "Sin Ver") ? '<b><span style="font-size:16px" class="label label-success">'."Visto el ".$fecha_vist[0]['fecha_act'].' a las '.$fecha_vist[0]['hora'].'</span></b>' : '<b><span style="font-size:16px" class="label label-default">...</span></b>';
						?>
				</div>
				<hr>
				<br>
			<?php } ?>
				<!--CAJON PARA ESCRIBIR MENSAJES U OBSERVACIONES-->
				<div class="div1">
					<div class="form">
							<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'ss-resp-obs-servicio-social-form',
								// Please note: When you enable ajax validation, make sure the corresponding
								// controller action is handling ajax validation correctly.
								// There is a call to performAjaxValidation() commented in generated controller code.
								// See class documentation of CActiveForm for details on this.
								'enableAjaxValidation'=>false,
								'htmlOptions' => array('autocomplete'=>'off')
							)); ?>

							<?php echo $form->errorSummary($modelObservacion); ?>

							<div class="form-group">
								<p class="center"><b>Escribir Observación:</b></p>
								<?php echo $form->textArea($modelObservacion,'observacion', array('size'=>60,'maxlength'=>500, 'class'=>'form-control')); ?>
								<?php echo $form->error($modelObservacion,'observacion'); ?>
							</div>

							<br>
							<div align="center" class="form-group">
								<?php echo CHtml::submitButton($modelObservacion->isNewRecord ? 'Enviar Observación' : 'Enviar Observación', array('class'=>'btn btn-primary')); ?>
								<?php echo CHtml::link('Volver a Átras', array('historialObservacionesServicioSocial'), array('class'=>'btn btn-danger')); ?>
							</div>

						<?php $this->endWidget(); ?>
					</div><!-- form -->
				</div>
				<!--CAJON PARA ESCRIBIR MENSAJES U OBSERVACIONES-->

				<br>
                <div align="center">
                    <?php //echo CHtml::link('Entendido', array('historialObservacionesServicioSocial'), array('class'=>'btn btn-success right')); ?>
					<?php //echo CHtml::link('Responder Observación', array('responderObservacion', 'id_observacion'=>$modelSSObservacionesActual->id_observacion, 'emisor_observ'=>$modelSSObservacionesActual->emisor_observ), array('class'=>'btn btn-primary left')); ?>
                </div>
			</div>
		</div>
	</div>
</div>
