<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Observaciones Recibidas' => array('ssObservacionesServicioSocial/historialObservacionesServicioSocial'),
    'Detalle de la Observación' => array('ssObservacionesServicioSocial/verObservacionRecibidaAlumno', 'id_observacion'=>$modelSSObservacionesServicioSocial->id_observacion),
    'Responder Observación'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Responder Observación
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
                    Responder
				</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formResponderObsServicioSocial', array(
                                           'modelObservacion' => $modelObservacion,
                                           'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
                                           'modelEDatosAlumno' => $modelEDatosAlumno,
									)); ?>
			</div>
		</div>
	</div>
</div>