
<?php
/* @var $this ObservacionesServicioSocialController */
/* @var $model ObservacionesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Historial Observaciones Servicio Social'
);

?>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Observaciones Recibidas
		</span>
	</h2>
</div>

<!--
<div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Filtrar por Estatus de las Observaciones
                    </h3>
                </div>
                <div class="panel-body">-->
					<!--index.php hace referencia al mismo archivo donde se esta y get es el metodo de envio de datos-->
                    <?php //echo CHtml::beginForm("index.php", "get"); ?>

                    <!--<input type="hidden" name='r' value="serviciosocial/ssObservacionesServicioSocial/historialObservacionesServicioSocial">

                    <b>Estatus de las Observaciones:</b>
                    <div class="form-group">
										<?php /*echo CHtml::dropDownList('fue_leida',
														'',
														array('0'=>'Observaciones NO Leídas','1'=>'Observaciones Leídas'),
														array('prompt'=>'-- Filtra por estatus de las observaciones --','class'=>'form-control')
													);?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php echo CHtml::submitButton("Buscar Observaciones", array("class"=>"btn btn-primary")); ?>
                        <?php echo CHtml::link('Salir de aquí', array('/serviciosocial'), array('class'=>'btn btn-danger')); ?>
                    </div>

                    <?php echo CHtml::endForm(); */?>
                </div>
            </div>
        </div>
</div>-->

<br><br><br><br><br><br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Observaciones recibidas del Supervisor del Programa y/o del Jefe Oficina de Servicio Social</b>
  </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'observaciones-servicio-social-grid',
	'dataProvider'=>$modelSSObservacionesServicioSocial->searchXHistorialAlumnoServicioSocial($id_servicio_social),
	'filter'=>$modelSSObservacionesServicioSocial, //
	'columns'=>array(
		/*'id_observacion',
		array(
			'name' => 'id_servicio_social',
			'filter' => false,
			'htmlOptions' => array('width'=>'100px')
		),*/
		//'id_servicio_social',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{obsSupervisor}, {obsAdmin}, {obsAlumno}',
			'header'=>'',
			'htmlOptions'=>array('width:50px', 'class'=>'text-center'),
			'buttons'=>array(
				'obsSupervisor' => array(
					'label'=>'Observación del Supervisor',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/supervisor_32.png',
					'visible' => '$data->tipo_observacion_emisor == 2'
				),
				'obsAdmin' => array(
					'label'=>'Observación del Jefe Oficina Servicio Social',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/jefe_ssocial_32.png',
					'visible' => '$data->tipo_observacion_emisor == 1'
				),
				'obsAlumno' => array(
					'label'=>'Observación del Alumno',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/jefe_ssocial_32.png',
					'visible' => '$data->tipo_observacion_emisor == 3'
				),
			),
		),
		array(
			'header' => 'Conversación con',
			'value' => function($data)
			{
				//return ($data->tipo_envio_observacion == 1) ? "OFICINA DEL SERVICIO SOCIAL" : "SUPERVISOR" ;
				switch($data->tipo_observacion_emisor)
				{
					case 1 : return "OFICINA DEL SERVICIO SOCIAL"; break;
					case 2 : return "SUPERVISOR"; break;
					case 3 : return "ALUMNO"; break;
				}
			},
			'filter' => CHtml::activeDropDownList($modelSSObservacionesServicioSocial,
												  'tipo_observacion_emisor',
		array('1'=>'OFICINA DEL SERVICIO SOCIAL', '2'=>'SUPERVISOR'/*, '3'=>'ALUMNO'*/),
												  array('prompt'=>' -- Filtrar por Tipo de Observación -- ')
			),
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Enviada el', //fecha_registro
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fecha_actualizacion = GetFormatoFecha::getFechaEnvioObservacionServicioSocial($data->id_observacion);
				return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),
		array(
			'header' => 'Fecha de Vista', //fecha_registro
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				$fecha_actualizacion = GetFormatoFecha::getFechaVistaObservacionServicioSocial($data->id_observacion);
				return ($fecha_actualizacion[0]['fecha_act'] != "Sin Ver") ? $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'] : "...";
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'250px', 'class'=>'text-center')
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detalleObservacion}',
			'header'=>'Ver Convervación',
			'htmlOptions'=>array('width:100px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detalleObservacion' => array
				(
					'label'=>'Observación Recíbida',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssObservacionesServicioSocial/verObservacionRecibidaAlumno", 
					array("id_observacion"=>$data->id_observacion, "id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{observacionVista}, {observacionNoVista}',
			'header'=>'Estatus',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'observacionVista' => array
				(
					'label'=>'Hay Observaciones pendientes de ver',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => '$data->fue_leida == false'
				),
				'observacionNoVista' => array
				(
					'label'=>'No hay Observaciones pendientes de ver',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => '$data->fue_leida == true'
				),
			),
		),
		array(
			'class' => 'ComponentObservacionesXSupervisor',
			'header' => 'Observaciones Pendientes de Ver',
			'htmlOptions' => array('width' => '80px', 'class' => 'text-center'),
		),
	),
)); ?>
