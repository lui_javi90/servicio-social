<?php
/* @var $this SsObservacionesServicioSocialController */
/* @var $model SsObservacionesServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ss-resp-obs-servicio-social-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('autocomplete'=>'off')
)); ?>

    <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

    <?php echo $form->errorSummary($modelObservacion); ?>

    <div class="row">
        <?php echo $form->labelEx($modelObservacion,'observacion'); ?>
        <?php echo $form->textField($modelObservacion,'observacion', array('size'=>60,'maxlength'=>500, 'class'=>'form-control')); ?>
        <?php echo $form->error($modelObservacion,'observacion'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($modelObservacion->isNewRecord ? 'Enviar Respuesta' : 'Enviar Respuesta', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Ir Átras', array('verObservacionRecibidaAlumno', 'id_observacion'=>$modelSSObservacionesServicioSocial->id_observacion), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->