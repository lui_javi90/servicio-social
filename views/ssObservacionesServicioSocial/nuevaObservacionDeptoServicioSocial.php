<style>
div.ex1 {
  /*background-color: lightblue;*/
  width: auto;
  height: 650px;
  overflow: auto;
}
.div1 {
    background-color: #EEEEEE;
}
.center{
    text-align: center;
}
.jumbotron1
{   
    padding-top: 0px;
    padding-bottom:0px;
    /*background-image:url('images/car/car.jpg');*/
    background-size: cover;
    background: contain;
    width: 100%; /* make sure to define width to fill container */
    height: 10px; /* define the height in pixels or make sure   */
                   /* you have something in your div with height */
                   /* so you can see your image */
    max-width:1400px;  /* define the max width */
 }
</style>

<?php
/* @var $this ObservacionesServicioSocialController */
/* @var $model ObservacionesServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Observaciones Alumnos Servicio Social' => array('ssServicioSocial/listaDeptoObservacionesServicioSocial'),
	'Observaciones Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Observaciones Servicio Social
		</span>
	</h2>
</div>


<!--<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información del Alumno
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3 text-center" >
					
					<img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php //echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno" height="200">
                </div>
                <div class="col-xs-9" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php //echo $servicioSocialAlumno[0]['name_alumno']; ?></p>
					
					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php //echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php //echo $servicioSocialAlumno[0]['carrera']; ?></p>
					
					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php //echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php //echo $servicioSocialAlumno[0]['periodo_servsocial'] ?></p>

                    <p><b>Calificación del Servicio Social:</b>
                    &nbsp;&nbsp;<?php //echo ($servicioSocialAlumno[0]['calificacion_servicio_social'] >=70) ? 
                                //"<b><FONT COLOR=\"green\">".$servicioSocialAlumno[0]['calificacion_servicio_social']."</FONT></b>" :
                                //"<b><FONT COLOR=\"red\">".$servicioSocialAlumno[0]['calificacion_servicio_social']."</FONT></b>"; ?>
					
                </div>
            </div>
        </div>
    </div>
</div>-->

<br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Una vez enviada la Observación al Alumno ya no se podra editar ni eliminar.
    </strong></p>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Historial de Conversación</h6>
			</div>
			<div class="panel-body ex1">
			<?php 
			//Verificamos que haya datos, sino devolvemos 0
			$tamanio = count($modelSSObservacionesServicioSocial);
	
			for($i = 0 ; $i < $tamanio; $i++){ ?>
				<!--Fecha de visto-->
				<div align="center">
					<?php 
						require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
						$fecha_env = GetFormatoFecha::getFechaEnvioObservacionServicioSocial($modelSSObservacionesServicioSocial[$i]['id_observacion']);
						echo ($fecha_env[0]['fecha_act'] != "Sin Ver") ? '<b><span style="font-size:16px" class="label label-default">'."Enviado el ".$fecha_env[0]['fecha_act'].' a las '.$fecha_env[0]['hora'].'</span></b>' : '<b><span style="font-size:16px" class="label label-default">...</span></b>'; 
						
					?>
				</div>
				<!--Fecha de visto-->
				<br>
				<b><?php 
				if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'] != NULL)
				{
					switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'])
					{
						case 1 : echo "Jefe Oficina Servicio Social dice:"; break; //En esta parte no se utiliza 
						case 2 : echo "Supervisor ".$nombre_supervisor." dice:"; break;
						case 3 : echo "Alumno (a) ".$servicioSocialAlumno[0]['name_alumno']." dice:"; break;
					}
				}else
				if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'] != NULL)
				{
					switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'])
					{
						case 1 : echo "Jefe Oficina Servicio Social dice:"; break; //En esta parte no se utiliza 
						case 2 : echo "Supervisor ".$nombre_supervisor." dice:"; break;
						case 3 : echo "Alumno (a) ".$servicioSocialAlumno[0]['name_alumno']." dice:"; break;
					}
				}
					
				?></b>
				<br><br>
				<div class="row">
					<div align="center" class="col-xs-2">
							<?php 
							if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'] != NULL)
							{
								switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_emisor'])
								{
									case 1 : 
									//Foto del Admin
										echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/jefe_ssocial_512.png"/>'; 
										break;
									//Foto del Admin
									case 2 :
									//Foto del Supervisor
										echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$rfcSupervisor.'/'.$foto_emisor.'"/>'; 
										break;
									//Foto del Supervisor
									?>
									<?php case 3 :?>
										<!--Foto del Alumno-->
										<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $no_ctrl; ?>" alt="foto alumno" height="120">
										<!--Foto del Alumno-->
									<?php break; 
								}
							}else
							if($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'] != NULL)
							{
								switch($modelSSObservacionesServicioSocial[$i]['tipo_observacion_receptor'])
								{
									case 1: 
									//Foto del Admin
										echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/jefe_ssocial_512.png"/>'; 
										break;
									//Foto del Admin
									case 2:
									//Foto del Supervisor
										echo '<img class="img-circle" width="120" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/supervisores/'.$rfcSupervisor.'/'.$foto_emisor.'"/>'; 
										break;
									//Foto del Supervisor
									?>
									<?php case 3:?>
										<!--Foto del Alumno-->
										<!--<img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php //echo $no_ctrl; ?>" alt="foto alumno" height="120">-->
										<!--Foto del Alumno-->
									<?php break; 
								}
							}
							
							?>
					</div>
					<div class="col-xs-10">
							<div align="center" class="jumbotron">
								<p><?php
									echo $modelSSObservacionesServicioSocial[$i]['observacion']; 
								?></p>
							</div>
					</div>
				</div>
				<!--Fecha de visto de la observacion-->
				<div align="center">
						<?php 
						require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
						$fecha_vist = GetFormatoFecha::getFechaVistaObservacionServicioSocial($modelSSObservacionesServicioSocial[$i]['id_observacion']);
						echo ($fecha_vist[0]['fecha_act'] != "Sin Ver") ? '<b><span style="font-size:16px" class="label label-success">'."Visto el ".$fecha_vist[0]['fecha_act'].' a las '.$fecha_vist[0]['hora'].'</span></b>' : '<b><span style="font-size:16px" class="label label-default">...</span></b>';
						?>
				</div>
				<hr>
				<br>
				<!--Fecha de visto de la observacion-->
			<?php }?>
			<?php //} ?>
			<!--CAJON DE MENSAJES U OBSERVACIONES-->
			<div class="div1">
				<div class="form">
					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'observ-depto-servicio-social-form',
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// There is a call to performAjaxValidation() commented in generated controller code.
						// See class documentation of CActiveForm for details on this.
						'enableAjaxValidation'=>false,
						'htmlOptions' => array('autocomplete'=>'off')
					)); ?>

					<?php echo $form->errorSummary($ObservacionesServicioSocial); ?>

					<div class="form-group">
						<p class="center"><b>Escribir Observación:</b></p>
						<?php echo $form->textField($ObservacionesServicioSocial,'observacion',array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required')); ?>
						<?php echo $form->error($ObservacionesServicioSocial,'observacion'); ?>
					</div>

					<div align="center" class="form-group">
						<?php echo CHtml::submitButton($ObservacionesServicioSocial->isNewRecord ? 'Enviar Observación' : 'Enviar Observación', array('class'=>'btn btn-primary')); ?>
						<?php echo CHtml::link('Cancelar', array('ssServicioSocial/listaDeptoObservacionesServicioSocial'), array('class'=>'btn btn-danger')); ?>
					</div>

				<?php $this->endWidget(); ?>

				</div><!-- form -->
			</div>
			<!--CAJON DE MENSAJES U OBSERVACIONES-->
		    </div>	
		</div>
	</div>
</div>
<br><br>

