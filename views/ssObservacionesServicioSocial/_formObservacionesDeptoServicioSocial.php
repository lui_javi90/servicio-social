<?php
/* @var $this ObservacionesServicioSocialController */
/* @var $model ObservacionesServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'observ-depto-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($modelSSObservacionesServicioSocial); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSObservacionesServicioSocial,'observacion'); ?>
		<?php echo $form->textField($modelSSObservacionesServicioSocial,'observacion',array('size'=>60,'maxlength'=>500, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSObservacionesServicioSocial,'observacion'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSObservacionesServicioSocial->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('ssServicioSocial/listaDeptoObservacionesServicioSocial'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->