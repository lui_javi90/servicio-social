<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Reportes Bimestrales' => array('ssReportesBimestral/listaAdminReportesBimestrales'),
	'Evaluación Oficina Servicio Social Reporte Bimestral' => array('EvaluacionDeptoReporteBimestral', 'id_reporte_bimestral'=>$id_reporte_bimestral),
	'Editar Evaluación Oficina Servicio Social Reporte Bimestral'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Evaluación Oficina Servicio Social Reporte Bimestral
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Editar Evaluación Oficina Servicio Social</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formAdminCalificacionB', array(
							'modelSSEvaluacionBimestral'=>$modelSSEvaluacionBimestral,
							'id_reporte_bimestral' => $modelSSEvaluacionBimestral->id_reporte_bimestral,
							'modelSSCriteriosAEvaluar' => $modelSSCriteriosAEvaluar
						)); ?>
			</div>
		</div>
	</div>
</div>
