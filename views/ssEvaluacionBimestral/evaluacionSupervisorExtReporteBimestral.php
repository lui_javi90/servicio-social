<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Reportes Bimestrales Servicio Social Externo' => array('ssReportesBimestral/listaAdminReportesBimestralesExterno'),
    'Evaluación Supervisor Reporte Bimestral'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Reporte Bimestral Servicio Social Externo
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información Alumno Reporte Bimestral
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3">
					<!--Es un Web Service-->
					<img class="img-circle" height="200" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno">
                </div>
                <div class="col-xs-9" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['name_alumno']; ?></p>

					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['carrera']; ?></p>

					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['id_tipo_programa'] == 1) ? "INTERNO" : "EXTERNO"; ?></p>

                    <p><b>Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['bimestre_final'] == true) ? '<span style="font-size:18px" class="label label-success">SI</span>' : "NO" ?></p>

                    <p><b>Reporte Bimestral Correspondiente:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['bimestres_correspondiente']; ?></p>

                    <!--<p><b>Calificación Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;--><?php /*echo ($servicioSocialAlumno[0]['calificacion_reporte_bimestral'] >=70) ?
                                '<span style="font-size:18px" class="label label-success">'.$servicioSocialAlumno[0]['calificacion_reporte_bimestral']."</span></b>" :
                                '<span style="font-size:18px" class="label label-danger">'.$servicioSocialAlumno[0]['calificacion_reporte_bimestral']."</span></b>";*/
                                ?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Evaluación Supervisor Reporte Bimestral Servicio Social Externo
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        La siguiente información son los criterios de evaluación que le corresponden al supervisor del programa, debera evaluar al alumno teniendo:<br><br>
        * El Valor A, que es la puntación maxima que le puede dar al alumno en ese criterio.<br>
        * Evaluación B es la calificación que asiganara para ese criterio, si fuera el caso podrá editar para cambiar la calificación del alumno en ese criterio.<br>
        * Si en Evaluación B asigan un valor que supere el valor maximo para ese criterio contenido en Valor A, retornara un error.
    </strong></p>
</div>


      <?php $this->widget('zii.widgets.grid.CGridView', array(
          'id'=>'sup-ext-evaluacion-bimestral-grid',
          'dataProvider'=>$modelSSEvaluacionBimestral->searchXEvaluacionBimestral($id_reporte_bimestral, $id_tipo),
          'filter'=>$modelSSEvaluacionBimestral,
          'columns'=>array(
              //'id_evaluacion_bimestral', //de prueba, se quitara
              //'id_reporte_bimestral', // de prueba, se quitara
              /*array(
                  'name' => 'idCriterio.id_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                  'filter' => false,
                  'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
              ),*/
              array(
                  'name' => 'idCriterio.posicion_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                  'filter' => false,
                  'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
              ),
              array(
                  'name' => 'idCriterio.descripcion_criterio',//id_criterio
                  'filter' => false,
                  'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
              ),
              array(
                  'header' => 'Valor del Criterio <br>(A)',
                  'filter' => false,
                  'type' => 'raw',
                  'value' => function($data)
                  {
                      return '<span style="font-size:18px" class="label label-info">'.$data->idCriterio->valor_a.'</span>';
                  },
                  'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
              ),
              array(
                'class' => 'ComponentSupEAsignarCalificacionCriterio'
              ),
              /*array(
                  'header' => 'Evaluación B',
                  'filter' => false,
                  'type' => 'raw',
                  'value' => function($data)
                  {
                      return '<span style="font-size:18px" class="label label-success">'.$data->evaluacion_b.'</span>';
                  },
                  'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
              ),*/
              /*array(
                  'name'=>'evaluacion_b', //asistencia
                  'filter' => false,
                  'type'=>'raw',
                  'value' => function($data, $modelSSEvaluacionBimestral)
                  {
                      $model = SsCriteriosAEvaluar::model()->findByPK($data->id_criterio);
                      //print_r($model);
                      //die();
                      $valores = array();
                      $length = (int)$model->valor_a;
                      for($i = 0 ; $i <= $length; $i++)
                      {
                          $valores [$i] = $i;
                      }

                      return CHtml::dropDownList($modelSSEvaluacionBimestral, 'evaluacion_b', $valores, array('class'=>'form-control'));
                  },
                  //'value'=>'CHtml::dropDownList("id_criterio[SsEvaluacionBimestral::model()->id_criterio]", SsEvaluacionBimestral::model()->id_criterio, array("p"=>"Presente","a"=>"Ausente","r"=>"Retardo"),array("class"=>"form-control"))',
                  'htmlOptions'=>array('width'=>'10px','class'=>'text-center'),
              ),*/
              /*array(
                  'class'=>'CButtonColumn',
                  'template'=>'{editCalificacionCriterio}',
                  'header'=>'Editar Calificación',
                  'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
                  'buttons'=>array
                  (
                      'editCalificacionCriterio' => array
                      (
                          'label'=>'Editar Calificación Criterio',
                          'url'=>'Yii::app()->createUrl("serviciosocial/ssEvaluacionBimestral/editarCalificacionCriterioSupervisor",
                          array("id_evaluacion_bimestral"=>$data->id_evaluacion_bimestral))',
                          'imageUrl'=>'images/servicio_social/editar_32.png'
                      ),
                  ),
              ),*/
          ),
      )); ?>

      <br><br>
      <div align="center" class="">
        <?php echo CHtml::link('Volver a Lista Reportes Bimestrales', array('ssReportesBimestral/listaAdminReportesBimestralesExterno'), array('class'=>'btn btn-default')); ?>
      </div>
      <br><br>
