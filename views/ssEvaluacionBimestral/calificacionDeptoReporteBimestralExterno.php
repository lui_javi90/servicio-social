<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Reportes Bimestrales Servicio Social Externo' => array('ssReportesBimestral/listaAdminReportesBimestralesExterno'),
    'Calificación Reporte Bimestral Externo'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Reporte Bimestral Externo
		</span>
	</h2>
</div>

<br><br>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Reporte Bimestral Escaneado
                </h6>
            </div>
            <div class="panel panel-body" align="center">
                <p align="center"><strong>Reporte Bimestral</strong></p>
                <br><br>
                <?php
                    echo '<img align="center" heigth="650" width="450" src="'. Yii::app()->request->baseUrl.'/reportesexternos/'.$modelSSServicioSocial->no_ctrl.'/'.$modelSSExpedientesReportesBimestralesServicioSocialExterno->url_documento.'"/>';
                ?>
            </div>
            <br><br><br><br>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Calificación Reporte Bimestral
                </h6>
            </div>
            <div class="panel panel-body">
                <?php $this->renderPartial('_formCalificacionReporteBimestralExterno', array('modelSSReportesBimestral'=>$modelSSReportesBimestral
                                                                                        )); ?>
            </div>
        </div>
    </div>
</div>
