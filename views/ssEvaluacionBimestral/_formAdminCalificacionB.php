<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'depto-evaluacion-bimestral-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions'=>array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSEvaluacionBimestral); ?>

	<br>
	<div class="form-group">
		<?php echo "<b>Descripción:</b>"." ".$modelSSCriteriosAEvaluar->descripcion_criterio; ?>
		<?php echo "<br>" ?>
		<?php echo "<b>Valor maximo asignado:</b>"." ".'<span style="font-size:16px" class="label label-info">'.$modelSSCriteriosAEvaluar->valor_a.'</span>'; ?>
	</div>

	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelSSEvaluacionBimestral,'id_criterio'); ?>
		<?php //echo $form->textField($modelSSEvaluacionBimestral,'id_criterio'); ?>
		<?php //echo $form->error($modelSSEvaluacionBimestral,'id_criterio'); ?>
	</div>-->

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'evaluacion_b'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'evaluacion_b',array('size'=>2,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'evaluacion_b'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSEvaluacionBimestral->isNewRecord ? 'Guardar Calificación' : 'Actualizar Calificación', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('evaluacionDeptoReporteBimestral','id_reporte_bimestral'=>$id_reporte_bimestral), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->