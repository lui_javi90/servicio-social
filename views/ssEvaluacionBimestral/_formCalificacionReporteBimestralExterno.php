<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */
/* @var $form CActiveForm */
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'reportes-bimestral-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'htmlOptions' => array('autocomplete'=>'off'),
    'enableAjaxValidation'=>false,
)); ?>

    <!--<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>-->

    <?php echo $form->errorSummary($modelSSReportesBimestral); ?>

    <div class="form-group">
        <center><?php echo "<b>No. de Reporte Bimestral: </b>".$modelSSReportesBimestral->bimestres_correspondiente; ?></center>
        <?php echo "<br>"; ?>
        <?php echo "<b>Calificación Reporte Bimestral</b>"; ?>
        <?php echo $form->textField($modelSSReportesBimestral,'calificacion_reporte_bimestral',array('size'=>3,'maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
        <?php echo $form->error($modelSSReportesBimestral,'calificacion_reporte_bimestral'); ?>
    </div>

    <div class="form-group">
        <?php echo CHtml::submitButton($modelSSReportesBimestral->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('ssReportesBimestral/listaAdminReportesBimestralesExterno'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
