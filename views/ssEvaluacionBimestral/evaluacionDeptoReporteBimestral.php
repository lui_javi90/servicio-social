<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Reportes Bimestrales' => array('ssReportesBimestral/listaAdminReportesBimestrales'),
    'Evaluación Oficina Servicio Social Reporte Bimestral'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Reporte Bimestral
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información del Alumno
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3">
					<!--Es un Web Service-->
					<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno" height="200">
                </div>
                <div class="col-xs-5" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['name_alumno']; ?></p>

					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['carrera']; ?></p>

					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['id_tipo_programa'] == 1) ? "INTERNO" : "EXTERNO"; ?></p>

                    <p><b>Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['bimestre_final'] == true) ? "SI" : "NO" ?></p>

                    <p><b>Reporte Bimestral Correspondiente:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['bimestres_correspondiente']; ?></p>

                    <!--<p><b>Calificación Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;--><?php /*echo ($servicioSocialAlumno[0]['calificacion_reporte_bimestral'] >=70) ?
                                '<span style="font-size:18px" class="label label-success">'.$servicioSocialAlumno[0]['calificacion_reporte_bimestral'].'</span></b>' :
                                '<span style="font-size:18px" class="label label-danger">'.$servicioSocialAlumno[0]['calificacion_reporte_bimestral'].'</span></b>';*/ ?>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Evaluación Oficina Servicio Social Reporte Bimestral
		</span>
	</h2>
</div>

<br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        La siguiente información son los criterios de evaluación que le corresponden al jefe de oficina de Servicio Social, debera evaluar al alumno teniendo:<br><br>
        * El Valor A, que es la puntación maxima que le puede dar al alumno en ese criterio.<br>
        * Evaluación B es la calificación que asiganara para ese criterio, si fuera el caso debera editar para cambiar la calificación del alumno en ese criterio.<br>
        * Si en Evaluación B asigan un valor que supere el valor maximo para ese criterio contenido en Valor A, retornara un error.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'dep-evaluacion-bimestral-grid',
    'dataProvider'=>$modelSSEvaluacionBimestral->searchXEvaluacionBimestral($id_reporte_bimestral, $id_tipo),
    'filter'=>$modelSSEvaluacionBimestral,
    'columns'=>array(
        //'id_evaluacion_bimestral', //de prueba, se quitara
        //'id_reporte_bimestral', // de prueba, se quitara
        /*array(
            'name' => 'idCriterio.id_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),*/
        array(
            'name' => 'idCriterio.posicion_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idCriterio.descripcion_criterio',//id_criterio
            'filter' => false,
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
        ),
        array(
            'header' => 'Valor del Criterio (A)',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-info">'.$data->idCriterio->valor_a.'</span>';
			},
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
      ),
      array(
        'class' => 'ComponentDeptoIAsignarCalificacionCriterio'
      ),
      /*array(
            'header' => 'Evaluación B',
            'filter' => false,
            'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->evaluacion_b.'</span>';
			},
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
      ),*/
      /*array(
			'class'=>'CButtonColumn',
			'template'=>'{editCalificacionCriterio}',
			'header'=>'Editar Calificación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editCalificacionCriterio' => array
				(
					'label'=>'Editar Calificación Criterio',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssEvaluacionBimestral/editarCalificacionCriterioDepto",
					array("id_evaluacion_bimestral"=>$data->id_evaluacion_bimestral))',
					'imageUrl'=>'images/servicio_social/editar_32.png'
				),
			),
    ),*/
    ),
)); ?>

<br><br>
<div align="center" class="">
    <?php echo CHtml::link('Volver a la lista Reportes Bimestrales', array('ssReportesBimestral/listaAdminReportesBimestrales'), array('class'=>'btn btn-default')); ?>
</div>
<br><br>
