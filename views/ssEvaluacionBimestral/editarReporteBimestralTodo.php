<?php
    $this->breadcrumbs=array(
        'Servicio Social'=>'?r=serviciosocial',
        'Editar Reportes Bimestrales' => array('ssReportesBimestral/editarCalificacionesCriterios'),
        'Detalle Reporte Bimestral'
    ); 
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Detalle Reporte Bimestral
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información Alumno Reporte Bimestral
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3">
					<!--Es un Web Service-->
					<img class="img-circle" height="200" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno">
                </div>
                <div class="col-xs-9" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['name_alumno']; ?></p>

					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['carrera']; ?></p>

					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['id_tipo_programa'] == 1) ? "INTERNO" : "EXTERNO"; ?></p>

                    <p><b>Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['bimestre_final'] == true) ? '<span style="font-size:18px" class="label label-success">SI</span>' : "NO" ?></p>

                    <p><b>Reporte Bimestral Correspondiente:</b>
                    &nbsp;&nbsp;<?php echo '<span style="font-size:18px" class="label label-success">'.$servicioSocialAlumno[0]['bimestres_correspondiente'].'</span>'; ?></p>

                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <h2 class="subTitulo" align="center">
        <span class="subTitulo_inside">
            Evaluación Reporte Bimestral
        </span>
    </h2>
</div>

<?php if($modelSSReportesBimestral->valida_responsable != NULL AND $modelSSReportesBimestral->valida_oficina_servicio_social == NULL){ ?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Evaluación Supervisor Reporte Bimestral
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'sup-evaluacion-bimestral-grid',
            'dataProvider'=>$modelSSEvaluacionBimestral->searchXEvaluacionBimestral($id_reporte_bimestral, 1),
            'filter'=>$modelSSEvaluacionBimestral,
            'columns'=>array(
                //'id_evaluacion_bimestral', //de prueba, se quitara
                //'id_reporte_bimestral', // de prueba, se quitara
                /*array(
                    'name' => 'idCriterio.id_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),*/
                array(
                    'name' => 'idCriterio.posicion_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
                ),
                array(
                    'name' => 'idCriterio.descripcion_criterio',//id_criterio
                    'filter' => false,
                    'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
                ),
                array(
                    'header' => 'Valor del Criterio <br>(A)<br>',
                    'filter' => false,
                    'type' => 'raw',
                    'value' => function($data)
                    {
                        return '<span style="font-size:18px" class="label label-info">'.$data->idCriterio->valor_a.'</span>';
                    },
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),
                array(
                    //'class' => 'ComponentDeptoIAsignarCalificacionCriterio' //Para asignar calificacion al criterio
                    'class' => 'ComponentSupIAsignarCalificacionCriterio' //Para asignar calificacion al criterio
                ),
            ),
        )); ?>

<?php }elseif($modelSSReportesBimestral->valida_oficina_servicio_social != NULL AND $modelSSReportesBimestral->valida_responsable == NULL){ ?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Evaluación Jefe Oficina Reporte Bimestral
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'sup-evaluacion-bimestral-grid',
            'dataProvider'=>$modelSSEvaluacionBimestral->searchXEvaluacionBimestral($id_reporte_bimestral, 2),
            'filter'=>$modelSSEvaluacionBimestral,
            'columns'=>array(
                //'id_evaluacion_bimestral', //de prueba, se quitara
                //'id_reporte_bimestral', // de prueba, se quitara
                /*array(
                    'name' => 'idCriterio.id_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),*/
                array(
                    'name' => 'idCriterio.posicion_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
                ),
                array(
                    'name' => 'idCriterio.descripcion_criterio',//id_criterio
                    'filter' => false,
                    'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
                ),
                array(
                    'header' => 'Valor del Criterio <br>(A)<br>',
                    'filter' => false,
                    'type' => 'raw',
                    'value' => function($data)
                    {
                        return '<span style="font-size:18px" class="label label-info">'.$data->idCriterio->valor_a.'</span>';
                    },
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),
                array(
                    'class' => 'ComponentSupIAsignarCalificacionCriterio' //Para asignar calificacion al criterio
                ),
            ),
        )); ?>

<?php }elseif($modelSSReportesBimestral->valida_oficina_servicio_social != NULL AND $modelSSReportesBimestral->valida_responsable != NULL){ ?>
    <div class="row">
        <h2 class="subTitulo" align="center">
            <span class="subTitulo_inside">
                Evaluación Supervisor Reporte Bimestral
            </span>
        </h2>
    </div>

    <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'sup-evaluacion-bimestral-grid',
            'dataProvider'=>$modelSSEvaluacionBimestral->searchXEvaluacionBimestral($id_reporte_bimestral, 1),
            'filter'=>$modelSSEvaluacionBimestral,
            'columns'=>array(
                //'id_evaluacion_bimestral', //de prueba, se quitara
                //'id_reporte_bimestral', // de prueba, se quitara
                /*array(
                    'name' => 'idCriterio.id_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),*/
                array(
                    'name' => 'idCriterio.posicion_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
                ),
                array(
                    'name' => 'idCriterio.descripcion_criterio',//id_criterio
                    'filter' => false,
                    'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
                ),
                array(
                    'header' => 'Valor del Criterio <br>(A)<br>',
                    'filter' => false,
                    'type' => 'raw',
                    'value' => function($data)
                    {
                        return '<span style="font-size:18px" class="label label-info">'.$data->idCriterio->valor_a.'</span>';
                    },
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),
                array(
                    'class' => 'ComponentSupIAsignarCalificacionCriterio' //Para asignar calificacion al criterio
                ),
            ),
        )); ?>

        <br><br>
        <div class="row">
            <h2 class="subTitulo" align="center">
                <span class="subTitulo_inside">
                    Evaluación Jefe Oficina Reporte Bimestral
                </span>
            </h2>
        </div>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'sup-evaluacion-bimestral-grid',
            'dataProvider'=>$modelSSEvaluacionBimestral->searchXEvaluacionBimestral($id_reporte_bimestral, 2),
            'filter'=>$modelSSEvaluacionBimestral,
            'columns'=>array(
                //'id_evaluacion_bimestral', //de prueba, se quitara
                //'id_reporte_bimestral', // de prueba, se quitara
                /*array(
                    'name' => 'idCriterio.id_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),*/
                array(
                    'name' => 'idCriterio.posicion_criterio', //Enumera los criterios de la tabla criterios_a_evaluar
                    'filter' => false,
                    'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
                ),
                array(
                    'name' => 'idCriterio.descripcion_criterio',//id_criterio
                    'filter' => false,
                    'htmlOptions' => array('width'=>'300px', 'class'=>'text-left')
                ),
                array(
                    'header' => 'Valor del Criterio <br>(A)<br>',
                    'filter' => false,
                    'type' => 'raw',
                    'value' => function($data)
                    {
                        return '<span style="font-size:18px" class="label label-info">'.$data->idCriterio->valor_a.'</span>';
                    },
                    'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
                ),
                array(
                    'class' => 'ComponentSupIAsignarCalificacionCriterio' //Para asignar calificacion al criterio
                ),
            ),
        )); ?>

<?php }else{ ?>

    <br><br><br><br><br>
    <div class="alert alert-danger">
        <p><strong>
            <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
            El Reporte no ha sido evaluado ni por el Responsable ni por la Oficina de Servicio Social.
        </strong></p>
    </div>
    <br><br><br><br><br>

<?php } ?>

    <br><br><br>

	  <div align="center" class="">
	    <?php echo CHtml::link('Volver a Lista Editar Reportes Bimestrales', array('ssReportesBimestral/editarCalificacionesCriterios'), array('class'=>'btn btn-default')); ?>
    </div>

<br><br><br>