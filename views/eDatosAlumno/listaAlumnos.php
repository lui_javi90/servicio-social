<?php
/* @var $this AlumnosVigentesInscritosCreditosController */
/* @var $model AlumnosVigentesInscritosCreditos */

$this->breadcrumbs=array(
	'Inicio'=>array('serviciosocial/'),
	'Alumnos Candidatos Servicio Social',
);


//PARA ACTIVAR EL SERVICIO SOCIAL DEL ALUMNO
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#edatos-cand-alumno-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#edatos-cand-alumno-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
//PARA ACTIVAR EL SERVICIO SOCIAL DEL ALUMNO

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Alta Alumnos Servicio Social
		</span>
	</h2>
</div>

<br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Filtrar por Estatus de Servicio Social
                    </h3>
                </div>
                <div class="panel-body">
					<!--index.php hace referencia al mismo archivo donde se esta y get es el metodo de envio de datos-->
                    <?php echo CHtml::beginForm("index.php", "get"); ?>

                    <input type="hidden" name='r' value="serviciosocial/eDatosAlumno/listaAlumnos">

                    <b>Estatus Servicio Social:</b>
                    <div class="form-group">
										<?php echo CHtml::dropDownList('val_serv_social',
														'',
														array('0'=>'Desactivado','1'=>'Activado'),
														array('prompt'=>'--Seleccione el estatus--','class'=>'form-control')
													);?>
                    </div>

                    <br>
                    <div class="form-group">
                        <?php echo CHtml::submitButton("Buscar por estatus servicio social", array("class"=>"btn btn-primary")); ?>
                        <?php echo CHtml::link('Sacame de aquí', array('/serviciosocial'), array('class'=>'btn btn-danger')); ?>
                    </div>

                    <?php echo CHtml::endForm(); ?>
                </div>
            </div>
        </div>
    </div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Alumnos Vigentes. Puede haber casos donde al alumno le falten menos de 5 creditos para tomar el Servicio Social, se le podra ACTIVAR en caso de hacer alguna excepción.
    </strong></p>
</div>
<div class="alert alert-success">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        <?php echo $creditosMinimos; ?> son los Creditos Mínimos para realizar el Servicio Social.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'edatos-cand-alumno-grid',
	'dataProvider'=>$modelEDatosAlumnos->searchXStatusServicioSocial($val_serv_social),
	'filter'=>$modelEDatosAlumnos,
	'columns'=>array(
        array(
            'name' => 'nctrAlumno',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
		/*nmbAlumno1',
		'tipoAlumno',
		'passAlumnoEnc',*/
		//'insAlumno',
        //'statAlumno',
        array(
            'header' => 'Nombre (s)',
            'name' => 'nmbSoloAlumno',
            'htmlOptions' => array('width'=>'180px', 'class'=>'text-center'),
        ),
        array(
            'header' => 'Apellido Paterno',
            'name' => 'apellPaternoAlu',
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center'),
        ),
        array(
            'header' => 'Apellido Materno',
            'name' => 'apellMaternoAlu',
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center'),
        ),
		/*'passAlumno',
		'cveModuloAlu',
		'apellCompletoAlu',
		'perIngresoAlu',
        'anioIngresoAlu',*/
        array(
            'header' => 'Especilidad',
            'name' => 'cveEspecialidadAlu0.dscEspecialidad',
            'filter' => CHtml::activeDropDownList($modelEDatosAlumnos,
                                                'cveEspecialidadAlu',
                                                $lista_carreras,
                                                array('prompt'=>'-- Selecciona una carrera --')
                                                ),
            'htmlOptions' => array('width'=>'280', 'class'=>'text-center')
        ),
        array(
            'header' => 'Semestre',
			'name' => 'semAlumno',
			'filter' => false,
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Creditos Acumulados',
			'name' => 'crdAcumulaAlu',
			'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center'),
		),
		array(
			'class' => 'ComponentEstatusServicioSocial',
			'header' => 'Estatus Servicio Social',
			'htmlOptions' => array('width' => '150px', 'class' => 'text-center'),
		),
		/*'crdSemestreAlu',
		'sexoAlumno',
		'anioTerminacion',
		'cvePlanAlu',
		'fchActualizacionDP',
		'examenMedico',
		'nmbAlumno',
		'promAnioAnterior',
		'anio_egreso',
		'ficha',
		'edoRespAlumno',
		'id_ubicacionTec',
		*/
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{activado}, {noactivado}', // buttons here...
			'header' 			   => 'Estatus Servicio Social',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'activado'   => [
					'label'   => 'Desactivar Servicio Social',
					//'url'     => 'Yii::app()->createUrl("serviciosocial/eDatosAlumno/statusServicioSocial", array("no_ctrl"=>$data->nctrAlumno, "oper" => 0))',
					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
                    'visible'=> function($index, $data)
                    {
                        $query = "select * from pe_planeacion.ss_status_servicio_social where no_ctrl = '$data->nctrAlumno' ";
                        $result = Yii::app()->db->createCommand($query)->queryAll();

                        return ($result[0]['val_serv_social'] == true) ? true : '';
                    },
					/*'options' => [
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar Baja Servicio Social?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
					*/
				],
				'noactivado' => [
					'label'   => 'Activar Servicio Social',
					'url'     => 'Yii::app()->createUrl("serviciosocial/eDatosAlumno/statusServicioSocial", array("no_ctrl"=>$data->nctrAlumno, "oper" => 1))', //
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible'=> function($index, $data)
                    {
                        $query = "select * from pe_planeacion.ss_status_servicio_social where no_ctrl = '$data->nctrAlumno' ";
                        $result = Yii::app()->db->createCommand($query)->queryAll();

                        return ($result[0]['val_serv_social'] == false) ? true : '';
                    },
					'options' => [
						//'title'        => 'Desactivado',
						'data-confirm' => 'Confirmar Alta Servicio Social?', // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],//Fin
	),
)); ?>