<?php
/* @var $this EDatosAlumnoController */
/* @var $model EDatosAlumno */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Pre-registro a Servicio Social',
);

?>


<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Pre-registro a Servicio Social
		</span>
	</h2>
</div>

<?php if($estaInscritoYVigente == true){ ?>
        <?php if($puedeTomarServicioSocialC == true or $puedeTomarServicioSocialA == true){ ?>
        <br><br><br><br>
        <div class="alert alert-info">
            <p><strong>
                <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                <b>Si tus datos no aparecen correctamente acude al Departamento de Servicios Escolares para corregir tu información.</b>
            </strong></p>
        </div>

        <br>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            Pre-registro
                        </h6>
                    </div>
                    <div class="panel-body">
                        <br>
                        <?php if($status == true) {?>
                            <div class="alert alert-success">
                                <p><strong>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                                    <b>Ya has realizado el Pre-registro correctamente.</b>
                                </strong></p>
                            </div>
                            
                            <div class="alert alert-info">
                                <p>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                                    Fecha realización del Pre-registro:&nbsp;&nbsp;<b><?php echo ($fec_preregistro[0]['fecha_act'] != null) ? $fec_preregistro[0]['fecha_act'].' a las '.$fec_preregistro[0]['hora'] : "Desconocida"; ?></b>
                                </p>
                            </div>
                        <?php }else{ ?>
                            <div class="alert alert-danger">
                                <p>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                                    <b>Es necesario realizar el Pre-registro para dar de ALTA tu Servicio Social.</b>
                                </p>
                            </div>
                        <?php } ?>
                        
                        <?php $this->renderPartial('_formPreRegistroAlumnoServSocial', array(
                                                                                            'modelEDatosAlumno'=>$modelEDatosAlumno,
                                                                                            'no_control' => $no_control,
                                                                                            'carrera' => $carrera,
                                                                                            'status' => $status
                                                                )); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php }else{ ?>
            <br><br><br><br>
            <div class="alert alert-danger">
                <p><strong>
                    <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                    <b>Aún no completas los creditos suficientes para llevar Servicio Social.</b>
                </strong></p>
            </div>

            <br><br><br>
            <div align="center">
                <?php echo '<img align="center" heigth="100" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/sad_512.png"/>'; ?>

                <br><br><br><br><br>
                <?php echo CHtml::link('Volver al Menú Principal', array('/'), array('class'=>'btn btn-success right')); ?>
            </div>
        <?php }?>

<?php }else{ ?>

            <br><br><br><br>
            <div class="alert alert-danger">
                <p><strong>
                    <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
                    <b>Debes estar Inscrito (a) y Vigente en la Institución para realizar el Pre-registro de Servicio Social.</b>
                </strong></p>
            </div>

            <br><br><br>
            <div align="center">
                <?php echo '<img align="center" heigth="100" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/sad_512.png"/>'; ?>

                <br><br><br><br><br>
                <?php echo CHtml::link('Volver al Menú Principal', array('/'), array('class'=>'btn btn-success right')); ?>
            </div>

<?php } ?>
