<!--Principal (View)-->
    
	    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                    <div class="modal-header">
                            <h5 class="modal-title">LISTADO DE MATERIAS</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                <div class="modal-body">
                    <div class="container-fluid" id="modalContent">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>

        <?php
        Yii::app()->clientScript->registerScript('modales', "
        
        $('#sinProfes').click(function(){
            verModal(1);
        });

        $('#sinInstru').click(function(){
            verModal(2);
        });

        $('#sinCal').click(function(){
            verModal(3);
        });
        
        
        function verModal(opc){
            $('#myModal').modal('show');
            $.ajax({
                cache: false,
                type: 'POST',
                url: '".$this->createUrl('edatosAlumno/detalleAlumnos')."',
                data: {'opc': opc},
                success: function(data) 
                {
                    $('#modalContent').show().html(data);
                }
            });
        }
        ");
    ?>

<div class="detalle">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'sin-profesor-grid',
    'dataProvider' => $modelEDatosAlumnos->search(),
    //'enableAjaxValidation' => false,
    'columns' => array(
        array(
            'name'=>'no_ctrl',
            'header'=>'Numero de Control',
            'htmlOptions'=>array('width'=>"3%", 'class' => 'text-center'),
        ),
    ),
));
?>
</div>