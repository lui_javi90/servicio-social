<style>
.boton {
    display:none;
}
</style>
<?php
/* @var $this AlumnosVigentesInscritosCreditosController */
/* @var $model AlumnosVigentesInscritosCreditos */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Alumnos Candidatos Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Alta Alumno a Servicio Social
		</span>
	</h2>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Buscar Alumno por No. de Control<br>
                </h4>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this EDatosAlumnoController */
                /* @var $model EDatosAlumno */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'edatos-alumno-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('autocomplete'=>'off'),
                )); ?>

                    <p class="note">Campos con <span class="required">*</span> son requeridos.</p>

                    <?php echo $form->errorSummary($modelEDatosAlumno); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelEDatosAlumno,'nctrAlumno'); ?>
                        <?php echo $form->textField($modelEDatosAlumno,
                                                    'nctrAlumno',
                                                    //array('class'=>'form-control','size'=>9,'maxlength'=>9));
                                                    array(
                                                        'class'=>'form-control', 'size'=>9,'maxlength'=>8, 'required'=>'required',
                                                        'ajax'=>array(
                                                        'type'=>'POST',
                                                        'dataType'=>'json',
                                                        'data' => array(
            
                                                            'nctrAlumno'=>'js:$(\'#EDatosAlumno_nctrAlumno\').val()',
                                                        ),
                                                        'url'=>CController::createUrl('eDatosAlumno/infoAlumno'),
                                                        'success'=>'function(data) {
                                                            $("#EDatosAlumno_insAlumno").html(data.inscrito);
                                                            
                                                                if(data.no_c == "*")
                                                                {
                                                                    $(".div_foto_alumno").hide();
                                                                    $(".div_nombre").hide();
                                                                    $(".div_carrera").hide();
                                                                    $(".div_inscrito").hide();
                                                                    $(".div_status").hide();
                                                                    $(".div_semestre").hide();
                                                                    $(".div_creditos_acum").hide();
                                                                    $(".div_sexo").hide();
                                                                    $(".boton").hide();
                                                                    $(".div_vis_cred").hide();
                                                                    $(".div_msg_error").show();
                                                                    $(".div_msg_error").html(\'<img align="center" heigth="150" width="200" src="images/servicio_social/\'+ data.msg_err +\' " />\');
                                                                    
                                                                }else if(data.no_c == "+"){

                                                                    $(".div_foto_alumno").hide();
                                                                    $(".div_nombre").hide();
                                                                    $(".div_carrera").hide();
                                                                    $(".div_inscrito").hide();
                                                                    $(".div_status").hide();
                                                                    $(".div_semestre").hide();
                                                                    $(".div_creditos_acum").hide();
                                                                    $(".div_sexo").hide();
                                                                    $(".boton").hide();
                                                                    $(".div_msg_error").hide();
                                                                    $(".div_vis_cred").hide();
                                                                    
                                                                }else{
                                                                    $(".div_foto_alumno").show();
                                                                    $(".div_nombre").show();
                                                                    $(".div_carrera").show();
                                                                    $(".div_inscrito").show();
                                                                    $(".div_status").show();
                                                                    $(".div_semestre").show();
                                                                    $(".div_creditos_acum").show();
                                                                    $(".div_sexo").show();
                                                                    $(".boton").show();
                                                                    $(".div_msg_error").hide();
                                                                    $(".div_foto_alumno").html(\'<img class="img-circle" align="center" heigth="150" width="200" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=\'+ data.no_c +\' " />\');
                                                                    $(".div_nombre").html(\'<b><label align="left">Nombre Completo:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.nombre_completo +\'" readonly />\');
                                                                    $(".div_carrera").html(\'<b align="left"><label>Carrera:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.carrera +\'" readonly />\');
                                                                    $(".div_inscrito").html(\'<b><label align="left">Esta Inscrito:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.inscrito +\'" readonly />\');
                                                                    $(".div_status").html(\'<b><label align="left">Estatus del Alumno:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.status +\'" readonly />\');
                                                                    $(".div_semestre").html(\'<b><label align="left">Semestre:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.semestre +\'" readonly />\');
                                                                    $(".div_vis_cred").show();
                                                                    if(data.val_creditos == true){
                                                                        $(".div_vis_cred").html(\'<img align="center" heigth="15" width="20" src="images/servicio_social/aceptar_32.png" />\');
                                                                    }else{
                                                                        $(".div_vis_cred").html(\'<img align="center" heigth="15" width="20" src="images/servicio_social/rechazar_32.png" />\');
                                                                    }
                                                                    $(".div_creditos_acum").html(\'<b><label align="left">Creditos Acumulados:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.creditos_acum +\'" readonly />\');
                                                                    $(".div_sexo").html(\'</b><label align="left">Sexo:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.sexo +\'" readonly />\');
                                                                }
                                                        }',
                                                        )) 
                                                    ); ?>
                        <?php echo $form->error($modelEDatosAlumno,'nctrAlumno'); ?>
                    </div>

                    <!--Foto-->
                    <hr>
                    <h4 align="center">Detalle de Información</h4>
                    <hr>

                    <div class="div_msg_error col-md-12 center" align="center">
                    </div>

                    <br>
                    <div class="div_foto_alumno col-md-12 center" align="center">
                    </div>

                    <br><br>

                    <!--Nombre Completo-->
                    <div class="div_nombre col-md-12 center" align="center">
                    </div>

                    <br><br>

                    <!--Carrera-->
                    <div class="div_carrera col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Inscrito-->
                    <div class="div_inscrito col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Estatus-->
                    <div class="div_status col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Semestre-->
                    <div class="div_semestre col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Creditos-->
                    <div align="center" class="div_vis_cred col-md-12">
                    </div>
                    <div class="div_creditos_acum col-md-12" align="center">
                    </div>

                    <br><br>

                    <!--Sexo-->
                    <div class="div_sexo col-md-12" align="center">
                    </div>

                    <br><br>
                    <div align="center" class="form-group">
                        <?php echo CHtml::submitButton('Dar ALTA Alumno', array('class'=>'boton btn btn-success')); ?>
                        <?php echo CHtml::link('Cancelar', array('/'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>  
        </div>
    </div>
</div>
