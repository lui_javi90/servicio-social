<?php
/* @var $this AlumnosVigentesInscritosCreditosController */
/* @var $model AlumnosVigentesInscritosCreditos */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Alta Alumnos Servicio Social',
);

//$js = ""

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Alta Alumnos Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Alumnos con Servicio Social ACTIVO.
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'alumnos-vigentes-inscritos-creditos-grid',
    'dataProvider'=>$modelEDatosAlumnos->searchAprobServSocial(),
    'filter'=>$modelEDatosAlumnos,
    'columns'=>array(
        array(
            'name' => 'nctrAlumno',
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Nombre (s)',
            'name' => 'nmbSoloAlumno',
            'htmlOptions' => array('width'=>'180px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Apellido Paterno',
            'name' => 'apellPaternoAlu',
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Apellido Materno',
            'name' => 'apellMaternoAlu',
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Semestre',
            'name' => 'semAlumno',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Especilidad',
            'name' => 'EEspecialidad.dscEspecialidad',
            'filter' => CHtml::activeDropDownList($modelEDatosAlumnos,
                                            'cveEspecialidadAlu',
                                            $lista_carreras,
                                            array('prompt'=>'-- Selecciona una carrera --')
                                            ),
            'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
        ),
        /*array(
            'header' => 'Horas Totales',
            'name' => 'noCtrl.horas_totales_servicio_social',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),*/
        array(
            'class'=>'CButtonColumn',
			'template'=>'{validoAlumnoServicioSocial}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons' => array
			(
				'validoAlumnoServicioSocial' => array
				(
					'label'=>'Servicio Social Activado',
                    'Yii::app()->createUrl("serviciosocial/eDatosAlumno/detalleAlumnos", array("no_ctrl"=>$data->nctrAlumno))',
                    'click' => "function verModal(opc){
                        $('#myModal').modal('show');
                        $.ajax({
                            cache: false,
                            type: 'POST',
                            url: '".$this->createUrl('edatosAlumno/detalleAlumnos')."',
                            data: {'opc': opc},
                            success: function(data) 
                            {
                                $('#modalContent').show().html(data);
                            }
                        })",
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			), 
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{validoAlumnoServicioSocial}',
			'header'=>'Estado',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons' => array
			(
				'validoAlumnoServicioSocial' => array
				(
					'label'=>'Servicio Social Activado',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/alta_servicio_social_32.png',
				),
			),
        ),
    ),
)); ?>