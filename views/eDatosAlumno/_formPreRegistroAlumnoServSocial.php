<?php
/* @var $this EDatosAlumnoController */
/* @var $model EDatosAlumno */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'edatos-prereg-alumno-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('autocomplete'=>'off')
)); ?>


    <?php if($status == false){?>
        <strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>
    <?php } ?>
    <br>

    <?php echo $form->errorSummary($modelEDatosAlumno); ?>

    <?php if($status == false){ ?>
    <div class="form-group">
        <p><strong>No. de Control Detectado </strong><span class="required">*</span><p>
        <?php echo $form->dropDownList($modelEDatosAlumno,
                                        'nctrAlumno',
                                        $no_control,
                                        array(
                                            'prompt'=>'-- No. de Control --', 'class'=>'form-control', 'required'=>'required',
                                            'ajax'=>array(
                                            'type'=>'POST',
                                            'dataType'=>'json',
                                            'data' => array(

                                                'nctrAlumno'=>'js:$(\'#EDatosAlumno_nctrAlumno option:selected\').val()',
                                            ),
                                            'url'=>CController::createUrl('eDatosAlumno/carreraAlumno'),
                                            'success'=>'function(data) {
                                                    $("#EDatosAlumno_cveEspecialidadAlu").html(data.carrera);
                                                    $("#EDatosAlumno_nmbAlumno").val(data.nombrecompleto);
                                                    $("#EDatosAlumno_semAlumno").val(data.semestre);
                                            }',
                                            ))
                                        ); ?>
        <?php echo $form->error($modelEDatosAlumno,'nctrAlumno'); ?>
    </div>
    <?php }else{ ?>
    <div class="form-group">
        <p><strong>No. de Control Detectado </strong><span class="required">*</span><p>
        <?php echo $form->dropDownList($modelEDatosAlumno,
                                        'nctrAlumno',
                                        $no_control,
                                        array('class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled'
                                        )); ?>
        <?php echo $form->error($modelEDatosAlumno,'nctrAlumno'); ?>
    </div>
    <?php } ?>

    <?php if($status == false){ ?>
    <div class="form-group">
        <p><strong>Especialidad </strong><span class="required">*</span><p>
        <?php echo $form->dropDownList($modelEDatosAlumno,
                                        'cveEspecialidadAlu',
                                        $carrera,
                                        array('prompt'=>'-- Selecciona tu Carrera --', 'class'=>'form-control', 'required'=>'required', /*'disabled'=>'disabled'*/
                                        )); ?>
        <?php echo $form->error($modelEDatosAlumno,'cveEspecialidadAlu'); ?>
    </div>
    <?php }else{?>
    <div class="form-group">
        <p><strong>Especialidad </strong><span class="required">*</span><p>
        <?php echo $form->dropDownList($modelEDatosAlumno,
                                        'cveEspecialidadAlu',
                                        $carrera,
                                        array('prompt'=>'-- Selecciona tu Carrera --', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled'
                                        )); ?>
        <?php echo $form->error($modelEDatosAlumno,'cveEspecialidadAlu'); ?>
    </div>
    <?php }?>

    <div class="form-group">
        <p><strong>Nombre Completo </strong><span class="required">*</span><p>
        <?php echo $form->textField($modelEDatosAlumno, 'nmbAlumno', array('class'=>'form-control', 'readOnly'=>true)); ?>
        <?php echo $form->error($modelEDatosAlumno,'nmbAlumno'); ?>
    </div>

    <div class="form-group">
        <p><strong>Semestre </strong><span class="required">*</span><p>
        <?php echo $form->textField($modelEDatosAlumno,'semAlumno', array('class'=>'form-control', 'readOnly'=>true)); ?>
        <?php echo $form->error($modelEDatosAlumno,'semAlumno'); ?>
    </div>

    <br>
    <?php if($status == false){ ?>
    <div class="form-group">
        <?php echo CHtml::submitButton($modelEDatosAlumno->isNewRecord ? 'Guardar Pre-Registro' : 'Guardar Pre-Registro', array('class'=>'btn btn-success')); ?>
        <?php echo CHtml::link('Cancelar', array('/'), array('class'=>'btn btn-danger')); ?>
    </div>
    <?php }else{ ?>
        <?php echo CHtml::link('Regresar al Ménu Principal', array('/'), array('class'=>'btn btn-danger right')); ?>
    <?php } ?>

<?php $this->endWidget(); ?>

</div><!-- form -->