<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supervisor-solicitud-programa-servicio-social-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSSolicitudProgramaServicioSocial); ?>

	<?php if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 1){ ?>
	<br>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSSolicitudProgramaServicioSocial,'id_estado_solicitud_programa_supervisor'); ?>
		<?php echo $form->dropDownList($modelSSSolicitudProgramaServicioSocial,
									'id_estado_solicitud_programa_supervisor',
									$list_edo_sol,
									array('prompt'=>'--Seleccione Estado Solicitud--', 'class'=>'form-control', 'required'=>'required')
									)?>
		<?php echo $form->error($modelSSSolicitudProgramaServicioSocial,'id_estado_solicitud_programa_supervisor'); ?>
	</div>
	<?php }else{ ?>
	<br>
	<div class="form-group">
		<?php echo $form->labelEx($modelSSSolicitudProgramaServicioSocial,'id_estado_solicitud_programa_supervisor'); ?>
		<?php echo $form->dropDownList($modelSSSolicitudProgramaServicioSocial,
									'id_estado_solicitud_programa_supervisor',
									$list_edo_sol,
									array('prompt'=>'--Seleccione Estado Solicitud--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
									)?>
		<?php echo $form->error($modelSSSolicitudProgramaServicioSocial,'id_estado_solicitud_programa_supervisor'); ?>
	</div>
	<?php } ?>

	<br>
	<div class="form-group buttons">
		<?php echo CHtml::submitButton($modelSSSolicitudProgramaServicioSocial->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaSolicitudSupervisorProgramaServicioSocial'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
