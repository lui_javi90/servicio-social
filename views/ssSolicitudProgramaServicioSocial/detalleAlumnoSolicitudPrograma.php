<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
    'Solicitudes al Programa' => array('ssSolicitudProgramaServicioSocial/listaSolicitudAlumnoProgramaServicioSocial'),
    'Detalle Solicitud'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Información de la Solicitud
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información Alumno Solicitante
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3 text-center">
					<!--Es un Web Service-->
					<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $modelEDatosAlumno->nctrAlumno; ?>" alt="foto alumno" height="200">
                </div>
                <div class="col-xs-9" align="left">
					<p><b><?php echo CHtml::encode($modelEDatosAlumno->getAttributeLabel('nmbAlumno')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelEDatosAlumno->nmbAlumno; ?></p>
					
					<p><b><?php echo CHtml::encode($modelEDatosAlumno->getAttributeLabel('nctrAlumno')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelEDatosAlumno->nctrAlumno; ?></p>

					<p><b>Especialidad:</b>
					&nbsp;&nbsp;<?php echo $carrera; ?></p>
					
					<p><b><?php echo CHtml::encode($modelEDatosAlumno->getAttributeLabel('semAlumno')); ?>:</b>
					&nbsp;&nbsp;<?php echo $modelEDatosAlumno->semAlumno; ?></p>
					
					<p><b>Estatus Servicio Social:</b>
					&nbsp;&nbsp;<?php echo ($modelSSStatusServicioSocial->val_serv_social == true) ? "ACTIVADO" : "DESACTIVADO"; ?></p>
					
					<p><b><?php echo CHtml::encode($modelSSSolicitudProgramaServicioSocial->getAttributeLabel('fecha_solicitud_programa')); ?>:</b>
					&nbsp;&nbsp;<?php echo $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'] ?></p>
				</div>
            </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Información del Programa Solicitado
		</span>
	</h2>
</div>

<br><br>
<?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$modelSSProgramas,
        'attributes'=>array(
            //'id_programa',
            'nombre_programa', 
            array(
                'header'=>'Empresa',
                'name' => 'idUnidadReceptora.nombre_unidad_receptora'
            ),
            'lugar_realizacion_programa',
            /*array(
                'name'=>'Departamento',
                'value' => function($data)
                {
                    $id = $data->rfcSupervisor0->cveDepartamento;
                    $query = "select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = '$id' ";
                    $nombre = Yii::app()->db->createCommand($query)->queryAll();
    
                    return $nombre[0]['dscDepartamento'];
                },
            ),*/
            'horas_totales',
            'idPeriodoPrograma.periodo_programa',//id_periodo_programa
            /*array(
                'name' => 'rfcSupervisor',
                'value' => function($data)
                {
                    $rfc_sup = $data->rfcSupervisor;
                    $query = 
                    "
                    SELECT (nombre_supervisor||' '|| apell_paterno || ' ' || apell_materno) as name
                    from pe_planeacion.ss_supervisores_programas 
                    where \"rfcSupervisor\" = '$rfc_sup' 
                    ";
                    $name = Yii::app()->db->createCommand($query)->queryAll();
                    
                    return (count($name) > 0) ? $name[0]['name'] : "NO ESPECIFICADO";
                }
            ),*/
            'numero_estudiantes_solicitados',
            'descripcion_objetivo_programa',
            'idTipoServicioSocial.tipo_servicio_social',//id_tipo_servicio_social
            'impacto_social_esperado',
            'beneficiarios_programa',
            'actividades_especificas_realizar',
            'mecanismos_supervision',
            'perfil_estudiante_requerido',
            'idApoyoEconomicoPrestador.descripcion_apoyo_economico',//id_apoyo_economico_prestador_servicio_social
            'idTipoApoyoEconomico.tipo_apoyo_economico',//id_tipo_apoyo_economico
            'apoyo_economico',
            array(
                'name' => 'fecha_registro_programa',
                'oneRow' => true,
                'value' => function($data)
                {
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				    $fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateProgramasReg($data->id_programa);
				    return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
                },
            ),
            /*array(
                'name' => 'fecha_modificacion_programa',
                'oneRow' => true,
                'value' => function($data)
                {
                    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
				    $fecha_actualizacion = GetFormatoFecha::getFechaLastUpdateProgramasMod($data->id_programa);
				    return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
                },
            ),*/
            array(
                'name' => 'id_recibe_capacitacion',
                'oneRow'=>true,
                'value' => function($data)
                {
                    return ($data->id_recibe_capacitacion == 1) ? "SI" : "NO";
                },
            ),
            'idClasificacionAreaServicioSocial.clasificacion_area_servicio_social',//id_clasificacion_area_servicio_social
            ),
)); ?>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Horario del Programa
		</span>
	</h2>
</div>

<br><br>
<?php
for($i=0; $i < count($modelSSHorarioDiasHabilesProgramas); $i++){
$this->widget('serviciosocial.components.DetailView4Col', array(
    'data'=>$modelSSHorarioDiasHabilesProgramas[$i],
    'attributes'=>array(
        //'id_horario',
        //'id_programa', id_dia_semana
        array(
            'name' => 'idDiaSemana.dia_semana',
            'oneRow'=>true,
        ),
        'hora_inicio',
        'hora_fin',
        //'horas_totales',
    ),
));
echo "<br><br>";
} 
?>

<br><br>
<div align="center">
    <?php //echo CHtml::link('Entendido', array('listaSolicitudAlumnoProgramaServicioSocial'), array('class'=>'btn btn-success')); ?>
</div>
<br><br>