<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Solicitudes Programa Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Solicitudes a Programas para Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Si aparece CANCELADO puedes elegir otro Programa para realizar tu Servicio Social. La Solicitud de Servicio Social
		aparecerá una vez seas ACEPTADO en el programa donde solicitaste para realizar Servicio Social.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alum-solicitud-programa-servicio-grid',
	'dataProvider'=>$modelSSSolicitudProgramaServicioSocial->searchSolicitudesXAlumno($no_ctrl),
	'filter'=>$modelSSSolicitudProgramaServicioSocial,
	'columns'=>array(
		//'id_solicitud_programa',
		//Numero de control solo para cuestiones de pruebas, despues de quitara
		array(
			'name' => 'no_ctrl',
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		array(
      'name' => 'idPrograma.nombre_programa',//id_programa_servicio_social
      'htmlOptions' => array('width'=>'200px', 'class'=>'text-center'),
    ),
    array(
			'header' => 'Estado Solicitud Supervisor',
      'name' => 'idEstadoSolicitudProgramaSupervisor.estado_solicitud_programa',//id_estado_solicitud_programa
      'filter' => false,
      'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
    ),
		array(
			'header' => 'Estado Solicitud Alumno',
      'name' => 'idEstadoSolicitudProgramaAlumno.estado_solicitud_programa',//id_estado_solicitud_programa
      'filter' => false,
      'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
    ),
		array(
      'header' => 'Tipo Servicio Social',//id_programa_servicio_social
			'value' => function($data)
			{
				return ($data->idPrograma->id_tipo_programa == 1) ? "INTERNO" : "EXTERNO";
			},
      'htmlOptions' => array('width'=>'70px', 'class'=>'text-center'),
    ),
		/*array(
			'name' => 'fecha_solicitud_programa',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
				$fecha_actualizacion = InfoSolicitudProgramas::fechaSolicitudPrograma($data->id_solicitud_programa);
				return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detAlumSolicitudPrograma}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detAlumSolicitudPrograma' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/detalleAlumnoSolicitudPrograma", array("id_solicitud_programa"=>$data->id_solicitud_programa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png'
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{solServicioSocialAlumno}, {pendiente}',
			'header'=>'Solicitud Servicio Social',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'solServicioSocialAlumno' => array
				(
					'label'=>'Solicitud Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/imprimirSolicitudServicioSocial", array("id_solicitud_programa"=>$data->id_solicitud_programa))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' => '($data->id_estado_solicitud_programa_supervisor == 2 AND $data->id_estado_solicitud_programa_alumno == 2) OR ($data->id_estado_solicitud_programa_supervisor == 4 AND $data->id_estado_solicitud_programa_alumno == 4)'
				),
				'pendiente' => array
				(
					'label'=>'No Disponible',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => '($data->id_estado_solicitud_programa_supervisor != 2 OR $data->id_estado_solicitud_programa_alumno != 2) AND ($data->id_estado_solicitud_programa_supervisor != 4 OR $data->id_estado_solicitud_programa_alumno != 4)'
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{impCartaAceptacion}, {nodisponible}',
			'header'=>'Cargar Carta Aceptación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				//Vista servicio social externo
				'impCartaAceptacion' => array
				(
					'label'=>'Descargar Formato Carta de Aceptación',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssCartasAceptacionSsocialExterno/subirCartaAceptacionSSocialExterno", array("id_solicitud_programa"=>$data->id_solicitud_programa, "no_ctrl" => '.$no_ctrl.'))',
					'imageUrl'=>'images/editar_32.png',
					'visible' =>'($data->id_estado_solicitud_programa == 1 or $data->id_estado_solicitud_programa == 2) AND "'.$id_tipo_programa.'" == 2' //Para visualizar su carta de aceptacion
				),
				'nodisponible' => array
				(
					'label'=>'Solo Disponible para Servicio Social Externo',
					'url'=>'#',
					'imageUrl'=>'images/bloquedo_32.png',
					'visible' =>'($data->id_estado_solicitud_programa == 1 or $data->id_estado_solicitud_programa == 2) AND "'.$id_tipo_programa.'" == 1' //Para visualizar su carta de aceptacion
				)
			),
		),*/
		array(/*El alumno debera confirmar si acepta las clausulas en la Solicitud de Servicio Social*/
			'class'=>'CButtonColumn',
			'template'=>'{valAlumSolicitudPrograma}',
			'header'=>'Validar Cláusula Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'valAlumSolicitudPrograma' => array
				(
					'label'=>'Confirmar Solicitud de Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/validaAlumnoClausulaSolicitudServicioSocial", array("id_solicitud_programa"=>$data->id_solicitud_programa))',
					'imageUrl'=>'images/servicio_social/confirm_solicitud_32.png'
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{pendiente},{aceptar},{rechazar},{finalizar}',
			'header'=>'Estado Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'pendiente' => array(
					'label'=>'Estado Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("serviciosocial/solicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
          'imageUrl'=>'images/servicio_social/pendiente_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 1 OR $data->id_estado_solicitud_programa_alumno == 1'
        ),
        'aceptar' => array(
					'label'=>'Estado Solicitud Aceptado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/solicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
          'imageUrl'=>'images/servicio_social/aceptar_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 2 AND $data->id_estado_solicitud_programa_alumno == 2'
				),
        'rechazar' => array(
					'label'=>'Estado Solicitud Rechazado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/solicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
          'imageUrl'=>'images/servicio_social/rechazar_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 3 OR $data->id_estado_solicitud_programa_alumno == 3'
				),
				'finalizar' => array(
					'label'=>'Estado Solicitud Finalizado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/solicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
          'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 4 AND $data->id_estado_solicitud_programa_alumno == 4'
        ),
			),
    ),
	),
)); ?>
