<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Estatus de las Solicitudes de los Programas',
);

/*JS PARA CANCELAR LAS SOLICITUDES DEL ALUMNO A LOS PROGRAMAS*/
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#ss-cancel-solicitud-programa-servicio-social-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#ss-cancel-solicitud-programa-servicio-social-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA CANCELAR LAS SOLICITUDES DEL ALUMNO A LOS PROGRAMAS*/

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
      Estatus de las Solicitudes de los Programas
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-warning"></span>&nbsp;
			     Todas las solicitudes a los Programas con estatus PENDIENTE por parte de los Supervisores.
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-cancel-solicitud-programa-servicio-social-grid',
    'dataProvider'=>$modelSsSolicitudProgramaServicioSocial->searchXSolicitudesEstatuspendienteSupervisores(),
    'filter'=>$modelSsSolicitudProgramaServicioSocial,
    'columns'=>array(
        //'id_solicitud_programa',
        array(
          'name' => 'no_ctrl',
          'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Nombre del Alumno',//
            'value' => function($data)
            {
                $query = " SELECT \"nmbAlumno\" from public.\"E_datosAlumno\" WHERE \"nctrAlumno\" ='$data->no_ctrl' ";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return (count($result) > 0) ? $result[0]['nmbAlumno'] : "DESCONOCIDO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
        ),
        array(
          'name' => 'idPrograma.nombre_programa',
          'filter' => false,
          'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
        ),
        array(
          'name' => 'fecha_solicitud_programa',
          'filter' => false,
          'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
          'header' => 'Validación Alumno',
          'filter' => false,
          'type' => 'raw',
          'value' => function($data)
          {
            return ($data->valida_solicitud_alumno != NULL) ? CHtml::image("images/servicio_social/aprobado_32.png", '', array('class'=>'img-circle','style' =>"width:30px;")) : CHtml::image("images/servicio_social/cancelado_32.png", '', array('class'=>'img-circle','style' =>"width:30px;"));
          },
          'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
        ),
        array(
          'header' => 'Validación Supervisor',
          'filter' => false,
          'type' => 'raw',
          'value' => function($data)
          {
            return ($data->valida_solicitud_supervisor_programa != NULL) ? CHtml::image("images/servicio_social/aprobado_32.png", '', array('class'=>'img-circle','style' =>"width:30px;")) : CHtml::image("images/servicio_social/cancelado_32.png", '', array('class'=>'img-circle','style' =>"width:30px;"));
          },
          'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
        ),
        array(
          'name' => 'idEstadoSolicitudProgramaSupervisor.estado_solicitud_programa',
          'filter' => false,
          'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
          'name' => 'idEstadoSolicitudProgramaAlumno.estado_solicitud_programa',
          'filter' => false,
          'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        [//inicio
    			'class'                => 'CButtonColumn',
    			'template'             => '{cancelar}, {cancelado}', // buttons here...
    			'header' 			   => 'cancelar Solicitud',
    			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
    			'buttons'              => [ // custom buttons options here...
    				'cancelado'   => [
    					'label'   => 'La Solicitud ha sido cancelada',
    					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
    					'visible'=> '$data->id_estado_solicitud_programa_supervisor == 3 AND id_estado_solicitud_programa_alumno == 3', // <-- SHOW IF ROW ACTIVE
    				],
    				'cancelar' => [
    					'label'   => 'Cancelar la Solicitud al Programa',
    					'url'     => 'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/cancelarSolicitudAlumnoPrograma", array("id_solicitud_programa"=>$data->id_solicitud_programa, "no_ctrl"=>$data->no_ctrl))', //
    					'imageUrl' => 'images/servicio_social/no_aprobado_32.png',
    					'visible' => '$data->id_estado_solicitud_programa_supervisor == 1', // <-- SHOW IF ROW INACTIVE
    					'options' => [
    						'title'        => 'Cancelar Solicitud',//
    						'data-confirm' => 'Confirmar Cancelar Solicitud al Programa?' // custom attribute to hold confirmation message
    					],
    					'click'   => $approveJs, // JS string which processes AJAX request
    				],
    			],
    		],//Fin
    ),
)); ?>
