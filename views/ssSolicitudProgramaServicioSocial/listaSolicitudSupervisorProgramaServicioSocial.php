<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Solicitudes al Programa',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
    	Solicitudes al Programa
		</span>
	</h2>
</div>


<!--INDICAR LOS LUGARES QUE TIENEN DISPONIBLES EL SUPERVISOR-->
<br><br><br>
<div align="center">
  <?php
    echo ($disponibles > 0 ) ? '<img align="center" height="50" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/agregar.png"/>' : '<img align="center" height="50" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/completo.png"/>';
  ?></p>
  <p><h4><span style="font-size:16px" class="label label-default">Lugares Disponibles:</span> <?php echo ($disponibles == 0 ) ? '<span style="font-size:16px" class="label label-danger">0</span>' : '<span style="font-size:16px" class="label label-success">'.$disponibles.'</span>'; ?></h4></p>
</div>
<!--INDICAR LOS LUGARES QUE TIENEN DISPONIBLES EL SUPERVISOR-->

<br>
<div align="right">
	<?php //echo ($disponibles == 0 AND $id_programa != 0) ? CHtml::link('Cancelar Todas las Solicitudes Pendientes', array('cancelarSolicitudesPendientes', 'id_programa'=>$id_programa), array('class'=>'btn btn-danger')) : ''; ?>
</div>

<br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		    Solicitudes al programa aparecen en orden ascendente en que son envíadas.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supervisor-solicitud-programa-servicio-social-grid',
	'dataProvider'=>$modelSSSolicitudProgramaServicioSocial->searchSolicitudesSupervisor($rfcsupervisor, $id_programa, $id_tipo_programa),
	'filter'=>$modelSSSolicitudProgramaServicioSocial,
	'columns'=>array(
        //'id_solicitud_programa',
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        array(
            'name' => 'Nombre del Alumno',//
            'value' => function($data)
            {
                $query =
                " SELECT \"nmbAlumno\" as name
                from public.\"E_datosAlumno\" WHERE \"nctrAlumno\" = '$data->no_ctrl'
                ";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return (count($result) > 0) ? $result[0]['name'] : "NO ESPECIFICADO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPrograma.nombre_programa',//id_programa_servicio_social
            'htmlOptions' => array('width'=>'220px', 'class'=>'text-center'),
        ),
		array(
      'name' => 'fecha_solicitud_programa',
			'value' => function($data)
			{

				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
				$fecha_actualizacion = InfoSolicitudProgramas::fechaSolicitudPrograma($data->id_solicitud_programa);
				return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
            },
      'filter' => false,
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detAdminSolicitudPrograma}',
			'header'=>'Revisión Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
				'detAdminSolicitudPrograma' => array(
					'label'=>'Detalle del Alumno Solicitante',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/detalleSupervisorSolicitudPrograma", array("id_solicitud_programa"=>$data->id_solicitud_programa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png'
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{pendiente},{aceptar},{finalizar},{rechazar}',
			'header'=>'Estado Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
				
				'pendiente' => array(
					'label'=>'Estado Solicitud Pendiente',
					//'url' => '#',
          'imageUrl'=>'images/servicio_social/pendiente_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 1 OR $data->id_estado_solicitud_programa_alumno == 1'
        ),
        'aceptar' => array(
					'label'=>'Estado Solicitud Aceptado',
					//'url' => '#',
          'imageUrl'=>'images/servicio_social/aceptar_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 2 AND $data->id_estado_solicitud_programa_alumno == 2'
				),
				'finalizar' => array(
					'label'=>'Estado Solicitud Finalizado',
					//'url' => '#',
          'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 3 AND $data->id_estado_solicitud_programa_alumno == 3'
          ),
        'rechazar' => array(
					'label'=>'Estado Solicitud Rechazado',
					//'url' => '#',
          'imageUrl'=>'images/servicio_social/rechazar_32.png',
          'visible' => '$data->id_estado_solicitud_programa_supervisor == 4 OR $data->id_estado_solicitud_programa_alumno == 4'
				),
			),
        ),
	),
)); ?>
