<?php
/* @var $this ProgramasServicioSocialController */
/* @var $model ProgramasServicioSocial */

$this->breadcrumbs=array(
    'Inicio'=>array('serviciosocial'),
    'Programas Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Firmar Solicitud a Programa Servicio Social
		</span>
	</h2>
</div>