<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */

$this->breadcrumbs=array(

		'Servicio Social'=>'?r=serviciosocial',
    'Solicitudes al Programa' => array('ssSolicitudProgramaServicioSocial/listaSolicitudAlumnoProgramaServicioSocial'),
    'Confirmar Solicitud al Programa'
);

?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
    	Validar Cláusula de Solicitud al Programa
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class = "alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		    Al ACEPTAR la cláusula de la Solicitud de Servicio Social que aparece a continuación se mandará la Solicitud al Supervisor del Programa que elegiste para que decida
				si te Acepta o No en su Programa. Evita dejar Pendiente la Solicitud para evitar que se llene el Programa al que aspiras a entrar para realizar tu Servicio Social.
				También debes tener en cuenta la FECHA LIMITE DE INSCRIPCIÓN.
    </strong></p>
</div>

<br>
<div class="row">
  <div class="col-xs-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h6 class="panel-title">
          Cláusula de la Solicitud de Servicio Social
        </h6>
      </div>
      <div class="panel-body">
        <!--Cuerpo de la Clausula-->
        <div style="letter-spacing: 0em" class="bold">
        Con el fin de dar cumplimiento a lo establecido en la Ley Reglamentaria del Artículo 5º Constitucional relativo al ejercicio
        de profesiones, el suscrito:
        </div>

        <br>
        <b><?php echo $datos_alumno->nmbAlumno; ?></b> con número de control <b><?php echo $modelSSSolicitudProgramaServicioSocial->no_ctrl;?></b> que actualmente
        se encuentra cursando el <b><?php echo $datos_alumno->semAlumno."º"; ?></b> semestre de la carrera de <b><?php echo $carrera; ?></b>
        realizará su <b><span style="color:#D50000;">Servicio Social</span></b> en el programa <b><?php echo $periodo_programa->periodo_programa; ?></b> <b>"<?php echo $datos_programa->nombre_programa; ?>"</b> 
				que será supervisado por <b><?php echo $nombre_supervisor; ?></b> que forma parte de la empresa o institución <b><?php echo $datos_empresa_supervisor; ?></b> ocupando el puesto de <b><?php echo $puesto_supervisor; ?></b>. Dicho Programa 
				tiene una fecha de inicio del <b><?php echo $fec_inicio_programa; ?></b> y una fecha aproximada de terminación del <b><?php echo $fec_fin_programa; ?></b>.

        <br><br><br>
        <div style="letter-spacing: 0em">
        <b><FONT FACE="arial">Me comprometo a realizar el Servicio Social acatando el reglamento del Tecnológico Nacional de México y llevarlo a cabo en el lugar y periodos
        manifestados, así como, a participar con mis conocimientos e iniciativa en las actividades que desempeñe, procurando dar una imagen positiva del
        TNM Instituto Tecnológico de Celaya en el organismo o dependencia oficial, de no hacerlo así, quedo enterado (a) de la cancelación respectiva, la cual procederá
        automáticamente.</FONT></b>
        </div>

        <br>
        <div align="right">
            En la ciudad de Celaya, Guanajuato del dia <?php echo $fecha_sol_programa; ?>.
        </div>

        <br>
        <div align="center">
            <h5 class="center"><FONT FACE="arial"><b>Conformidad</b></FONT></h5>
        </div>
        <!--Cuerpo de la Clausula-->

        <!--Formulario-->
				<div align="center">
	        <div class="form">
	            <?php $form=$this->beginWidget('CActiveForm', array(
	                'id'=>'ss-solicitud-programa-servicio-social-form',
	                // Please note: When you enable ajax validation, make sure the corresponding
	                // controller action is handling ajax validation correctly.
	                // There is a call to performAjaxValidation() commented in generated controller code.
	                // See class documentation of CActiveForm for details on this.
	                'htmlOptions' => array('autocomplete'=>'off'),
	                'enableAjaxValidation'=>false,
	            )); ?>

	            <?php echo $form->errorSummary($modelSSSolicitudProgramaServicioSocial); ?>

							<br><br>
							<?php if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno == 1){ ?>
	            <div class="form-group">
	                <b><?php echo "Responder Cláusula:"; ?></b>
	                <?php echo $form->dropDownList($modelSSSolicitudProgramaServicioSocial,
																								'id_estado_solicitud_programa_alumno',
																								$edos_solicitud,
																								array('prompt'=>'--Seleccionar opción--', 'class'=>'form-control', 'required'=>'required')
																							); ?>
	                <?php echo $form->error($modelSSSolicitudProgramaServicioSocial,'id_estado_solicitud_programa_alumno'); ?>
	            </div>
						<?php }else{ ?>
							<div class="form-group">
	                <b><?php echo "Responder Cláusula:"; ?></b>
	                <?php echo $form->dropDownList($modelSSSolicitudProgramaServicioSocial,
																								'id_estado_solicitud_programa_alumno',
																								$edos_solicitud,
																								array('prompt'=>'--Seleccionar opción--', 'class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled')
																							); ?>
	                <?php echo $form->error($modelSSSolicitudProgramaServicioSocial,'id_estado_solicitud_programa_alumno'); ?>
	            </div>
						<?php }?>

						<br>
						<?php if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno == 1){ ?>
	            <div class="form-group">
	            	<?php echo CHtml::submitButton($modelSSSolicitudProgramaServicioSocial->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
	            </div>
						<?php }else{ ?>
							<div class="form-group">
								<?php echo "<b>Ya elegiste una opción.</b>";?>
								<?php echo "<br>"; ?>
								<?php echo CHtml::link('Volver al Menu Anterior', array('listaSolicitudAlumnoProgramaServicioSocial'), array('class'=>'btn btn-success')); ?>
							</div>
						<?php } ?>

	            <?php $this->endWidget(); ?>
	        </div><!-- form -->
				</div>

        <!--Formulario-->
      </div>
    </div>
  </div>
</div>
