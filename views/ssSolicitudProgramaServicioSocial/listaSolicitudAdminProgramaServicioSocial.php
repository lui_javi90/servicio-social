<?php
/* @var $this SolicitudProgramaServicioSocialController */
/* @var $model SolicitudProgramaServicioSocial */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Solicitudes al Programa',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Solicitudes al Programa
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			Todas las solicitudes a los programas activos.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admin-solicitud-programa-servicio-social-grid',
	'dataProvider'=>$modelSSSolicitudProgramaServicioSocial->searchXSolicitud(),
	'filter'=>$modelSSSolicitudProgramaServicioSocial,
	'columns'=>array(
        //'id_solicitud_programa',
        array(
            'name' => 'no_ctrl',
            'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
        ),
        array(
            'name' => 'Nombre del Alumno',//
            'value' => function($data)
            {
                $query =
                " SELECT \"nmbAlumno\" from public.\"E_datosAlumno\" WHERE \"nctrAlumno\" ='$data->no_ctrl' ";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return (count($result) > 0) ? $result[0]['nmbAlumno'] : "NO ESPECIFICADO";
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        array(
            'name' => 'idPrograma.nombre_programa',//id_programa_servicio_social
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center'),
        ),
		array(
            'name' => 'fecha_solicitud_programa',
			'value' => function($data)
			{
	
				require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
				$fecha_actualizacion = InfoSolicitudProgramas::fechaSolicitudPrograma($data->id_solicitud_programa);
				return $fecha_actualizacion[0]['fecha_act'].' a las '.$fecha_actualizacion[0]['hora'];
            },
            'filter' => false,
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detAdminSolicitudPrograma}',
			'header'=>'Revisión Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detAdminSolicitudPrograma' => array
				(
					'label'=>'Solicitud del Alumno',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/detalleAdminSolicitudPrograma", array("id_solicitud_programa"=>$data->id_solicitud_programa,"no_ctrl"=>$data->no_ctrl))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
					'visible' => '$data->id_estado_solicitud_programa_supervisor != 3 AND $data->id_estado_solicitud_programa_alumno != 3'
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{pendiente},{aceptar},{finalizar},{rechazar}',
			'header'=>'Estado Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'pendiente' => array
				(
					'label'=>'Estado Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
                    'imageUrl'=>'images/servicio_social/pendiente_32.png',
                    'visible' => '$data->id_estado_solicitud_programa_supervisor == 1 || $data->id_estado_solicitud_programa_alumno == 1' 
                ),
                'aceptar' => array
				(
					'label'=>'Estado Solicitud Aceptado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
                    'imageUrl'=>'images/servicio_social/aceptar_32.png',
                    'visible' => '$data->id_estado_solicitud_programa_supervisor == 2 AND $data->id_estado_solicitud_programa_alumno == 2' 
				),
				'finalizar' => array
				(
					'label'=>'Estado Solicitud Finalizado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
                    'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
                    'visible' => '$data->id_estado_solicitud_programa_supervisor == 3 AND $data->id_estado_solicitud_programa_alumno == 3' 
                ),
                'rechazar' => array
				(
					'label'=>'Estado Solicitud Rechazado',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssSolicitudProgramaServicioSocial/responderSolicitudAlumno", array("no_ctrl"=>$data->no_ctrl))',
                    'imageUrl'=>'images/servicio_social/rechazar_32.png',
                    'visible' => '$data->id_estado_solicitud_programa_supervisor == 4 || $data->id_estado_solicitud_programa_alumno == 4' 
				),
			),
        ),
	),
)); ?>