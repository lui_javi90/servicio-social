<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Reportes Bimestrales Validados',
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Reportes Bimestrales Validados
		</span>
	</h2>
</div>

<br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-reportes-bimestral-grid',
    'dataProvider'=>$modelSSReportesBimestral->searchXReporteBimestralValidadoSupervisor($rfcSupervisor, $nocontrol, $anio, $periodo),
    'filter'=>$modelSSReportesBimestral,
    'columns'=>array(
        //'id_reporte_bimestral',
        /*array(
            'name' => 'id_reporte_bimestral',
            'filter' => false,
            'htmlOptions' =>  array('width'=>'20px', 'class'=>'text-center')
        ),*/
        /*array(
            'header' => 'Perfil',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return (empty($data->idserviciosocial->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->idserviciosocial->no_ctrl, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
            },
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),*/
        array(
            'class' => 'ComponentNameCompletoFoto',
            'htmlOptions' => array('width'=>'60px', 'class'=>'text-center')
        ),
        array(
			'name' => 'nocontrol',
			'value' => '$data->idserviciosocial->no_ctrl', // no_ctrl del alumno dueño del servicio social
			'header' => 'No. de Control',
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
        array(
			'header' => 'Bimestre <br>Correspondiente',// no_ctrl del alumno dueño del servicio social
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->bimestres_correspondiente.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Programa',
            'filter' => false,
            'value' => function($data)
            {
                $id = $data->idserviciosocial->id_programa;
                $model = SsProgramas::model()->findByPk($id);

                return $model->nombre_programa;
            },
            'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Año',
            //'filter' => false,
            'filter' => CHtml::activeDropDownList($modelSSReportesBimestral,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $id = $data->id_servicio_social;

                $qry = "select * from pe_planeacion.ss_servicio_social ss 
                            join pe_planeacion.ss_programas p
                            on p.id_programa = ss.id_programa
                            join pe_planeacion.ss_registro_fechas_servicio_social rf
                            on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                            where ss.id_servicio_social = '$id'
                        ";

                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                return '<span style="font-size:14px" class="label label-info">'.$rs[0]['anio'].'</span>';
            },
            'htmlOptions' => array('width'=>'90px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Periodo',
            //'filter' => false,
            'filter' => CHtml::activeDropDownList($modelSSReportesBimestral,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $id = $data->id_servicio_social;

                $qry = "select * from pe_planeacion.ss_servicio_social ss 
                            join pe_planeacion.ss_programas p
                            on p.id_programa = ss.id_programa
                            join pe_planeacion.ss_registro_fechas_servicio_social rf
                            on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
                            where ss.id_servicio_social = '$id'
                        ";

                $rs = Yii::app()->db->createCommand($qry)->queryAll();

                $per = ($rs[0]['id_periodo'] == 1) ? "ENERO-JUNIO" : "AGOSTO-DICIEMBRE";

                return '<span style="font-size:14px" class="label label-success">'.$per.'</span>';
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'name' => 'bimestre <br>Final',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->bimestre_final == 1) ? '<span style="font-size:18px" class="label label-success">SI</span>' : '<span style="font-size:18px" class="label label-danger">NO</span>';
            },
            'htmlOptions' => array('width'=>'40px', 'class'=>'text-center')
        ),
        /*array(
            'header' => 'Validó el Supervisor',
            'filter' => false,
            'type' => 'raw',
            'value' =>  function($data)
            {
                require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
                $fec_val_sup = GetFormatoFecha::getFechaValidaSupervisorReporteBimestral($data->id_reporte_bimestral, $data->id_servicio_social);

                return ($fec_val_sup[0]['fecha_act'] == 'SIN EVALUAR') ? "SIN EVALUAR" :  $fec_val_sup[0]['fecha_act'].' a las '.$fec_val_sup[0]['hora'];
            },
            'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
        ),*/
        array(
            'class' => 'ComponentValidacionSupervisor',
            'header' => 'Validación Supervisor',
            'htmlOptions' => array('width' => '200px', 'class' => 'text-center'),
        ),
        array(
            'name' => 'calificacion_reporte_bimestral',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->calificacion_reporte_bimestral >= 70) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_reporte_bimestral.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_reporte_bimestral.'</span>';
            },
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        /*array(
			'class'=>'CButtonColumn',
			'template'=>'{detReporteBimestralVal}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
            'buttons'=>array
            (
				'detReporteBimestralVal' => array(
					'label'=>'Ver Detalle del Servicio Social',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/showDetServSocialValidado",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',
                    'imageUrl'=>'images/servicio_social/detalle_32.png',
                    'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#showServicioSocial").dialog("open"); 
						
						return false;
					}',
				),
			),
		),*/
        /*
        'observaciones_reporte_bimestral',
        'fecha_inicio_rep_bim',
        'fecha_fin_rep_bim',
        'calificacion_reporte_bimestral',
        'reporte_bimestral_actual',
        'envio_alum_evaluacion',
        'rfcResponsable',
        'rfcJefeOfiServicioSocial',
        */
    ),
)); ?>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'showServicioSocial',
	'options' => array(
		'title' => 'Detalle del Servicio Social',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1200, 
		'height' => 750,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<br><br><br><br><br>
<br><br><br><br><br>