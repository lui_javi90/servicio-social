<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Evaluación Supervisor Reportes Bimestrales',
);

/*JS PARA VALIDAR EVALUACION DEPTO. DEL REPORTE DE SERVICIO SOCIAL */
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#sup-reportes-bimestral-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#sup-reportes-bimestral-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA VALIDAR EVALUACION DEPTO. DEL REPORTE DE SERVICIO SOCIAL */

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Evaluación Supervisor Reportes Bimestrales
		</span>
	</h2>
</div>

<br><br><br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Evaluacion bimestral de los reportes de Servicio Social enviados por los alumnos para su revisión. Debes Evaluar el Reporte Bimestral
        antes de Validarlo.
    </strong></p>
</div>

<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Una vez VALIDADO no se podrá modificar el Reporte Bimestral.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'sup-reportes-bimestral-grid',
    'dataProvider'=>$modelSSReportesBimestral->searchReportesXServicioSocialInterno($rfc_supervisor, $id_tipo_programa, $nocontrol, $id_programa),
    'filter'=>$modelSSReportesBimestral,
    'columns'=>array(
		/*array(
			'name' => 'id_reporte_bimestral',
			'filter' => false,
			'htmlOptions' => array('width'=>'10px')
		),
        array(
			'name' => 'id_servicio_social',//SOlo para verificar que si pertenezcan al servicio social correcto
			'filter' => false,
			'htmlOptions' => array('width'=>'10px')
		),*/
		array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return (empty($data->idserviciosocial->no_ctrl)) ? '--' : CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->idserviciosocial->no_ctrl, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
		array(
			'name' => 'nocontrol',
			'value' => '$data->idserviciosocial->no_ctrl', // no_ctrl del alumno dueño del servicio social
			'header' => 'No. de Control',
			'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
		),
		array(
			'name' => 'bimestres_correspondiente',// no_ctrl del alumno dueño del servicio social
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->bimestres_correspondiente.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
		),
        /*array(
			'header' => 'Nombre del Programa',
			'value' => function($data)
			{
				$id_programa = $data->idserviciosocial->id_programa_servicio_social;
				$query =
				"select nombre_programa from programas_servicio_social
				where id_programa_servicio_social = '$id_programa'
				";
				$nombre_programa = Yii::app()->db->createCommand($query)->queryAll();

				return $nombre_programa[0]['nombre_programa'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'200px')
		),
        array(
			'header' => 'Estado Servicio Social',
			'value' => function($data)
			{
				$id_estado = $data->idserviciosocial->id_estado_servicio_social;
				$query =
				"select estado from estado_servicio_social
				where id_estado_servicio_social = '$id_estado'
				";

				$estado = Yii::app()->db->createCommand($query)->queryAll();

				return strtoupper($estado[0]['estado']);
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'120px')
		),*/
        /*'valida_responsable',
        'valida_oficina_servicio_social',
        'observaciones_reporte_bimestral',
        'fecha_inicio_rep_bim',
        'fecha_fin_rep_bim',
        'envio_alum_evaluacion',
        */
      array(
        'class' => 'ComponentTiemposEvaluacion',
        'header' => 'Tiempo de Validación',
        'htmlOptions' => array('width' => '300px', 'class' => 'text-center'),
      ),
      array(
			'class'=>'CButtonColumn',
			'template'=>'{impReporteBimestral}',
			'header'=>'Ver',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
				'impReporteBimestral' => array(
					'label'=>'Ver Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/imprimirSReporteBimestral",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{obsReporteBimestral}',
			'header'=>'Observación',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'obsReporteBimestral' => array
				(
					'label'=>'Observaciones Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/reportesBimestral/nuevaSupervisorObservacionReporteBimestral",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral))',
					'imageUrl'=>'images/observaciones_32.png'
				),
			),
        ),*/
        array(
			'class'=>'CButtonColumn',
			'template'=>'{evalReporteBimestral}, {noEvalReporteBimestral}',
			'header'=>'Evaluar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
				'evalReporteBimestral' => array(
					'label'=>'Evaluar Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssEvaluacionBimestral/evaluacionSupervisorReporteBimestral", array("id_reporte_bimestral"=>$data->id_reporte_bimestral))',
					'imageUrl'=>'images/servicio_social/evaluar_rep_bimestral_32.png',
					'visible' => '$data->valida_responsable == null'
				),
				'noEvalReporteBimestral' => array(
					'label'=>'Reporte Bimestral fue Evaluado y Validado',
					//'url'=>'#',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => '$data->valida_responsable != null'
				),
			),
		),
        [//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{evaluado}, {noevaluado}', // buttons here...
			'header' 			   => 'Validación',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'evaluado'   => [
					'label'   => 'Reporte Bimestral ya fue Validado',
					//'url'     => '#', // ?r=controller/approve/id/123
					'imageUrl' => 'images/servicio_social/aprobado_32.png',
					'visible' => '$data->valida_responsable != NULL', // <-- SHOW IF ROW ACTIVE
					/*'options' => [
						'title'        => 'Validado',
						'data-confirm' => 'Confirmar Cancelación de Validación del Reporte Bimestral?',
					],
					'click'   => $approveJs, */
				],
				'noevaluado' => [
					'label'   => 'Validar Reporte Bimestral',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/validarSupervisorEvaluacionReporteBimestral", array("id_reporte_bimestral"=>$data->id_reporte_bimestral))', //
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => '$data->valida_responsable === NULL', // <-- SHOW IF ROW INACTIVE
					'options' => [
						//'title'        => 'No Validado', //
						'data-confirm' => 'Confirmar Validación del Reporte Bimestral?' // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],//Fin
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>