<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Reportes Bimestrales del Servicio Social',
);

/*JS PARA VALIDAR ENVIO A EVALUACION DEL REPORTE DE SERVICIO SOCIAL (INTERNO)*/
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#int-reportes-bimestral-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#int-reportes-bimestral-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA VALIDAR ENVIO A EVALUACION DEL REPORTE DE SERVICIO SOCIAL (INTERNO)*/

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Reportes Bimestrales
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Estos son tus reportes de Servicio Social, verifica las fechas de entrega que vienen en ellos para saber en que fecha enviarlo a revisión.
		Únicamente debes dar click en la opción "Enviar a Evaluación" para que se envie al Supervisor del programa en donde estas dando tu Servicio Social y
		posteriormente pasará a la Oficina de Servicio Social. Una vez evaluado y firmado digitalmente por ambas partes se actualizará tu reporte bimestral.
    </strong></p>
</div>

<div class="alert alert-danger">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Una vez enviado el Reporte Bimestral ya no se podrá cancelar el envio. Por lo que verifica bien las fechas de revisión de cada Reporte Bimestral. Si lo envias fuera de fecha de entrega
	  es posible que haya alguna penalización que se vea reflejada en la calificación de tu reporte bimestral.
  </b>
  </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'int-reportes-bimestral-grid',
	'dataProvider'=>$modelSSReportesBimestral->searchReportesBimestralesAlumno($no_ctrl),
	'filter'=>$modelSSReportesBimestral,
	'columns'=>array(
		/*'id_reporte_bimestral',
		array(
			'name' => 'id_servicio_social',//Solo para verificar que si pertenezcan al servicio social correcto
			'filter' => false,
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),*/
		array(
			'name' => 'bimestres_correspondiente',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->bimestres_correspondiente.'</span>';
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
		),
		array(
			'header' => 'Bimestre Final',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->bimestre_final == true) ? '<span style="font-size:18px" class="label label-success">SI</span>' : '<span style="font-size:18px" class="label label-default">NO</span>';
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		/*array(
			'name' => 'observaciones_reporte_bimestral',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),*/
		array(
			'class' => 'ComponentTiemposEvaluacion',
			'header' => 'Tiempo de Validación',
			'htmlOptions' => array('width' => '330px', 'class' => 'text-center'),
		),
		array(
			'header' => 'Calificación Reporte Bimestral',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				$id = $data->id_reporte_bimestral;
				$query =
				"select calificacion_reporte_bimestral from pe_planeacion.ss_reportes_bimestral
				where id_reporte_bimestral = '$id' AND envio_alum_evaluacion is not null
				AND valida_responsable is not null AND valida_oficina_servicio_social is not null
				";

				$cal = Yii::app()->db->createCommand($query)->queryAll();

				if(count($cal) > 0)
				{
					if($cal[0]['calificacion_reporte_bimestral'] >= 70)
					{
						return '<span style="font-size:18px" class="label label-success center">'.$cal[0]['calificacion_reporte_bimestral'].'</span>';
					}else{
						return '<span style="font-size:18px" class="label label-danger center">'.$cal[0]['calificacion_reporte_bimestral'].'</span>';
					}

				}else{
					return "-";
				}

				//return (count($cal) > 0 ) ? $cal[0]['calificacion_reporte_bimestral'] : "-";
			},
			'htmlOptions' => array('width'=>'30px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impReporteBimestral}',
			'header'=>'Ver',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impReporteBimestral' => array
				(
					'label'=>'Ver Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/imprimirReporteBimestral",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
		),
		[//inicio
			'class'                => 'CButtonColumn',
			'template'             => '{enviado}, {noenviado}', // buttons here...
			'header' 			   => 'Enviar a Evaluación',
			'htmlOptions' 		   => array('width'=>'80px','class'=>'text-center'),
			'buttons'              => [ // custom buttons options here...
				'enviado'   => [
					'label'   => 'Reporte Bimestral Enviado a Evaluación',
					//'url'     => '#', // ?r=controller/approve/id/123
					'imageUrl'=> 'images/servicio_social/aprobado_32.png',
					'visible'=> '$data->envio_alum_evaluacion != null', // <-- SHOW IF ROW ACTIVE
					/*'options' => [
						'title'        => 'Enviado',
						'data-confirm' => 'Lo siento pero el Reporte Bimestral ya fue enviado a evaluación.',
					],
					'click'   => $approveJs, */
				],
				'noenviado' => [
					'label'   => 'Enviar Reporte Bimestral a Evaluación',
					'url'     => 'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/envioAlumnoReporteBimestral", array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "oper" => 0))', //
					'imageUrl' => 'images/servicio_social/no_aprobado_32.png',
					'visible' => '$data->envio_alum_evaluacion === null AND $data->reporte_bimestral_actual == true', // <-- SHOW IF ROW INACTIVE
					'options' => [
						'title'        => 'Enviar a Evaluación',//
						'data-confirm' => 'Confirmar envio de Reporte Bimestral a Revisión?' // custom attribute to hold confirmation message
					],
					'click'   => $approveJs, // JS string which processes AJAX request
				],
			],
		],//Fin
	),
)); ?>

<br><br><br>

<!--<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Grafica de Calificaciones de Reportes Bimestrales
		</span>
	</h2>
</div>

<br><br><br>
<div align="center">-->
<?php
	        /*$this->widget(
	            'chartjs.widgets.ChLine',
	            array(
	                'width' => 1000,
	                'height' => 300,
	                'htmlOptions' => array(),
	                //'labels' => array("January","February","March","April","May","June"),
									'labels' => $meses,
	                'datasets' => array(
	                    array(
	                        "fillColor" => "#EF5350",
	                        "strokeColor" => "rgba(220,220,220,1)",
	                        "pointColor" => "#00C853",
	                        "pointStrokeColor" => "#ffffff",
	                        "data" => array(10, 20, 25, 25)
	                    ),
	                ),
	                'options' => array()
	            )
	        );*/
?>
<!--</div>

<br><br><br>-->
