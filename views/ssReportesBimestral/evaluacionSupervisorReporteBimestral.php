<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */

$this->breadcrumbs=array(
	'Servicio Social'=>'?r=serviciosocial',
	'Reportes Bimestrales Servicio Social' => array('ssReportesBimestrales/listaAdminReportesBimestrales'),
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Evaluación Bimestral
		</span>
	</h2>
</div>


<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					Evaluación Bimestral
				</h6>
			</div>
			<div class="panel-body">
			<?php $this->renderPartial('_formEvaluacionBimestral', array(
							'			modelSSEvaluacionBimestral'=>$modelSSEvaluacionBimestral
						)); ?>
			</div>
		</div>
	</div>
</div>