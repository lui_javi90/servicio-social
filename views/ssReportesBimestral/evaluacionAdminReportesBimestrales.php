<?php
/* @var $this EvaluacionBimestralController */
/* @var $model EvaluacionBimestral */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Evaluación Departamento Reportes Bimestrales',
);

/*JS PARA VALIDAR EVALUACION DEPTO. DEL REPORTE DE SERVICIO SOCIAL */
$approveJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#admin-reportes-bimestral-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#admin-reportes-bimestral-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';
/*JS PARA VALIDAR EVALUACION DEPTO. DEL REPORTE DE SERVICIO SOCIAL */

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Evaluación Departamento de los Reportes Bimestrales
		</span>
	</h2>
</div>