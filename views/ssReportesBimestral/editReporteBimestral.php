<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Reportes Bimestrales Todos' => array('ssReportesBimestral/listaReportesBimestralesTodos'),
    'Editar Reporte Bimestral'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Reporte Bimestral
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">

            <div class="panel-heading">
                <h4 class="panel-title">
                    Editar
                </h4>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this SsReportesBimestralController */
                /* @var $model SsReportesBimestral */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ss-reportes-bimestral-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

                    <?php echo $form->errorSummary($modelSSReportesBimestral); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelSSReportesBimestral,'calificacion_reporte_bimestral'); ?>
                        <?php echo $form->textField($modelSSReportesBimestral,'calificacion_reporte_bimestral',array('size'=>3,'maxlength'=>3, 'class'=>'form-control')); ?>
                        <?php echo $form->error($modelSSReportesBimestral,'calificacion_reporte_bimestral'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelSSReportesBimestral->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-success')); ?>
                        <?php echo CHtml::link('Cancelar', array('listaReportesBimestralesTodos'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>

        </div>

    </div>

</div>

<br><br><br><br><br>
<br><br><br><br><br>
