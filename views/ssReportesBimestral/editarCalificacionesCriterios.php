<?php
    $this->breadcrumbs=array(
        'Servicio Social'=>'?r=serviciosocial',
        'Editar Reportes Bimestrales',
    ); 
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Editar Reportes Bimestrales
		</span>
	</h2>
</div>

<br><br><br><br><br>

<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
        Todos los Reportes Bimestrales de los Alumnos con Servicio Social Vigente y que aún no se completa.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-reportes-bimestral-grid',
    'dataProvider'=>$modelSSReportesBimestral->searchXReportesBimestralesAlumnoServicioSocialActual($nocontrol), //Filtrar solo reportes de servicios sociales activos
    'filter'=>$modelSSReportesBimestral,
    'columns'=>array(
        //'id_reporte_bimestral',
        array(
            'header' => 'No. de Control',
            'name' => 'nocontrol',
            'type' => 'raw',
            'value' => function($data)
            {
                $query = "select * from pe_planeacion.ss_servicio_social ss
                          join pe_planeacion.ss_reportes_bimestral rb
                          on rb.id_servicio_social = ss.id_servicio_social
                          where rb.id_servicio_social = '$data->id_servicio_social'";

                $result = Yii::app()->db->createCommand($query)->queryAll();
                return '<span style="font-size:18px" class="label label-success">'.$result[0]['no_ctrl'].'</span>';
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        /*array(
            'header' => 'Nombre Programa',
            'value' => function($data)
            {
                $query = "select * from pe_planeacion.ss_servicio_social ss
                        join pe_planeacion.ss_programas p
                        on p.id_programa = ss.id_programa
                        where ss.id_servicio_social = '$data->id_servicio_social' ";
                
                $result = Yii::app()->db->createCommand($query)->queryAll();

                return $result[0]['nombre_programa'];
            },
            'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
        ),*/
        array(
            'class' => 'ComponentValidacionAlumno',
            'header' => 'Validación Alumno',
            'htmlOptions' => array('width' => '200px', 'class' => 'text-center'),
        ),
        array(
            'class' => 'ComponentValidacionSupervisor',
            'header' => 'Validación Supervisor',
            'htmlOptions' => array('width' => '200px', 'class' => 'text-center'),
        ),
        array(
            'class' => 'ComponentValidacionOficinaServicioSocial',
            'header' => 'Validación Oficina Servicio Social',
            'htmlOptions' => array('width' => '200px', 'class' => 'text-center'),
        ),
        array(
            'name' => 'bimestres_correspondiente',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:18px" class="label label-success">'.$data->bimestres_correspondiente.'</span>';
            },
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        array(
            'name' => 'bimestre_final',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->bimestre_final == true) ? '<span style="font-size:18px" class="label label-success">'."SI".'</span>' : '<span style="font-size:18px" class="label label-danger">'."NO".'</span>';
            },
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        array(
            'name' => 'calificacion_reporte_bimestral',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->calificacion_reporte_bimestral >= 70 ) ? '<span style="font-size:18px" class="label label-success">'.$data->calificacion_reporte_bimestral.'</span>' : '<span style="font-size:18px" class="label label-danger">'.$data->calificacion_reporte_bimestral.'</span>';
            },
            'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
        ),
        //'valida_responsable',
        //'valida_oficina_servicio_social',
        /*
        'observaciones_reporte_bimestral',
        'fecha_inicio_rep_bim',
        'fecha_fin_rep_bim',
        'reporte_bimestral_actual',
        'envio_alum_evaluacion',
        'rfcResponsable',
        'rfcJefeOfiServicioSocial',
        */
        array(
			'class'=>'CButtonColumn',
			'template'=>'{impReporteBimestral}',
			'header'=>'Ver Reporte',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
				'impReporteBimestral' => array(
					'label'=>'Ver Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/imprimirAReporteBimestral",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
		),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{editarReporteBimestral},{bloqEditReporteBimestral}',
			'header'=>'Editar Reporte',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array(
                'editarReporteBimestral' => array
                (
					'label'=>'Editar Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssEvaluacionBimestral/editarReporteBimestralTodo",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',
                    'imageUrl'=>'images/servicio_social/evaluar_rep_bimestral_32.png',
                    'visible' => function($row, $data)
                    {
                        //Si el Servicio Social esta en estatus Cancelado y Finalizado
                        $id = $data->id_servicio_social;

                        $model = SsServicioSocial::model()->findByPk($id);

                        return ($model->id_estado_servicio_social > 1 AND $model->id_estado_servicio_social < 6) ? true : false;
                    }
                ),
                'bloqEditReporteBimestral' => array
                (
                    'label'=>'Editar Reporte Bimestral Bloqueado',
					/*'url'=>'Yii::app()->createUrl("serviciosocial/ssEvaluacionBimestral/editarReporteBimestralTodo",
					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>$data->id_servicio_social))',*/
                    'imageUrl'=>'images/servicio_social/bloquedo_32.png',
                    'visible' => function($row, $data)
                    {
                        //Si el Servicio Social esta en estatus Cancelado y Finalizado
                        $id = $data->id_servicio_social;

                        $model = SsServicioSocial::model()->findByPk($id);

                        return ($model->id_estado_servicio_social == 6 OR $model->id_estado_servicio_social == 7) ? true : false;
                    }
                )
			),
		),
    ),
)); ?>


<br><br><br>