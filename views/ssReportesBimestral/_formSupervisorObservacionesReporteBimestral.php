<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'sup-obs-reportes-bimestral-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'htmlOptions' => array('autocomplete'=>"off"),
    'enableAjaxValidation'=>false,
)); ?>

    <?php echo $form->errorSummary($modelSSReportesBimestral); ?>

    <div class="row">
        <?php echo $form->labelEx($modelSSReportesBimestral,'observaciones_reporte_bimestral'); ?>
        <?php echo $form->textField($modelSSReportesBimestral,'observaciones_reporte_bimestral',array('size'=>60,'maxlength'=>200, 'class'=>'form-control')); ?>
        <?php echo $form->error($modelSSReportesBimestral,'observaciones_reporte_bimestral'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($modelSSReportesBimestral->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Cancelar', array('listaSupervisorReportesBimestrales'), array('class'=>'btn btn-danger')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->