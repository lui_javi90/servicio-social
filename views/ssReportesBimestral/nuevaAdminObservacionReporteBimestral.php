
<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */

if($modelSSReportesBimestral->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Reportes Bimestrales' => array('ssReportesBimestral/listaAdminReportesBimestrales'),
		'Nueva Observación Reporte Bimestral'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Reportes Bimestrales' => array('ssReportesBimestral/listaAdminReportesBimestrales'),
		'Editar Observación Reporte Bimestral'
	);
}

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Observaciones Reportes Bimestrales
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información del Alumno
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3">
					<!--Es un Web Service-->
					<img class="img-circle" align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno" height="200">
                </div>
                <div class="col-xs-5" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['name_alumno']; ?></p>
					
					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['carrera']; ?></p>
					
					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['id_tipo_programa'] == 1) ? "INTERNO" : "EXTERNO"; ?></p>

                    <p><b>Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['bimestre_final'] == true) ? "SI" : "NO" ?></p>

                    <p><b>Reporte Bimestral Correspondiente:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['bimestres_correspondiente']; ?></p>

                    <p><b>Calificación Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['calificacion_reporte_bimestral'] >=70) ? 
                                '<span style="font-size:18px" class="label label-success">'.$servicioSocialAlumno[0]['calificacion_reporte_bimestral'].'</span></b>' :
								'<span style="font-size:18px" class="label label-danger">'.$servicioSocialAlumno[0]['calificacion_reporte_bimestral'].'</span></b>'; ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Realizar Observación Reporte Bimestral
		</span>
	</h2>
</div>

<br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		La Observación va hacia el reporte bimestral del alumno especificado.
    </strong></p>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Observación</h6>
			</div>
			<div class="panel-body">
                <?php $this->renderPartial('_formAdminObservacionesReporteBimestral', array(
                                           'modelSSReportesBimestral'=>$modelSSReportesBimestral
                                        )); ?>
			</div>
		</div>
	</div>
</div>

