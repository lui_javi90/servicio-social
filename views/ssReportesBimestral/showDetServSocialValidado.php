<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 clas="panel-title">
                    Detalle Servicio Social
                </h4>
            </div>
            <div class="panel-body">

                <p><b><?php echo "Programa" ?>:</b>
                <?php echo $name_programa; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('id_estado_servicio_social')); ?>:</b>
                <?php echo $estado_servicio; ?></p>

                <p><b><?php echo CHtml::encode($modelSSServicioSocial->getAttributeLabel('fecha_registro')); ?>:</b>
                <?php echo CHtml::encode($modelSSServicioSocial->fecha_registro); ?></p>
                
                
            </div>
        </div>
    </div>
</div>