<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reportes-bimestral-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('autocomplete'=>'off'),
	'enableAjaxValidation'=>false,
)); ?>

	<strong><p class="note">Campos con <span class="required">*</span> son requeridos.</p></strong>

	<?php echo $form->errorSummary($modelSSEvaluacionBimestral); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'id_servicio_social'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'id_servicio_social'); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'id_servicio_social'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'bimestre_correspondiente'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'bimestre_correspondiente',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'bimestre_correspondiente'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'bimestre_final'); ?>
		<?php echo $form->checkBox($modelSSEvaluacionBimestral,'bimestre_final'); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'bimestre_final'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'valida_responsable'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'valida_responsable'); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'valida_responsable'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'valida_oficina_servicio_social'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'valida_oficina_servicio_social'); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'valida_oficina_servicio_social'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'observaciones_reporte_bimestral'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'observaciones_reporte_bimestral',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'observaciones_reporte_bimestral'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'fecha_inicio_rep_bim'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'fecha_inicio_rep_bim'); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'fecha_inicio_rep_bim'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelSSEvaluacionBimestral,'fecha_fin_rep_bim'); ?>
		<?php echo $form->textField($modelSSEvaluacionBimestral,'fecha_fin_rep_bim'); ?>
		<?php echo $form->error($modelSSEvaluacionBimestral,'fecha_fin_rep_bim'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelSSEvaluacionBimestral->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('reportesBimestrales/listaAdminReportesBimestrales'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->