<style>
    .hoja{
        background-color: transparent;
    }
    table, th, td, tr
    {
        /*border: 1px solid black;*/ /*solid black*/
        border-collapse: collapse;
        color: #000000;
        height:20%;
        text-align: left;
    }
    .table1{
        border: 1px solid black; /*solid black*/
        border-collapse: collapse;
        color: #000000;
        height:15%;
        text-align: left;
    }
    .td1{
        border: 1px solid black;
        color: #000000;
        height:5;
        text-align: left;
    }
    .th1{
        border: 1px solid black;
        color: #000000;
        height:7%
        text-align: left;
    }
    .div2{
        background-color: transparent;
        padding: 0%;
        padding-left: -1px;
    }
    .div3{
        background-color: transparent;
        padding: -1px;
        padding-left: -1px;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .letra4{
        font-size: 9px;
    }
    .letra5{
        font-size: 16px;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    #contenedor{
      margin-left:0px;
      margin-top:0px;
      padding:0 0 0 0;
      width:auto;
      /*background-color: #2196F3;*/
      position:absolute;
  }
  .divcontenido1{

      float:left;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
  }
  .divcontenido2{

      float:right;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
      margin-left:0px;
  }
  /*Firmas del reporte*/
  .divcontenido3{
    float:left;
    /*color:white;*/
    font-size:10px;
    padding-top:50px;
    padding:0px;
    border-style:groove;
    width:37%;
    text-align:left;
  }
  .divcontenido4{
    float:left;
    /*color:white;*/
    font-size:10px;
    padding-top:10px;
    padding-left: 20px;
    border-style:groove;
    width:20%;
    text-align:left;

  }
  .divcontenido5{
    float:right;
    /*color:white;*/
    font-size:10px;
    padding-top:50px;
    padding:0px;
    border-style:groove;
    width:37%;
    text-align:left;
  }
  /*Firmas del reporte*/
  .mytable {
      border-collapse: collapse;
      width: 100%;
      background-color: transparent;
    }
    .mytable-head {
      border: 1px solid black;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .mytable-body {
      border: 1px solid black;
      border-top: 0;
      margin-top: 0;
      padding-top: 0;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .border-bug {
      border-left: 2px dotted black;
      border-top: 2px dotted black;
      border-bottom: 2px dotted black;
      border-right: 2px dotted black;
    }
      .wrapper {
      display : flex;
      align-items : center;
    }
    /*Imagen sobre texto del sello*/
    .contenedor3{
    position: relative;
    display: inline-block;
    text-align: center;
    }
    .texto-encima{
        position: absolute;
        top: 10px;
        left: 10px;
    }
    .centrado{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    /*Imagen sobre texto del sello*/
</style>

<div class="hoja">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />
    <?php //echo '<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/banner_superior_'.trim($datos_reporte_bimestral[0]['no_empresa']).'/'.$banners->banner_superior.'"/>'; ?>
    <br>
    <div class="div2">
        <h6 class="center letra1">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="center letra1">OFICINA DE SERVICIO SOCIAL</h6>
        <h6 class="center bold letra1">FORMATO DE EVALUACIÓN (PLANES 2018-2019)</h6>
        <hr>
    </div>

    <!--NOMBRE COMPLETO DEL PRESTADOR DE SERVICIO SOCIAL (ALUMNO)-->
    <div class="div2">
    <table class="table" align="left">
        <tr class="left" width="100%">
            <td class="bold letra1 left" width="39%">
                Nombre del Prestador de Servicio Social:
            </td>
            <td class="letra1 left" width="61%">
                <?php echo $datos_reporte_bimestral[0]['nombre_alumno']; ?>
            </td>
        <tr>
    </table>
    </div>
    <!--NOMBRE COMPLETO DEL PRESTADOR DE SERVICIO SOCIAL (ALUMNO)-->

    <!--CARRERA Y NO. CONTROL DEL ALUMNO-->
    <div class="div2">
        <table class="table" align="left">
            <tr class="left" width="100%">
                <td class="bold letra1 left" width="9%">
                    Carrera:
                </td>
                <td class="letra1 left" width="61%">
                    <?php echo $datos_reporte_bimestral[0]['carrera_alumno']; ?></p>
                </td>
                <td class="bold letra1 left" width="17%">
                    No. de Control:
                </td>
                <td class="letra1 left" width="13%">
                    <?php echo $datos_reporte_bimestral[0]['no_ctrl']; ?>
                </td>
            </tr>
        </table>
    </div>
    <!--CARRERA Y NO. CONTROL DEL ALUMNO-->

    <!--NOMBRE DEL PROGRAMA-->
    <div class="div2">
    <table class="table" align="left">
        <tr class="left" width="100%">
            <td class="bold letra1 left" width="11%">
                Programa:
            </td>
            <td class="letra1 left" width="89%">
                <?php echo $datos_reporte_bimestral[0]['nombre_programa']; ?>
            </td>
        <tr>
    </table>
    </div>
    <!--NOMBRE DEL PROGRAMA-->

    <!--PERIODO DE REALIZACIÓN-->
    <div class="div2">
    <table class="table" align="left">
        <tr class="left" width="100%">
            <td class="bold letra1 left" width="24%">
                Periodo de Realización:
            </td>
            <td class="letra1 left" width="76%">
                <?php echo "del ". $fec_inicio." al ".$fec_fin; ?>
            </td>
        <tr>
    </table>
    </div>
    <!--PERIODO DE REALIZACIÓN-->

    <!--INDICADOR DE BIMESTRE AL QUE CORRESPONDE EL REPORTE-->
    <div class="div2">
    <table class="table">
        <tr class="left" width="100%" align="left">
            <td class="bold letra1" align="left" width="38%">
                Indique a que bimestre corresponde:
            </td>
            <td class="letra1 th1" align="left" width="12%">
                Bimestre
            </td>
            <td class="bold letra1 th1" align="center" width="12%">
                <?php echo ($datos_reporte_bimestral[0]['bimestre_final'] == true) ? "&nbsp;" : $datos_reporte_bimestral[0]['bimestres_correspondiente']; ?>
            </td>
            <td class="letra1 th1" align="left" width="12%">
                <!--Aqui va una condicion para saber si es el final y mostrar la X-->
                Final&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($datos_reporte_bimestral[0]['bimestre_final'] != NULL) ? "<span class='bold'>".$datos_reporte_bimestral[0]['bimestre_final']."</span>" : "<span class='bold'>&nbsp;</span>"; ?>
            </td>
            <td class="letra1" align="left" width="26%">
                <!--Espacio vacio-->
            </td>
        <tr>
    </table>
    </div>
    <!--INDICADOR DE BIMESTRE AL QUE CORRESPONDE EL REPORTE-->

    <br>
    <!--TABLA DE CRITERIOS A EVALUAR POR PARTE DEL SUPERVISOR DEL PROGRAMA Y DEPTO. VINCULACION-->
    <div class="div3">
        <table class="mytable-head" width="100%">
            <tr>
                <th class="letra2" align="center">
                    En qué medida el prestador del Servicio Social cumple con lo siguiente:
                </th>
            </tr>
        </table>
        <table class="mytable-body" width="100%">
            <tr class="letra1" width="100%" align="center">
                <!--<th style="border: 1px solid black; border-right: 1px solid #FFFFFF" width="10%"> </th>-->
                <th style="border: 1px solid black;" align="center" height="30px" width="80%" class="letra3">Criterios a Evaluar</th>
                <th style="border: 1px solid black;" align="center" height="30px" width="10%" class="letra1">A <br>Valor</div>
                <th style="border: 1px solid black;" align="center" height="30px" width="10%" class="letra2">B <br>Evaluación</div>
            </tr>
        </table>
        <!--CRITERIOS DEL SUPERVISOR-->
        <table class="mytable-body" width="100%">
            <?php
            $contador1 = 0;
            $tamanio = count($lista_criterios_sup);
            for($i=0 ; $i < $tamanio; $i++){
                $contador1++;
                ?>
            <tr class="letra1" width="100%" align="center">
            <?php if($i == 0){ //Encabezado ?>
                <td style="border: 1px solid black;" rowspan="<?php echo $tamanio; ?>" width="12%" class="bold letra1">
                    <p>Evaluación <br>por el <br>responsable <br>del <br>programa</p>
                </td>
                <td style="border: 1px solid black;" height="30px" width="68%" align="left" class="letra1"><?php echo $contador1.". ".$lista_criterios_sup[$i]['descripcion_criterio']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="bold letra1"><?php echo $lista_criterios_sup[$i]['valor_a']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="letra1"><?php echo $lista_criterios_sup[$i]['evaluacion_b']; ?></td>
            <?php }else{ //Cuando $i es mayor a cero ?>
                <td style="border: 1px solid black;" height="30px" width="68%" align="left" class="letra1"><?php echo $contador1.". ".$lista_criterios_sup[$i]['descripcion_criterio']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="bold letra1"><?php echo $lista_criterios_sup[$i]['valor_a']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="letra1"><?php echo $lista_criterios_sup[$i]['evaluacion_b']; ?></td>
                <?php } ?>
            </tr>
            <?php } ?>
        </table>
        <!--CRITERIOS DEL SUPERVISOR-->

        <!--CRITERIOS DEL ADMIN-->
        <table class="mytable-body" width="100%">
            <?php
            $contador2 = 0;
            $tamanio2 = count($lista_criterios_vinc);
            $rows = ($tamanio2 + 2);
            for($j=0 ; $j < $tamanio2; $j++){
                $contador2++;
                ?>
            <tr class="letra1" width="100%" align="center">
            <?php if($j == 0){ //Encabezado ?>
                <td style="border: 1px solid black;" rowspan="<?php echo $rows; ?>" width="12%" class="bold letra1">
                    Evaluación por el Jefe de Oficina de Servicio Social
                </td>
            <?php } ?>
                <td style="border: 1px solid black;" height="30px" width="68%" align="left" class="letra1"><?php echo $contador2.". ".$lista_criterios_vinc[$j]['descripcion_criterio']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="bold letra1"><?php echo $lista_criterios_vinc[$j]['valor_a']; ?></td>
                <td style="border: 1px solid black;" height="30px" width="10%" align="center" class="letra1"><?php echo $lista_criterios_vinc[$j]['evaluacion_b']; ?></td>
            </tr>
            <?php } ?>
             <!--CALIFICACION FINAL-->
             <tr class="letra1" width="100%" align="center">
                <td style="border: 1px solid black; border-right: 1px solid #FFFFFF" height="30px" align="center" class="bold letra2">CALIFICACIÓN FINAL</td>
                <td style="border: 1px solid black;" height="30px" align="center" class="bold letra2"><?php echo ""; ?></td>
                <td style="border: 1px solid black;" height="30px" align="center" class="bold letra2"><?php echo $totalB; ?></td>
            </tr>
            <!--CALIFICACION FINAL-->
            <!--NIVEL DE DESEMPEÑO-->
            <tr class="letra2" width="100%" align="center">
                <td style="border: 1px solid black; border-right: 1px solid #FFFFFF" height="30px" align="center" class="bold letra2 center">NIVEL DE DESEMPEÑO:</td>
                <td style="border: 1px solid black; border-right: 1px solid #FFFFFF" height="30px" align="center" class="bold letra2"> </td>
                <td style="border: 1px solid black;" height="30px" align="center" class="bold letra2"> </td>
            </tr>
            <!--NIVEL DE DESEMPEÑO-->
        </table>
        <!--CRITERIOS DEL ADMIN-->
        
        <!--OBSERVACIONES DEL REPORTE BIMESTRAL-->
        <table class="mytable-head" width="100%">
            <tr class="letra1" width="100%" height="30%" align="center">
                <th class="letra2" align="left">
                    OBSERVACIONES: <?php echo "<span style=\"font-weight:normal;\">".$datos_reporte_bimestral[0]['observaciones_reporte_bimestral']."</span>"; ?>
                </th>
            </tr>
        </table>
        <!--OBSERVACIONES DEL REPORTE BIMESTRAL-->
    </div>
    <!--TABLA DE CRITERIOS A EVALUAR POR PARTE DEL SUPERVISOR DEL PROGRAMA Y DEPTO. VINCULACION-->

    <!--FIRMAS DEL SUPERVISOR, DEL ENCARGADO DE SERVICIO SOCIAL EN VINCULACION Y EL SELLO DE CIENCIAS BASICAS-->
    <div id="contenedor">
      <div class="divcontenido3 center">
            <?php
                //echo "<br>";
                if($datos_reporte_bimestral[0]['valida_responsable'] != NULL)
                {
                    if($datos_reporte_bimestral[0]['rfcSupervisor'] == 'LUMC781102KT1')
                    {
                        echo "<br>";
                        echo '<img align="center" height="60" src="firmas/LUMC781102KT1.jpg" />'; //Firma de jefe ofic de Servicio Social 
                        echo "<br>";
                        echo "<span class=\"bold\">".$datos_reporte_bimestral[0]['nombre_supervisor']."</span>";
                        
                    }else{
                        echo "<br><br>";
                        echo "<span class=\"bold\">El Responsable del Programa de nombre ".$datos_reporte_bimestral[0]['nombre_supervisor']." validó el Reporte Bimestral el dia ".$fec_val_supervisor[0]['fecha_act']." a las ".$fec_val_supervisor[0]['hora'].".</span>";
                    }
                }else{
                    echo "<br><br><br><br>";
                }
                    //echo ($datos_reporte_bimestral[0]['valida_responsable'] != NULL) ? "<span class='letra2'>".$datos_reporte_bimestral[0]['namesupervisor']."</span>" : " ";
                    echo "<br>";
                    echo "<span class='bold'>___________________________________</span>";
                    echo "<br>";
                    //echo ($datos_reporte_bimestral[0]['valida_responsable'] != NULL) ? "<span class='letra2 bold'>".$datos_reporte_bimestral[0]['cargo_supervisor']."</span>" : " ";
                    echo "<span class='letra2 bold'>".$datos_reporte_bimestral[0]['cargo_supervisor']."</span>";
            ?>
      </div>

      <div style="vertical-align: text-top;" class="divcontenido4 bold center">
          <table class="table border-bug" align="center">
              <tr class="letra1" aling="center" width="100%">
                  <td style="border-bottom: 1px solid white;" class="bold" align="center">
                    <span class="center letra1">
                        <!--Sello de la<br>Depedencia<br>Empresa-->
                        <img heigth="85" src="images/servicio_social/sello_sep.jpg" />
                    </span>
                  </td>
              </tr>
          </table>
      </diV>

      <div class="divcontenido5 bold center">
            <?php
                //echo "<br>";
                if($datos_reporte_bimestral[0]['valida_oficina_servicio_social'] != NULL)
                {
                    echo "<br>";
                    echo '<img align="center" height="60" src="firmas/LUMC781102KT1.jpg" />'; //Firma de jefe ofic de Servicio Social 
                    echo "<br>";
                }else{

                    echo "<br><br><br><br>";
                }
                    echo ($datos_reporte_bimestral[0]['valida_oficina_servicio_social'] != NULL) ? "<span class='letra2 bold'>".$datos_reporte_bimestral[0]['namejefeserviciosocial']."</span>" : " ";
                    echo "<br>";
                    echo "___________________________________";
                    echo "<br>";
                    //echo ($datos_reporte_bimestral[0]['valida_oficina_servicio_social'] != NULL) ? "<span class='letra2 bold'>VoBo. Oficina del Servicio Social</span>" : " ";
                    echo "<span class='letra2 bold'>VoBo. Oficina del Servicio Social</span>";
            ?>
      </div>
    </div>
    <!--FIRMAS DEL SUPERVISOR, DEL ENCARGADO DE SERVICIO SOCIAL EN VINCULACION Y EL SELLO DE CIENCIAS BASICAS-->
    
    <br><br>
    <!--LEGENDA INFERIOR DEL EXPEDIENTE-->
    <div class=" bold div2" align="left">
        <?php echo "<span class='bold letra4'>"."c.c.p. Expediente Oficina de Servicio Social"."</span>"; ?>
    </div>
    <!--LEGENDA INFERIOR DEL EXPEDIENTE-->
</div>
