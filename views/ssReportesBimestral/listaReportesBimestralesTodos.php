<?php
/* @var $this SsProgramasController */
/* @var $model SsProgramas */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Reportes Bimestrales Todos'
);

$delRepJs = 'js:function(__event)
{
	__event.preventDefault(); // disable default action

	var $this = $(this), // link/button
		confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
		url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

	if(confirm(confirm_message)) // Si se confirma la operacion entonces...
	{
		// perform AJAX request
		$("#ss-reportes-bimestral-todos-grid").yiiGridView("update",
		{
			type	: "POST", // important! we only allow POST in filters()
			dataType: "json",
			url		: url,
			success	: function(data)
			{
				console.log("Success:", data);
				$("#ss-reportes-bimestral-todos-grid").yiiGridView("update"); // refresh gridview via AJAX
			},
			error	: function(xhr)
			{
				console.log("Error:", xhr);
			}
		});
	}
}';

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Reportes Bimestrales Todos
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ss-reportes-bimestral-todos-grid',
    'dataProvider'=>$modelSSReportesBimestral->search(), //
    'filter'=>$modelSSReportesBimestral,
    'columns'=>array(
        //
        array(
            'name' => 'id_reporte_bimestral',
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'id_servicio_social',
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'bimestres_correspondiente',
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'bimestre_final',
            'filter' => false,
            'value' => function($data)
            {
                return ($data->bimestre_final == true) ? "SI" : "NO";
            },
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'envio_alum_evaluacion',
            'filter' => false,
            'htmlOptions' => array('width'=>'120px','class'=>'text-center')
        ),
        array(
            'name' => 'valida_responsable',
            'filter' => false,
            'htmlOptions' => array('width'=>'120px','class'=>'text-center')
        ),
        array(
            'name' => 'valida_oficina_servicio_social',
            'filter' => false,
            'htmlOptions' => array('width'=>'120px','class'=>'text-center')
        ),
        array(
            'name' => 'calificacion_reporte_bimestral',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                return ($data->calificacion_reporte_bimestral >= 70) ? '<span style="font-size:14px" class="label label-success">'.$data->calificacion_reporte_bimestral.'</span>' : '<span style="font-size:14px" class="label label-danger">'.$data->calificacion_reporte_bimestral.'</span>';
            },
            'htmlOptions' => array('width'=>'120px','class'=>'text-center')
        ),
        /*
        'observaciones_reporte_bimestral',
        'fecha_inicio_rep_bim',
        'fecha_fin_rep_bim',
        'calificacion_reporte_bimestral',
        'reporte_bimestral_actual',
        'envio_alum_evaluacion',
        'rfcResponsable',
        'rfcJefeOfiServicioSocial',
        */
        array(
			'class'=>'CButtonColumn',//
			'template'=>'{editReporteBimestral}',
			'header'=>'Editar Reporte Bimestral',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editReporteBimestral' => array
				(
					'label'=>'Editar Reporte Bimestral',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/editReporteBimestral", array("id_reporte_bimestral"=>$data->id_reporte_bimestral))',
                    'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
        array(
			'class'=>'CButtonColumn',//
			'template'=>'{elimReporteBimestral}',
			'header'=>'Eliminar Reporte Bimestral',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'elimReporteBimestral' => array
				(
					'label'=>'Eliminar Programa',
					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/delReporteBimestralServicioSocial", array("id_reporte_bimestral"=>$data->id_reporte_bimestral))',
                    'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
                    'options' => array(
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Eliminar Reporte Bimestral?', // custom attribute to hold confirmation message
                    ),
					'click'   => $delRepJs, // JS string which processes AJAX request
				),
			),
		)
    ),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>