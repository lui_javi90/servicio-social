<?php
/* @var $this ReportesBimestralController */
/* @var $model ReportesBimestral */

if($modelSSReportesBimestral->isNewRecord)
{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Reportes Bimestrales' => array('reportesBimestral/listaSupervisorReportesBimestrales'),
		'Nueva Observación Reporte Bimestral'
	);
}else{
	$this->breadcrumbs=array(
		'Servicio Social'=>'?r=serviciosocial',
		'Reportes Bimestrales' => array('reportesBimestral/listaSupervisorReportesBimestrales'),
		'Editar Observación Reporte Bimestral'
	);
}

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Observaciones Reportes Bimestrales
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    Información del Alumno
                </h6>
            </div>
            <div class="panel-body">
                <div class="col-xs-3">
					<!--Es un Web Service-->
					<img align="center" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?>" alt="foto alumno" height="200">
                </div>
                <div class="col-xs-5" align="left">
					<p><b>Nombre:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['name_alumno']; ?></p>
					
					<p><b>No. Control:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['no_ctrl']; ?></p>

					<p><b>Carrera:</b>
					&nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['carrera']; ?></p>
					
					<p><b>Programa:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['programa'] ?></p>

                    <p><b>Periodo Escolar Inicio Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo $periodo_inicio; ?></p>

                    <p><b>Periodo Escolar Fin Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo $periodo_fin; ?></p>
					
                </div>
                <div class="col-xs-4" align="left">
                    <p><b>Tipo de Servicio Social:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['periodo_servsocial'] ?></p>

                    <p><b>Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['bimestre_final'] == true) ? "SI" : "NO" ?></p>

                    <p><b>Reporte Bimestral Correspondiente:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['bimestre_correspondiente']; ?></p>
                    
                    <p><b>Fecha Inicio Reporte Bimestral:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['fecha_inicio_rep_bim']; ?></p>
                    
                    <p><b>Fecha Fin Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo $servicioSocialAlumno[0]['fecha_fin_rep_bim']; ?></p>
                    
                    <p><b>Calificación Reporte Bimestral Final:</b>
                    &nbsp;&nbsp;<?php echo ($servicioSocialAlumno[0]['calificacion_reporte_bimestral'] >=70) ? 
                                "<b><FONT COLOR=\"green\">".$servicioSocialAlumno[0]['calificacion_reporte_bimestral'].'</FONT></b>' :
                                "<b><FONT COLOR=\"red\">".$servicioSocialAlumno[0]['calificacion_reporte_bimestral'].'</FONT></b>'; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Realizar Observación Reporte
		</span>
	</h2>
</div>

<br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		La Observación va hacia el reporte bimestral del alumno.
    </strong></p>
</div>

<br>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Observación</h6>
			</div>
			<div class="panel-body">
                <?php $this->renderPartial('_formSupervisorObservacionesReporteBimestral', array(
                                                                            'modelSSReportesBimestral'=>$modelSSReportesBimestral
                                                                            
                                                                            )); ?>
			</div>
		</div>
	</div>
</div>