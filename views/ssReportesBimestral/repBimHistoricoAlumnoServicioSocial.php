<?php
/* @var $this ServicioSocialController */
/* @var $model ServicioSocial */

$this->breadcrumbs=array(
    'Servicio Social'=>'?r=serviciosocial',
    'Histórico Servicio Social' => array('ssServicioSocial/historicoAlumnoServicioSocial'),
    'Documentación Histórico Servicio Social',
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Documentación Histórico Servicio Social
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign">
        </span>&nbsp;Reportes Bimestrales correspondientes al Servicio Social FINALIZADO.
    </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'hist-alum-reportes-bimestral-grid',
    'dataProvider'=>$modelSSReportesBimestral->searchXHistoricoDocumentosServicioSocial($id_servicio_social),
    'filter'=>$modelSSReportesBimestral,
    'columns'=>array(
        //'id_reporte_bimestral',
        //'id_servicio_social',
        array(
            'name' => 'bimestres_correspondiente',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        //'bimestre_final',
        //'valida_responsable',
        //'valida_oficina_servicio_social',
        array(
            'name' => 'observaciones_reporte_bimestral',
            'filter' => false,
            'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
        ),
        /*'fecha_inicio_rep_bim',
        'fecha_fin_rep_bim',
        'envio_alum_evaluacion',*/
        array(
            'name' => 'calificacion_reporte_bimestral',
            'filter' => false,
            'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:18px" class="label label-success">'.$data->calificacion_reporte_bimestral.'</span>';
			},
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
  			'class'=>'CButtonColumn',
  			'template'=>'{impReporteBimestral}',
  			'header'=>'Ver',
  			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
  			'buttons'=>array
  			(
  				'impReporteBimestral' => array
  				(
  					'label'=>'Ver Reporte Bimestral',
  					'url'=>'Yii::app()->createUrl("serviciosocial/ssReportesBimestral/imprimirReporteBimestral",
  					array("id_reporte_bimestral"=>$data->id_reporte_bimestral, "id_servicio_social"=>'.$id_servicio_social.'))',
  					'imageUrl'=>'images/servicio_social/printer.png',
  				),
  			),
		),
  ),
)); ?>
