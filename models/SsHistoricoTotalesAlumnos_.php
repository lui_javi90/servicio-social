<?php
class SsHistoricoTotalesAlumnos_ extends SsHistoricoTotalesAlumnos
{
	public $cve_especialidad;

    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('horas_totales_servicio_social',$this->horas_totales_servicio_social,true);
		$criteria->compare('fecha_modificacion_horas_totales',$this->fecha_modificacion_horas_totales,true);
		$criteria->compare('calificacion_final_servicio_social',$this->calificacion_final_servicio_social,true);
		$criteria->compare('completo_servicio_social',$this->completo_servicio_social);
		$criteria->compare('firma_digital_alumno',$this->firma_digital_alumno,true);
		$criteria->compare('calificacion_kardex',$this->calificacion_kardex);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	/*Horas completadas de los alumnos con el servicio social activado*/
	public function searchXAlumnosAltaServicioSocial()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "no_ctrl ASC";

		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('horas_totales_servicio_social',$this->horas_totales_servicio_social,true);
		$criteria->compare('fecha_modificacion_horas_totales',$this->fecha_modificacion_horas_totales,true);
		$criteria->compare('calificacion_final_servicio_social',$this->calificacion_final_servicio_social,true);
		$criteria->compare('completo_servicio_social',$this->completo_servicio_social);
		$criteria->compare('firma_digital_alumno',$this->firma_digital_alumno,true);
		$criteria->compare('calificacion_kardex',$this->calificacion_kardex);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Horas completadas de los alumnos con el servicio social activado*/

	/*ALUMNOS QUE COMPLETARON LAS 480 HORAS DE SERVICIO SOCIAL */
	public function searchXAlumnosCompletaronServicioSocial()
	{
		$totalHoras = $this->getHorasServicioSocial();
		$criteria = new CDbCriteria;
		$criteria->condition = "horas_totales_servicio_social = '$totalHoras' AND completo_servicio_social = true AND 
								calificacion_kardex = false";
		$criteria->order = "no_ctrl ASC";

		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('horas_totales_servicio_social',$this->horas_totales_servicio_social,true);
		$criteria->compare('fecha_modificacion_horas_totales',$this->fecha_modificacion_horas_totales,true);
		$criteria->compare('calificacion_final_servicio_social',$this->calificacion_final_servicio_social,true);
		$criteria->compare('completo_servicio_social',$this->completo_servicio_social);
		$criteria->compare('firma_digital_alumno',$this->firma_digital_alumno,true);
		$criteria->compare('calificacion_kardex',$this->calificacion_kardex);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*ALUMNOS QUE COMPLETARON LAS 480 HORAS DE SERVICIO SOCIAL */

	//calificacion_kardex pasa a true cuando la calificacion de Servicio Social del Alumno ya fue insertada en su Kardex
	public function searchXServicioSocialCompletadoXAlumno($cve_especialidad)
	{
		//480 en la tabla ss_configuracion
		$totalHoras = $this->getHorasServicioSocial(); 
		$criteria = new CDbCriteria;
		/*$criteria->condition = " horas_totales_servicio_social = '$totalHoras' AND completo_servicio_social = true AND 
								calificacion_kardex = true";
		$criteria->order = "no_ctrl ASC";*/
		$criteria->alias = "hta";
		$criteria->select = "*";

		if($cve_especialidad != NULL)
		{
			
			$criteria->join = " join public.\"E_datosAlumno\" eda on eda.\"nctrAlumno\" = hta.no_ctrl
									join public.\"E_especialidad\" esp on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\" ";
			$criteria->condition = " hta.completo_servicio_social = true AND hta.calificacion_kardex = true AND 
									hta.horas_totales_servicio_social = '$totalHoras' AND esp.\"cveEspecialidad\" = '$cve_especialidad' ";
			
		}else{

			$criteria->join = " join public.\"E_datosAlumno\" eda on eda.\"nctrAlumno\" = hta.no_ctrl
									join public.\"E_especialidad\" esp on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\" ";
			$criteria->condition = " hta.completo_servicio_social = true AND hta.calificacion_kardex = true AND 
									 hta.horas_totales_servicio_social = '$totalHoras' ";
		}

		$criteria->order = "no_ctrl ASC";

		/*
		select * from pe_planeacion.ss_historico_totales_alumnos hta
		join public."E_datosAlumno" eda
		on eda."nctrAlumno" = hta.no_ctrl
		join public."E_especialidad" esp
		on esp."cveEspecialidad" = eda."cveEspecialidadAlu"
		where hta.completo_servicio_social = true AND hta.calificacion_kardex = true AND 
		hta.horas_totales_servicio_social = 480 AND esp."cveEspecialidad" = 3
		*/

		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('horas_totales_servicio_social',$this->horas_totales_servicio_social,true);
		$criteria->compare('fecha_modificacion_horas_totales',$this->fecha_modificacion_horas_totales,true);
		$criteria->compare('calificacion_final_servicio_social',$this->calificacion_final_servicio_social,true);
		$criteria->compare('completo_servicio_social',$this->completo_servicio_social);
		$criteria->compare('firma_digital_alumno',$this->firma_digital_alumno,true);
		$criteria->compare('calificacion_kardex',$this->calificacion_kardex);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	//480 Horas
	public function getHorasServicioSocial()
	{
		$modelSSConfiguracion = SsConfiguracion::model()->findByPk(1);

		return ($modelSSConfiguracion === NULL) ? 0 : $modelSSConfiguracion->horas_max_servicio_social;
	}
}
?>