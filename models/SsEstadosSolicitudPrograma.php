<?php

/**
 * This is the model class for table "ss_estados_solicitud_programa".
 *
 * The followings are the available columns in table 'ss_estados_solicitud_programa':
 * @property integer $id_estado_solicitud_programa
 * @property string $estado_solicitud_programa
 *
 * The followings are the available model relations:
 * @property SsSolicitudProgramaServicioSocial[] $ssSolicitudProgramaServicioSocials
 * @property SsSolicitudProgramaServicioSocial[] $ssSolicitudProgramaServicioSocials1
 */
class SsEstadosSolicitudPrograma extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_estados_solicitud_programa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estado_solicitud_programa', 'required'),
			array('estado_solicitud_programa', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_estado_solicitud_programa, estado_solicitud_programa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
			// NOTE: you may need to adjust the relation name and the related
	    // class name for the relations automatically generated below.
	    return array(
	          'ssSolicitudProgramaServicioSocials' => array(self::HAS_MANY, 'SsSolicitudProgramaServicioSocial', 'id_estado_solicitud_programa_supervisor'),
	          'ssSolicitudProgramaServicioSocials1' => array(self::HAS_MANY, 'SsSolicitudProgramaServicioSocial', 'id_estado_solicitud_programa_alumno'),
	    );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_estado_solicitud_programa' => 'Id Estado Solicitud Programa',
			'estado_solicitud_programa' => 'Estado Solicitud Programa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_estado_solicitud_programa',$this->id_estado_solicitud_programa);
		$criteria->compare('estado_solicitud_programa',$this->estado_solicitud_programa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsEstadosSolicitudPrograma the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
