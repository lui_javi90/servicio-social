<?php

/**
 * This is the model class for table "pe_planeacion.ss_supervisores_programas".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_supervisores_programas':
 * @property string $rfcSupervisor
 * @property string $nombre_supervisor
 * @property string $apell_paterno
 * @property string $apell_materno
 * @property string $cargo_supervisor
 * @property string $email_supervisor
 * @property integer $id_tipo_supervisor
 * @property integer $id_status_supervisor
 * @property string $id_sexo
 * @property string $foto_supervisor
 * @property integer $id_unidad_receptora
 * @property string $nombre_jefe_depto
 * @property string $dscDepartamento
 * @property boolean $eres_jefe_depto
 * @property string $gradoMaxEstudios
 * @property string $passwSupervisor
 * @property string $grMaxEstSup
 *
 * The followings are the available model relations:
 * @property SsTipoStatus $idStatusSupervisor
 * @property SsUnidadesReceptoras $idUnidadReceptora
 * @property SsResponsableProgramaExterno[] $ssResponsableProgramaExternos
 */
class SsSupervisoresProgramas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_supervisores_programas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('rfcSupervisor, nombre_supervisor, apell_paterno, apell_materno, cargo_supervisor, email_supervisor, id_tipo_supervisor, id_status_supervisor, id_sexo, id_unidad_receptora, nombre_jefe_depto, dscDepartamento, eres_jefe_depto', 'required'),
            array('id_tipo_supervisor, id_status_supervisor, id_unidad_receptora', 'numerical', 'integerOnly'=>true),
            array('rfcSupervisor', 'length', 'max'=>13),
            array('nombre_supervisor', 'length', 'max'=>30),
            array('apell_paterno, apell_materno', 'length', 'max'=>20),
            array('cargo_supervisor', 'length', 'max'=>150),
            array('email_supervisor', 'length', 'max'=>50),
            array('id_sexo', 'length', 'max'=>1),
            array('foto_supervisor', 'length', 'max'=>17),
            array('nombre_jefe_depto', 'length', 'max'=>200),
            array('dscDepartamento', 'length', 'max'=>250),
            array('gradoMaxEstudios, grMaxEstSup', 'length', 'max'=>5),
			array('passwSupervisor', 'length', 'max'=>32),
			array('rfcSupervisor', 'unique','message'=>'Este RFC ya existe!'),
			array('passwSupervisor', 'match', 'pattern'=>'/[!@#$%*a-zA-Z0-9]{8,}/','message' => 'Contraseña débil'),
			array('passwSupervisor','required', 'except' => 'update'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('rfcSupervisor, nombre_supervisor, apell_paterno, apell_materno, cargo_supervisor, email_supervisor, id_tipo_supervisor, id_status_supervisor, id_sexo, foto_supervisor, id_unidad_receptora, nombre_jefe_depto, dscDepartamento, eres_jefe_depto, gradoMaxEstudios, passwSupervisor, grMaxEstSup', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'idStatusSupervisor' => array(self::BELONGS_TO, 'SsTipoStatus', 'id_status_supervisor'),
            'idUnidadReceptora' => array(self::BELONGS_TO, 'SsUnidadesReceptoras', 'id_unidad_receptora'),
            'ssResponsableProgramaExternos' => array(self::HAS_MANY, 'SsResponsableProgramaExterno', 'rfcSupervisor'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rfcSupervisor' => 'RFC del Supervisor',
			'nombre_supervisor' => 'Nombre',
			'apell_paterno' => 'Apellido Paterno',
			'apell_materno' => 'Apellido Materno',
			'cargo_supervisor' => 'Cargo del Supervisor',
			'email_supervisor' => 'Email',
			'id_tipo_supervisor' => 'Tipo de Supervisor',
			'id_status_supervisor' => 'Estatus Supervisor',
			'id_sexo' => 'Genero',
			'foto_supervisor' => 'Foto de Perfil',
			'id_unidad_receptora' => 'Empresa',
			'nombre_jefe_depto' => 'Nombre Jefe del Departamento',
            'dscDepartamento' => 'Departamento',
            'eres_jefe_depto' => '¿Eres el Jefe del Departamento?',
            'gradoMaxEstudios' => 'Grado Max. de Estudios del Jefe de Departamento',
			'passwSupervisor' => 'Contraseña del Supervisor',
			'grMaxEstSup' => 'Grado Max. Estudios Supervisor',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsSupervisoresProgramas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
