<?php

/**
 * This is the model class for table "pe_planeacion.ss_servicios_sociales_anteriores".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_servicios_sociales_anteriores':
 * @property integer $id_servicio_social_anterior
 * @property string $no_ctrl
 * @property string $nombre_programa
 * @property integer $id_periodo_programa
 * @property string $nombre_completo_supervisor
 * @property string $cargo_supervisor
 * @property string $horas_liberadas
 * @property string $fecha_inicio_programa
 * @property string $fecha_fin_programa
 * @property string $calificacion_servicio_social
 * @property string $nombre_empresa
 *
 * The followings are the available model relations:
 * @property EDatosAlumno $noCtrl
 * @property SsPeriodosProgramas $idPeriodoPrograma
 */
class SsServiciosSocialesAnteriores extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_servicios_sociales_anteriores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('no_ctrl, nombre_programa, id_periodo_programa, nombre_completo_supervisor, horas_liberadas, fecha_inicio_programa, fecha_fin_programa, calificacion_servicio_social', 'required'),
            array('id_periodo_programa', 'numerical', 'integerOnly'=>true),
            array('no_ctrl', 'length', 'max'=>8),
            array('nombre_programa', 'length', 'max'=>150),
            array('nombre_completo_supervisor', 'length', 'max'=>80),
            array('cargo_supervisor', 'length', 'max'=>100),
            array('horas_liberadas, calificacion_servicio_social', 'length', 'max'=>3),
            array('nombre_empresa', 'length', 'max'=>200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_servicio_social_anterior, no_ctrl, nombre_programa, id_periodo_programa, nombre_completo_supervisor, cargo_supervisor, horas_liberadas, fecha_inicio_programa, fecha_fin_programa, calificacion_servicio_social, nombre_empresa', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'noCtrl' => array(self::BELONGS_TO, 'EDatosAlumno', 'no_ctrl'),
			'idPeriodoPrograma' => array(self::BELONGS_TO, 'SsPeriodosProgramas', 'id_periodo_programa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_servicio_social_anterior' => 'Id Servicio Social Anterior',
			'no_ctrl' => 'No. de Control',
			'nombre_programa' => 'Nombre del Programa',
			'id_periodo_programa' => 'Periodo Programa',
			'nombre_completo_supervisor' => 'Nombre Completo del Supervisor',
			'cargo_supervisor' => 'Cargo del Supervisor',
			'horas_liberadas' => 'Horas Liberadas',
			'fecha_inicio_programa' => 'Fecha Inicio del Programa',
			'fecha_fin_programa' => 'Fecha Fin del Programa',
			'calificacion_servicio_social' => 'Calificación',
			'nombre_empresa' => 'Nombre de la Empresa',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsServiciosSocialesAnteriores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
