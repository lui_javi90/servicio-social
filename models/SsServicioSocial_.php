<?php
class SsServicioSocial_ extends SsServicioSocial
{
	//Para filtrar
	public $anio;
	public $periodo;

    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	//Filttar los servicios sociales de los alumnos inscritos en determinado Programa
	public function searchListaAlumnossEnPrograma($criteria2)
	{
		$criteria2->compare('id_servicio_social',$this->id_servicio_social);
		$criteria2->compare('no_ctrl',$this->no_ctrl,true);
		$criteria2->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria2->compare('id_programa',$this->id_programa);
		$criteria2->compare('fecha_registro',$this->fecha_registro,true);
		$criteria2->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria2->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria2->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria2->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria2->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria2->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria2->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria2->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria2->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria2->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria2->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria2,
			'pagination'=>array(
				'pageSize'=>10,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	/*Para obtener las cartas de presentacion y terminacion del alumno como Historico */
	public function searchXServicioSocial($id_servicio_social)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = "*";
		$criteria->condition = "id_servicio_social = '$id_servicio_social' AND id_estado_servicio_social = 6 ";

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>10,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Para obtener las cartas de presentacion y terminacion del alumno como Historico */

	/*Filtra por el servicio social ACTUAL del alumno con no. control*/
	public function searchXAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria;
		$criteria->select = "*";
		$criteria->condition = " (id_estado_servicio_social > 1 AND id_estado_servicio_social != 6 AND id_estado_servicio_social != 7 AND id_estado_servicio_social != 8) AND no_ctrl = '$no_ctrl' ";

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
		$criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
		$criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
		$criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
		$criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
		$criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
		$criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>5,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Filtra por el servicio social ACTUAL del alumno con no. control*/

	/*Alumnos que estan dados de alta y por tanto se les puede hacer una observacion con respecto a su servicio social */
	public function searchXAlumnosAltaServSocial()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias = "ss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas pss on pss.id_programa = ss.id_programa";
		$criteria->condition = "pss.id_status_programa = 1 AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6)";
		$criteria->order = "ss.id_servicio_social ASC";

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Alumnos que estan dados de alta y por tanto se les puede hacer una observacion con respecto a su servicio social */

	/*Historico servicio social del alumno con no. control (a Excepcion de los cancelados y el actual)
	Puros con Estatus Finalizado = 6*/
	public function searchXHistoricoAlumno($no_ctrl)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = "*";
		$criteria->condition = "(id_estado_servicio_social = 6) AND no_ctrl = '$no_ctrl' ";
		$criteria->order = "id_servicio_social ASC";

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>10,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Historico servicio social del alumno con no. control (a Excepcion de los cancelados y el actual)
	Puros con Estatus Finalizado = 6*/

	/*Lista de servicios sociales que se van a liberar*/
	public function searchServicioSocialXReportesEvaluadosYValidados()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias = "ss";
		$criteria->select = "distinct ss.id_servicio_social, ss.no_ctrl, ss.id_estado_servicio_social,
		ss.id_programa, ss.fecha_registro, ss.fecha_modificacion, ss.calificacion_servicio_social";
		$criteria->join = "join pe_planeacion.ss_reportes_bimestral rb on rb.id_servicio_social = ss.id_servicio_social";
		$criteria->condition = " ss.id_estado_servicio_social = 4 AND
		(rb.envio_alum_evaluacion is not null AND rb.valida_oficina_servicio_social is not null AND rb.valida_responsable is not null)";
		$criteria->order = "ss.id_servicio_social ASC";

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Lista de servicios sociales que se van a liberar*/

	/*Filtrar por las observaciones que puede hacer el supervisor a cada uno de los
	alumnos que estan en su programa*/
	public function searchXObservacionesPrograma($rfcSupervisor, $id_tipo_programa, $id_programa)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->alias = "ss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas pss ON pss.id_programa = ss.id_programa
		join pe_planeacion.ss_responsable_programa_interno rpi ON rpi.id_programa = pss.id_programa";
		$criteria->condition = "rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6)";

		if($id_tipo_programa == 1)
		{
			$criteria->alias = "ss";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_programas pss ON pss.id_programa = ss.id_programa
			join pe_planeacion.ss_responsable_programa_interno rpi ON rpi.id_programa = pss.id_programa";
			$criteria->condition = "rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6)";
		}else
		if($id_tipo_programa == 2)
		{
			$criteria->alias = "ss";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_programas pss ON pss.id_programa = ss.id_programa
			join pe_planeacion.ss_responsable_programa_externo rpe ON rpe.id_programa = pss.id_programa";
			$criteria->condition = "rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6)";
		}


		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Filtrar por las observaciones que puede hacer el supervisor a cada uno de los
	alumnos que estan en su programa*/

	/*Filtrar por Programas del servicio social de los alumnos*/
	public function searchXProgramasServicioSocial($id_programa)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		if($id_programa != null)
		{
			$criteria->select = "*";
			$criteria->condition = "id_programa = '$id_programa' AND (id_estado_servicio_social != 1 )"; //No muestra estado "Inscripcion"
			$criteria->order = "id_servicio_social ASC";

		}else{
			$criteria->select = "*";
			$criteria->condition = "id_estado_servicio_social != 1"; //No muestra estado "Inscripcion"
			$criteria->order = "id_servicio_social ASC";
		}

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Filtrar por estados del servicio social (menos finalizados y cancelados) de los alumnos*/

	/*Filtrar por estados de los Historicos del Servicio Social (SOLO finalizados y cancelados) de los alumnos*/
	public function searchXEdoHistoricoServicioSocial($anio, $periodo)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		if($anio != NULL AND $periodo != NULL)
		{
			$criteria->alias = "ss";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa 
								join pe_planeacion.ss_registro_fechas_servicio_social rf 
								on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "(ss.id_estado_servicio_social = 6 OR ss.id_estado_servicio_social = 7) AND
									rf.id_periodo = '$periodo' AND rf.anio = '$anio' ";

		}elseif($anio != NULL AND $periodo == NULL)
		{
			$criteria->alias = "ss";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa 
								join pe_planeacion.ss_registro_fechas_servicio_social rf 
								on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "(ss.id_estado_servicio_social = 6 OR ss.id_estado_servicio_social = 7) AND rf.anio = '$anio' ";

		}elseif($anio == NULL AND $periodo != NULL)
		{
			$criteria->alias = "ss";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa 
								join pe_planeacion.ss_registro_fechas_servicio_social rf 
								on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "(ss.id_estado_servicio_social = 6 OR ss.id_estado_servicio_social = 7) AND 
									rf.id_periodo = '$periodo' ";

		}/*else{

			$criteria->alias = "ss";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa 
								join pe_planeacion.ss_registro_fechas_servicio_social rf 
								on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "(ss.id_estado_servicio_social = 6 OR ss.id_estado_servicio_social = 7) ";
		}*/

		$criteria->order = " id_servicio_social DESC";

		/*select * from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas p
		on p.id_programa = ss.id_programa
		join pe_planeacion.ss_registro_fechas_servicio_social rf
		on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
		where (ss.id_estado_servicio_social = 6 OR ss.id_estado_servicio_social = 7) AND
		rf.id_periodo = '1' AND rf.anio = 2020;*/

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('"no_ctrl"',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('"nombre_supervisor"',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Filtrar por estados de los Historicos del Servicio Social (SOLO finalizados y cancelados) de los alumnos*/

	/*LISTA DE SERVICIOS SOCIALES COMPLETADOS POR EL ALUMNO POR NO. DE CONTROL */
	public function searchXAlumnoCompletoServicioSocial($no_ctrl)
	{

		$criteria = new CDbCriteria;
		$criteria->select = "*";
		$criteria->condition = " no_ctrl = '$no_ctrl' AND id_estado_servicio_social = 6";
		$criteria->order = "id_servicio_social ASC";

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*LISTA DE SERVICIOS SOCIALES COMPLETADOS POR EL ALUMNO POR NO. DE CONTROL */

	public function searchXProgramaHistoricoServicioSocial($id_programa)
	{
		//SE podria agregar a la condicion el RFC del empleado tambien
		$criteria = new CDbCriteria;
		$criteria->alias = "ss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas p on ss.id_programa = p.id_programa";
		$criteria->condition = " p.id_programa = '$id_programa' AND (p.id_status_programa = 3 OR p.id_status_programa = 4) ";
		$criteria->order = "ss.id_servicio_social ASC";

		/*select * from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas p
		on ss.id_programa = p.id_programa
		where p.id_programa = 33 AND (p.id_status_programa = 3 OR p.id_status_programa = 4)*/

		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('"rfcDirector"',$this->rfcDirector,true);
        $criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('"empresaSupervisorJefe"',$this->empresaSupervisorJefe,true);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"departamentoSupervisorJefe"',$this->departamentoSupervisorJefe,true);
        $criteria->compare('"rfcJefeOficServicioSocial"',$this->rfcJefeOficServicioSocial,true);
		$criteria->compare('"rfcJefeDeptoVinculacion"',$this->rfcJefeDeptoVinculacion,true);
		$criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
}
?>
