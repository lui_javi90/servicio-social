<?php
class SsServiciosSocialesAnteriores_ extends SsServiciosSocialesAnteriores
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_servicio_social_anterior',$this->id_servicio_social_anterior);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('nombre_programa',$this->nombre_programa,true);
		$criteria->compare('id_periodo_programa',$this->id_periodo_programa);
		$criteria->compare('nombre_completo_supervisor',$this->nombre_completo_supervisor,true);
		$criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
		$criteria->compare('horas_liberadas',$this->horas_liberadas,true);
		$criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
		$criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('nombre_empresa',$this->nombre_empresa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchXAlumno($no_ctrl)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition = "no_ctrl='$no_ctrl' ";
		$criteria->order = "id_servicio_social_anterior ASC";

		$criteria->compare('id_servicio_social_anterior',$this->id_servicio_social_anterior);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('nombre_programa',$this->nombre_programa,true);
		$criteria->compare('id_periodo_programa',$this->id_periodo_programa);
		$criteria->compare('nombre_completo_supervisor',$this->nombre_completo_supervisor,true);
		$criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
		$criteria->compare('horas_liberadas',$this->horas_liberadas,true);
		$criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
		$criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
		$criteria->compare('calificacion_servicio_social',$this->calificacion_servicio_social,true);
		$criteria->compare('nombre_empresa',$this->nombre_empresa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
}

?>