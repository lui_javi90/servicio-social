<?php

/**
 * This is the model class for table "ss_tipo_servicio_social".
 *
 * The followings are the available columns in table 'ss_tipo_servicio_social':
 * @property integer $id_tipo_servicio_social
 * @property string $tipo_servicio_social
 *
 * The followings are the available model relations:
 * @property SsProgramas[] $ssProgramases
 */
class SsTipoServicioSocial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_tipo_servicio_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_servicio_social', 'required'),
			array('tipo_servicio_social', 'length', 'max'=>25),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tipo_servicio_social, tipo_servicio_social', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssProgramases' => array(self::HAS_MANY, 'SsProgramas', 'id_tipo_servicio_social'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tipo_servicio_social' => 'Id Tipo Servicio Social',
			'tipo_servicio_social' => 'Tipo de Servicio Social',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
		$criteria->compare('tipo_servicio_social',$this->tipo_servicio_social,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsTipoServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
