<?php
class SsConfiguracion_ extends SsConfiguracion
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_configuracion',$this->id_configuracion);
		$criteria->compare('horas_max_servicio_social',$this->horas_max_servicio_social,true);
		$criteria->compare('porcentaje_creditos_req_servicio_social',$this->porcentaje_creditos_req_servicio_social,true);
		$criteria->compare('fecha_modificacion_configuracion',$this->fecha_modificacion_configuracion,true);
		$criteria->compare('texto_leyenda_pdf_doc',$this->texto_leyenda_pdf_doc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>