<?php

/**
 * This is the model class for table "pe_planeacion.ss_codigos_calidad".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_codigos_calidad':
 * @property integer $id_codigo_calidad
 * @property string $nombre_documento_digital
 * @property string $codigo_calidad
 * @property string $revision
 */
class SsCodigosCalidad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		//return 'ss_codigos_calidad';
		return 'pe_planeacion.ss_codigos_calidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_documento_digital, codigo_calidad, revision', 'required'),
			array('nombre_documento_digital', 'length', 'max'=>75),
			array('codigo_calidad', 'length', 'max'=>25),
			array('revision', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_codigo_calidad, nombre_documento_digital, codigo_calidad, revision', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_codigo_calidad' => 'Id Codigo Calidad',
			'nombre_documento_digital' => 'Nombre del Documento Digital',
			'codigo_calidad' => 'Codigo de Calidad',
			'revision' => 'Revisión',
		);
	}
	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsCodigosCalidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
