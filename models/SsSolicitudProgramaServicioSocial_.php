<?php
class SsSolicitudProgramaServicioSocial_ extends SsSolicitudProgramaServicioSocial
{
    public function search()
	{
	// @todo Please modify the following code to remove attributes that should not be searched.

	$criteria=new CDbCriteria;

	$criteria->compare('id_solicitud_programa',$this->id_solicitud_programa);
    $criteria->compare('no_ctrl',$this->no_ctrl,true);
    $criteria->compare('id_programa',$this->id_programa);
    $criteria->compare('fecha_solicitud_programa',$this->fecha_solicitud_programa,true);
    $criteria->compare('valida_solicitud_alumno',$this->valida_solicitud_alumno,true);
    $criteria->compare('valida_solicitud_supervisor_programa',$this->valida_solicitud_supervisor_programa,true);
    $criteria->compare('id_estado_solicitud_programa_supervisor',$this->id_estado_solicitud_programa_supervisor);
    $criteria->compare('id_estado_solicitud_programa_alumno',$this->id_estado_solicitud_programa_alumno);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*Vista de las solicitudes del alumno que ha realizado */
	public function searchSolicitudesXAlumno($no_ctrl)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias = "spss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas pss on spss.id_programa = pss.id_programa";
		$criteria->condition = "(spss.id_estado_solicitud_programa_alumno != 3 AND spss.id_estado_solicitud_programa_supervisor != 3) AND (spss.id_estado_solicitud_programa_alumno != 4 AND spss.id_estado_solicitud_programa_supervisor != 4)
                            AND (spss.id_estado_solicitud_programa_alumno != 5 AND spss.id_estado_solicitud_programa_supervisor != 5) AND spss.no_ctrl = '$no_ctrl' ";
		$criteria->order = "spss.id_solicitud_programa DESC";

		$criteria->compare('id_solicitud_programa',$this->id_solicitud_programa);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_solicitud_programa',$this->fecha_solicitud_programa,true);
		$criteria->compare('valida_solicitud_alumno',$this->valida_solicitud_alumno,true);
		$criteria->compare('valida_solicitud_supervisor_programa',$this->valida_solicitud_supervisor_programa,true);
		$criteria->compare('id_estado_solicitud_programa_supervisor',$this->id_estado_solicitud_programa_supervisor);
		$criteria->compare('id_estado_solicitud_programa_alumno',$this->id_estado_solicitud_programa_alumno);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	public function searchSolicitudesSupervisor($rfcSupervisor, $id_programa_servicio_social, $id_tipo_programa)
	{
		//echo $rfcSupervisor;
		//die();
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		if($id_tipo_programa == 1){

			$criteria->alias = 'sps';
			$criteria->select = '*';
			$criteria->join = "JOIN pe_planeacion.ss_programas pss ON sps.id_programa = pss.id_programa join pe_planeacion.ss_responsable_programa_interno rpi ON rpi.id_programa = pss.id_programa";
			$criteria->condition = "(rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa_servicio_social' AND pss.id_status_programa = 1)
			AND (sps.id_estado_solicitud_programa_supervisor = 1 AND sps.id_estado_solicitud_programa_alumno = 2)";
		}else
		if($id_tipo_programa == 2){
			$criteria->alias = 'sps';
			$criteria->select = '*';
			$criteria->join = "JOIN pe_planeacion.ss_programas pss ON sps.id_programa = pss.id_programa join pe_planeacion.ss_responsable_programa_externo rpe ON rpe.id_programa = pss.id_programa";
			$criteria->condition = "rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa_servicio_social' AND pss.id_status_programa = 1
			AND (sps.id_estado_solicitud_programa_supervisor = 1 AND sps.id_estado_solicitud_programa_alumno = 2)";
		}

		$criteria->compare('id_solicitud_programa',$this->id_solicitud_programa);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_solicitud_programa',$this->fecha_solicitud_programa,true);
		$criteria->compare('valida_solicitud_alumno',$this->valida_solicitud_alumno,true);
		$criteria->compare('valida_solicitud_supervisor_programa',$this->valida_solicitud_supervisor_programa,true);
		$criteria->compare('id_estado_solicitud_programa_supervisor',$this->id_estado_solicitud_programa_supervisor);
		$criteria->compare('id_estado_solicitud_programa_alumno',$this->id_estado_solicitud_programa_alumno);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	/*Lista las solicitudes al servicio social menos las canceladas */
	public function searchXSolicitud()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias = "spss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas as pss on pss.id_programa = spss.id_programa";
		$criteria->condition = "pss.id_tipo_programa = 2 AND spss.id_estado_solicitud_programa_supervisor != 3 AND spss.id_estado_solicitud_programa_alumno != 3";
		$criteria->order = "spss.id_solicitud_programa ASC";

		$criteria->compare('id_solicitud_programa',$this->id_solicitud_programa);
		$criteria->compare('no_ctrl',$this->no_ctrl,true);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('fecha_solicitud_programa',$this->fecha_solicitud_programa,true);
		$criteria->compare('valida_solicitud_alumno',$this->valida_solicitud_alumno,true);
		$criteria->compare('valida_solicitud_supervisor_programa',$this->valida_solicitud_supervisor_programa,true);
		$criteria->compare('id_estado_solicitud_programa_supervisor',$this->id_estado_solicitud_programa_supervisor);
		$criteria->compare('id_estado_solicitud_programa_alumno',$this->id_estado_solicitud_programa_alumno);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Lista las solicitudes al servicio social menos las canceladas */

  public function searchXSolicitudesEstatuspendienteSupervisores()
  {
    $criteria=new CDbCriteria;
    $criteria->select = "*";
    $criteria->condition = "id_estado_solicitud_programa_supervisor = 1 ";
    $criteria->order = "id_solicitud_programa ASC";

    $criteria->compare('id_solicitud_programa',$this->id_solicitud_programa);
    $criteria->compare('no_ctrl',$this->no_ctrl,true);
    $criteria->compare('id_programa',$this->id_programa);
    $criteria->compare('fecha_solicitud_programa',$this->fecha_solicitud_programa,true);
    $criteria->compare('valida_solicitud_alumno',$this->valida_solicitud_alumno,true);
    $criteria->compare('valida_solicitud_supervisor_programa',$this->valida_solicitud_supervisor_programa,true);
    $criteria->compare('id_estado_solicitud_programa_supervisor',$this->id_estado_solicitud_programa_supervisor);
    $criteria->compare('id_estado_solicitud_programa_alumno',$this->id_estado_solicitud_programa_alumno);

    return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
      'pagination'=>array(
        'pageSize'=>50,//El numero de registros que se muestran en el CGridView
      ),
    ));
  }
}
?>
