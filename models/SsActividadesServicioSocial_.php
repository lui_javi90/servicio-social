<?php
class SsActividadesServicioSocial_ extends SsActividadesServicioSocial
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_actividad_servicio_social',$this->id_actividad_servicio_social);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('id_mes',$this->id_mes);
		$criteria->compare('actividad_mensual',$this->actividad_mensual,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*Filtrar actividades por id_servicio_social (tambien podria ser no_ctrl) */
	public function searchXActividadesAlumno($id_servicio_social)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias = "ass";
		$criteria->select = "ass.id_actividad_servicio_social, ass.id_servicio_social, ass.id_mes, ass.actividad_mensual";
		$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = ass.id_servicio_social";
		$criteria->condition = "(ss.id_estado_servicio_social < 6 AND (ss.id_estado_servicio_social != 7 or ss.id_estado_servicio_social != 8)) AND ss.id_servicio_social = '$id_servicio_social'";
		$criteria->order = "ass.id_actividad_servicio_social ASC";

		$criteria->compare('id_actividad_servicio_social',$this->id_actividad_servicio_social);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('id_mes',$this->id_mes);
		$criteria->compare('actividad_mensual',$this->actividad_mensual,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Filtrar actividades por id_servicio_social (tambien podria ser no_ctrl) */
}
?>
