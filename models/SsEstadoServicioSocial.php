<?php

/**
 * This is the model class for table "ss_estado_servicio_social".
 *
 * The followings are the available columns in table 'ss_estado_servicio_social':
 * @property integer $id_estado_servicio_social
 * @property string $estado
 * @property string $descripcion_estado
 *
 * The followings are the available model relations:
 * @property SsServicioSocial[] $ssServicioSocials
 */
class SsEstadoServicioSocial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_estado_servicio_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estado, descripcion_estado', 'required'),
			array('estado', 'length', 'max'=>20),
			array('descripcion_estado', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_estado_servicio_social, estado, descripcion_estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssServicioSocials' => array(self::HAS_MANY, 'SsServicioSocial', 'id_estado_servicio_social'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_estado_servicio_social' => 'Id Estado Servicio Social',
			'estado' => 'Estado Servicio Social',
			'descripcion_estado' => 'Descripción del Estado',
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsEstadoServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
