<?php
class SsProgramas_ extends SsProgramas
{
    public $nombreSupervisor;
    public $apePaterno;
    public $apeMaterno;
    public $rfc;
    //Para filtro por Periodo y Año
    public $periodo;
    public $anio;

    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
    }
    
    public function searchXListaHistoricoProgramasSupervisor($rfcEmpleado, $anio, $periodo)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "p";
        $criteria->select = "*";
        

        if($anio != NULL AND $periodo != NULL)
        {
            $criteria->join = "join pe_planeacion.ss_responsable_programa_interno rpi on rpi.id_programa = p.id_programa
            join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
            $criteria->condition = " rpi.\"rfcEmpleado\" = '$rfcEmpleado' AND rf.id_periodo = '$periodo' AND rf.anio = '$anio' AND 
                                    (p.id_status_programa = 3 OR p.id_status_programa = 4)";
            
        }elseif($anio != NULL AND $periodo == NULL)
        {
            $criteria->join = "join pe_planeacion.ss_responsable_programa_interno rpi on rpi.id_programa = p.id_programa
            join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
            $criteria->condition = " rpi.\"rfcEmpleado\" = '$rfcEmpleado' AND rf.anio = '$anio' AND 
                                    (p.id_status_programa = 3 OR p.id_status_programa = 4)";
        }elseif($anio == NULL AND $periodo != NULL)
        {
            $criteria->join = "join pe_planeacion.ss_responsable_programa_interno rpi on rpi.id_programa = p.id_programa
            join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
            $criteria->condition = " rpi.\"rfcEmpleado\" = '$rfcEmpleado' AND rf.periodo = '$periodo' AND 
                                    (p.id_status_programa = 3 OR p.id_status_programa = 4)";

        }else{

            $criteria->join = "join pe_planeacion.ss_responsable_programa_interno rpi on rpi.id_programa = p.id_programa";
            $criteria->condition = " rpi.\"rfcEmpleado\" = '$rfcEmpleado' AND (p.id_status_programa = 3 OR p.id_status_programa = 4)";
        }


        $criteria->order = "p.id_programa DESC";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
    }

	/*UNICAMENTE MUESTRA EL PROGRAMA DEL SUPERVISOR ACTUAL, SIN STATUS BAJA(2) O FINALIZAR(3)*/
	public function searchProgramaSupervisor($rfcsupervisor, $id_tipo_programa)
	{
        // @todo Please modify the following code to remove attributes that should not be searched.
        //echo "val:".$id_tipo_programa;
        //echo $rfcsupervisor;
        //die;

        $criteria = new CDbCriteria;
        $criteria->condition = " (id_status_programa = 1 OR id_status_programa = 8)";
        
        if($id_tipo_programa == 1)
        {
            $criteria->alias = "pss";
            $criteria->select = "*";
            $criteria->join = "join pe_planeacion.ss_responsable_programa_interno rpi on rpi.id_programa = pss.id_programa";
            $criteria->condition = "rpi.\"rfcEmpleado\" = '$rfcsupervisor' AND (pss.id_status_programa = 1 OR pss.id_status_programa = 8)";
        }else
        if($id_tipo_programa == 2)
        {
            $criteria->alias = "pss";
            $criteria->select = "*";
            $criteria->join = "join pe_planeacion.ss_responsable_programa_externo rpe on rpe.id_programa = pss.id_programa";
            $criteria->condition = "rpe.\"rfcSupervisor\" = '$rfcsupervisor' AND (pss.id_status_programa = 1 OR pss.id_status_programa = 8)";

        }else{

            //$criteria->join = "join pe_planeacion.ss_responsable_programa_externo rpe on rpe.id_programa = pss.id_programa";
            $criteria->condition = " id_programa = 0 ";
        }

        $criteria->order = " id_programa ASC";


		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*UNICAMENTE MUESTRA EL PROGRAMA DEL SUPERVISOR ACTUAL, SIN STATUS BAJA(2) O FINALIZAR(3)*/

	 /*MUESTRA TODOS LOS PROGRAMAS AL DEPTO. MENOS LOS ELIMINADOS*/
	public function searchProgramasSinEliminar()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition = "id_status_programa > 0";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
     /*MUESTRA TODOS LOS PROGRAMAS AL DEPTO. MENOS LOS ELIMINADOS*/
     
     /*MUESTRA TODOS LOS PROGRAMAS PENDIENTE AL DEPTO.*/
	public function searchProgramasPendientes()
	{
        // @todo Please modify the following code to remove attributes that should not be searched.
        
		$criteria = new CDbCriteria;
		$criteria->alias = "pss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_registro_fechas_servicio_social as rfss on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial";
		$criteria->condition = "pss.id_status_programa = 6 AND rfss.ssocial_actual = true";
    	$criteria->order = "id_programa ASC";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*MUESTRA TODOS LOS PROGRAMAS PENDIENTES AL DEPTO.*/

	/*MUESTRA TODOS LOS PROGRAMAS ACTIVOS AL DEPTO. Y SE FILTRA POR NOMBRE, APELLIDOS DEL SUPERVISOR DEL PROGRAMA*/
	public function searchProgramasActivos($nombreSupervisor, $apePaterno, $apeMaterno)
	{
        
		$criteria = new CDbCriteria;
		$criteria->alias = "pss";
        $criteria->select = "*";
        $criteria->join = "join pe_planeacion.ss_registro_fechas_servicio_social rfss 
                            on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial";
        
        //$criteria->condition = "pss.id_status_programa = 1 AND rfss.ssocial_actual = true";
        $criteria->condition = " pss.id_status_programa != 3 AND pss.id_status_programa = 4";
       
        
        if($nombreSupervisor != NULL AND $apePaterno == NULL AND $apeMaterno == NULL) //Filtra por nombre
        {
            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true and 
                                    spt.nombre_supervisor = '$nombreSupervisor' ";
            //$criteria->order = "pss.id_programa ASC";
            
        }elseif($nombreSupervisor != NULL AND $apePaterno != NULL AND $apeMaterno == NULL)
        { //Filtrar por Nombre y apellido paterno
           
            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true and 
                                    (spt.nombre_supervisor = '$nombreSupervisor' AND 
                                    spt.apellido_paterno = '$apePaterno')";
            //$criteria->order = "pss.id_programa ASC";
            
        }elseif($nombreSupervisor != NULL AND $apePaterno == NULL AND $apeMaterno != NULL)
        { //Filtrar por Nombre y apellido materno
           
            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true and 
                                    (spt.nombre_supervisor = '$nombreSupervisor' AND 
                                    spt.apellido_materno = '$apeMaterno')";
            //$criteria->order = "pss.id_programa ASC";
            
        }elseif($nombreSupervisor != NULL AND $apePaterno != NULL AND $apeMaterno != NULL)
        { //Filtrar por nombre, apellido paterno y materno
            
            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true and 
                                    (spt.nombre_supervisor = '$nombreSupervisor' AND
                                    spt.apellido_paterno = '$apePaterno' AND
                                    spt.apellido_materno = '$apeMaterno')";
            //$criteria->order = "pss.id_programa ASC";

        }elseif($nombreSupervisor == NULL AND $apePaterno != NULL AND $apeMaterno == NULL)
        { //Filtra por apellido paterno

            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true AND
                                    spt.apellido_paterno = '$apePaterno' ";
            //$criteria->order = "pss.id_programa ASC";

        }elseif($nombreSupervisor == NULL AND $apePaterno == NULL AND $apeMaterno != NULL)
        { //Filtrar por apellido materno

            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true AND
                                    spt.apellido_materno = '$apeMaterno' ";
            //$criteria->order = "pss.id_programa ASC";

        }elseif($nombreSupervisor == NULL AND $apePaterno != NULL AND $apeMaterno != NULL)
        { //Filtrar por apellido paterno y materno

            $criteria->join = "join pe_planeacion.ss_supervisores_programas_todos spt on spt.id_programa = pss.id_programa";
            $criteria->condition = "pss.id_status_programa = 1 and spt.programa_vigente = true AND
                                    (spt.apellido_paterno = '$apePaterno' AND 
                                    spt.apellido_materno = '$apeMaterno')";
            //$criteria->order = "pss.id_programa ASC";
        }

        $criteria->order = "pss.id_programa ASC";
		
		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*MUESTRA TODOS LOS PROGRAMAS ACTIVOS AL DEPTO. Y SE FILTRA POR NOMBRE, APELLIDOS DEL SUPERVISOR DEL PROGRAMA*/

	/*Muestra los Programas Extratemporales UNICAMENTE*/
	public function searchProgramasExtratemporales()
	{
		$criteria=new CDbCriteria;
		$criteria->condition = "id_status_programa > 0 AND id_periodo_programa = 3";
        $criteria->order = "id_programa ASC";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
    /*Muestra los Programas Extratemporales UNICAMENTE*/
    
    /*HISTORICO DE PROGRAMAS TANTO FINALIZADOS COMO CANCELADOS */
    public function searchHistoricoProgramas($anio, $periodo)
    {
        $criteria = new CDbCriteria;
        
        

        if($anio != NULL AND $periodo != NULL)
        {
            $criteria->alias = "p";
            $criteria->select = "*";
            $criteria->join = " join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
            $criteria->condition = " (id_status_programa = 3 OR id_status_programa = 4) AND rf.id_periodo = '$periodo' AND rf.anio = '$anio' ";
            
        }elseif($anio != NULL AND $periodo == NULL)
        {
            $criteria->alias = "p";
            $criteria->select = "*";
            $criteria->join = " join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
            $criteria->condition = " (id_status_programa = 3 OR id_status_programa = 4) AND rf.anio = '$anio' ";

        }elseif($anio == NULL AND $periodo != NULL)
        {
            $criteria->alias = "p";
            $criteria->select = "*";
            $criteria->join = " join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
            $criteria->condition = " (id_status_programa = 3 OR id_status_programa = 4) AND rf.id_periodo = '$periodo' ";

        }else
        {
            $criteria->condition = " id_status_programa = 3 OR id_status_programa = 4 ";
        }

        $criteria->order = "id_programa DESC";
        /*select * from pe_planeacion.ss_programas p
        join pe_planeacion.ss_registro_fechas_servicio_social rf
        on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
        where (p.id_status_programa = 3 OR p.id_status_programa = 4) AND rf.id_periodo = '1' AND rf.anio = 2020*/

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
    }
    /*HISTORICO DE PROGRAMAS TANTO FINALIZADOS COMO CANCELADOS */

    /*PROGRAMAS SIN ASESOR ASIGNADO */
    public function searchProgramasSinSupervisor()
    {
        $criteria=new CDbCriteria;
		$criteria->condition = "id_status_programa = 7";
        $criteria->order = "id_programa ASC";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
    }
    /*PROGRAMAS SIN ASESOR ASIGNADO */

    /*PROGRAMAS SIN HORARIO ASIGNADO */
    public function searchProgramasSinHorario()
    {
        $criteria=new CDbCriteria;
		$criteria->condition = "id_status_programa = 8";
        $criteria->order = "id_programa ASC";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
    }
    /*PROGRAMAS SIN HORARIO ASIGNADO */

    public function searchListaProgramasTodos()
    {
        $criteria = new CDbCriteria;

        /*if($rfc != NULL)
        {
            $criteria->alias = "p";
            $criteria->select = "*"
            $criteria->join = " join pe_planeacion.ss_responsable_programa_interno rpi
                                on rpi.id_programa = p.id_programa"
		    $criteria->condition = " rpi.\"rfcEmpleado\" p.id_status_programa != 3 AND p.id_status_programa != 4";
            
        }else{

            
            
        }*/

        $criteria->condition = " id_status_programa != 3 AND id_status_programa != 4";
        $criteria->order = "id_programa ASC";

		$criteria->compare('id_programa',$this->id_programa);
        $criteria->compare('nombre_programa',$this->nombre_programa,true);
        $criteria->compare('lugar_realizacion_programa',$this->lugar_realizacion_programa,true);
        $criteria->compare('horas_totales',$this->horas_totales,true);
        $criteria->compare('id_periodo_programa',$this->id_periodo_programa);
        $criteria->compare('numero_estudiantes_solicitados',$this->numero_estudiantes_solicitados,true);
        $criteria->compare('descripcion_objetivo_programa',$this->descripcion_objetivo_programa,true);
        $criteria->compare('id_tipo_servicio_social',$this->id_tipo_servicio_social);
        $criteria->compare('impacto_social_esperado',$this->impacto_social_esperado,true);
        $criteria->compare('beneficiarios_programa',$this->beneficiarios_programa,true);
        $criteria->compare('actividades_especificas_realizar',$this->actividades_especificas_realizar,true);
        $criteria->compare('mecanismos_supervision',$this->mecanismos_supervision,true);
        $criteria->compare('perfil_estudiante_requerido',$this->perfil_estudiante_requerido,true);
        $criteria->compare('id_apoyo_economico_prestador',$this->id_apoyo_economico_prestador);
        $criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
        $criteria->compare('apoyo_economico',$this->apoyo_economico,true);
        $criteria->compare('fecha_registro_programa',$this->fecha_registro_programa,true);
        $criteria->compare('fecha_modificacion_programa',$this->fecha_modificacion_programa,true);
        $criteria->compare('id_recibe_capacitacion',$this->id_recibe_capacitacion);
        $criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
        $criteria->compare('lugares_disponibles',$this->lugares_disponibles,true);
        $criteria->compare('id_tipo_programa',$this->id_tipo_programa);
        $criteria->compare('id_status_programa',$this->id_status_programa);
        $criteria->compare('fecha_inicio_programa',$this->fecha_inicio_programa,true);
        $criteria->compare('fecha_fin_programa',$this->fecha_fin_programa,true);
        $criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('lugares_ocupados',$this->lugares_ocupados,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
    }
}
?>