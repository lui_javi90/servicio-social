<?php
class SsClasificacionArea_ extends SsClasificacionArea
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "id_clasificacion_area_servicio_social ASC";

		$criteria->compare('id_clasificacion_area_servicio_social',$this->id_clasificacion_area_servicio_social);
		$criteria->compare('clasificacion_area_servicio_social',$this->clasificacion_area_servicio_social,true);
		$criteria->compare('status_area',$this->status_area);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>