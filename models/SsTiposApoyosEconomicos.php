<?php

/**
 * This is the model class for table "ss_tipos_apoyos_economicos".
 *
 * The followings are the available columns in table 'ss_tipos_apoyos_economicos':
 * @property integer $id_tipo_apoyo_economico
 * @property integer $id_apoyo_economico_prestador_servicio_social
 * @property string $tipo_apoyo_economico
 *
 * The followings are the available model relations:
 * @property SsProgramas[] $ssProgramases
 * @property SsApoyosEconomicos $idApoyoEconomicoPrestadorServicioSocial
 */
class SsTiposApoyosEconomicos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_tipos_apoyos_economicos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_apoyo_economico_prestador_servicio_social, tipo_apoyo_economico', 'required'),
			array('id_apoyo_economico_prestador_servicio_social', 'numerical', 'integerOnly'=>true),
			array('tipo_apoyo_economico', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tipo_apoyo_economico, id_apoyo_economico_prestador_servicio_social, tipo_apoyo_economico', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssProgramases' => array(self::HAS_MANY, 'SsProgramas', 'id_tipo_apoyo_economico'),
			'idApoyoEconomicoPrestadorServicioSocial' => array(self::BELONGS_TO, 'SsApoyosEconomicos', 'id_apoyo_economico_prestador_servicio_social'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tipo_apoyo_economico' => 'Id Tipo Apoyo Economico',
			'id_apoyo_economico_prestador_servicio_social' => 'Apoyo Económico al Alumno',
			'tipo_apoyo_economico' => 'Tipo de Apoyo Económico',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tipo_apoyo_economico',$this->id_tipo_apoyo_economico);
		$criteria->compare('id_apoyo_economico_prestador_servicio_social',$this->id_apoyo_economico_prestador_servicio_social);
		$criteria->compare('tipo_apoyo_economico',$this->tipo_apoyo_economico,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsTiposApoyosEconomicos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
