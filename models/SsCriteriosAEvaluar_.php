<?php
class SsCriteriosAEvaluar_ extends SsCriteriosAEvaluar
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "id_criterio ASC";

		$criteria->compare('id_criterio',$this->id_criterio);
        $criteria->compare('descripcion_criterio',$this->descripcion_criterio,true);
        $criteria->compare('valor_a',$this->valor_a,true);
        $criteria->compare('id_tipo_criterio',$this->id_tipo_criterio);
        $criteria->compare('status_criterio',$this->status_criterio);
        $criteria->compare('posicion_criterio',$this->posicion_criterio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	public function searchCriteriosActivados()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition = "status_criterio = true";
		$criteria->order = "id_criterio ASC";

		$criteria->compare('id_criterio',$this->id_criterio);
        $criteria->compare('descripcion_criterio',$this->descripcion_criterio,true);
        $criteria->compare('valor_a',$this->valor_a,true);
        $criteria->compare('id_tipo_criterio',$this->id_tipo_criterio);
        $criteria->compare('status_criterio',$this->status_criterio);
        $criteria->compare('posicion_criterio',$this->posicion_criterio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	//Muestra unicamente los criterios del supervisor y que estan ACTIVADOS
	public function searchCriteriosXSupervisor()
	{
		$criteria=new CDbCriteria;
		$criteria->condition = "id_tipo_criterio = 1 AND status_criterio = true";
		$criteria->order = "posicion_criterio ASC";

		$criteria->compare('id_criterio',$this->id_criterio);
        $criteria->compare('descripcion_criterio',$this->descripcion_criterio,true);
        $criteria->compare('valor_a',$this->valor_a,true);
        $criteria->compare('id_tipo_criterio',$this->id_tipo_criterio);
        $criteria->compare('status_criterio',$this->status_criterio);
        $criteria->compare('posicion_criterio',$this->posicion_criterio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	//Muestra unicamente los criterios del supervisor y que estan ACTIVADOS

	//Muestra unicamente los criterios del Admin y que estan ACTIVADOS
	public function searchCriteriosXAdmin()
	{
		$criteria=new CDbCriteria;
		$criteria->condition = "id_tipo_criterio = 2 AND status_criterio = true";
		$criteria->order = "posicion_criterio ASC";

		$criteria->compare('id_criterio',$this->id_criterio);
        $criteria->compare('descripcion_criterio',$this->descripcion_criterio,true);
        $criteria->compare('valor_a',$this->valor_a,true);
        $criteria->compare('id_tipo_criterio',$this->id_tipo_criterio);
        $criteria->compare('status_criterio',$this->status_criterio);
        $criteria->compare('posicion_criterio',$this->posicion_criterio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	//Muestra unicamente los criterios del Admin y que estan ACTIVADOS
}
?>