<?php
class SsHorarioDiasHabilesProgramas_ extends SsHorarioDiasHabilesProgramas
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_horario',$this->id_horario);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('id_dia_semana',$this->id_dia_semana);
		$criteria->compare('hora_inicio',$this->hora_inicio,true);
		$criteria->compare('hora_fin',$this->hora_fin,true);
		$criteria->compare('horas_totales',$this->horas_totales,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*SOLO SE NECESITA UN REGISTRO DEL HORARIO DEL PROGRAMA PARA HACER LA CONSULTA Y MOSTRAR COMPLETO EL HORARIO DEL PROGRAMA*/
	public function searchNoRepetidosXPrograma()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->distinct = true;
		$criteria->alias = "hdhp";
		$criteria->select = 'hdhp.id_programa';
		$criteria->join = 'join pe_planeacion.ss_programas pss ON hdhp.id_programa = pss.id_programa';
		$criteria->condition = "pss.id_status_programa =1";

		$criteria->compare('id_horario',$this->id_horario);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('id_dia_semana',$this->id_dia_semana);
		$criteria->compare('hora_inicio',$this->hora_inicio,true);
		$criteria->compare('hora_fin',$this->hora_fin,true);
		$criteria->compare('horas_totales',$this->horas_totales,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	/*PARA FILTRAR POR HORARIO DE CADA PROGRAMA Y HACER LAS EDICIONES AL HORARIO */
	public function searchEditarHorarioPrograma($id_programa_servicio_social)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "id_dia_semana ASC";

		$criteria->compare('id_horario',$this->id_horario);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('id_dia_semana',$this->id_dia_semana);
		$criteria->compare('hora_inicio',$this->hora_inicio,true);
		$criteria->compare('hora_fin',$this->hora_fin,true);
		$criteria->compare('horas_totales',$this->horas_totales,true);

		$criteria->compare('id_programa',$id_programa_servicio_social);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	public function searchXPrograma($id_programa)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->condition = " id_programa = '$id_programa' ";
		$criteria->order = "id_dia_semana ASC";

		$criteria->compare('id_horario',$this->id_horario);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('id_dia_semana',$this->id_dia_semana);
		$criteria->compare('hora_inicio',$this->hora_inicio,true);
		$criteria->compare('hora_fin',$this->hora_fin,true);
		$criteria->compare('horas_totales',$this->horas_totales,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}
}

?>