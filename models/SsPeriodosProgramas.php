<?php

/**
 * This is the model class for table "ss_periodos_programas".
 *
 * The followings are the available columns in table 'ss_periodos_programas':
 * @property integer $id_periodo_programa
 * @property string $periodo_programa
 *
 * The followings are the available model relations:
 * @property SsServicioSocial[] $ssServicioSocials
 * @property SsProgramas[] $ssProgramases
 */
class SsPeriodosProgramas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_periodos_programas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('periodo_programa', 'required'),
			array('periodo_programa', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_periodo_programa, periodo_programa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssServicioSocials' => array(self::HAS_MANY, 'SsServicioSocial', 'id_periodo_servicio_social'),
			'ssProgramases' => array(self::HAS_MANY, 'SsProgramas', 'id_periodo_programa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_periodo_programa' => 'Id Periodo Programa',
			'periodo_programa' => 'Periodo del Programa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "id_periodo_programa ASC";

		$criteria->compare('id_periodo_programa',$this->id_periodo_programa);
		$criteria->compare('periodo_programa',$this->periodo_programa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsPeriodosProgramas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
