<?php

/**
 * This is the model class for table "pe_planeacion.ss_configuracion".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_configuracion':
 * @property integer $id_configuracion
 * @property string $horas_max_servicio_social
 * @property string $porcentaje_creditos_req_servicio_social
 * @property string $fecha_modificacion_configuracion
 * @property string $texto_leyenda_pdf_doc
 */
class SsConfiguracion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		//return 'ss_configuracion';
		return 'pe_planeacion.ss_configuracion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('horas_max_servicio_social, porcentaje_creditos_req_servicio_social', 'required'),
            array('horas_max_servicio_social', 'length', 'max'=>3),
            array('porcentaje_creditos_req_servicio_social', 'length', 'max'=>2),
            array('texto_leyenda_pdf_doc', 'length', 'max'=>200),
            array('fecha_modificacion_configuracion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_configuracion, horas_max_servicio_social, porcentaje_creditos_req_servicio_social, fecha_modificacion_configuracion, texto_leyenda_pdf_doc', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_configuracion' => 'Id Configuracion',
			'horas_max_servicio_social' => 'Horas Máximo',
			'porcentaje_creditos_req_servicio_social' => 'Porcentaje Creditos Requeridos',
			'fecha_modificacion_configuracion' => 'Fecha Modificación',
			'texto_leyenda_pdf_doc' => 'Texto Leyenda PDF Doc',
		);
	}
	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsConfiguracion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
