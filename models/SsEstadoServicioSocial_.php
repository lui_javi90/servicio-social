<?php
class SsEstadoServicioSocial_ extends SsEstadoServicioSocial
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "id_estado_servicio_social ASC";

		$criteria->compare('id_estado_servicio_social',$this->id_estado_servicio_social);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('descripcion_estado',$this->descripcion_estado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
?>