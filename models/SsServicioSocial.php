<?php

/**
 * This is the model class for table "pe_planeacion.ss_servicio_social".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_servicio_social':
 * @property integer $id_servicio_social
 * @property string $no_ctrl
 * @property integer $id_estado_servicio_social
 * @property integer $id_programa
 * @property string $fecha_registro
 * @property string $fecha_modificacion
 * @property string $calificacion_servicio_social
 * @property string $rfcDirector
 * @property string $rfcSupervisor
 * @property string $cargo_supervisor
 * @property string $empresaSupervisorJefe
 * @property string $nombre_jefe_depto
 * @property string $departamentoSupervisorJefe
 * @property string $rfcJefeOficServicioSocial
 * @property string $rfcJefeDeptoVinculacion
 * @property string $nombre_supervisor
 *
 * The followings are the available model relations:
 * @property SsReportesBimestral[] $ssReportesBimestrals
 * @property SsActividadesServicioSocial[] $ssActividadesServicioSocials
 * @property SsObservacionesServicioSocial[] $ssObservacionesServicioSocials
 * @property EDatosAlumno $noCtrl
 * @property SsEstadoServicioSocial $idEstadoServicioSocial
 * @property SsProgramas $idPrograma
 * @property SsSolicitudProgramaServicioSocial $idServicioSocial
 */
class SsServicioSocial extends CActiveRecord
{
	//Para filtrar
	public $anio;
	public $periodo;
	//public $id_estado_servicio;

	public function tableName()
	{
		return 'pe_planeacion.ss_servicio_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
            array('no_ctrl, id_estado_servicio_social, id_programa', 'required'),
            array('id_estado_servicio_social, id_programa', 'numerical', 'integerOnly'=>true),
            array('no_ctrl', 'length', 'max'=>8),
            array('calificacion_servicio_social', 'length', 'max'=>4),
            array('rfcDirector, rfcSupervisor, rfcJefeOficServicioSocial, rfcJefeDeptoVinculacion', 'length', 'max'=>13),
            array('cargo_supervisor', 'length', 'max'=>150),
            array('empresaSupervisorJefe, nombre_jefe_depto, nombre_supervisor', 'length', 'max'=>200),
            array('departamentoSupervisorJefe', 'length', 'max'=>250),
            array('fecha_registro, fecha_modificacion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_servicio_social, no_ctrl, id_estado_servicio_social, id_programa, fecha_registro, fecha_modificacion, calificacion_servicio_social, rfcDirector, rfcSupervisor, cargo_supervisor, empresaSupervisorJefe, nombre_jefe_depto, departamentoSupervisorJefe, rfcJefeOficServicioSocial, rfcJefeDeptoVinculacion, nombre_supervisor, anio, periodo', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
				// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ssReportesBimestrals' => array(self::HAS_MANY, 'SsReportesBimestral', 'id_servicio_social'),
            'ssActividadesServicioSocials' => array(self::HAS_MANY, 'SsActividadesServicioSocial', 'id_servicio_social'),
            'ssObservacionesServicioSocials' => array(self::HAS_MANY, 'SsObservacionesServicioSocial', 'id_servicio_social'),
            'noCtrl' => array(self::BELONGS_TO, 'EDatosAlumno', 'no_ctrl'),
            'idEstadoServicioSocial' => array(self::BELONGS_TO, 'SsEstadoServicioSocial', 'id_estado_servicio_social'),
            'idPrograma' => array(self::BELONGS_TO, 'SsProgramas', 'id_programa'),
            'idServicioSocial' => array(self::BELONGS_TO, 'SsSolicitudProgramaServicioSocial', 'id_servicio_social'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id_servicio_social' => 'Id Servicio Social',
            'no_ctrl' => 'No. de Control',
            'id_estado_servicio_social' => 'Estado del Servicio Social',
            'id_programa' => 'Programa',
            'fecha_registro' => 'Fecha de Registro',
            'fecha_modificacion' => 'Fecha de Modificación',
			'calificacion_servicio_social' => 'Calificación del Servicio Social',
			'rfcDirector' => 'Rfc Director',
            'rfcSupervisor' => 'Rfc Supervisor',
            'cargo_supervisor' => 'Cargo Supervisor',
            'empresaSupervisorJefe' => 'Empresa Supervisor Jefe',
            'nombre_jefe_depto' => 'Nombre Jefe Depto',
            'departamentoSupervisorJefe' => 'Departamento Supervisor Jefe',
            'rfcJefeOficServicioSocial' => 'Rfc Jefe Ofic Servicio Social',
			'rfcJefeDeptoVinculacion' => 'Rfc Jefe Depto Vinculacion',
			'nombre_supervisor' => 'Nombre del Supervisor',
        );
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
