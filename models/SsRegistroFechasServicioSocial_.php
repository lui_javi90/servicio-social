<?php
class SsRegistroFechasServicioSocial_ extends SsRegistroFechasServicioSocial
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "ssocial_actual DESC";

		$criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('fecha_limite_inscripcion',$this->fecha_limite_inscripcion,true);
        $criteria->compare('fecha_inicio_ssocial',$this->fecha_inicio_ssocial,true);
        $criteria->compare('ssocial_actual',$this->ssocial_actual);
        $criteria->compare('anio',$this->anio);
		$criteria->compare('id_periodo',$this->id_periodo,true);
		$criteria->compare('cambiar_fech_limite_insc',$this->cambiar_fech_limite_insc);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
	}
}


?>