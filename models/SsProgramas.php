<?php

/**
 * This is the model class for table "pe_planeacion.ss_programas".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_programas':
 * @property integer $id_programa
 * @property string $nombre_programa
 * @property string $lugar_realizacion_programa
 * @property string $horas_totales
 * @property integer $id_periodo_programa
 * @property string $numero_estudiantes_solicitados
 * @property string $descripcion_objetivo_programa
 * @property integer $id_tipo_servicio_social
 * @property string $impacto_social_esperado
 * @property string $beneficiarios_programa
 * @property string $actividades_especificas_realizar
 * @property string $mecanismos_supervision
 * @property string $perfil_estudiante_requerido
 * @property integer $id_apoyo_economico_prestador
 * @property integer $id_tipo_apoyo_economico
 * @property string $apoyo_economico
 * @property string $fecha_registro_programa
 * @property string $fecha_modificacion_programa
 * @property integer $id_recibe_capacitacion
 * @property integer $id_clasificacion_area_servicio_social
 * @property string $lugares_disponibles
 * @property integer $id_tipo_programa
 * @property integer $id_status_programa
 * @property string $fecha_inicio_programa
 * @property string $fecha_fin_programa
 * @property integer $id_registro_fechas_ssocial
 * @property string $lugares_ocupados
 * @property integer $id_unidad_receptora
 *
 * The followings are the available model relations:
 * @property SsSolicitudProgramaServicioSocial[] $ssSolicitudProgramaServicioSocials
 * @property SsServicioSocial[] $ssServicioSocials
 * @property SsPeriodosProgramas $idPeriodoPrograma
 * @property SsTipoServicioSocial $idTipoServicioSocial
 * @property SsApoyosEconomicos $idApoyoEconomicoPrestador
 * @property SsTiposApoyosEconomicos $idTipoApoyoEconomico
 * @property SsClasificacionArea $idClasificacionAreaServicioSocial
 * @property SsTipoStatus $idStatusPrograma
 * @property SsRegistroFechasServicioSocial $idRegistroFechasSsocial
 * @property SsUnidadesReceptoras $idUnidadReceptora
 * @property SsResponsableProgramaInterno[] $ssResponsableProgramaInternos
 * @property SsHorarioDiasHabilesProgramas[] $ssHorarioDiasHabilesProgramases
 * @property SsResponsableProgramaExterno[] $ssResponsableProgramaExternos
 */
class SsProgramas extends CActiveRecord
{
	public $nombreSupervisor;
	public $apePaterno;
	public $apeMaterno;
	public $rfc;

	public $anio;
	public $periodo;

	public function tableName()
	{
		return 'pe_planeacion.ss_programas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre_programa, lugar_realizacion_programa, horas_totales, id_periodo_programa, numero_estudiantes_solicitados, descripcion_objetivo_programa, id_tipo_servicio_social, impacto_social_esperado, beneficiarios_programa, actividades_especificas_realizar, mecanismos_supervision, perfil_estudiante_requerido, id_apoyo_economico_prestador, id_tipo_apoyo_economico, apoyo_economico, id_recibe_capacitacion, id_clasificacion_area_servicio_social, lugares_disponibles, id_tipo_programa, id_status_programa, fecha_inicio_programa, id_registro_fechas_ssocial, lugares_ocupados', 'required'),
            array('id_periodo_programa, id_tipo_servicio_social, id_apoyo_economico_prestador, id_tipo_apoyo_economico, id_recibe_capacitacion, id_clasificacion_area_servicio_social, id_tipo_programa, id_status_programa, id_registro_fechas_ssocial, id_unidad_receptora', 'numerical', 'integerOnly'=>true),
            array('nombre_programa', 'length', 'max'=>150),
            array('lugar_realizacion_programa, mecanismos_supervision', 'length', 'max'=>200),
            array('horas_totales', 'length', 'max'=>3),
            array('numero_estudiantes_solicitados, lugares_disponibles, lugares_ocupados', 'length', 'max'=>2),
            array('descripcion_objetivo_programa', 'length', 'max'=>400),
            array('impacto_social_esperado, beneficiarios_programa', 'length', 'max'=>300),
            array('actividades_especificas_realizar, perfil_estudiante_requerido', 'length', 'max'=>500),
            array('apoyo_economico', 'length', 'max'=>100),
            array('fecha_registro_programa, fecha_modificacion_programa, fecha_fin_programa', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_programa, nombre_programa, lugar_realizacion_programa, horas_totales, id_periodo_programa, numero_estudiantes_solicitados, descripcion_objetivo_programa, id_tipo_servicio_social, impacto_social_esperado, beneficiarios_programa, actividades_especificas_realizar, mecanismos_supervision, perfil_estudiante_requerido, id_apoyo_economico_prestador, id_tipo_apoyo_economico, apoyo_economico, fecha_registro_programa, fecha_modificacion_programa, id_recibe_capacitacion, id_clasificacion_area_servicio_social, lugares_disponibles, id_tipo_programa, id_status_programa, fecha_inicio_programa, fecha_fin_programa, id_registro_fechas_ssocial, lugares_ocupados, id_unidad_receptora, nombreSupervisor, apePaterno, apeMaterno, anio, periodo, rfc', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'ssSolicitudProgramaServicioSocials' => array(self::HAS_MANY, 'SsSolicitudProgramaServicioSocial', 'id_programa'),
            'ssServicioSocials' => array(self::HAS_MANY, 'SsServicioSocial', 'id_programa'),
            'idPeriodoPrograma' => array(self::BELONGS_TO, 'SsPeriodosProgramas', 'id_periodo_programa'),
            'idTipoServicioSocial' => array(self::BELONGS_TO, 'SsTipoServicioSocial', 'id_tipo_servicio_social'),
            'idApoyoEconomicoPrestador' => array(self::BELONGS_TO, 'SsApoyosEconomicos', 'id_apoyo_economico_prestador'),
            'idTipoApoyoEconomico' => array(self::BELONGS_TO, 'SsTiposApoyosEconomicos', 'id_tipo_apoyo_economico'),
            'idClasificacionAreaServicioSocial' => array(self::BELONGS_TO, 'SsClasificacionArea', 'id_clasificacion_area_servicio_social'),
            'idStatusPrograma' => array(self::BELONGS_TO, 'SsTipoStatus', 'id_status_programa'),
            'idRegistroFechasSsocial' => array(self::BELONGS_TO, 'SsRegistroFechasServicioSocial', 'id_registro_fechas_ssocial'),
            'idUnidadReceptora' => array(self::BELONGS_TO, 'SsUnidadesReceptoras', 'id_unidad_receptora'),
            'ssResponsableProgramaInternos' => array(self::HAS_MANY, 'SsResponsableProgramaInterno', 'id_programa'),
            'ssHorarioDiasHabilesProgramases' => array(self::HAS_MANY, 'SsHorarioDiasHabilesProgramas', 'id_programa'),
            'ssResponsableProgramaExternos' => array(self::HAS_MANY, 'SsResponsableProgramaExterno', 'id_programa'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_programa' => 'Id Programa',
			'nombre_programa' => 'Nombre del Programa',
			'lugar_realizacion_programa' => 'Lugar de Realización (Donde se llevará a cabo el Programa)',
			'horas_totales' => 'Horas a Liberar',
			'id_periodo_programa' => 'Periodo del Programa (Semestral, Anual o Especial)',
			'numero_estudiantes_solicitados' => 'Numero de Estudiantes a Solicitar',
			'descripcion_objetivo_programa' => 'Objetivo del Programa',
			'id_tipo_servicio_social' => 'Tipo de Servicio Social',
			'impacto_social_esperado' => 'Impacto Social Esperado (Que beneficios se espera lograr con el Programa)',
			'beneficiarios_programa' => 'Beneficiarios del Programa (Quienes se beneficiaran con el desarrollo del Programa)',
			'actividades_especificas_realizar' => 'Actividades Especificas a Realizar',
			'mecanismos_supervision' => 'Mecanismos de Supervisión (Además de los Reportes Bimestrales pueden ser Fotografías, Reportes diarios u otras evidencias.)',
			'perfil_estudiante_requerido' => 'Perfil del Estudiante Requerido',
			'id_apoyo_economico_prestador' => 'Apoyo Económico al Estudiante',
			'id_tipo_apoyo_economico' => 'Tipo de Apoyo Económico',
			'apoyo_economico' => 'Apoyo Económico',
			'fecha_registro_programa' => 'Fecha de Registro',
			'fecha_modificacion_programa' => 'Fecha de Modificación',
			'id_recibe_capacitacion' => 'Recibe Capacitación',
			'id_clasificacion_area_servicio_social' => 'Clasificación del Área',
			'lugares_disponibles' => 'Lugares Disponibles',
			'id_tipo_programa' => 'Tipo de Programa',
			'id_status_programa' => 'Estatus del Programa',
			'fecha_inicio_programa' => 'Fecha de Inicio',
			'fecha_fin_programa' => 'Fecha Fin',
			'id_registro_fechas_ssocial' => 'Registro Fechas de Inicio',
     		'lugares_ocupados' => 'Lugares Ocupados',
      		'id_unidad_receptora' => 'Empresa',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsProgramas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
