<?php

/**
 * This is the model class for table "ss_tipo_expediente".
 *
 * The followings are the available columns in table 'ss_tipo_expediente':
 * @property integer $id_tipo_expediente
 * @property string $tipo_expediente
 */
class SsTipoExpediente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ss_tipo_expediente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_expediente', 'required'),
			array('tipo_expediente', 'length', 'max'=>80),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tipo_expediente, tipo_expediente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
				// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tipo_expediente' => 'Id Tipo Expediente',
			'tipo_expediente' => 'Tipo de Expediente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tipo_expediente',$this->id_tipo_expediente);
		$criteria->compare('tipo_expediente',$this->tipo_expediente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsTipoExpediente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
