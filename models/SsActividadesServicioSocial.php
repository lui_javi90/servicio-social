<?php

/**
 * This is the model class for table "ss_actividades_servicio_social".
 *
 * The followings are the available columns in table 'ss_actividades_servicio_social':
 * @property integer $id_actividad_servicio_social
 * @property integer $id_servicio_social
 * @property integer $id_mes
 * @property string $actividad_mensual
 *
 * The followings are the available model relations:
 * @property SsServicioSocial $idServicioSocial
 */
class SsActividadesServicioSocial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_actividades_servicio_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_servicio_social, id_mes', 'required'),
			array('id_servicio_social, id_mes', 'numerical', 'integerOnly'=>true),
			array('actividad_mensual', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_actividad_servicio_social, id_servicio_social, id_mes, actividad_mensual', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idServicioSocial' => array(self::BELONGS_TO, 'SsServicioSocial', 'id_servicio_social'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_actividad_servicio_social' => 'Id Actividad Servicio Social',
			'id_servicio_social' => 'Id Servicio Social',
			'id_mes' => 'Mes',
			'actividad_mensual' => 'Actividad',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsActividadesServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
