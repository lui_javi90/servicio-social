<?php

/**
 * This is the model class for table "pe_planeacion.ss_reportes_bimestral".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_reportes_bimestral':
 * @property integer $id_reporte_bimestral
 * @property integer $id_servicio_social
 * @property string $bimestres_correspondiente
 * @property boolean $bimestre_final
 * @property string $valida_responsable
 * @property string $valida_oficina_servicio_social
 * @property string $observaciones_reporte_bimestral
 * @property string $fecha_inicio_rep_bim
 * @property string $fecha_fin_rep_bim
 * @property string $calificacion_reporte_bimestral
 * @property boolean $reporte_bimestral_actual
 * @property string $envio_alum_evaluacion
 * @property string $rfcResponsable
 * @property string $rfcJefeOfiServicioSocial
 *
 * The followings are the available model relations:
 * @property SsEvaluacionBimestral[] $ssEvaluacionBimestrals
 * @property SsServicioSocial $idserviciosocial
 */
class SsReportesBimestral extends CActiveRecord
{
	public $nocontrol;
	public $anio;
	public $periodo;

	public function tableName()
	{
		return 'pe_planeacion.ss_reportes_bimestral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
    	// will receive user inputs.
		return array(
            array('id_reporte_bimestral, id_servicio_social, bimestres_correspondiente, fecha_inicio_rep_bim, fecha_fin_rep_bim, reporte_bimestral_actual', 'required'),
            array('id_reporte_bimestral, id_servicio_social', 'numerical', 'integerOnly'=>true),
            array('bimestres_correspondiente', 'length', 'max'=>1),
            array('observaciones_reporte_bimestral', 'length', 'max'=>200),
            array('calificacion_reporte_bimestral', 'length', 'max'=>3),
            array('rfcResponsable, rfcJefeOfiServicioSocial', 'length', 'max'=>13),
            array('bimestre_final, valida_responsable, valida_oficina_servicio_social, envio_alum_evaluacion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_reporte_bimestral, id_servicio_social, bimestres_correspondiente, bimestre_final, valida_responsable, valida_oficina_servicio_social, observaciones_reporte_bimestral, fecha_inicio_rep_bim, fecha_fin_rep_bim, calificacion_reporte_bimestral, reporte_bimestral_actual, envio_alum_evaluacion, rfcResponsable, rfcJefeOfiServicioSocial, nocontrol, anio, periodo', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
				// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ssEvaluacionBimestrals' => array(self::HAS_MANY, 'SsEvaluacionBimestral', 'id_reporte_bimestral'),
            'idserviciosocial' => array(self::BELONGS_TO, 'SsServicioSocial', 'id_servicio_social'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_reporte_bimestral' => 'Id Reporte Bimestral',
			'id_servicio_social' => 'Id Servicio Social',
			'bimestres_correspondiente' => 'Bimestre Correspondiente',
			'bimestre_final' => 'Bimestre Final',
			'valida_responsable' => 'Valida Responsable',
			'valida_oficina_servicio_social' => 'Valida Oficina Servicio Social',
			'observaciones_reporte_bimestral' => 'Observaciones del Reporte Bimestral',
			'fecha_inicio_rep_bim' => 'Fecha de Inicio',
			'fecha_fin_rep_bim' => 'Fecha Fin',
			'envio_alum_evaluacion' => 'Envio Alumno Evaluación',
			'calificacion_reporte_bimestral' => 'Calificación Reporte Bimestral',
			'reporte_bimestral_actual' => 'Reporte Bimestral Actual',
			'rfcResponsable' => 'Rfc del Supervisor',
            'rfcJefeOfiServicioSocial' => 'Jefe Oficina Servicio Social',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsReportesBimestral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
