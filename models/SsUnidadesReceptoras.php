<?php

/**
 * This is the model class for table "pe_planeacion.ss_unidades_receptoras".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_unidades_receptoras':
 * @property integer $id_unidad_receptora
 * @property string $nombre_unidad_receptora
 * @property string $rfc_unidad_responsable
 * @property integer $id_estado
 * @property integer $id_municipio
 * @property string $calle
 * @property string $numero
 * @property string $colonia
 * @property string $telefono
 * @property string $logo_unidad_receptora
 * @property string $fecha_registro_unidad_receptora
 * @property string $fecha_modificacion_unidad_receptora
 * @property integer $id_tipo_empresa
 * @property integer $id_status_unidad_receptora
 * @property string $sello_empresa
 * @property string $banner_superior
 * @property string $banner_inferior
 * @property string $path_carpeta
 *
 * The followings are the available model relations:
 * @property SsSupervisoresProgramas[] $ssSupervisoresProgramases
 * @property XEstados $idEstado
 * @property XMunicipios $idMunicipio
 * @property SsTipoStatus $idStatusUnidadReceptora
 */
class SsUnidadesReceptoras extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_unidades_receptoras';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('nombre_unidad_receptora, id_estado, id_municipio, calle, numero, colonia, telefono, id_tipo_empresa, id_status_unidad_receptora', 'required'),
            array('id_estado, id_municipio, id_tipo_empresa, id_status_unidad_receptora', 'numerical', 'integerOnly'=>true),
            array('nombre_unidad_receptora', 'length', 'max'=>200),
            array('rfc_unidad_responsable', 'length', 'max'=>13),
            array('calle, colonia', 'length', 'max'=>40),
            array('numero', 'length', 'max'=>4),
            array('telefono', 'length', 'max'=>50),
            array('logo_unidad_receptora, banner_superior, banner_inferior', 'length', 'max'=>25),
            array('sello_empresa', 'length', 'max'=>18),
            array('path_carpeta', 'length', 'max'=>250),
            array('fecha_registro_unidad_receptora, fecha_modificacion_unidad_receptora', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_unidad_receptora, nombre_unidad_receptora, rfc_unidad_responsable, id_estado, id_municipio, calle, numero, colonia, telefono, logo_unidad_receptora, fecha_registro_unidad_receptora, fecha_modificacion_unidad_receptora, id_tipo_empresa, id_status_unidad_receptora, sello_empresa, banner_superior, banner_inferior, path_carpeta', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'ssSupervisoresProgramases' => array(self::HAS_MANY, 'SsSupervisoresProgramas', 'id_unidad_receptora'),
            'idEstado' => array(self::BELONGS_TO, 'XEstados', 'id_estado'),
            'idMunicipio' => array(self::BELONGS_TO, 'XMunicipios', 'id_municipio'),
            'idStatusUnidadReceptora' => array(self::BELONGS_TO, 'SsTipoStatus', 'id_status_unidad_receptora'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_unidad_receptora' => 'Id Unidad Receptora',
			'nombre_unidad_receptora' => 'Nombre Empresa',
			'rfc_unidad_responsable' => 'RFC Empresa',
			'id_estado' => 'Estado',
			'id_municipio' => 'Municipio',
			'calle' => 'Calle',
			'numero' => 'No.',
			'colonia' => 'Colonia',
			'telefono' => 'Teléfono',
			'logo_unidad_receptora' => 'Logo Empresa',
			'fecha_registro_unidad_receptora' => 'Fecha Registro',
			'fecha_modificacion_unidad_receptora' => 'Fecha Modificación',
			'id_tipo_empresa' => 'Tipo de Empresa',
			'id_status_unidad_receptora' => 'Estatus Empresa',
			'sello_empresa' => 'Sello Empresa',
            'banner_superior' => 'Banner Superior',
			'banner_inferior' => 'Banner Inferior',
			'path_carpeta' => 'Path Carpeta',
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsUnidadesReceptoras the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
