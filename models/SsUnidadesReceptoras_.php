<?php
class SsUnidadesReceptoras_ extends SsUnidadesReceptoras
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);
		$criteria->compare('nombre_unidad_receptora',$this->nombre_unidad_receptora,true);
		$criteria->compare('rfc_unidad_responsable',$this->rfc_unidad_responsable,true);
		$criteria->compare('id_estado',$this->id_estado,true);
		$criteria->compare('id_municipio',$this->id_municipio,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('logo_unidad_receptora',$this->logo_unidad_receptora,true);
		$criteria->compare('fecha_registro_unidad_receptora',$this->fecha_registro_unidad_receptora,true);
		$criteria->compare('fecha_modificacion_unidad_receptora',$this->fecha_modificacion_unidad_receptora,true);
		$criteria->compare('id_tipo_empresa',$this->id_tipo_empresa);
		$criteria->compare('id_status_unidad_receptora',$this->id_status_unidad_receptora);
		$criteria->compare('sello_empresa',$this->sello_empresa,true);
        $criteria->compare('banner_superior',$this->banner_superior,true);
		$criteria->compare('banner_inferior',$this->banner_inferior,true);
		$criteria->compare('path_carpeta',$this->path_carpeta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	/*Solo empresas dadas de alta*/
	public function searchXAlta()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition = "id_status_unidad_receptora = 1";
		$criteria->order = "id_unidad_receptora ASC";

		$criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);
		$criteria->compare('nombre_unidad_receptora',$this->nombre_unidad_receptora,true);
		$criteria->compare('rfc_unidad_responsable',$this->rfc_unidad_responsable,true);
		$criteria->compare('id_estado',$this->id_estado,true);
		$criteria->compare('id_municipio',$this->id_municipio,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('logo_unidad_receptora',$this->logo_unidad_receptora,true);
		$criteria->compare('fecha_registro_unidad_receptora',$this->fecha_registro_unidad_receptora,true);
		$criteria->compare('fecha_modificacion_unidad_receptora',$this->fecha_modificacion_unidad_receptora,true);
		$criteria->compare('id_tipo_empresa',$this->id_tipo_empresa);
		$criteria->compare('id_status_unidad_receptora',$this->id_status_unidad_receptora);
		$criteria->compare('sello_empresa',$this->sello_empresa,true);
        $criteria->compare('banner_superior',$this->banner_superior,true);
		$criteria->compare('banner_inferior',$this->banner_inferior,true);
		$criteria->compare('path_carpeta',$this->path_carpeta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Solo empresas dadas de alta*/
}
?>