<?php

/**
 * This is the model class for table "ss_horario_dias_habiles_programas".
 *
 * The followings are the available columns in table 'ss_horario_dias_habiles_programas':
 * @property integer $id_horario
 * @property integer $id_programa
 * @property integer $id_dia_semana
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property string $horas_totales
 *
 * The followings are the available model relations:
 * @property SsProgramas $idPrograma
 * @property SsDiasSemana $idDiaSemana
 */
class SsHorarioDiasHabilesProgramas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_horario_dias_habiles_programas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_programa, id_dia_semana, hora_inicio, hora_fin', 'required'),
			array('id_programa, id_dia_semana', 'numerical', 'integerOnly'=>true),
			array('horas_totales', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_horario, id_programa, id_dia_semana, hora_inicio, hora_fin, horas_totales', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPrograma' => array(self::BELONGS_TO, 'SsProgramas', 'id_programa'),
			'idDiaSemana' => array(self::BELONGS_TO, 'SsDiasSemana', 'id_dia_semana'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_horario' => 'Id Horario',
			'id_programa' => 'Programa',
			'id_dia_semana' => 'Dia de la Semana',
			'hora_inicio' => 'Hora de Inicio',
			'hora_fin' => 'Hora Fin',
			'horas_totales' => 'Horas Totales',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsHorarioDiasHabilesProgramas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
