<?php
class SsSupervisoresProgramas_ extends SsSupervisoresProgramas
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition = "id_status_supervisor != 2";
		$criteria->order = ' id_unidad_receptora ASC';

		$criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);
        $criteria->compare('apell_paterno',$this->apell_paterno,true);
        $criteria->compare('apell_materno',$this->apell_materno,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('email_supervisor',$this->email_supervisor,true);
        $criteria->compare('id_tipo_supervisor',$this->id_tipo_supervisor);
        $criteria->compare('id_status_supervisor',$this->id_status_supervisor);
        $criteria->compare('id_sexo',$this->id_sexo,true);
        $criteria->compare('foto_supervisor',$this->foto_supervisor,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"dscDepartamento"',$this->dscDepartamento,true);
        $criteria->compare('eres_jefe_depto',$this->eres_jefe_depto);
        $criteria->compare('"gradoMaxEstudios"',$this->gradoMaxEstudios,true);
		$criteria->compare('"passwSupervisor"',$this->passwSupervisor,true);
		$criteria->compare('"grMaxEstSup"',$this->grMaxEstSup,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	//Los supervisores que fueron agregados a un programa
	public function searchXSupervisorAgrEmpresa($id_programa)
	{
		$criteria=new CDbCriteria;
		$criteria->alias = "sp";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_responsable_programa_externo as rpe on rpe.\"rfcSupervisor\" = sp.\"rfcSupervisor\" ";
		$criteria->condition = " rpe.id_programa = '$id_programa' ";
		$criteria->order = " sp.id_unidad_receptora ASC";

		$criteria->compare('"rfcSupervisor"',$this->rfcSupervisor,true);
        $criteria->compare('nombre_supervisor',$this->nombre_supervisor,true);
        $criteria->compare('apell_paterno',$this->apell_paterno,true);
        $criteria->compare('apell_materno',$this->apell_materno,true);
        $criteria->compare('cargo_supervisor',$this->cargo_supervisor,true);
        $criteria->compare('email_supervisor',$this->email_supervisor,true);
        $criteria->compare('id_tipo_supervisor',$this->id_tipo_supervisor);
        $criteria->compare('id_status_supervisor',$this->id_status_supervisor);
        $criteria->compare('id_sexo',$this->id_sexo,true);
        $criteria->compare('foto_supervisor',$this->foto_supervisor,true);
        $criteria->compare('id_unidad_receptora',$this->id_unidad_receptora);
        $criteria->compare('nombre_jefe_depto',$this->nombre_jefe_depto,true);
        $criteria->compare('"dscDepartamento"',$this->dscDepartamento,true);
        $criteria->compare('eres_jefe_depto',$this->eres_jefe_depto);
        $criteria->compare('"gradoMaxEstudios"',$this->gradoMaxEstudios,true);
		$criteria->compare('"passwSupervisor"',$this->passwSupervisor,true);
		$criteria->compare('"grMaxEstSup"',$this->grMaxEstSup,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>10,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	//Los supervisores que fueron agregados a un programa
}
?>