<?php
class SsEvaluacionBimestral_ extends SsEvaluacionBimestral
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_evaluacion_bimestral',$this->id_evaluacion_bimestral);
		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_criterio',$this->id_criterio);
		$criteria->compare('evaluacion_b',$this->evaluacion_b,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	/*Busqueda para el supervisor/depto*/
	public function searchXEvaluacionBimestral($id_reporte_bimestral, $id_tipo)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias="eb";
		$criteria->select = "eb.id_evaluacion_bimestral, eb.id_reporte_bimestral, eb.id_criterio, eb.evaluacion_b";
		$criteria->join = "INNER JOIN pe_planeacion.ss_criterios_a_evaluar AS ce ON ce.id_criterio = eb.id_criterio";
		$criteria->condition = "ce.id_tipo_criterio = '$id_tipo' and eb.id_reporte_bimestral = '$id_reporte_bimestral' ";
		$criteria->order = "ce.posicion_criterio ASC";

		$criteria->compare('id_evaluacion_bimestral',$this->id_evaluacion_bimestral);
		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_criterio',$this->id_criterio);
		$criteria->compare('evaluacion_b',$this->evaluacion_b,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
    /*Busqueda para el supervisor/depto*/
} ?>
