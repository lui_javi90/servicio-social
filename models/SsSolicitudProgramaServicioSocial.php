<?php

/**
 * This is the model class for table "ss_solicitud_programa_servicio_social".
 *
 * The followings are the available columns in table 'ss_solicitud_programa_servicio_social':
 * @property integer $id_solicitud_programa
 * @property string $no_ctrl
 * @property integer $id_programa
 * @property string $fecha_solicitud_programa
 * @property string $valida_solicitud_alumno
 * @property string $valida_solicitud_supervisor_programa
 * @property integer $id_estado_solicitud_programa_supervisor
 * @property integer $id_estado_solicitud_programa_alumno
 *
 * The followings are the available model relations:
 * @property AlumnosVigentesInscritosCreditos $noCtrl
 * @property SsProgramas $idPrograma
 * @property SsEstadosSolicitudPrograma $idEstadoSolicitudProgramaSupervisor
 * @property SsEstadosSolicitudPrograma $idEstadoSolicitudProgramaAlumno
 * @property SsServicioSocial $ssServicioSocial
 */
class SsSolicitudProgramaServicioSocial extends CActiveRecord
{

	public function tableName()
	{
		return 'pe_planeacion.ss_solicitud_programa_servicio_social';
	}


	public function rules()
	{
				// NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('no_ctrl, id_programa, id_estado_solicitud_programa_supervisor, id_estado_solicitud_programa_alumno', 'required'),
            array('id_programa, id_estado_solicitud_programa_supervisor, id_estado_solicitud_programa_alumno', 'numerical', 'integerOnly'=>true),
            array('no_ctrl', 'length', 'max'=>8),
            array('fecha_solicitud_programa, valida_solicitud_alumno, valida_solicitud_supervisor_programa', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_solicitud_programa, no_ctrl, id_programa, fecha_solicitud_programa, valida_solicitud_alumno, valida_solicitud_supervisor_programa, id_estado_solicitud_programa_supervisor, id_estado_solicitud_programa_alumno', 'safe', 'on'=>'search'),
        );
	}


	public function relations()
	{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
					'noCtrl' => array(self::BELONGS_TO, 'AlumnosVigentesInscritosCreditos', 'no_ctrl'),
					'idPrograma' => array(self::BELONGS_TO, 'SsProgramas', 'id_programa'),
					'idEstadoSolicitudProgramaSupervisor' => array(self::BELONGS_TO, 'SsEstadosSolicitudPrograma', 'id_estado_solicitud_programa_supervisor'),
					'idEstadoSolicitudProgramaAlumno' => array(self::BELONGS_TO, 'SsEstadosSolicitudPrograma', 'id_estado_solicitud_programa_alumno'),
					'ssServicioSocial' => array(self::HAS_ONE, 'SsServicioSocial', 'id_servicio_social'),
			);
	}


	public function attributeLabels()
	{
			return array(
					'id_solicitud_programa' => 'Id Solicitud Programa',
					'no_ctrl' => 'No. de Control',
					'id_programa' => 'Programa',
					'valida_solicitud_alumno' => 'Valida Solicitud Alumno',
					'valida_solicitud_supervisor_programa' => 'Valida Solicitud Supervisor',
					'id_estado_solicitud_programa_supervisor' => 'Estado Solicitud del Supervisor',
					'id_estado_solicitud_programa_alumno' => 'Id Estado Solicitud del Alumno',
			);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsSolicitudProgramaServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
