<?php

/**
 * This is the model class for table "pe_planeacion.ss_registro_fechas_servicio_social".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_registro_fechas_servicio_social':
 * @property integer $id_registro_fechas_ssocial
 * @property string $fecha_limite_inscripcion
 * @property string $fecha_inicio_ssocial
 * @property boolean $ssocial_actual
 * @property string $anio
 * @property string $id_periodo
 * @property boolean $cambiar_fech_limite_insc
 *
 * The followings are the available model relations:
 * @property EPeriodos $idPeriodo
 * @property SsProgramas[] $ssProgramases
 */
class SsRegistroFechasServicioSocial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		//return 'ss_registro_fechas_servicio_social';
		return 'pe_planeacion.ss_registro_fechas_servicio_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('fecha_inicio_ssocial', 'required'),
            array('anio', 'length', 'max'=>4),
            array('id_periodo', 'length', 'max'=>1),
            array('fecha_limite_inscripcion, ssocial_actual, cambiar_fech_limite_insc', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_registro_fechas_ssocial, fecha_limite_inscripcion, fecha_inicio_ssocial, ssocial_actual, anio, id_periodo, cambiar_fech_limite_insc', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'idPeriodo' => array(self::BELONGS_TO, 'EPeriodos', 'id_periodo'),
            'ssProgramases' => array(self::HAS_MANY, 'SsProgramas', 'id_registro_fechas_ssocial'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_registro_fechas_ssocial' => 'Id Registro Fechas Ssocial',
			'fecha_limite_inscripcion' => 'Fecha Limite Inscripción',
			'fecha_inicio_ssocial' => 'Fecha Inicio',
			'ssocial_actual' => 'Servicio Social Actual',
			'anio' => 'Año',
			'id_periodo' => 'Periodo',
			'cambiar_fech_limite_insc' => 'Cambiar Fech Limite Insc', //Para poder cambiar la fecha limite de inscripcion a servicio social
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "ssocial_actual DESC";

		$criteria->compare('id_registro_fechas_ssocial',$this->id_registro_fechas_ssocial);
        $criteria->compare('fecha_limite_inscripcion',$this->fecha_limite_inscripcion,true);
        $criteria->compare('fecha_inicio_ssocial',$this->fecha_inicio_ssocial,true);
        $criteria->compare('ssocial_actual',$this->ssocial_actual);
        $criteria->compare('anio',$this->anio);
		$criteria->compare('id_periodo',$this->id_periodo,true);
		$criteria->compare('cambiar_fech_limite_insc',$this->cambiar_fech_limite_insc);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsRegistroFechasServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
