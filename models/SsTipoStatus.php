<?php

/**
 * This is the model class for table "ss_tipo_status".
 *
 * The followings are the available columns in table 'ss_tipo_status':
 * @property integer $id_status
 * @property string $descripcion_status
 *
 * The followings are the available model relations:
 * @property SsProgramas[] $ssProgramases
 * @property SsSupervisoresProgramas[] $ssSupervisoresProgramases
 * @property SsUnidadesReceptoras[] $ssUnidadesReceptorases
 */
class SsTipoStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_tipo_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion_status', 'required'),
			array('descripcion_status', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_status, descripcion_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssProgramases' => array(self::HAS_MANY, 'SsProgramas', 'id_status_programa'),
			'ssSupervisoresProgramases' => array(self::HAS_MANY, 'SsSupervisoresProgramas', 'id_status_supervisor'),
			'ssUnidadesReceptorases' => array(self::HAS_MANY, 'SsUnidadesReceptoras', 'id_status_unidad_receptora'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_status' => 'Id Status',
			'descripcion_status' => 'Descripcion Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('descripcion_status',$this->descripcion_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsTipoStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
