<?php

/**
 * This is the model class for table "pe_planeacion.ss_observaciones_servicio_social".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_observaciones_servicio_social':
 * @property integer $id_observacion
 * @property integer $id_servicio_social
 * @property string $observacion
 * @property string $fecha_registro
 * @property integer $tipo_observacion_emisor
 * @property boolean $fue_leida
 * @property string $fecha_leida
 * @property integer $hilo_observacion
 * @property string $emisor_observ
 * @property string $receptor_observ
 * @property integer $tipo_observacion_receptor
 *
 * The followings are the available model relations:
 * @property SsServicioSocial $idServicioSocial
 */
class SsObservacionesServicioSocial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_observaciones_servicio_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('id_servicio_social, observacion, tipo_observacion_emisor', 'required'),
            array('id_servicio_social, tipo_observacion_emisor, hilo_observacion, tipo_observacion_receptor', 'numerical', 'integerOnly'=>true),
            array('observacion', 'length', 'max'=>500),
            array('emisor_observ, receptor_observ', 'length', 'max'=>13),
            array('fecha_registro, fue_leida, fecha_leida', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_observacion, id_servicio_social, observacion, fecha_registro, tipo_observacion_emisor, fue_leida, fecha_leida, hilo_observacion, emisor_observ, receptor_observ, tipo_observacion_receptor', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idServicioSocial' => array(self::BELONGS_TO, 'SsServicioSocial', 'id_servicio_social'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_observacion' => 'Id Observacion',
			'id_servicio_social' => 'Id Servicio Social',
			'observacion' => 'Observación',
			'fecha_registro' => 'Fecha de Registro',
			'tipo_observacion_emisor' => 'Tipo Observación Emisor',
			'fue_leida' => 'Vista',
			'fecha_leida' => 'Fecha de Vista',
			'hilo_observacion' => 'Hilo Observacion', //Para saber loas observaciones que corresponden a cada conversacion 
			'emisor_observ' => 'Emisor de Observación',
			'receptor_observ' => 'Receptor de Observación',
			'tipo_observacion_receptor' => 'Tipo Observación Receptor',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsObservacionesServicioSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
