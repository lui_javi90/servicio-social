<?php

/**
 * This is the model class for table "ss_evaluacion_bimestral".
 *
 * The followings are the available columns in table 'ss_evaluacion_bimestral':
 * @property integer $id_evaluacion_bimestral
 * @property integer $id_reporte_bimestral
 * @property integer $id_criterio
 * @property string $evaluacion_b
 *
 * The followings are the available model relations:
 * @property SsReportesBimestral $idReporteBimestral
 * @property SsCriteriosAEvaluar $idCriterio
 */
class SsEvaluacionBimestral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_evaluacion_bimestral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_reporte_bimestral, id_criterio, evaluacion_b', 'required'),
			array('id_reporte_bimestral, id_criterio', 'numerical', 'integerOnly'=>true),
			array('evaluacion_b', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_evaluacion_bimestral, id_reporte_bimestral, id_criterio, evaluacion_b', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idReporteBimestral' => array(self::BELONGS_TO, 'SsReportesBimestral', 'id_reporte_bimestral'),
			'idCriterio' => array(self::BELONGS_TO, 'SsCriteriosAEvaluar', 'id_criterio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_evaluacion_bimestral' => 'Id Evaluacion Bimestral',
			'id_reporte_bimestral' => 'Id Reporte Bimestral',
			'id_criterio' => 'Criterio',
			'evaluacion_b' => 'Evaluación B',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsEvaluacionBimestral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
