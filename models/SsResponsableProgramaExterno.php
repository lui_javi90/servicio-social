<?php

/**
 * This is the model class for table "pe_planeacion.ss_responsable_programa_externo".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_responsable_programa_externo':
 * @property integer $id_programa
 * @property string $rfcSupervisor
 * @property string $fecha_asignacion
 * @property boolean $superv_principal_ext
 *
 * The followings are the available model relations:
 * @property SsSupervisoresProgramas $rfcSupervisor
 * @property SsProgramas $idPrograma
 */
class SsResponsableProgramaExterno extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_responsable_programa_externo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha_asignacion, superv_principal_ext', 'required'),
			array('id_programa', 'numerical', 'integerOnly'=>true),
			array('rfcSupervisor', 'length', 'max'=>13),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_programa, rfcSupervisor, fecha_asignacion, superv_principal_ext', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rfcSupervisor' => array(self::BELONGS_TO, 'SsSupervisoresProgramas', 'rfcSupervisor'),
			'idPrograma' => array(self::BELONGS_TO, 'SsProgramas', 'id_programa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_programa' => 'Id Programa',
			'rfcSupervisor' => 'Rfc Supervisor',
			'fecha_asignacion' => 'Fecha Asignacion',
			'superv_principal_ext' => 'Superv Principal Ext',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('rfcSupervisor',$this->rfcSupervisor,true);
		$criteria->compare('fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('superv_principal_ext',$this->superv_principal_ext);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchSupervisorXPrograma($id_programa)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias = "rpe";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_supervisores_programas sp on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\" ";
		$criteria->condition = "rpe.id_programa = '$id_programa' ";

		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('rfcSupervisor',$this->rfcSupervisor,true);
		$criteria->compare('fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('superv_principal_ext',$this->superv_principal_ext);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsResponsableProgramaExterno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
