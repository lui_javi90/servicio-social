<?php

/**
 * This is the model class for table "pe_planeacion.ss_responsable_programa_interno".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_responsable_programa_interno':
 * @property integer $id_programa
 * @property string $rfcEmpleado
 * @property string $fecha_asignacion
 * @property boolean $superv_principal_int
 *
 * The followings are the available model relations:
 * @property HEmpleados $rfcEmpleado
 * @property SsProgramas $idPrograma
 */
class SsResponsableProgramaInterno extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_responsable_programa_interno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha_asignacion, superv_principal_int', 'required'),
			array('id_programa', 'numerical', 'integerOnly'=>true),
			array('rfcEmpleado', 'length', 'max'=>13),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_programa, rfcEmpleado, fecha_asignacion, superv_principal_int', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rfcEmpleado' => array(self::BELONGS_TO, 'HEmpleados', 'rfcEmpleado'),
			'idPrograma' => array(self::BELONGS_TO, 'SsProgramas', 'id_programa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_programa' => 'Id Programa',
			'rfcEmpleado' => 'Rfc Empleado',
			'fecha_asignacion' => 'Fecha Asignacion',
			'superv_principal_int' => 'Superv Principal Int',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('rfcEmpleado',$this->rfcEmpleado,true);
		$criteria->compare('fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('superv_principal_int',$this->superv_principal_int);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchSupervisorXPrograma($id_programa)
	{
		$criteria=new CDbCriteria;
		$criteria->alias = "rpi";
		$criteria->select = "*";
		$criteria->join = "join public.\"H_empleados\" hemp  on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\" ";
		$criteria->condition = "rpi.id_programa = '$id_programa' ";


		/*select * from pe_planeacion.ss_responsable_programa_interno rpi
		join public."H_empleados" hemp 
		on hemp."rfcEmpleado" = rpi."rfcEmpleado"
		where rpi.id_programa = 8*/

		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('rfcEmpleado',$this->rfcEmpleado,true);
		$criteria->compare('fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('superv_principal_int',$this->superv_principal_int);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsResponsableProgramaInterno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
