<?php

/**
 * This is the model class for table "pe_planeacion.ss_clasificacion_area".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_clasificacion_area':
 * @property integer $id_clasificacion_area_servicio_social
 * @property string $clasificacion_area_servicio_social
 * @property boolean $status_area
 *
 * The followings are the available model relations:
 * @property SsProgramas[] $ssProgramases
 */
class SsClasificacionArea extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_clasificacion_area';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('clasificacion_area_servicio_social', 'required'),
            array('clasificacion_area_servicio_social', 'length', 'max'=>200),
            array('status_area', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_clasificacion_area_servicio_social, clasificacion_area_servicio_social, status_area', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssProgramases' => array(self::HAS_MANY, 'SsProgramas', 'id_clasificacion_area_servicio_social'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_clasificacion_area_servicio_social' => 'Cve. Clasificación Área',
			'clasificacion_area_servicio_social' => 'Clasificación Área Servicio Social',
			'status_area' => 'Estatus del Área',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsClasificacionArea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
