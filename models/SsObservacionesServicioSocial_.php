<?php
class SsObservacionesServicioSocial_ extends SsObservacionesServicioSocial
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_observacion',$this->id_observacion);
        $criteria->compare('id_servicio_social',$this->id_servicio_social);
        $criteria->compare('observacion',$this->observacion,true);
        $criteria->compare('fecha_registro',$this->fecha_registro,true);
        $criteria->compare('tipo_observacion_emisor',$this->tipo_observacion_emisor);
        $criteria->compare('fue_leida',$this->fue_leida);
		$criteria->compare('fecha_leida',$this->fecha_leida,true);
		$criteria->compare('hilo_observacion',$this->hilo_observacion);
		$criteria->compare('emisor_observ',$this->emisor_observ,true);
		$criteria->compare('receptor_observ',$this->receptor_observ,true);
		$criteria->compare('tipo_observacion_receptor',$this->tipo_observacion_receptor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}

	/*Muestra todas las observaciones del alumno en el servicio social actual por medio del id_servicio_social*/
	public function searchXHistorialAlumnoServicioSocial($id_servicio_social)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->alias = "oss";
		$criteria->select = "DISTINCT ON(oss.tipo_observacion_emisor) oss.id_observacion, oss.id_servicio_social, oss.observacion, oss.fecha_registro, oss.tipo_observacion_emisor, oss.fue_leida, oss.fecha_leida, oss.hilo_observacion, oss.emisor_observ, oss.receptor_observ, oss.tipo_observacion_receptor";
		$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = oss.id_servicio_social";
		$criteria->condition = "oss.id_servicio_social = '$id_servicio_social' AND oss.tipo_observacion_emisor != 3";

		//$criteria->alias='oss';
		//$criteria->select = "DISTINCT ON(oss.tipo_observacion_receptor) oss.id_observacion, oss.id_servicio_social, oss.observacion, oss.fecha_registro, oss.tipo_observacion_emisor, oss.fue_leida, oss.fecha_leida, oss.hilo_observacion, oss.emisor_observ, oss.receptor_observ, oss.tipo_observacion_receptor";
		//$criteria->join = "join pe_planeacion.ss_servicio_social as ss ON ss.id_servicio_social = oss.id_servicio_social";
		//$criteria->condition = "ss.id_estado_servicio_social > 0 AND ss.id_estado_servicio_social < 6 AND oss.id_servicio_social = '$id_servicio_social' ";
		//$criteria->order = "oss.id_observacion ASC";
		
		$criteria->compare('id_observacion',$this->id_observacion);
        $criteria->compare('id_servicio_social',$this->id_servicio_social);
        $criteria->compare('observacion',$this->observacion,true);
        $criteria->compare('fecha_registro',$this->fecha_registro,true);
        $criteria->compare('tipo_observacion_emisor',$this->tipo_observacion_emisor);
        $criteria->compare('fue_leida',$this->fue_leida);
		$criteria->compare('fecha_leida',$this->fecha_leida,true);
		$criteria->compare('hilo_observacion',$this->hilo_observacion);
		$criteria->compare('emisor_observ',$this->emisor_observ,true);
		$criteria->compare('receptor_observ',$this->receptor_observ,true);
		$criteria->compare('tipo_observacion_receptor',$this->tipo_observacion_receptor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>25,//El numero de registros que se muestran en el CGridView
			),
		));
	}
	/*Muestra todas las observaciones del alumno en el servicio social actual por medio del id_servicio_social*/
}

?>