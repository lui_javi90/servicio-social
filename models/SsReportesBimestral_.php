<?php
class SsReportesBimestral_ extends SsReportesBimestral
{
	public $nocontrol;
	public $anio;
	public $periodo;

    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->order = "id_reporte_bimestral ASC";

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	//Todos los reportes validados por la oficina de Servicio Social
	public function searchXReportesBimnestralesValidadosOficServicioSocial($nocontrol, $anio, $periodo)
	{
		$criteria = new CDbCriteria;
		//$criteria->select = "*";

		if($nocontrol != NULL AND $anio != NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND 
									ss.no_ctrl = '$nocontrol' AND rf.id_periodo = '$periodo' AND rf.anio = '$anio' ";

		}elseif($nocontrol != NULL AND $anio == NULL AND $periodo == NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND 
									ss.no_ctrl = '$nocontrol' ";

		}elseif($nocontrol != NULL AND $anio != NULL AND $periodo == NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND 
									ss.no_ctrl = '$nocontrol' AND rf.anio = '$anio' ";

		}elseif($nocontrol != NULL AND $anio == NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND 
									ss.no_ctrl = '$nocontrol' AND rf.id_periodo = '$periodo' ";

		}elseif($nocontrol == NULL AND $anio != NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND rf.anio = '$anio' AND rf.id_periodo = '$periodo' ";

		}elseif($nocontrol == NULL AND $anio != NULL AND $periodo == NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND 
									rf.anio = '$anio' ";

		}elseif($nocontrol == NULL AND $anio == NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = " rb.valida_oficina_servicio_social is not null AND 
									rf.id_periodo = '$periodo' ";

		}elseif($nocontrol == NULL AND $anio == NULL AND $periodo == NULL)
		{
			//Cuando no se filtre por ningun parametro mas que el RFC del Supervisor
			$criteria->select = "*";
			$criteria->condition = " valida_oficina_servicio_social is not null ";
			
		}

		$criteria->order = " id_reporte_bimestral DESC";


		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));


	}

	//Todos los Reportes Bimestrales validados por el Supervisor
	public function searchXReporteBimestralValidadoSupervisor($rfcSupervisor, $nocontrol, $anio, $periodo)
	{
		$criteria = new CDbCriteria;
		//$criteria->select = "*";

		if($nocontrol != NULL AND $anio != NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									ss.no_ctrl = '$nocontrol' AND rf.id_periodo = '$periodo' AND rf.anio = '$anio' ";

		}elseif($nocontrol != NULL AND $anio == NULL AND $periodo == NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									ss.no_ctrl = '$nocontrol' ";

		}elseif($nocontrol != NULL AND $anio != NULL AND $periodo == NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									ss.no_ctrl = '$nocontrol' AND rf.anio = '$anio' ";

		}elseif($nocontrol != NULL AND $anio == NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									ss.no_ctrl = '$nocontrol' AND rf.id_periodo = '$periodo' ";

		}elseif($nocontrol == NULL AND $anio != NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									rf.anio = '$anio' AND rf.id_periodo = '$periodo' ";

		}elseif($nocontrol == NULL AND $anio != NULL AND $periodo == NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									rf.anio = '$anio' ";

		}elseif($nocontrol == NULL AND $anio == NULL AND $periodo != NULL)
		{
			$criteria->alias = "rb";
			$criteria->select = "*";
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social
			join pe_planeacion.ss_programas p on p.id_programa = ss.id_programa
			join pe_planeacion.ss_registro_fechas_servicio_social rf on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial";
			$criteria->condition = "rb.\"rfcResponsable\" = '$rfcSupervisor' AND rb.valida_responsable is not null AND 
									rf.id_periodo = '$periodo' ";

		}elseif($nocontrol == NULL AND $anio == NULL AND $periodo == NULL)
		{
			//Cuando no se filtre por ningun parametro mas que el RFC del Supervisor
			$criteria->select = "*";
			$criteria->condition = " \"rfcResponsable\" = '$rfcSupervisor' AND valida_responsable is not null ";
			
		}

		$criteria->order = " id_reporte_bimestral DESC";
		

		/*select * from pe_planeacion.ss_reportes_bimestral rb
		join pe_planeacion.ss_servicio_social ss
		on ss.id_servicio_social = rb.id_servicio_social
		join pe_planeacion.ss_programas p
		on p.id_programa = ss.id_programa
		join pe_planeacion.ss_registro_fechas_servicio_social rf
		on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
		where rb."rfcResponsable" = 'ROVG8201127G6' AND rb.valida_responsable is not null AND ss.no_ctrl = '10030355' AND
		rf.id_periodo = '1' AND rf.anio = 2020 */

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	public function searchXReportesBimestralesAlumnoServicioSocialActual($nocontrol)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->select = "*";
		$criteria->alias = "rb";

		if($nocontrol != NULL)
		{
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social";
			$criteria->condition = "(ss.id_estado_servicio_social > 0 AND ss.id_estado_servicio_social < 5) AND ss.no_ctrl = '$nocontrol' ";
			$criteria->order = "rb.id_reporte_bimestral ASC";
		}else{
			$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social";
			$criteria->condition = "ss.id_estado_servicio_social > 0 AND ss.id_estado_servicio_social < 5";
			$criteria->order = "rb.id_reporte_bimestral ASC";
		}

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	/*Muestra todos los Reportes Bimestrales del Servicio Social Actual del alumno (Finalizado) */
	public function searchXReportesBimestralesServSocialActual($id_servicio_social)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias='rb';
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social join pe_planeacion.ss_programas pss on pss.id_programa = ss.id_programa";
		$criteria->condition = "(rb.envio_alum_evaluacion is not null AND rb.valida_responsable is not null
		AND rb.valida_oficina_servicio_social is not null) AND (ss.id_estado_servicio_social > 3 AND ss.id_estado_servicio_social < 7)
		AND ss.id_servicio_social = '$id_servicio_social' ";
		$criteria->order = "rb.id_reporte_bimestral ASC";

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}
	/*Muestra todos los Reportes Bimestrales del Servicio Social Actual del alumno (Finalizado) */

	/*Muestra el servicio social de cada alumno para evaluar los reportes bimestrales por supervisor*/
	public function searchReportesXServicioSocialInterno($rfcSupervisor, $id_tipo_programa, $nocontrol, $id_programa)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria;
		$criteria->together = true;
		$criteria->with = array(
			'idserviciosocial' => array(
				'joinType' => 'JOIN',
			)
		);

		$criteria->alias='rb';
		$criteria->select = "*";

		//Filtrar por Supervisor Externo
		if($id_tipo_programa == 2)
		{
			//Filtrar por Servicio Social del Alumno
			if($nocontrol == NULL)
			{
				$criteria->join = "join pe_planeacion.ss_servicio_social ss
				on ss.id_servicio_social = rb.id_servicio_social
				join pe_planeacion.ss_programas pss
				on pss.id_programa = ss.id_programa
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa";
				$criteria->condition = "rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND rb.envio_alum_evaluacion is not null AND (ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6 AND ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 8)";
			}else if($nocontrol != NULL){

				$criteria->join = "join pe_planeacion.ss_servicio_social ss
				on ss.id_servicio_social = rb.id_servicio_social
				join pe_planeacion.ss_programas pss
				on pss.id_programa = ss.id_programa
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa";
				$criteria->condition = "rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND ss.no_ctrl = '$nocontrol' AND rb.envio_alum_evaluacion is not null AND (ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6 AND ss.id_estado_servicio_social != 8)";
			}

		}else{ //Interno
			//Filtrar por Servicio Social del Alumno
			if($nocontrol == NULL)
			{
				$criteria->join = "join pe_planeacion.ss_servicio_social ss
				on ss.id_servicio_social = rb.id_servicio_social
				join pe_planeacion.ss_programas pss
				on pss.id_programa = ss.id_programa
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa";
				$criteria->condition = "rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND 
										rb.envio_alum_evaluacion is not null AND rb.valida_responsable is null AND 
										(ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6 AND 
										ss.id_estado_servicio_social != 8)";
			}else if($nocontrol != NULL){

				$criteria->join = "join pe_planeacion.ss_servicio_social ss
				on ss.id_servicio_social = rb.id_servicio_social
				join pe_planeacion.ss_programas pss
				on pss.id_programa = ss.id_programa
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa";
				$criteria->condition = "rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_programa = '$id_programa' AND 
										ss.no_ctrl = '$nocontrol' AND rb.envio_alum_evaluacion is not null AND rb.valida_responsable is null AND
										(ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6 AND 
										ss.id_estado_servicio_social != 8)";
			}

		}

		$criteria->order = "ss.id_servicio_social ASC";

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
    $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'nocontrol'=>array(
						'asc'=>'idserviciosocial.no_ctrl',
						'desc'=>'idserviciosocial.no_ctrl DESC',
					),
					'*',
				),
			),
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}
	/*Muestra el servicio social de cada alumno para evaluar los reportes bimestrales por supervisor*/

	/*Reportes Bimestrales de Servicio Social Externo*/
	public function searchReportesXServicioSocialExternoAdmin($nocontrol)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->together = true;
		$criteria->with = array(
			'idserviciosocial' => array(
				'joinType' => 'JOIN',
			)
		);

		/*$criteria=new CDbCriteria;
		$criteria->alias='rb';
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_servicio_social ss on ss.id_servicio_social = rb.id_servicio_social join pe_planeacion.ss_programas pss on pss.id_programa = ss.id_programa";
		$criteria->condition = "pss.id_tipo_programa = 2 AND (rb.envio_alum_evaluacion is not null AND rb.valida_responsable is not null AND rb.valida_oficina_servicio_social is null)";
		$criteria->order = "rb.id_servicio_social ASC";*/

		if($nocontrol == NULL)
		{
			$criteria->alias='rb';
			$criteria->select = "*";
			$criteria->join = "inner join pe_planeacion.ss_servicio_social ss ON ss.id_servicio_social = rb.id_servicio_social join pe_planeacion.ss_programas pss ON pss.id_programa = ss.id_programa";
			$criteria->condition = "pss.id_status_programa = 1 AND (rb.envio_alum_evaluacion is not null AND rb.valida_oficina_servicio_social is null) AND (ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6) AND
									pss.id_tipo_programa = 2";
			//$criteria->order = "rb.id_servicio_social ASC";

		}else if($nocontrol != NULL){

			//die();//JOIN pe_planeacion.ss_programas pss ON pss.id_programa = ss.id_programa
			$criteria->alias='rb';
			$criteria->select = "*";
			$criteria->join = "inner join pe_planeacion.ss_servicio_social ss ON ss.id_servicio_social = rb.id_servicio_social join pe_planeacion.ss_programas pss ON pss.id_programa = ss.id_programa";
			$criteria->condition = "pss.id_status_programa = 1 AND (rb.envio_alum_evaluacion is not null AND rb.valida_oficina_servicio_social is null) AND ss.no_ctrl = '$nocontrol' AND
									(ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6) AND pss.id_tipo_programa = 2";
			//$criteria->order = "rb.id_servicio_social ASC";
		}

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}
	/*Reportes Bimestrales de Servicio Social Externo*/

	/*Muestra el servicio social (INTERNO) de cada alumno para evaluar los reportes bimestrales por parte del depto*/
	public function searchReportesXServicioSocialInternoAdmin($nocontrol)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->together = true;
		$criteria->with = array(
			'idserviciosocial' => array(
				'joinType' => 'JOIN',
			)
		);

		$criteria->alias='rb';
		$criteria->select = "*";

		if($nocontrol == NULL)
		{
			$criteria->join = "inner join pe_planeacion.ss_servicio_social ss ON ss.id_servicio_social = rb.id_servicio_social
								inner join pe_planeacion.ss_programas pss on pss.id_programa = ss.id_programa";
			$criteria->condition = "pss.id_status_programa = 1 AND (rb.envio_alum_evaluacion is not null AND rb.valida_responsable is not null AND rb.valida_oficina_servicio_social is null) AND 
									(ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6)";
			//$criteria->order = "rb.id_servicio_social ASC";

		}else if($nocontrol != NULL){

			$criteria->join = "inner join pe_planeacion.ss_servicio_social ss ON ss.id_servicio_social = rb.id_servicio_social
							   inner join pe_planeacion.ss_programas pss on pss.id_programa = ss.id_programa";
			$criteria->condition = "pss.id_status_programa = 1 AND (rb.envio_alum_evaluacion is not null AND rb.valida_responsable is not null AND rb.valida_oficina_servicio_social is null) AND 
									ss.no_ctrl = '$nocontrol' AND (ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6)";
			//$criteria->order = "rb.id_servicio_social ASC";
		}


		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
        $criteria->compare('id_servicio_social',$this->id_servicio_social);
        $criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
        $criteria->compare('bimestre_final',$this->bimestre_final);
        $criteria->compare('valida_responsable',$this->valida_responsable,true);
        $criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
        $criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
        $criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
        $criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
        $criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
        $criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
        $criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
        $criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'nocontrol'=>array(
						'asc'=>'idserviciosocial.no_ctrl',
						'desc'=>'idserviciosocial.no_ctrl DESC',
					),
					'*',
				),
			),
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
	}
	/*Muestra el servicio social (INTERNO) de cada alumno para evaluar los reportes bimestrales por parte del depto*/

	//Para que solo muestre los reportes bimestrales del servicio social actual del alumno logueado.
	//Recordar que el alumno solo podra realizar un servicio social a la vez. Finalizado ya no.
	public function searchReportesBimestralesAlumno($no_ctrl)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->together = true;
		$criteria->with = array(
			'idserviciosocial' => array(
				'joinType' => 'JOIN',
			)
		);

		//No muestra ni Cancelados ni dados de Baja y los finalizados
		$criteria->alias='rb';
		$criteria->select = "*";
		$criteria->join = "inner join pe_planeacion.ss_servicio_social as ss on ss.id_servicio_social = rb.id_servicio_social";
		$criteria->condition = "(ss.id_estado_servicio_social > 1 and ss.id_estado_servicio_social < 6) and ss.no_ctrl = '$no_ctrl' ";
		$criteria->order = "rb.id_reporte_bimestral ASC";


		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'nocontrol'=>array(
						'asc'=>'idserviciosocial.no_ctrl',
						'desc'=>'idserviciosocial.no_ctrl DESC',
					),
					'*',
				),
			),
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}
	//Para que solo muestre los reportes bimestrales del servicio social actual del alumno logueado.
	//Recordar que el alumno solo podra realizar un servicio social a la vez. Finalizado ya no.

	/*Almacena todos los documentos (Reportes Bimestrales) del Servicio Social completados */
	public function searchXHistoricoDocumentosServicioSocial($id_servicio_social)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = "*";
		$criteria->condition = "id_servicio_social = '$id_servicio_social' ";
		$criteria->order = "id_reporte_bimestral ASC";

		$criteria->compare('id_reporte_bimestral',$this->id_reporte_bimestral);
		$criteria->compare('id_servicio_social',$this->id_servicio_social);
		$criteria->compare('bimestres_correspondiente',$this->bimestres_correspondiente,true);
		$criteria->compare('bimestre_final',$this->bimestre_final);
		$criteria->compare('valida_responsable',$this->valida_responsable,true);
		$criteria->compare('valida_oficina_servicio_social',$this->valida_oficina_servicio_social,true);
		$criteria->compare('observaciones_reporte_bimestral',$this->observaciones_reporte_bimestral,true);
		$criteria->compare('fecha_inicio_rep_bim',$this->fecha_inicio_rep_bim,true);
		$criteria->compare('fecha_fin_rep_bim',$this->fecha_fin_rep_bim,true);
		$criteria->compare('calificacion_reporte_bimestral',$this->calificacion_reporte_bimestral,true);
		$criteria->compare('envio_alum_evaluacion',$this->envio_alum_evaluacion,true);
		$criteria->compare('reporte_bimestral_actual',$this->reporte_bimestral_actual);
		$criteria->compare('"rfcResponsable"',$this->rfcResponsable,true);
        $criteria->compare('"rfcJefeOfiServicioSocial"',$this->rfcJefeOfiServicioSocial,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}
	/*Almacena todos los documentos (Reportes Bimestrales) del Servicio Social completados */
}

?>
