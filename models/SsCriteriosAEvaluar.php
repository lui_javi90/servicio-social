<?php

/**
 * This is the model class for table "pe_planeacion.ss_criterios_a_evaluar".
 *
 * The followings are the available columns in table 'pe_planeacion.ss_criterios_a_evaluar':
 * @property integer $id_criterio
 * @property string $descripcion_criterio
 * @property string $valor_a
 * @property integer $id_tipo_criterio
 * @property boolean $status_criterio
 * @property integer $posicion_criterio
 *
 * The followings are the available model relations:
 * @property SsEvaluacionBimestral[] $ssEvaluacionBimestrals
 */
class SsCriteriosAEvaluar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_planeacion.ss_criterios_a_evaluar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('descripcion_criterio, id_tipo_criterio', 'required'),
            array('id_tipo_criterio, posicion_criterio', 'numerical', 'integerOnly'=>true),
            array('descripcion_criterio', 'length', 'max'=>500),
            array('valor_a', 'length', 'max'=>2),
            array('status_criterio', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_criterio, descripcion_criterio, valor_a, id_tipo_criterio, status_criterio, posicion_criterio', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ssEvaluacionBimestrals' => array(self::HAS_MANY, 'SsEvaluacionBimestral', 'id_criterio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_criterio' => 'Id Criterio',
			'descripcion_criterio' => 'Descripción del Criterio',
			'valor_a' => 'Valor del Criterio (A)',
			'id_tipo_criterio' => 'Tipo de Criterio',
			'status_criterio' => 'Estatus del Criterio',
			'posicion_criterio' => 'Posición del Criterio',
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsCriteriosAEvaluar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
