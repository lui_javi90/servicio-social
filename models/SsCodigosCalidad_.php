<?php
class SsCodigosCalidad_ extends SsCodigosCalidad
{
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = "id_codigo_calidad ASC";

		$criteria->compare('id_codigo_calidad',$this->id_codigo_calidad);
		$criteria->compare('nombre_documento_digital',$this->nombre_documento_digital,true);
		$criteria->compare('codigo_calidad',$this->codigo_calidad,true);
		$criteria->compare('revision',$this->revision,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
?>