--Cambiar de schema public a schema pe_planeacion
drop function agregarReportesBimestralesAlumno(integer, integer, date);

--Limpiar tablas
delete from pe_planeacion.ss_actividades_servicio_social;
delete from pe_planeacion.ss_evaluacion_bimestral;
delete from pe_planeacion.ss_reportes_bimestral;
delete from pe_planeacion.ss_horario_dias_habiles_programas;
delete from pe_planeacion.ss_observaciones_servicio_social;
delete from pe_planeacion.ss_servicio_social;
delete from pe_planeacion.ss_solicitud_programa_servicio_social;
--delete from pe_planeacion.ss_responsable_programa_interno;
--delete from pe_planeacion.ss_responsable_programa_externo;
--delete from pe_planeacion.ss_programas;

---------------FIN TRIGGER CALCULA HORAS TOTALES POR DIA DEL PROGRAMA---------------
CREATE OR REPLACE FUNCTION pe_planeacion.ss_horas_totales_dia_programa() RETURNS trigger AS
$htdp$
DECLARE
	--Secuencia
	_id_horario integer;

BEGIN

      IF New.id_dia_semana IS NULL THEN
          RAISE EXCEPTION 'id_dia_semana cannot be null';
      END IF;

      IF New.hora_inicio IS NULL THEN
          RAISE EXCEPTION 'hora_inicio cannot be null';
      END IF;

      IF New.hora_fin IS NULL THEN
          RAISE EXCEPTION 'hora_fin cannot be null';
      END IF;

      IF (TG_OP = 'INSERT') THEN
      	--Obtenemos el ID
      	_id_horario = ( select MAX(id_horario) from pe_planeacion.ss_horario_dias_habiles_programas );
        IF (_id_horario IS NULL OR _id_horario = 0) THEN
        	_id_horario = 1;
        ELSE
        	_id_horario = _id_horario + 1;
        END IF;

        NEW.id_horario = _id_horario; --Secuencia
        NEW.horas_totales = NEW.hora_fin - NEW.hora_inicio;

      	RETURN NEW;
      ELSEIF (TG_OP = 'UPDATE') THEN
      	NEW.horas_totales =	NEW.hora_fin - NEW.hora_inicio;

        RETURN NEW;
      END IF;
END;
$htdp$ LANGUAGE 'plpgsql';

--drop trigger ss_horas_totales_dia_programa on pe_planeacion.ss_horario_dias_habiles_programas;

CREATE TRIGGER ss_horas_totales_dia_programa
BEFORE INSERT OR UPDATE ON pe_planeacion.ss_horario_dias_habiles_programas
FOR EACH ROW
EXECUTE PROCEDURE pe_planeacion.ss_horas_totales_dia_programa();
---------------FIN TRIGGER CALCULA HORAS TOTALES POR DIA DEL PROGRAMA---------------


--INSERTS
INSERT INTO pe_planeacion.ss_estado_servicio_social
VALUES(8, 'BAJA', 'El Servicio Social se dio de Baja por algun motivo, puede inscribirse en otro Programa dependiendo las fechas.');

INSERT INTO pe_planeacion.ss_estados_solicitud_programa
VALUES(5, 'BAJA');