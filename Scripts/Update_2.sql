﻿--Obtener los programas finalizados y cancelados del Supervisor con rfc especificado
select * from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
where rpi."rfcEmpleado" = 'ROCN781003P20' AND p.id_status_programa = 3 OR p.id_status_programa = 4
order by p.id_programa DESC;

--Obtener el supervisor principal del programa
select hemp."nmbCompletoEmp" from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
join public."H_empleados" hemp
on hemp."rfcEmpleado" = rpi."rfcEmpleado"
where p.id_programa = 32 AND rpi.superv_principal_int = true;

--Obtener servicio social de alumnos inscritos en un Programa (NO BAJA NI CANCELADO)
select * from pe_planeacion.ss_servicio_social ss
join pe_planeacion.ss_programas p
on p.id_programa = ss.id_programa
where p.id_programa = 32 AND (ss.id_servicio_social != 7 OR ss.id_servicio_social != 8);

--Valida Renovacion Programa en diferente registro vigente de servicio social
select p.id_programa from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
where rpi."rfcEmpleado" = 'ROCN781003P20';

--Programas que estan pendientes de asignacion de horas y alumnos del registro vigente de servicio social
select count(p.id_programa) as total_programas from pe_planeacion.ss_programas p
join pe_planeacion.ss_registro_fechas_servicio_social rfss
on rfss.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where p.id_status_programa = 6 AND rfss.ssocial_actual = true

--Validacion pendiente de Reportes Bimestrales por parte del supervisor del Programa (Programa y reg servicio vigente)
select count(rb.id_reporte_bimestral) as reportes_bim_sup
from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
join pe_planeacion.ss_servicio_social ss
on ss.id_programa = p.id_programa
join pe_planeacion.ss_reportes_bimestral rb
on rb.id_servicio_social = ss.id_servicio_social
join pe_planeacion.ss_registro_fechas_servicio_social rfss
on rfss.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where rpi."rfcEmpleado" = 'ROVG8201127G6' AND rfss.ssocial_actual = true AND
rb.envio_alum_evaluacion is not null AND (rb.valida_responsable is null OR rb."rfcResponsable" is null)

--Mostrar todos los Reportes Bimestrales que hayan sido validados por el Supervisor (RFC)
select * from pe_planeacion.ss_reportes_bimestral rb
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = rb.id_servicio_social
where rb."rfcResponsable" = 'ROVG8201127G6' AND rb.valida_responsable is not null AND ss.no_ctrl = '10030355'
order by id_reporte_bimestral DESC;

--Reportes Bimestrales pendientes de validacion por parte de oficina de servicio social (TODOS)
--id_tipo_programa = 1 -> INTERNO
select count(rb.id_reporte_bimestral) as rep_bim_pend from pe_planeacion.ss_programas p
join pe_planeacion.ss_servicio_social ss
on ss.id_programa = p.id_programa
join pe_planeacion.ss_reportes_bimestral rb
on rb.id_servicio_social = ss.id_servicio_social
where rb.valida_oficina_servicio_social is null AND p.id_tipo_programa = 1 AND
rb.envio_alum_evaluacion is not null AND (rb.valida_responsable is not null AND rb."rfcResponsable" is not null)

--Validar el finalizado del Programa del Supervisor en base a todos los servicios sociales terminados
select * from pe_planeacion.ss_programas p
join pe_planeacion.ss_servicio_social ss
on ss.id_programa = p.id_programa
where p.id_programa = 33 AND ss.id_estado_servicio_social != 5 and ss.id_estado_servicio_social != 6

--Validar que pueda finalizar el programa cuando tenga puros servicios en estado
select * from pe_planeacion.ss_programas p
join pe_planeacion.ss_servicio_social ss
on ss.id_programa = p.id_programa
where p.id_programa = 33 AND (ss.id_estado_servicio_social = 1 or ss.id_estado_servicio_social = 2 or
ss.id_estado_servicio_social = 3 or ss.id_estado_servicio_social = 4);

--Validar que No se pueda renovar un Programa en el mismo Periodo y Año
select p.id_programa from pe_planeacion.ss_programas p
join pe_planeacion.ss_registro_fechas_servicio_social rf
on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where rf.ssocial_actual = true AND p.id_status_programa = 3;

--Mostrar Programas por Periodo y Año en que se crearon (FINALIZADOS Y CANCELADO)
select * from pe_planeacion.ss_programas p
join pe_planeacion.ss_registro_fechas_servicio_social rf
on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where (p.id_status_programa = 3 OR p.id_status_programa = 4) AND rf.id_periodo = '1' AND rf.anio = 2020

--Verificar que no tenga un programa en el registro de servicio social actual
select * from pe_planeacion.ss_programas p
join pe_planeacion.ss_registro_fechas_servicio_social rf
on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
where rf.ssocial_actual = true AND rpi."rfcEmpleado" = 'ROVG8201127G6' AND
(p.id_status_programa = 1 OR p.id_status_programa = 8 OR p.id_status_programa = 6);

--Obtener los servicios sociales por periodo y año en que se crearon
--6 -> Finalizado
--7 -> Cancelado
select * from pe_planeacion.ss_servicio_social ss
join pe_planeacion.ss_programas p
on p.id_programa = ss.id_programa
join pe_planeacion.ss_registro_fechas_servicio_social rf
on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where (ss.id_estado_servicio_social = 6 OR ss.id_estado_servicio_social = 7) AND
rf.id_periodo = '1' AND rf.anio = 2020;

--Seleccionar por no control, rfc supervisor y periodo y año del servicio social los reportes bimestrales
select * from pe_planeacion.ss_reportes_bimestral rb
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = rb.id_servicio_social
join pe_planeacion.ss_programas p
on p.id_programa = ss.id_programa
join pe_planeacion.ss_registro_fechas_servicio_social rf
on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where rb."rfcResponsable" = 'ROVG8201127G6' AND rb.valida_responsable is not null AND ss.no_ctrl = '10030355' AND
rf.id_periodo = '1' AND rf.anio = 2020

--Obtener los servicio sociales por programas finazalidos
select * from pe_planeacion.ss_servicio_social ss
join pe_planeacion.ss_programas p
on ss.id_programa = p.id_programa
where p.id_programa = 33 AND (p.id_status_programa = 3 OR p.id_status_programa = 4)

--Numero de alumnos en estatus "En Liberacion" (4) del servicio social
select count(id_servicio_social) as tot_lib_alum
from pe_planeacion.ss_servicio_social
where id_estado_servicio_social = 4;

--Programas con Estatus de ALTA (1), es decir, Vigentes
--Los Cancelados (4) pasan directamente a Historico Programa
select * from pe_planeacion.ss_programas
where id_status_programa = 1;

--Alumnos que completaron su servicio social (5) que pasan a lista de escolares para asignar calificacion
select * from pe_planeacion.ss_servicio_social
where id_estado_servicio_social = 5

--Calificacion del servicio social
select SUM(calificacion_reporte_bimestral) from pe_planeacion.ss_reportes_bimestral where id_servicio_social = 69;

select (( select SUM(calificacion_reporte_bimestral)
			from pe_planeacion.ss_reportes_bimestral
            where id_servicio_social = 69 ) / 4);

--Filtrar por carrera de los alumnos
select * from pe_planeacion.ss_historico_totales_alumnos hta
join public."E_datosAlumno" eda
on eda."nctrAlumno" = hta.no_ctrl
join public."E_especialidad" esp
on esp."cveEspecialidad" = eda."cveEspecialidadAlu"
where hta.completo_servicio_social = true AND hta.calificacion_kardex = true AND
hta.horas_totales_servicio_social = 480 AND esp."cveEspecialidad" = 3

--Obtener el no. de reportes bimestrales externos que faltan por evaluar
--id_tipo_programa = 2 -> Externo
select count(rb.id_reporte_bimestral) as total_reportes_ext
from pe_planeacion.ss_programas p
join pe_planeacion.ss_servicio_social ss
on ss.id_programa = p.id_programa
join pe_planeacion.ss_reportes_bimestral rb
on rb.id_servicio_social = ss.id_servicio_social
where rb.valida_oficina_servicio_social is null AND rb.envio_alum_evaluacion is not null AND
rb.valida_oficina_servicio_social is null AND (ss.id_estado_servicio_social != 7 AND ss.id_estado_servicio_social != 6) AND
p.id_tipo_programa = 2;

--Programas Semestrales y Especiales vigentes en el registro de fechas vigente de servicio social
--id_periodo_programa = 1 -> SEMESTRAL
--id_periodo_programa = 3 -> ESPECIAL
--id_status_programa = 1 -> ALTA
select count(p.id_programa) as programas_vigentes
from pe_planeacion.ss_programas p
join pe_planeacion.ss_registro_fechas_servicio_social rf
on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
where (p.id_periodo_programa = 1 OR p.id_periodo_programa = 3) AND
p.id_registro_fechas_ssocial = 9 AND p.id_status_programa = 1 AND
rf.ssocial_actual = true;

--Jefe escolares para oficio de calificaciones de servicio social
select "rfcEmpleado", "nmbCompletoEmp", "nmbPuesto"
from public.h_ocupacionpuesto
where "cvePuestoGeneral" = '03' AND "cvePuestoParticular" = '04' AND
"cveDepartamentoEmp" = 9


--Lista de Supervisores que faltan de evaluar reportes bimestrales
select
rpi."rfcEmpleado",
(select "nmbCompletoEmp" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as name_supervisor,
(select "mailPersonal" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as correo_personal,
(select "mailEmpleado" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as correo_empleado
from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
join pe_planeacion.ss_servicio_social ss
on ss.id_programa = p.id_programa
where ss.id_estado_servicio_social = 2 OR ss.id_servicio_social = 3
limit 1;

--Lista de Supervisores que faltan de evaluar reportes bimestrales por
SELECT
rpi."rfcEmpleado",
(select "nmbEmpleado" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as nombre,
(select "apellPaterno" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as apellido_paterno,
(select "apellMaterno" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as apellido_materno,
(select "mailPersonal" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as correo_personal,
(select "mailEmpleado" from public."H_empleados" where "rfcEmpleado" = rpi."rfcEmpleado" ) as correo_empleado,
p.nombre_programa,
p.fecha_inicio_programa,
p.fecha_fin_programa,
(select periodo_programa from pe_planeacion.ss_periodos_programas
	where id_periodo_programa = p.id_periodo_programa ) as periodo_programa
from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = p.id_programa
where p.id_status_programa = 1 AND p.fecha_fin_programa <= now()


--Limpiar datos de las tablas para pruebas en modulo servicio social
delete from pe_planeacion.ss_status_servicio_social;
delete from pe_planeacion.ss_actividades_servicio_social;
delete from pe_planeacion.ss_evaluacion_bimestral;
delete from pe_planeacion.ss_historico_totales_alumnos;
delete from pe_planeacion.ss_horario_dias_habiles_programas;
delete from pe_planeacion.ss_observaciones_servicio_social;
delete from pe_planeacion.ss_reportes_bimestral;
delete from pe_planeacion.ss_responsable_programa_interno;
delete from pe_planeacion.ss_servicio_social;
delete from pe_planeacion.ss_solicitud_programa_servicio_social;
delete from pe_planeacion.ss_programas;
delete from pe_planeacion.ss_registro_fechas_servicio_social;

--Cambiar de schema funciones para evitar errores
--de public a pe_planeacion
drop function crearRegistroEvaluacionBimestral(integer);
drop function agregarReportesBimestralesAlumno(integer, integer, date);
drop function agregarReportesBimestralesExtraTemporal(integer, integer, date, date);
drop function getCalificacionReporteBimestral(integer);
drop function agregarActividadesServicioSocialAlumno(integer, integer, date);

--Agregar Campos a tablas del Modulo de Servicio Social
--alter table pe_planeacion.ss_programas add programa_renovado boolean;

alter table pe_planeacion.ss_configuracion add texto_leyenda_pdf_doc varchar(200);

