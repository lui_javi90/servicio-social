-----------FUNCION PARA CALCULAR LA CALIFICACION DEL REPORTE BIMESTRAL (SERVICIO SOCIAL INTERNO)(4)-----------
CREATE OR REPLACE FUNCTION pe_planeacion.getCalificacionReporteBimestral(id_reporte_bimestral integer)
RETURNS boolean AS $$
DECLARE
    bandera_principal boolean = false;
    /*Guardara el valor de id_reporte_bimestral, nuestra llave foranea*/
    id_reportebimestral integer;
    /*Sumatoria de criterios*/
    calificacion_reporte NUMERIC(3,0);

BEGIN
    id_reportebimestral := id_reporte_bimestral;
    calificacion_reporte := 0;

    IF id_reportebimestral > 0 THEN
        calificacion_reporte = (select SUM(eb.evaluacion_b) from pe_planeacion.ss_evaluacion_bimestral as eb
        where eb.id_reporte_bimestral = id_reportebimestral);

        UPDATE pe_planeacion.ss_reportes_bimestral
        SET calificacion_reporte_bimestral = calificacion_reporte
        WHERE ss_reportes_bimestral.id_reporte_bimestral = id_reportebimestral;

        bandera_principal = true;
    END IF;

    return bandera_principal;

END;
$$ LANGUAGE 'plpgsql';


--drop function getCalificacionReporteBimestral(integer);

--select getCalificacionReporteBimestral(1) as valido;
-----------FIN FUNCION PARA CALCULAR LA CALIFICACION DEL REPORTE BIMESTRAL (SERVICIO SOCIAL INTERNO)-----------
