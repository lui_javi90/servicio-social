--Consulta para obetener los datos del alumno para la Carta de Presentacion
select 
ss.fecha_registro,
ss.id_servicio_social as folio,
(select extract(year from ss.fecha_registro)) as anio_servicio_social,
spss.valida_solicitud_supervisor_programa as validacion_jefesup,
spss.id_estado_solicitud_programa_supervisor as estatus_validacion_validacion_jefesup,
ss.nombre_jefe_depto,
ss."departamentoSupervisorJefe",
ss."empresaSupervisorJefe",
(select "nmbAlumno" from public."E_datosAlumno" where "nctrAlumno" = ss.no_ctrl) as nombre_alumno,
ss.no_ctrl,
(select "semAlumno" from public."E_datosAlumno" where "nctrAlumno" = ss.no_ctrl) as semestre_alumno,
(select esp."dscEspecialidad" from public."E_datosAlumno" eda
join public."E_especialidad" esp
on esp."cveEspecialidad" = eda."cveEspecialidadAlu" 
where eda."nctrAlumno" = ss.no_ctrl) as carrera_alumno,
pss.horas_totales,
pss.nombre_programa,
(case 
when pss.id_periodo_programa = 1 then 'seis'
when pss.id_periodo_programa = 2 then 'doce'
when pss.id_periodo_programa = 3 then 'dos'
end) as periodo_programa,
(select "siglaGradoMaxEstudio" from public."H_empleados" where "rfcEmpleado" = ss."rfcJefeDeptoVinculacion") as grado_max_estudios_jefvinculacion,
(select "nmbCompletoEmp" from public."H_empleados" where "rfcEmpleado" = ss."rfcJefeDeptoVinculacion") as jefe_vinculacion,
ss."rfcJefeDeptoVinculacion"
from pe_planeacion.ss_servicio_social ss
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_solicitud_programa_servicio_social spss
on spss.id_solicitud_programa = ss.id_servicio_social
where ss.id_servicio_social = 3