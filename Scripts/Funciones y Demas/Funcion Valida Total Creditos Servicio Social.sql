﻿CREATE OR REPLACE FUNCTION public."validaSiCubreTotalCreditosServicioSocialAlumno"(id_estado_ssocial integer)
RETURNS void AS $$
DECLARE

	/*Alumnos en Estatus "En Liberacion" (4)*/
	row_alumn_servicio_social RECORD;

	/*Guardamos la variable del estado de servicio social que viene por parametro*/
    no_control CHAR(9); --Almacenamos el no. de control del Alumno
    id_estado_serv_social INTEGER; --ID del estado de Servicio Social DEFAULT (6)
    horas_libero_alumno INTEGER; --Historico del horas de servicio social liberadas por el Alumno
    horas_liberara_alumno INTEGER; --Horas que va liberar en el actual servicio social
    horas_totales_sum INTEGER; --Horas totales del Alumno entre las historicas y las que liberara con el servicio social
    total_horas_a_cubrir INTEGER; --Horas que se deben cubrir para liberar el Servicio Social
    calificacion_servicio_social INTEGER; --Calificacion reportes bimestrales
    tipo_programa INTEGER; --Si es Programa SEMESTRAL, ANUAL O ESPECIAL
    divisor INTEGER; --Numero de Reportes bimestrales por el Tipo de Programa
    cal_servicio_social NUMERIC(3,0); --Calificacion del Servicio Social

    /*Califcacion FINAL de Servicio Social del Alumno*/
    cal_final_servicio_social NUMERIC(3,0); --Calificacion final del Servicio Social del Alumno
    suma_total_calificaciones_alumno_ssocial NUMERIC(3,0); --Suma total de las calificaciones del Alumno
    no_servicios_sociales_completo INTEGER; --No. de veces que realizó el servicio social y lo completó

    /*Calificaciones de Servicios Sociales Anteriores*/
    suma_total_calificaciones_alumno_ssocial_ant INTEGER; --Calificaciones anteriores del Servicio Social del Alumno (Antes del Modulo)
    no_servicios_sociales_ant INTEGER; --No. de Servicios Sociales registrados del Alumno

BEGIN

	--Mientras haya alumnos de servicio social en estatus "En Liberacion" (4) calcular si ya completa las 480 horas
	FOR row_alumn_servicio_social IN select *
    							from pe_planeacion.ss_servicio_social
                                where id_estado_servicio_social = 4
    LOOP

      --Almacenamos el no. de control del Alumno
      no_control := trim(row_alumn_servicio_social.no_ctrl);

      --Obtenemos las horas que va a liberar el alumno con el actual Servicio Social
      horas_liberara_alumno := ( select horas_totales
      								from pe_planeacion.ss_programas
                                	where id_programa = row_alumn_servicio_social.id_programa
      							);

      --Obtenemos el historico de horas de servicio social que lleva el alumno
      horas_libero_alumno := ( select horas_totales_servicio_social
      							from pe_planeacion.ss_historico_totales_alumnos
                                where no_ctrl = no_control
                             );

      --Sumamos las horas de servicio social del alumno, historicas y actuales a liberar del alumno
      horas_totales_sum := ( horas_libero_alumno + horas_liberara_alumno);

      --Horas totales de Servicio Social (480)
      total_horas_a_cubrir := ( select horas_max_servicio_social
      							from pe_planeacion.ss_configuracion
                                where id_configuracion = 1
                              );

      --Calculamos su calificacion del servicio social ACTUAL

      	--Si es Programa SEMESTRAl, ANUAl O ESPECIAL
      	tipo_programa = ( select id_tipo_programa
                          from pe_planeacion.ss_programas
                          where id_programa = row_alumn_servicio_social.id_programa
                        );

      	/*Entre cuanto se dividira el servicio social*/
		/*Obtenemos el periodo del servicio social Semestral(1), Anual(2) y Una vez(1)*/
        CASE tipo_programa
        WHEN 1 THEN
        	divisor := 4;
        WHEN 2 THEN
        	divisor := 7;
        WHEN 3 THEN
        	divisor := 2;
        ELSE
       		divisor := 0;
        END CASE;

        cal_servicio_social := (( select SUM(calificacion_reporte_bimestral)
        								  from pe_planeacion.ss_reportes_bimestral
                                          where id_servicio_social = row_alumn_servicio_social.id_servicio_social ) / divisor);

        --Entra al IF si ya cumple las horas totales como limite de Servicio Social
        IF (horas_totales_sum >= total_horas_a_cubrir AND divisor != 0)
        THEN
              --Si cubre las horas totales pasa a estatus "Completado" (5) y se va a Lista de Escolares (profe Oscar)
              UPDATE pe_planeacion.ss_servicio_social
              SET id_estado_servicio_social = 5, calificacion_servicio_social = cal_servicio_social
              WHERE id_servicio_social = row_alumn_servicio_social.id_servicio_social AND
                    no_ctrl = no_control;

              /*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicio_social */
              suma_total_calificaciones_alumno_ssocial = ( SELECT (CASE WHEN sum(calificacion_servicio_social) = 0
                          THEN 0 ELSE sum(calificacion_servicio_social) END)
                          FROM pe_planeacion.ss_servicio_social
                          WHERE no_ctrl = no_control AND id_estado_servicio_social = 6 );

              /*NUMERO DE TODOS LOS SERVICIOS SOCIALES QUE COMPLETO */
              no_servicios_sociales_completo = ( SELECT (CASE WHEN COUNT(id_servicio_social) = 0
                          THEN 0 ELSE COUNT(id_servicio_social) END)
                          FROM pe_planeacion.ss_servicio_social
                          WHERE no_ctrl = no_control AND id_estado_servicio_social = 6 );

              /*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicios_sociales_anteriores */
              suma_total_calificaciones_alumno_ssocial_ant = ( SELECT (CASE WHEN sum(calificacion_servicio_social) = 0
                          THEN 0 ELSE sum(calificacion_servicio_social) END)
                          FROM pe_planeacion.ss_servicios_sociales_anteriores
                          WHERE no_ctrl = no_control );

              /*NUMERO DE TODOS LOS SERVICIOS SOCIALES QUE COMPLETO (ANTERIORES)*/
              no_servicios_sociales_ant = ( SELECT (CASE WHEN COUNT(id_servicio_social_anterior) = 0
                          THEN 0 ELSE COUNT(id_servicio_social_anterior) END)
                          FROM pe_planeacion.ss_servicios_sociales_anteriores
                          WHERE no_ctrl = no_control );

              --Calificacion FINAL del Servicio Social del Alumno
              cal_final_servicio_social = ((suma_total_calificaciones_alumno_ssocial + suma_total_calificaciones_alumno_ssocial_ant) / (no_servicios_sociales_completo + no_servicios_sociales_ant))::INTEGER;

              /*Agregar las horas que realizó el alumno para el conteo final historico de horas de servicio social,
              su calificacion final de servicio social y cambiar bandera a TRUE de que completo el servicio social*/
              UPDATE pe_planeacion.ss_historico_totales_alumnos
              SET horas_totales_servicio_social = total_horas_a_cubrir, fecha_modificacion_horas_totales = now(),
                  calificacion_final_servicio_social = cal_final_servicio_social, completo_servicio_social = true;

        ELSE
              --Si aun le falta para cubrir las horas totales pasa a estatus "Finalizado" (6)
              UPDATE pe_planeacion.ss_servicio_social
              SET id_estado_servicio_social = 6, calificacion_servicio_social = cal_servicio_social
              WHERE id_servicio_social = row_alumn_servicio_social.id_servicio_social AND
                    no_ctrl = no_control;

              --Agregar las horas que realizó el alumno para el conteo historico de horas de servicio social
              UPDATE pe_planeacion.ss_historico_totales_alumnos
              SET horas_totales_servicio_social = horas_totales_sum, fecha_modificacion_horas_totales = now()
              WHERE no_ctrl = no_control;

        END IF;


    END LOOP;
    --Mientras haya alumnos de servicio social en estatus "En Liberacion" (4) calcular si ya completa las 480 horas

END;
$$ LANGUAGE plpgsql;

--Probando funcion
--drop function public."validaSiCubreTotalCreditosServicioSocialAlumno"(integer);
--select public."validaSiCubreTotalCreditosServicioSocialAlumno"(5);

alter table pe_planeacion.ss_configuracion add texto_leyenda_pdf_doc varchar(200);