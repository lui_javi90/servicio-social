-------------INICIO FUNCION PARA GENERAR REPORTES BIMESTRALES DE LOS SERVICIO SOCIALES (UNICA VEZ)--------------
CREATE OR REPLACE FUNCTION pe_planeacion.agregarReportesBimestralesExtraTemporal(tipo_servsocial integer , idserviciosocial integer, fec_inicio date, fec_fin date)
RETURNS boolean AS $$
DECLARE
    /*Para saber si no hubo errores de insercion*/
    bandera_principal boolean = false;
    /*Bandera para saber cuando es el ultimo reporte bimestral a generar*/
    bandera boolean = false;
    /*Guardamos la fecha de inicio para generar la fecha final del servicio social*/
    fec_ini date;
    /*Almacena las fechas de entrega de los reportes bimestrales*/
    fec_final date;
    /*Almacenan el tipo de servicio social y el id del servicio social del alumno*/
    tiposervsocial integer;
    /*id dels servicio social*/
    ssocial integer;
    /*Almacena el numero de reportes a generar dependiendo del tipo de servicio social*/
    no_reportes integer;
    /*Variables de control que funciona como contador de los reporte a generar*/
    contador_bimestres integer = 1;
    /*secuencia*/
    id_contador integer;
BEGIN
    /*Instanciamos las variables con la fecha de inicio y fecha fin que vienen por parametro*/
    fec_ini := fec_inicio;
    fec_final := fec_fin;
    /*tipo de servicio y llave foranea a insertar en la tabla reportes_bimestral*/
    tiposervsocial := tipo_servsocial;
    ssocial = idserviciosocial;

    /*Obtenemos el numero de reportes a generar que seran 2 del servicio social UNICA VEZ*/
    IF (tiposervsocial = 3) THEN
        no_reportes := 2;
    ELSE
	   no_reportes := 0;
    END If;

     /*Si viene una fecha de inicio y un tipo de servicio social entonces ejecuta el codigo*/
    IF (no_reportes != 0 AND (fec_ini IS NOT NULL AND fec_final IS NOT NULL)) THEN
	   /*Ciclo que crea los registros para los reportes bimestrales del alumno dependiendo de su tipo de servicio social*/
        WHILE contador_bimestres <= no_reportes
        LOOP
        /*funciona como id de la tabla ss_reportes_bimestrales*/
        id_contador = (select MAX(id_reporte_bimestral) from pe_planeacion.ss_reportes_bimestral);

        /*Si entra quiere decir que no hay registros en la tabla, asignamoc 1 para que empieza ahi el registro de la llave primaria*/
        IF (id_contador IS NULL OR id_contador = 0) THEN
            id_contador = 1;
        ELSE
            id_contador = id_contador + 1;
        END IF;

        IF (contador_bimestres = no_reportes) THEN
            bandera = true;
            /*Insertamos el numero de registros dependiendo del tipo de servicio social UNICA VEZ*/
            insert into pe_planeacion.ss_reportes_bimestral values(id_contador, ssocial, contador_bimestres, bandera, null, null, ' ', fec_ini, fec_final, 0, false, null, null, null);
        ELSE
            /*Insertamos el numero de registros dependiendo del tipo de servicio social UNICA VEZ*/
            insert into pe_planeacion.ss_reportes_bimestral values(id_contador, ssocial, contador_bimestres, bandera, null, null, ' ', fec_ini, fec_final, 0, true, null, null, null);
        END IF;

       /*Aumenta contador*/
       contador_bimestres := contador_bimestres + 1;

       END LOOP;
       /*Si todo salio bien entonces regresara true y sabremos que se inserto correctamente los registros para los reportes bimestrales*/
       bandera_principal := true;

       END IF;

       return bandera_principal;
END;
$$ LANGUAGE 'plpgsql';

--drop function agregarReportesBimestralesExtraTemporal(integer, integer, date, date);

--select agregarReportesBimestralesExtraTemporal(3);

-----------FIN FUNCION PARA GENERAR REPORTES BIMESTRALES DE LOS SERVICIO SOCIALES (UNICA VEZ)------------