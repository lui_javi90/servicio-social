--Consulta para reportes bimestrales supervisor 
select
eda."nmbAlumno" as nombre_alumno,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera_alumno,
ss.no_ctrl,
pss.nombre_programa,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
rb.bimestres_correspondiente,
(case 
when (rb.bimestre_final = false) then ''
when (rb.bimestre_final = true) then 'X'
end) as bimestre_final,
ss."rfcSupervisor",
ss.nombre_supervisor,
ss.cargo_supervisor,
rb.valida_responsable,
ss."rfcJefeOficServicioSocial",
(select "nmbCompletoEmp" from public.h_ocupacionpuesto where "rfcEmpleado" = ss."rfcJefeOficServicioSocial") as namejefeserviciosocial,
rb.valida_oficina_servicio_social,
rb.calificacion_reporte_bimestral,
rb.observaciones_reporte_bimestral
from pe_planeacion.ss_servicio_social ss
join public."E_datosAlumno" eda
on eda."nctrAlumno" = ss.no_ctrl
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_reportes_bimestral rb
on rb.id_servicio_social = ss.id_servicio_social
where rb.id_reporte_bimestral = 1 AND ss.no_ctrl = '13030026' AND ss.id_servicio_social = 3

--Obtener el rfc del director actual del instituto
select * from public.h_ocupacionpuesto where "cvePuestoGeneral" = '01' AND "cvePuestoParticular" = '01';

--RFC del supervisor del programa interno
select rpi."rfcEmpleado" from pe_planeacion.ss_programas pss
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = pss.id_programa
where rpi.id_programa = 1 AND rpi.superv_principal_int = true;

--RFC del supervisor del programa externo
select rpe."rfcSupervisor" from pe_planeacion.ss_programas pss
join pe_planeacion.ss_responsable_programa_externo rpe
on rpe.id_programa = pss.id_programa
where rpe.id_programa = 1 AND rpe.superv_principal_ext = true;

/*****************************************/
--Eliminar datos de las tablas para pruebas
delete from pe_planeacion.ss_evaluacion_bimestral;
delete from pe_planeacion.ss_criterios_a_evaluar;
delete from pe_planeacion.ss_reportes_bimestral;
delete from pe_planeacion.ss_actividades_servicio_social;
delete from pe_planeacion.ss_observaciones_servicio_social;
delete from pe_planeacion.ss_servicio_social;
delete from pe_planeacion.ss_solicitud_programa_servicio_social;
delete from pe_planeacion.ss_historico_totales_alumnos;
delete from pe_planeacion.ss_status_servicio_social;
delete from pe_planeacion.ss_responsable_programa_interno;
delete from pe_planeacion.ss_responsable_programa_externo;
delete from pe_planeacion.ss_programas;
/***************************************/

delete from pe_planeacion.ss_horario_dias_habiles_programas where id_programa != 2;
delete from pe_planeacion.ss_programas where id_programa != 2;

--Obtener el cargo del supervisor INTERNO
select * from public.h_ocupacionpuesto where "rfcEmpleado" = 'RAJL9008175V0';

--Obtener el cargo del supervisor EXTERNO
select * from pe_planeacion.ss_supervisores_programas where "rfcSupervisor" = 'RAJL9008175V0';

--Obtener la empresa supervisor INTERNA
select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1;

--Obtener la empresa del supervisor EXTERNO
select ur.nombre_unidad_receptora from pe_planeacion.ss_supervisores_programas sp
join pe_planeacion.ss_unidades_receptoras ur
on ur.id_unidad_receptora = sp.id_unidad_receptora 
where sp."rfcSupervisor" = 'RAJL9008175V0';

--Obtener RFC jefe de vinculacion
select * from public.h_ocupacionpuesto where "cvePuestoGeneral" = '03' AND "cvePuestoParticular" = '02';

--Obtener RFC jefe de Oficina de Servicio Social
select * from public.h_ocupacionpuesto where "cvePuestoGeneral" = '04' AND "cvePuestoParticular" = '02' AND "cveDepartamentoEmp" = '06';

--Obtener departamento supervisor y jefe depto interno (es el mismo)
select * from public.h_ocupacionpuesto where "rfcEmpleado"= 'RAJL9008175V0';

--Obtener el departamento del supervisor y le jefe depto externo(es el mismo)
select * from pe_planeacion.ss_supervisores_programas where "rfcSupervisor" = 'RAJL9008175V0';

--Obtener el nombre del jefe depto. del depto. del supervisor INTERNO
select * from public.h_ocupacionpuesto where "rfcEmpleado" = 'BABI8109199JA';

--Obtenemos el jefe del depto por la clave del departamento
select * from public.h_ocupacionpuesto where "cveDepartamentoEmp" = 4 AND ("cvePuestoGeneral" = '02' AND "cvePuestoParticular" = '03');

--Obtener el nombre del jefe depto. del depto. del supervisor EXTERNO
select nombre_jefe_depto from pe_planeacion.ss_supervisores_programas where "rfcSupervisor" = 'RAJL9008175V0';

--Obtener los reportes bimestrales de cada supervisor que ya hayan sido validados y enviados por el alumno y no hayan sido validados y enviados por el jefe de ofic. de servicio
--(EXTERNO)
select * 
from pe_planeacion.ss_reportes_bimestral rb
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = rb.id_servicio_social
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_responsable_programa_externo rpe
on rpe.id_programa = pss.id_programa
where rpe."rfcSupervisor" = 'RAJL9008175V0' AND 
(rb.envio_alum_evaluacion is not null AND rb.valida_responsable is null AND rb.valida_oficina_servicio_social is null AND rb.reporte_bimestral_actual = true)
order by rb.id_servicio_social

--(INTERNO)
select * 
from pe_planeacion.ss_reportes_bimestral rb
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = rb.id_servicio_social
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = pss.id_programa
where rpi."rfcEmpleado" = 'RAJL9008175V0' AND 
(rb.envio_alum_evaluacion is not null AND rb.valida_responsable is null AND rb.valida_oficina_servicio_social is null AND rb.reporte_bimestral_actual = true)
order by rb.id_servicio_social


--Verificar si hay servicio social en estado activo
select * from pe_planeacion.ss_registro_fechas_servicio_social where ssocial_actual = true;

--Obtener los supervisores que pertenecen solo a dicho rpograma
select hemp."rfcEmpleado" from public."H_empleados" hemp 
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi."rfcEmpleado" = hemp."rfcEmpleado"
where rpi.id_programa = 3

--Agregar campo de empresa a la tabla ss_servicios_sociales_pasados
ALTER TABLE pe_planeacion.ss_servicios_sociales_anteriores ADD COLUMN nombre_empresa varchar(200);

--Obtener el programa actual del supervisor INTERNO
select * from pe_planeacion.ss_programas pss
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = pss.id_programa
where rpi."rfcEmpleado" = 'RAJL9008175V0' AND pss.id_status_programa = 1

--Obtener el programa actual del supervisor EXTERNO
select * from pe_planeacion.ss_programas pss
join pe_planeacion.ss_responsable_programa_externo rpe
on rpe.id_programa = pss.id_programa
where rpe."rfcSupervisor" = 'RAJL9008175V0' AND pss.id_status_programa = 1

--Obtener las conversaciones con el supervisor y el jefe de oficina de servicio social (1 - admin, 2-supervisor y 3 -alumno)
select * from pe_planeacion.ss_observaciones_servicio_social oss
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = oss.id_servicio_social
where oss.id_servicio_social = 3 AND oss.tipo_observacion_emisor != 3

delete from pe_planeacion.ss_observaciones_servicio_social;

--Agregar campos a la tabla pe_planeacion.ss_reportes_bimestrales
ALTER TABLE pe_planeacion.ss_reportes_bimestral ADD COLUMN "rfcResponsable" varchar(13);
ALTER TABLE pe_planeacion.ss_reportes_bimestral ADD COLUMN "rfcJefeOfiServicioSocial" varchar(13);
--
--ALTER TABLE pe_planeacion.ss_reportes_bimestral DROP COLUMN "rfcResponsable";
--ALTER TABLE pe_planeacion.ss_reportes_bimestral DROP COLUMN "rfcJefeOfiServicioSocial";

--ALTER TABLE pe_planeacion.ss_status_servicio_social ALTER COLUMN val_serv_social DROP default
--delete from pe_planeacion.ss_status_servicio_social;

--Agregar alumnos que no hayan egresado o esten en baja definitiva
INSERT INTO pe_planeacion.ss_status_servicio_social (no_ctrl) SELECT "nctrAlumno" FROM public."E_datosAlumno";
--
UPDATE pe_planeacion.ss_status_servicio_social
SET val_serv_social = false;

--Datos del supervisor del programa con ese id
select * from pe_planeacion.ss_responsable_programa_interno rpi
join public."H_empleados" hemp 
on hemp."rfcEmpleado" = rpi."rfcEmpleado"
where rpi.id_programa = 8

--Verificamos si el supervisor interno tiene algun programa en curso
select * from pe_planeacion.ss_responsable_programa_interno rpi
join pe_planeacion.ss_programas pss
on pss.id_programa = rpi.id_programa
where rpi."rfcEmpleado" = 'LUMC781102KT1' AND pss.id_status_programa != 1;


