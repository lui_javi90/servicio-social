---Consulta para la Solicitud de Servicio Social---
select
eda."nmbAlumno"  as namealumno,
ss.no_ctrl,
(edpa."cllDomAlu" || ' #' || edpa."numDomAlu" || ' ' || edpa."colDomAlu") as domicilio,
edpa."telDomAlu" as telefono,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera,
eda."semAlumno",
(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = sp.id_unidad_receptora) as nameempresa,
(sp.nombre_supervisor || ' ' || sp.apell_paterno || ' ' || sp.apell_materno) as namesupervisor,
sp.cargo_supervisor,
pss.nombre_programa,
(select periodo_programa from pe_planeacion.ss_periodos_programas where id_periodo_programa=pss.id_periodo_programa) as periodo_programa,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
pss.actividades_especificas_realizar as actividades,
pss.id_clasificacion_area_servicio_social as areasocial,
spss.fecha_solicitud_programa,
spss.valida_solicitud_alumno
from pe_planeacion.ss_solicitud_programa_servicio_social spss
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = spss.id_solicitud_programa
join public."E_datosAlumno" eda
on eda."nctrAlumno" = ss.no_ctrl
join public."E_datosPersonalesAlumno" edpa
on edpa."nctrAlumno" = eda."nctrAlumno"
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_supervisores_programas sp
on sp."rfcSupervisor" = pss."rfcSupervisor"
where spss.id_solicitud_programa = 1
---Consulta para la Solicitud de Servicio Social---

---Consultas para Reportes Actividades de Servicio Social---
select 
eda."nmbAlumno" as namealumno,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera,
ss.no_ctrl,
pss.nombre_programa,
pss.descripcion_objetivo_programa,
pss.fecha_inicio_programa
from public."E_datosAlumno" eda
join pe_planeacion.ss_servicio_social ss
on ss.no_ctrl = eda."nctrAlumno"
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
where ss.id_servicio_social = 1;

select
ass.id_actividad_servicio_social ,ass.id_mes, ass.actividad_mensual
from pe_planeacion.ss_actividades_servicio_social ass
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = ass.id_servicio_social
where ass.id_servicio_social = 1
order by ass.id_actividad_servicio_social ASC
---Consultas para Reportes Actividades de Servicio Social---

---Consultas para Reportes Bimestrales---
select 
eda."nmbAlumno" as namealumno,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera,
ss.no_ctrl,
pss.nombre_programa as programa,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
rb.bimestres_correspondiente,
rb.bimestre_final,
case 
when (pss.id_tipo_programa = 1) then (select "nmbCompletoEmp" from public.h_ocupacionpuesto where "rfcEmpleado" = rpi."rfcEmpleado")
when (pss.id_tipo_programa = 2) then (select nombre_supervisor || ' ' || apell_paterno|| ' ' || apell_materno from pe_planeacion.ss_supervisores_programas where "rfcSupervisor" = rpe."rfcSupervisor")
end as namesupervisor,
--(select (nombre_supervisor || ' ' || apell_paterno||' '||apell_materno) from pe_planeacion.ss_supervisores_programas where "rfcSupervisor" = pss."rfcSupervisor") as namesupervisor,
--(select cargo_supervisor from pe_planeacion.ss_supervisores_programas where "rfcSupervisor" = pss."rfcSupervisor") as cargosupervisor,
(select "nmbCompletoEmp" from public.h_ocupacionpuesto where "cveDepartamentoEmp" = 6 AND "cvePuestoGeneral" = '04' AND "cvePuestoParticular" = '02') as namejefeserviciosocial,
(select "nmbPuesto" from public.h_ocupacionpuesto where "cveDepartamentoEmp" = 6 AND "cvePuestoGeneral" = '04' AND "cvePuestoParticular" = '02') as puestojefeserviciosocial,
rb.calificacion_reporte_bimestral,
rb.observaciones_reporte_bimestral,
rb.valida_responsable,
rb.valida_oficina_servicio_social
from pe_planeacion.ss_servicio_social ss
join public."E_datosAlumno" eda
on eda."nctrAlumno" = no_ctrl
join pe_planeacion.ss_reportes_bimestral rb
on rb.id_servicio_social = ss.id_servicio_social
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = pss.id_programa
join pe_planeacion.ss_responsable_programa_externo rpe
on rpe.id_programa = pss.id_programa
where rb.id_reporte_bimestral = 1 AND ss.no_ctrl = '13030026' AND ss.id_servicio_social = 2
---Consultas para Reportes Bimestrales---

---Consulta para Carta de Aceptacion---
select 
(select "nmbCompletoEmp" from public.h_ocupacionpuesto where "cveDepartamentoEmp" = 1 AND "cvePuestoGeneral" = '01' AND "cvePuestoParticular" = '01') as name_dir_empresa,
(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1) as name_empresa,
(select "nmbPuesto" from public.h_ocupacionpuesto where "cveDepartamentoEmp" = 1 AND "cvePuestoGeneral" = '01' AND "cvePuestoParticular" = '01') as puesto_dir_empresa
from pe_planeacion.ss_servicio_social ss
where ss.id_servicio_social = 1
---Consulta para Carta de Aceptacion---

--Obtener Informacion del Alumno y su Servicio Social para la vista de la evaluacion del Reporte Bimestral
select
eda."nmbAlumno" as name_alumno,
ss.no_ctrl,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera,
pss.nombre_programa as programa,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
pss.id_periodo_programa as periodo_servicio_social,
pss.id_tipo_programa,
rb.bimestres_correspondiente,
rb.bimestre_final,
rb.fecha_inicio_rep_bim,
rb.fecha_fin_rep_bim,
rb.calificacion_reporte_bimestral,
(select anio from pe_planeacion.ss_registro_fechas_servicio_social where id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial) as a?o_servicio_social
from pe_planeacion.ss_servicio_social ss
join public."E_datosAlumno" eda
on eda."nctrAlumno" = ss.no_ctrl
join pe_planeacion.ss_reportes_bimestral rb
on rb.id_servicio_social = ss.id_servicio_social
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
where ss.id_servicio_social = 1 AND rb.id_reporte_bimestral = 1
--Obtener Informacion del Alumno y su Servicio Social para la vista de la evaluacion del Reporte Bimestral

---Consulta datos alumno para vista Observaciones
select
eda."nmbAlumno" as name_alumno,
ss.no_ctrl,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera,
pss.nombre_programa as programa,
ss.id_estado_servicio_social,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
(select periodo_programa from pe_planeacion.ss_periodos_programas where id_periodo_programa = pss.id_periodo_programa) periodo_servsocial,
ss.calificacion_servicio_social
from pe_planeacion.ss_servicio_social ss
join public."E_datosAlumno" eda
on eda."nctrAlumno" = ss.no_ctrl
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
where ss.id_servicio_social = 1
---Consulta datos alumno para vista Observaciones

--Obtener las observaciones del Servicio Social ACTUAL del alumno
select COUNT(obs.id_observacion) as "totalObsAlumno" from pe_planeacion.ss_servicio_social ss 
join pe_planeacion.ss_observaciones_servicio_social obs
on obs.id_servicio_social = ss.id_servicio_social
where ss.no_ctrl = '13030026' AND (ss.id_estado_servicio_social > 0 AND ss.id_estado_servicio_social < 6) AND obs.fue_leida = false AND obs.tipo_envio_observacion != 3;

--Obtener las observaciones enviadas al Supervisor por el programa
select COUNT(obs.id_observacion) as "totalObsSupervisor" from pe_planeacion.ss_servicio_social ss
join pe_planeacion.ss_observaciones_servicio_social obs
on obs.id_servicio_social = ss.id_servicio_social
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
where pss."rfcSupervisor" = 'RAJL9008175V0' AND (ss.id_estado_servicio_social > 0 AND ss.id_estado_servicio_social < 6) AND obs.fue_leida = false AND obs.tipo_envio_observacion != 2;

--Eliminar datos de las tablas para pruebas
delete from pe_planeacion.ss_evaluacion_bimestral;
delete from pe_planeacion.ss_criterios_a_evaluar;
delete from pe_planeacion.ss_reportes_bimestral;
delete from pe_planeacion.ss_actividades_servicio_social;
delete from pe_planeacion.ss_observaciones_servicio_social;
delete from pe_planeacion.ss_servicio_social;
delete from pe_planeacion.ss_solicitud_programa_servicio_social;
delete from pe_planeacion.ss_programas;

select rpe."rfcSupervisor" from pe_planeacion.ss_supervisores_programas sp
join pe_planeacion.ss_responsable_programa_externo rpe
on rpe."rfcSupervisor" = sp."rfcSupervisor"
where rpe.id_programa = 2

--Obtener el nombre del supervisor para internos y externos
select hemp."nmbEmpleado" from pe_planeacion.ss_responsable_programa_interno rpi
			join public."H_empleados" hemp
			on hemp."rfcEmpleado" = rpi."rfcEmpleado"
			where rpi.id_programa = 2 AND superv_principal_int = true

select (sp.nombre_supervisor || ' ' || sp.apell_paterno || ' ' || sp.apell_materno) as name from pe_planeacion.ss_responsable_programa_externo rpe
                        join pe_planeacion.ss_supervisores_programas sp
                        on sp."rfcSupervisor" = rpe."rfcSupervisor"
                        where rpe.id_programa = 2 AND superv_principal_ext = true
                        
--Obtener el nombre del supervisor para internos y externos
select rpi."rfcEmpleado" 
from pe_planeacion.ss_programas pss
join pe_planeacion.ss_responsable_programa_interno rpi
on rpi.id_programa = pss.id_programa
where rpi.id_programa = 2 AND rpi.superv_principal_int = true

select "nmbPuesto" from public.h_ocupacionpuesto where rfcEmpleado = ''

select cargo_supervisor from pe_planeacion.ss_programas pss
join pe_planeacion.ss_responsable_programa_externo rpe
on rpe.id_programa = pss.id_programa
join pe_planeacion.ss_supervisores_programas sp
on sp."rfcSupervisor" = rpe."rfcSupervisor"
where rpe.id_programa = 2 AND rpe.superv_principal_ext = true
--Obtener el nombre del supervisor para internos y externos


---Observaciones 
select COUNT(oss.id_observacion) as "totalObsAlumno" from pe_planeacion.ss_servicio_social ss 
        join pe_planeacion.ss_observaciones_servicio_social oss
        on oss.id_servicio_social = ss.id_servicio_social
        where oss.receptor_observ = '13030026' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6) AND 
        oss.fue_leida = false
        
        
--Obtener una converdacion entre alumno y jefe de servicio social (Vista jefe oficina servicio social)
select oss.id_observacion, oss.id_servicio_social, oss.observacion
from pe_planeacion.ss_observaciones_servicio_social oss
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = oss.id_servicio_social
where (oss.tipo_observacion_emisor = 2 AND oss.tipo_observacion_receptor = 3 OR oss.tipo_observacion_emisor = 3 AND oss.tipo_observacion_receptor = 2) AND
(oss.receptor_observ = '13030026' AND oss.emisor_observ = 'RAJL9008175V0' OR oss.receptor_observ = 'RAJL9008175V0' AND oss.emisor_observ = '13030026') AND 
(ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6) AND ss.id_servicio_social = 2

--Listar las conversaciones con el supervisor y el jefe oficina de servicio social
select * from pe_planeacion.ss_observaciones_servicio_social oss
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = oss.id_servicio_social
where 






