---------------FUNCION FINAL PARA CARGAR LOS REPORTES BIMESTRALES Y SUS PERIODOS(2)--------------
--3 registros si es servicio social semestral
--6 registros si es servicio social anual
--1 registro si es servicio social especial (1 sola vez)
CREATE OR REPLACE FUNCTION pe_planeacion.agregarReportesBimestralesAlumno(tipo_servsocial integer , idserviciosocial integer, fec_inicio date)
RETURNS boolean AS $$
DECLARE
    /*Para saber si no hubo errores de insercion*/
    bandera_principal boolean = false;
    /*Bandera para saber cuando es el ultimo reporte bimestral a generar*/
    bandera boolean = false;
    /*Guardamos la fecha de inicio para generar la fecha final del servicio social*/
    fec_ini date;
    /*Almacena las fechas de entrega de los reportes bimestrales*/
    fec_final date;
    /*Almacenan el tipo de servicio social y el id del servicio social del alumno*/
    tiposervsocial integer;
    /*id dels servicio social*/
    ssocial integer;
    /*Almacena el numero de reportes a generar dependiendo del tipo de servicio social*/
    no_reportes integer;
    /*Variables de control que funciona como contador de los reporte a generar*/
    contador_bimestres integer = 1;
    /*secuencia*/
    id_contador integer;
    /*Fechas ultimo reporte bimestral*/
    fec_i date = fec_inicio;
BEGIN
    /*Instanciamos las variables con la fecha de inicio*/
    fec_ini := fec_inicio;
    /*tipo de servicio y llave foranea a insertar en la tabla reportes_bimestral*/
    tiposervsocial := tipo_servsocial;
    ssocial = idserviciosocial;

    /*Obtenemos el numero de reportes a generar dependiendo de su tipo de servicio social*/
    IF (tiposervsocial = 1) THEN
        no_reportes := 4;
    ELSEIF (tiposervsocial = 2) THEN
        no_reportes := 7;
    ELSEIF (tiposervsocial < 0 AND tiposervsocial > 4) THEN
        no_reportes := 0;
    END IF;

    /*Si viene una fecha de inicio y un tipo de servicio social entonces ejecuta el codigo*/
    IF no_reportes != 0 AND fec_ini IS NOT NULL THEN
        /*Ciclo que crea los registros para los reportes bimestrales del alumno dependiendo de su tipo de servicio social*/
        WHILE contador_bimestres <= no_reportes
        LOOP

        /*Guardamos la fecha despues de sumarle dos meses para le fecha fin del reporte*/
        fec_final := fec_ini + interval '+2 month';

        id_contador = (select MAX(id_reporte_bimestral) from pe_planeacion.ss_reportes_bimestral);

        /*Si entra quiere decir que no hay registros en la tabla, asignamoc 1 para que empieza ahi el registro de la llave primaria*/
        IF (id_contador IS NULL OR id_contador = 0) THEN
            id_contador = 1;
        ELSE
            id_contador = id_contador + 1;
        END IF;

        /*Si es el ultimo reporte cambia la bandera a true, indicando que es el ultimo reporte y generando las fechas de inicio y fin del reporte bimestral*/
        IF (contador_bimestres = no_reportes) THEN
            bandera = true;
            fec_final := fec_final - interval '+2 month';
            fec_final := fec_final - interval '+1 days';
            /*Insertamos el numero de registros dependiendo del tipo de servicio social semestral, anual o especial (unica vez)*/
            insert into pe_planeacion.ss_reportes_bimestral values(id_contador, ssocial, contador_bimestres, bandera, null, null, ' ', fec_i, fec_final, 0, false, null, null, null);
        ELSIF (contador_bimestres = 1) THEN
            /*Insertamos el numero de registros dependiendo del tipo de servicio social semestral, anual o especial (unica vez)*/
            insert into pe_planeacion.ss_reportes_bimestral values(id_contador, ssocial, contador_bimestres, bandera, null, null, ' ', fec_ini, fec_final, 0, true, null, null, null);
        ELSE
            /*Insertamos el numero de registros dependiendo del tipo de servicio social semestral, anual o especial (unica vez)*/
            insert into pe_planeacion.ss_reportes_bimestral values(id_contador, ssocial, contador_bimestres, bandera, null, null, ' ', fec_ini, fec_final, 0, false, null, null, null);
        End IF;

        /*Guardamos la nueva fecha despues de sumarle dos meses y aumentamos contador*/
        fec_ini := fec_final + interval '+1 days';
        contador_bimestres := contador_bimestres + 1;

        END LOOP;
        /*Si todo salio bien entonces regresara true y sabremos que se inserto correctamente los registros para los reportes bimestrales*/
        bandera_principal := true;
        /*Hacemos el commit para guardar los cambios en la BD*/

    END IF;

    return bandera_principal;
END ;
$$ LANGUAGE plpgsql;

--drop function agregarReportesBimestralesAlumno(integer, integer, date);
--select agregarReportesBimestralesAlumno(3, 6, '2018-09-1') as inserto;
-------------FUNCION FINAL PARA CARGAR LOS REPORTES BIMESTRALES Y SUS PERIODOS-------------

delete from pe_planeacion.ss_reportes_bimestral;
delete from pe_planeacion.ss_actividades_servicio_social;
Alter table pe_planeacion.ss_reportes_bimestral

--Cambios en columna calificacion_reporte_bimestral de la tabla pe_planeacion.ss_reportes_bimestral
ALTER TABLE pe_planeacion.ss_reportes_bimestral ALTER COLUMN calificacion_reporte_bimestral DROP NOT NULL;
ALTER TABLE pe_planeacion.ss_reportes_bimestral ALTER COLUMN calificacion_reporte_bimestral DROP default
--
ALTER TABLE pe_planeacion.ss_reportes_bimestral ALTER COLUMN calificacion_reporte_bimestral SET DEFAULT 0;
ALTER TABLE pe_planeacion.ss_reportes_bimestral ALTER COLUMN calificacion_reporte_bimestral SET NOT NULL