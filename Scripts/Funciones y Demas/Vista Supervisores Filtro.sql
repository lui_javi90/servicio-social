create view pe_planeacion.ss_supervisores_programas_todos AS
SELECT
pss.id_programa,
pss.id_tipo_programa,
(select rf.ssocial_actual from pe_planeacion.ss_programas p
join pe_planeacion.ss_registro_fechas_servicio_social rf
    on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
    where p.id_programa = pss.id_programa and p.id_status_programa = 1) as programa_vigente,
(case when (pss.id_tipo_programa = 1) then (select ri."rfcEmpleado" from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno ri
                                            on ri.id_programa = p.id_programa
                                            where p.id_programa = pss.id_programa and ri.superv_principal_int = true)
      when (pss.id_tipo_programa = 2) then (select re."rfcSupervisor" from pe_planeacion.ss_programas p
      join pe_planeacion.ss_responsable_programa_externo re
                                            on re.id_programa = p.id_programa
                                            where p.id_programa = pss.id_programa and re.superv_principal_ext = true)
      else '' end) as rfcsupervisor,
(case when (pss.id_tipo_programa = 1) then (select hemp."nmbEmpleado" from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno ri
                                            on ri.id_programa = p.id_programa
                                            join public."H_empleados" hemp
      on hemp."rfcEmpleado" = ri."rfcEmpleado"
                                            where p.id_programa = pss.id_programa and ri.superv_principal_int = true)
      when (pss.id_tipo_programa = 2) then (select sp.nombre_supervisor from pe_planeacion.ss_programas p
      join pe_planeacion.ss_responsable_programa_externo re
                                            on re.id_programa = p.id_programa
                                            join pe_planeacion.ss_supervisores_programas sp
      on sp."rfcSupervisor" = re."rfcSupervisor"
                                            where p.id_programa = pss.id_programa and re.superv_principal_ext = true)
      else '' end) as nombre_supervisor,
(case when (pss.id_tipo_programa = 1) then (select hemp."apellPaterno" from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno ri
                                            on ri.id_programa = p.id_programa
                                            join public."H_empleados" hemp
      on hemp."rfcEmpleado" = ri."rfcEmpleado"
                                            where p.id_programa = pss.id_programa and ri.superv_principal_int = true)
      when (pss.id_tipo_programa = 2) then (select sp.apell_paterno from pe_planeacion.ss_programas p
      join pe_planeacion.ss_responsable_programa_externo re
                                            on re.id_programa = p.id_programa
                                            join pe_planeacion.ss_supervisores_programas sp
      on sp."rfcSupervisor" = re."rfcSupervisor"
                                            where p.id_programa = pss.id_programa and re.superv_principal_ext = true)
      else '' end) as apellido_paterno,
(case when (pss.id_tipo_programa = 1) then (select hemp."apellMaterno" from pe_planeacion.ss_programas p
join pe_planeacion.ss_responsable_programa_interno ri
                                            on ri.id_programa = p.id_programa
                                            join public."H_empleados" hemp
      on hemp."rfcEmpleado" = ri."rfcEmpleado"
                                            where p.id_programa = pss.id_programa and ri.superv_principal_int = true)
      when (pss.id_tipo_programa = 2) then (select sp.apell_materno from pe_planeacion.ss_programas p
      join pe_planeacion.ss_responsable_programa_externo re
                                            on re.id_programa = p.id_programa
                                            join pe_planeacion.ss_supervisores_programas sp
      on sp."rfcSupervisor" = re."rfcSupervisor"
                                            where p.id_programa = pss.id_programa and re.superv_principal_ext = true)
      else '' end) as apellido_materno
FROM pe_planeacion.ss_programas as pss
where pss.id_status_programa = 1
order by pss.id_programa;  