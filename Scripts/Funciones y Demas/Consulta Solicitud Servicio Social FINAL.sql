--Consulta para Solicitud de Servicio Social (SUPERVISORES INTERNOS)
select 
eda."nmbAlumno" as nombre_alumno,
ss.no_ctrl,
(select "cllDomAlu" || '#' || "numDomAlu" || ' Col. ' || "colDomAlu" from public."E_datosPersonalesAlumno" where "nctrAlumno" = eda."nctrAlumno") as direccion_alumno,
(select "telDomAlu" from public."E_datosPersonalesAlumno" where "nctrAlumno" = eda."nctrAlumno")as telefono,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu") as carrera_alumno,
eda."semAlumno" as semestre,
pss.nombre_programa,
ss."empresaSupervisorJefe" as nombre_empresa,
ss.nombre_supervisor,
ss.cargo_supervisor,
pss.nombre_programa,
(select periodo_programa from pe_planeacion.ss_periodos_programas where id_periodo_programa = pss.id_periodo_programa) as periodo_programa,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
pss.actividades_especificas_realizar,
pss.id_clasificacion_area_servicio_social as tipo_programa,
spss.fecha_solicitud_programa,
spss.valida_solicitud_alumno,
spss.id_estado_solicitud_programa_alumno
from pe_planeacion.ss_solicitud_programa_servicio_social spss
join pe_planeacion.ss_servicio_social ss
on ss.id_servicio_social = spss.id_solicitud_programa
join public."E_datosAlumno" eda
on eda."nctrAlumno" = ss.no_ctrl
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
where spss.id_solicitud_programa = 3


ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN nombre_supervisor varchar(200);
