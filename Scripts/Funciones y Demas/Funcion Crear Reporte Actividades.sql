--------------CREAR FUNCION PARA INSERTAR LAS ACTIVIDADES DE LOS MESES QUE DURA EL SERVICIO SOCIAL(5)--------------
CREATE OR REPLACE FUNCTION pe_planeacion.agregarActividadesServicioSocialAlumno(tipo_servsocial integer, id_servicio_social integer, fec_inicio date)
RETURNS boolean AS $$
DECLARE
		/*Para saber si no hubo errores de insercion*/
		bandera_principal boolean = false;
		/*Almacena el tipo de servicio social*/
		tipo_servicio integer;
		/*Almacena el numero de actividades por mes de cada Servicio Social*/
		no_actividades integer;
		/*Contador de manejo de ciclos*/
		contador_actividades integer;
		/*Guarda fecha inicio*/
		fec_ini date;
		/*Guardar fechas*/
		fec_actual date;
		/*Guarda el mes para insertarlo en la tabla*/
		mes integer;
		/*Como llave primaria*/
		id_contador integer;
		/*Guarda el ID del servicio social*/
		id_servsocial integer;
BEGIN

			/*Asignamos a nuestra variable el tipo de servicio social del alumno*/
			tipo_servicio := tipo_servsocial;
			id_servsocial := id_servicio_social;
			fec_ini := fec_inicio;
			contador_actividades = 1;
			id_contador = 0;

			/*(tipo_servicio IN NOT NULL) AND id_servsocial != null AND fec_ini != null*/
			IF (tipo_servicio > 0) THEN

				/*Obtenemos el numero de reportes a generar dependiendo de su tipo de servicio social*/
				IF (tipo_servicio = 1) THEN
					no_actividades := 6;
				ELSEIF (tipo_servicio = 2) THEN
					no_actividades := 12;
				ELSEIF (tipo_servicio = 3) THEN
					no_actividades := 1;
				ELSEIF (tipo_servicio < 0 OR tipo_servicio > 4) THEN
					no_actividades := -1;
				END IF;

			/*Ciclo que crea las actividades del alumno dependiendo de su tipo de servicio social*/
            WHILE contador_actividades <= no_actividades
            LOOP

            IF (contador_actividades = 1) THEN
			    /*Extraemos solo el mes*/
                mes := extract(month from fec_ini);
			ELSE
                /*Guardamos la fecha despues de sumarle dos meses para le fecha fin del reporte*/
                fec_actual := fec_ini + interval '+1 month';
                /*Extraemos solo el mes*/
                mes := extract(month from fec_actual);
			END IF;

			id_contador = (select MAX(id_actividad_servicio_social) from pe_planeacion.ss_actividades_servicio_social);

        	/*Si entra quiere decir que no hay registros en la tabla, asignamos 1 para que empieza ahi el registro de la llave primaria*/
        	IF id_contador IS NULL OR id_contador = 0 THEN
            id_contador = 1;
        	ELSE
            id_contador = id_contador + 1;
        	END IF;

			/*Insertamos el numero de registros dependiendo del tipo de servicio social semestral, anual o especial (unica vez)*/
        	insert into pe_planeacion.ss_actividades_servicio_social values(id_contador, id_servicio_social, mes, ' ');

			/*Guardamos la nueva fecha despues de sumarle dos meses y aumentamos contador*/
			IF (contador_actividades > 1) THEN
        	   fec_ini := fec_actual;
        	END IF;

        	contador_actividades := contador_actividades + 1;

			END LOOP;

			/*Si llega hasta es que no hubo problemas*/
			bandera_principal = true;

			END IF;

			RETURN bandera_principal;

END ;
$$ LANGUAGE plpgsql;

--drop function agregarActividadesServicioSocialAlumno(integer, integer, date);
--select agregarActividadesServicioSocialAlumno(1, 6, '2018-09-1') as inserto
--------------FIN CREAR FUNCION PARA INSERTAR LAS ACTIVIDADES DE LOS MESES QUE DURA EL SERVICIO SOCIAL--------------