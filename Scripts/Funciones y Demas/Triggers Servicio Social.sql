-----------------TRIGGER CALCULA HORAS TOTALES POR DIA DEL PROGRAMA-----------------
CREATE OR REPLACE FUNCTION ss_horas_totales_dia_programa() RETURNS trigger AS
$htdp$
DECLARE

BEGIN

      IF New.id_dia_semana IS NULL THEN
          RAISE EXCEPTION 'id_dia_semana cannot be null';
      END IF;

      IF New.hora_inicio IS NULL THEN
          RAISE EXCEPTION 'hora_inicio cannot be null';
      END IF;

      IF New.hora_fin IS NULL THEN
          RAISE EXCEPTION 'hora_fin cannot be null';
      END IF;

      IF (NEW.id_dia_semana is not null AND New.hora_inicio is not null AND NEW.hora_fin IS NOT NULL) THEN
          New.horas_totales = NEW.hora_fin - NEW.hora_inicio;
          return NEW;
      END IF;

END;
$htdp$ LANGUAGE 'plpgsql';

--drop trigger horas_totales_dia_programa on horario_dias_habiles_programas;

CREATE TRIGGER ss_horas_totales_dia_programa
BEFORE INSERT ON pe_planeacion.ss_horario_dias_habiles_programas
FOR EACH ROW
EXECUTE PROCEDURE ss_horas_totales_dia_programa();
---------------FIN TRIGGER CALCULA HORAS TOTALES POR DIA DEL PROGRAMA---------------