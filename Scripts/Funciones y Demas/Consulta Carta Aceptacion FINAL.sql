--Consulta para obtener los datos de la Carta de Aceptacion de Servicio Social
select
(ss.id_servicio_social) as folio, --tome el id
(select extract(year from ss.fecha_registro)) as anio_servicio_social,
ss.fecha_registro,
(select "siglaGradoMaxEstudio" from public."H_empleados" where "rfcEmpleado" = ss."rfcDirector") as grado_max_estudios_director,
(select "nmbCompletoEmp" from public."H_empleados" where "rfcEmpleado" = ss."rfcDirector") as director,
(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1) as institucion,
(select "siglaGradoMaxEstudio" from public."H_empleados" where "rfcEmpleado" = ss."rfcJefeDeptoVinculacion") as grado_max_estudios_jefvinculacion,
(select "nmbCompletoEmp" from public."H_empleados" where "rfcEmpleado" = ss."rfcJefeDeptoVinculacion") as jefe_vinculacion,
(select "nmbAlumno" from public."E_datosAlumno" where "nctrAlumno" = ss.no_ctrl) as nombre_alumno,
(select esp."dscEspecialidad" from public."E_datosAlumno" eda
join public."E_especialidad" esp
on esp."cveEspecialidad" = eda."cveEspecialidadAlu" 
where eda."nctrAlumno" = ss.no_ctrl) as carrera_alumno,
ss.no_ctrl,
pss.nombre_programa,
ss."departamentoSupervisorJefe",
pss.horas_totales,
pss.fecha_inicio_programa,
pss.fecha_fin_programa,
(select horas_totales from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = pss.id_programa limit 1) as horas_diarias_programa,
(case 
when pss.id_periodo_programa = 1 then 'seis'
when pss.id_periodo_programa = 2 then 'doce'
when pss.id_periodo_programa = 3 then 'dos'
end) as periodo_programa,
ss.nombre_jefe_depto,
spss.valida_solicitud_supervisor_programa,
spss.id_estado_solicitud_programa_supervisor
from pe_planeacion.ss_servicio_social ss
join pe_planeacion.ss_programas pss
on pss.id_programa = ss.id_programa
join pe_planeacion.ss_solicitud_programa_servicio_social spss
on spss.id_solicitud_programa = ss.id_servicio_social
where ss.id_servicio_social = 3;

select horas_totales from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = 2 limit 1