---------------FUNCION PARA CREAR EL REGISTRO DONDE SE GUARDARA LA EVALUACION DE CADA REPORTE BIMESTRAL(3)--------------
CREATE OR REPLACE FUNCTION pe_planeacion.crearRegistroEvaluacionBimestral(id_reporte_bimestral integer)
RETURNS boolean AS $$
DECLARE
    /*Para saber si no hubo errores de insercion de los registros*/
    bandera_principal boolean = false;
    /*Guardara el valor de id_reporte_bimestral, nuestra llave foranea*/
    idreportebimestral integer;
    /*Guardara nuestros criterios a insertar*/
    id_criterio integer;
    /*Guardamos el maximo valor de a para que sea el valor default del registro, es decir, el mas alto*/
    my_valor_a integer;
    /*Almacena el tipo de criterio para la condicion*/
    tipo_criterio integer;
    /*Almacena el id_criterio de la tabla criterios_a_evaluar que es llave foranea en evaluacion_bimestral*/
    r_idcriterio RECORD;
    /*Contador secuencia*/
    id_contador integer;
BEGIN
    /*Inicializamos nuestra llave foranea*/
    idreportebimestral = id_reporte_bimestral;
    /*Inicializamos contador en cero*/
    id_contador = 0;

    IF idreportebimestral > 0 THEN
        /*Obtenemos los criterios de evaluacion del supervisor por id*/
        FOR r_idcriterio IN SELECT * FROM pe_planeacion.ss_criterios_a_evaluar
        WHERE status_criterio = true
        ORDER BY id_tipo_criterio, id_criterio asc
        LOOP

        /*Obtenemos el id para insertar correctamente*/
        id_contador = (select MAX(id_evaluacion_bimestral) from pe_planeacion.ss_evaluacion_bimestral);

        /*Si entra quiere decir que no hay registros en la tabla, asignamos 1 para que empieza ahi el registro de la llave primaria*/
        IF id_contador IS NULL OR id_contador = 0 THEN
            id_contador = 1;
        ELSE
            id_contador = id_contador + 1;
        END IF;
        /*Obtenemos el id para insertar correctamente*/

        id_criterio := r_idcriterio.id_criterio;
        my_valor_a := r_idcriterio.valor_a;
        tipo_criterio := r_idcriterio.id_tipo_criterio;

        insert into pe_planeacion.ss_evaluacion_bimestral values(id_contador, idreportebimestral, id_criterio, 0);

        END LOOP;
        /*Obtenemos los criterios de evaluacion del supervisor por id*/

        /*Si llega hasta es que no hubo error, cambia a true*/
        bandera_principal = true;

    END IF;

    RETURN bandera_principal;
END;
$$
LANGUAGE 'plpgsql';


--drop function crearRegistroEvaluacionBimestral(integer);
--select crearRegistroEvaluacionBimestral(1) as valido;
---------------FUNCION PARA CREAR EL REGISTRO DONDE SE GUARDARA LA EVALUACION DE CADA REPORTE BIMESTRAL--------------

delete from pe_planeacion.ss_evaluacion_bimestral;

--Obtener el total de b de cada Reporte Bimestral
select SUM(evaluacion_b) from pe_planeacion.ss_reportes_bimestral rb
join pe_planeacion.ss_evaluacion_bimestral eb
on eb.id_reporte_bimestral = rb.id_reporte_bimestral
where rb.id_servicio_social = 1 