SELECT * FROM public."E_datosAlumno";
SELECT * FROM public."E_datosPersonalesAlumno";

--vaciamos tabla de criterios
delete from pe_planeacion.ss_criterios_a_evaluar;

CREATE TABLE pe_planeacion.ss_status_servicio_social
(
    no_ctrl char(9) not null;
    val_serv_social boolean default false,
    Constraint pk_no_ctrl Primary key(no_ctrl)
);

Alter table pe_planeacion.ss_status_servicio_social Add Constraint fk1_no_ctrl foreign key(no_ctrl) References public."E_datosAlumno"("nctrAlumno");

--Cambios Tabla Criterios 
ALTER TABLE pe_planeacion.ss_criterios_a_evaluar ALTER COLUMN valor_a DROP NOT NULL;
ALTER TABLE pe_planeacion.ss_criterios_a_evaluar ALTER COLUMN status_criterio DROP NOT NULL;
ALTER TABLE pe_planeacion.ss_criterios_a_evaluar ALTER COLUMN status_criterio DROP DEFAULT;

--Agregar estatus a la tabla ss_clasificacion_area
Alter table pe_planeacion.ss_clasificacion_area add Column status_area boolean;

--Agregar datos a la tabla ss_status_servicio_social
Update pe_planeacion.ss_status_servicio_social set no_ctrl = , val_serv

-- 1 A?o
-- 0 Periodo
select periodoactual(1);


---Consulta para obtener los alumnos que cumplen los creditos para servicio social y ademas tiene activado o no el servicio social
select * from public."E_datosAlumno" eda
join pe_planeacion.ss_status_servicio_social as sss
on sss.no_ctrl = eda."nctrAlumno"
where eda."crdAcumulaAlu" >= 123 AND sss.val_serv_social = true;

delete from pe_planeacion.ss_status_servicio_social;

--Cambios en las tablas ss_programas y ss_supervisores_programas
ALTER TABLE pe_planeacion.ss_programas DROP CONSTRAINT fk7_rfc_supervisor;
ALTER TABLE pe_planeacion.ss_programas DROP COLUMN "rfcSupervisor";
ALTER TABLE pe_planeacion.ss_responsable_programa_externo ADD COLUMN fecha_asignacion timestamp with time zone;
ALTER TABLE pe_planeacion.ss_responsable_programa_externo ALTER COLUMN fecha_asignacion SET NOT NULL
ALTER TABLE pe_planeacion.ss_responsable_programa_interno ADD COLUMN fecha_asignacion timestamp with time zone;
ALTER TABLE pe_planeacion.ss_responsable_programa_interno ALTER COLUMN fecha_asignacion SET NOT NULL

--Cambios en los ss_supervisores_programas
ALTER TABLE pe_planeacion.ss_supervisores_programas ADD COLUMN nombre_jefe_depto varchar(200);
ALTER TABLE pe_planeacion.ss_supervisores_programas ALTER COLUMN nombre_jefe_depto SET NOT NULL
Alter table pe_planeacion.ss_supervisores_programas drop constraint fk3_cvedepartamento;
ALTER TABLE pe_planeacion.ss_supervisores_programas DROP COLUMN "cveDepartamento";
ALTER TABLE pe_planeacion.ss_supervisores_programas ADD COLUMN "dscDepartamento" varchar(250) not null;

--Agregar datos de los jefes como director, vinculacion, jefe depto de la empresa donde se hizo el proogram
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN "rfcDirector" varchar(13); --Por rfc para traernos su grado maximo de estudios (debe aparecer en la carta) y su nombre si se modifica
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN "rfcSupervisor" varchar(13); --Para traernos su nombre por su rfc si se modifica su nombre
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN cargo_supervisor varchar(150); --se guarda el cargo del supervisor en ese momento, puede que cambie de cargo y/o empresa
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN "empresaSupervisorJefe" varchar(200); --Nombre de la empresa, puede que cambie el nombre de la empresa
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN nombre_jefe_depto varchar(200); --Nombre del jefe de departamento en ese momento del programa
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN "departamentoSupervisorJefe" varchar(250); --El departamento del supervisor y el jefe en ese momento, es el mismo depto.
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN "rfcJefeOficServicioSocial" varchar(13); --El rfc de la persona que fue jefe de oficina de servicio social en ese momento
ALTER TABLE pe_planeacion.ss_servicio_social ADD COLUMN "rfcJefeDeptoVinculacion" varchar(13); --El rfc de la persona que fue jefe de Vinculacion en ese momento

--Agregar campos a la tabla pe_planeacion.ss_observaciones_servicio_social
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social ADD COLUMN fue_leida BOOLEAN DEFAULT false;
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social ADD COLUMN fecha_leida timestamp with time zone;
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social ADD COLUMN hilo_observacion integer;
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social ADD COLUMN emisor_observ varchar(13);
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social ADD COLUMN receptor_observ varchar(13);
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social ADD COLUMN tipo_observacion_receptor integer;
ALTER TABLE pe_planeacion.ss_observaciones_servicio_social RENAME COLUMN tipo_envio_observacion TO tipo_observacion_emisor;


select * from pe_planeacion.ss_observaciones_servicio_social;

--Agregar campos a la tabla pe_planeacion.ss_programas
ALTER TABLE pe_planeacion.ss_programas ADD COLUMN lugares_ocupados Numeric(2,0) not null;
ALTER TABLE pe_planeacion.ss_programas ADD COLUMN id_unidad_receptora integer;
ALTER TABLE pe_planeacion.ss_programas ADD FOREIGN KEY(id_unidad_receptora) REFERENCES pe_planeacion.ss_unidades_receptoras(id_unidad_receptora);


--Agregar campos a la tabla pe_planeacion.ss_supervisores_programas
ALTER TABLE pe_planeacion.ss_supervisores_programas ADD COLUMN eres_jefe_depto BOOLEAN not null;
ALTER TABLE pe_planeacion.ss_supervisores_programas ADD COLUMN "gradoMaxEstudios" char(5);
ALTER TABLE pe_planeacion.ss_supervisores_programas ADD COLUMN "passwSupervisor" char(32);

--Agregar campos a la tabla pe_planeacion.ss_unidades_receptoras
ALTER TABLE pe_planeacion.ss_unidades_receptoras ADD COLUMN sello_empresa varchar(18);
ALTER TABLE pe_planeacion.ss_unidades_receptoras ADD COLUMN banner_superior varchar(18);
ALTER TABLE pe_planeacion.ss_unidades_receptoras ADD COLUMN banner_inferior varchar(18);
ALTER TABLE pe_planeacion.ss_unidades_receptoras ADD COLUMN path_carpeta varchar(250);

--Agregar campo a la tabla pe_planeacion.ss_responsable_programa_interno y pe_planeacion.ss_responsable_programa_externo
ALTER TABLE pe_planeacion.ss_responsable_programa_interno ADD COLUMN superv_principal_int boolean not null;
ALTER TABLE pe_planeacion.ss_responsable_programa_externo ADD COLUMN superv_principal_ext boolean not null;

--Cambiar tama�o de los campos pe_planeacion.ss_unidades_receptoras
alter table pe_planeacion.ss_unidades_receptoras alter column logo_unidad_receptora type varchar(25);
alter table pe_planeacion.ss_unidades_receptoras alter column banner_superior type varchar(25);
alter table pe_planeacion.ss_unidades_receptoras alter column banner_inferior type varchar(25);




