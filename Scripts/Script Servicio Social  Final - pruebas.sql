------------INICIO DE SCRIPT DE SERVICIO SOCIAL------------
--28 tablas del Modulo de Servicio Social (public.ss_tipo_expediente NO SE TOMO EN CUENTA)--

-------------TABLAS SIMULACION DE PRUEBA DE SCRIPT-------------
CREATE TABLE public."E_datosAlumno"
(
	"nctrAlumno" char(9) NOT NULL,
	nombre varchar(200),
	Constraint pk_nctrAlumno Primary key("nctrAlumno")
);

CREATE TABLE public."E_periodos"
(
	"numPeriodo" char(1) NOT NULL,
	"dscPeriodo" char(16),
	Constraint pk_numPeriodo Primary key("numPeriodo")
);

CREATE TABLE public.x_estados
(
	id_estado SERIAL NOT NULL,
	nombre varchar(45),
	Constraint pk_id_estado Primary key(id_estado)
);

CREATE TABLE public.x_municipios
(
	id_municipio integer NOT NULL,
	nombre varchar(45),
	Constraint pk_id_municipio Primary key(id_municipio)
);

CREATE TABLE public."H_empleados"
(
	"rfcEmpleado" char(13) NOT NULL,
	nombre_empleado varchar(200),
	Constraint pk_rfcEmpleado Primary key("rfcEmpleado")
);
-------------TABLAS SIMULACION DE PRUEBA DE SCRIPT-------------

--(1)
CREATE TABLE public.ss_status_servicio_social
(
    no_ctrl char(9) not null,
    val_serv_social boolean,
	fecha_preregistro timestamp with time zone,
	periodo char(1),
	anio NUMERIC(4,0),
    Constraint pk_no_ctrl Primary key(no_ctrl),
	Constraint fk1_no_ctrl foreign key(no_ctrl) References public."E_datosAlumno"("nctrAlumno")
);

--(2)
CREATE TABLE public.ss_configuracion
(
	id_configuracion SERIAL NOT NULL, --Id rel registro de configuracion (1) Solo habra un registro en esta tabla
	horas_max_servicio_social NUMERIC(3,0) NOT NULL, --Define el numero de horas a cubrir para liberar el servicio social (480)
	porcentaje_creditos_req_servicio_social NUMERIC(2,0) NOT NULL, --Define el porcentaje a partir del cual se podra realizar el servicio social
    fecha_modificacion_configuracion timestamp with time zone,  --Define la fecha y la hora en que se modifico por ultima vez la tabla configuracion
	CONSTRAINT pk_id_configuracion PRIMARY KEY(id_configuracion)
);

--(3)
CREATE TABLE public.ss_codigos_calidad
(
    id_codigo_calidad SERIAL NOT NULL, --Id del codigo de calidad
    nombre_documento_digital VARCHAR(100) NOT NULL, --Nombre del Documento
    codigo_calidad VARCHAR(25) NOT NULL, --Codigo Calidad Asignado al Documento
    revision NUMERIC(2,0) NOT NULL, --No. de Revision del Documento
    CONSTRAINT pk_id_codigo_calidad PRIMARY KEY(id_codigo_calidad)
);


--(4)
CREATE TABLE public.ss_periodos_programas
(
	id_periodo_programa SERIAL NOT NULL, --Id unica del periodo del programa
    periodo_programa VARCHAR(30) NOT NULL,  --Nombre del periodo del programa (SEMESTRAl, ANUAL, UNICA VEZ)
    CONSTRAINT pk_id_periodo_programa PRIMARY KEY(id_periodo_programa)
);

--(5)
CREATE TABLE public.ss_servicios_sociales_anteriores
(
    id_servicio_social_anterior SERIAL NOT NULL,  --id del servicio social anterior al sistema
    no_ctrl VARCHAR(8) NOT NULL,  --no. de control del alumno que hizo el servicio social
    nombre_programa VARCHAR(150) NOT NULL, --nombre del programa
    id_periodo_programa integer NOT NULL, --semestral, anual o especial
    nombre_completo_supervisor VARCHAR(80) NOT NULL, --nombre completo del supervisor
    cargo_supervisor VARCHAR(100),  --El cargo del supervisor del programa tomado
    horas_liberadas NUMERIC(3,0) NOT NULL, --horas liberadas en ese servicio social
    fecha_inicio_programa date NOT NULL, --fecha de inicio del programa o aproximada
    fecha_fin_programa date NOT NULL, --fecha fin del programa o aproximada
    calificacion_servicio_social NUMERIC(3,0) NOT NULL, --Calificacion obtenida
	nombre_empresa varchar(200), --Nombre de la Empresa
    CONSTRAINT pk_id_servicio_social_anterior PRIMARY KEY(id_servicio_social_anterior),
    CONSTRAINT fk1_no_ctrl FOREIGN KEY(no_ctrl) REFERENCES public."E_datosAlumno"("nctrAlumno"),
    CONSTRAINT fk2_id_periodo_programa FOREIGN KEY(id_periodo_programa) REFERENCES public.ss_periodos_programas(id_periodo_programa)
);

--(6)
CREATE TABLE public.ss_historico_totales_alumnos
(
    no_ctrl VARCHAR(8) NOT NULL,
    horas_totales_servicio_social NUMERIC(3,0) default 0,
    fecha_modificacion_horas_totales timestamp with time zone,
    calificacion_final_servicio_social NUMERIC(3,0) DEFAULT 0 NOT NULL,
    completo_servicio_social boolean DEFAULT false NOT NULL,
    firma_digital_alumno VARCHAR(12),
	calificacion_kardex boolean,
    CONSTRAINT pk_noctrl PRIMARY KEY(no_ctrl),
    CONSTRAINT fk1_no_ctrl FOREIGN KEY(no_ctrl) REFERENCES public."E_datosAlumno"("nctrAlumno")
);

--(7)
CREATE TABLE public.ss_tipo_servicio_social
(
    id_tipo_servicio_social SERIAL NOT NULL, --Clave unica del tipo de servicio social
    tipo_servicio_social VARCHAR(25) NOT NULL,  --Nombre del tipo de servicio social
    CONSTRAINT pk_id_tipo_servicio_social PRIMARY KEY(id_tipo_servicio_social)
);

--(8)
CREATE TABLE public.ss_apoyos_economicos
(
    id_apoyo_economico_prestador_servicio_social SERIAL NOT NULL,  --Clave unica de las opciones de si hay apoyoo economico
    descripcion_apoyo_economico VARCHAR(200) NOT NULL,  --Descripcion de las opciones de si hay apoyo economico
    CONSTRAINT pk_id_apoyo_economico_prestador_servicio_social PRIMARY KEY(id_apoyo_economico_prestador_servicio_social)
);

--(9)
CREATE TABLE public.ss_tipos_apoyos_economicos
(
    id_tipo_apoyo_economico SERIAL NOT NULL,  --Clave unica del tipo de apoyo economico
    id_apoyo_economico_prestador_servicio_social integer NOT NULL, --Clave foranea Tabla "hay_apoyos_economicos_servicio_social"
    tipo_apoyo_economico VARCHAR(100) NOT NULL,  --Nombre del Tipo de apoyo economico
    CONSTRAINT pk_id_tipo_apoyo_economico PRIMARY KEY(id_tipo_apoyo_economico),
    CONSTRAINT fk1_id_apoyo_economico FOREIGN KEY(id_apoyo_economico_prestador_servicio_social) REFERENCES public.ss_apoyos_economicos(id_apoyo_economico_prestador_servicio_social)
);

--(10)
CREATE TABLE public.ss_clasificacion_area
(
    id_clasificacion_area_servicio_social SERIAL NOT NULL,  --Clave unica de la clasificacion del area de servicio social
    clasificacion_area_servicio_social VARCHAR(200) NOT NULL,  --Nombre de la clasificacion del area del servicio social
	status_area boolean, --Estatus del clasificador
    CONSTRAINT pk_id_clasificacion_area_servicio_social PRIMARY KEY(id_clasificacion_area_servicio_social)
);

--(11)
CREATE TABLE public.ss_tipo_status
(
    id_status SERIAL NOT NULL, --Id del estatus del servicio social
    descripcion_status VARCHAR(10) NOT NULL, --Descripcion del estatus de servicio social
    CONSTRAINT pk_id_status PRIMARY KEY(id_status)
);

--(12)
CREATE TABLE public.ss_registro_fechas_servicio_social
(
    id_registro_fechas_ssocial SERIAL NOT NULL,
    fecha_limite_inscripcion date,
    fecha_inicio_ssocial date NOT NULL,
    ssocial_actual boolean,
    id_periodo char(1) NOT NULL,
	anio NUMERIC(4,0) NOT NULL,
	cambiar_fech_limite_insc boolean,
    CONSTRAINT pk_id_registro_fechas_ssocial PRIMARY KEY(id_registro_fechas_ssocial),
	CONSTRAINT fk1_id_periodo FOREIGN KEY(id_periodo) REFERENCES public."E_periodos"("numPeriodo")
);

--(13)
CREATE TABLE public.ss_unidades_receptoras
(
    id_unidad_receptora SERIAL NOT NULL, --Clave unica de la unidad receptora (Empresa privada o publica, gobierno)
    nombre_unidad_receptora VARCHAR(200) NOT NULL, --Nombre de la unidad receptora
    rfc_unidad_responsable VARCHAR(13),  --RFC de la unidad responsable (Opcional)
    id_estado integer NOT NULL, --Clave del estado de la unidad receptora
    id_municipio integer NOT NULL, --Clave del municipio donde se encuentra la unidad receptora
    calle VARCHAR(40) NOT NULL, --Calle donde se encuentra ubicada la empresa
    numero VARCHAR(4) NOT NULL, --Numero de la empresa
    colonia VARCHAR(40) NOT NULL, --Colonia donde se encuentra la empresa
    telefono VARCHAR(50) NOT NULL, --Telefono de la empresa, poner la extension si tiene
    logo_unidad_receptora VARCHAR(25),  --Logo de la empresa (Opcional subirlo)
    fecha_registro_unidad_receptora timestamp with time zone,  --Fecha registro
    fecha_modificacion_unidad_receptora timestamp with time zone,  --Fecha modificacion
    id_tipo_empresa integer NOT NULL, --Id del tipo de empresa
    id_status_unidad_receptora integer NOT NULL, --Estatus de la empresa
	sello_empresa varchar(18), --Sello de la empresa
	banner_superior varchar(25), --Banner Superior para las cartas de aceptacion y finalizacion
	banner_inferior varchar(25), --Banner Inferior para las cartas de aceptacion y finalizacion
	path_carpeta varchar(250), --Path de la carpeta donde se almacenan los banners
    CONSTRAINT pk_id_unidad_receptora PRIMARY KEY(id_unidad_receptora),
	CONSTRAINT fk1_id_estado FOREIGN KEY(id_estado) REFERENCES public.x_estados(id_estado),
	CONSTRAINT fk1_id_municipio FOREIGN KEY(id_municipio) REFERENCES public.x_municipios(id_municipio),
	CONSTRAINT fk3_id_status_unidad_receptora FOREIGN KEY(id_status_unidad_receptora) REFERENCES public.ss_tipo_status(id_status)
);

--(14)
CREATE TABLE public.ss_supervisores_programas
(
    "rfcSupervisor" VARCHAR(13) NOT NULL, --Clave unica del supervisor
    nombre_supervisor VARCHAR(30) NOT NULL, --Nombre (s) del supervisor
    apell_paterno VARCHAR(20) NOT NULL, --Apellido paterno del supervisor
    apell_materno VARCHAR(20) NOT NULL, --Apellido materno del supervisor
    cargo_supervisor VARCHAR(150) NOT NULL, --cargo que ocupa el supervisor
    email_supervisor VARCHAR(50) NOT NULL, --Email del supervisor
    id_tipo_supervisor integer NOT NULL,  --Si es interno o externo
    id_status_supervisor integer NOT NULL, --Su estatus ALTA, BAJA o Finalizar(eliminado)
    id_sexo CHAR(1) NOT NULL, --Sexo del supervisor (1 Masculino, 2 Femenino)
    foto_supervisor VARCHAR(17), --Imagen del supervisor
    id_unidad_receptora integer NOT NULL, --id de la empresa en la que trabaja
    nombre_jefe_depto varchar(200) not null, --Nombre del jefe de departamento donde labora el supervisor
	"dscDepartamento" varchar(250) not null, --Nombre del departamento
	eres_jefe_depto BOOLEAN not null, --Si el supervisor tambien es jefe departamento
	"gradoMaxEstudios" char(5), --Grado maximo de estudios del jefe de departamento
	"passwSupervisor" char(32), --Contraseña del supervisor para acceder al sistema y evaluar al alumno
	"grMaxEstSup" char(5), --Grado maximo de estudios del Supervisor
    CONSTRAINT pk_rfc_supervisor PRIMARY KEY("rfcSupervisor"),
	CONSTRAINT fk1_id_status_supervisor FOREIGN KEY(id_status_supervisor) REFERENCES public.ss_tipo_status(id_status),
	CONSTRAINT fk2_id_unidad_receptora FOREIGN KEY(id_unidad_receptora) REFERENCES public.ss_unidades_receptoras(id_unidad_receptora)
);

--(15)
CREATE TABLE public.ss_programas
(
	id_programa SERIAL NOT NULL, --Clave unica del programa
    nombre_programa VARCHAR(150) NOT NULL, --Nombre del programa
	lugar_realizacion_programa VARCHAR(200) NOT NULL, --Se pone en esta tabla porque sino se tendrian varios registros de la misma empresa pero diferente lugar de realizacion del programa
	horas_totales NUMERIC(3,0) NOT NULL, --horas totales del programa (1 y 480)
	id_periodo_programa integer NOT NULL, --Clave del periodo del programa (Tabla periodos_programas)
	numero_estudiantes_solicitados numeric(2,0) NOT NULL, --Numero de estudiantes solicitados para el programa (Solo disponible para Vinculacion)
	descripcion_objetivo_programa VARCHAR(400) NOT NULL, --Describir brevemente la finalidad del programa
	id_tipo_servicio_social integer NOT NULL, --Clave del tipo de Servicio Social (Tabla tipo_servicio_social)
	impacto_social_esperado VARCHAR(300) NOT NULL, --Impacto que se espera de la realizacion del programa
	beneficiarios_programa VARCHAR(300) NOT NULL, --Que personas o areas se beneficiaran de la realizacion del programa (personas, asociaciones, etc.,)
	actividades_especificas_realizar VARCHAR(500) NOT NULL, --Definir las actividades que se llevaran a cabo en el programa
	mecanismos_supervision VARCHAR(200) NOT NULL, --Evidencias con las que se supervisara al estudiante durante la realizacion del programa
	perfil_estudiante_requerido VARCHAR(500) NOT NULL, --Definir caracteristicas que debe cumplir el alumno para ser seleccionado en el programa
	id_apoyo_economico_prestador integer NOT NULL, --Clave foranea para definir si hay apoyo ecomico (Tabla apoyos_economicos_servicio_social) (Si - No - Otra)
	id_tipo_apoyo_economico integer NOT NULL, --Clave del tipo de apoyo economico a ofrecer
    apoyo_economico VARCHAR(100) NOT NULL, --Apoyo economico a ofrecer en el programa (NO CONFUNDIR con tipo de apoyo economico a ofrecer) (Si SE ELIGE NO se pondra "NINGUNO")
	fecha_registro_programa timestamp with time zone, --Fecha y hora en que registra la empresa
    fecha_modificacion_programa timestamp with time zone, --Fecha y hora en que se modifican los datos de la empresa
	id_recibe_capacitacion integer NOT NULL,  --Id Especificar si recibira capacitacion el alumno para llevar a cabo sus actividades en el programa de servicio social
	id_clasificacion_area_servicio_social integer NOT NULL, --Id Clasificacion del Area de realizacion del Programa
	lugares_disponibles NUMERIC(2,0) NOT NULL, --Lugares disponibles
	id_tipo_programa integer NOT NULL, --Id del tipo del programa
	id_status_programa integer NOT NULL, --Id del estatus del programa
	fecha_inicio_programa  date NOT NULL, --Fecha inicio del programa
	fecha_fin_programa date NOT NULL, --Fecha Fin del programa
	id_registro_fechas_ssocial integer NOT NULL, --Id del resgistro del Programa (Periodo y Año)
	lugares_ocupados NUMERIC(2,0) NOT NULL, --Lugares que ya fueron ocupados en el programa
	id_unidad_receptora integer NOT NULL, --Empresa a la que pertenece el programa
	CONSTRAINT pk_id_programa PRIMARY KEY(id_programa),
	CONSTRAINT fk1_id_periodo_programa FOREIGN KEY(id_periodo_programa) REFERENCES public.ss_periodos_programas(id_periodo_programa),
	CONSTRAINT fk2_id_tipo FOREIGN KEY(id_tipo_servicio_social) REFERENCES public.ss_tipo_servicio_social(id_tipo_servicio_social),
	CONSTRAINT fk3_id_apoyo_economico_prestador FOREIGN KEY(id_apoyo_economico_prestador) REFERENCES public.ss_apoyos_economicos(id_apoyo_economico_prestador_servicio_social),
	CONSTRAINT fk4_id_tipo_apoyo_economico FOREIGN KEY(id_tipo_apoyo_economico) REFERENCES public.ss_tipos_apoyos_economicos(id_tipo_apoyo_economico),
	CONSTRAINT fk5_id_clasificacion_area FOREIGN KEY(id_clasificacion_area_servicio_social) REFERENCES public.ss_clasificacion_area(id_clasificacion_area_servicio_social),
	CONSTRAINT fk6_id_status_programa FOREIGN KEY(id_status_programa) REFERENCES public.ss_tipo_status(id_status),
	CONSTRAINT fk7_id_registro_fechas_ssocial FOREIGN KEY(id_registro_fechas_ssocial) REFERENCES public.ss_registro_fechas_servicio_social(id_registro_fechas_ssocial),
	CONSTRAINT fk8_id_unidad_receptora FOREIGN KEY(id_unidad_receptora) REFERENCES public.ss_unidades_receptoras(id_unidad_receptora)
	
);

--(16)
CREATE TABLE public.ss_responsable_programa_externo
(
	id_programa integer,
	"rfcSupervisor" varchar(13),
	fecha_asignacion timestamp with time zone NOT NULL,
	superv_principal_ext boolean not null,
	CONSTRAINT fk1_id_programa FOREIGN KEY(id_programa) REFERENCES public.ss_programas(id_programa),
	CONSTRAINT fk2_rfcSupervisor FOREIGN KEY("rfcSupervisor") REFERENCES public.ss_supervisores_programas("rfcSupervisor")
);

--(17)
CREATE TABLE public.ss_responsable_programa_interno
(
	id_programa integer,
	"rfcEmpleado" varchar(13),
	fecha_asignacion timestamp with time zone NOT NULL,
	superv_principal_int boolean not null,
	CONSTRAINT fk1_id_programa FOREIGN KEY(id_programa) REFERENCES public.ss_programas(id_programa),
	CONSTRAINT fk2_rfcEmpleado FOREIGN KEY("rfcEmpleado") REFERENCES public."H_empleados"("rfcEmpleado")
);

--(18)
CREATE TABLE public.ss_estados_solicitud_programa
(
	id_estado_solicitud_programa SERIAL NOT NULL, --Id del estado de la solicitud de servicio social
    estado_solicitud_programa VARCHAR(20) NOT NULL, --Nombre del estado de Solicitud de Servicio Social
    CONSTRAINT pk_id_estado_solicitud_programa PRIMARY KEY(id_estado_solicitud_programa)
);

--(19)
CREATE TABLE public.ss_solicitud_programa_servicio_social
(
    id_solicitud_programa SERIAL NOT NULL, --Id de la Solicitud del Programa
    no_ctrl VARCHAR(8) NOT NULL, --Llave foranea No. de Control del Alumno
    id_programa integer NOT NULL, --Id del Programa al que realiza la solicitud
	fecha_solicitud_programa timestamp with time zone, --Fecha en que se hace la solicitud al Programa
	valida_solicitud_alumno timestamp with time zone, --Fecha en que valida la solicitud el Alumno
	valida_solicitud_supervisor_programa timestamp with time zone, --Fecha en que valida la solicitud el Supervisor del Programa
    id_estado_solicitud_programa_supervisor integer NOT NULL, --Id del Estado de la Solicitud al Programa por parte del Supervisor
	id_estado_solicitud_programa_alumno integer NOT NULL, --Id del Estado de la Solicitud al Programa por parte del Alumno
    CONSTRAINT pk_id_solicitud_programa PRIMARY KEY(id_solicitud_programa),
    CONSTRAINT fk1_no_ctrl FOREIGN KEY(no_ctrl) REFERENCES public."E_datosAlumno"("nctrAlumno"),
    CONSTRAINT fk2_id_programa FOREIGN KEY(id_programa) REFERENCES public.ss_programas(id_programa),
    CONSTRAINT fk3_id_estado_solicitud_programa_supervisor FOREIGN KEY(id_estado_solicitud_programa_supervisor) REFERENCES public.ss_estados_solicitud_programa(id_estado_solicitud_programa),
	CONSTRAINT fk4_id_estado_solicitud_programa_alumno FOREIGN KEY(id_estado_solicitud_programa_alumno) REFERENCES public.ss_estados_solicitud_programa(id_estado_solicitud_programa)
);

--(20)
CREATE TABLE public.ss_estado_servicio_social
(
    id_estado_servicio_social SERIAL NOT NULL,
    estado VARCHAR(20) NOT NULL,
    descripcion_estado VARCHAR(250) NOT NULL,
    CONSTRAINT pk_id_estado_servicio_social PRIMARY KEY(id_estado_servicio_social)
);

--(21)
CREATE TABLE public.ss_servicio_social
(
    id_servicio_social SERIAL NOT NULL, --LLave primaria del servicio sociaL
    no_ctrl VARCHAR(8) NOT NULL, --No. de control del alumno
    id_estado_servicio_social integer NOT NULL, --Estado del Servicio Social
	id_programa integer NOT NULL, --Programa que realiza el alumno
    fecha_registro timestamp with time zone, --Fecha de registro de Servicio Social
    fecha_modificacion timestamp with time zone, --Fecha de modificacion de informacion de la tabla de ss_servicio_social
    calificacion_servicio_social NUMERIC(4,0) DEFAULT 0 NOT NULL, --Calificacion del servicio social actual
	"rfcDirector" varchar(13), --Por rfc para traernos su grado maximo de estudios (debe aparecer en la carta) y su nombre si se modifica
	"rfcSupervisor" varchar(13), --Para traernos su nombre por su rfc si se modifica su nombre
	cargo_supervisor varchar(150), --se guarda el cargo del supervisor en ese momento, puede que cambie de cargo y/o empresa
	"empresaSupervisorJefe" varchar(200), --Nombre de la empresa, puede que cambie el nombre de la empresa
	nombre_jefe_depto varchar(200), --Nombre del jefe de departamento en ese momento del programa
	"departamentoSupervisorJefe" varchar(250), --El departamento del supervisor y el jefe en ese momento, es el mismo depto.
	"rfcJefeOficServicioSocial" varchar(13), --El rfc de la persona que fue jefe de oficina de servicio social en ese momento
	"rfcJefeDeptoVinculacion" varchar(13), --El rfc de la persona que fue jefe de Vinculacion en ese momento
	nombre_supervisor varchar(200), --Nombre del Supervisor
    CONSTRAINT pk_id_servicio_social PRIMARY KEY(id_servicio_social),
	CONSTRAINT fk1_no_ctrl foreign key(no_ctrl) References public."E_datosAlumno"("nctrAlumno"),
	CONSTRAINT fk2_id_estado_servicio_social FOREIGN KEY(id_estado_servicio_social) REFERENCES public.ss_estado_servicio_social(id_estado_servicio_social),
	CONSTRAINT fk3_id_programa FOREIGN KEY(id_programa) REFERENCES public.ss_programas(id_programa),
	CONSTRAINT fk4_id_solicitud_servicio_social FOREIGN KEY(id_servicio_social) REFERENCES public.ss_solicitud_programa_servicio_social(id_solicitud_programa)
	
);

--(22)
CREATE TABLE public.ss_observaciones_servicio_social
(
    id_observacion SERIAL NOT NULL,
    id_servicio_social integer NOT NULL,
    observacion VARCHAR(500) NOT NULL,
    fecha_registro timestamp with time zone,
	fue_leida BOOLEAN DEFAULT false,
	fecha_leida timestamp with time zone,
	hilo_observacion integer,
	emisor_observ varchar(13),
	receptor_observ varchar(13),
	tipo_observacion_receptor integer NOT NULL, --si el jefe oficina servicio social, supervisor o alumno
    tipo_observacion_emisor integer NOT NULL, --si el jefe oficina servicio social, supervisor o alumno
    CONSTRAINT pk_id_observacion PRIMARY KEY(id_observacion),
	CONSTRAINT fk1_id_servicio_social FOREIGN KEY(id_servicio_social) REFERENCES public.ss_servicio_social(id_servicio_social)
);

--(23)
CREATE TABLE public.ss_actividades_servicio_social
(
    id_actividad_servicio_social SERIAL NOT NULL,
    id_servicio_social integer NOT NULL,
    id_mes integer NOT NULL,
    actividad_mensual VARCHAR(200),
    CONSTRAINT pk_id_actividad_servicio_social PRIMARY KEY(id_actividad_servicio_social),
    CONSTRAINT fk1_id_servicio_social FOREIGN KEY(id_servicio_social) REFERENCES public.ss_servicio_social(id_servicio_social)
);

--(24)
CREATE TABLE public.ss_reportes_bimestral
(
    id_reporte_bimestral integer NOT NULL, --Id del reporte bimestral
    id_servicio_social integer NOT NULL, --Id del servicio social
    bimestres_correspondiente NUMERIC(1,0) NOT NULL, --Bimestre correspondiente del reporte bimestral
    bimestre_final boolean DEFAULT false NOT NULL, --Si es bimestre final
    valida_responsable timestamp with time zone, --Fecha de Validacion del reporte bimestral por el supervisor del programa
    valida_oficina_servicio_social timestamp with time zone, --Fecha de Validacion del reporte bimestral por la oficina de servicio social
    observaciones_reporte_bimestral VARCHAR(200), --Observaciones del reporte bimestral
    fecha_inicio_rep_bim date not null, --Fecha de Inicio bimestral reporte 
    fecha_fin_rep_bim date check(fecha_fin_rep_bim > fecha_inicio_rep_bim) not null, --Fecha de Fin bimestral reporte
	calificacion_reporte_bimestral NUMERIC(3,0) DEFAULT 0 NOT NULL, --calificacion del reporte bimestral
	reporte_bimestral_actual boolean not null, --Para saber cual es el reporte bimestral que esta siendo evaludado
    envio_alum_evaluacion timestamp with time zone, --Fecha de envio del reporte bimestral a evaluacion y validacion por el alumno
	"rfcResponsable" varchar(13), --RFC del supervisor que valido el reporte bimestral
	"rfcJefeOfiServicioSocial" varchar(13), --RFC del jefe de oficina de servicio social
    CONSTRAINT pk_id_reporte_bimestral PRIMARY KEY(id_reporte_bimestral),
	CONSTRAINT fk1_id_servicio_social FOREIGN KEY(id_servicio_social) REFERENCES public.ss_servicio_social(id_servicio_social)
);

--(25)
CREATE TABLE public.ss_criterios_a_evaluar
(
    id_criterio SERIAL NOT NULL, --Ld del criterio
    descripcion_criterio VARCHAR(500) NOT NULL, --Descripcion del criterio
    valor_a NUMERIC(2,0), --Valor que tendra el criterio
    id_tipo_criterio integer NOT NULL, --1.Evaluacion_Supervisor, 2.Evaluacion_vinculacion
    status_criterio boolean, --Si esta activado el criterio o no
	posicion_criterio integer, --Posicion que tendra el criterio
    CONSTRAINT pk_id_criterio PRIMARY KEY(id_criterio)
);

--(26)
CREATE TABLE public.ss_evaluacion_bimestral
(
    id_evaluacion_bimestral SERIAL NOT NULL, --Id de la evaluacion bimestral
    id_reporte_bimestral integer NOT NULL, --Id del reporte bimestral
    id_criterio integer NOT NULL, --Id del criterio de evaluacion
    evaluacion_B NUMERIC(2,0) NOT NULL, --Calificacion del criterio
    CONSTRAINT pk1_id_evaluacion_bimestral PRIMARY KEY(id_evaluacion_bimestral),
	CONSTRAINT fk1_id_reporte_bimestral FOREIGN KEY(id_reporte_bimestral) REFERENCES public.ss_reportes_bimestral(id_reporte_bimestral),
	CONSTRAINT fk2_id_criterio FOREIGN KEY(id_criterio) REFERENCES public.ss_criterios_a_evaluar(id_criterio)
);

--(27)
CREATE TABLE public.ss_dias_semana
(   id_dia_semana SERIAL NOT NULL, --Id del dia de la semana
    dia_semana VARCHAR(9) NOT NULL, --Lunes, martes, miercoles, jueves, viernes, sabado
    CONSTRAINT pk_id_dia_semana PRIMARY KEY(id_dia_semana)
);

--(28)
CREATE TABLE public.ss_horario_dias_habiles_programas
(
    id_horario SERIAL NOT NULL,
    id_programa integer NOT NULL,
    id_dia_semana integer NOT NULL,
    hora_inicio time NOT NULL,
    hora_fin time NOT NULL,
    horas_totales time,
    CONSTRAINT pk_id_horario PRIMARY KEY(id_horario),
	CONSTRAINT fk1_id_programa FOREIGN KEY(id_programa) REFERENCES public.ss_programas(id_programa),
	CONSTRAINT fk2_id_dia_semana FOREIGN KEY(id_dia_semana) REFERENCES public.ss_dias_semana(id_dia_semana)
);

---------INSERTS---------
INSERT INTO public.ss_configuracion("id_configuracion", "horas_max_servicio_social", "porcentaje_creditos_req_servicio_social", "fecha_modificacion_configuracion")
VALUES
    (1, 480, 56, null);
	
INSERT INTO public.ss_estados_solicitud_programa(id_estado_solicitud_programa, estado_solicitud_programa)
VALUES
	(1, 'PENDIENTE'),
    (2, 'ACEPTADO'),
    (3, 'CANCELADO');
	
INSERT INTO public.ss_periodos_programas("id_periodo_programa", "periodo_programa")
VALUES
    (1, 'SEMESTRAL'),
    (2, 'ANUAL'),
    (3, 'UNICA VEZ');

INSERT INTO public.ss_tipo_servicio_social("id_tipo_servicio_social", "tipo_servicio_social")
VALUES
    (1, 'COMUNITARIO'),
    (2, 'PROFESIONALIZANTE');

INSERT INTO public.ss_apoyos_economicos("id_apoyo_economico_prestador_servicio_social", "descripcion_apoyo_economico")
VALUES
    (1, 'SI'),
    (2, 'NO'),
    (3, 'OTRO');

INSERT INTO public.ss_tipos_apoyos_economicos("id_tipo_apoyo_economico", "id_apoyo_economico_prestador_servicio_social", "tipo_apoyo_economico")
VALUES
    (1, 1, 'ESTÍMULO ECONÓMICO-MONTO'),
    (2, 1, 'ESTÍMULO ECONÓMICO-PERIORICIDAD'),
    (3, 3, 'OTROS APOYOS'),
    (4, 2, 'NINGUNO');
	
INSERT INTO public.ss_clasificacion_area("id_clasificacion_area_servicio_social", "clasificacion_area_servicio_social", "status_area")
VALUES
    (1, 'DESARROLLO INTEGRAL DE LA COMUNIDAD', true),
    (2, 'DESARROLLO EN ACTIVIDADES ACADÉMICAS', true),
    (3, 'CONVIVENCIA SOCIAL Y SEGURIDAD', true),
    (4, 'DESARROLLO SOCIOECONÓMICO O DESARROLLO SUSTENTABLE', true),
    (5, 'ACTIVIDADES CULTURALES', true),
    (6, 'ACTIVIDADES DEPORTIVAS', true);

INSERT INTO public.ss_tipo_status(id_status, descripcion_status) 
VALUES 
    (1, 'ALTA'),
    (2, 'BAJA'),
    (3, 'FINALIZAR'),
	(4, 'CANCELADO'),
    (5, 'OCUPADO'),
    (6, 'PENDIENTE');


INSERT INTO public.ss_dias_semana("id_dia_semana", "dia_semana")
VALUES
    (1, 'LUNES'),
    (2, 'MARTES'),
    (3, 'MIERCOLES'),
    (4, 'JUEVES'),
    (5, 'VIERNES'),
    (6, 'SABADO'),
    (7, 'DOMINGO');
---------FIN INSERTS---------

------------INICIO DE SCRIPT DE SERVICIO SOCIAL------------

