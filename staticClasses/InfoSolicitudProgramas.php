<?php
class InfoSolicitudProgramas
{
    public static function fechaSolicitudPrograma($id_solicitud_programa)
    {
        $query =
            "SELECT (case when fecha_solicitud_programa is null then 'No disponible' ELSE(
            extract('day' from fecha_solicitud_programa) ||' de '|| ( case
            when extract('month' from fecha_solicitud_programa)=1 then 'Enero'
            when extract('month' from fecha_solicitud_programa)=2 then 'Febrero'
            when extract('month' from fecha_solicitud_programa)=3 then 'Marzo'
            when extract('month' from fecha_solicitud_programa)=4 then 'Abril'
            when extract('month' from fecha_solicitud_programa)=5 then 'Mayo'
            when extract('month' from fecha_solicitud_programa)=6 then 'Junio'
            when extract('month' from fecha_solicitud_programa)=7 then 'Julio'
            when extract('month' from fecha_solicitud_programa)=8 then 'Agosto'
            when extract('month' from fecha_solicitud_programa)=9 then 'Septiembre'
            when extract('month' from fecha_solicitud_programa)=10 then 'Octubre'
            when extract('month' from fecha_solicitud_programa)=11 then 'Noviembre'
            when extract('month' from fecha_solicitud_programa)=12 then 'Noviembre'
            END)||' de '||extract('year' from fecha_solicitud_programa))end)
            as fecha_act,
            ((case when extract('hour' from fecha_solicitud_programa) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_solicitud_programa)) || ':' ||
            (case when extract('minute' from fecha_solicitud_programa) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_solicitud_programa)) || ' ' ||
            (case when extract('hour' from fecha_solicitud_programa) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_solicitud_programa_servicio_social WHERE id_solicitud_programa = '$id_solicitud_programa'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    /*Damos formato a la fecha de validacion de la Solicitud por parte del Alumno*/
    public static function fechaValidaSolicitudAlumno($id_solicitud_programa)
    {
      $query =
          "SELECT (case when valida_solicitud_alumno is null then 'No disponible' ELSE(
          extract('day' from valida_solicitud_alumno) ||' de '|| ( case
          when extract('month' from valida_solicitud_alumno)=1 then 'Enero'
          when extract('month' from valida_solicitud_alumno)=2 then 'Febrero'
          when extract('month' from valida_solicitud_alumno)=3 then 'Marzo'
          when extract('month' from valida_solicitud_alumno)=4 then 'Abril'
          when extract('month' from valida_solicitud_alumno)=5 then 'Mayo'
          when extract('month' from valida_solicitud_alumno)=6 then 'Junio'
          when extract('month' from valida_solicitud_alumno)=7 then 'Julio'
          when extract('month' from valida_solicitud_alumno)=8 then 'Agosto'
          when extract('month' from valida_solicitud_alumno)=9 then 'Septiembre'
          when extract('month' from valida_solicitud_alumno)=10 then 'Octubre'
          when extract('month' from valida_solicitud_alumno)=11 then 'Noviembre'
          when extract('month' from valida_solicitud_alumno)=12 then 'Noviembre'
          END)||' de '||extract('year' from valida_solicitud_alumno))end)
          as fecha_act,
          ((case when extract('hour' from valida_solicitud_alumno) < 10 then '0' else '' end) ||
          (extract('hour' from valida_solicitud_alumno)) || ':' ||
          (case when extract('minute' from valida_solicitud_alumno) < 10 then '0' else '' end) ||
          (extract('minute' from valida_solicitud_alumno)) || ' ' ||
          (case when extract('hour' from valida_solicitud_alumno) >= 12 then 'PM' else 'AM' end)) as hora
          FROM pe_planeacion.ss_solicitud_programa_servicio_social WHERE id_solicitud_programa = '$id_solicitud_programa'
      ";

      $fec_act = Yii::app()->db->createCommand($query)->queryAll();

      return $fec_act;
    }
    /*Damos formato a la fecha de validacion de la Solicitud por parte del Alumno*/

     /*Damos formato a la fecha de validacion de la Solicitud por parte del Supervisor*/
     public static function fechaValidaSolicitudSupervisor($id_solicitud_programa)
     {
       $query =
           "SELECT (case when valida_solicitud_alumno is null then 'No disponible' ELSE(
           extract('day' from valida_solicitud_alumno) ||' de '|| ( case
           when extract('month' from valida_solicitud_alumno)=1 then 'Enero'
           when extract('month' from valida_solicitud_alumno)=2 then 'Febrero'
           when extract('month' from valida_solicitud_alumno)=3 then 'Marzo'
           when extract('month' from valida_solicitud_alumno)=4 then 'Abril'
           when extract('month' from valida_solicitud_alumno)=5 then 'Mayo'
           when extract('month' from valida_solicitud_alumno)=6 then 'Junio'
           when extract('month' from valida_solicitud_alumno)=7 then 'Julio'
           when extract('month' from valida_solicitud_alumno)=8 then 'Agosto'
           when extract('month' from valida_solicitud_alumno)=9 then 'Septiembre'
           when extract('month' from valida_solicitud_alumno)=10 then 'Octubre'
           when extract('month' from valida_solicitud_alumno)=11 then 'Noviembre'
           when extract('month' from valida_solicitud_alumno)=12 then 'Noviembre'
           END)||' de '||extract('year' from valida_solicitud_alumno))end)
           as fecha_act,
           ((case when extract('hour' from valida_solicitud_alumno) < 10 then '0' else '' end) ||
           (extract('hour' from valida_solicitud_alumno)) || ':' ||
           (case when extract('minute' from valida_solicitud_alumno) < 10 then '0' else '' end) ||
           (extract('minute' from valida_solicitud_alumno)) || ' ' ||
           (case when extract('hour' from valida_solicitud_alumno) >= 12 then 'PM' else 'AM' end)) as hora
           FROM pe_planeacion.ss_solicitud_programa_servicio_social WHERE id_solicitud_programa = '$id_solicitud_programa'
       ";
 
       $fec_act = Yii::app()->db->createCommand($query)->queryAll();
 
       return $fec_act;
     }
     /*Damos formato a la fecha de validacion de la Solicitud por parte del Supervisor*/

}

?>
