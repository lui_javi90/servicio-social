<?php 
/*DATOS DEL FORMULARIO DE PROGRAMA EXTRATEMPORAL PARA FECHA FIN DEL PROGRAMA EXTRATEMPORAL */
?>
<?php if($modelSSProgramas->isNewRecord){ ?>
    <div class="form-group">
        <p><?php echo "<b>Fecha Fin </b>"."<span class=\"required\">*</span>"; ?></p>
		<?php //echo $form->textField($model,'fecha_inicio_ssocial');
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $modelSSProgramas,
			'attribute' => 'fecha_fin_programa',
			'language' => 'es',
			'theme' => 'softark',
			'options' => array(
				//'showOn' => 'both',				// also opens with a button
				'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
				'showOtherMonths' => true,		// show dates in other months
				'selectOtherMonths' => true,	// can seelect dates in other months
				//'changeYear' => true,			// can change year
				'changeMonth' => true,			// can change month
				'yearRange' => '2018:2025',		// range of year
				//'showButtonPanel' => true,		// show button panel
			),
			'htmlOptions' => array(
				'size' => '10',			// textField size
				'maxlength' => '10',	// textField maxlength
				'class' => 'form-control',
                'required' => 'required',
                'readOnly' => true
				//'disabled' => 'disabled'
			),
		));  ?>
        <?php echo $form->error($modelSSProgramas,'fecha_fin_programa'); ?>
	</div>
	<?php }else{ ?>
		<div class="form-group">
        <p><?php echo "<b>Fecha Fin </b>"."<span class=\"required\">*</span>"; ?></p>
		<?php //echo $form->textField($model,'fecha_inicio_ssocial');
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $modelSSProgramas,
			'attribute' => 'fecha_fin_programa',
			'language' => 'es',
			'theme' => 'softark',
			'options' => array(
				//'showOn' => 'both',			// also opens with a button
				'dateFormat' => 'yy-mm-dd',		// format of "2012-12-25"
				'showOtherMonths' => true,		// show dates in other months
				'selectOtherMonths' => true,	// can seelect dates in other months
				//'changeYear' => true,			// can change year
				'changeMonth' => true,			// can change month
				'yearRange' => '2018:2025',		// range of year
				//'showButtonPanel' => true,		// show button panel
			),
			'htmlOptions' => array(
				'size' => '10',			// textField size
				'maxlength' => '10',	// textField maxlength
				'class' => 'form-control',
                'required' => 'required',
                'readOnly' => true,
				'disabled' => 'disabled'
			),
		));  ?>
        <?php echo $form->error($modelSSProgramas,'fecha_fin_programa'); ?>
	</div>
	<?php } ?>

<?php 
/*DATOS DEL FORMULARIO DE PROGRAMA EXTRATEMPORAL PARA FECHA FIN DEL PROGRAMA EXTRATEMPORAL */
?>

<!--DATOS DEL ESTATUS DEL PROGRAMA SE USARA EN HISTORICO-->
//1, 3, 4 y 6
<?php
				switch($data->id_status_programa)
				{
					case 1:
						return '<span style="size:16px" class="label label-success">VIGENTE</span>';
					break;
					case 3:
						return '<span style="size:16px" class="label label-info">FINALIZADO</span>';
					break;
					case 4:
						return '<span style="size:16px" class="label label-danger">CANCELADO</span>';
					break;
					case 6:
						return '<span style="size:16px" class="label label-warning">PENDIENTE</span>';
					break;
					default: 
						return '<span style="size:16px" class="label label-danger">SIN ESPECIFICAR</span>';
					break;
				}
?>
<!--DATOS DEL ESTATUS DEL PROGRAMA SE USARA EN HISTORICO-->