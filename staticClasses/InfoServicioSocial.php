<?php 
class InfoServicioSocial
{
    /*Fecha registro del servicio social */
    public static function getFechaRegistroServicioSocial($id_servicio_social)
    {
        $query =
            "SELECT (case when fecha_registro is null then 'ND' ELSE(
            extract('day' from fecha_registro) ||' de '|| ( case 
            when extract('month' from fecha_registro)=1 then 'Enero'
            when extract('month' from fecha_registro)=2 then 'Febrero'
            when extract('month' from fecha_registro)=3 then 'Marzo'
            when extract('month' from fecha_registro)=4 then 'Abril'
            when extract('month' from fecha_registro)=5 then 'Mayo'
            when extract('month' from fecha_registro)=6 then 'Junio'
            when extract('month' from fecha_registro)=7 then 'Julio'
            when extract('month' from fecha_registro)=8 then 'Agosto'
            when extract('month' from fecha_registro)=9 then 'Septiembre'
            when extract('month' from fecha_registro)=10 then 'Octubre'
            when extract('month' from fecha_registro)=11 then 'Noviembre'
            when extract('month' from fecha_registro)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_registro))end) 
            as fecha_act,
            ((case when extract('hour' from fecha_registro) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_registro)) || ':' || 
            (case when extract('minute' from fecha_registro) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_registro)) || ' ' || 
            (case when extract('hour' from fecha_registro) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_servicio_social WHERE id_servicio_social = '$id_servicio_social'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    /*Fecha modificacion del servicio social */
    public static function getFechaModificacionServicioSocial($id_servicio_social)
    {
        $query =
            "SELECT (case when fecha_modificacion is null then 'No Disponible' ELSE(
            extract('day' from fecha_modificacion) ||' de '|| ( case 
            when extract('month' from fecha_modificacion)=1 then 'Enero'
            when extract('month' from fecha_modificacion)=2 then 'Febrero'
            when extract('month' from fecha_modificacion)=3 then 'Marzo'
            when extract('month' from fecha_modificacion)=4 then 'Abril'
            when extract('month' from fecha_modificacion)=5 then 'Mayo'
            when extract('month' from fecha_modificacion)=6 then 'Junio'
            when extract('month' from fecha_modificacion)=7 then 'Julio'
            when extract('month' from fecha_modificacion)=8 then 'Agosto'
            when extract('month' from fecha_modificacion)=9 then 'Septiembre'
            when extract('month' from fecha_modificacion)=10 then 'Octubre'
            when extract('month' from fecha_modificacion)=11 then 'Noviembre'
            when extract('month' from fecha_modificacion)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_modificacion))end) 
            as fecha_act,
            ((case when extract('hour' from fecha_modificacion) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_modificacion)) || ':' || 
            (case when extract('minute' from fecha_modificacion) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_modificacion)) || ' ' || 
            (case when extract('hour' from fecha_modificacion) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_servicio_social WHERE id_servicio_social = '$id_servicio_social'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    /*Fecha de inicio de Servicio Social */
    public static function getFechaInicioServicioSocial($id_servicio_social)
    {
        $query =
            "SELECT (case when fecha_inicio is null then 'No Disponible' ELSE(
            extract('day' from fecha_inicio) ||' de '|| ( case 
            when extract('month' from fecha_inicio)=1 then 'Enero'
            when extract('month' from fecha_inicio)=2 then 'Febrero'
            when extract('month' from fecha_inicio)=3 then 'Marzo'
            when extract('month' from fecha_inicio)=4 then 'Abril'
            when extract('month' from fecha_inicio)=5 then 'Mayo'
            when extract('month' from fecha_inicio)=6 then 'Junio'
            when extract('month' from fecha_inicio)=7 then 'Julio'
            when extract('month' from fecha_inicio)=8 then 'Agosto'
            when extract('month' from fecha_inicio)=9 then 'Septiembre'
            when extract('month' from fecha_inicio)=10 then 'Octubre'
            when extract('month' from fecha_inicio)=11 then 'Noviembre'
            when extract('month' from fecha_inicio)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_inicio))end) 
            as fecha_act
            FROM pe_planeacion.ss_servicio_social WHERE id_servicio_social = '$id_servicio_social'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
    /*Fecha de inicio de Servicio Social */

    /*Fecha fin de Servicio Social */
    public static function getFechaFinServicioSocial($id_servicio_social)
    {
        $query =
            "SELECT (case when fecha_fin is null then 'No Disponible' ELSE(
            extract('day' from fecha_fin) ||' de '|| ( case 
            when extract('month' from fecha_fin)=1 then 'Enero'
            when extract('month' from fecha_fin)=2 then 'Febrero'
            when extract('month' from fecha_fin)=3 then 'Marzo'
            when extract('month' from fecha_fin)=4 then 'Abril'
            when extract('month' from fecha_fin)=5 then 'Mayo'
            when extract('month' from fecha_fin)=6 then 'Junio'
            when extract('month' from fecha_fin)=7 then 'Julio'
            when extract('month' from fecha_fin)=8 then 'Agosto'
            when extract('month' from fecha_fin)=9 then 'Septiembre'
            when extract('month' from fecha_fin)=10 then 'Octubre'
            when extract('month' from fecha_fin)=11 then 'Noviembre'
            when extract('month' from fecha_fin)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_fin))end) 
            as fecha_act
            FROM pe_planeacion.ss_servicio_social WHERE id_servicio_social = '$id_servicio_social'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
    /*Fecha fin de Servicio Social */
}
?>