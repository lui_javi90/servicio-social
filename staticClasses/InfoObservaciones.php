<?php
class InfoObservaciones{

    /*Como no se puede llevar mas un Servicio Sociales a la vez solo se toman las observaciones del Servicio Social
    actual del Alumno con el no. de control recibido como parametro*/
    public static function getObservacionesSinLeerAlumno($no_ctrl)
    {
        //(3)-Son las que envia el alumno a su Supervisor y/o Jefe Oficina de Servicio Social
        $query = 
        "select COUNT(oss.id_observacion) as \"totalObsAlumno\" from pe_planeacion.ss_servicio_social ss 
        join pe_planeacion.ss_observaciones_servicio_social oss
        on oss.id_servicio_social = ss.id_servicio_social
        where oss.receptor_observ = '$no_ctrl' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6) AND 
        oss.fue_leida = false AND oss.tipo_observacion_receptor = 3
        ";

        $result = Yii::app()->db->createCommand($query)->queryAll();

        return $result[0]['totalObsAlumno'];
    }

    //Observaciones recibidas de los Alumnos que estan en el programa del supervisor realizando su Servicio Social
    public static function getObservacionesSinLeerSupervisor($rfcSupervisor)
    {
        //(2)-Son las que envia el alumno a su Supervisor y/o Jefe Oficina de Servicio Social
        $query = "
        select COUNT(oss.id_observacion) as \"totalObsSupervisor\" from pe_planeacion.ss_servicio_social ss
        join pe_planeacion.ss_observaciones_servicio_social oss
        on oss.id_servicio_social = ss.id_servicio_social
        where oss.receptor_observ = '$rfcSupervisor' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6) AND 
        oss.fue_leida = false AND oss.tipo_observacion_receptor = 2
        ";

        $result = Yii::app()->db->createCommand($query)->queryAll();
        
        return $result[0]['totalObsSupervisor'];
    }

    //Observaciones recibidas de los Alumnos que estan en realizando su Servicio Social ACTUALMENTE
    public static function getObservacionesSinLeerJefeOfServSocial($rfcJefeOfServSocial)
    {
        $query = 
        "select COUNT(oss.id_observacion) as \"totalObsSupervisor\" from pe_planeacion.ss_servicio_social ss
        join pe_planeacion.ss_observaciones_servicio_social oss
        on oss.id_servicio_social = ss.id_servicio_social
        where oss.receptor_observ = '$rfcJefeOfServSocial' AND (ss.id_estado_servicio_social > 1 AND ss.id_estado_servicio_social < 6) AND 
        oss.fue_leida = false AND oss.tipo_observacion_receptor = 1
        ";

        $result = Yii::app()->db->createCommand($query)->queryAll();
        
        return $result[0]['totalObsSupervisor'];
    }
    //Observaciones recibidas de los Alumnos que estan en realizando su Servicio Social ACTUALMENTE
}
?>