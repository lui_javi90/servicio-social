<?php
class GetDataSSConfiguracion
{
  //Modificacion ss_configuracion
  public static function getFechaLastUpdateConfiguracionMod($id)
  {
      $query =
          "SELECT (case when fecha_modificacion_configuracion is null then 'No disponible' ELSE(
          extract('day' from fecha_modificacion_configuracion) ||' de '|| ( case
          when extract('month' from fecha_modificacion_configuracion)=1 then 'Enero'
          when extract('month' from fecha_modificacion_configuracion)=2 then 'Febrero'
          when extract('month' from fecha_modificacion_configuracion)=3 then 'Marzo'
          when extract('month' from fecha_modificacion_configuracion)=4 then 'Abril'
          when extract('month' from fecha_modificacion_configuracion)=5 then 'Mayo'
          when extract('month' from fecha_modificacion_configuracion)=6 then 'Junio'
          when extract('month' from fecha_modificacion_configuracion)=7 then 'Julio'
          when extract('month' from fecha_modificacion_configuracion)=8 then 'Agosto'
          when extract('month' from fecha_modificacion_configuracion)=9 then 'Septiembre'
          when extract('month' from fecha_modificacion_configuracion)=10 then 'Octubre'
          when extract('month' from fecha_modificacion_configuracion)=11 then 'Noviembre'
          when extract('month' from fecha_modificacion_configuracion)=12 then 'Noviembre'
          END)||' de '||extract('year' from fecha_modificacion_configuracion))end)
          as fecha_act,
          ((case when extract('hour' from fecha_modificacion_configuracion) < 10 then '0' else '' end) ||
          (extract('hour' from fecha_modificacion_configuracion)) || ':' ||
          (case when extract('minute' from fecha_modificacion_configuracion) < 10 then '0' else '' end) ||
          (extract('minute' from fecha_modificacion_configuracion)) || ' ' ||
          (case when extract('hour' from fecha_modificacion_configuracion) >= 12 then 'PM' else 'AM' end)) as hora
          FROM pe_planeacion.ss_configuracion WHERE id_configuracion = $id
      ";

      $fec_act = Yii::app()->db->createCommand($query)->queryAll();

      return $fec_act;
  }

}


?>
