<?php
class ValidacionesServicioSocial
{
    /*VALIDAR QUE EL ALUMNO TENGA DADO DE ALTA EL SERVICIO SOCIAL*/
    public static function validarAltaServicioSocialAlumno($no_ctrl)
    {
        $query =
        "
        SELECT val_serv_social FROM pe_planeacion.ss_status_servicio_social
        WHERE no_ctrl = '$no_ctrl'
        ";

        $validacion = Yii::app()->db->createCommand($query)->queryAll();

        return ($validacion[0]['val_serv_social'] == false || $validacion[0]['val_serv_social'] == null) ? false : $validacion[0]['val_serv_social'];
    }
    /*VALIDAR QUE EL ALUMNO TENGA DADO DE ALTA EL SERVICIO SOCIAL*/

    /*VALIDAR QUE EL ALUMNO NO TENGA SOLICITUDES PENDIENTES O YA ESTE LLEVANDO SERVICIO SOCIAL*/
    public static function validarNoSolicitudesPendientesProgramasServicioSocial($no_ctrl)
    {
        /*Validamos que el alumno con el no. control enviado no tenga solicitudes a programas pendientes*/
        $query ="SELECT * FROM pe_planeacion.ss_solicitud_programa_servicio_social
                WHERE no_ctrl = '$no_ctrl' and (id_estado_solicitud_programa_supervisor = 1 or id_estado_solicitud_programa_supervisor = 2)
                AND (id_estado_solicitud_programa_alumno = 1  OR id_estado_solicitud_programa_alumno = 2)";

        $val_solicitud = Yii::app()->db->createCommand($query)->queryAll();

        return (count($val_solicitud) > 0) ? true : false;
    }
    /*VALIDAR QUE EL ALUMNO NO TENGA SOLICITUDES PENDIENTES O YA ESTE LLEVANDO SERVICIO SOCIAL

    /*VALIDAR QUE NO SE QUIERAN HACER SOLICITUDES A PROGRAMAS DESPUES DE LA FECHA LIMITE DE INSCRIPCION*/
    public static function validarFechaLimiteInscripcion($fec_solicitud)
    {

        $query =
        "
        SELECT fecha_limite_inscripcion FROM pe_planeacion.ss_registro_fechas_servicio_social
        WHERE ssocial_actual = true;
        ";

        $result = Yii::app()->db->createCommand($query)->queryAll();
        $fec_limite = $result[0]['fecha_limite_inscripcion'];

        return ($fec_solicitud <= $fec_limite) ? true  : false;
    }
    /*VALIDAR QUE NO SE QUIERAN HACER SOLICITUDES A PROGRAMAS DESPUES DE LA FECHA LIMITE DE INSCRIPCION*/

    /*VALIDAR SI EXISTE REGISTRO HISTORICO DEL SERVICIO SOCIAL DEL ALUMNO CON EL NO. CONTROL*/
    public static function validaRegistroTotalDatosServicioSocial($no_ctrl)
    {
      $query = "select * from pe_planeacion.ss_historico_totales_alumnos where no_ctrl = '$no_ctrl' ";

      $result = Yii::app()->db->createCommand($query)->queryAll();

      return (count($result) > 0) ? true : false;
    }
    /*VALIDAR SI EXISTE REGISTRO HISTORICO DEL SERVICIO SOCIAL DEL ALUMNO CON EL NO. CONTROL*/

    public static function fechaModificacionDatosEmpresa($id_unidad_receptora)
    {
        $query =
            "SELECT (case when fecha_modificacion_unidad_receptora is null then 'ND' ELSE(
            extract('day' from fecha_modificacion_unidad_receptora) ||' de '|| ( case
            when extract('month' from fecha_modificacion_unidad_receptora)=1 then 'Enero'
            when extract('month' from fecha_modificacion_unidad_receptora)=2 then 'Febrero'
            when extract('month' from fecha_modificacion_unidad_receptora)=3 then 'Marzo'
            when extract('month' from fecha_modificacion_unidad_receptora)=4 then 'Abril'
            when extract('month' from fecha_modificacion_unidad_receptora)=5 then 'Mayo'
            when extract('month' from fecha_modificacion_unidad_receptora)=6 then 'Junio'
            when extract('month' from fecha_modificacion_unidad_receptora)=7 then 'Julio'
            when extract('month' from fecha_modificacion_unidad_receptora)=8 then 'Agosto'
            when extract('month' from fecha_modificacion_unidad_receptora)=9 then 'Septiembre'
            when extract('month' from fecha_modificacion_unidad_receptora)=10 then 'Octubre'
            when extract('month' from fecha_modificacion_unidad_receptora)=11 then 'Noviembre'
            when extract('month' from fecha_modificacion_unidad_receptora)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_modificacion_unidad_receptora))end)
            as fecha_act,
            ((case when extract('hour' from fecha_modificacion_unidad_receptora) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_modificacion_unidad_receptora)) || ':' ||
            (case when extract('minute' from fecha_modificacion_unidad_receptora) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_modificacion_unidad_receptora)) || ' ' ||
            (case when extract('hour' from fecha_modificacion_unidad_receptora) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_unidades_receptoras WHERE id_unidad_receptora = '$id_unidad_receptora'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
}


?>
