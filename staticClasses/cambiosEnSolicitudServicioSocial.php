<?php

$modelAlumnosVigentesInscritosCreditos=AlumnosVigentesInscritosCreditos::model()->findByPK($no_ctrl);
$modelSSHistoricoTotalesAlumnos=SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);
$modelSSProgramas= SsProgramas::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_programa);
$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAllByAttributes(array('id_programa'=>$modelSSSolicitudProgramaServicioSocial->id_programa));

if($modelAlumnosVigentesInscritosCreditos === null)
  throw new CHttpException(404,'No se encontraron datos del alumno.');

if($modelSSHistoricoTotalesAlumnos === null)
  throw new CHttpException(404,'No se encontraron datos de las horas totales.');

if($modelSSProgramas ===null)
  throw new CHttpException(404,'No se encontraron datos del programa.');

//INSERTAR REGISTRO EN LA TABLA servicio_social si se ACEPTO al alumno (2)
if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 2 AND $modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno == 2)
{
  /*Se crea su registro de Servicio Social al alumno*/
  $this->insertRegistroTablaServicioSocial($no_ctrl, $modelSSSolicitudProgramaServicioSocial->id_solicitud_programa, $modelSSSolicitudProgramaServicioSocial->id_programa);

  /*Crear los reportes bimestrales Servicio Social Semestral y Anual*/
  if($modelSSProgramas->id_periodo_programa == 1 or $modelSSProgramas->id_periodo_programa == 2)
  {
    if(Yii::app()->db->createCommand("select agregarReportesBimestralesAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll() &&
    Yii::app()->db->createCommand("select agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
    {
      /*Agregar firma de aceptado del supervisor */
      $this->agregarFirmaSupervisor($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
      /*Una vez registrado todo, sino hubo problemas ahora asi restar el campo lugares_disponibles en la tabla programas_servicio_social*/
      $this->restarLugarDisponibleEnPrograma($modelSSProgramas);
      $modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
      if($modelSSServicioSocial ===null)
        throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

      $modelSSProgramas->fecha_fin_programa = $this->obtenerFechaFinServicioSocial($modelSSServicioSocial->id_servicio_social);
      $modelSSProgramas->save();

    }else{
      Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
    }
    /*Crear los reportes bimestrales Servicio Social Semestral y Anual*/
  }else
  /*Crear los reportes bimestrales Servicio Social Unica vez*/
  if($modelSSProgramas->id_periodo_programa == 3)
  {
    if(Yii::app()->db->createCommand("select agregarReportesBimestralesExtraTemporal('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa', '$modelSSProgramas->fecha_fin_programa')")->queryAll() &&
    Yii::app()->db->createCommand("select agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
    {
      /*Agregar firma de aceptado del supervisor */
      $this->agregarFirmaSupervisor($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
      /*Una vez registrado todo, sino hubo problemas ahora asi restar el campo lugares_disponibles en la tabla programas_servicio_social*/
      $this->restarLugarDisponibleEnPrograma($modelSSProgramas);
      $modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
      if($modelSSServicioSocial ===null)
        throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

    }else{

      Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
    }
  }
  /*Crear los reportes bimestrales Servicio Social Unica vez*/
}else if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 4 OR $modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno == 4){

  /*ACTUALIZAR id_estado_solicitud_programa EN LA TABLA solicitud_programa_servicio_social si se RECHAZO al alumno (3)*/
  $this->actualizarRegistroTablaSolicitudPrograma($id_solicitud_programa, $no_ctrl, $modelSSSolicitudProgramaServicioSocial->id_programa, 4);
}
