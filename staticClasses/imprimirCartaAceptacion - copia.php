<style>
	.hoja{
        background-color: #FFFFFF;
    }
    .div1{
        background-color: #FFFFFF;
        font-weight: bold;
    }
    .div2{
        background-color: #FFFFFF;
    }
    table, th, td{
        padding: 0px;
        padding-left: 0px;
        padding-bottom: 0px;
        padding-right: 0px;
        text-align: left;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 12px;
    }
    .letra2{
        font-size: 10px;
    }
    .letra3{
        font-size: 14px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado2{
        /*letter-spacing: 1pt;       para separar entre letras */
        word-spacing: 1.5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado3{
        line-height: 1pt;
    }
    #contenedor{
      margin-left:0px;
      margin-top:0px;
      padding:0 0 0 0;
      width:auto;
      /*background-color: #2196F3;*/
      position:absolute;
  }
  .divcontenido1{

      float:left;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
  }
  .divcontenido2{

      float:right;
      /*color:white;*/
      font-size:10px;
      padding:0px;
      border-style:groove;
      width:47%;
      text-align:left;
      margin-left:0px;
  }
</style>

<div class="hojas">
	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner_top_itc.jpg" />
    <br><br><br><br>
    <!--fecha, oficio y asunto-->
    <div class="div2 interlineado3">
        <table style="width:500px" class="interlineado3 letra3" align="right">
            <tr class="interlineado3">
                <td align="right"><?php echo "CELAYA, GTO.,&nbsp;<span style='background-color:black;color:white;' class='bold'>".$fec_reporte."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "NO. DE OFICIO:&nbsp;<span class='bold'>."$datos_carta_aceptacion[0]['folio']."/".$datos_carta_aceptacion[0]['anio_servicio_social']."</span>"; ?></td>
            </tr>
            <tr class="interlineado3">
                <td align="right"><?php echo "ASUNTO:&nbsp;<span class='bold'>ACEPTACIÓN DE SERVICIO SOCIAL</span>"; ?></td>
            </tr>
        </table>
    </div>
    <!--fecha, oficio y asunto-->

    <!--Datos del supervisor del programa-->
    <br><br><br>
    <div class="div2 interlineado1">
        <h6 class="bold letra3 interlineado1">C.M.E.D. IGNACIO LÓPEZ VALDOVINOS</h6>
        <h6 class="bold letra3 interlineado1">DIRECTOR<</h6>
        <h6 class="bold letra3 interlineado1"><?php echo $datos_carta_aceptacion[0]['nameempresa']; ?></h6>
        <h6 class="bold letra3 interlineado1">PRESENTE</h6>
    </div>
    <!--Datos del supervisor del programa-->

    <!--Datos del jefe del depto. de vinculacion-->
    <br>
    <div class="div2 interlineado1 right">
    	<h6 class="bold letra3 interlineado1"><?php echo "AT´N: M.G.A. ERNESTO LUGO LEDESMA"; ?></h6>
        <h6 class="bold letra3 interlineado1"><?php echo "Departamento de Gestión Tecnológica y Vinculación"; ?></h6>
    </div>
    <!--Datos del jefe del depto. de vinculacion-->

    <!--CUERPO DEL REPORTE-->
    <br><br>
    <div class="interlineado2 div2 letra3">
        <p ALIGN="justify">
            Por medio de la presente me permito informarle que el (a) <?php echo "<span class='bold'>"."C. ".$datos_carta_aceptacion[0]['namealumno']."</span>";?>, estudiante de la carrera de <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['carrera']."</span>"; ?>, con número de control <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['no_ctrl']."</span>"; ?>, fue aceptado (a) para realizar su <?php echo "<span class='bold'>Servicio Social</span>"; ?> en <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['nombre_programa']."</span>"; ?>, donde cubrirá un total de <?php echo "<span class='bold'>".$datos_carta_aceptacion[0]['horas_totales']." horas"."</span>"; ?> a partir del dia <?php echo "<span class='bold'>".$fecha_inicio."</span>"; ?> al <?php echo "<span class='bold'>".$fecha_fin."</span>"; ?>, laborando un total de <?php echo "<span class='bold'>".$horas_diarias."</span>"; ?> horas diarias, en un lapso mínimo de seis meses,  no excediéndose de dos años.
            <br><br>
            Sin otro particular por el momento, aprovecho la ocasión para enviarle un cordial saludo.
        </p>
    </div>
    <!--CUERPO DEL REPORTE-->

    <!--Firma del encargado-->
    <br><br>
    <div class="interlineado1 div2 left">
        <h6 class="interlineado1 bold letra3">A T E N T A M E N T E</h6>
        <i class="interlineado1 letra3">LA TÉCNICA POR UN MÉXICO MEJOR &reg;</i>
    </div>

    <div id="contenedor">
        <div class="divcontenido1 left">
            <!--Si la firma no viene entonces deje el espacio en blanco-->
            <br>
            <?php
            if($datos_carta_aceptacion[0]['valida_solicitud_supervisor_programa'] == NULL){
                echo "<br><br><br><br>";
            }else{
            ?>
            <img height="90" src="<?php echo Yii::app()->request->baseUrl; ?>/images/catalina.jpg" />
            <?php } ?>
            <h6 class="interlineado1 bold letra3"><?php echo strtoupper($datos_carta_aceptacion[0]['namesupervisor']); ?></h6>
            <h6 class="interlineado1 bold letra3"><?php echo strtoupper($datos_carta_aceptacion[0]['cargo_supervisor']); ?></h6>
        </div>
        <div class="divcontenido2 center">
            <img height="150" src="<?php echo Yii::app()->request->baseUrl; ?>/images/sello_sep.jpg" />
        </div>
    </div>
    <!--Firma del encargado-->

    <!--Datos del archivo-->
    <br><br>
    <div class="div2 left">
        <h6 class="interlineado1 letra3"><?php echo "C.c.p.-Archivo"; ?></h6>
        <h6 class="interlineado1 letra3"><?php echo "AJRG/iov*"; ?></h6>
    </div>
    <!--Datos del archivo-->

</div>
