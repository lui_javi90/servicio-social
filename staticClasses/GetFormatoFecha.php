<?php
class GetFormatoFecha
{
    /*Damos formato a la fecha*/
    public static function getFormatFec($fecha)
    {
        $fechaComoEntero = strtotime($fecha);

        //Obtenemos el dia, mes y año
        $dia = date("d", $fechaComoEntero);
        $mes = GetFormatoFecha::getMes(date("m", $fechaComoEntero));
        $anio = date("Y", $fechaComoEntero);

      return $dia." de ".$mes." de ".$anio;
    }
    /*Damos formato a la fecha*/

    public static function getMes($mes)
    {

      $_mes="";

      switch ($mes)
      {
        case '01': $_mes = "ENERO"; break;
        case '02': $_mes = "FEBRERO"; break;
        case '03': $_mes = "MARZO"; break;
        case '04': $_mes = "ABRIL"; break;
        case '05': $_mes = "MAYO"; break;
        case '06': $_mes = "JUNIO"; break;
        case '07': $_mes = "JULIO"; break;
        case '08': $_mes = "AGOSTO"; break;
        case '09': $_mes = "SEPTIEMBRE"; break;
        case '10': $_mes = "OCTUBRE"; break;
        case '11': $_mes = "NOVIEMBRE"; break;
        case '12': $_mes = "DICIEMBRE"; break;
        default: $_mes = "DESCONOCIDO"; break;
      }

      return $_mes;
    }

    public static function getFechaLastUpdateHorasTotalesAlumno($no_ctrl)
    {
        $query =
            "SELECT (case when fecha_modificacion_horas_totales is null then 'No disponible' ELSE(
            extract('day' from fecha_modificacion_horas_totales) ||' de '|| ( case
            when extract('month' from fecha_modificacion_horas_totales)=1 then 'Enero'
            when extract('month' from fecha_modificacion_horas_totales)=2 then 'Febrero'
            when extract('month' from fecha_modificacion_horas_totales)=3 then 'Marzo'
            when extract('month' from fecha_modificacion_horas_totales)=4 then 'Abril'
            when extract('month' from fecha_modificacion_horas_totales)=5 then 'Mayo'
            when extract('month' from fecha_modificacion_horas_totales)=6 then 'Junio'
            when extract('month' from fecha_modificacion_horas_totales)=7 then 'Julio'
            when extract('month' from fecha_modificacion_horas_totales)=8 then 'Agosto'
            when extract('month' from fecha_modificacion_horas_totales)=9 then 'Septiembre'
            when extract('month' from fecha_modificacion_horas_totales)=10 then 'Octubre'
            when extract('month' from fecha_modificacion_horas_totales)=11 then 'Noviembre'
            when extract('month' from fecha_modificacion_horas_totales)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_modificacion_horas_totales))end)
            as fecha_act,
            ((case when extract('hour' from fecha_modificacion_horas_totales) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_modificacion_horas_totales)) || ':' ||
            (case when extract('minute' from fecha_modificacion_horas_totales) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_modificacion_horas_totales)) || ' ' ||
            (case when extract('hour' from fecha_modificacion_horas_totales) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_historico_totales_alumnos WHERE no_ctrl='$no_ctrl'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Fecha Modificacion del Programa
    public static function getFechaLastUpdateProgramasMod($id_programa)
    {
        $query =
            "SELECT (case when fecha_modificacion_programa is null then 'No disponible' ELSE(
            extract('day' from fecha_modificacion_programa) ||' de '|| ( case
            when extract('month' from fecha_modificacion_programa)=1 then 'Enero'
            when extract('month' from fecha_modificacion_programa)=2 then 'Febrero'
            when extract('month' from fecha_modificacion_programa)=3 then 'Marzo'
            when extract('month' from fecha_modificacion_programa)=4 then 'Abril'
            when extract('month' from fecha_modificacion_programa)=5 then 'Mayo'
            when extract('month' from fecha_modificacion_programa)=6 then 'Junio'
            when extract('month' from fecha_modificacion_programa)=7 then 'Julio'
            when extract('month' from fecha_modificacion_programa)=8 then 'Agosto'
            when extract('month' from fecha_modificacion_programa)=9 then 'Septiembre'
            when extract('month' from fecha_modificacion_programa)=10 then 'Octubre'
            when extract('month' from fecha_modificacion_programa)=11 then 'Noviembre'
            when extract('month' from fecha_modificacion_programa)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_modificacion_programa))end)
            as fecha_act,
            ((case when extract('hour' from fecha_modificacion_programa) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_modificacion_programa)) || ':' ||
            (case when extract('minute' from fecha_modificacion_programa) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_modificacion_programa)) || ' ' ||
            (case when extract('hour' from fecha_modificacion_programa) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_programas WHERE id_programa = '$id_programa'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Fecha de Registro del Programa
    public static function getFechaLastUpdateProgramasReg($id_programa)
    {
        $query =
            "SELECT (case when fecha_registro_programa is null then 'No disponible' ELSE(
            extract('day' from fecha_registro_programa) ||' de '|| ( case
            when extract('month' from fecha_registro_programa)=1 then 'Enero'
            when extract('month' from fecha_registro_programa)=2 then 'Febrero'
            when extract('month' from fecha_registro_programa)=3 then 'Marzo'
            when extract('month' from fecha_registro_programa)=4 then 'Abril'
            when extract('month' from fecha_registro_programa)=5 then 'Mayo'
            when extract('month' from fecha_registro_programa)=6 then 'Junio'
            when extract('month' from fecha_registro_programa)=7 then 'Julio'
            when extract('month' from fecha_registro_programa)=8 then 'Agosto'
            when extract('month' from fecha_registro_programa)=9 then 'Septiembre'
            when extract('month' from fecha_registro_programa)=10 then 'Octubre'
            when extract('month' from fecha_registro_programa)=11 then 'Noviembre'
            when extract('month' from fecha_registro_programa)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_registro_programa))end)
            as fecha_act,
            ((case when extract('hour' from fecha_registro_programa) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_registro_programa)) || ':' ||
            (case when extract('minute' from fecha_registro_programa) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_registro_programa)) || ' ' ||
            (case when extract('hour' from fecha_registro_programa) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_programas WHERE id_programa = '$id_programa'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Fecha de envio de observacion
    public static function getFechaEnvioObservacionServicioSocial($id_observacion)
    {
        $query =
            "SELECT (case when fecha_registro is null then 'Sin Ver' ELSE(
            extract('day' from fecha_registro) ||' de '|| ( case
            when extract('month' from fecha_registro)=1 then 'Enero'
            when extract('month' from fecha_registro)=2 then 'Febrero'
            when extract('month' from fecha_registro)=3 then 'Marzo'
            when extract('month' from fecha_registro)=4 then 'Abril'
            when extract('month' from fecha_registro)=5 then 'Mayo'
            when extract('month' from fecha_registro)=6 then 'Junio'
            when extract('month' from fecha_registro)=7 then 'Julio'
            when extract('month' from fecha_registro)=8 then 'Agosto'
            when extract('month' from fecha_registro)=9 then 'Septiembre'
            when extract('month' from fecha_registro)=10 then 'Octubre'
            when extract('month' from fecha_registro)=11 then 'Noviembre'
            when extract('month' from fecha_registro)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_registro))end)
            as fecha_act,
            ((case when extract('hour' from fecha_registro) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_registro)) || ':' ||
            (case when extract('minute' from fecha_registro) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_registro)) || ' ' ||
            (case when extract('hour' from fecha_registro) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_observaciones_servicio_social WHERE id_observacion= '$id_observacion'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
    //Fecha de envio de observacion

     //Fecha en que el Alumno vio la observaciones enviada
     public static function getFechaVistaObservacionServicioSocial($id_observacion)
     {
         $query =
             "SELECT (case when fecha_leida is null then 'Sin Ver' ELSE(
             extract('day' from fecha_leida) ||' de '|| ( case
             when extract('month' from fecha_leida)=1 then 'Enero'
             when extract('month' from fecha_leida)=2 then 'Febrero'
             when extract('month' from fecha_leida)=3 then 'Marzo'
             when extract('month' from fecha_leida)=4 then 'Abril'
             when extract('month' from fecha_leida)=5 then 'Mayo'
             when extract('month' from fecha_leida)=6 then 'Junio'
             when extract('month' from fecha_leida)=7 then 'Julio'
             when extract('month' from fecha_leida)=8 then 'Agosto'
             when extract('month' from fecha_leida)=9 then 'Septiembre'
             when extract('month' from fecha_leida)=10 then 'Octubre'
             when extract('month' from fecha_leida)=11 then 'Noviembre'
             when extract('month' from fecha_leida)=12 then 'Diciembre'
             END)||' de '||extract('year' from fecha_leida))end)
             as fecha_act,
             ((case when extract('hour' from fecha_leida) < 10 then '0' else '' end) ||
             (extract('hour' from fecha_leida)) || ':' ||
             (case when extract('minute' from fecha_leida) < 10 then '0' else '' end) ||
             (extract('minute' from fecha_leida)) || ' ' ||
             (case when extract('hour' from fecha_leida) >= 12 then 'PM' else 'AM' end)) as hora
             FROM pe_planeacion.ss_observaciones_servicio_social WHERE id_observacion= '$id_observacion'
         ";
         $fec_act = Yii::app()->db->createCommand($query)->queryAll();
 
         return $fec_act;
     }
      //Fecha en que el Alumno vio la observaciones enviada

    /*Fecha de inicio de Plan de Trabajo tomada de Servicio Social*/
    public static function getFechaInicioPlanServicioSocial($id_servicio_social)
    {
        $query =
            "SELECT (case when fecha_inicio is null then 'NO DISPONIBLE' ELSE(
            extract('day' from fecha_inicio) ||' de '|| ( case
            when extract('month' from fecha_inicio)=1 then 'Enero'
            when extract('month' from fecha_inicio)=2 then 'Febrero'
            when extract('month' from fecha_inicio)=3 then 'Marzo'
            when extract('month' from fecha_inicio)=4 then 'Abril'
            when extract('month' from fecha_inicio)=5 then 'Mayo'
            when extract('month' from fecha_inicio)=6 then 'Junio'
            when extract('month' from fecha_inicio)=7 then 'Julio'
            when extract('month' from fecha_inicio)=8 then 'Agosto'
            when extract('month' from fecha_inicio)=9 then 'Septiembre'
            when extract('month' from fecha_inicio)=10 then 'Octubre'
            when extract('month' from fecha_inicio)=11 then 'Noviembre'
            when extract('month' from fecha_inicio)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_inicio))end)
            as fecha_act
            FROM pe_planeacion.ss_servicio_social WHERE id_servicio_social = '$id_servicio_social'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
    /*Fecha de inicio de Plan de Trabajo */

    /*Fecha de Fin de Plan de Trabajo tomada de Servicio Social*/
    public static function getFechaFinPlanServicioSocial($id_servicio_social)
    {
        $query =
            "SELECT (case when fecha_fin is null then 'NO DISPONIBLE' ELSE(
            extract('day' from fecha_fin) ||' de '|| ( case
            when extract('month' from fecha_fin)=1 then 'Enero'
            when extract('month' from fecha_fin)=2 then 'Febrero'
            when extract('month' from fecha_fin)=3 then 'Marzo'
            when extract('month' from fecha_fin)=4 then 'Abril'
            when extract('month' from fecha_fin)=5 then 'Mayo'
            when extract('month' from fecha_fin)=6 then 'Junio'
            when extract('month' from fecha_fin)=7 then 'Julio'
            when extract('month' from fecha_fin)=8 then 'Agosto'
            when extract('month' from fecha_fin)=9 then 'Septiembre'
            when extract('month' from fecha_fin)=10 then 'Octubre'
            when extract('month' from fecha_fin)=11 then 'Noviembre'
            when extract('month' from fecha_fin)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_fin))end)
            as fecha_act
            FROM pe_planeacion.ss_servicio_social WHERE id_servicio_social = '$id_servicio_social'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
    /*Fecha de Fin de Plan de Trabajo */

    /*Fecha en que se envia a evaluacion el reporte bimestral por parte del alumno*/
    public static function getFechaValidaAlumnoReporteBimestral($id_reporte_bimestral, $id_servicio_social)
    {
      $query =
          "SELECT (case when envio_alum_evaluacion is null then 'SIN EVALUAR' ELSE(
          extract('day' from envio_alum_evaluacion) ||' de '|| ( case
          when extract('month' from envio_alum_evaluacion)=1 then 'Enero'
          when extract('month' from envio_alum_evaluacion)=2 then 'Febrero'
          when extract('month' from envio_alum_evaluacion)=3 then 'Marzo'
          when extract('month' from envio_alum_evaluacion)=4 then 'Abril'
          when extract('month' from envio_alum_evaluacion)=5 then 'Mayo'
          when extract('month' from envio_alum_evaluacion)=6 then 'Junio'
          when extract('month' from envio_alum_evaluacion)=7 then 'Julio'
          when extract('month' from envio_alum_evaluacion)=8 then 'Agosto'
          when extract('month' from envio_alum_evaluacion)=9 then 'Septiembre'
          when extract('month' from envio_alum_evaluacion)=10 then 'Octubre'
          when extract('month' from envio_alum_evaluacion)=11 then 'Noviembre'
          when extract('month' from envio_alum_evaluacion)=12 then 'Diciembre'
          END)||' de '||extract('year' from envio_alum_evaluacion))end)
          as fecha_act,
          ((case when extract('hour' from envio_alum_evaluacion) < 10 then '0' else '' end) ||
          (extract('hour' from envio_alum_evaluacion)) || ':' ||
          (case when extract('minute' from envio_alum_evaluacion) < 10 then '0' else '' end) ||
          (extract('minute' from envio_alum_evaluacion)) || ' ' ||
          (case when extract('hour' from envio_alum_evaluacion) >= 12 then 'PM' else 'AM' end)) as hora
          FROM pe_planeacion.ss_reportes_bimestral WHERE id_reporte_bimestral = '$id_reporte_bimestral'
          AND id_servicio_social = '$id_servicio_social'
      ";

      $fec_act = Yii::app()->db->createCommand($query)->queryAll();

      return $fec_act;
    }
    /*Fecha en que se envia a evaluacion el reporte bimestral por parte del alumno*/

    /*Fecha en que se evalua el reporte bimestral por parte del supervisor*/
    public static function getFechaValidaSupervisorReporteBimestral($id_reporte_bimestral, $id_servicio_social)
    {
      $query =
          "SELECT (case when valida_responsable is null then 'SIN EVALUAR' ELSE(
          extract('day' from valida_responsable) ||' de '|| ( case
          when extract('month' from valida_responsable)=1 then 'Enero'
          when extract('month' from valida_responsable)=2 then 'Febrero'
          when extract('month' from valida_responsable)=3 then 'Marzo'
          when extract('month' from valida_responsable)=4 then 'Abril'
          when extract('month' from valida_responsable)=5 then 'Mayo'
          when extract('month' from valida_responsable)=6 then 'Junio'
          when extract('month' from valida_responsable)=7 then 'Julio'
          when extract('month' from valida_responsable)=8 then 'Agosto'
          when extract('month' from valida_responsable)=9 then 'Septiembre'
          when extract('month' from valida_responsable)=10 then 'Octubre'
          when extract('month' from valida_responsable)=11 then 'Noviembre'
          when extract('month' from valida_responsable)=12 then 'Diciembre'
          END)||' de '||extract('year' from valida_responsable))end)
          as fecha_act,
          ((case when extract('hour' from valida_responsable) < 10 then '0' else '' end) ||
          (extract('hour' from valida_responsable)) || ':' ||
          (case when extract('minute' from valida_responsable) < 10 then '0' else '' end) ||
          (extract('minute' from valida_responsable)) || ' ' ||
          (case when extract('hour' from valida_responsable) >= 12 then 'PM' else 'AM' end)) as hora
          FROM pe_planeacion.ss_reportes_bimestral WHERE id_reporte_bimestral = '$id_reporte_bimestral'
          AND id_servicio_social = '$id_servicio_social'
      ";

      $fec_act = Yii::app()->db->createCommand($query)->queryAll();

      return $fec_act;
    }
    /*Fecha en que se evalua el reporte bimestral por parte del supervisor*/

    /*Fecha en que se evalua el reporte bimestral por parte de vinculacion*/
    public static function getFechaValidaAdminReporteBimestral($id_reporte_bimestral, $id_servicio_social)
    {
      $query =
      "SELECT (case when valida_oficina_servicio_social is null then 'SIN EVALUAR' ELSE(
          extract('day' from valida_oficina_servicio_social) ||' de '|| ( case
          when extract('month' from valida_oficina_servicio_social)=1 then 'Enero'
          when extract('month' from valida_oficina_servicio_social)=2 then 'Febrero'
          when extract('month' from valida_oficina_servicio_social)=3 then 'Marzo'
          when extract('month' from valida_oficina_servicio_social)=4 then 'Abril'
          when extract('month' from valida_oficina_servicio_social)=5 then 'Mayo'
          when extract('month' from valida_oficina_servicio_social)=6 then 'Junio'
          when extract('month' from valida_oficina_servicio_social)=7 then 'Julio'
          when extract('month' from valida_oficina_servicio_social)=8 then 'Agosto'
          when extract('month' from valida_oficina_servicio_social)=9 then 'Septiembre'
          when extract('month' from valida_oficina_servicio_social)=10 then 'Octubre'
          when extract('month' from valida_oficina_servicio_social)=11 then 'Noviembre'
          when extract('month' from valida_oficina_servicio_social)=12 then 'Diciembre'
          END)||' de '||extract('year' from valida_oficina_servicio_social))end)
          as fecha_act,
          ((case when extract('hour' from valida_oficina_servicio_social) < 10 then '0' else '' end) ||
          (extract('hour' from valida_oficina_servicio_social)) || ':' ||
          (case when extract('minute' from valida_oficina_servicio_social) < 10 then '0' else '' end) ||
          (extract('minute' from valida_oficina_servicio_social)) || ' ' ||
          (case when extract('hour' from valida_oficina_servicio_social) >= 12 then 'PM' else 'AM' end)) as hora
          FROM pe_planeacion.ss_reportes_bimestral WHERE id_reporte_bimestral = '$id_reporte_bimestral'
          AND id_servicio_social = '$id_servicio_social'
      ";

      $fec_act = Yii::app()->db->createCommand($query)->queryAll();

      return $fec_act;
    }
    /*Fecha en que se evalua el reporte bimestral por parte de vinculacion*/

    public static function getFechaLimiteEvaluacionReporteBimestral($id_reporte_bimestral, $id_servicio_social)
    {
      $query =
          "SELECT (case when fecha_fin_rep_bim is null then 'NO DISPONIBLE' ELSE(
          extract('day' from fecha_fin_rep_bim) ||' de '|| ( case
          when extract('month' from fecha_fin_rep_bim)=1 then 'Enero'
          when extract('month' from fecha_fin_rep_bim)=2 then 'Febrero'
          when extract('month' from fecha_fin_rep_bim)=3 then 'Marzo'
          when extract('month' from fecha_fin_rep_bim)=4 then 'Abril'
          when extract('month' from fecha_fin_rep_bim)=5 then 'Mayo'
          when extract('month' from fecha_fin_rep_bim)=6 then 'Junio'
          when extract('month' from fecha_fin_rep_bim)=7 then 'Julio'
          when extract('month' from fecha_fin_rep_bim)=8 then 'Agosto'
          when extract('month' from fecha_fin_rep_bim)=9 then 'Septiembre'
          when extract('month' from fecha_fin_rep_bim)=10 then 'Octubre'
          when extract('month' from fecha_fin_rep_bim)=11 then 'Noviembre'
          when extract('month' from fecha_fin_rep_bim)=12 then 'Diciembre'
          END)||' de '||extract('year' from fecha_fin_rep_bim))end)
          as fecha_act
          FROM pe_planeacion.ss_reportes_bimestral WHERE id_reporte_bimestral = '$id_reporte_bimestral'
          AND id_servicio_social = '$id_servicio_social'
      ";

      $fec_act = Yii::app()->db->createCommand($query)->queryAll();

      return $fec_act;

    }

    /*FORMATO FECHA DE PRE-REGISTRO */
    public static function getFechaPreRegistro($no_ctrl)
    {
        $query =
            "SELECT (case when fecha_preregistro is null then 'NO DISPONIBLE' ELSE(
            extract('day' from fecha_preregistro) ||' de '|| ( case
            when extract('month' from fecha_preregistro)=1 then 'Enero'
            when extract('month' from fecha_preregistro)=2 then 'Febrero'
            when extract('month' from fecha_preregistro)=3 then 'Marzo'
            when extract('month' from fecha_preregistro)=4 then 'Abril'
            when extract('month' from fecha_preregistro)=5 then 'Mayo'
            when extract('month' from fecha_preregistro)=6 then 'Junio'
            when extract('month' from fecha_preregistro)=7 then 'Julio'
            when extract('month' from fecha_preregistro)=8 then 'Agosto'
            when extract('month' from fecha_preregistro)=9 then 'Septiembre'
            when extract('month' from fecha_preregistro)=10 then 'Octubre'
            when extract('month' from fecha_preregistro)=11 then 'Noviembre'
            when extract('month' from fecha_preregistro)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_preregistro))end)
            as fecha_act,
            ((case when extract('hour' from fecha_preregistro) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_preregistro)) || ':' ||
            (case when extract('minute' from fecha_preregistro) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_preregistro)) || ' ' ||
            (case when extract('hour' from fecha_preregistro) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_planeacion.ss_status_servicio_social WHERE no_ctrl = '$no_ctrl'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }
    /*FORMATO FECHA DE PRE-REGISTRO */


}//Fin de la clase GetFormatoFecha

?>
