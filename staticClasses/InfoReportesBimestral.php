<?php
class InfoReportesBimestral
{
    /*Fecha de inicio de Reporte Bimestral */
    public static function getFechaInicioReporteBimestral($id_reporte_bimestral)
    {
        $query =
            "SELECT (case when fecha_inicio_rep_bim is null then 'NO DISPONIBLE' ELSE(
            extract('day' from fecha_inicio_rep_bim) ||' de '|| ( case 
            when extract('month' from fecha_inicio_rep_bim)=1 then 'Enero'
            when extract('month' from fecha_inicio_rep_bim)=2 then 'Febrero'
            when extract('month' from fecha_inicio_rep_bim)=3 then 'Marzo'
            when extract('month' from fecha_inicio_rep_bim)=4 then 'Abril'
            when extract('month' from fecha_inicio_rep_bim)=5 then 'Mayo'
            when extract('month' from fecha_inicio_rep_bim)=6 then 'Junio'
            when extract('month' from fecha_inicio_rep_bim)=7 then 'Julio'
            when extract('month' from fecha_inicio_rep_bim)=8 then 'Agosto'
            when extract('month' from fecha_inicio_rep_bim)=9 then 'Septiembre'
            when extract('month' from fecha_inicio_rep_bim)=10 then 'Octubre'
            when extract('month' from fecha_inicio_rep_bim)=11 then 'Noviembre'
            when extract('month' from fecha_inicio_rep_bim)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_inicio_rep_bim))end) 
            as fecha_ini
            FROM pe_planeacion.ss_reportes_bimestral WHERE id_reporte_bimestral = '$id_reporte_bimestral'
        ";

        $fecha_ini = Yii::app()->db->createCommand($query)->queryAll();

        return $fecha_ini;
    }
    /*Fecha de inicio de Reporte Bimestral */

     /*Fecha de fin de Reporte Bimestral */
     public static function getFechaFinReporteBimestral($id_reporte_bimestral)
     {
         $query =
             "SELECT (case when fecha_fin_rep_bim is null then 'NO DISPONIBLE' ELSE(
             extract('day' from fecha_fin_rep_bim) ||' de '|| ( case 
             when extract('month' from fecha_fin_rep_bim)=1 then 'Enero'
             when extract('month' from fecha_fin_rep_bim)=2 then 'Febrero'
             when extract('month' from fecha_fin_rep_bim)=3 then 'Marzo'
             when extract('month' from fecha_fin_rep_bim)=4 then 'Abril'
             when extract('month' from fecha_fin_rep_bim)=5 then 'Mayo'
             when extract('month' from fecha_fin_rep_bim)=6 then 'Junio'
             when extract('month' from fecha_fin_rep_bim)=7 then 'Julio'
             when extract('month' from fecha_fin_rep_bim)=8 then 'Agosto'
             when extract('month' from fecha_fin_rep_bim)=9 then 'Septiembre'
             when extract('month' from fecha_fin_rep_bim)=10 then 'Octubre'
             when extract('month' from fecha_fin_rep_bim)=11 then 'Noviembre'
             when extract('month' from fecha_fin_rep_bim)=12 then 'Diciembre'
             END)||' de '|| extract('year' from fecha_fin_rep_bim))end) 
             as fecha_fin
             FROM pe_planeacion.ss_reportes_bimestral WHERE id_reporte_bimestral = '$id_reporte_bimestral'
         ";
 
         $fecha_fin = Yii::app()->db->createCommand($query)->queryAll();
 
         return $fecha_fin;
     }
     /*Fecha de fin de Reporte Bimestral */

}

?>