<?php
class RegularExpression
{
    //Valida que el dato introducido sea un numero entero
    public static function isEnteroPositivo($valor)
	{
  		$bandera = true;

  		if(!preg_match('/^[0-9]+$/', $valor))
  			$bandera = false;

  		return $bandera;
	}
}
?>
