<?php

class EDatosAlumnoController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		//'listaAlumnos', //Vista para dar de ALTA el Servicio Social
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'altaAlumno', //Para que el Admin de de ALTA Aumnos a Servicio Social (casos especiales)
								 'listaAlumnosAltaServicioSocial', //Alumnos que estan dados de ALTA en el Servicio Social
								 'statusServicioSocial', //Cambiar estatus de Servicio Social del Alumno
								 'carreraAlumno', //Nos traemos la carrera del alumno
								 'infoAlumno', //Traemos los datos del alumno
								 'detalleAlumnos'
								),
				//'roles' =>array('vinculacion_oficina_servicio_social'),
				'users' => array('@')
				),
			array('allow',
				   'actions'=>array('preRegistroAlumnoServicioSocial',
									'carreraAlumno',
									'infoAlumno'
								),
				//'roles'=>array('alumno'),
				'users' => array('@')
           		),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionDetalleAlumnos($no_ctrl)
	{
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($no_ctrl);

		if($modelEDatosAlumno === NULL)
			throw new CHttpException(404,'No se encontraron datos del Alumno.');

		$this->renderPartial('detalleAlumnos', array('modelEDatosAlumno' => $modelEDatosAlumno),false,true);
	}

	/*LISTA DE LOS ALUMNOS QUE CUMPLEN LOS CREDITOS DE CARRERA Y DIERON DE ALTA EL
	SERVICIO SOCIAL*/
	public function actionListaAlumnosAltaServicioSocial()
	{

		$modelEDatosAlumnos = new EDatosAlumno_('search');
		$modelEDatosAlumnos->unsetAttributes();  // clear any default values

		//lista de las carreras
		$lista_carreras = $this->getCarreras();

		if(isset($_GET['EDatosAlumno'])){

			$modelEDatosAlumnos->attributes=$_GET['EDatosAlumno'];
		}

		$this->render('listaAlumnosAltaServicioSocial',array(
					  'modelEDatosAlumnos'=>$modelEDatosAlumnos,
					  'lista_carreras' => $lista_carreras
		));
	}
	/*LISTA DE LOS ALUMNOS QUE CUMPLEN LOS CREDITOS DE CARRERA Y DIERON DE ALTA EL
	SERVICIO SOCIAL*/

	//Aprobar Servicio Social
	public function actionStatusServicioSocial($no_ctrl, $oper)
	{
		//0 desactivar
		//1 activar
		$modelEDatosAlumno = $this->loadModel($no_ctrl);
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($modelEDatosAlumno->nctrAlumno);

		if($modelSSStatusServicioSocial === null)
			throw new CHttpException(404,'No se encontraron datos del Alumno.');

		$periodo = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
		$anio = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual

		$modelSSStatusServicioSocial->val_serv_social = $oper;
		$modelSSStatusServicioSocial->fecha_preregistro = date('Y-m-d H:i:s');
		$modelSSStatusServicioSocial->periodo = $periodo[0]['periodoactual'];
		$modelSSStatusServicioSocial->anio = $anio[0]['periodoactual'];

		if($modelSSStatusServicioSocial->save())
		{
			//Se crea el registro del alumno para las horas totales de servicio social, solo la primera vez que se da de ALTA el Servicio Social
			$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk(trim($modelEDatosAlumno->nctrAlumno));
			if($modelSSHistoricoTotalesAlumnos === null)
				$this->activarHorasTotalesAlumnoServicioSocial($modelEDatosAlumno->nctrAlumno);

			echo CJSON::encode( [ 'code' => 200 ] );
		}
			else{
			echo CJSON::encode( $modelSSStatusServicioSocial->getErrors() );
		}
	}
	//Aprobar Servicio Social

	public function getChangeStatusServicioSocial($no_ctrl)
	{
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($no_ctrl);

		if($modelSSStatusServicioSocial === null)
			throw new CHttpException(404,'No se encontraron datos del Alumno.');

		return $modelSSStatusServicioSocial;

	}

	public function getCadena($no_ctrl)
	{
		$noctrl = "";

		//strlen: obtener la longitud de una cadena string
		for($i=0; $i<strlen($no_ctrl); $i++){ $noctrl = $noctrl.$no_ctrl[$i]; }

		return trim($noctrl);
	}

	public function actionPreRegistroAlumnoServicioSocial()
	{
		//$this->checkAccess('alumno');
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = trim(Yii::app()->params['no_ctrl']);
		//$no_ctrl = Yii::app()->user->name;

		$status = $this->getEstatusServicioSocial($no_ctrl);
		$no_control[$no_ctrl] = $no_ctrl;

		//Bandera para ver si ya alvanza los creditos suficientes para llevar Servicio Social
		$puedeTomarServicioSocialC = $this->getCreditosSuficientes($no_ctrl);
		//Bandera si puede tomar servicio social si se la activo el Admin del modulo de servicio social
		$puedeTomarServicioSocialA = $this->puedeLlevarServicioSocial($no_ctrl);
		//Verificar si el alumnno esta inscrito en la institucion y/o vigente
		$estaInscritoYVigente = $this->estaInscritoYVigenteElAlumno($no_ctrl);

		if($status == true){

			$modelEDatosAlumno = $this->loadModel($no_ctrl);
			$carrera = $this->getCarreras($modelEDatosAlumno->cveEspecialidadAlu);
		}else{

			$modelEDatosAlumno = new EDatosAlumno;
			$carrera = array();
		}

		//Fecha formato del preregistro
		$fec_preregistro = GetFormatoFecha::getFechaPreRegistro($no_ctrl);

		if(isset($_POST['EDatosAlumno']))
		{
			$modelEDatosAlumno->attributes=$_POST['EDatosAlumno'];

			if($modelEDatosAlumno->nctrAlumno != null AND $modelEDatosAlumno->cveEspecialidadAlu AND $modelEDatosAlumno->nmbAlumno != null AND $modelEDatosAlumno->semAlumno != null)
			{
				if($this->altaServicioSocial($modelEDatosAlumno->nctrAlumno))
				{
					//Agrega registro historico del alumno
					$this->activarHorasTotalesAlumnoServicioSocial($modelEDatosAlumno->nctrAlumno);

					Yii::app()->user->setFlash('success', 'Datos registrados correctamente!!! has sido dado de ALTA para llevar el Servicio Social.');
					$this->redirect(array('/'));

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al dar de Alta tu Servicio Social.');
				}

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! tus datos registrados no estan completos.');
			}
		}

		$this->render('preRegistroAlumnoServicioSocial',array(
					  'modelEDatosAlumno'=>$modelEDatosAlumno,
					  'no_control' => $no_control,
					  'carrera' => $carrera,
					  'status' => $status,
					  'puedeTomarServicioSocialC' => $puedeTomarServicioSocialC, //Por Creditos
					  'puedeTomarServicioSocialA' => $puedeTomarServicioSocialA,
					  'estaInscritoYVigente' => $estaInscritoYVigente, //Esta inscrito y viegnte el alumno
					  'fec_preregistro' => $fec_preregistro
		));
	}

	/*Si en la tabla ss_status_servicio_social el campo Val_serv_social aparece con true quiere decir que se activo el Servicio Social y solo debe hacer el pre-registro */
	public function puedeLlevarServicioSocial($no_ctrl)
	{
		$bandera = true;
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($no_ctrl);

		if($modelSSStatusServicioSocial === NULL)
		{
			$bandera = false;
			return $bandera;
			if($modelSSStatusServicioSocial->val_serv_social == false){
				$bandera = false;
			}
		}

		return $bandera;
	}

	/*Valida que pueda tomar servicio social el alumno por los creditos acumulados y los requeridos*/
	public function getCreditosSuficientes($no_ctrl)
	{
		$bandera;
		//Sacamos el numero de creditos que debe tener el estudiante para realizar su servicio social
		$creditosMinimos = $this->getPorcentajeCreditos();//145 creditos de 260

		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($no_ctrl);

		if($modelEDatosAlumno === NULL)
			throw new CHttpException(404,'Error!!! no existen datos de ese Alumno.');

		$bandera = ($modelEDatosAlumno->crdAcumulaAlu >= $creditosMinimos) ? true : false;

		return $bandera;

	}

	public function estaInscritoYVigenteElAlumno($no_ctrl)
	{
		$bandera;
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($no_ctrl);

		if($modelEDatosAlumno === NULL)
			throw new CHttpException(404,'Error!!! no existen datos de ese Alumno.');

		$bandera = ($modelEDatosAlumno->statAlumno == "VI" && $modelEDatosAlumno->insAlumno == "S") ? true : false;

		return $bandera;
	}

	public function getPorcentajeCreditos()
	{
		//Calcular porcentaje de creditos para poder realizar servicio social
		//El numero total de creditos por carrera se fijo en 260 (valor real)

		//devuelve el modelo cuyo id es igual a 1 si existe y null si no existe
		$datosConfiguracion = SsConfiguracion::model()->find('id_configuracion = 1');
		$porcCreditos=$datosConfiguracion->porcentaje_creditos_req_servicio_social;

		$cred_minim = (($porcCreditos * 260) / 100);

		return (int)$cred_minim;
	}

	public function getEstatusServicioSocial($no_ctrl)
	{
		$bandera = true;
		$modelEDatosAlumno = $this->loadModel($no_ctrl);
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($modelEDatosAlumno->nctrAlumno);

		if($modelSSStatusServicioSocial === null)
			$bandera = false;
		else
			$bandera = ($modelSSStatusServicioSocial->val_serv_social == 1) ? true : false;

		return $bandera;
	}

	//Aqui se da de ALTA el Servicio Social del Alumno
	public function altaServicioSocial($no_ctrl)
	{
		$bandera = false;
		$periodo = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
		$anio = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk(trim($no_ctrl));

		//Si no existe el Registro entonces se inserta el nuevo registro
		if($modelSSStatusServicioSocial === null)
		{
			//Si insertar los datos del nuevo registro
			$SSStatusServicioSocial = new SsStatusServicioSocial;
			$SSStatusServicioSocial->no_ctrl = $no_ctrl;
			$SSStatusServicioSocial->val_serv_social = true;
			$SSStatusServicioSocial->fecha_preregistro = date('Y-m-d H:i:s');
			$SSStatusServicioSocial->periodo = $periodo[0]['periodoactual'];
			$SSStatusServicioSocial->anio = $anio[0]['periodoactual'];

			//Si no hubo problemas devuelve true
			if($SSStatusServicioSocial->save()){ $bandera = true; }

		}else{

			//Si el servicio social del alumno esta desactivado entonces su activa para que pueda realizarlo
			if($modelSSStatusServicioSocial->val_serv_social == false or $modelSSStatusServicioSocial->val_serv_social == null)
			{
				$modelSSStatusServicioSocial->val_serv_social = true;
				$modelSSStatusServicioSocial->fecha_preregistro = date('Y-m-d H:i:s');
				$modelSSStatusServicioSocial->periodo = $periodo[0]['periodoactual'];
				$modelSSStatusServicioSocial->anio = $anio[0]['periodoactual'];

				//Si no hubo problemas devuelve true
				if($modelSSStatusServicioSocial->save()){ $bandera = true; }
			}

		}

		return $bandera;
	}
	//Aqui se da de ALTA el Servicio Social del Alumno

	/*Si es la primera vez que activa el servicio social se le genera un registro al
	alumno con las horas por default (0), se hace aqui porque puede ser que el alumno
	tenga horas de servicio social y en dicho periodo no quiera tomar servicio social,
	en la parte de Horas Totales Alumnos Servicio Social unicamente el Administrador
	podra ACTUALIZAR las horas que lleva el alumno de Servicio Social*/
	public function activarHorasTotalesAlumnoServicioSocial($nctrAlumno)
	{
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($nctrAlumno);

		if($modelSSHistoricoTotalesAlumnos === NULL)
		{
			$HistoricoTotalesAlumnos = new SsHistoricoTotalesAlumnos;
			$HistoricoTotalesAlumnos->no_ctrl = $this->getCadena($nctrAlumno);
			$HistoricoTotalesAlumnos->horas_totales_servicio_social = 0;
			$HistoricoTotalesAlumnos->fecha_modificacion_horas_totales = date('Y-m-d H:i:s');
			$HistoricoTotalesAlumnos->calificacion_final_servicio_social = 0;
			$HistoricoTotalesAlumnos->completo_servicio_social = false; //Cuando se cumplen las 480 horas se valida que completo su Servicio Social
			$HistoricoTotalesAlumnos->firma_digital_alumno = null;
			$HistoricoTotalesAlumnos->calificacion_kardex = false; //Escolares la validara para que pase la calificacion de servicio social al kardex
			if(!$HistoricoTotalesAlumnos->save())
				throw new CHttpException(404,'Ocurrio un error al crear el registro en la tabla Horas Totales Alumnos Servicio Social.');

		}

	}

	public function actionAltaAlumno()
	{
		//$this->checkAccess('vinculacion_oficina_servicio_social');
		$modelEDatosAlumno = new EDatosAlumno;

		if(isset($_POST['EDatosAlumno']))
		{
			$modelEDatosAlumno->attributes = $_POST['EDatosAlumno'];
			$modelEDatosAlumno = EDatosAlumno::model()->findByPk($modelEDatosAlumno->nctrAlumno);

			if($modelEDatosAlumno != NULL)
			{
				if($modelEDatosAlumno->insAlumno != 'N')
				{
					if(!$this->yaEstaRegistrado($modelEDatosAlumno->nctrAlumno))
					{
						if($this->cumpleCreditos($modelEDatosAlumno->nctrAlumno))
						{
							$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($modelEDatosAlumno->nctrAlumno);

							if($modelSSStatusServicioSocial === null)
							{
								$periodo = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
								$anio = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual

								$modelSSStatusServicioSocial = new SsStatusServicioSocial;
								$modelSSStatusServicioSocial->no_ctrl = $modelEDatosAlumno->nctrAlumno;
								$modelSSStatusServicioSocial->val_serv_social = true;
								$modelSSStatusServicioSocial->fecha_preregistro = date('Y-m-d H:i:s');
								$modelSSStatusServicioSocial->periodo = $periodo[0]['periodoactual'];
								$modelSSStatusServicioSocial->anio = $anio[0]['periodoactual'];

								if($modelSSStatusServicioSocial->save())
								{
									//Se crea el registro del alumno para las horas totales de servicio social, solo la primera vez que se da de ALTA el Servicio Social
									$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk(trim($modelEDatosAlumno->nctrAlumno));
									if($modelSSHistoricoTotalesAlumnos === null)
										$this->activarHorasTotalesAlumnoServicioSocial($modelEDatosAlumno->nctrAlumno);

									Yii::app()->user->setFlash('success', 'Datos registrados correctamente!!! has sido dado de ALTA para llevar el Servicio Social.');
									$this->redirect(array('/'));
								}else{
									//echo CJSON::encode( $modelSSStatusServicioSocial->getErrors());
									Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al querer dar de Alta el Servicio Social.');
								}
							}else{
								Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno ya realizó el Preregistro de Servicio Social.');
							}
						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno NO CUMPLE con los creditos suficientes para llevar el Servicio Social.');
							//die('Error!!! Este Alumno NO CUMPLE con los creditos sificientes para llevar el Servicio Social.');
						}

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno ya tiene dado de ALTA el Servicio Social.');
						//die('Error!!! Este Alumno ya tiene dado de ALTA el Servicio Social.');
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno no esta inscrito en la Institución');
					//die('Error!!! Este Alumno no esta inscrito en la Institución');
				}
			}else{

				$modelEDatosAlumno = new EDatosAlumno;
				//die('Error!!! No se encontraron datos con ese No. de Control.');
				Yii::app()->user->setFlash('danger', 'Error!!! No se encontraron datos con ese No. de Control.');
			}

		}

		$this->render('altaAlumno',array(
					  'modelEDatosAlumno'=>$modelEDatosAlumno,
					  //'lista_carreras' => $lista_carreras
		));
	}

	public function yaEstaRegistrado($no_ctrl)
	{

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK(trim($no_ctrl));

		return ($modelSSHistoricoTotalesAlumnos != NULL) ? true : false;
	}

	//Verificamos que el Alumno cumpla con minimo 135 creditos para llevar servicio social
	public function cumpleCreditos($no_ctrl)
	{
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk(trim($no_ctrl));

		return ($modelEDatosAlumno->crdAcumulaAlu >= 135) ? true : false;
	}

	public function actionInfoAlumno()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$nocontrol = Yii::app()->request->getParam('nctrAlumno');

			//Si no vienen datos por POST
			if($nocontrol != NULL)
			{
				$alumnos = EDatosAlumno::model()->findByPk($nocontrol);

				if($alumnos === NULL)
				{
					$carrera = "";
					$inscrito = "";
					$status = "";
					$semestre = "";
					$creditos_acum = "";
					$sexo = "";
					$nombre_completo = "";
					$no_c = "*";
					$msg_err = 'sad_512.png';
					$val_creditos = "";

				}else{

					$carreras = EEspecialidad::model()->find(" \"cveEspecialidad\"=?", array($alumnos->cveEspecialidadAlu));

					$carrera = $carreras->dscEspecialidad;
					$inscrito = $alumnos->insAlumno;
					$status = $alumnos->statAlumno;
					$semestre = $alumnos->semAlumno;
					$creditos_acum = $alumnos->crdAcumulaAlu;
					$sexo = $alumnos->sexoAlumno;
					$nombre_completo = $alumnos->nmbAlumno;
					$ape_paterno = $alumnos->apellPaternoAlu;
					$ape_materno = $alumnos->apellMaternoAlu;
					$no_c = $alumnos->nctrAlumno;
					$msg_err = "";
					$val_creditos = ($creditos_acum >= 135) ? true : false;
				}

			}else{

				$carrera = "";
				$inscrito = "";
				$status = "";
				$semestre = "";
				$ape_paterno = "";
				$ape_materno = "";
				$creditos_acum = "";
				$sexo = "";
				$nombre_completo = "";
				$no_c = "+";
				$msg_err = "";
				$val_creditos = "";
			}

			//datos enviados al form por AJAX
			echo CJSON::encode(array(
				'carrera' => $carrera,
				'inscrito' => $inscrito,
				'status' => $status,
				'semestre' => $semestre,
				'ape_paterno' => $ape_paterno,
				'ape_materno' => $ape_materno,
				'creditos_acum' => $creditos_acum,
				'sexo' => $sexo,
				'nombre_completo' => $nombre_completo,
				'no_c' => $no_c,
				'msg_err' => $msg_err,
				'val_creditos' => $val_creditos
			));

			Yii::app()->end();
		}
	}

	public function loadModel($id)
	{
		$model = EDatosAlumno::model()->findByPk($id);

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='edatos-alumno-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*************************************GETTERS AND SETTERS **************************************/
	public function getCarreras()
	{
		$modelCarreras = EEspecialidad::model()->findAll();
		$lista_carreras = CHtml::listData($modelCarreras, 'cveEspecialidad', 'dscEspecialidad');

		return $lista_carreras;
	}

	public function actionCarreraAlumno()
	{

		if (Yii::app()->request->isAjaxRequest)
		{
			$id = Yii::app()->request->getParam('nctrAlumno');
			//$alumnos = $this->loadModel($_POST['nctrAlumno']);

			//Si no vienen datos por POST
			if($id == null)
			{
				$carrera = "<option value=''>-- Selecciona tu Carrera --</option>";
				$nombrecompleto = "";
				$semestre = "";

			}else{

				$alumnos = $this->loadModel($id);
				$nombrecompleto = $alumnos->nmbAlumno;
				$semestre = $alumnos->semAlumno;
				//$carrera = $alumnos->cveEspecialidadAlu;

				$carreras = EEspecialidad::model()->findAll(" \"cveEspecialidad\"=?", array($alumnos->cveEspecialidadAlu));
				$lista_carreras = CHtml::listData($carreras, 'cveEspecialidad', 'dscEspecialidad');

				$carrera="";
				foreach($lista_carreras as $value=>$dscEspecialidad)
					$carrera .= CHtml::tag('option', array('value'=>$value), CHtml::encode($dscEspecialidad), true);

			}

			echo CJSON::encode(array(
				'carrera' => $carrera,
				'nombrecompleto' => $nombrecompleto,
				'semestre' => $semestre
			));

			Yii::app()->end();
		}

	}
	/*************************************GETTERS AND SETTERS **************************************/
}
