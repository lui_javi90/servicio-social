<?php

class SsCriteriosAEvaluarController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'menuCriteriosReporteBimestral', //Menu de los criterios crear, valorar y ordenar
								 'listaCriteriosReporteBimestral',
								 'nuevoCriterioReporteBimestral', //en pruebas
								 'editarCriterioReporteBimestral',
								 'eliminarCriterioReporteBimestral', // en pruebas
								 'cambiarFormatoCriteriosReporteBimestral', //Cambiar el orden, valores y criterios de los Reportes Bimestrales
								 'asignarValoresCriteriosReporteBimestral', //Asignar valores a los criterios para el reporte bimestral
								 'guardarValoresCriterios', //Guardar valores asignados a los criterios
								 'establecerValoresDefault', //Establecer los valores default a cero (Solo criterios Activos)
								 'vistaPreviaReporteBimestral', //Vista previa del reporte bimestral
								 'ordenarCriteriosReporteBimestral', //Vista general para el orden en el que aparecen los criterios en el Reporte Bimestral
								 'asigarPosicionCriterioSupervisor', //Vista para asignar el Orden de los criterios del Supervisor
								 'asigarPosicionCriterioAdmin', //Vista para asignar el Orden de los criterios del Admin
								 'establecerPosicionesDefault', //Establecer las posiciones por default
								 'asignarPosicionesDefault', //Asigna las posiciones en forma arbitaria
								 'quitarPosicionActualCriterio' //Quitar la posicion actual al Criterio (Solo tiene asignada una posicion)
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
				),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
		);
	}

	public function actionListaCriteriosReporteBimestral()
	{
		$modelSSCriteriosAEvaluar = new SsCriteriosAEvaluar_('searchCriteriosActivados');
		$modelSSCriteriosAEvaluar->unsetAttributes();  // clear any default values

		//Total del valor (A) de los Criterios ACTIVOS
		$valorATotalCriterios = $this->getValorATotalCriterios();

		//Saber si hay un servicio social activado para no permitir agregar mas criterios
		$hay_servicio_social_activo = $this->hayServicioSocialActivo();

		if(isset($_GET['SsCriteriosAEvaluar']))
		{
			$modelSSCriteriosAEvaluar->attributes=$_GET['SsCriteriosAEvaluar'];
		}

		$this->render('listaCriteriosReporteBimestral',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'valorATotalCriterios' => $valorATotalCriterios,
					  'hay_servicio_social_activo' => $hay_servicio_social_activo
		));
	}

	public function hayServicioSocialActivo()
	{
		$query = "select * from pe_planeacion.ss_registro_fechas_servicio_social where ssocial_actual = true";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return ($result != NULL) ? true : false;
	}

	/*Aun en construccion */
	public function actionNuevoCriterioReporteBimestral()
	{
		$modelSSCriteriosAEvaluar = new SsCriteriosAEvaluar;

		if(isset($_POST['SsCriteriosAEvaluar']))
		{
			$modelSSCriteriosAEvaluar->attributes=$_POST['SsCriteriosAEvaluar'];
			//Cuando se crea el criterio se deja en 0 para manejar el valor que tendra el nuevo criterio.
			$modelSSCriteriosAEvaluar->valor_a = 0;
			$modelSSCriteriosAEvaluar->posicion_criterio = null;
			//$modelSSCriteriosAEvaluar->status_criterio = ($modelSSCriteriosAEvaluar->status_criterio == null) ? false : true;
			if($this->getValorATotalCriterios() >= 100)
				$modelSSCriteriosAEvaluar->status_criterio = false;
			
			if($modelSSCriteriosAEvaluar->status_criterio == null AND $this->getValorATotalCriterios() < 100)
				$modelSSCriteriosAEvaluar->status_criterio = true;

			if($modelSSCriteriosAEvaluar->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaCriteriosReporteBimestral'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevoCriterioReporteBimestral',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
		));
	}

	public function actionEditarCriterioReporteBimestral($id_criterio)
	{
		$modelSSCriteriosAEvaluar=$this->loadModel($id_criterio);

		//Lista de los Criterios totales
		$modelSSCriterios = new SsCriteriosAEvaluar;

		if(isset($_POST['SsCriteriosAEvaluar']))
		{
			$modelSSCriteriosAEvaluar->attributes=$_POST['SsCriteriosAEvaluar'];
			if($modelSSCriteriosAEvaluar->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaCriteriosReporteBimestral'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevoCriterioReporteBimestral',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'modelSSCriterios' => $modelSSCriterios
		));
	}

	public function actionCambiarFormatoCriteriosReporteBimestral()
	{
		$modelSSCriteriosAEvaluar =new SsCriteriosAEvaluar_('search');
		$modelSSCriteriosAEvaluar->unsetAttributes();  // clear any default values

		if(isset($_GET['SsCriteriosAEvaluar']))
		{
			$modelSSCriteriosAEvaluar->attributes=$_GET['SsCriteriosAEvaluar'];
		}

		$this->render('cambiarFormatoCriteriosReporteBimestral',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
		));
	}

	/*Muestra el menu de lo referente a los criterios de los Reportes Bimestrales */
	public function actionMenuCriteriosReporteBimestral()
	{
		$this->render('menuCriteriosReporteBimestral',array(
					  //'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
		));
	}
	/*Muestra el menu de lo referente a los criterios de los Reportes Bimestrales */

	public function actionAsignarValoresCriteriosReporteBimestral()
	{
		$modelSSCriteriosAEvaluar = new SsCriteriosAEvaluar_('search');
		$modelSSCriteriosAEvaluar->unsetAttributes();  // clear any default values

		//Porcentaje a mostrar en el Progress Bar 
		$valorATotalCriterios = $this->getValorATotalCriterios();
		//Saber si hay un servicio social activado para no permitir agregar mas criterios
		$hay_servicio_social_activo = $this->hayServicioSocialActivo();

		if(isset($_GET['SsCriteriosAEvaluar']))
		{
			$modelSSCriteriosAEvaluar->attributes=$_GET['SsCriteriosAEvaluar'];
		}

		$this->render('asignarValoresCriteriosReporteBimestral',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'valorATotalCriterios' => $valorATotalCriterios,
					  'hay_servicio_social_activo' => $hay_servicio_social_activo
		));
	}

	/*Sumatoria del valor A asigando a cada criterio activado */
	public function getValorATotalCriterios()
	{
		$query = "select sum(valor_a) as \"totalA\" from pe_planeacion.ss_criterios_a_evaluar where status_criterio = true";

		$totalA = Yii::app()->db->createCommand($query)->queryAll();

		return ($totalA[0]['totalA'] === NULL) ? 0 : $totalA[0]['totalA'];
	}
	/*Sumatoria del valor A asigando a cada criterio activado */

	public function actionGuardarValoresCriterios($id_criterio)
	{
		
		$modelSSCriteriosAEvaluar = $this->loadModel($id_criterio);
		$valorA = $this->getValoresA();

		if(isset($_POST['SsCriteriosAEvaluar']))
        {
			$modelSSCriteriosAEvaluar->attributes=$_POST['SsCriteriosAEvaluar'];
			$modelSSCriteriosAEvaluar->valor_a = $this->validarTotalValorANoPaseDe100($modelSSCriteriosAEvaluar->valor_a); // Validemos que el total de valor A no pase de 100

			if($modelSSCriteriosAEvaluar->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('asignarValoresCriteriosReporteBimestral')); //Vista a redireccionar

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
        }

        $this->render('guardarValoresCriterios',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'valorA' => $valorA
        ));
	}

	/*Validar que el valor total de A no pase de 100 */
	public function validarTotalValorANoPaseDe100($valor_A)
	{
		$valorTotalA = $this->getValorATotalCriterios();
		$valorSuma = $valorTotalA + $valor_A; //Sumatoria del total de A mas el nuevo valor agregado

		return ( $valorSuma > 100 ) ? ($valor_A - ($valorSuma - 100)) : $valor_A;
	}

	/*Valores que se mostraran en el dropDownList para asignar a los criterios */
	public function getValoresA()
	{
		$lista = array();

		for($i = 0; $i <= 20 ; $i++)
		{
			$lista[$i] = $i;
		}

		return $lista;
	}
	/*Valores que se mostraran en el dropDownList para asignar a los criterios */

	/*Establecemos los criterios ACTIVOS a cero, por default */
	public function actionEstablecerValoresDefault()
	{
		$query = "Update pe_planeacion.ss_criterios_a_evaluar set valor_a = 0 where status_criterio = true";

		if(Yii::app()->db->createCommand($query)->queryAll())
		{
			Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
			$this->redirect(array('asignarValoresCriteriosReporteBimestral')); //Vista a redireccionar

		}else{

			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
		}
	}
	/*Establecemos los criterios ACTIVOS a cero, por default */

	/*Unicamente funciona como Vista Previa del Reporte Bimestral con los criterios */
	public function actionVistaPreviaReporteBimestral()
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		/*Cargar los Criterios tanto del Supervisor como del Admin de los Reporte Bimestrales */
		//Obtenemos los criterios a evaluar por el supervisor del programa y que esten en status = true
		$query_crierios_sup =
		"
		select * from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 1 AND status_criterio = true
		order by posicion_criterio ASC
		";

		$lista_criterios_sup = Yii::app()->db->createCommand($query_crierios_sup)->queryAll();

		//Obtenemos los criterios a evaluar por gestion tecnologica y vinculacion y que esten en status = true
		$query_crierios_vinc = 
		"
        select * from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 2 and status_criterio = true
		order by posicion_criterio ASC;
		";
		//Ejecutamos la consulta criterios para reporte bimestral
		$lista_criterios_vinc = Yii::app()->db->createCommand($query_crierios_vinc)->queryAll();

		//Datos banners
		$banners = $this->getDatosBanners();

		//Sumatoria de los Valores A de los Criterios
		$totalA = $this->getValorATotalCriterios();
		/*Cargar los Criterios tanto del Supervisor como del Admin de los Reporte Bimestrales */

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 10, 10, 5, 5, 5, 5);
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad = 1
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Vista Prevía Reporte Bimestral');
		# render (full page)
    	$mPDF1->WriteHTML(
		$this->render('vistaPreviaReporteBimestral', array(
					  'lista_criterios_sup' => $lista_criterios_sup,
					  'lista_criterios_vinc' => $lista_criterios_vinc,
					  'banners' => $banners,
					  'totalA' => $totalA //Sumatoria Total de los valores A de los Criterios
					), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Vista Prevía Reporte Bimestral.pdf', 'I');
	}
	/*Unicamente funciona como Vista Previa del Reporte Bimestral con los criterios */

	//La institucion local debe ser la primera empresa en registrar, debe tener el id 1
	public function getDatosBanners()
	{
		$modelEmpresas = SsUnidadesReceptoras::model()->findByPk(1);

		if($modelEmpresas === NULL)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelEmpresas;
	}

	public function actionOrdenarCriteriosReporteBimestral()
	{
		$modelSSCriteriosAEvaluar = new SsCriteriosAEvaluar_('search');
		$modelSSCriteriosAEvaluar->unsetAttributes();  // clear any default values

		//Porcentaje a mostrar en el Progress Bar 
		$valorATotalCriterios = $this->getValorATotalCriterios();
		$posSup = (sizeof($this->getNoPosicionesSupervisor()) == 0) ? true : false; //Si devuelve true estonces todas las posiciones han sido asignadas
		$posAdm = (sizeof($this->getNoPosicionesAdmin()) == 0) ? true : false; //Si devuelve true estonces todas las posiciones han sido asignadas

		if(isset($_GET['SsCriteriosAEvaluar']))
		{
			$modelSSCriteriosAEvaluar->attributes=$_GET['SsCriteriosAEvaluar'];
		}

		$this->render('ordenarCriteriosReporteBimestral',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'valorATotalCriterios' => $valorATotalCriterios, //Para Progress Bar
					  'posSup' => $posSup,
					  'posAdm' => $posAdm
		));
	}

	public function actionAsigarPosicionCriterioSupervisor($id_criterio)
	{
		$modelSSCriteriosAEvaluar = $this->loadModel($id_criterio);
		$posicionesSupervisor = $this->getNoPosicionesSupervisor();

		if(isset($_POST['SsCriteriosAEvaluar']))
        {
			$modelSSCriteriosAEvaluar->attributes=$_POST['SsCriteriosAEvaluar'];
			
			if($modelSSCriteriosAEvaluar->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('ordenarCriteriosReporteBimestral'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
			}
        }

        $this->render('asignarPosicionCriterio',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'posicionesSupervisor' => $posicionesSupervisor
        ));
	}

	public function getNoPosicionesSupervisor()
	{
		$lista = array();
		$lista2 = array();

		/*Criterios Posiciones Totales */
		$query = 
		"select count(id_criterio) as \"totalCriteriosSup\" from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 1 AND status_criterio = true
		";
		$totalCriteriosSup = Yii::app()->db->createCommand($query)->queryAll();
		/*Criterios Totales */

		for($i = 1; $i <= $totalCriteriosSup[0]['totalCriteriosSup'] ; $i++)
		{
			$lista[$i] = $i;
		}

		/*Criterios Posiciones Ocupadas */
		$query2 = 
		"select posicion_criterio from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 1 AND status_criterio = true AND posicion_criterio is not null 
		order by posicion_criterio ASC
		";

		$totalCriteriosOcup = Yii::app()->db->createCommand($query2)->queryAll();
		$tamanio = sizeof($totalCriteriosOcup);

		for($j = 0; $j < $tamanio; $j++)
		{
			$lista2[$totalCriteriosOcup[$j]['posicion_criterio']] = $totalCriteriosOcup[$j]['posicion_criterio'];
		}
	
		//Devolver las posiciones que no esten ocupadas
		$resto = array_diff($lista, $lista2);
	
		return $resto;
	}

	public function actionAsigarPosicionCriterioAdmin($id_criterio)
	{
		$modelSSCriteriosAEvaluar = $this->loadModel($id_criterio);
		$posicionesAdmin = $this->getNoPosicionesAdmin();

		if(isset($_POST['SsCriteriosAEvaluar']))
        {
			$modelSSCriteriosAEvaluar->attributes=$_POST['SsCriteriosAEvaluar'];
			
			if($modelSSCriteriosAEvaluar->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('ordenarCriteriosReporteBimestral'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
			}
        }

        $this->render('asignarPosicionCriterio',array(
					  'modelSSCriteriosAEvaluar'=>$modelSSCriteriosAEvaluar,
					  'posicionesAdmin' => $posicionesAdmin
        ));
	}

	public function getNoPosicionesAdmin()
	{
		$lista = array();
		$lista2 = array();

		/*Devuelve todas las posiciones que estaran disponibles dependiendo del numero de criterios */
		$query = 
		"select count(id_criterio) as \"totalCriteriosAdm\" from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 2 AND status_criterio = true
		";

		$totalCriteriosAdm = Yii::app()->db->createCommand($query)->queryAll();
		/*Devuelve todas las posiciones que estaran disponibles dependiendo del numero de criterios */

		for($i = 1; $i <= $totalCriteriosAdm[0]['totalCriteriosAdm']; $i++)
		{
			$lista[$i] = $i;
		}

		/*Almacena las posiciones que han sido ocupadas */
		$query2 = 
		"select posicion_criterio from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 2 AND status_criterio = true AND posicion_criterio is not null
		order by posicion_criterio ASC
		";

		$totalCriteriosOcup = Yii::app()->db->createCommand($query2)->queryAll();
		$tamanio = sizeof($totalCriteriosOcup);

		for($j = 0; $j < $tamanio; $j++)
		{
			$lista2[$totalCriteriosOcup[$j]['posicion_criterio']] = $totalCriteriosOcup[$j]['posicion_criterio'];
		}

		$resto = array_diff($lista, $lista2);

		return $resto;
	}

	/*Establecer las posiciones default */
	public function actionEstablecerPosicionesDefault()
	{
		$query = "Update pe_planeacion.ss_criterios_a_evaluar set posicion_criterio = null where status_criterio = true";

		if(Yii::app()->db->createCommand($query)->queryAll())
		{
			Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
			$this->redirect(array('ordenarCriteriosReporteBimestral')); //Vista a redireccionar

		}else{

			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
		}
	}
	/*Establecer las posiciones default */

	/*Asigna lugares por id de menor a mayor para todos los criterios ACTIVOS*/
	public function actionAsignarPosicionesDefault()
	{
		$query = 
		"select count(id_criterio) from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 1 AND status_criterio = true
		";

		$totalPosCritSup = Yii::app()->db->createCommand($query)->queryAll();

		$query2 =
		"select count(id_criterio) from pe_planeacion.ss_criterios_a_evaluar
		where id_tipo_criterio = 2 AND status_criterio = true
		";

		$totalPosCritAdm = Yii::app()->db->createCommand($query2)->queryAll();

		if($this->generarPosicionesDefaultSup($totalPosCritSup) AND $this->generarPosicionesDefaultAdm($totalPosCritAdm)){

			Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
			$this->redirect(array('ordenarCriteriosReporteBimestral')); //Vista a redireccionar

		}else{
			//Si hubo algun error de insercion entonces se devuelve todas las posiciones al null
			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			$query3 = "Update pe_planeacion.ss_criterios_a_evaluar set posicion_criterio = null where status_criterio = true";
			Yii::app()->db->createCommand($query3)->queryAll();
			$this->redirect(array('ordenarCriteriosReporteBimestral')); //Vista a redireccionar
		}
	}
	/*Asigna lugares por id de menor a mayor para todos los criterios ACTIVOS*/

	public function generarPosicionesDefaultSup($totalPosCritSup)
	{
		//Verificamos que no haya ningun error
		$bandera = true;

		$query1 = 
		"select id_criterio, posicion_criterio from pe_planeacion.ss_criterios_a_evaluar 
		where id_tipo_criterio = 1 AND status_criterio = true
		order by id_criterio ASC
		";

		$lista_crit_sup = Yii::app()->db->createCommand($query1)->queryAll();

		$tamanio = $totalPosCritSup[0]['count'];
		$contador = 0;

		for($k = 0; $k < $tamanio; $k++)
		{
			//echo "k:".$k;
			$contador = $contador + 1;
			$modelSSCriteriosAEvaluar = $this->loadModel($lista_crit_sup[$k]['id_criterio']);
			$modelSSCriteriosAEvaluar->posicion_criterio = $contador;

			if(!$modelSSCriteriosAEvaluar->save())
			{
				//throw new CHttpException(404,'No existen datos de ese criterio.');
				$k = $tamanio + 1; //Finalizamos el for
				$bandera = false;
			}
		}
		
		return $bandera;
	}

	public function generarPosicionesDefaultAdm($totalPosCritAdm)
	{
		//Verificamos que no haya ningun error
		$bandera = true;

		$query1 = 
		"select id_criterio, posicion_criterio from pe_planeacion.ss_criterios_a_evaluar 
		where id_tipo_criterio = 2 AND status_criterio = true
		order by id_criterio ASC
		";

		$lista_crit_adm = Yii::app()->db->createCommand($query1)->queryAll();

		$tamanio = $totalPosCritAdm[0]['count'];
		$contador = 0;

		for($k = 0; $k < $tamanio; $k++)
		{
			//echo "k:".$k;
			$contador = $contador + 1;
			$modelSSCriteriosAEvaluar = $this->loadModel($lista_crit_adm[$k]['id_criterio']);
			$modelSSCriteriosAEvaluar->posicion_criterio = $contador;

			if(!$modelSSCriteriosAEvaluar->save())
			{
				//throw new CHttpException(404,'No existen datos de ese criterio.');
				$k = $tamanio + 1; //Finalizamos el for
				$bandera = false;
			}
		}
		
		return $bandera;
	}

	public function actionQuitarPosicionActualCriterio($id_criterio)
	{
		$modelSSCriteriosAEvaluar = $this->loadModel($id_criterio);

		$modelSSCriteriosAEvaluar->posicion_criterio = null;

		if($modelSSCriteriosAEvaluar->save())
		{
			
			echo CJSON::encode( [ 'code' => 200 ] );

		}else{
			echo CJSON::encode( $modelSSCriteriosAEvaluar->getErrors() );
		}
	}

	public function loadModel($id)
	{
		$modelSSCriteriosAEvaluar=SsCriteriosAEvaluar::model()->findByPk($id);
		
		if($modelSSCriteriosAEvaluar===null)
		{
			throw new CHttpException(404,'No existen datos de ese criterio.');
		}
			
		return $modelSSCriteriosAEvaluar;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-criterios-aevaluar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
