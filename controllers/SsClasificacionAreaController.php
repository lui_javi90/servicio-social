<?php

class SsClasificacionAreaController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaClasificacionAreaServicioSocial',
								 'nuevaAreaClasificacionServicioSocial',
								 'editarAreaClasificacionServicioSocial',
								 'eliminarAreaClasificacionServicioSocial',
								 'vistaPreviaSolicitudServicioSocial'
								 ),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users'=>array('@'),
				),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
		);
	}

	public function actionListaClasificacionAreaServicioSocial()
	{
		$modelSSClasificacionArea = new SsClasificacionArea_('search');
		$modelSSClasificacionArea->unsetAttributes();  // clear any default values

		$status_reg_servicio_social = $this->getRegistroFechasServicioSocial();

		if(isset($_GET['SsClasificacionArea']))
		{
			$modelSSClasificacionArea->attributes=$_GET['SsClasificacionArea'];
		}
			
		$this->render('listaClasificacionAreaServicioSocial',array(
					  'modelSSClasificacionArea'=>$modelSSClasificacionArea,
					  'status_reg_servicio_social' => $status_reg_servicio_social
		));
	}

	/*Para saber el registro de Servicio Social ACTUAL */
	public function getRegistroFechasServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'ssocial_actual = true';
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->find($criteria);

		return ($modelSSRegistroFechasServicioSocial === NULL ) ? false : true;
	}
	/*Para saber el registro de Servicio Social ACTUAL */

	public function actionNuevaAreaClasificacionServicioSocial()
	{
		$modelSSClasificacionArea = new SsClasificacionArea;

		if(isset($_POST['SsClasificacionArea']))
		{
			$modelSSClasificacionArea->attributes=$_POST['SsClasificacionArea'];
			$modelSSClasificacionArea->status_area = ($modelSSClasificacionArea->status_area == null) ? false : true;

			if($modelSSClasificacionArea->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaClasificacionAreaServicioSocial'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
				
		}

		$this->render('nuevaAreaClasificacionServicioSocial',array(
					  'modelSSClasificacionArea'=>$modelSSClasificacionArea,
		));
	}

	public function actionEditarAreaClasificacionServicioSocial($id_clasificacion_area_servicio_social)
	{
		
		$modelSSClasificacionArea=$this->loadModel($id_clasificacion_area_servicio_social);

		if(isset($_POST['SsClasificacionArea']))
		{
			$modelSSClasificacionArea->attributes=$_POST['SsClasificacionArea'];
			if($modelSSClasificacionArea->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaClasificacionAreaServicioSocial'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevaAreaClasificacionServicioSocial',array(
					  'modelSSClasificacionArea'=>$modelSSClasificacionArea,
		));
	}

	public function actionEliminarAreaClasificacionServicioSocial($id_clasificacion_area)
	{
	    //ARMAMOS LA CONSULTA
		$query = "	SELECT * FROM programas_servicio_social WHERE id_clasificacion_area_servicio_social = '$id_clasificacion_area' ";

		//EJECUTAMOS LA CONSULTA
		$registros = Yii::app()->db->createCommand($query)->queryAll();

		//VALIDAMOS QUE EL REGISTRO NO ESTE SIENDO USADO EN OTRA TABLA
		if($registros != NULL)
		{
			if($this->loadModel($id_clasificacion_area)->delete())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaClasificacionAreaServicioSocial'));

			}else{

				throw new CHttpException(404,'Error!!! no se encontraron datos de ese registro.');
			}

		}else{
			Yii::app()->user->setFlash('danger', 'Error!!! No se puede borrar el registro porque esta siendo usado en otra tabla.');
			$this->redirect(array('listaClasificacionAreaServicioSocial'));
		}
	}

	public function actionVistaPreviaSolicitudServicioSocial()
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		//Nos traemos las areas que esten ACTIVADAS
		$query = "select * from pe_planeacion.ss_clasificacion_area where status_area = true order by id_clasificacion_area_servicio_social ASC";

		$lista_areas_activas = Yii::app()->db->createCommand($query)->queryAll();

		//Datos banners
		$banners = $this->getDatosBanners();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 10, 10, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '10',
			'margin_right' =>  '10',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = " Select codigo_calidad, revision from pe_planeacion.ss_codigos_calidad where id_codigo_calidad=3";
		
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Vista Prevía Solicitud Servicio Social');
		# render (full page)
        $mPDF1->WriteHTML(
			$this->render('vistaPreviaSolicitudServicioSocial', array(
						  'lista_areas_activas' => $lista_areas_activas,
						  'banners' => $banners
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Vista Prevía Solicitud Servicio Social.pdf', 'I');
	}

	//La institucion local debe ser la primera empresa en registrar, debe tener el id 1
	public function getDatosBanners()
	{
		$modelEmpresas = SsUnidadesReceptoras::model()->findByPk(1);

		if($modelEmpresas === NULL)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelEmpresas;
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function loadModel($id)
	{
		$modelSSClasificacionArea = SsClasificacionArea::model()->findByPk($id);
		if($modelSSClasificacionArea===null)
		{
			throw new CHttpException(404,'No existe datos de esa clasificación.');
		}
			
		return $modelSSClasificacionArea;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-clasificacion-area-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
