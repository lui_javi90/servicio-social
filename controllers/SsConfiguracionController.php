<?php

class SsConfiguracionController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaConfiguracion',
								 'editarConfiguracion'
								 ),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users' => array('@'),
				),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionEditarConfiguracion($id_configuracion)
	{
		//require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		//$id_configuracion = base64_decode($id_configuracion);
		$modelSSConfiguracion = $this->loadModel($id_configuracion);

		if(isset($_POST['SsConfiguracion']))
		{
			$modelSSConfiguracion->attributes=$_POST['SsConfiguracion'];
			$modelSSConfiguracion->fecha_modificacion_configuracion = date('Y-m-d H:i:s');

			if($modelSSConfiguracion->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaConfiguracion'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('editarConfiguracion',array(
						'modelSSConfiguracion'=>$modelSSConfiguracion,
		));
	}

	public function actionListaConfiguracion()
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetDataSSConfiguracion.php';

		$modelSSConfiguracion=new SsConfiguracion_('search');
		$modelSSConfiguracion->unsetAttributes();  // clear any default values

		//Ultima Fecha de Modificacion
		$fecha_actualizacion = GetDataSSConfiguracion::getFechaLastUpdateConfiguracionMod(1);

		if(isset($_GET['SsConfiguracion']))
		{
			$modelSSConfiguracion->attributes=$_GET['SsConfiguracion'];
		}

		$this->render('listaConfiguracion',array(
									'modelSSConfiguracion'=>$modelSSConfiguracion,
									'fecha_actualizacion' => $fecha_actualizacion
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionCreate()
	{
		$model=new SsConfiguracion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsConfiguracion']))
		{
			$model->attributes=$_POST['SsConfiguracion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_configuracion));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsConfiguracion']))
		{
			$model->attributes=$_POST['SsConfiguracion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_configuracion));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsConfiguracion');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	public function actionAdmin()
	{
		$model=new SsConfiguracion_('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SsConfiguracion']))
			$model->attributes=$_GET['SsConfiguracion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function loadModel($id)
	{
		//die();
		$modelSSConfiguracion = SsConfiguracion::model()->findByPk($id);

		if($modelSSConfiguracion === null)
			throw new CHttpException(404,'El registro de configuración no se encuentra.');

		return $modelSSConfiguracion;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-configuracion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
