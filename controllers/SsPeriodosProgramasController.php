<?php

class SsPeriodosProgramasController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaPeriodosProgramas',
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social','vinculacion_supervisor_servicio_social'),
				),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionListaPeriodosProgramas()
	{
		$modelSSPeriodosProgramas = new SsPeriodosProgramas('search');
		$modelSSPeriodosProgramas->unsetAttributes();  // clear any def ault values

		if(isset($_GET['SsPeriodosProgramas']))
		{
			$modelPeriodosProgramas->attributes=$_GET['SsPeriodosProgramas'];
		}
			
		$this->render('listaPeriodosProgramas',array(
					  'modelPeriodosProgramas'=>$modelPeriodosProgramas,
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'modelSSPeriodosProgramas'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$modelSSPeriodosProgramas=new SsPeriodosProgramas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($modelSSPeriodosProgramas);

		if(isset($_POST['SsPeriodosProgramas']))
		{
			$modelSSPeriodosProgramas->attributes=$_POST['SsPeriodosProgramas'];
			if($modelSSPeriodosProgramas->save())
				$this->redirect(array('view','id'=>$modelSSPeriodosProgramas->id_periodo_programa));
		}

		$this->render('create',array(
			'modelSSPeriodosProgramas'=>$modelSSPeriodosProgramas,
		));
	}

	public function actionUpdate($id)
	{
		$modelSSPeriodosProgramas=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($modelSSPeriodosProgramas);

		if(isset($_POST['SsPeriodosProgramas']))
		{
			$modelSSPeriodosProgramas->attributes=$_POST['SsPeriodosProgramas'];
			if($modelSSPeriodosProgramas->save())
				$this->redirect(array('view','id'=>$modelSSPeriodosProgramas->id_periodo_programa));
		}

		$this->render('update',array(
			'modelSSPeriodosProgramas'=>$modelSSPeriodosProgramas,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsPeriodosProgramas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SsPeriodosProgramas the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$modelSSPeriodosProgramas=SsPeriodosProgramas::model()->findByPk($id);
		if($modelSSPeriodosProgramas===null)
		{
			throw new CHttpException(404,'No se encontraron datos del periodo.');
		}
			
		return $modelSSPeriodosProgramas;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsPeriodosProgramas $model the model to be validated
	 */
	protected function performAjaxValidation($modelSSPeriodosProgramas)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='periodos-programas-form')
		{
			echo CActiveForm::validate($modelSSPeriodosProgramas);
			Yii::app()->end();
		}
	}
}
