<?php

class SsSolicitudProgramaServicioSocialController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaSolicitudAdminProgramaServicioSocial', //Lista de solicitudes vistas por el admin
								 'detalleAdminSolicitudPrograma', //Vista Admin
								 'responderSolicitudAlumno', //Responder solicitud a programa para Servicio Social
								 'imprimirSolicitudServicioSocial', //Reporte de Solicitud de Servicio Social
								 'ImprimirSolicitudHistoricaServicioSocial',
								 'listaSolicitudesProgramasAlumnos', //Soliictudes de los alumnos en pendiente por el supervisor
								 'cancelarSolicitudAlumnoPrograma'
								 ),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users' => array('@')
			),
			array('allow',
			   'actions'=>array('listaSolicitudSupervisorProgramaServicioSocial',
								'detalleSupervisorSolicitudPrograma',
								'responderSolicitudAlumno',
								'cancelarSolicitudesPendientes',
								'ImprimirSolicitudHistoricaServicioSocial'
								),
			   //'roles'=>array('vinculacion_supervisor_servicio_social'),
			   'users' => array('@')
			),
			array('allow',
			   'actions'=>array('listaSolicitudAlumnoProgramaServicioSocial',
								'detalleAlumnoSolicitudPrograma',
								'imprimirSolicitudServicioSocial',
								'validaAlumnoClausulaSolicitudServicioSocial', //El alumno confirma si acepta las clausulas de la Solicitud de Servicio Social
								'ImprimirSolicitudHistoricaServicioSocial'
								),
			   //'roles'=>array('alumno'),
			   'users' => array('@')
			),
			array('allow',
			   'actions'=>array('imprimirSolicitudServicioSocial',
								'ImprimirSolicitudHistoricaServicioSocial'
								),
			   //'roles'=>array('jefe_departamento_escolares'),
			   'users' => array('@')
            ),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	//Para eliminar las solicitudes de los alumnos que estan en pendiente por parte del supervisor
	public function actionListaSolicitudesProgramasAlumnos()
	{
		$modelSsSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial_('searchXSolicitudesEstatuspendienteSupervisores');
        $modelSsSolicitudProgramaServicioSocial->unsetAttributes();  // clear any default values

        if(isset($_GET['SsSolicitudProgramaServicioSocial']))
		{
			$modelSsSolicitudProgramaServicioSocial->attributes = $_GET['SsSolicitudProgramaServicioSocial'];
		}

        $this->render('listaSolicitudesProgramasAlumnos',array(
            		  'modelSsSolicitudProgramaServicioSocial'=>$modelSsSolicitudProgramaServicioSocial,
        ));
	}

	public function actionCancelarSolicitudAlumnoPrograma($id_solicitud_programa, $no_ctrl)
	{
			$modelSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->findByAttributes(
																																															array(
																																																'id_solicitud_programa' => $id_solicitud_programa,
																																																'no_ctrl' => $no_ctrl
																																													));
			if($modelSolicitudProgramaServicioSocial === null)
				throw new CHttpException(404,'No existe Solicitud Pendiente de ese Alumno.');

			if ($modelSolicitudProgramaServicioSocial->save())
			{
				/*La solicitud para estatus CANCELADA */
				$modelSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 3;
				$modelSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 3;
				$modelSolicitudProgramaServicioSocial->save();//Guardamos los cambios

				echo CJSON::encode( [ 'code' => 200 ] );

			}else{
				echo CJSON::encode( $modelSolicitudProgramaServicioSocial->getErrors() );
			}
	}

	public function actionCancelarSolicitudesPendientes($id_programa)
	{
		$query = "Update pe_planeacion.ss_solicitud_programa_servicio_social
		SET id_estado_solicitud_programa_supervisor = 3
		where (valida_solicitud_alumno is not Null and valida_solicitud_supervisor_programa is NULL and
		id_estado_solicitud_programa_supervisor = 1 and id_estado_solicitud_programa_alumno = 2) and id_programa = '$id_programa' ";

		if(Yii::app()->db->createCommand($query)->queryAll())
		{
			Yii::app()->user->setFlash('success', 'Las Solicitudes Pendientes fueron Canceladas correctamente!!!');
			$this->redirect(array('listaSolicitudSupervisorProgramaServicioSocial'));

		}else{
			Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al Cancelar las Solicitudes Pendientes.');
			$this->redirect(array('listaSolicitudSupervisorProgramaServicioSocial'));
		}
	}

	//Solicitudes vista del alumno (1 a la vez) e historial (duda)
	public function actionListaSolicitudAlumnoProgramaServicioSocial()
	{
		$modelSSSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial_('searchSolicitudesXAlumno');
		$modelSSSolicitudProgramaServicioSocial->unsetAttributes();  // clear any default values
		//Se toma del inicio de sesion del alumno
		$no_ctrl= Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		$criteria = new CDbCriteria();
		$criteria->alias = "spss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas as pss on pss.id_programa = spss.id_programa";
		$criteria->condition = "spss.no_ctrl = '$no_ctrl' AND (spss.id_estado_solicitud_programa_alumno = 1 OR spss.id_estado_solicitud_programa_alumno = 2)
		AND (spss.id_estado_solicitud_programa_supervisor = 1 OR spss.id_estado_solicitud_programa_supervisor = 2)";
		$SolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->find($criteria);

		if($SolicitudProgramaServicioSocial != null)
		{
			$modelSSProgramas = SsProgramas::model()->findByPk($SolicitudProgramaServicioSocial->id_programa);
			if($modelSSProgramas === null)
				throw new CHttpException(404,'No existen datos de ese Programa.');

		}else{
			$modelSSSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial_('searchSolicitudesXAlumno');
			$modelSSSolicitudProgramaServicioSocial->unsetAttributes();  // clear any default values
			$modelSSProgramas = new SsProgramas;
			//$id_tipo_programa = 0;
		}

		if(isset($_GET['SsSolicitudProgramaServicioSocial']))
		{
			$modelSSSolicitudProgramaServicioSocial->attributes=$_GET['SsSolicitudProgramaServicioSocial'];
		}

		$this->render('listaSolicitudAlumnoProgramaServicioSocial',array(
					  'modelSSSolicitudProgramaServicioSocial'=>$modelSSSolicitudProgramaServicioSocial,
					  'modelSSProgramas' => $modelSSProgramas,
					  'no_ctrl' => $no_ctrl,
					  'id_tipo_programa' => $modelSSProgramas->id_tipo_programa
		));
	}

	//Lista de solicitudes que se le muestran al Administrador del programa a inscribirse por parte de los alumnos
	public function actionListaSolicitudAdminProgramaServicioSocial()
	{
		$modelSSSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial_('search');
		$modelSSSolicitudProgramaServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsSolicitudProgramaServicioSocial']))
		{
			$modelSSSolicitudProgramaServicioSocial->attributes=$_GET['SsSolicitudProgramaServicioSocial'];
		}

		$this->render('listaSolicitudAdminProgramaServicioSocial',array(
					'modelSSSolicitudProgramaServicioSocial'=>$modelSSSolicitudProgramaServicioSocial,
		));
	}

	//Lista de solicitudes que se le muestran al supervisor del programa (obviamente las solicitudes del programa que supervisa)
	public function actionListaSolicitudSupervisorProgramaServicioSocial()
	{
		/*El rfc se toma del inicio de sesion del empleado a cargo de un programa */
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		$modelSSSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial_('searchSolicitudesSupervisor');
		$modelSSSolicitudProgramaServicioSocial->unsetAttributes();  // clear any default values
		$Programas = $this->getIDprogramaVigenteSupervisor($rfcSupervisor);
		//Si no hay datos devolvemos cero, si los hay entonces el id_programa
		//$id_programa = ($Programas === NULL) ? 0 : $Programas[0]['id_programa'];

		if($Programas != NULL)
		{
			/*Verificar el numero de lugares disponibles que tiene para el programa*/
			$id_programa = $Programas[0]['id_programa'];
			$id_tipo_programa = $Programas[0]['id_tipo_programa'];
			$disponibles = $this->lugaresDisponibles($rfcSupervisor, $id_programa, $id_tipo_programa);
		}else{

			$id_programa = 0;
			$id_tipo_programa = $this->isSupervisorInternoOExterno($rfcSupervisor);
			$disponibles = 0;
		}

		if(isset($_GET['SsSolicitudProgramaServicioSocial']))
		{
			$modelSSSolicitudProgramaServicioSocial->attributes=$_GET['SsSolicitudProgramaServicioSocial'];
		}

		$this->render('listaSolicitudSupervisorProgramaServicioSocial',array(
					  'modelSSSolicitudProgramaServicioSocial'=>$modelSSSolicitudProgramaServicioSocial,
					  'rfcsupervisor' => $rfcSupervisor,
					  'id_programa' => $id_programa,
					  'id_tipo_programa' => $id_tipo_programa,
					  'disponibles' => $disponibles
		));
	}

	//Saber si el supervisor es interno o externo
	public function isSupervisorInternoOExterno($rfcSupervisor)
	{
		$id_tipo_programa;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != null)
		{
			$id_tipo_programa = 1;

		}else{
			$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);
			$id_tipo_programa = ($modelSSSupervisoresProgramas != NULL) ? 2 : 0;
		}

		return $id_tipo_programa;
	}

	/*Lugares disponibles de cada programa*/
	public function lugaresDisponibles($rfcSupervisor, $id_programa, $id_tipo_programa)
	{
		if($id_tipo_programa == 1)
		{
			$query =
			"select pss.lugares_disponibles from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.id_programa = '$id_programa' AND rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$disponibles = Yii::app()->db->createCommand($query)->queryAll();

		}else
		if($id_tipo_programa == 2)
		{
			$query =
			"select pss.lugares_disponibles from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			where rpe.id_programa = '$id_programa' AND rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$disponibles = Yii::app()->db->createCommand($query)->queryAll();
		}

		return ($disponibles != NULL) ? $disponibles[0]['lugares_disponibles'] : 0;
	}
	/*Lugares disponibles de cada programa*/

	//Detalle vista solicitud del programa del Alumno
	public function actionDetalleAlumnoSolicitudPrograma($id_solicitud_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarSolicitudServicioSocial($id_solicitud_programa, $no_ctrl))
			throw new CHttpException(4044,'La informacion No esta disponible.');

		$modelSSSolicitudProgramaServicioSocial = $this->loadModel($id_solicitud_programa);

		$modelEDatosAlumno = EDatosAlumno::model()->findByPK($no_ctrl);
		if($modelEDatosAlumno === null)
			throw new CHttpException(404,'No se encontraron datos del alumno.');

		$carrera = $this->getCarrera($modelEDatosAlumno->cveEspecialidadAlu);
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($no_ctrl);
		if($modelSSStatusServicioSocial === null)
			throw new CHttpException(404,'No se encontraron datos del alta del alumno en el Servicio Social.');

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);
		if($modelSSHistoricoTotalesAlumnos === null)
			throw new CHttpException(404,'No se encontraron datos de las horas totales.');

		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_programa);
		if($modelSSProgramas ===null)
			throw new CHttpException(404,'No se encontraron datos del programa.');

		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAllByAttributes(array('id_programa'=>$modelSSSolicitudProgramaServicioSocial->id_programa));
		if($modelSSHorarioDiasHabilesProgramas === null)
			throw new CHttpException(404,'No se encontraron datos del horario.');


		if(isset($_POST['SsSolicitudProgramaServicioSocial']))
        {
            $modelSSSolicitudProgramaServicioSocial->attributes=$_POST['SsSolicitudProgramaServicioSocial'];
			if($modelSSSolicitudProgramaServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//INSERTAR REGISTRO EN LA TABLA servicio_social si se ACEPTO al alumno (2)
				if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa == 2)
				{
					$this->insertRegistroTablaServicioSocial($no_ctrl, $modelSSSolicitudProgramaServicioSocial->id_programa);
				}else
				if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa == 4){
					/*ACTUALIZAR id_estado_solicitud_programa EN LA TABLA solicitud_programa_servicio_social
					si se RECHAZO al alumno (4)*/
					$this->actualizarRegistroTablaSolicitudPrograma($id_solicitud_programa, $no_ctrl, $id_programa, 3);
				}
				$this->redirect(array('listaSolicitudProgramaServicioSocialSupervisor'));

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
        }

		$this->render('detalleAlumnoSolicitudPrograma', array(
					  'modelEDatosAlumno' => $modelEDatosAlumno,
					  'carrera' => $carrera,
					  'modelSSStatusServicioSocial' => $modelSSStatusServicioSocial,
				      'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'modelSSSolicitudProgramaServicioSocial' => $modelSSSolicitudProgramaServicioSocial,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'fecha_actualizacion' => $fecha_actualizacion = InfoSolicitudProgramas::fechaSolicitudPrograma($id_solicitud_programa),
		));
	}

	//Detalle vista solicitud del programa del Admin (Dar de alta servicio sociales externos)
	public function actionDetalleAdminSolicitudPrograma($id_solicitud_programa, $no_ctrl)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
		$modelSSSolicitudProgramaServicioSocial = $this->loadModel($id_solicitud_programa);
		$modelEDatosAlumno = EDatosAlumno::model()->findByPK($no_ctrl);
		$carrera = EEspecialidad::model()->findByPK($modelEDatosAlumno->cveEspecialidadAlu);
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);
		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_programa);
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAllByAttributes(array('id_programa'=>$modelSSSolicitudProgramaServicioSocial->id_programa));

		if($modelEDatosAlumno === null)
			throw new CHttpException(404,'No se encontraron datos del alumno.');

		if($modelSSHistoricoTotalesAlumnos === null)
			throw new CHttpException(404,'No se encontraron datos de las horas totales.');

		if($modelSSProgramas === null)
			throw new CHttpException(404,'No se encontraron datos del programa.');

		if($carrera === null)
			throw new CHttpException(404,'No se encontraron datos de esa Carrera.');

		if($modelSSHorarioDiasHabilesProgramas === null)
			throw new CHttpException(404,'No se encontraron datos del Horario del Programa.');

		$list_edo_sol = $this->getEstadosXLugaresProgramas($modelSSProgramas->id_programa);

		if(isset($_POST['SsSolicitudProgramaServicioSocial']))
    	{
      		$modelSSSolicitudProgramaServicioSocial->attributes=$_POST['SsSolicitudProgramaServicioSocial'];
			/*HACEMOS UNA VALIDACION DE QUE VENGAN LOS DATOS PORQUE SON MUY IMPORTANTES
			PARA CREAR LOS REPORTES BIMESTRALES*/
			if($modelSSProgramas->id_periodo_programa === null || $modelSSProgramas->fecha_inicio_programa === null)
				throw new CHttpException(404,'Error!!! no se guardo la Fecha de Inicio, tipo o periodo del servicio social.');

			if($modelSSSolicitudProgramaServicioSocial->save())
			{
				//INSERTAR REGISTRO EN LA TABLA servicio_social si se ACEPTO al alumno (2)
				if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 3)
				{
					//Se Cancela toda la solicitud al programa
					$this->cancelarSolicitudAlumnoAlPrograma($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);

				}elseif($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 2)
				{
					$modelSSSolicitudProgramaServicioSocial->valida_solicitud_supervisor_programa = date('Y-m-d H:i:s');
					$modelSSSolicitudProgramaServicioSocial->save();

					/*Se crea su registro de Servicio Social al alumno*/
					$this->insertRegistroTablaServicioSocial($no_ctrl, $modelSSSolicitudProgramaServicioSocial->id_solicitud_programa, $modelSSSolicitudProgramaServicioSocial->id_programa);

					/*Crear los reportes bimestrales Servicio Social Semestral y Anual*/
					if($modelSSProgramas->id_periodo_programa == 1 or $modelSSProgramas->id_periodo_programa == 2)
					{
						if(Yii::app()->db->createCommand("select pe_planeacion.agregarReportesBimestralesAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll() &&
						Yii::app()->db->createCommand("select pe_planeacion.agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
						{
							//Se agrega la fecha de validacion de la solicitud del alumnos
							$this->agregarValidacionSupervisor($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);

							/*Restar el campo lugares_disponibles en la tabla programas_servicio_social*/
							$this->restarLugarDisponibleEnPrograma($modelSSProgramas);
							/*Insertar registros para la Carta de Terminación y las Actividades del Servicio Social*/
							$modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
							if($modelSSServicioSocial ===null)
								throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

							$modelSSProgramas->fecha_fin_programa = $this->obtenerFechaFinServicioSocial($modelSSServicioSocial->id_servicio_social);
							$modelSSProgramas->save();

							//mandar observacion de bienvenida al ALUMNO
							$this->ObservacionDeBienvenidaSupervisorAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getRFCSupervisor($modelSSProgramas->id_programa));
							$this->ObservacionDeBienvenidaDeptoAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getJefeOficinaServicioSocialActual());
							//Yii::app()->user->setFlash('success', 'Registro creado correctamente!!!');

						}else{
							Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
						}

					}/*Crear los reportes bimestrales Servicio Social Unica vez EXTRATEMPORALES*/
					if($modelSSProgramas->id_periodo_programa == 3)
					{
						if(Yii::app()->db->createCommand("select pe_planeacion.agregarReportesBimestralesExtraTemporal('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa', '$modelSSProgramas->fecha_fin_programa')")->queryAll() &&
						Yii::app()->db->createCommand("select pe_planeacion.agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
						{
							/*Agregar firma de aceptado del supervisor */
							$this->agregarValidacionSupervisor($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);

							/*Una vez registrado todo, sino hubo problemas ahora asi restar el campo lugares_disponibles en la tabla programas_servicio_social*/
							$this->restarLugarDisponibleEnPrograma($modelSSProgramas);
							$modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
							if($modelSSServicioSocial ===null)
								throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

							//mandar observacion de bienvenida al ALUMNO
							$this->ObservacionDeBienvenidaSupervisorAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getRFCSupervisor($modelSSProgramas->id_programa));
							$this->ObservacionDeBienvenidaDeptoAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getJefeOficinaServicioSocialActual());

						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
						}
					}
				}

				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//Redireccionamos a la vista
				$this->redirect(array('listaSolicitudAdminProgramaServicioSocial'));

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
    	}

		$this->render('detalleAdminSolicitudPrograma', array(
					  'modelEDatosAlumno' => $modelEDatosAlumno,
					  'carrera' => $carrera->dscEspecialidad,
					  'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'modelSSSolicitudProgramaServicioSocial' => $modelSSSolicitudProgramaServicioSocial,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'no_ctrl' => $no_ctrl,
					  'fecha_actualizacion' => $fecha_actualizacion = InfoSolicitudProgramas::fechaSolicitudPrograma($id_solicitud_programa),
					  'list_edo_sol' => $list_edo_sol
		));
	}
	//Detalle vista solicitud del programa del Admin (Dar de alta servicio sociales externos)

	/*Aceptar la solicitud del alumno al programa por parte del supervisor*/
	public function actionDetalleSupervisorSolicitudPrograma($id_solicitud_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
		$modelSSSolicitudProgramaServicioSocial = $this->loadModel($id_solicitud_programa);

		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarSolicitudesSupervisor($rfcSupervisor, $id_solicitud_programa, $modelSSSolicitudProgramaServicioSocial->no_ctrl))
			throw new CHttpException(4043,'Esta Solicitud No te corresponde.');

		//Datos del Alumno, Programa y Horario del Programa
		$modelEDatosAlumno = EDatosAlumno::model()->findByPK($modelSSSolicitudProgramaServicioSocial->no_ctrl);
		if($modelEDatosAlumno === null)
		  throw new CHttpException(404,'No se encontraron datos del alumno.');

		$modelEEspecialidad = EEspecialidad::model()->findByPK($modelEDatosAlumno->cveEspecialidadAlu);
		if($modelEEspecialidad === null)
		  throw new CHttpException(404,'No se encontraron datos de la especialidad alumno.');

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($modelSSSolicitudProgramaServicioSocial->no_ctrl);
		if($modelSSHistoricoTotalesAlumnos === null)
		  throw new CHttpException(404,'No se encontraron datos de las horas totales.');

		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_programa);
		if($modelSSProgramas === null)
		  throw new CHttpException(404,'No se encontraron datos del programa.');

		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAllByAttributes(array('id_programa'=>$modelSSSolicitudProgramaServicioSocial->id_programa));
		if($modelSSHorarioDiasHabilesProgramas === null)
		  throw new CHttpException(404,'No se encontraron datos del Horario del Programa.');

		//Lista de Estados de Solicitud de Servicio Social Disponibles, si ya no hay lugares disponibles se quita la opcion de 'ACEPTADO'
		$list_edo_sol = $this->getEstadosXLugaresProgramas($modelSSProgramas->id_programa);

		if(isset($_POST['SsSolicitudProgramaServicioSocial']))
    	{
			$modelSSSolicitudProgramaServicioSocial->attributes=$_POST['SsSolicitudProgramaServicioSocial'];

			if($modelSSSolicitudProgramaServicioSocial->save())
			{
				//Si el Supervisor RECHAZO la Solicitud del Alumno no tiene caso preguntar al alumno si acepta la clausula de la Solicitud
				if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 3)
				{
					//Se Cancela toda la solicitud al programa
					$this->cancelarSolicitudAlumnoAlPrograma($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);

				}elseif ($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor == 2)
				{ //Se acepto al Alumno

					$modelSSSolicitudProgramaServicioSocial->valida_solicitud_supervisor_programa = date('Y-m-d H:i:s');
					$modelSSSolicitudProgramaServicioSocial->save();

					/*Se crea su registro de Servicio Social al alumno*/
				  	$this->insertRegistroTablaServicioSocial($modelSSSolicitudProgramaServicioSocial->no_ctrl, $modelSSSolicitudProgramaServicioSocial->id_solicitud_programa, $modelSSSolicitudProgramaServicioSocial->id_programa);

					/*Crear los reportes bimestrales Servicio Social Semestral y Anual*/
				  if($modelSSProgramas->id_periodo_programa == 1 or $modelSSProgramas->id_periodo_programa == 2)
					{
						if(Yii::app()->db->createCommand("select pe_planeacion.agregarReportesBimestralesAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll() &&
					    Yii::app()->db->createCommand("select pe_planeacion.agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
					    {
							//Se agrega la fecha de validacion de la solicitud del alumnos
							$this->agregarValidacionSupervisor($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);

							/*Una vez registrado todo, sino NO hubo problemas ahora asi restar el campo lugares_disponibles en la tabla ss_programas*/
					        $this->restarLugarDisponibleEnPrograma($modelSSProgramas);
					        $modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
					        if($modelSSServicioSocial ===null)
					        	throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

					      	$modelSSProgramas->fecha_fin_programa = $this->obtenerFechaFinServicioSocial($modelSSServicioSocial->id_servicio_social);
							$modelSSProgramas->save();

							//mandar observacion de bienvenida al ALUMNO
							$this->ObservacionDeBienvenidaSupervisorAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getRFCSupervisor($modelSSProgramas->id_programa));
							$this->ObservacionDeBienvenidaDeptoAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getJefeOficinaServicioSocialActual());
							//Yii::app()->user->setFlash('success', 'Registro creado correctamente!!!');

						}else{

						  Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');

					    }

					}elseif ($modelSSProgramas->id_periodo_programa == 3)
					{ //programas Especiales (Unica vez)

						if(Yii::app()->db->createCommand("select pe_planeacion.agregarReportesBimestralesExtraTemporal('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa', '$modelSSProgramas->fecha_fin_programa')")->queryAll() &&
						Yii::app()->db->createCommand("select pe_planeacion.agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
						{
							/*Agregar firma de aceptado del supervisor */
							$this->agregarValidacionSupervisor($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);

							/*Una vez registrado todo, sino hubo problemas ahora asi restar el campo lugares_disponibles en la tabla programas_servicio_social*/
							$this->restarLugarDisponibleEnPrograma($modelSSProgramas);
							$modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
							if($modelSSServicioSocial ===null)
								throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

							//mandar observacion de bienvenida al ALUMNO
							$this->ObservacionDeBienvenidaSupervisorAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getRFCSupervisor($modelSSProgramas->id_programa));
							$this->ObservacionDeBienvenidaDeptoAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getJefeOficinaServicioSocialActual());

						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
						}
					}
				}

				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				/*Si se acepto o rechazo al alumno nos redirecciona a la lista de solicitudes */
				$this->redirect(array('listaSolicitudSupervisorProgramaServicioSocial'));

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
    	}

		$this->render('detalleSupervisorSolicitudPrograma', array(
					  'modelSSSolicitudProgramaServicioSocial' => $modelSSSolicitudProgramaServicioSocial,
					  'modelEDatosAlumno' => $modelEDatosAlumno,
					  'modelEEspecialidad' => $modelEEspecialidad,
					  'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'fecha_actualizacion' => $fecha_actualizacion = InfoSolicitudProgramas::fechaSolicitudPrograma($id_solicitud_programa),
					  'list_edo_sol' => $list_edo_sol
		));
	}
	/*Aceptar la solicitud del alumno al programa por parte del supervisor*/

	//Mensaje de bienvenida del SUPERVISOR para el Alumno
	public function ObservacionDeBienvenidaSupervisorAlAlumno($id_programa, $id_servicio_social, $no_ctrl, $rfcSupervisor)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);

		//Observacion de Bienvenida del Supervisor
		$ObservacionesServicioSocial = new SsObservacionesServicioSocial;
		$ObservacionesServicioSocial->id_servicio_social = $id_servicio_social;
		$ObservacionesServicioSocial->observacion = "BIENVENIDO seas y mucha suerte te desea ".$this->getDatosSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa)." Supervisor del Programa.";
		$ObservacionesServicioSocial->fecha_registro = date('Y-m-d H:i:s');
		$ObservacionesServicioSocial->tipo_observacion_emisor = 2; //Quien la realiza
		$ObservacionesServicioSocial->tipo_observacion_receptor = 3; //Quien la recibe
		$ObservacionesServicioSocial->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
		$ObservacionesServicioSocial->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
		$ObservacionesServicioSocial->emisor_observ = $rfcSupervisor;
		$ObservacionesServicioSocial->receptor_observ = $no_ctrl;

		if(!$ObservacionesServicioSocial->save()){
			throw new CHttpException(404,'Ocurrio un error al guardar los datos.');
		}
	}
	//Mensaje de bienvenida del SUPERVISOR para el Alumno

	//Mensaje de bienvenida del JEFE OF SERVICIO SOCIAL para el Alumno
	public function ObservacionDeBienvenidaDeptoAlAlumno($id_programa, $id_servicio_social, $no_ctrl, $rfcJefDeptoVinc)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);

		//Observacion de Bienvenida del Supervisor
		$ObservacionesServicioSocial = new SsObservacionesServicioSocial;
		$ObservacionesServicioSocial->id_servicio_social = $id_servicio_social;
		$ObservacionesServicioSocial->observacion = "BIENVENIDO seas y mucha suerte te desea ".$this->getNombreJefeOfServicioSocial(). " Jefe de la Oficina de Servicio Social.";
		$ObservacionesServicioSocial->fecha_registro = date('Y-m-d H:i:s');
		$ObservacionesServicioSocial->tipo_observacion_emisor = 1; //Quien la realiza
		$ObservacionesServicioSocial->tipo_observacion_receptor = 3; //Quien la recibe
		$ObservacionesServicioSocial->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
		$ObservacionesServicioSocial->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
		$ObservacionesServicioSocial->emisor_observ = $rfcJefDeptoVinc;
		$ObservacionesServicioSocial->receptor_observ = $no_ctrl;

		if(!$ObservacionesServicioSocial->save()){
			throw new CHttpException(404,'Ocurrio un error al guardar los datos.');
		}
	}
	//Mensaje de bienvenida del JEFE OF SERVICIO SOCIAL para el Alumno

	/*Nos traemos el RFC del supervisor */
	/*public function getRFCSupervisor($id_programa, $id_tipo_programa)
	{
		$rfc = "";

		if($id_tipo_programa == 1)
		{
			$query = "select * from pe_planeacion.ss_responsable_programa_interno where id_programa = '$id_programa' AND superv_principal_int = true";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcEmpleado'];

		}else
		if($id_tipo_programa == 2){
			$query = "select * from pe_planeacion.ss_responsable_programa_externo where id_programa = '$id_programa' AND superv_principal_ext = true";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcSupervisor'];
		}

		return $rfc;
	}*/
	/*Nos traemos el RFC del supervisor */

	/*Si se Rechaza la solicitud del alumno*/
	public function cancelarSolicitudAlumnoAlPrograma($id_solicitud_programa)
	{
		$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->findByPK($id_solicitud_programa);
		if($modelSSSolicitudProgramaServicioSocial === NULL)
			throw new CHttpException(404,'No existen datos de esa Solicitud.');

		$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 3;
		$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 3;

		if(!$modelSSSolicitudProgramaServicioSocial->save())
		{
			Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error, no se guardaron los datos.');
		}

	}

	public function agregarValidacionSupervisor($id_solicitud_programa)
	{
		$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->findByPK($id_solicitud_programa);
		$modelSSSolicitudProgramaServicioSocial->valida_solicitud_supervisor_programa = date('Y-m-d H:i:s');

		/*Si se inserto correctamente no mandara mensaje de error */
		if(!$modelSSSolicitudProgramaServicioSocial->save()){
			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			die('Ocurrio un error al querer guardar los datos.');
		}
	}

	public function obtenerFechaFinServicioSocial($id_servicio_social)
	{
		$query = "select fecha_fin_rep_bim from pe_planeacion.ss_reportes_bimestral
		where id_servicio_social = '$id_servicio_social' and bimestre_final = true";

		$fec_fin = Yii::app()->db->createCommand($query)->queryAll();

		return $fec_fin[0]['fecha_fin_rep_bim'];
	}

	public function actionImprimirSolicitudHistoricaServicioSocial($id_solicitud_programa)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarSolicitudHistoricaServicioSocial($id_solicitud_programa, $no_ctrl))
			throw new CHttpException(4044,'La informacion No esta disponible.');

		//Consulta de Solicitud de Servicio Social
		$query = "
		select
		eda.\"nmbAlumno\" as nombre_alumno,
		ss.no_ctrl,
		(select \"cllDomAlu\" || ' #' || \"numDomAlu\" || ' Col. ' || \"colDomAlu\" from public.\"E_datosPersonalesAlumno\" where \"nctrAlumno\" = eda.\"nctrAlumno\") as direccion_alumno,
		(select \"telDomAlu\" from public.\"E_datosPersonalesAlumno\" where \"nctrAlumno\" = eda.\"nctrAlumno\")as telefono,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera_alumno,
		eda.\"semAlumno\" as semestre,
		pss.nombre_programa,
		ss.\"empresaSupervisorJefe\" as nombre_empresa,
		ss.nombre_supervisor,
		ss.cargo_supervisor,
		pss.nombre_programa,
		(select periodo_programa from pe_planeacion.ss_periodos_programas where id_periodo_programa = pss.id_periodo_programa) as periodo_programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		pss.actividades_especificas_realizar,
		pss.id_clasificacion_area_servicio_social as tipo_programa,
		pss.id_unidad_receptora as no_empresa,
		spss.fecha_solicitud_programa,
		spss.valida_solicitud_alumno,
		spss.id_estado_solicitud_programa_alumno
		from pe_planeacion.ss_solicitud_programa_servicio_social spss
		join pe_planeacion.ss_servicio_social ss
		on ss.id_servicio_social = spss.id_solicitud_programa
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where spss.id_solicitud_programa = '$id_solicitud_programa'
		";

		$datos_alumno_solicitud = Yii::app()->db->createCommand($query)->queryAll();

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Llenar la parte de areas de servicio social en el Reporte
		$query2 = "select * from pe_planeacion.ss_clasificacion_area where status_area = true order by id_clasificacion_area_servicio_social ASC";
		$lista_areas_activas = Yii::app()->db->createCommand($query2)->queryAll();

		$fecha_inicio_programa = $this->getFormatoFecha($datos_alumno_solicitud[0]['fecha_inicio_programa']);
		$fecha_fin_programa = $this->getFormatoFecha($datos_alumno_solicitud[0]['fecha_fin_programa']);
		$fecha_sol_programa = $this->getFormatoFecha($datos_alumno_solicitud[0]['fecha_solicitud_programa']);
		$fec_val_sol_alum = InfoSolicitudProgramas::fechaValidaSolicitudAlumno($id_solicitud_programa);

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', 0, '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad=3
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Solicitud Servicio Social');
		# render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirSolicitudServicioSocial', array(
						  'datos_alumno_solicitud' => $datos_alumno_solicitud,
						  'lista_areas_activas' => $lista_areas_activas,
						  'banners' => $banners,
						  'fecha_inicio_programa' => $fecha_inicio_programa,
						  'fecha_fin_programa' => $fecha_fin_programa,
						  'fecha_sol_programa' => $fecha_sol_programa,
						  'fec_val_sol_alum' => $fec_val_sol_alum
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Solicitud Servicio Social.pdf', 'I');
	}

	//Imprime la solicitud del servicio social cuando es alumno ya es aceptado al programa
	public function actionImprimirSolicitudServicioSocial($id_solicitud_programa)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarSolicitudServicioSocial($id_solicitud_programa, $no_ctrl))
			throw new CHttpException(4044,'La informacion No esta disponible.');

		//Consulta de Solicitud de Servicio Social
		$query = "
		select
		eda.\"nmbAlumno\" as nombre_alumno,
		ss.no_ctrl,
		(select \"cllDomAlu\" || ' #' || \"numDomAlu\" || ' Col. ' || \"colDomAlu\" from public.\"E_datosPersonalesAlumno\" where \"nctrAlumno\" = eda.\"nctrAlumno\") as direccion_alumno,
		(select \"telDomAlu\" from public.\"E_datosPersonalesAlumno\" where \"nctrAlumno\" = eda.\"nctrAlumno\")as telefono,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera_alumno,
		eda.\"semAlumno\" as semestre,
		pss.nombre_programa,
		ss.\"empresaSupervisorJefe\" as nombre_empresa,
		ss.nombre_supervisor,
		ss.cargo_supervisor,
		pss.nombre_programa,
		(select periodo_programa from pe_planeacion.ss_periodos_programas where id_periodo_programa = pss.id_periodo_programa) as periodo_programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		pss.actividades_especificas_realizar,
		pss.id_clasificacion_area_servicio_social as tipo_programa,
		pss.id_unidad_receptora as no_empresa,
		spss.fecha_solicitud_programa,
		spss.valida_solicitud_alumno,
		spss.id_estado_solicitud_programa_alumno
		from pe_planeacion.ss_solicitud_programa_servicio_social spss
		join pe_planeacion.ss_servicio_social ss
		on ss.id_servicio_social = spss.id_solicitud_programa
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where spss.id_solicitud_programa = '$id_solicitud_programa'
		";

		$datos_alumno_solicitud = Yii::app()->db->createCommand($query)->queryAll();

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Llenar la parte de areas de servicio social en el Reporte
		$query2 = "select * from pe_planeacion.ss_clasificacion_area 
					where status_area = true order by id_clasificacion_area_servicio_social ASC";
		$lista_areas_activas = Yii::app()->db->createCommand($query2)->queryAll();

		$fecha_inicio_programa = $this->getFormatoFecha($datos_alumno_solicitud[0]['fecha_inicio_programa']);
		$fecha_fin_programa = $this->getFormatoFecha($datos_alumno_solicitud[0]['fecha_fin_programa']);
		$fecha_sol_programa = $this->getFormatoFecha($datos_alumno_solicitud[0]['fecha_solicitud_programa']);
		$fec_val_sol_alum = InfoSolicitudProgramas::fechaValidaSolicitudAlumno($id_solicitud_programa);

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', 0, '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad=3
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Solicitud Servicio Social');
		# render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirSolicitudServicioSocial', array(
						  'datos_alumno_solicitud' => $datos_alumno_solicitud,
						  'lista_areas_activas' => $lista_areas_activas,
						  'banners' => $banners,
						  'fecha_inicio_programa' => $fecha_inicio_programa,
						  'fecha_fin_programa' => $fecha_fin_programa,
						  'fecha_sol_programa' => $fecha_sol_programa,
						  'fec_val_sol_alum' => $fec_val_sol_alum
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Solicitud Servicio Social.pdf', 'I');
	}
	//Imprime la solicitud del servicio social cuando es alumno ya es aceptado al programa

	//La institucion local debe ser la primera empresa en registrar, debe tener el id 1
	public function getDatosBanners()
	{
		$modelEmpresas = SsUnidadesReceptoras::model()->findByPk(1);

		if($modelEmpresas === NULL)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelEmpresas;
	}

	/*Una vex que el alumno sea ACEPTADO en el programa se creara su registro en la tabla servicio_social*/
	public function insertRegistroTablaServicioSocial($no_ctrl, $id_solicitud_programa, $id_programa)
	{
		//Inicializamos la instancia del modelo
		$modelSSServicioSocial = new SsServicioSocial;
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);

		/*REALIZAMOS LA INSERCION DEL REGISTRO EN LA TABLA servicio_social */
		/*Comprobamos que no haya registros repetidos del alumno en la tabla servicio_social*/
		if(!$this->evitarRegistroRepetidoTablaServicioSocial($no_ctrl, $modelSSProgramas->id_programa))
		{
			$modelSSServicioSocial->id_servicio_social = $id_solicitud_programa; //Mismo id de la solicitud_servicio_social
			$modelSSServicioSocial->no_ctrl = $no_ctrl;
			$modelSSServicioSocial->id_estado_servicio_social = 2; // En Curso
			$modelSSServicioSocial->id_programa = $modelSSProgramas->id_programa;
			$modelSSServicioSocial->fecha_registro = date('Y-m-d H:i:s');
			$modelSSServicioSocial->fecha_modificacion = date('Y-m-d H:i:s');
			$modelSSServicioSocial->calificacion_servicio_social = 0; //Calificacion default 0
			$modelSSServicioSocial->rfcDirector = $this->getDirectorActual(); //RFC del director del instituto
			$modelSSServicioSocial->rfcSupervisor = $this->getRFCSupervisor($modelSSProgramas->id_programa);
			$modelSSServicioSocial->cargo_supervisor = $this->getCargoSupervisor($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa));
			$modelSSServicioSocial->empresaSupervisorJefe = $this->getEmpresaSupervisorYJefe($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa)); //Obtener la empresa a la que pertenece el supervisor
			$modelSSServicioSocial->nombre_jefe_depto = $this->getNombreJefeDepto($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa)); // Obtener nombre de jefe depto del depto del supervisor
			$modelSSServicioSocial->departamentoSupervisorJefe = $this->getDepartamentoSupervisorJefe($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa)); //Nombre del jefe de depto. al que pertenece el supervisor
			$modelSSServicioSocial->rfcJefeOficServicioSocial = $this->getJefeOficinaServicioSocialActual(); //Jefe de Oficina de Servicio Social
			$modelSSServicioSocial->rfcJefeDeptoVinculacion = $this->getJefeDeptoVinculacionActual(); //Jefe de Depto. de Vinculacion
			$modelSSServicioSocial->nombre_supervisor = $this->getDatosSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);

			/*Si se inserto correctamente no mandara mensaje de error */
			if(!$modelSSServicioSocial->save()){
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
				die('Ocurrio un error al querer guardar los datos.');
			}
		}
	}

	public function getNombreJefeDepto($id_programa, $rfcSupervisor)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$nombre_jefe_depto = "";

		//Interno
		if($modelSSProgramas->id_tipo_programa == 1)
		{
			//Nos trae el nombre del jefe de depto.
			$nombre_jefe_depto = $this->getJefeDeptoXDepartamento($rfcSupervisor);

		}else
		if($modelSSProgramas->id_tipo_programa == 2)
		{
			$query = "select nombre_jefe_depto from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nombre_jefe_depto'];
		}

		return $nombre_jefe_depto;
	}

	//Obtener el nomnbre del jefe de depto del supervisor interno por medio del rfc del supervisor
	public function getJefeDeptoXDepartamento($rfcSupervisor)
	{
		$depto = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$result2 = Yii::app()->db->createCommand($depto)->queryAll();
		$cveDepto = ($result2 != NULL) ? $result2[0]['cveDepartamentoEmp'] : null; //Obtenemos la clave del depto. al cual pertenece ese supervisor
		$nombre_jefe_depto = "";

		if($cveDepto == 1) //Direccion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '01' AND \"cvePuestoParticular\" = '01')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 2) //Subdirector Planeacion y Vinculacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '01')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 3) //Subdireccion Academica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '02')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 4) //Subdireccion de Servicios Administrativos
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '03')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 5) //Depto Planeacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '01')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 6) //Vinculacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '02')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 7) //Comunicacion y difusion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '03')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 8)//Actividades Extraescolares
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '03')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 9) //Servicios Escolares
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '04');";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 10) //Centro de informacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '05')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 11)
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '06')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 12)
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '07')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 13) //Mecanica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '08')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 14) //Bioquimica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '09')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 15) //Quimica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '09')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 16) //Industrial
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '10')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 17) //Electronica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '11')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 18)// Ciencias Economico-Administrativas
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '12')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 19) // Desarrollo Academico
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '13')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 20) //Div. Estudios Profesionales
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '14')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 21) //Div. Est. Postgrado e Investigacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '15')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 22) //Recursos Humanos
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '16')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 23) //Recursos Financieros
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '17')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 24) //Recursos Materiales y de Servicios
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '18')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 25) //Centro de Computo
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '19')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 26) //Mantenimiento de equipo
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '20')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 29) //Ambiental
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '21')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 30) //Mecatronica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '22')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 32) //Calidad
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '23')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else{
			$nombre_jefe_depto = null;
		}

		return $nombre_jefe_depto;
	}

	public function getDepartamentoSupervisorJefe($id_programa, $rfcSupervisor)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$departamento = "";

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$query = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$departamento = ($result != NULL) ? $result[0]['dscDepartamento'] : null;

		}else
		if($modelSSProgramas->id_tipo_programa == 2){//EXTERNO

			$query = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$departamento = $result[0]['dscDepartamento'];
		}

		return $departamento;
	}

	//Obtenemnos el RFC del jefe de Vinculacion
	public function getJefeDeptoVinculacionActual()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '02' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['rfcEmpleado'];
	}

	public function getNombreJefeOfServicioSocial()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '04' AND \"cvePuestoParticular\" = '02' AND \"cveDepartamentoEmp\" = '06' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['nmbCompletoEmp'];
	}

	//Obtenemos el RFC del jefe de la oficina de Servicio Social
	public function getJefeOficinaServicioSocialActual()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '04' AND \"cvePuestoParticular\" = '02' AND \"cveDepartamentoEmp\" = '06' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['rfcEmpleado'];
	}

	//Nos traemos el RFC del supervisor del programa dependiendo del tipo de programa
	public function getRFCSupervisor($id_programa)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$rfc = "";

		if($modelSSProgramas->id_tipo_programa == 1)//INTERNO
		{
			$query = "select rpi.\"rfcEmpleado\" from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.id_programa = '$idPrograma' AND rpi.superv_principal_int = true";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcEmpleado'];

		}else
		if($modelSSProgramas->id_tipo_programa == 2)//EXTERNO
		{

			$query = "select rpe.\"rfcSupervisor\" from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			where rpe.id_programa = '$idPrograma' AND rpe.superv_principal_ext = true";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcSupervisor'];
		}

		return $rfc;
	}

	public function getCargoSupervisor($id_programa, $rfcSupervisor)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$cargoSupervisor = "";

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$query = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();

			$cargoSupervisor = ($result != NULL) ? $result[0]['nmbPuesto'] : null;
		}else
		if($modelSSProgramas->id_tipo_programa == 2)
		{
			$query = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();

			$cargoSupervisor = ($result != NULL) ? $result[0]['cargo_supervisor'] : null;
		}

		return $cargoSupervisor;
	}

	public function getEmpresaSupervisorYJefe($id_programa, $rfcSupervisor)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_tipo_programa;
		$empresa = "";

		if($idPrograma == 1)
		{
			//id = 1 porque tec celaya debe ser la "empresa" default
			$query = "select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$empresa = $result[0]['nombre_unidad_receptora'];

		}else
		if($idPrograma == 2)
		{
			$query = "select ur.nombre_unidad_receptora from pe_planeacion.ss_supervisores_programas sp
			join pe_planeacion.ss_unidades_receptoras ur
			on ur.id_unidad_receptora = sp.id_unidad_receptora
			where sp.\"rfcSupervisor\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$empresa = $result[0]['nombre_unidad_receptora'];

		}

		return $empresa;
	}

	//Obtenemos el RFC del director del instituto
	public function getDirectorActual()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '01' AND \"cvePuestoParticular\" = '01' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['rfcEmpleado'];

	}

	/*Restar lugar_disponible del programa donde se acaba de ACEPTAR al ALUMNO*/
	public function restarLugarDisponibleEnPrograma($modelSSProgramas)
	{
		$modelSSProgramas->lugares_disponibles = $modelSSProgramas->lugares_disponibles - 1;

		if($modelSSProgramas->lugares_disponibles == 0 OR $modelSSProgramas->lugares_disponibles < 0)
		{
			$modelSSProgramas->lugares_disponibles = 0;
		}

		/*Se actualiza el campo de lugares disponibles para saber si el programa aun tiene lugares */
		if(!$modelSSProgramas->save()){
			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			die('Ocurrio un error!!!');
		}

	}
	/*Restar lugar_disponible del programa donde se acaba de ACEPTAR al ALUMNO*/

	/*Damos formato a la fecha*/
	public function getFormatoFecha($fecha)
	{
		$fechats = strtotime($fecha); //pasamos a timestamp

		//Obtenemos el dia, mes y año
		$dia = date('d', $fechats);
		$mes = $this->getMes(date('m', $fechats));
		$anio = date('Y', $fechats);

		return $dia." de ".$mes." de ".$anio;
	}
	/*Damos formato a la fecha*/

	public function getMes($mes)
	{
		$_mes="";

		switch ($mes)
		{
			case 1: $_mes = "Enero"; break;
			case 2: $_mes = "Febrero"; break;
			case 3: $_mes = "Marzo"; break;
			case 4: $_mes = "Abril"; break;
			case 5: $_mes = "Mayo"; break;
			case 6: $_mes = "Junio"; break;
			case 7: $_mes = "Julio"; break;
			case 8: $_mes = "Agosto"; break;
			case 9: $_mes = "Septiembre"; break;
			case 10: $_mes = "Octubre"; break;
			case 11: $_mes = "Noviembre"; break;
			case 12: $_mes = "Diciembre"; break;
			default: $_mes = "Desconocido"; break;
		}

		return $_mes;
	}

	/*Revisamos si el alumno ya esta cursando su servicio social */
	public function evitarRegistroRepetidoTablaServicioSocial($no_ctrl, $id_programa)
	{
		$query =
		"select * from pe_planeacion.ss_servicio_social
		where no_ctrl = '$no_ctrl'
		and id_programa = '$id_programa'
		and id_estado_servicio_social != 6 and id_estado_servicio_social != 7;
		";

		$datos_alumno = Yii::app()->db->createCommand($query)->queryAll();

		/*Si es mayor a cero ya hay registro del alumno en la tabla servicio_social y por
		lo tanto regresa true*/
		return ($datos_alumno != NULL) ? true : false;
	}
	/*Revisamos si el alumno ya esta cursando su servicio social */

	public function conteoDeHorasRealizadasServicioSocial($no_ctrl, $horas_realizar)//100
	{
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);
		$horas_totales = 480;
		//300
		$horas_liberadas = $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social;
        //horas faltantes
		$horas_faltantes = $horas_totales - $horas_liberadas;//180
		$horas_liberar = 0;

		/*Solamente entra aqui cuando las horas a realizar son mayores a las horas que le
		faltan por liberar*/
		if($horas_realizar >= $horas_faltantes)
		{
			$horas_liberar = $horas_realizar - ($horas_realizar - $horas_faltantes);//20

		}else{
			$horas_liberar = $horas_realizar;
		}

		return $horas_liberar;
	}

	public function actionValidaAlumnoClausulaSolicitudServicioSocial($id_solicitud_programa)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarSolicitudServicioSocial($id_solicitud_programa, $no_ctrl))
			throw new CHttpException(4044,'La informacion No esta disponible.');

		$modelSSSolicitudProgramaServicioSocial = $this->loadModel($id_solicitud_programa);

		//DATOS DEL ALUMNO
		//Nombre del alumno
		$datos_alumno = $this->getNameAlumno($modelSSSolicitudProgramaServicioSocial->no_ctrl);
		//carrera
		$carrera = $this->getCarrera($datos_alumno->cveEspecialidadAlu);
		//DATOS DEL ALUMNO

		//INFORMACION DEL PROGRAMA
		//Datos del Programa
		$datos_programa = $this->getDatosPrograma($modelSSSolicitudProgramaServicioSocial->id_programa);
		//Periodo del programa (Semestral, Anual o Unica Vez)
		$periodo_programa = $this->getPeriodoPrograma($datos_programa->id_periodo_programa);
		//Formato fecha de inicio del programa
		$fec_inicio_programa = $this->getFormatoFecha($datos_programa->fecha_inicio_programa);
		//Formato fecha fin del programa
		$fec_fin_programa = $this->getFormatoFecha($datos_programa->fecha_fin_programa);
		//Fecha solicitud al Programa
		$fecha_sol_programa = $this->getFormatoFecha($modelSSSolicitudProgramaServicioSocial->fecha_solicitud_programa);
		//Estados de la Solicitud
		$edos_solicitud = $this->getEstadosSolPrograma();
		//INFORMACION DEL PROGRAMA

		//INFORMACION DEL SUPERVISOR
		//Datos del supervisor
		$nombre_supervisor = $this->getDatosSupervisor($datos_programa->id_programa, $datos_programa->id_tipo_programa);
		//Datos de la Empresa del Supervisor
		$datos_empresa_supervisor = $this->getDatosEmpresa($datos_programa->id_programa, $datos_programa->id_tipo_programa);
		//Puesto del Supervisor
		$puesto_supervisor = $this->getPuestoSupervisor($datos_programa->id_programa, $datos_programa->id_tipo_programa);
		//INFORMACION DEL SUPERVISOR

		if(isset($_POST['SsSolicitudProgramaServicioSocial']))
    	{
    		$modelSSSolicitudProgramaServicioSocial->attributes=$_POST['SsSolicitudProgramaServicioSocial'];

			if($modelSSSolicitudProgramaServicioSocial->save())
			{
					/*Dependiendo si acepto o no la clausulas de la Solicitud, entonces...*/
					if($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno == 2){

						$modelSSSolicitudProgramaServicioSocial->valida_solicitud_alumno = date('Y-m-d H:i:s');
						$modelSSSolicitudProgramaServicioSocial->save();

					}elseif ($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno == 4){

						$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 4;
						$modelSSSolicitudProgramaServicioSocial->save();

					}elseif ($modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 1){

						Yii::app()->user->setFlash('danger', 'Error!!! No puedes dejar la Solicitud en PENDIENTE.');

					}

					//Redireccionamos
					Yii::app()->user->setFlash('success', 'Datos guardados correctamente!!!');
					$this->redirect(array('ssProgramas/listaProgramasAlumnoServicioSocial'));

			}else{
					//Si ocurrio un error
					Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al guardar los datos.');
			}
    	}

		$this->render('validaAlumnoClausulaSolicitudServicioSocial',array(
					  'modelSSSolicitudProgramaServicioSocial'=>$modelSSSolicitudProgramaServicioSocial,
					  'no_ctrl' => $modelSSSolicitudProgramaServicioSocial->no_ctrl,
					  'fecha_sol_programa' => $fecha_sol_programa,
					  'datos_alumno' => $datos_alumno,
					  'carrera' => $carrera,
					  'datos_programa' => $datos_programa,
					  'nombre_supervisor' => $nombre_supervisor,
					  'datos_empresa_supervisor' => $datos_empresa_supervisor,
					  'puesto_supervisor' => $puesto_supervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio_programa' => $fec_inicio_programa,
					  'fec_fin_programa' => $fec_fin_programa,
					  'edos_solicitud' => $edos_solicitud
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model = new SsSolicitudProgramaServicioSocial;

		if(isset($_POST['SsSolicitudProgramaServicioSocial']))
		{
			$model->attributes=$_POST['SsSolicitudProgramaServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_solicitud_programa));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['SsSolicitudProgramaServicioSocial']))
		{
			$model->attributes=$_POST['SsSolicitudProgramaServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_solicitud_programa));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsSolicitudProgramaServicioSocial');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$model=SsSolicitudProgramaServicioSocial::model()->findByPk($id);

		if($model===null)
			throw new CHttpException(404,'No existen datos de esa Solicitud.');


		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsSolicitudProgramaServicioSocial $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='solicitud-programa-servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/***************************************GETTERS AND SETTERS****************************************/
	public function getEstadosXLugaresProgramas($id_programa)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de esa Solicitud.');

		/*Verificamos si hay lugares disponibles en el Programa, de lo contrario se va la opcion de ACEPTADO */
		if($modelSSProgramas->lugares_disponibles == 0)
		{
			$criteria = new CDbCriteria();
			$criteria->condition = 'id_estado_solicitud_programa != 1 AND id_estado_solicitud_programa != 4 AND id_estado_solicitud_programa != 2 AND id_estado_solicitud_programa != 5';
			$criteria->order = "id_estado_solicitud_programa ASC";
			$modelSSEstadosSolicitudPrograma = SsEstadosSolicitudPrograma::model()->findAll($criteria);

			$lista_edo_sol = CHtml::listData($modelSSEstadosSolicitudPrograma, "id_estado_solicitud_programa", "estado_solicitud_programa");
		}else{

			$criteria = new CDbCriteria();
			$criteria->condition = 'id_estado_solicitud_programa != 1 AND id_estado_solicitud_programa != 4 AND id_estado_solicitud_programa != 5';
			$criteria->order = "id_estado_solicitud_programa ASC";
			$modelSSEstadosSolicitudPrograma = SsEstadosSolicitudPrograma::model()->findAll($criteria);

			$lista_edo_sol = CHtml::listData($modelSSEstadosSolicitudPrograma, "id_estado_solicitud_programa", "estado_solicitud_programa");
		}

		return $lista_edo_sol;
	}

	public function getEstadosSolPrograma()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_estado_solicitud_programa != 1 AND id_estado_solicitud_programa != 4 AND id_estado_solicitud_programa != 5';
		$criteria->order = "id_estado_solicitud_programa ASC";
		$modelSSEstadosSolicitudPrograma = SsEstadosSolicitudPrograma::model()->findAll($criteria);

		$lista_edo_sol = CHtml::listData($modelSSEstadosSolicitudPrograma, "id_estado_solicitud_programa", "estado_solicitud_programa");

		return $lista_edo_sol;
	}

	public function getIDprogramaVigenteSupervisor($rfcSupervisor)
	{
		$interno = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$externo = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

		$result1 = Yii::app()->db->createCommand($interno)->queryAll();
		$result2 = Yii::app()->db->createCommand($externo)->queryAll();

		if($result1 != NULL)
		{
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$ProgramaServicioSocial = Yii::app()->db->createCommand($query)->queryAll();

		}else
		if($result2 != NULL){
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			where rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$ProgramaServicioSocial = Yii::app()->db->createCommand($query)->queryAll();
		}

		return $ProgramaServicioSocial;
	}

	public function getNameAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";
		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		return $modelEDatosAlumno;
	}

	//Obtener la carrera del alumno
	public function getCarrera($cveEspecialidadAlu)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "  \"cveEspecialidad\" = '$cveEspecialidadAlu' "; //1 - Activo
		$carrera = EEspecialidad::model()->find($criteria);

		return $carrera->dscEspecialidad;
	}

	public function getDatosPrograma($id_programa)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_programa = '$id_programa' ";
		$modelSSProgramas = SsProgramas::model()->find($criteria);

		return $modelSSProgramas;
	}

	public function getDatosSupervisor($id_programa, $id_tipo_programa)
	{
		//Si es 1 es Interno, si es 2 es Externo
		if($id_tipo_programa == 1)
		{
			$query =
			"
			select hemp.\"nmbCompletoEmp\" as name from pe_planeacion.ss_responsable_programa_interno rpi
			join public.\"H_empleados\" hemp
			on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
			where rpi.id_programa = '$id_programa' AND rpi.superv_principal_int = true
			";
			$name = Yii::app()->db->createCommand($query)->queryAll();
		}else
		if($id_tipo_programa == 2)
		{
			$query =
			"
			select (sp.nombre_supervisor || ' ' || sp.apell_paterno || ' ' || sp.apell_materno) as name from pe_planeacion.ss_responsable_programa_externo rpe
			join pe_planeacion.ss_supervisores_programas sp
			on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
			where rpe.id_programa = '$id_programa' AND rpe.superv_principal_ext = true
			";
			$name = Yii::app()->db->createCommand($query)->queryAll();
		}

		return $name[0]['name'];
	}

	public function getDatosEmpresa($id_programa, $id_tipo_programa)
	{
		//1 - Interno y 2 - Externo
		//TNM INSTITUTO TECNOLOGICO DE CELAYA DEBE SER LA EMPRESA CON EL ID 1 EN LA TABLA ss_unidades_receptoras
		if($id_tipo_programa == 1)
		{
			$query = "
			select nombre_unidad_receptora as name_empresa
			from pe_planeacion.ss_responsable_programa_interno rpi
			join pe_planeacion.ss_programas pss
			on pss.id_programa = rpi.id_programa
			join pe_planeacion.ss_unidades_receptoras ur
			on ur.id_unidad_receptora = pss.id_unidad_receptora
			where rpi.id_programa = '$id_programa' AND rpi.superv_principal_int = true AND pss.id_unidad_receptora = 1
			";
			$result = Yii::app()->db->createCommand($query)->queryAll();
		}else
		if($id_tipo_programa == 2)
		{
			$query = "
			select ur.nombre_unidad_receptora as name_empresa
			from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			join pe_planeacion.ss_supervisores_programas sp
			on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
			join pe_planeacion.ss_unidades_receptoras ur
			on ur.id_unidad_receptora = sp.id_unidad_receptora
			where rpe.id_programa = '$id_programa' AND rpe.superv_principal_ext = true
			";
			$result = Yii::app()->db->createCommand($query)->queryAll();
		}

		return $result[0]['name_empresa'];
	}

	public function getPuestoSupervisor($id_programa, $id_tipo_programa)
	{
		//1 - Interno y 2 - Externo
		if($id_tipo_programa == 1)
		{
			$query =
			"select rpi.\"rfcEmpleado\" as rfc
			from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.id_programa = '$id_programa' AND rpi.superv_principal_int = true
			";
			$rfc = Yii::app()->db->createCommand($query)->queryAll();

			$rfcEmpleado = $rfc[0]['rfc'];
			$query2 =
			"select \"nmbPuesto\" as puesto_supervisor
			from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcEmpleado'
			";

			$result = Yii::app()->db->createCommand($query2)->queryAll();
		}else
		if($id_tipo_programa == 2)
		{
			$query =
			"select cargo_supervisor as puesto_supervisor
			from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			join pe_planeacion.ss_supervisores_programas sp
			on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
			where rpe.id_programa = '$id_programa' AND rpe.superv_principal_ext = true
			";

			$result = Yii::app()->db->createCommand($query)->queryAll();
		}

		return ($result != NULL) ? $result[0]['puesto_supervisor'] : null;
	}

	public function getPeriodoPrograma($id_periodo_programa)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " id_periodo_programa = '$id_periodo_programa' ";
		$modelSSPeriodosProgramas = SsPeriodosProgramas::model()->find($criteria);

		return $modelSSPeriodosProgramas;
	}

	public function validarSolicitudServicioSocial($id_solicitud_programa, $no_ctrl)
	{
		/*$model = SsSolicitudProgramaServicioSocial::model()->findByAttributes(
			array('id_solicitud_programa'=>$id_solicitud_programa,
				  'no_ctrl'=>$no_ctrl,
				  'status=:status',
    			array(':status'=>1)
			)
		);*/

		$query = "select * from pe_planeacion.ss_solicitud_programa_servicio_social
		where id_solicitud_programa = '$id_solicitud_programa' and no_ctrl = '$no_ctrl' and
		(id_estado_solicitud_programa_supervisor = 1 or id_estado_solicitud_programa_supervisor = 2) and
		(id_estado_solicitud_programa_alumno = 1 or id_estado_solicitud_programa_alumno = 2)";

		$model = Yii::app()->db->createCommand($query)->queryAll();

		return ($model != NULL) ? true : false;
	}

	public function validarSolicitudHistoricaServicioSocial($id_solicitud_programa, $no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_servicio_social
		where id_servicio_social = '$id_solicitud_programa' and no_ctrl = '$no_ctrl' and
		id_estado_servicio_social = 6";

		$model = Yii::app()->db->createCommand($query)->queryAll();

		return ($model != NULL) ? true : false;
	}

	public function validarSolicitudesSupervisor($rfcSupervisor, $id_solicitud_programa, $no_ctrl)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_solicitud_programa_servicio_social spss
				on spss.id_programa = pss.id_programa
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and spss.id_solicitud_programa = '$id_solicitud_programa' and
				spss.no_ctrl = '$no_ctrl' and (spss.id_estado_solicitud_programa_supervisor = 1 and spss.id_estado_solicitud_programa_alumno = 2)";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_solicitud_programa_servicio_social spss
				on spss.id_programa = pss.id_programa
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and spss.id_solicitud_programa = '$id_solicitud_programa' and
				spss.no_ctrl = '$no_ctrl' and (spss.id_estado_solicitud_programa_supervisor = 1 and spss.id_estado_solicitud_programa_alumno = 2)";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}
	/***************************************GETTERS AND SETTERS****************************************/
}
