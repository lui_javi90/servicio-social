<?php

class SsServiciosSocialesAnterioresController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaServiciosSocialesAnteriores',
								 'nuevoRegistroServicioSocialAnterior',
								 'editarRegistroServicioSocialAnterior',
								 'detalleServicioSocialAnterior'
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
				),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
		);
	}

	public function actionListaServiciosSocialesAnteriores()
	{
		$modelSSServiciosSocialesAnteriores = new SsServiciosSocialesAnteriores_('search');
		$modelSSServiciosSocialesAnteriores->unsetAttributes();  // clear any default values

		//Verificar que no haya registro de servicio social
		$hayRegistroFechasServicioSocial = $this->hayRegistroServicioSocialActual();

		if(isset($_GET['SsServiciosSocialesAnteriores']))
		{
				$modelSSServiciosSocialesAnteriores->attributes=$_GET['SsServiciosSocialesAnteriores'];
		}

		$this->render('listaServiciosSocialesAnteriores',array(
					  'modelSSServiciosSocialesAnteriores'=>$modelSSServiciosSocialesAnteriores,
					  'hayRegistroFechasServicioSocial' => $hayRegistroFechasServicioSocial
		));
	}

	public function hayRegistroServicioSocialActual()
	{
		$query = "select * from pe_planeacion.ss_registro_fechas_servicio_social where ssocial_actual = true";
		$datos = Yii::app()->db->createCommand($query)->queryAll();

		return ($datos === NULL) ? false : true;
	}

	public function actionNuevoRegistroServicioSocialAnterior()
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';
		$modelSSServiciosSocialesAnteriores=new SsServiciosSocialesAnteriores;

		$lista_periodos_programas = $this->getPeriodosProgramas();

		if(isset($_POST['SsServiciosSocialesAnteriores']))
		{
			$modelSSServiciosSocialesAnteriores->attributes=$_POST['SsServiciosSocialesAnteriores'];

			$no_ctrl = trim($modelSSServiciosSocialesAnteriores->no_ctrl);

			/*Verificamos que las horas liberadas sean un numero entero positivo*/
			if(RegularExpression::isEnteroPositivo($modelSSServiciosSocialesAnteriores->horas_liberadas))
			{
				/*Verificamos que la CALIFICACION sean un numero entero positivo*/
				if(RegularExpression::isEnteroPositivo($modelSSServiciosSocialesAnteriores->calificacion_servicio_social))
				{
					/*Verificamos si su registro de horas ya fue creado, sino no se insertan datos*/
					if($this->servicioSocialActivado($no_ctrl))
					{
						/*No se puede registrar un Servicio Social pasado cuando el alumno esta llevando o al menos hizo una SOLICITUD para llevar Servicio Social*/
						if(!$this->alumnoEstaLlevandoServicioSocialActualmente($no_ctrl))
						{
							
							$modelSSServiciosSocialesAnteriores->no_ctrl = $no_ctrl;

							if($modelSSServiciosSocialesAnteriores->fecha_inicio_programa < $modelSSServiciosSocialesAnteriores->fecha_fin_programa)
							{
								if($modelSSServiciosSocialesAnteriores->no_ctrl != NULL)
								{
									if($modelSSServiciosSocialesAnteriores->calificacion_servicio_social != 0 AND $modelSSServiciosSocialesAnteriores->calificacion_servicio_social <= 100)
									{
										if($modelSSServiciosSocialesAnteriores->horas_liberadas != 0 AND $modelSSServiciosSocialesAnteriores->horas_liberadas <= $this->getHorasServicioSocial())
										{
											/*Si inseto correctamente entonces actualiza las horas totales */
											if($modelSSServiciosSocialesAnteriores->save())
											{
												Yii::app()->user->setFlash('success', 'Datos guardados correctamente!!!');
												/*Antes de redireccionar agregamos la horas al historial del alumno*/
												$this->agregarHorasLiberadasAHistoricoTotalesAlumnos($modelSSServiciosSocialesAnteriores->no_ctrl, $modelSSServiciosSocialesAnteriores->horas_liberadas);
												//Redireccionamos
												$this->redirect(array('listaServiciosSocialesAnteriores'));

											}else{

												Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
											}

										}else{

											Yii::app()->user->setFlash('danger','Error!!! Las Horas liberadas del Servicio Social deben ser enteras positivas mayor a 0 y no mayor a 480.');
										}
									}else{

										Yii::app()->user->setFlash('danger','Error!!! La calificación del Servicio Social debe ser entero positivo mayor a 0 y no mayor a 100.');
									}
								}else{

									Yii::app()->user->setFlash('danger', 'Error!!! El No. de Control no puede ser Nulo');
								}
							}else{
								
								Yii::app()->user->setFlash('danger', 'Error!!! La fecha inicio no puede ser mayor a la fecha fin del programa.');
							}

						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! No se puede realizar el Registro porque el Alumno esta realizando Servicio Social Actualmente.');
							//die('Error!!! No se puede realizar el Registro porque el Alumno esta realizando Servicio Social Actualmente.');
						}

					}else{
						Yii::app()->user->setFlash('danger', 'Error!!! El Alumno con ese No. de Control no tiene dado de ALTA el Servicio Social.');
						//die('Error!!! El Alumno con ese No. de Control no tiene dado de ALTA el Servicio Social.');
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! El Calificación debe ser un numero entero positivo.');
				}

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! El numero de horas liberadas debe ser un numero entero positivo.');
			}

		}

		$this->render('nuevoRegistroServicioSocialAnterior',array(
					  'modelSSServiciosSocialesAnteriores'=>$modelSSServiciosSocialesAnteriores,
					  'lista_periodos_programas' => $lista_periodos_programas
		));
	}

	/*Verificamos que el Alumno ya tenga un registro para el conteo de horas de servicio social */
	public function servicioSocialActivado($no_ctrl)
	{
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);

		return ($modelSSHistoricoTotalesAlumnos != NULL) ? true : false;
	}

	/*Verificar que el alumno no haya hecho ningun registro para servicio social ni tenga solicitud a algun Programa*/
	public function alumnoEstaLlevandoServicioSocialActualmente($no_ctrl)
	{
		/*Verificamos que el almno no tenga ninguna solicitud de Servicio Social*/
		$criteria = new CDbCriteria;
		$criteria->condition = "no_ctrl = '$no_ctrl' AND (id_estado_solicitud_programa_supervisor = 1 OR id_estado_solicitud_programa_supervisor = 2) AND 
		(id_estado_solicitud_programa_alumno = 1 OR id_estado_solicitud_programa_alumno = 2)";
		$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->find($criteria);
		$cond1 = ($modelSSSolicitudProgramaServicioSocial != NULL) ? true : false;

		/*Verificamos que el alumno No este llevando servicio social ACTUALMENTE */
		$criteria2 = new CDbCriteria;
		$criteria2->condition = "no_ctrl = '$no_ctrl' AND (id_estado_servicio_social >= 1 AND id_estado_servicio_social <= 5)";
		$modelSSServicioSocial = SsServicioSocial::model()->find($criteria2);
		$cond2 = ($modelSSServicioSocial != NULL) ? true : false;

		return ($cond1 == false AND $cond2 == false) ? false : true;
	}

	/*Agregamos las horas liberadas a la tabla de ss_historico_totales_alumnos*/
	public function agregarHorasLiberadasAHistoricoTotalesAlumnos($no_ctrl, $horas_liberadas)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " no_ctrl = '$no_ctrl' ";
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->find($criteria);

		if($modelSSHistoricoTotalesAlumnos === NULL)
		{
			throw new CHttpException(404,'No existe ningun registro de Servicio Social del alumno con ese no. de control.');

		}else{
			$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social = $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social + $horas_liberadas;
			$modelSSHistoricoTotalesAlumnos->fecha_modificacion_horas_totales = date('Y-m-d H:i:s');
			$modelSSHistoricoTotalesAlumnos->save();
			//Verificamos si ya completo las 480 horas de servicio social solicitadas
			$this->verificarHorasCompletadas($modelSSHistoricoTotalesAlumnos, $no_ctrl);
		}

	}
	/*Agregamos las horas liberadas a la tabla de ss_historico_totales_alumnos*/

	public function verificarHorasCompletadas($modelSSHistoricoTotalesAlumnos, $no_ctrl)
	{
		/*Validar cuando haya completado las 480 horas y poder calcular su calificacion final de servicio social*/
		if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= $this->getHorasServicioSocial())
		{
			/*Se actualiza el campo servicio social completado*/
			$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social = $this->getHorasServicioSocial();
			$modelSSHistoricoTotalesAlumnos->calificacion_final_servicio_social = $this->getCalificacionServicioSocialFinal($no_ctrl);
			$modelSSHistoricoTotalesAlumnos->completo_servicio_social = true;
			$modelSSHistoricoTotalesAlumnos->save();
		}
	}

	public function getCalificacionServicioSocialFinal($no_ctrl)
	{
		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicio_social */
		$query1 = "SELECT (CASE WHEN sum(calificacion_servicio_social) = 0
						THEN 0 ELSE sum(calificacion_servicio_social) END) as suma1
						FROM pe_planeacion.ss_servicio_social
						WHERE no_ctrl = '$no_ctrl' AND id_estado_servicio_social = 6 ";

		$total1 = Yii::app()->db->createCommand($query1)->queryAll();

		/*NUMERO DE TODOS LOS SERVICIOS SOCIALES QUE COMPLETO */
		$query2 ="SELECT (CASE WHEN COUNT(id_servicio_social) = 0
						THEN 0 ELSE COUNT(id_servicio_social) END) as div1
						FROM pe_planeacion.ss_servicio_social
						WHERE no_ctrl = '$no_ctrl' AND id_estado_servicio_social = 6 ";

		$div1 = Yii::app()->db->createCommand($query2)->queryAll();
		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicio_social */

		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicios_sociales_anteriores */
		$query3 = "SELECT (CASE WHEN sum(calificacion_servicio_social) = 0
						THEN 0 ELSE sum(calificacion_servicio_social) END) as suma2
						FROM pe_planeacion.ss_servicios_sociales_anteriores
						WHERE no_ctrl = '$no_ctrl' ";

		$total2 = Yii::app()->db->createCommand($query3)->queryAll();

		/*NUMERO DE TODOS LOS SERVICIOS SOCIALES QUE COMPLETO (ANTERIORES)*/
		$query4 = "SELECT (CASE WHEN COUNT(id_servicio_social_anterior) = 0
						THEN 0 ELSE COUNT(id_servicio_social_anterior) END) as div2
						FROM pe_planeacion.ss_servicios_sociales_anteriores
						WHERE no_ctrl = '$no_ctrl' ";

		$div2 = Yii::app()->db->createCommand($query4)->queryAll();
		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicios_sociales_anteriores */

		return (int)($total1[0]['suma1'] + $total2[0]['suma2']) / ($div1[0]['div1'] + $div2[0]['div2']);
	}

	/*Obtenemos las horas que se piden para liberar el Servicio Social*/
	public function getHorasServicioSocial()
	{
		$query = "select horas_max_servicio_social
		from pe_planeacion.ss_configuracion where id_configuracion = 1
		";

		$horas = Yii::app()->db->createCommand($query)->queryAll();

		return $horas[0]['horas_max_servicio_social'];
	}
	/*Obtenemos las horas que se piden para liberar el Servicio Social*/

	public function actionEditarRegistroServicioSocialAnterior($id_servicio_social_anterior)
	{
		$modelSSServiciosSocialesAnteriores = $this->loadModel($id_servicio_social_anterior);

		$lista_periodos_programas = $this->getPeriodosProgramas();

		if(isset($_POST['SsServiciosSocialesAnteriores']))
		{
			$modelSSServiciosSocialesAnteriores->attributes=$_POST['SsServiciosSocialesAnteriores'];

			if($modelSSServiciosSocialesAnteriores->fecha_inicio_programa > $modelSSServiciosSocialesAnteriores->fecha_fin_programa)
			{
				throw new CHttpException(404,'Error!!! La fecha inicio no puede ser mayor a la fecha fin del programa.');
			}

			if($modelSSServiciosSocialesAnteriores->no_ctrl === NULL)
			{
				throw new CHttpException(404,'Error!!! El No. de Control no puede ser Nulo.');
			}

			if($modelSSServiciosSocialesAnteriores->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaServiciosSocialesAnteriores'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

		}

		$this->render('nuevoRegistroServicioSocialAnterior',array(
					  'modelSSServiciosSocialesAnteriores'=>$modelSSServiciosSocialesAnteriores,
					  'lista_periodos_programas' => $lista_periodos_programas
		));
	}

	public function actionDetalleServicioSocialAnterior($id_servicio_social_anterior)
	{
		$modelSSServiciosSocialesAnteriores = $this->loadModel($id_servicio_social_anterior);

		$nombre_alumno = $this->getNameCompletoAlumno($modelSSServiciosSocialesAnteriores->no_ctrl);
		$carrera = $this->getCarreraAlumno($modelSSServiciosSocialesAnteriores->no_ctrl);
		$semestre = $this->getSemestre($modelSSServiciosSocialesAnteriores->no_ctrl);
		$fec_ini_programa = $this->getFormatoFecha($modelSSServiciosSocialesAnteriores->fecha_inicio_programa);
		$fec_fin_programa = $this->getFormatoFecha($modelSSServiciosSocialesAnteriores->fecha_fin_programa);

		$this->render('detalleServicioSocialAnterior',array(
					  'modelSSServiciosSocialesAnteriores'=>$modelSSServiciosSocialesAnteriores,
					  'nombre_alumno' => $nombre_alumno,
					  'carrera' => $carrera,
					  'semestre' => $semestre,
					  'fec_ini_programa' => $fec_ini_programa,
					  'fec_fin_programa' => $fec_fin_programa
		));
	}

	/*Devuelve el nombre completo del alumno */
	public function getNameCompletoAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";

		$modelAlumnosVigentes = EDatosAlumno::model()->find($criteria);

		return $modelAlumnosVigentes->nmbAlumno;
	}

	/*Devuelve la carrera del alumno */
	public function getCarreraAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";
		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		$criteria2 = new CDbCriteria();
		$criteria2->condition = " \"cveEspecialidad\" = '$modelEDatosAlumno->cveEspecialidadAlu' ";

		$modelEEspecialidad = EEspecialidad::model()->find($criteria2);

		return $modelEEspecialidad->dscEspecialidad;
	}

	/*Devuelve el semestre del alumno */
	public function getSemestre($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";

		$modelAlumnosVigentes = EDatosAlumno::model()->find($criteria);

		return $modelAlumnosVigentes->semAlumno;
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsServiciosSocialesAnteriores');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelSSServiciosSocialesAnteriores=SsServiciosSocialesAnteriores::model()->findByPk($id);

		if($modelSSServiciosSocialesAnteriores===null)
		{
			throw new CHttpException(404,'Error!!! no existen datos de ese Servicio Social.');
		}

		return $modelSSServiciosSocialesAnteriores;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-servicios-sociales-anteriores-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*******************************************GETTERS AND SETTERS********************************************/
	public function getPeriodosProgramas()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_periodo_programa > 0 ";
		$modelSSPeriodosProgramas = SsPeriodosProgramas::model()->findAll($criteria);

		$lista_periodos= CHtml::listData($modelSSPeriodosProgramas, "id_periodo_programa", "periodo_programa");

		return $lista_periodos;
	}

	public function getNoCtrlsAlumnos()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "no_ctrl != null ";
		$modelAlumnosVigentesInscritosCreditos = AlumnosVigentesInscritosCreditos::model()->findAll($criteria);

		$lista_no_ctrls= CHtml::listData($modelAlumnosVigentesInscritosCreditos, "no_ctrl", "no_ctrl");

		return $lista_no_ctrls;
	}

	/*Damos formato a la fecha*/
	public function getFormatoFecha($fecha)
	{
		$fechats = strtotime($fecha); //pasamos a timestamp

		//Obtenemos el dia, mes y año
		$dia = date('d', $fechats);
		$mes = $this->getMes(date('m', $fechats));
		$anio = date('Y', $fechats);

		return $dia." de ".$mes." de ".$anio;
	}
	/*Damos formato a la fecha*/

	public function getMes($mes)
	{
		$_mes="";

		switch ($mes)
		{
			case 1: $_mes = "ENERO"; break;
			case 2: $_mes = "FEBRERO"; break;
			case 3: $_mes = "MARZO"; break;
			case 4: $_mes = "ABRIL"; break;
			case 5: $_mes = "MAYO"; break;
			case 6: $_mes = "JUNIO"; break;
			case 7: $_mes = "JULIO"; break;
			case 8: $_mes = "AGOSTO"; break;
			case 9: $_mes = "SEPTIEMBRE"; break;
			case 10: $_mes = "OCTUBRE"; break;
			case 11: $_mes = "NOVIEMBRE"; break;
			case 12: $_mes = "DICIEMBRE"; break;
			default: $_mes = "DESCONOCIDO"; break;
		}

		return $_mes;
	}
	/*******************************************GETTERS AND SETTERS********************************************/

}
