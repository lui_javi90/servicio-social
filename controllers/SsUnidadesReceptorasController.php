<?php

class SsUnidadesReceptorasController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaUnidadesReceptoras',
								 'nuevaUnidadReceptora',
								 'editarUnidadesReceptoras',
								 'editarLogoUnidadesReceptoras',
								 'deshabilitarUnidadReceptora', //Solo deshabilita la empresa
								 'detalleUnidadReceptora',
								 'municipiosEstados',
								 'menuEmpresas', //Menu para mostrar todas las empresas
								 'listaHistoricoUnidadesReceptoras', //Historico empresas
								 'nuevoSelloEmpresa', //Agregar sello
								 'nuevosBannersEmpresa',  //Agregar banners para las cartas de aceptacion, presentacion y terminacion
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
				),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
		);
	}

	/*Lista todas la empresas*/
	public function actionListaUnidadesReceptoras()
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$modelSSUnidadesReceptoras = new SsUnidadesReceptoras_('searchXAlta');
		$modelSSUnidadesReceptoras->unsetAttributes();  // clear any default values

		if(isset($_GET['SsUnidadesReceptoras']))
		{
			$modelSSUnidadesReceptoras->attributes=$_GET['SsUnidadesReceptoras'];
		}
			
		$this->render('listaUnidadesReceptoras',array(
						'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
		));
	}

	/*Lista todas la empresas*/
	public function actionNuevaUnidadReceptora()
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';
		$modelSSUnidadesReceptoras = new SsUnidadesReceptoras;

		/*Lista de los estados */
		$lista_estados = $this->getEstados();

		/*Lista de los municipios */
		$lista_municipios = array();

		if(isset($_POST['SsUnidadesReceptoras']))
		{
			$modelSSUnidadesReceptoras->attributes=$_POST['SsUnidadesReceptoras'];
			$modelSSUnidadesReceptoras->fecha_registro_unidad_receptora = date('Y-m-d H:i:s');
			$modelSSUnidadesReceptoras->fecha_modificacion_unidad_receptora = $modelSSUnidadesReceptoras->fecha_registro_unidad_receptora;
			$modelSSUnidadesReceptoras->id_status_unidad_receptora = 1; //ALTA

			if($modelSSUnidadesReceptoras->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaUnidadesReceptoras'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se pudieron guardar los datos.');
			}
				
		}

		$this->render('nuevaUnidadReceptora',array(
					  'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
					  'lista_estados' => $lista_estados,
					  'lista_municipios' => $lista_municipios
		));
	}

	/*Editar la Empresa */
	public function actionEditarUnidadesReceptoras($id_unidad_receptora)
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$modelSSUnidadesReceptoras=$this->loadModel($id_unidad_receptora);

		/*Lista de los Estados */
		$lista_estados = $this->getEstados();

		/*Lista de los Municipios*/
		$lista_municipios = $this->getMunicipios();

		if(isset($_POST['SsUnidadesReceptoras']))
		{
			$modelSSUnidadesReceptoras->attributes=$_POST['SsUnidadesReceptoras'];
			$modelSSUnidadesReceptoras->fecha_modificacion_unidad_receptora = date('Y-m-d H:i:s');
			if($modelSSUnidadesReceptoras->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaUnidadesReceptoras'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se pudieron guardar los datos.');
			}
				
		}

		$this->render('nuevaUnidadReceptora',array(
					  'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
					  'lista_estados' => $lista_estados,
					  'lista_municipios' => $lista_municipios
		));
	}
	/*Editar la Empresa */

	/*Agregar Logo a la Unidad Receptora o Empresa */
	public function actionEditarLogoUnidadesReceptoras($id_unidad_receptora)
	{
		$modelSSUnidadesReceptoras = $this->loadModel($id_unidad_receptora);
		
		/*Tipos de FORMATO permitidos */
		$extensiones = array("jpeg", "jpg", "png");
		$SSUnidadesReceptoras = new SsUnidadesReceptoras;

		if(isset($_POST['SsUnidadesReceptoras']))
		{
			$SSUnidadesReceptoras->attributes=$_POST['SsUnidadesReceptoras'];

			/*CARGAR LOGO DE LA EMPRESA O UNIDAD RECEPTORA*/
			//Devuelve una Instancia del archivo subido
			$SSUnidadesReceptoras->logo_unidad_receptora = CUploadedFile::getInstance($SSUnidadesReceptoras, 'logo_unidad_receptora');
			
			//Verificamos que se haya subido alguna imagen
			if($SSUnidadesReceptoras->logo_unidad_receptora != null)
			{
				//2MB = 2097152
				if($SSUnidadesReceptoras->logo_unidad_receptora->getSize() > '2097152')
					$errors[] = 'El archivo debe pesar menos de 2MB.';

				if(in_array($SSUnidadesReceptoras->logo_unidad_receptora->getExtensionName(), $extensiones) === false)
					$errors[] = 'La extensión del archivo debe ser png, jpeg o jpg';

				/*if(strlen($modelSSUnidadesReceptoras->logo_unidad_receptora->getName()) > 15){ $errors[] = 'El nombre del logo de perfil es demasiado largo. Debe ser de '; }*/
				$name = $_FILES['SsUnidadesReceptoras']['name']['logo_unidad_receptora'];
				$filename = pathinfo($name, PATHINFO_FILENAME); //Obtenemos el nombre del archivo
				$ext = pathinfo($name, PATHINFO_EXTENSION); //Obtenemos el formato del documento subido

				if(empty($errors))
				{
					//Ejemplo de nombre de la carpeta seria 1_TNM INSTITUTO TECNOLÓGICO DE CELAYA
					//$new_name_carp = $modelSSUnidadesReceptoras->id_unidad_receptora.'_'.$modelSSUnidadesReceptoras->nombre_unidad_receptora;
					$new_name_carp = 'Empresa'.'_'.$modelSSUnidadesReceptoras->id_unidad_receptora;

					//Nuevo nombre del archivo de acuerdo al formato ejemplo sello_1.jpg
					$new_name_logo = 'lg_'.date('YmdHis').'_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'.'.$ext;

					//Eliminamos el logo actual para subir el nuevo
					if(file_exists(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/'.$modelSSUnidadesReceptoras->logo_unidad_receptora)) 
					{
						//удаляем файл
						unlink(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/'.$modelSSUnidadesReceptoras->logo_unidad_receptora);
						$modelSSUnidadesReceptoras->logo_unidad_receptora = null;
						$modelSSUnidadesReceptoras->save();
					}

					//Ruta donde se guardara el logo de la empresa o unidad receptora
					if(!is_dir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp))
					{
						mkdir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp, 0, true);
						chmod(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp, 0777);
					}

					//Guardamos la imagen localmente
					$SSUnidadesReceptoras->logo_unidad_receptora->saveAs(Yii::getPathOfAlias("webroot")."/images/servicio_social/encabezados_empresas/".$new_name_carp."/".$new_name_logo);

					//Insertamos nombre en el campo url_documento
					$modelSSUnidadesReceptoras->logo_unidad_receptora = $new_name_logo;

					//Guardamos el nombre de la carpeta para acceder a las imagenes de la empresa despues
					$modelSSUnidadesReceptoras->path_carpeta = $new_name_carp;

					//Si no hubo error alguno entonces mandara devolvera true
					if($modelSSUnidadesReceptoras->save())
					{
						Yii::app()->user->setFlash('success', "Logo guardado correctamente!");
						$this->redirect(array('listaUnidadesReceptoras'));
					}else{

						Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar el logo.");
					}

				}else{

					//Mostrar todos los errores que surgieron al tratar de subir la imagen
					$var = "";
					$esp="<br>";
					$contador = 0;

					foreach($errors as $key=>$value)
					{
						$contador++;
						if($contador == 1)
						{
							$var = $value;
						}else{
							$var .= $esp.$value;
						}
					}

					Yii::app()->user->setFlash('danger', trim($var));
					//Resfrescamos pantalla
					$this->refresh();

				}
			}
			/*CARGAR LOGO DE LA EMPRESA O UNIDAD RECEPTORA*/
		}

		$this->render('editarLogoUnidadesReceptoras', array(
					  'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
		));
	}
	/*Agregar Logo a la Unidad Receptora o Empresa */

	/*Se permite cambiar de sello, las veces que gusten */
	public function actionNuevoSelloEmpresa($id_unidad_receptora)
	{
		$modelSSUnidadesReceptoras = $this->loadModel($id_unidad_receptora);
		
		/*Tipos de FORMATO permitidos, en los pdfs solo permite mostrar imagenes en formato jpg o jpeg*/
		$extensiones = array("jpeg", "jpg");

		if(isset($_POST['SsUnidadesReceptoras']))
        {
	
			$modelSSUnidadesReceptoras->attributes = $_POST['SsUnidadesReceptoras'];
			
			/*CARGAR SELLOS Y BANNERS DE LA EMPRESA PARA LOS REPORTES BIMESTRALES*/
			//Devuelve una Instancia del archivo subido
			$modelSSUnidadesReceptoras->sello_empresa = CUploadedFile::getInstance($modelSSUnidadesReceptoras, 'sello_empresa');

			//Verificamos que se haya subido alguna imagen
			if($modelSSUnidadesReceptoras->sello_empresa != null)
			{

				//2MB = 2097152
				if($modelSSUnidadesReceptoras->sello_empresa->getSize() > '2097152')
					$errors[] = 'El archivo debe pesar menos de 2MB.';

				if(in_array($modelSSUnidadesReceptoras->sello_empresa->getExtensionName(), $extensiones) === false)
					$errors[] = 'La extensión del archivo debe ser jpeg o jpg';

				/*if(strlen($modelSSUnidadesReceptoras->sello_empresa->getName()) > 18){ $errors[] = 'El nombre de la imagen es demasiado largo. Debe ser de maximo 18 caracteres.'; }*/
				$name = $_FILES['SsUnidadesReceptoras']['name']['sello_empresa'];
				$filename = pathinfo($name, PATHINFO_FILENAME); //Obtenemos el nombre del archivo
				$ext = pathinfo($name, PATHINFO_EXTENSION); //Obtenemos el formato del documento subido

				//Nuevo nombre del archivo de acuerdo al formato ejemplo sello_1.jpg
				$new_name_sello = 'sello_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'.'.$ext;

				if(empty($errors))
				{
					//Ejemplo de nombre de la carpeta seria 1_TNM INSTITUTO TECNOLÓGICO DE CELAYA
					//$new_name_carp = $modelSSUnidadesReceptoras->id_unidad_receptora.'_'.$modelSSUnidadesReceptoras->nombre_unidad_receptora;
					$new_name_carp = 'Empresa'.'_'.$modelSSUnidadesReceptoras->id_unidad_receptora;

					//Ruta donde se guardara el logo de la empresa o unidad receptora
					if(!is_dir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp))
					{
						mkdir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp, 0, true);
						chmod(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp, 0775);
					}

					//Guardamos la imagen localmente
					$modelSSUnidadesReceptoras->sello_empresa->saveAs(Yii::getPathOfAlias("webroot")."/images/servicio_social/encabezados_empresas/".$new_name_carp.'/'.$new_name_sello);

					//Insertamos nombre en el campo url_documento
					$modelSSUnidadesReceptoras->sello_empresa = $new_name_sello;

					//Actualizamos la fecha de la ultima actualizacion
					$modelSSUnidadesReceptoras->fecha_modificacion_unidad_receptora = date('Y-m-d H:i:s');

					//Guardamos el nombre de la carpeta para acceder a las imagenes de la empresa despues
					//if($modelSSUnidadesReceptoras->path_carpeta === NULL)
					$modelSSUnidadesReceptoras->path_carpeta = $new_name_carp;

					//Si no hubo error alguno entonces mandara devolvera true
					if($modelSSUnidadesReceptoras->save())
					{
						Yii::app()->user->setFlash('success', "Imagenes guardadas correctamente!");
						$this->redirect(array('listaUnidadesReceptoras'));
					}else{

						Yii::app()->user->setFlash('danger', "Error!!! no se pudieron guardar las imagenes.");
					}

				}else{

					//Mostrar todos los errores que surgieron al tratar de subir la imagen
					$var = "";
					$esp="<br>";
					$contador = 0;

					foreach($errors as $key=>$value)
					{
						$contador++;
						if($contador == 1)
						{
							$var = $value;
						}else{
							$var .= $esp.$value;
						}
					}

					Yii::app()->user->setFlash('danger', trim($var));
					//Resfrescamos pantalla
					$this->refresh();

				}
			}
			/*CARGAR SELLOS Y BANNERS DE LA EMPRESA PARA LOS REPORTES BIMESTRALES*/ 
        }

		$this->render('NuevoSelloEmpresa',array(
					  'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
		));
	}

	/* Cargaremos uno o ambos banners de la empresa */
	public function actionNuevosBannersEmpresa($id_unidad_receptora)
	{
		
		$modelSSUnidadesReceptoras = $this->loadModel($id_unidad_receptora);

		/*Tipos de FORMATO permitidos, en los pdfs solo permite mostrar imagenes en formato jpg o jpeg*/
		$extensiones = array("jpg");
		//Instanciamos un Objeto
		$SSUnidadesReceptoras = new SsUnidadesReceptoras;

		if(isset($_POST['SsUnidadesReceptoras']))
        {
			$SSUnidadesReceptoras->attributes = $_POST['SsUnidadesReceptoras'];

			//Devuelve una Instancia del archivo subido (Si se subio)
			$SSUnidadesReceptoras->banner_superior = CUploadedFile::getInstance($SSUnidadesReceptoras, 'banner_superior');
			$SSUnidadesReceptoras->banner_inferior = CUploadedFile::getInstance($SSUnidadesReceptoras, 'banner_inferior');

			if($SSUnidadesReceptoras->banner_superior != NULL)
			{
				//2MB = 2097152
				if($SSUnidadesReceptoras->banner_superior->getSize() > '2097152')
					$errors[] = 'El banner superior debe pesar menos de 2MB.';

				if(in_array($SSUnidadesReceptoras->banner_superior->getExtensionName(), $extensiones) === false)
					$errors[] = 'La extensión del banner superior debe ser jpg.';

				$name = $_FILES['SsUnidadesReceptoras']['name']['banner_superior'];
				$filename = pathinfo($name, PATHINFO_FILENAME); //Obtenemos el nombre del archivo
				$ext = pathinfo($name, PATHINFO_EXTENSION); //Obtenemos el formato del documento subido

				//Si no hubo errores con la propiedades de la imagen
				if(empty($errors))
				{
					
					//Le damos Nuevo Nombre del archivo de acuerdo al formato ejemplo bs_20190704130124_1.jpg
					$new_name_bann_sup = 'bs_'.date('YmdHis').'_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'.'.$ext;

					//Ejemplo de nombre de la carpeta seria Empresa_1
					$new_name_carp = 'Empresa_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora);

					//Eliminamos el banner actual para subir el nuevo
					if(file_exists(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.$modelSSUnidadesReceptoras->banner_superior)) {
						//удаляем файл
						unlink(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.$modelSSUnidadesReceptoras->banner_superior);
						$modelSSUnidadesReceptoras->banner_superior = null;
						$modelSSUnidadesReceptoras->save();
					}

					//Ruta donde se guardara el logo de la empresa o unidad receptora, ejemplo Empresa_1
					if(!is_dir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora)))
					{
						mkdir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora), 0, true);
						chmod(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora), 0777);
					}

					//Guardamos la imagen localmente Yii::getPathOfAlias("webroot").
					$SSUnidadesReceptoras->banner_superior->saveAs(Yii::getPathOfAlias("webroot").'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_superior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.$new_name_bann_sup);
					//Insertamos nombre en el campo url_documento
					$modelSSUnidadesReceptoras->banner_superior = $new_name_bann_sup;

					//Actualizamos la fecha de la ultima actualizacion
					$modelSSUnidadesReceptoras->fecha_modificacion_unidad_receptora = date('Y-m-d H:i:s');

					//Guardamos el nombre de la carpeta para acceder a las imagenes de la empresa despues, se agrega la path solo si no se ha guardado anteriormente
					$modelSSUnidadesReceptoras->path_carpeta = $new_name_carp;

					//Si no hubo error alguno entonces mandara devolvera true
					if($modelSSUnidadesReceptoras->save())
					{
						Yii::app()->user->setFlash('success', "Imagen guardada correctamente!!!");
						$this->redirect(array('listaUnidadesReceptoras'));
					}else{

						Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar las imagen.");
					}

				}else{
					//Mostrar todos los errores que surgieron al tratar de subir la imagen
					$var = "";
					$esp="<br>";
					$contador = 0;

					foreach($errors as $key=>$value)
					{
						$contador++;
						if($contador == 1)
						{
							$var = $value;
						}else{
							$var .= $esp.$value;
						}
					}

					Yii::app()->user->setFlash('danger', trim($var));
					//Resfrescamos pantalla
					$this->refresh();
				}

			}elseif($SSUnidadesReceptoras->banner_inferior != NULL)
			{
				
				//2MB = 2097152
				if($SSUnidadesReceptoras->banner_inferior->getSize() > '2097152')
					$errors[] = 'El banner inferior debe pesar menos de 2MB.';

				if(in_array($SSUnidadesReceptoras->banner_inferior->getExtensionName(), $extensiones) === false)
					$errors[] = 'La extensión del banner inferior debe ser jpg.';

				$name = $_FILES['SsUnidadesReceptoras']['name']['banner_inferior'];
				$filename = pathinfo($name, PATHINFO_FILENAME); //Obtenemos el nombre del archivo
				$ext = pathinfo($name, PATHINFO_EXTENSION); //Obtenemos el formato del documento subido

				if(empty($errors))
				{
					//Nuevo nombre del archivo de acuerdo al formato ejemplo banner_inferior_1.jpg
					$new_name_bann_inf = 'bi_'.date('YmdHis').'_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'.'.$ext;

					//Ejemplo de nombre de la carpeta seria Empresa_1
					$new_name_carp = 'Empresa_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora);

					//Eliminamos el banner actual para subir el nuevo
					if(file_exists(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.$modelSSUnidadesReceptoras->banner_inferior)) {
						//удаляем файл
						unlink(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.$modelSSUnidadesReceptoras->banner_inferior);
						$modelSSUnidadesReceptoras->banner_inferior = null;
						$modelSSUnidadesReceptoras->save();
					}

					//Ruta donde se guardara el logo de la empresa o unidad receptora
					if(!is_dir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora)))
					{
						mkdir(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora), 0, true);
						chmod(Yii::getPathOfAlias('webroot').'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora), 0777);
					}

					//Guardamos la imagen localmente
					$SSUnidadesReceptoras->banner_inferior->saveAs(Yii::getPathOfAlias("webroot").'/images/servicio_social/encabezados_empresas/'.$new_name_carp.'/banner_inferior_'.trim($modelSSUnidadesReceptoras->id_unidad_receptora).'/'.$new_name_bann_inf);

					//Insertamos nombre en el campo url_documento
					$modelSSUnidadesReceptoras->banner_inferior = $new_name_bann_inf;

					//Actualizamos la fecha de la ultima actualizacion
					$modelSSUnidadesReceptoras->fecha_modificacion_unidad_receptora = date('Y-m-d H:i:s');

					//Guardamos el nombre de la carpeta para acceder a las imagenes de la empresa despues, se agrega la path solo si no se ha guardado anteriormente
					$modelSSUnidadesReceptoras->path_carpeta = $new_name_carp;

					//Si no hubo error alguno entonces mandara devolvera true
					if($modelSSUnidadesReceptoras->save())
					{
						Yii::app()->user->setFlash('success', "Imagen guardada correctamente!!!");
						$this->redirect(array('listaUnidadesReceptoras'));
					}else{

						Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar la imagen.");
					}

				}else{
					//Mostrar todos los errores que surgieron al tratar de subir la imagen
					$var = "";
					$esp="<br>";
					$contador = 0;

					foreach($errors as $key=>$value)
					{
						$contador++;
						if($contador == 1)
						{
							$var = $value;
						}else{
							$var .= $esp.$value;
						}
					}

					Yii::app()->user->setFlash('danger', trim($var));
					//Resfrescamos pantalla
					$this->refresh();
				}
			}

        }

		$this->render('nuevosBannersEmpresa',array(
					  'modelSSUnidadesReceptoras' => $modelSSUnidadesReceptoras
		));
		
	}
	/* Cargaremos uno o ambos banners de la empresa */

	public function actionDeshabilitarUnidadReceptora($id_unidad_receptora, $opc)
	{
		$modelSSUnidadesReceptoras = $this->loadModel($id_unidad_receptora);

		/*HAY DATOS DEL PROGRAMA ENTONCES SE PROCEDE A ELIMINAR*/
		if ($modelSSUnidadesReceptoras)
		{
			//Asigna el status FINALIZAR
			$modelSSUnidadesReceptoras->id_status_unidad_receptora = $opc; //1 para habilitar y 2 para deshabilitar

			if($modelSSUnidadesReceptoras->save())
				echo CJSON::encode( [ 'code' => 200 ] );
			else
				echo CJSON::encode( $modelSSUnidadesReceptoras->getErrors() );

		}
		/*HAY DATOS DEL PROGRAMA ENTONCES SE PROCEDE A ELIMINAR*/
	}

	public function actionDetalleUnidadReceptora($id_unidad_receptora)
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$modelSSUnidadesReceptoras = $this->loadModel($id_unidad_receptora);
		$estado = $this->getEstado($modelSSUnidadesReceptoras->id_estado);
		$municipio = $this->getMunicipio($modelSSUnidadesReceptoras->id_municipio);

		$this->render('detalleUnidadReceptora',array(
			'modelSSUnidadesReceptoras' => $modelSSUnidadesReceptoras,
			'estado' => $estado,
			'municipio' => $municipio
        ));
	}

	public function actionMenuEmpresas()
	{
		$this->render('menuEmpresas',array());
	}

	public function actionListaHistoricoUnidadesReceptoras()
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';
		
		$modelSSUnidadesReceptoras = new SsUnidadesReceptoras_('search');
		$modelSSUnidadesReceptoras->unsetAttributes();  // clear any default values

		if(isset($_GET['SsUnidadesReceptoras']))
		{
			$modelSSUnidadesReceptoras->attributes=$_GET['SsUnidadesReceptoras'];
		}
			
		$this->render('listaHistoricoUnidadesReceptoras',array(
						'modelSSUnidadesReceptoras'=>$modelSSUnidadesReceptoras,
		));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsUnidadesReceptoras');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelSSUnidadesReceptoras = SsUnidadesReceptoras::model()->findByPk($id);

		if($modelSSUnidadesReceptoras===null)
			throw new CHttpException(404,'No se encontro registro de la unidad receptora.');

			
		return $modelSSUnidadesReceptoras;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unidades-receptoras-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	/**********************************GETTERS AND SETTERS**********************************/
	public function getEstados()
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$criteria = new CDbCriteria();
		$criteria->condition="id_estado > 0 "; //Son 32 estados en el pais

		$modelXEstados = XEstados::model()->findAll($criteria);

		$lista_estados = CHtml::listData($modelXEstados, "id_estado", "nombre");

		return $lista_estados;
	}

	public function getMunicipios()
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$criteria = new CDbCriteria();
		$criteria->condition="id_municipio > 0 ";
		$modelXMunicipios = XMunicipios::model()->findAll($criteria);

		$lista_municipios = CHtml::listData($modelXMunicipios, "id_municipio", "nombre");

		return $lista_municipios;
	}

	public function actionMunicipiosEstados()
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$listMunicipios = XMunicipios::model()->findAll("id_estado=?",array($_POST['SsUnidadesReceptoras']['id_estado']));
		foreach($listMunicipios as $data)
			echo "<option value=\"{$data->id_municipio}\">{$data->nombre}</option>";

	}

	public function getEstado($id_estado)
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';

		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado = '$id_estado' "; //Son 32 estados en el pais

		$modelXEstados = XEstados::model()->find($criteria);

		return $modelXEstados->nombre;
	}

	public function getMunicipio($id_municipio)
	{
		require_once Yii::app()->basePath.'/models/CMN/XMunicipios.php';
		require_once Yii::app()->basePath.'/models/CMN/XEstados.php';
		
		$criteria = new CDbCriteria();
		$criteria->condition = "id_municipio = '$id_municipio' "; //Son 32 estados en el pais

		$modelXMunicipios = XMunicipios::model()->find($criteria);

		return $modelXMunicipios->nombre;
	}
	/**********************************GETTERS AND SETTERS**********************************/
}
