<?php

class SsServicioSocialController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaAdminServicioSocial',//Admin
								 'historicoAdminServicioSocial', //Vista SOLO Admin para ver Servicio Social historicos
								 'nuevoServicioSocial',
								 'editarServicioSocial',
								 'detalleAdminServicioSocial',
								 'detalleHistoricoAlumnoServicioSocial',
								 'imprimirCartaAceptacion', //Reporte Carta de Aceptacion
								 'imprimirCartaHistoricaAceptacion',
								 'imprimirCartaPresentacion', //Reporte
								 'imprimirCartaHistoricaPresentacion',
								 'imprimirCartaTerminacion',	//Reporte
								 'imprimirCartaHistoricaTerminacion',
								 'statusServicioSocialAlumno',
								 'listaDeptoObservacionesServicioSocial', //Vista para hacer observaciones al alumno por parte del depto
								 'listaCandidatosALiberarServicioSocial', //Libera el Servicio Social Actual (y completo si hizo las 480 horas)
								 'liberarServicioSocialAlumno', //Una vez revisado todo se cambia el estado del servicio social a FINALIZADO (6) y se agregan la calificacion y las horas liberadas
								 'detalleLiberacionServicioSocialAlumno', //Detalle del servicio social de la parte de liberacion
								 'listaAdminActividades', //Vista Actividades Admin
								 'cartasHistoricoAdminServicioSocial',
								 'detalleAdminHistoricoServicioSocial',
								 'cancelarServicioSocialActual', //Cancelar Servicio Social del Alumno
								 'bajaServicioSocialActual', //Dar de baja el Servicio Social Actual del Alumno
								 'cartasHistoricoEscolaresAlumnoServicioSocial',
								 'liberarServicioSocialTodos',
								 'imprimirOficioEntregaCalificacionesServicioSocial', //Reporte Alumnos completaron el Servicio Social
								 //'listaAlumnosTerminacionServicioSocial' //Vista de los alumnos que concluyeron el Servicio Social (480 horas y se les asigno califiacion)
								 ),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users' => array('@')
				),
			array('allow',
				'actions'=>array('listaAlumnoServicioSocial',
								 'detalleAlumnoServicioSocial',
								 'imprimirCartaAceptacion',
								 'imprimirCartaHistoricaAceptacion',
								 'imprimirCartaPresentacion',
								 'imprimirCartaHistoricaPresentacion',
								 'imprimirCartaTerminacion',
								 'ImprimirCartaHistoricaTerminacion',
								 'historicoAlumnoServicioSocial',
								 'cartasHistoricoAlumnoServicioSocial',
								 'cartaTerminacionAlumnoServicioSocial',
								 'showHorarioProgramaServicioSocial' //Horario Programa (modal)
								),
				//'roles'=>array('alumno'),
				'users' => array('@')
			),
			array('allow',
				'actions'=>array('listaSupervisorObservacionesServicioSocial',
								'listaSupervisorActividades',
								'listaAlumnosServicioSocialProgramaVigente' //Lista Alumnos Programa Vigentes Servicio Social
								),
			   //'roles'=>array('vinculacion_supervisor_servicio_social'),
			   'users' =>array('@')
			),
			array('allow',
				'actions'=>array('cartasHistoricoEscolaresAlumnoServicioSocial',
								 'imprimirCartaAceptacion',
								 'imprimirCartaHistoricaAceptacion',
								 'imprimirCartaPresentacion',
								 'imprimirCartaHistoricaPresentacion',
								 'imprimirCartaTerminacion',
								 'imprimirCartaHistoricaTerminacion',
								),
				//'roles'=>array('jefe_departamento_escolares'),
				'users'=>array('@')
			),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionListaAlumnosServicioSocialProgramaVigente()
	{
		//Tomamos de su inicio de sesion del alumno
		$rfc_supervisor = Yii::app()->params['rfcSupervisor'];
		//$rfc_supervisor = Yii::app()->user->name;
		$rfcSupervisor = trim($rfc_supervisor);

		$modelSSServicioSocial = new SsServicioSocial('search');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values
		
        if(isset($_GET['SsServicioSocial']))
            $modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];

        $this->render('listaAlumnosServicioSocialProgramaVigente',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'rfcSupervisor' => $rfcSupervisor
        ));
	}

	public function actionShowHorarioProgramaServicioSocial($id_servicio_social)
	{
		$this->layout='//layouts/mainVacio';

		$modelSSServicioSocial = $this->loadModel($id_servicio_social); //Simple inspeccion
		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSServicioSocial->id_programa);

		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de ese Programa.');

		//Obtenemos el horario del programa para mostrarlo
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		if($modelSSHorarioDiasHabilesProgramas === null)
			throw new CHttpException(404,'No existen datos de ese registro.');

		
        if(isset($_GET['SsHorarioDiasHabilesProgramas']))
            $modelSSHorarioDiasHabilesProgramas->attributes=$_GET['SsHorarioDiasHabilesProgramas'];

		$this->render('showHorarioProgramaServicioSocial',array(
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas
		));

	}

	public function actionLiberarServicioSocialTodos()
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelSSServicioSocial = new SsServicioSocial_('searchServicioSocialXReportesEvaluadosYValidados');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values


		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		//Pasamos a Estatus "Completado" todos los servicios sociales en estatus "En Liberacion" del registro de servicio social vigente
		$qry_ssocial_lib = "Update pe_planeacion.ss_servicio_social 
							SET id_estado_servicio_social = 5
							where id_estado_servicio_social = 4 ";

		/*$criteria = new CDbCriteria;
		$criteria->condition = "";
		$model = SsServicioSocial::model()->find();*/
		
		try
		{
			if(Yii::app()->db->createCommand($qry_ssocial_lib)->queryAll())
			{
				//die("1");
				Yii::app()->user->setFlash('success', 'Orden Realizada correctamente!!!');
				//Si se realizo correctamente el cambio
				$transaction->commit();
				//$this->render('listaCandidatosALiberarServicioSocial', array('modelSSServicioSocial'=>$modelSSServicioSocial));

			}else{
				//die("2");
				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al realizar la Orden.');
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				//$this->renderPartial('listaCandidatosALiberarServicioSocial');
			}

		}catch(Exception $e)
		{
			//die("3");
			Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al realizar la Orden.');
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
		}
	}

	//Cancelar el Servicio Social ACTUAL del Alumno
	public function actionCancelarServicioSocialActual($id_servicio_social)
	{
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);

		$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->findByPk($modelSSServicioSocial->id_servicio_social);
		if($modelSSSolicitudProgramaServicioSocial === NULL)
			throw new CHttpException(404,'No existen datos de esa Solicitud de Servicio Social.');

		//Pasar al estatus Cancelado el Servicio Social del Alumno con ese id en la tabla servicio social
		$modelSSServicioSocial->id_estado_servicio_social = 7; //7.- CANCELADO
		$modelSSServicioSocial->fecha_modificacion = date('Y-m-d H:i:s'); //Actualizamos la fecha en que se modifico

		if($modelSSServicioSocial->save())
		{
			//Cambio en el estatus de la solicitud de Servicio Social del Alumno tambien
			$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 3; // CANCELADO
			$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 3;//	CANCELADO
			$modelSSSolicitudProgramaServicioSocial->save();

			echo CJSON::encode( [ 'code' => 200 ] );
		}
		else{
			echo CJSON::encode( $modelSSServicioSocial->getErrors() );
		}
	}
	//Cancelar el Servicio Social ACTUAL del Alumno

	public function actionBajaServicioSocialActual($id_servicio_social)
	{
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);

		$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->findByPk($modelSSServicioSocial->id_servicio_social);
		if($modelSSSolicitudProgramaServicioSocial === NULL)
			throw new CHttpException(404,'No existen datos de esa Solicitud de Servicio Social.');

		//Pasar al estatus Cancelado el Servicio Social del Alumno con ese id en la tabla servicio social
		$modelSSServicioSocial->id_estado_servicio_social = 8; //7.- CANCELADO
		$modelSSServicioSocial->fecha_modificacion = date('Y-m-d H:i:s'); //Actualizamos la fecha en que se modifico

		if($modelSSServicioSocial->save())
		{
			//Cambio en el estatus de la solicitud de Servicio Social del Alumno tambien
			$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 5; // BAJA
			$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 5;//	BAJA
			$modelSSSolicitudProgramaServicioSocial->save();

			//Restaurar lugar al Programa
			$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
			if($modelSSProgramas === NULL)
				throw new CHttpException(404,'No existen datos de ese Programa de Servicio Social.');

			$modelSSProgramas->lugares_disponibles = ($modelSSProgramas->lugares_disponibles + 1);
			$modelSSProgramas->save();
			//Restaurar lugar al Programa

			echo CJSON::encode( [ 'code' => 200 ] );
		}
		else{
			echo CJSON::encode( $modelSSServicioSocial->getErrors() );
		}
	}

	//Todos los Servicios Sociales (menos finalizados y cancelados)*/
	public function actionListaAdminServicioSocial($id_programa = null)
	{
		$modelSSServicioSocial = new SsServicioSocial_('searchXProgramasServicioSocial');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		/*Estados Servicio Social (menos finalizados y cancelados)*/
		//$lista_edos_ssocial = $this->getEstadosNoHistoricosServicioSocial();
		$lista_programas_activos = $this->getProgramasDisponibles();

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('listaAdminServicioSocial',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  //'lista_edos_ssocial' => $lista_edos_ssocial,
					  'lista_programas_activos' => $lista_programas_activos,
					  //'id_estado_servicio_social' => $id_estado_servicio_social
					  'id_programa' => $id_programa
		));
	}
	//Todos los Servicios Sociales (menos finalizados y cancelados)*/

	//Todos los Servicios Sociales Historicos (Solo finalizados y cancelados)*/
	public function actionHistoricoAdminServicioSocial($anio = null, $periodo = null)
	{
		$modelSSServicioSocial = new SsServicioSocial_('searchXEdoHistoricoServicioSocial');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		/*Estados Servicio Social (menos finalizados y cancelados)*/
		$lista_edos_ssocial = $this->getEstadosHistoricosServicioSocial();

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes = $_GET['SsServicioSocial'];
			//$modelSSServicioSocial->id_estado_servicio_social  = $_GET['SsServicioSocial']['id_estado_servicio_social'];
			$modelSSServicioSocial->anio = $_GET['SsServicioSocial']['anio'];
			$modelSSServicioSocial->periodo = $_GET['SsServicioSocial']['periodo'];
			$anio = $modelSSServicioSocial->anio;
			$periodo = $modelSSServicioSocial->periodo;
		}

		$this->render('historicoAdminServicioSocial',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'lista_edos_ssocial' => $lista_edos_ssocial,
					  //'id_estado_servicio_social' => $id_estado_servicio_social,
					  'anio' => $anio,
					  'periodo' => $periodo
		));
	}
	//Todos los Servicios Sociales Historicos (Solo finalizados y cancelados)*/

	public function actionListaAlumnoServicioSocial()
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		//Obtener el tipo de programa para saber que carta de aceptacion se muestra
		$modelSSServicioSocial = new SsServicioSocial_('searchXAlumno');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		$criteria = new CDbCriteria();
		$criteria->alias = "ss";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas as pss on pss.id_programa = ss.id_programa";
		$criteria->condition = "ss.no_ctrl = '$no_ctrl' AND ss.id_estado_servicio_social > 1 and ss.id_estado_servicio_social < 6";
		$ServicioSocial = SsServicioSocial::model()->find($criteria);

		if($ServicioSocial != null)
		{
			$modelSSProgramas = SsProgramas::model()->findByPk($ServicioSocial->id_programa);
			if($modelSSProgramas === null)
				throw new CHttpException(404,'No existen datos de ese Programa.');

		}else{
			$modelSSServicioSocial = new SsServicioSocial_('searchXAlumno');
			$modelSSServicioSocial->unsetAttributes();  // clear any default values
			$modelSSProgramas = new SsProgramas;
		}

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('listaAlumnoServicioSocial',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'modelSSProgramas' => $modelSSProgramas,
					  'no_ctrl' => $no_ctrl,
					  'id_tipo_programa' => $modelSSProgramas->id_tipo_programa
		));
	}

	public function getDatos($no_ctrl)
	{
		$query = "select * from ss_servicio_social as ss
				join pe_planeacion.ss_programas as pss
				on pss.id_programa = ss.id_programa
				where ss.no_ctrl = '$no_ctrl' AND ss.id_estado_servicio_social > 1
				and ss.id_estado_servicio_social < 6 ";

		$modelSSServicioSocial = Yii::app()->db->createCommand($query)->queryAll();

		return $modelSSServicioSocial;
	}

	public function actionNuevoServicioSocial()
	{
		$modelSSServicioSocial=new SsServicioSocial;

		$lista_programas = $this->getProgramas();

		$lista_estados_servicio = $this->getEstadosServicioSocial();

		if(isset($_POST['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_POST['SsServicioSocial'];
			if($modelSSServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaAdminServicioSocial'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

		}

		$this->render('nuevoServicioSocial',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'lista_programas' => $lista_programas,
					  'lista_estados_servicio' => $lista_estados_servicio
		));
	}

	public function actionEditarServicioSocial($id_servicio_social)
	{
		$modelSSServicioSocial=$this->loadModel($id_servicio_social);

		if(isset($_POST['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_POST['SsServicioSocial'];
			if($modelSSServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaAdminServicioSocial'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevoServicioSocial',array(
						'modelSSServicioSocial'=>$modelSSServicioSocial,
		));
	}

	public function actionDetalleAlumnoServicioSocial($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		$modelSSServicioSocial = $this->loadModel($id_servicio_social); //Simple inspeccion

		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSServicioSocial->id_programa);

		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de ese Programa.');

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK($modelSSServicioSocial->no_ctrl);

		$periodo_inicio = $this->getPeriodoServicioSocial($modelSSProgramas->fecha_inicio_programa);
		$periodo_fin = $this->getPeriodoServicioSocial($modelSSProgramas->fecha_fin_programa);

		$this->render('detalleAlumnoServicioSocial', array(
					  'nombre_alumno' => $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl),
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'carrera' => $this->getCarreraAlumno($modelSSHistoricoTotalesAlumnos->no_ctrl),
					  'semestre' => $this->getSemestre($modelSSHistoricoTotalesAlumnos->no_ctrl),
					  'estado_servicio_social' => $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social),
					  'modelSSProgramas' => $modelSSProgramas,
					  //'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin
		));
	}

	public function actionDetalleAdminHistoricoServicioSocial($id_servicio_social)
	{
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($modelSSServicioSocial->no_ctrl);
		if($modelEDatosAlumno === null)
			throw new CHttpException(404,'No existen datos de ese Alumno con ese No. de Control.');

		$modelEEspecialidad = EEspecialidad::model()->findByPk($modelEDatosAlumno->cveEspecialidadAlu);

		if($modelEEspecialidad === null)
			throw new CHttpException(404,'No existen datos de esa Especialidad.');

		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de ese programa.');

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK($modelSSServicioSocial->no_ctrl);
		if($modelSSHistoricoTotalesAlumnos === null)
			throw new CHttpException(404,'No existe Historial de Servicio Social de ese Alumno con ese No. de Control.');

		//Obtenemos el horario del programa para mostrarlo
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		if($modelSSHorarioDiasHabilesProgramas === null)
			throw new CHttpException(404,'No existen datos de ese registro.');

		$this->render('detalleAdminHistoricoServicioSocial', array(
					  'nombre_alumno' => $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl),
					  'no_ctrl' => $modelSSServicioSocial->no_ctrl,
					  'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'estado_servicio_social' => $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social),
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'modelEEspecialidad' => $modelEEspecialidad

		));
	}

	/*Obtener el periodo de inicio y fin del servicio social */
	public function getPeriodoServicioSocial($fecha)
	{
		if($fecha != null)
		{
			$dt = new DateTime($fecha);
			$mes = $dt->format("m");
			$anio = $dt->format("Y");
			$periodo="";

			if($mes <= 7)
			{
				$periodo = "ENERO-JULIO"." de ".$anio;
			}else
			if($mes > 7){
				$periodo = "AGOSTO-DICIEMBRE"." de ".$anio;
			}

		}else{

			$periodo = "POR DEFINIR";
		}

		return $periodo;
	}
	/*Obtener el periodo de inicio y fin del servicio social */

	public function actionDetalleHistoricoAlumnoServicioSocial($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		$modelSSServicioSocial = $this->loadModel($id_servicio_social);
		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSServicioSocial->id_programa);
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($modelSSServicioSocial->no_ctrl);
		if($modelEDatosAlumno === null)
			throw new CHttpException(404,'No existen datos de ese Alumno con ese No. de Control.');

		$modelEEspecialidad = EEspecialidad::model()->findByPk($modelEDatosAlumno->cveEspecialidadAlu);
		if($modelEEspecialidad === null)
			throw new CHttpException(404,'No existen datos de esa Especialidad.');

		if($modelSSProgramas === null)
		{
			$modelSSProgramas=new SsServicioSocial;
		}else{
			$nombre_alumno = $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl);
			$estado_servicio_social = $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social);
		}

		//Obtenemos el horario del programa para mostrarlo
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		if($modelSSHorarioDiasHabilesProgramas === null)
			$modelSSHorarioDiasHabilesProgramas=new SsHorarioDiasHabilesProgramas;


		$this->render('detalleHistoricoAlumnoServicioSocial', array(
					  'nombre_alumno' => $nombre_alumno,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'estado_servicio_social' => $estado_servicio_social,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'modelEEspecialidad' => $modelEEspecialidad

   		));
	}

	public function actionDetalleAdminServicioSocial($id_servicio_social)
	{
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);
		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSServicioSocial->id_programa);
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK($modelSSServicioSocial->no_ctrl);

		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de ese programa.');

		//Obtenemos el horario del programa para mostrarlo
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		if($modelSSHorarioDiasHabilesProgramas === null)
			throw new CHttpException(404,'No existen datos de ese registro.');

		$periodo_inicio = $this->getPeriodoServicioSocial($modelSSProgramas->fecha_inicio_programa);
		$periodo_fin = $this->getPeriodoServicioSocial($modelSSProgramas->fecha_fin_programa);

		$this->render('detalleAdminServicioSocial', array(
					  'nombre_alumno' => $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl),
					  'no_ctrl' => $modelSSServicioSocial->no_ctrl,
					  'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'estado_servicio_social' => $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social),
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin
		));

	}

	public function actionImprimirOficioEntregaCalificacionesServicioSocial()
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Datos del Jefe de Escolares
		$qry_jefe_escol = " select \"rfcEmpleado\", \"nmbCompletoEmp\", \"nmbPuesto\" 
							from public.h_ocupacionpuesto 
							where \"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '04' AND
							\"cveDepartamentoEmp\" = 9
						";
				
		$jefe_escolares = Yii::app()->db->createCommand($qry_jefe_escol)->queryAll();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/banner_inferior_'.trim($datos_carta_presentacion[0]['no_empresa']).'/'.$banners->banner_inferior.'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Presentación');

		$mPDF1->WriteHTML(
			$this->render('imprimirOficioEntregaCalificacionesServicioSocial', array(
						  'anio_genera_rep' => $this->getAnio(),
						  'fec_reporte' => $this->getFormatoFechaReportePDF(),
						  'jefe_escolares' => $jefe_escolares
						  
			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Presentacion.pdf', 'I');

	}

	public function getAnio()
	{
		$fechaComoEntero = strtotime(date('Y-m-d'));
		$anio = date("Y", $fechaComoEntero);

		return $anio;
	}

	public function actionImprimirCartaHistoricaPresentacion($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		//Construimos la consulta (Falta hacer la consulta, lo demas es de prueba)
		//falta nombre y depto de jefe gestion tecnologica y vinculacion
		//Agregar sigla estudios antes del nombre del supervisor del programa
		$query =
		"
		select
		ss.fecha_registro,
		ss.id_servicio_social as folio,
		(select extract(year from ss.fecha_registro)) as anio_servicio_social,
		spss.valida_solicitud_supervisor_programa as validacion_jefesup,
		spss.id_estado_solicitud_programa_supervisor as estatus_validacion_validacion_jefesup,
		ss.nombre_jefe_depto,
		ss.\"departamentoSupervisorJefe\",
		ss.\"empresaSupervisorJefe\",
		(select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as nombre_alumno,
		ss.no_ctrl,
		(select \"semAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as semestre_alumno,
		(select esp.\"dscEspecialidad\" from public.\"E_datosAlumno\" eda
		join public.\"E_especialidad\" esp
		on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
		where eda.\"nctrAlumno\" = ss.no_ctrl) as carrera_alumno,
		pss.horas_totales,
		pss.nombre_programa,
		pss.id_unidad_receptora as no_empresa,
		(case
		when pss.id_periodo_programa = 1 then 'seis'
		when pss.id_periodo_programa = 2 then 'doce'
		when pss.id_periodo_programa = 3 then 'dos'
		end) as periodo_programa,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as grado_max_estudios_jefvinculacion,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as jefe_vinculacion,
		ss.\"rfcJefeDeptoVinculacion\"
		from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_solicitud_programa_servicio_social spss
		on spss.id_solicitud_programa = ss.id_servicio_social
		where ss.id_servicio_social = '$id_servicio_social'
		";
		//Ejecutamos la consulta
		$datos_carta_presentacion = Yii::app()->db->createCommand($query)->queryAll();

		$fecha_validacion_sol = InfoSolicitudProgramas::fechaValidaSolicitudSupervisor($datos_carta_presentacion[0]['folio']);

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Fecha de creacion del servicio social
		$fec_reporte = $this->getFormatoFechaReportes($datos_carta_presentacion[0]['fecha_registro']);

		//Leyenda del documento PDF
		$leyenda_reporte_pdf = $this->getLeyendaReportePDF();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/banner_inferior_'.trim($datos_carta_presentacion[0]['no_empresa']).'/'.$banners->banner_inferior.'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Presentación');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirCartaPresentacion', array(
						  'fecha_validacion_sol' => $fecha_validacion_sol,
						  'datos_carta_presentacion' => $datos_carta_presentacion,
						  'fec_reporte' => $fec_reporte,
						  'banners' => $banners,
						  'leyenda_reporte_pdf' => $leyenda_reporte_pdf,
						  'anio' => $this->getAnio()
			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Presentacion.pdf', 'I');
	}

	public function getLeyendaReportePDF()
	{
		$id = 1;
		$modelSsConfiguracion = SsConfiguracion::model()->findByPk($id);

		if($modelSsConfiguracion === NULL)
			throw new CHttpException(404,'No existen datos de la Configuración.');

		return $modelSsConfiguracion->texto_leyenda_pdf_doc;
	}

	public function actionImprimirCartaPresentacion($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		//Construimos la consulta (Falta hacer la consulta, lo demas es de prueba)
		//falta nombre y depto de jefe gestion tecnologica y vinculacion
		//Agregar sigla estudios antes del nombre del supervisor del programa
		$query =
		"
		select
		ss.fecha_registro,
		ss.id_servicio_social as folio,
		(select extract(year from ss.fecha_registro)) as anio_servicio_social,
		spss.valida_solicitud_supervisor_programa as validacion_jefesup,
		spss.id_estado_solicitud_programa_supervisor as estatus_validacion_validacion_jefesup,
		ss.nombre_jefe_depto,
		ss.\"departamentoSupervisorJefe\",
		ss.\"empresaSupervisorJefe\",
		(select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as nombre_alumno,
		ss.no_ctrl,
		(select \"semAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as semestre_alumno,
		(select esp.\"dscEspecialidad\" from public.\"E_datosAlumno\" eda
		join public.\"E_especialidad\" esp
		on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
		where eda.\"nctrAlumno\" = ss.no_ctrl) as carrera_alumno,
		pss.horas_totales,
		pss.nombre_programa,
		pss.id_unidad_receptora as no_empresa,
		(case
		when pss.id_periodo_programa = 1 then 'seis'
		when pss.id_periodo_programa = 2 then 'doce'
		when pss.id_periodo_programa = 3 then 'dos'
		end) as periodo_programa,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as grado_max_estudios_jefvinculacion,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as jefe_vinculacion,
		ss.\"rfcJefeDeptoVinculacion\"
		from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_solicitud_programa_servicio_social spss
		on spss.id_solicitud_programa = ss.id_servicio_social
		where ss.id_servicio_social = '$id_servicio_social'
		";
		//Ejecutamos la consulta
		$datos_carta_presentacion = Yii::app()->db->createCommand($query)->queryAll();

		$fecha_validacion_sol = InfoSolicitudProgramas::fechaValidaSolicitudSupervisor($datos_carta_presentacion[0]['folio']);

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Fecha de creacion del servicio social
		$fec_reporte = $this->getFormatoFechaReportes($datos_carta_presentacion[0]['fecha_registro']);

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.$banners->path_carpeta.'/banner_inferior_'.trim($datos_carta_presentacion[0]['no_empresa']).'/'.$banners->banner_inferior.'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Presentación');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirCartaPresentacion', array(
						  'fecha_validacion_sol' => $fecha_validacion_sol,
						  'datos_carta_presentacion' => $datos_carta_presentacion,
						  'fec_reporte' => $fec_reporte,
						  'banners' => $banners
			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Presentacion.pdf', 'I');
	}

	//La institucion local debe ser la primera empresa en registrar, debe tener el id 1
	public function getDatosBanners()
	{
		$modelEmpresas = SsUnidadesReceptoras::model()->findByPk(1);

		if($modelEmpresas === NULL)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelEmpresas;
	}

	public function actionImprimirCartaHistoricaTerminacion($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		/*if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');*/

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
		//Construimos la consulta (Falta hacer la consulta, lo demas es de prueba)
		$query =
		"
		select
		ss.fecha_registro,
		ss.id_servicio_social as folio,
		(select extract(year from ss.fecha_registro)) as anio_servicio_social,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"cvePuestoGeneral\" = '01' AND \"cvePuestoParticular\" = '01') as grado_max_estudios_director,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"cvePuestoGeneral\" = '01' AND \"cvePuestoParticular\" = '01') as director,
		(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1) as institucion,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as grado_max_estudios_jefvinculacion,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as jefe_vinculacion,
		(select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as nombre_alumno,
		(select esp.\"dscEspecialidad\" from public.\"E_datosAlumno\" eda
		join public.\"E_especialidad\" esp
		on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
		where eda.\"nctrAlumno\" = ss.no_ctrl) as carrera_alumno,
		ss.no_ctrl,
		pss.nombre_programa,
		ss.\"departamentoSupervisorJefe\",
		pss.horas_totales,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		(select horas_totales from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = pss.id_programa limit 1) as horas_diarias_programa,
		(case
		when pss.id_periodo_programa = 1 then 'seis'
		when pss.id_periodo_programa = 2 then 'doce'
		when pss.id_periodo_programa = 3 then 'dos'
		end) as periodo_programa,
		ss.nombre_jefe_depto,
		spss.valida_solicitud_supervisor_programa,
		spss.id_estado_solicitud_programa_supervisor,
		(select banner_superior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_superior,
		(select banner_inferior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_inferior,
		(select path_carpeta from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) path_carpeta,
		(select id_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as no_empresa
		from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_solicitud_programa_servicio_social spss
		on spss.id_solicitud_programa = ss.id_servicio_social
		where ss.id_servicio_social = '$id_servicio_social'
		";

		//Ejecutamos la consulta
		$datos_carta_terminacion = Yii::app()->db->createCommand($query)->queryAll();

		//Fecha de creacion del Reporte
		$fec_reporte = $this->getFormatoFechaReportes($datos_carta_terminacion[0]['fecha_registro']);

		//Formato de fecha inicio y fin del Servicio Social
		$fec_inicio = $this->getFormatoFecha($datos_carta_terminacion[0]['fecha_inicio_programa']);
		$fec_fin = $this->getFormatoFecha($datos_carta_terminacion[0]['fecha_fin_programa']);
		$fecha_validacion_sol = InfoSolicitudProgramas::fechaValidaSolicitudSupervisor($datos_carta_terminacion[0]['folio']);

		$leyenda_reporte_pdf = $this->getLeyendaReportePDF();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', 0, '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.trim($datos_carta_terminacion[0]['path_carpeta']).'/banner_inferior_'.trim($datos_carta_terminacion[0]['no_empresa']).'/'.trim($datos_carta_terminacion[0]['banner_inferior']).'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Terminación');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirCartaTerminacion', array(
						  'datos_carta_terminacion' => $datos_carta_terminacion,
						  'fec_reporte' => $fec_reporte,
						  'fec_inicio' => $fec_inicio,
						  'fec_fin' => $fec_fin,
						  'fecha_validacion_sol' => $fecha_validacion_sol,
						  'leyenda_reporte_pdf' => $leyenda_reporte_pdf,
						  'anio' => $this->getAnio()
			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Terminacion.pdf', 'I');
	}

	/*Reporte Carta Terminacion para los Servicio Sociales Internos */
	public function actionImprimirCartaTerminacion($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';
		//Construimos la consulta (Falta hacer la consulta, lo demas es de prueba)
		$query =
		"
		select
		ss.fecha_registro,
		ss.id_servicio_social as folio,
		(select extract(year from ss.fecha_registro)) as anio_servicio_social,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcDirector\") as grado_max_estudios_director,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcDirector\") as director,
		(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1) as institucion,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as grado_max_estudios_jefvinculacion,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as jefe_vinculacion,
		(select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as nombre_alumno,
		(select esp.\"dscEspecialidad\" from public.\"E_datosAlumno\" eda
		join public.\"E_especialidad\" esp
		on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
		where eda.\"nctrAlumno\" = ss.no_ctrl) as carrera_alumno,
		ss.no_ctrl,
		pss.nombre_programa,
		ss.\"departamentoSupervisorJefe\",
		pss.horas_totales,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		(select horas_totales from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = pss.id_programa limit 1) as horas_diarias_programa,
		(case
		when pss.id_periodo_programa = 1 then 'seis'
		when pss.id_periodo_programa = 2 then 'doce'
		when pss.id_periodo_programa = 3 then 'dos'
		end) as periodo_programa,
		ss.nombre_jefe_depto,
		spss.valida_solicitud_supervisor_programa,
		spss.id_estado_solicitud_programa_supervisor,
		(select banner_superior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_superior,
		(select banner_inferior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_inferior,
		(select path_carpeta from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) path_carpeta,
		(select id_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as no_empresa
		from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_solicitud_programa_servicio_social spss
		on spss.id_solicitud_programa = ss.id_servicio_social
		where ss.id_servicio_social = '$id_servicio_social'
		";

		//Ejecutamos la consulta
		$datos_carta_terminacion = Yii::app()->db->createCommand($query)->queryAll();

		//Fecha de creacion del Reporte
		$fec_reporte = $this->getFormatoFechaReportes($datos_carta_terminacion[0]['fecha_registro']);

		//Formato de fecha inicio y fin del Servicio Social
		$fec_inicio = $this->getFormatoFecha($datos_carta_terminacion[0]['fecha_inicio_programa']);
		$fec_fin = $this->getFormatoFecha($datos_carta_terminacion[0]['fecha_fin_programa']);
		$fecha_validacion_sol = InfoSolicitudProgramas::fechaValidaSolicitudSupervisor($datos_carta_terminacion[0]['folio']);

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', 0, '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.trim($datos_carta_terminacion[0]['path_carpeta']).'/banner_inferior_'.trim($datos_carta_terminacion[0]['no_empresa']).'/'.trim($datos_carta_terminacion[0]['banner_inferior']).'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Terminación');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirCartaTerminacion', array(
						  'datos_carta_terminacion' => $datos_carta_terminacion,
						  'fec_reporte' => $fec_reporte,
						  'fec_inicio' => $fec_inicio,
						  'fec_fin' => $fec_fin,
						  'fecha_validacion_sol' => $fecha_validacion_sol
			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Terminacion.pdf', 'I');
	}

	public function actionImprimirCartaHistoricaAceptacion($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		/*$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');*/

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		$query = "
		select
		(ss.id_servicio_social) as folio, --tome el id
		(select extract(year from ss.fecha_registro)) as anio_servicio_social,
		ss.fecha_registro,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcDirector\") as grado_max_estudios_director,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcDirector\") as director,
		(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1) as institucion,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as grado_max_estudios_jefvinculacion,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as jefe_vinculacion,
		(select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as nombre_alumno,
		(select esp.\"dscEspecialidad\" from public.\"E_datosAlumno\" eda
		join public.\"E_especialidad\" esp
		on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
		where eda.\"nctrAlumno\" = ss.no_ctrl) as carrera_alumno,
		ss.no_ctrl,
		pss.nombre_programa,
		ss.\"departamentoSupervisorJefe\" as depto_supjefe,
		pss.horas_totales,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		(select horas_totales from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = pss.id_programa limit 1) as horasdiarias,
		(case
		when pss.id_periodo_programa = 1 then 'seis'
		when pss.id_periodo_programa = 2 then 'doce'
		when pss.id_periodo_programa = 3 then 'dos'
		end) as periodo_programa,
		ss.nombre_jefe_depto,
		spss.id_solicitud_programa as validacion_jefesup, --con el id obtenemos le fecha de validacion del supervisor y formatear
		spss.id_estado_solicitud_programa_supervisor as estatus_validacion_validacion_jefesup,
		(select banner_superior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_superior,
		(select banner_inferior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_inferior,
		(select path_carpeta from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) path_carpeta,
		(select id_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as no_empresa
		from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_solicitud_programa_servicio_social spss
		on spss.id_solicitud_programa = ss.id_servicio_social
		where ss.id_servicio_social = '$id_servicio_social'
		";

		$datos_carta_aceptacion = Yii::app()->db->createCommand($query)->queryAll();

    	//Fecha de creacion del Reporte
		$fec_reporte = $this->getFormatoFechaReportes($datos_carta_aceptacion[0]['fecha_registro']);

		//fecha inicio y fin
		$fecha_inicio = $this->getFormatoFecha($datos_carta_aceptacion[0]['fecha_inicio_programa']);
		$fecha_fin = $this->getFormatoFecha($datos_carta_aceptacion[0]['fecha_fin_programa']);
		$fecha_val_sol = InfoSolicitudProgramas::fechaValidaSolicitudSupervisor($datos_carta_aceptacion[0]['validacion_jefesup']);

		//Pasamos a entero las horas_totales diarias
		$horas_diarias = $this->getHorasDiariasPrograma($datos_carta_aceptacion[0]['horasdiarias']);

		$leyenda_reporte_pdf = $this->getLeyendaReportePDF();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.trim($datos_carta_aceptacion[0]['path_carpeta']).'/banner_inferior_'.trim($datos_carta_aceptacion[0]['no_empresa']).'/'.trim($datos_carta_aceptacion[0]['banner_inferior']).'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Aceptacion');
		# render (full page)
		$mPDF1->WriteHTML(
			$this->render('imprimirCartaAceptacion', array(
						  //'fecha_actual' => $this->getFormatoFecha(date('Y-m-d')),
						  'fec_reporte' => $fec_reporte,
						  'datos_carta_aceptacion' => $datos_carta_aceptacion,
						  'fecha_inicio' => $fecha_inicio,
						  'fecha_fin' => $fecha_fin,
						  'horas_diarias' => $horas_diarias,
						  //'meses' => $meses,
						  'fecha_val_sol' => $fecha_val_sol,
						  'anio' => $this->getAnio(),
						  'leyenda_reporte_pdf' => $leyenda_reporte_pdf
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Aceptacion.pdf', 'I');
	}

	public function actionImprimirCartaAceptacion($id_servicio_social)
	{
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoSolicitudProgramas.php';

		$query = "
		select
		(ss.id_servicio_social) as folio, --tome el id
		(select extract(year from ss.fecha_registro)) as anio_servicio_social,
		ss.fecha_registro,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcDirector\") as grado_max_estudios_director,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcDirector\") as director,
		(select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1) as institucion,
		(select \"siglaGradoMaxEstudio\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as grado_max_estudios_jefvinculacion,
		(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = ss.\"rfcJefeDeptoVinculacion\") as jefe_vinculacion,
		(select \"nmbAlumno\" from public.\"E_datosAlumno\" where \"nctrAlumno\" = ss.no_ctrl) as nombre_alumno,
		(select esp.\"dscEspecialidad\" from public.\"E_datosAlumno\" eda
		join public.\"E_especialidad\" esp
		on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
		where eda.\"nctrAlumno\" = ss.no_ctrl) as carrera_alumno,
		ss.no_ctrl,
		pss.nombre_programa,
		ss.\"departamentoSupervisorJefe\" as depto_supjefe,
		pss.horas_totales,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		(select horas_totales from pe_planeacion.ss_horario_dias_habiles_programas where id_programa = pss.id_programa limit 1) as horasdiarias,
		(case
		when pss.id_periodo_programa = 1 then 'seis'
		when pss.id_periodo_programa = 2 then 'doce'
		when pss.id_periodo_programa = 3 then 'dos'
		end) as periodo_programa,
		ss.nombre_jefe_depto,
		spss.id_solicitud_programa as validacion_jefesup, --con el id obtenemos le fecha de validacion del supervisor y formatear
		spss.id_estado_solicitud_programa_supervisor as estatus_validacion_validacion_jefesup,
		(select banner_superior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_superior,
		(select banner_inferior from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as banner_inferior,
		(select path_carpeta from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) path_carpeta,
		(select id_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = pss.id_unidad_receptora) as no_empresa
		from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_solicitud_programa_servicio_social spss
		on spss.id_solicitud_programa = ss.id_servicio_social
		where ss.id_servicio_social = '$id_servicio_social'
		";

		$datos_carta_aceptacion = Yii::app()->db->createCommand($query)->queryAll();

    	//Fecha de creacion del Reporte
		$fec_reporte = $this->getFormatoFechaReportes($datos_carta_aceptacion[0]['fecha_registro']);

		//fecha inicio y fin
		$fecha_inicio = $this->getFormatoFecha($datos_carta_aceptacion[0]['fecha_inicio_programa']);
		$fecha_fin = $this->getFormatoFecha($datos_carta_aceptacion[0]['fecha_fin_programa']);
		$fecha_val_sol = InfoSolicitudProgramas::fechaValidaSolicitudSupervisor($datos_carta_aceptacion[0]['validacion_jefesup']);

		//Pasamos a entero las horas_totales diarias
		$horas_diarias = $this->getHorasDiariasPrograma($datos_carta_aceptacion[0]['horasdiarias']);

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		$mPDF1->showImageErrors = true;
		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		//$mPDF1->SetWatermarkImage('https://i.imgur.com/DSQ1OM3.png', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//$mPDF1->watermarkImageAlpha = 0.5;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		//Footer
		//$mPDF1->SetHTMLFooter('<img src="images/encabezados_empresas/'.trim($datos_carta_aceptacion[0]['path_carpeta']).'/banner_inferior_'.trim($datos_carta_aceptacion[0]['no_empresa']).'/'.trim($datos_carta_aceptacion[0]['banner_inferior']).'"/>');
		//$mPDF1->SetHTMLFooter('<img src="images/servicio_social/banner_inf.jpg');
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		//Titulo
		$mPDF1->SetTitle('Carta de Aceptacion');
		# render (full page)
		$mPDF1->WriteHTML(
			$this->render('imprimirCartaAceptacion', array(
						  //'fecha_actual' => $this->getFormatoFecha(date('Y-m-d')),
						  'fec_reporte' => $fec_reporte,
						  'datos_carta_aceptacion' => $datos_carta_aceptacion,
						  'fecha_inicio' => $fecha_inicio,
						  'fecha_fin' => $fecha_fin,
						  'horas_diarias' => $horas_diarias,
						  'fecha_val_sol' => $fecha_val_sol
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Carta Aceptacion.pdf', 'I');
	}

	/*public function mesesXPeriodoprograma($id_periodo_programa)
	{
		switch($id_periodo_programa)
		{
			case 1: return "seis"; break;
			case 2: return "doce"; break;
			case 3: return "dos"; break;
		}
	}*/

	public function getHorasDiariasPrograma($horas_totales)
	{
		//Obtenemos solo las horas
		$hour = explode( ':', $horas_totales);

		return intval($hour[0]);

	}

	/*Formato fecha 27/ENERO/2018*/
	public function getFormatoFechaReportes($fecha)
	{
		$fechats = strtotime($fecha); //pasamos a timestamp

		//Obtenemos el dia, mes y año
		$dia = date('d', $fechats);
		$mes = $this->getMes(date('m', $fechats));
		$anio = date('Y', $fechats);

		return $dia."/".$mes."/".$anio;
	}
	/*Formato fecha 27/ENERO/2018*/

	/*Damos formato a la fecha*/
	public function getFormatoFecha($fecha)
	{
		$fechats = strtotime($fecha); //pasamos a timestamp

		//Obtenemos el dia, mes y año
		$dia = date('d', $fechats);
		$mes = $this->getMes(date('m', $fechats));
		$anio = date('Y', $fechats);

		return $dia." de ".$mes." de ".$anio;
	}
	/*Damos formato a la fecha*/

	public function getMes($mes)
	{
		$_mes="";

		switch ($mes)
		{
			case 1: $_mes = "ENERO"; break;
			case 2: $_mes = "FEBRERO"; break;
			case 3: $_mes = "MARZO"; break;
			case 4: $_mes = "ABRIL"; break;
			case 5: $_mes = "MAYO"; break;
			case 6: $_mes = "JUNIO"; break;
			case 7: $_mes = "JULIO"; break;
			case 8: $_mes = "AGOSTO"; break;
			case 9: $_mes = "SEPTIEMBRE"; break;
			case 10: $_mes = "OCTUBRE"; break;
			case 11: $_mes = "NOVIEMBRE"; break;
			case 12: $_mes = "DICIEMBRE"; break;
			default: $_mes = "DESCONOCIDO"; break;
		}

		return $_mes;
	}

	/*Se puede enviar el no control del alumno para verificar bien el servicio social
	a editar su status*/
	public function actionStatusServicioSocialAlumno($id_servicio_social)
	{
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK($modelSSServicioSocial->no_ctrl);
		if($modelSSHistoricoTotalesAlumnos === null)
			throw new CHttpException(404,'No existen datos de Servicio Social de ese alumno.');

		/*Lista de programas */
		$lista_programas = $this->getProgramas();

		/*Lista estados servicio social */
		$lista_edos_servicio = $this->getEstadosServicioSocial();

		/*Estado actual del servicio */
		$estado_servicio_social = $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social);

		if(isset($_POST['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_POST['SsServicioSocial'];

			if($modelSSServicioSocial->save())
			{
				/*Una vez que cancele el servicio social del alumno, tambien su solicitud al programa */
				$criteria = new CDbCriteria;
				$criteria->condition = "no_ctrl = '$modelSSServicioSocial->no_ctrl' AND id_estado_solicitud_programa = 2";
				$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->find($criteria);

				$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa = 4;
				$modelSSSolicitudProgramaServicioSocial->save();
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaAdminServicioSocial'));

			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
			}
		}

		$this->render('statusServicioSocialAlumno',array(
					  'nombre_alumno' => $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl),
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'lista_edos_servicio' => $lista_edos_servicio,
					  'estado_servicio_social' => $estado_servicio_social,
					  'lista_programas' => $lista_programas
        ));

	}

	public function actionListaDeptoObservacionesServicioSocial()
	{
		$modelSSServicioSocial = new SsServicioSocial_('search');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

        $this->render('listaDeptoObservacionesServicioSocial',array(
            		'modelSSServicioSocial'=>$modelSSServicioSocial,
        ));
	}

	public function actionListaSupervisorObservacionesServicioSocial()
	{
		//Tomar del inicio de sesion del alumno en el SII
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		$modelSSServicioSocial = new SsServicioSocial_('search');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		$Programas = $this->getIDprogramaVigenteSupervisor($rfcSupervisor);

		if($Programas != NULL)
		{
			/*Verificar el numero de lugares disponibles que tiene para el programa*/
			$id_tipo_programa = $Programas[0]['id_tipo_programa'];
			$id_programa = $Programas[0]['id_programa'];


		}else{

			$id_programa = 0;
			$id_tipo_programa = $this->isSupervisorInternoOExterno($rfcSupervisor);
		}

		if(isset($_GET['ObservacionesServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['ObservacionesServicioSocial'];
		}

        $this->render('listaSupervisorObservacionesServicioSocial',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'rfcSupervisor' => $rfcSupervisor,
					  'id_tipo_programa' => $id_tipo_programa,
					  'id_programa' => $id_programa
        ));
	}

	//Saber si el supervisor es interno o externo
	public function isSupervisorInternoOExterno($rfcSupervisor)
	{
		$id_tipo_programa;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != null)
		{
			$id_tipo_programa = 1;

		}else{
			$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);
			$id_tipo_programa = ($modelSSSupervisoresProgramas != NULL) ? 2 : 0;
		}

		return $id_tipo_programa;
	}

	public function actionHistoricoAlumnoServicioSocial()
	{

		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		$modelSSServicioSocial = new SsServicioSocial_('searchXHistoricoAlumno');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK($no_ctrl);

		//Verificamos si hay registros en servicios sociales pasados
		$hayRegServicioSocialPasados = $this->getServiciosSocialesPasados($no_ctrl);
		$modelSSServiciosSocialesAnteriores = new SsServiciosSocialesAnteriores_('search');
		$modelSSServiciosSocialesAnteriores->unsetAttributes();  // clear any default values


		if($modelSSHistoricoTotalesAlumnos === null)
		{
			$modelSSHistoricoTotalesAlumnos = new SsHistoricoTotalesAlumnos;
			$modelEDatosAlumno = EDatosAlumno::model()->findByPk($no_ctrl);

			if($modelEDatosAlumno === NULL)
			{
				$nombre_alumno = null;
				$carrera = null;
				$semestre = null;

			}else{

				$nombre_alumno = $modelEDatosAlumno->nmbAlumno;
				$carrera = $this->getCarreraAlumno($modelEDatosAlumno->nctrAlumno);
				$semestre = $modelEDatosAlumno->semAlumno;

			}

		}else{

			$nombre_alumno = $this->getNameCompletoAlumno($modelSSHistoricoTotalesAlumnos->no_ctrl);
			$carrera = $this->getCarreraAlumno($modelSSHistoricoTotalesAlumnos->no_ctrl);
			$semestre = $this->getSemestre($modelSSHistoricoTotalesAlumnos->no_ctrl);
		}

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('historicoAlumnoServicioSocial',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
					  'nombre_alumno' => $nombre_alumno,
					  'carrera' => $carrera,
					  'semestre' => $semestre,
					  'horas_servicio_social' => $this->getHorasTotalesServicioSocial(),
					  'no_ctrl' => $no_ctrl,
					  'modelSSServiciosSocialesAnteriores' => $modelSSServiciosSocialesAnteriores,
					  'hayRegServicioSocialPasados' => $hayRegServicioSocialPasados

		));
	}

	public function getServiciosSocialesPasados($no_ctrl)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "no_ctrl = '$no_ctrl' ";
		$modelSSServiciosSocialesAnteriores = SsServiciosSocialesAnteriores::model()->findAll($criteria);

		return ($modelSSServiciosSocialesAnteriores != NULL) ? true : false;
	}

	/*Historico Cartas Servicio Social Vista Alumno*/
	public function actionCartasHistoricoAlumnoServicioSocial($id_servicio_social)
	{

		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		$modelSSServicioSocial =new SsServicioSocial_('search');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		//Obtener el tipo de servicio social
		$ServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		if($ServicioSocial === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Servicio Social.');

		$modelSSProgramas = SsProgramas::model()->findByPk($ServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Programa.');

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('cartasHistoricoAlumnoServicioSocial',array(
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'id_servicio_social' => $ServicioSocial->id_servicio_social,
					  'no_ctrl' => $no_ctrl,
					  'id_tipo_programa' => $modelSSProgramas->id_tipo_programa
		));
	}
	/*Historico Cartas Servicio Social Vista Alumno*/

	public function actionCartasHistoricoEscolaresAlumnoServicioSocial($id_servicio_social, $no_ctrl)
	{
		$modelSSServicioSocial = new SsServicioSocial_('search');
   		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		//Obtener el tipo de servicio social
		//$modelSSServicioSocial =new SsServicioSocial;
		$ServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		if($ServicioSocial === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Servicio Social.');

		$modelSSProgramas = SsProgramas::model()->findByPk($ServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Programa.');

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('cartasHistoricoEscolaresAlumnoServicioSocial',array(
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'id_servicio_social' => $ServicioSocial->id_servicio_social,
					  'no_ctrl' => $no_ctrl,
					  'id_tipo_programa' => $modelSSProgramas->id_tipo_programa
		));
	}

	/*Historico Cartas Servicio Social Vista Admin*/
	public function actionCartasHistoricoAdminServicioSocial($id_servicio_social, $no_ctrl)
	{
		$modelSSServicioSocial = new SsServicioSocial_('search');
    	$modelSSServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

        $this->render('cartasHistoricoAdminServicioSocial',array(
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'id_servicio_social' => $id_servicio_social,
					  'no_ctrl' => $no_ctrl
        ));
	}
	/*Historico Cartas Servicio Social Vista Admin*/

	public function actionListaCandidatosALiberarServicioSocial($id_estado_servicio_social = null)
	{
		$modelSSServicioSocial = new SsServicioSocial_('searchServicioSocialXReportesEvaluadosYValidados');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		//Estado servicio social
		$lista_estado = $this->getEstadoCompletadoServicioSocial(); //Pasar a Completado

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes = $_GET['SsServicioSocial'];
			
		}

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		//Si se pasan a estado Completado (5) entonces entra y realiza la actualizacion
		if($id_estado_servicio_social != NULL)
		{
			try
			{
				//Se podria validar que el servicio social fuera del registro de servicio social vigente
				$qry_comp = "update pe_planeacion.ss_servicio_social
								SET id_estado_servicio_social = 5
								where id_estado_servicio_social = 4 ";

				if(Yii::app()->db->createCommand($qry_comp)->queryAll())
				{
					Yii::app()->user->setFlash('success', "Servicio Sociales Validados Correctamente!!!");
					//Si se realizo correctamente el cambio
					$transaction->commit();

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Validar los Servicios Sociales.");
					//Si se realizo correctamente el cambio
					$transaction->rollback();
					
				}
			}catch(Exception $e)
			{
				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Validar los Servicios Sociales.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				
			}
		}

        $this->render('listaCandidatosALiberarServicioSocial',array(
            		  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'lista_estado' => $lista_estado,
					  'id_estado_servicio_social' => $id_estado_servicio_social
        ));
	}

	/*Se libera el servicio social y se agrega la calificacion final y las horas liberadas */
	public function actionLiberarServicioSocialAlumno($id_servicio_social)
	{
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);
		$edo_actual_servicio = $modelSSServicioSocial->id_estado_servicio_social;
		//Obtenemos los estados actuales del Servicio Social para finalizar el Servicio Social
		$lista_estados = $this->getEstadosLiberacionServicioSocial();

		$criteria = new CDbCriteria();
		$criteria->condition = "no_ctrl = '$modelSSServicioSocial->no_ctrl' ";
		$modelSSSolicitudProgramaServicioSocial = SsSolicitudProgramaServicioSocial::model()->find($criteria);
		if($modelSSSolicitudProgramaServicioSocial === null)
			throw new CHttpException(404,'No existen datos de ese Solicitud.');

		//Obtenemos la Carrera del alumno (todo por si ocupamos mas info del alumno)
		$modelAlumnos = EDatosAlumno::model()->findByPK($modelSSServicioSocial->no_ctrl);
		if($modelAlumnos === null)
			throw new CHttpException(404,'No existen datos de ese alumno con ese Servicio Social.');

		//Especialidad del Alumno
		$modelEEspecialidad = EEspecialidad::model()->findByPK($modelAlumnos->cveEspecialidadAlu);
		if($modelEEspecialidad === null)
			throw new CHttpException(404,'No existen datos de esa Especialidad.');

		$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de ese Programa.');

		//Traemos sus reportes bimestrales
		$criteria2 = new CDbCriteria();
		$criteria2->condition = " id_servicio_social = '$modelSSServicioSocial->id_servicio_social' ";
		$criteria2->order = "id_reporte_bimestral ASC";
		$modelSSReportesBimestral = SsReportesBimestral::model()->findAll($criteria2);

		if($modelSSReportesBimestral === null)
		{
			throw new CHttpException(404,'No existen datos de ese registro de reporte bimestral.');

		}else{
			$modelSSReportesBimestral = new SsReportesBimestral_('searchReportesXServicioSocialInterno');
			$modelSSReportesBimestral->unsetAttributes();  // clear any default values

			if(isset($_GET['SsReportesBimestral']))
				$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];

		}

		if(isset($_POST['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_POST['SsServicioSocial'];

			if($modelSSServicioSocial->save())
			{
				/*Si no hubo problemas de insercion de datos. Se agregan la calificacion final al servicio social y las horas liberadas*/
				$this->liberarActualServicioSocialAlumno($modelSSServicioSocial->id_servicio_social, $modelSSProgramas);
				/*Verificar si ya completo las 480 horas*/
				$this->verificarHorasCompletadas($modelSSServicioSocial->no_ctrl);
				/*Cambia a estado Finalizado la solicitud del Programa para saber los Servicio Sociales COMPLETADOS*/
				$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 4;
				$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 4;
				$modelSSSolicitudProgramaServicioSocial->save();
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!!!');
				$this->redirect(array('listaCandidatosALiberarServicioSocial'));

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
			}
		}

		$this->render('liberarServicioSocialAlumno',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'modelAlumnos' => $modelAlumnos,
					  'nombre_alumno' => $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl),
					  'modelEEspecialidad' => $modelEEspecialidad,
					  'no_ctrl' => $modelSSServicioSocial->no_ctrl,
					  'id_servicio_social' => $modelSSServicioSocial->id_servicio_social,
					  'estado_servicio_social' => $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social),
					  'lista_estados' => $lista_estados,
					  'edo_actual_servicio' => $edo_actual_servicio,
					  'modelSSProgramas' => $modelSSProgramas,
					  'id_tipo_programa' => $modelSSProgramas->id_tipo_programa,
					  'modelSSReportesBimestral' => $modelSSReportesBimestral,
      ));

	}
	/*Se libera el servicio social y se agrega la calificacion final y las horas liberadas */

	public function liberarActualServicioSocialAlumno($id_servicio_social, $modelSSProgramas)
	{
		$divisor = 0;
		/*Entre cuanto se dividira el servicio social*/
		/*Obtenemos el periodo del servicio social Semestral(1), Anual(2) y Una vez(1)*/
		switch($modelSSProgramas->id_periodo_programa)
		{
			case 1: $divisor = 4;	break; //Semestral (4 reportes)
			case 2: $divisor = 7;	break; //Anual (7 reportes)
			case 3: $divisor = 2;	break; //Unica Vez (2 reportes)
		}

		/*Obtenemos los calificaciones de los reportes del servicio social correspondiente*/
		$query = "
		select SUM(calificacion_reporte_bimestral) as total from pe_planeacion.ss_reportes_bimestral
		where id_servicio_social = '$id_servicio_social'
		";

		$total = Yii::app()->db->createCommand($query)->queryAll();

		//Asignamos calificacion final del servicio social actual
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);
		$modelSSServicioSocial->calificacion_servicio_social = intval($total[0]['total'] / $divisor);
		$modelSSServicioSocial->save();

		/*Obtenemos las horas completadas para hacer la sumatoria entre las que ha completado y completo*/
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($modelSSServicioSocial->no_ctrl);

		if($modelSSHistoricoTotalesAlumnos === null)
		{
			throw new CHttpException(404,'No existe ningun registro del alumno con ese No. de Control.');
		}else{

			$horas_completadas = $modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social;//400
			$modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social = ($horas_completadas + $modelSSProgramas->horas_totales);
			$modelSSHistoricoTotalesAlumnos->fecha_modificacion_horas_totales = date('Y-m-d H:i:s');
			$modelSSHistoricoTotalesAlumnos->save();

		}

	}

	public function verificarHorasCompletadas($no_ctrl)
	{

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);
		if($modelSSHistoricoTotalesAlumnos === NULL)
			throw new CHttpException(404,'No existe registro de Servicio Social del alumno con ese No. de Control.');

		/*Validar cuando haya completado las 480 horas y poder calcular su calificacion final de servicio social*/
		if($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= $this->getHorasServicioSocial())
		{
			/*Se actualiza el campo servicio social completado*/
			$horas = $this->getHorasServicioSocial();
			$calif = $this->getCalificacionServicioSocialFinal($no_ctrl);

			if($horas != NULL AND $calif != NULL)
			{
				$query = "Update pe_planeacion.ss_historico_totales_alumnos
						SET horas_totales_servicio_social = '$horas',
						calificacion_final_servicio_social = '$calif',
						completo_servicio_social = true
						where no_ctrl = '$no_ctrl'
									";

				if(!Yii::app()->db->createCommand($query)->queryAll()){
				  die('Error al guardar los datos');
				}
			}
		}//Fin de if si ya completo las 480 horas de servicio social
	}

	public function getHorasServicioSocial()
	{
		$query = "select horas_max_servicio_social
		from pe_planeacion.ss_configuracion where id_configuracion = 1
		";

		$horas = Yii::app()->db->createCommand($query)->queryAll();

		return $horas[0]['horas_max_servicio_social'];
	}

	public function getCalificacionServicioSocialFinal($no_ctrl)
	{
		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicio_social */
		$query1 = "SELECT (CASE WHEN sum(calificacion_servicio_social) = 0
						THEN 0 ELSE sum(calificacion_servicio_social) END) as suma1
						FROM pe_planeacion.ss_servicio_social
						WHERE no_ctrl = '$no_ctrl' AND id_estado_servicio_social = 6 ";

		$total1 = Yii::app()->db->createCommand($query1)->queryAll();

		/*NUMERO DE TODOS LOS SERVICIOS SOCIALES QUE COMPLETO */
		$query2 ="SELECT (CASE WHEN COUNT(id_servicio_social) = 0
						THEN 0 ELSE COUNT(id_servicio_social) END) as div1
						FROM pe_planeacion.ss_servicio_social
						WHERE no_ctrl = '$no_ctrl' AND id_estado_servicio_social = 6 ";

		$div1 = Yii::app()->db->createCommand($query2)->queryAll();
		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicio_social */

		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicios_sociales_anteriores */
		$query3 = "SELECT (CASE WHEN sum(calificacion_servicio_social) = 0
						THEN 0 ELSE sum(calificacion_servicio_social) END) as suma2
						FROM pe_planeacion.ss_servicios_sociales_anteriores
						WHERE no_ctrl = '$no_ctrl' ";

		$total2 = Yii::app()->db->createCommand($query3)->queryAll();

		/*NUMERO DE TODOS LOS SERVICIOS SOCIALES QUE COMPLETO (ANTERIORES)*/
		$query4 = "SELECT (CASE WHEN COUNT(id_servicio_social_anterior) = 0
						THEN 0 ELSE COUNT(id_servicio_social_anterior) END) as div2
						FROM pe_planeacion.ss_servicios_sociales_anteriores
						WHERE no_ctrl = '$no_ctrl' ";

		$div2 = Yii::app()->db->createCommand($query4)->queryAll();
		/*CALIFICACIONES DE TODOS SUS SERVICIOS SOCIALES DEL ALUMNO, tabla ss_servicios_sociales_anteriores */

		return (int)($total1[0]['suma1'] + $total2[0]['suma2']) / ($div1[0]['div1'] + $div2[0]['div2']);
	}

	public function actionDetalleLiberacionServicioSocialAlumno($id_servicio_social)
	{
		$id_reportes = array();
		//Nos traemos los datos del Servicio Social
		$modelSSServicioSocial = $this->loadModel($id_servicio_social);

		//Nos traemos los datos del Programa Actual del Alumno
		$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'No existen datos de ese programa.');

		//Obtenemos la Carrera del alumno (todo por si ocupamos mas info del alumno)
		$modelAlumnos = EDatosAlumno::model()->findByPK($modelSSServicioSocial->no_ctrl);
		if($modelAlumnos === null)
			throw new CHttpException(404,'No existen datos de ese alumno con ese Servicio Social.');

		//Especialidad del Alumno
		$modelEEspecialidad = EEspecialidad::model()->findByPK($modelAlumnos->cveEspecialidadAlu);
		if($modelEEspecialidad === null)
			throw new CHttpException(404,'No existen datos de esa Especialidad.');

		//Historico de Horas liberdadas de Servicio Social
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK($modelSSServicioSocial->no_ctrl);
		if($modelSSHistoricoTotalesAlumnos === null)
			throw new CHttpException(404,'No existen datos de ese registro de horas totales.');

		//Obtenemos el horario del programa para mostrarlo
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		if($modelSSHorarioDiasHabilesProgramas === null)
			throw new CHttpException(404,'No existen datos de ese horario.');

		$this->render('detalleLiberacionServicioSocialAlumno', array(
			'nombre_alumno' => $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl),
			'no_ctrl' => $modelSSServicioSocial->no_ctrl,
			'modelEEspecialidad' => $modelEEspecialidad,
			'modelSSHistoricoTotalesAlumnos' => $modelSSHistoricoTotalesAlumnos,
			'modelSSServicioSocial' => $modelSSServicioSocial,
			'modelAlumnos' => $modelAlumnos,
			'estado_servicio_social' => $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social),
			'modelSSProgramas' => $modelSSProgramas,
			'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,

			//'modelSSExpedientesReportesBimestralesServicioSocialExterno' => $modelSSExpedientesReportesBimestralesServicioSocialExterno
   		));

	}

	public function actionListaAdminActividades()
	{
		$modelSSServicioSocial = new SsServicioSocial_('searchXAlumnosAltaServSocial');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('listaAdminActividades',array(
            		'modelSSServicioSocial'=>$modelSSServicioSocial,
        ));
	}

	public function actionListaSupervisorActividades()
	{
		$modelSSServicioSocial = new SsServicioSocial_('searchXObservacionesPrograma');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		//Tomar del inicio de sesion del alumno en el SII
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		$Programas = $this->getIDprogramaVigenteSupervisor($rfcSupervisor);

		if($Programas != NULL)
		{
			/*Verificar el numero de lugares disponibles que tiene para el programa*/
			$id_programa = $Programas[0]['id_programa'];
			$id_tipo_programa = $Programas[0]['id_tipo_programa'];

		}else{

			$id_programa = 0;
			$id_tipo_programa = $this->isSupervisorInternoOExterno($rfcSupervisor);
		}

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('listaSupervisorActividades',array(
					  'modelSSServicioSocial'=>$modelSSServicioSocial,
					  'id_servicio_social' => $modelSSServicioSocial->id_servicio_social,
					  'rfcSupervisor' => $rfcSupervisor,
					  'id_tipo_programa' => $id_tipo_programa,
					  'id_programa' => $id_programa
        ));
	}

	public function actionCartaTerminacionAlumnoServicioSocial()
	{
		
		//Tomar del inicio de sesion del alumno en el SII
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		$modelSSServicioSocial = new SsServicioSocial_('searchXAlumnosAltaServSocial');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}

		$this->render('cartaTerminacionAlumnoServicioSocial',array(
					'modelSSServicioSocial'=>$modelSSServicioSocial,
					'no_ctrl' => $no_ctrl
        ));
	}

	public function getIDprogramaVigenteSupervisor($rfcSupervisor)
	{
		$interno = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$externo = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

		$result1 = Yii::app()->db->createCommand($interno)->queryAll();
		$result2 = Yii::app()->db->createCommand($externo)->queryAll();

		if($result1 != NULL)
		{
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$ProgramaServicioSocial = Yii::app()->db->createCommand($query)->queryAll();

		}else
		if($result2 != NULL){
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			where rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$ProgramaServicioSocial = Yii::app()->db->createCommand($query)->queryAll();
		}else{
			$ProgramaServicioSocial = array();
		}

		return $ProgramaServicioSocial;
	}

	public function loadModel($id)
	{
		$modelSSServicioSocial=SsServicioSocial::model()->findByPk($id);

		if($modelSSServicioSocial===null)
		{
			throw new CHttpException(404,'No existen datos de ese Servicio Social.');
		}

		return $modelSSServicioSocial;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsServicioSocial $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*********************************GETTERS Y SETTERS/*********************************/
	public function getProgramas()
	{

		$criteria = new CDbCriteria();
		$criteria->condition = "id_programa > 0 ";
		$modelProgramasServicioSocial = SsProgramas::model()->findAll($criteria);

		$lista_programas= CHtml::listData($modelProgramasServicioSocial, "id_programa", "nombre_programa");

		return $lista_programas;
	}

	public function getProgramasDisponibles()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_status_programa = 1 ";
		$modelProgramasServicioSocial = SsProgramas::model()->findAll($criteria);

		$lista_programas= CHtml::listData($modelProgramasServicioSocial, "id_programa", "nombre_programa");

		return $lista_programas;
	}

	public function getEstadosNoHistoricosServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social != 1 AND id_estado_servicio_social != 5";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_estados= CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_estados;
	}

	public function getEstadosHistoricosServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = 6 OR id_estado_servicio_social = 7";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_estados = CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_estados;
	}

	//Lista de todos los estados que puede tener el servicio social
	public function getEstadosServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social > 0 ";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_estados= CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_estados;
	}

	//Para mostrarlo como informacion del alumno
	public function getEstadoServicioSocial($id_estado_servicio_social)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = '$id_estado_servicio_social' ";

		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->find($criteria);

		return strtoupper($modelSSEstadoServicioSocial->estado);
	}

	/*Devuelve el nombre completo del alumno */
	public function getNameCompletoAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";
		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		return $modelEDatosAlumno->nmbAlumno;
	}

	/*Estados del servicio social*/
	public function getEstado($id_estado_servicio_social)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = '$id_estado_servicio_social' ";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$edo_servicio = Chtml::listData($modelSSEstadoServicioSocial,'id_estado_servicio_social','estado');

		return $edo_servicio;
	}

	public function getPeriodos()
	{

		$criteria = new CDbCriteria();
		$criteria->condition = "id_periodo_programa > 0 ";
		$modelSSPeriodosProgramas = SsPeriodosProgramas::model()->findAll($criteria);

		$lista_periodos= CHtml::listData($modelSSPeriodosProgramas, "id_periodo_programa", "periodo_programa");

		return $lista_periodos;
	}

	/*ESTO SE HIZO UN TRIGGER YA*/
	public function getCalcularFechaFinServicioSocial($tipo_servicio_social, $fecha_inicio)
	{
		$fi = new DateTime($fecha_inicio);
		$nuevafecha;

		switch($tipo_servicio_social)
		{
			//Semestral
			case 1:
				$nuevafecha = strtotime ( '+6 month' , strtotime ($fi->format('d-m-Y')));
			break;
			//Anual
			case 2:
				$nuevafecha = strtotime ( '+12 month' , strtotime ($fi->format('d-m-Y')));
			break;
			//Unica vez
			case 3:
				$nuevafecha = strtotime ( '+0 month' , strtotime ($fi->format('d-m-Y')));
			break;

		}

		//Formateamos la fecha
		$fecha_fin = date ( 'Y-m-d' , $nuevafecha );
		return $fecha_fin;

	}
	/*ESTO SE HIZO UN TRIGGER YA*/

	/*Devuelve solamente los estados para liberacion de servicio social */
	public function getEstadosLiberacionServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = 5"; //Completado
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_estados= CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_estados;
	}

	public function getEstadoCompletadoServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = 5";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_estados= CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_estados;
	}

	/*Devuelve la carrera del alumno */
	public function getCarreraAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";
		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		$criteria2 = new CDbCriteria();
		$criteria2->condition = " \"cveEspecialidad\" = '$modelEDatosAlumno->cveEspecialidadAlu' ";
		$modelEEspecialidad = EEspecialidad::model()->find($criteria2);

		return $modelEEspecialidad->dscEspecialidad;
	}

	/*Devuelve el semestre del alumno */
	public function getSemestre($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";

		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		return $modelEDatosAlumno->semAlumno;
	}

	/*Devuelve el total de horas a completar de Servicio Social */
	public function getHorasTotalesServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_configuracion = 1 ";

		$modelAlumnosVigentes = SsConfiguracion::model()->find($criteria);

		return $modelAlumnosVigentes->horas_max_servicio_social;
	}

	//Validar que el servicio social pertenezca al usuario actualmente logeado en el SII
	public function validarServicioSocial($id_servicio_social, $no_ctrl)
	{
		/*$model = SsServicioSocial::model()->findByAttributes(
			array('id_servicio_social'=>$id_servicio_social,
				  'no_ctrl'=>$no_ctrl
			)
		);*/

		$query = "select * from pe_planeacion.ss_servicio_social
		where id_servicio_social = '$id_servicio_social' and no_ctrl = '$no_ctrl'
		and (id_estado_servicio_social != 6 or id_estado_servicio_social != 7)";

		$model = Yii::app()->db->createCommand($query)->queryAll();

		return ($model != NULL) ? true : false;
	}

	public function validarHistoricoServicioSocial($id_servicio_social, $no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_servicio_social
		where id_servicio_social = '$id_servicio_social' and no_ctrl = '$no_ctrl'
		and id_estado_servicio_social = 6";

		$model = Yii::app()->db->createCommand($query)->queryAll();

		return ($model != NULL) ? true : false;
	}

	//Damos formato a la fecha de los reportes de PDF
	public function getFormatoFechaReportePDF()
	{
		$ahora = time();
		$anio = date("Y",$ahora);
		$mes = date("m",$ahora);
		$dia = date("d",$ahora);

		switch($mes)
		{
			case '01':
				$mes_format = 'Enero';
			break;
			case '02':
				$mes_format = 'Febrero';
			break;
			case '03':
				$mes_format = 'Marzo';
			break;
			case '04':
				$mes_format = 'Abril';
			break;
			case '05':
				$mes_format = 'Mayo';
			break;
			case '06':
				$mes_format = 'Junio';
			break;
			case '07':
				$mes_format = 'Julio';
			break;
			case '08':
				$mes_format = 'Agosto';
			break;
			case '09':
				$mes_format = 'Septiembre';
			break;
			case '10':
				$mes_format = 'Octubre';
			break;
			case '11':
				$mes_format = 'Noviembre';
			break;
			case '12':
				$mes_format = 'Diciembre';
			break;
		}

		return $dia.'/'.$mes_format.'/'.$anio;
	}

	/*********************************GETTERS Y SETTERS/*********************************/
}
