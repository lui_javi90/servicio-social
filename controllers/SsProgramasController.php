<?php

class SsProgramasController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaProgramasAdminServicioSocial', //Vista Admin para crear Programas siempre que no pase de la fecha de inicio de Servicio Social
								 'nuevoProgramaServicioSocialDepto', //Vista para el depo
								 'editarDeptoProgramaServicioSocial', //Editar programa Depto
								 'detalleAdminProgramaServicioSocial',
								 'eliminarProgramaServicioSocial',
								 'siHayApoyoEconomico', //Si hay apoyo economico en el Programa
								 'listaProgramasAsignacionAlumnos',
								 'asignarAlumnosProgramaServicioSocial', //Vista para asignar alumnos y las horas a liberar en los programas
								 'nuevoHorarioDiasHabilesProgramaDepto', //Vista para asignar el horario del Programa por parte del jefe de oficina de Servicio Social
								 'asignacionSupervisoresProgramasDepto',  //Vista jefe de servicio social para elegir los Supervisores a cargo del Programa
								 'asignacionSupervisoresProgramaEstraTemporal', //Vista para asignar Supervisor al Programa EstraTemporal
								 'mostrarTipoEmpresas', //mostrar solo cierto tipo de supervisores
								 'MostrarTipoApoyoEconomico',
								 'listaProgramasExtraTemporales', //Lista Programas ExtraTemporales
								 'nuevoProgramaExtraTemporal', //Programas especiales
								 'editarProgramaExtraTemporal', //Editar Programas especiales
								 'nuevoHorarioDiasHabilesProgramaExtraTemporal',
								 'detalleProgramaExtraTemporal', //Vista programas extratemporales
								 'finalizarProgramaServicioSocial', //Se finaliza el programa
								 'mostrarDetalleSupervisorExterno',
								 'mostrarDetalleSupervisorInterno',
								 'listaProgramasActivos', //Lista de Programas para cambiar de Supervisor, solo para casos especiales
								 'cambiarDeSupervisorDelPrograma', //Solo casos realmente especiales
								 'historicoProgramasServicioSocial', //Vista de los Programas que se han completado y han pasado a historico
								 'cancelarProgramaServicioSocial', //Cancelar el Programa siempre y cuando este vigente
								 'listaProgramasSinSupervisorAsignado', //Vista para asignar supervisor
								 'listaProgramasSinHorarioAsignado', //Vista para mostrar Programas que no tienen Horario alguno asignado
								 'addLugaresProgramaServicioSocial', //Agregar Lugares al programa y registrar alumnos al Programa
								 'validarAddAlumnoPrograma', //Validar que el Alumno exista por medio de su no. de control
								 'delLugaresProgramaServicioSocial', //Quitar lugares asignados a un Programa
								 'showDetalleProgramaHistServicioSocial', //Mostrar detalle Programa Historicos
								 'cancelProgramaServicioSocial', //Cancelar Programa, vista agregar Horas y alumnos
								 'listaExcelSupervisoresFaltanEvaluar', //Generar lista de Supervisores que falta de evaluar reportes bimestrales
								 'listaProgramasTodos', //Vista solo temporal para Programas que estan en el limbo
								 //'finalizarProgramaServicioSocial' //Finalizar Programa
								),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
			),
			array('allow',
				'actions'=>array('listaProgramasSupervisorServicioSocial',
								 'nuevoProgramaServicioSocialSupervisor',
								 'editarSupervisorProgramaServicioSocial',
								 'detalleSupervisorProgramaServicioSocial',
								 'siHayApoyoEconomico',
								 'nuevoHorarioDiasHabilesPrograma',
								 'asignacionSupervisoresProgramasSupervisor',
								 'mostrarTipoEmpresas',
								 'MostrarTipoApoyoEconomico',
								 'programaSupervisor', //Vista para mensaje de que el Programa esta pendiente
								 'listaHistoricoProgramasSupervisor', //Lista de Historicos de Programas del Supervisor
								 'detalleHistoricoSPrograma', //Detalle Historico de Programas
								 'detalleAlumServSocialHistoricoSPrograma', //Detalle Servicio Social con determinado ID Programa
								 'renovarProgramaServicioSocial', //Renovar el Programa para el Periodo Actual
								 'showHorarioProgramaHistorico' //Ver el Horario de un Programa pasado (cancelado o finalizado)
								),
				'roles'=>array('vinculacion_supervisor_servicio_social'),
				//'users' => array('@')
			),
			array('allow',
				'actions'=>array('listaProgramasAlumnoServicioSocial',
								 'detalleAlumProgramaServicioSocial',
								 'seleccionarProgramaServicioSocial',
								),
				'roles'=>array('alumno'),
				//'users' => array('@')
			),
			
		);
	}

	public function actionListaProgramasTodos()
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  // clear any default values

		//Lista de periodos
		$lista_periodos = $this->getPeriodosProgramas();
		
		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_GET['SsProgramas'];
		}

        $this->render('listaProgramasTodos',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'lista_periodos' => $lista_periodos
        ));
	}

	public function actionListaExcelSupervisoresFaltanEvaluar()
	{
		$qry_sup_fal_eval = " select
							rpi.\"rfcEmpleado\",
							(select \"nmbEmpleado\" from public.\"H_empleados\" where \"rfcEmpleado\" = rpi.\"rfcEmpleado\" ) as nombre,
							(select \"apellPaterno\" from public.\"H_empleados\" where \"rfcEmpleado\" = rpi.\"rfcEmpleado\" ) as apellido_paterno,
							(select \"apellMaterno\" from public.\"H_empleados\" where \"rfcEmpleado\" = rpi.\"rfcEmpleado\" ) as apellido_materno,
							(select \"mailPersonal\" from public.\"H_empleados\" where \"rfcEmpleado\" = rpi.\"rfcEmpleado\" ) as correo_personal,
							(select \"mailEmpleado\" from public.\"H_empleados\" where \"rfcEmpleado\" = rpi.\"rfcEmpleado\" ) as correo_empleado,
							p.nombre_programa,
							p.fecha_inicio_programa,
							p.fecha_fin_programa,
							(select periodo_programa from pe_planeacion.ss_periodos_programas
								where id_periodo_programa = p.id_periodo_programa ) as periodo_programa
							from pe_planeacion.ss_programas p
							join pe_planeacion.ss_responsable_programa_interno rpi
							on rpi.id_programa = p.id_programa
							where p.id_status_programa = 1 AND p.fecha_fin_programa <= now()
						";

		$titulos = array(
							'A5'=>'RFC Empleado','B5'=>'Nombre', 'C5'=>'Apellido Paterno', 'D5'=>'Apellido Materno',
							'E5'=>'Correo Personal', 'F5'=>'Correo Laboral', 'G5'=>'Nombre del Programa', 'H5'=>'Fecha Inicio Programa',
							'I5'=>'Fecha Fin Programa','J5'=>'Periodo del Programa'
							//'G5'=>'Financieros','H5'=>'Pagado','I5'=>'Clasificacion'
						);

		$result = Yii::app()->db->createCommand($qry_sup_fal_eval)->queryAll();
        $nombre = "Supervisores Faltantes Evaluacion Reportes Bimestrales";
		$titulo = "Supervisores Faltantes de Evaluar Reportes Bimestrales";
		
		$excel = new Excel();

		$excel->reporteGenerico($result,$titulos,$titulo,$nombre);
	}

	public function actionCancelProgramaServicioSocial($id_programa)
	{
		$modelSSProgramas = $this->loadModel($id_programa);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelSSProgramas->id_status_programa = 4; // 4 - CANCELADO
		$modelSSProgramas->fecha_modificacion_programa = date('Y-m-d H:i:s');

		try
		{
			if($modelSSProgramas->save())
			{
				//$this->cancelarServicioSocialesDelProgramaCancelado($modelSSProgramas->id_programa);
				Yii::app()->user->setFlash('success', "Programa Cancelado correctamente!!!");
				//Si se realizo correctamente el cambio
				$transaction->commit();
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! mo se pudo Cancelar el Programa.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelSSProgramas->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Cancelar el Programa.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelSSProgramas->getErrors() );
		}
	}

	public function actionShowHorarioProgramaHistorico($id_programa)
	{
		$this->layout='//layouts/mainVacio';

		$modelSSProgramas = $this->loadModel($id_programa);

		$model = new SsHorarioDiasHabilesProgramas_('searchXPrograma');
		$model->unsetAttributes();  // clear any default values
		
        if(isset($_GET['SsHorarioDiasHabilesProgramas']))
            $model->attributes=$_GET['SsHorarioDiasHabilesProgramas'];

		$this->render('showHorarioProgramaHistorico', array(
					  'modelSSProgramas' => $modelSSProgramas,
					  'model' => $model
		));
	}

	//Detalle Programa Historico MODAL
	public function actionShowDetalleProgramaHistServicioSocial($id_programa)
	{
		$this->layout='//layouts/mainVacio';

		$modelSSProgramas = $this->loadModel($id_programa);

		$this->render('showDetalleProgramaHistServicioSocial',array(
					  'modelSSProgramas' => $modelSSProgramas
		));
	}

	//Renovar Programa Servicio Social
	public function actionRenovarProgramaServicioSocial($id_programa)
	{
		//Obtener el no_ctrl del alumno del inicio de sesión
		//$rfc_empleado = Yii::app()->params['rfcSupervisor'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$model = $this->loadModel($id_programa);

		//NUEVO PROGRAMA (RENOVACION)
		$modelSsProgramas = new SsProgramas;
		$modelSsProgramas->nombre_programa = $model->nombre_programa;
		$modelSsProgramas->lugar_realizacion_programa = $model->lugar_realizacion_programa;
		$modelSsProgramas->horas_totales = $model->horas_totales;
		$modelSsProgramas->id_periodo_programa = $model->id_periodo_programa;
		$modelSsProgramas->numero_estudiantes_solicitados = $model->numero_estudiantes_solicitados;
		$modelSsProgramas->descripcion_objetivo_programa = $model->descripcion_objetivo_programa;
		$modelSsProgramas->id_tipo_servicio_social = $model->id_tipo_servicio_social;
		$modelSsProgramas->impacto_social_esperado = $model->impacto_social_esperado;
		$modelSsProgramas->beneficiarios_programa = $model->beneficiarios_programa;
		$modelSsProgramas->actividades_especificas_realizar = $model->actividades_especificas_realizar;
		$modelSsProgramas->mecanismos_supervision = $model->mecanismos_supervision;
		$modelSsProgramas->perfil_estudiante_requerido = $model->perfil_estudiante_requerido;
		$modelSsProgramas->id_apoyo_economico_prestador = $model->id_apoyo_economico_prestador;
		$modelSsProgramas->id_tipo_apoyo_economico = $model->id_tipo_apoyo_economico;
		$modelSsProgramas->apoyo_economico = $model->apoyo_economico;
		$modelSsProgramas->fecha_registro_programa = date('Y-m-d H:i:s'); //Como es Programa Nuevo se obtiene la fecha y hora actual
		$modelSsProgramas->fecha_modificacion_programa = $modelSsProgramas->fecha_registro_programa;
		$modelSsProgramas->id_recibe_capacitacion = $model->id_recibe_capacitacion;
		$modelSsProgramas->id_clasificacion_area_servicio_social = $model->id_clasificacion_area_servicio_social;
		$modelSsProgramas->lugares_disponibles = $modelSsProgramas->numero_estudiantes_solicitados; //Se ponen los que solicitó al inicio
		$modelSsProgramas->id_tipo_programa = $model->id_tipo_programa;
		$modelSsProgramas->id_status_programa = 6; //PENDIENTE
		$modelSsProgramas->lugares_ocupados = 0;
		$modelSsProgramas->id_unidad_receptora = $model->id_unidad_receptora;

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			//Revisar que haya registro vigente de servicio social para poder dar de alta el Programa
			if($this->hayRegistroHabilitadoServicioSocial())
			{
				//Nos traemos el ID del registro vigente de servicio social
				$id_reg_vig_ssocial = $this->getRegistroFechasServicioSocial();
				//fecha_inicio_programa
				$modelSsProgramas->fecha_inicio_programa = $this->getFechaInicioPrograma($id_reg_vig_ssocial);
				//fecha_fin_programa
				$modelSsProgramas->fecha_fin_programa = $this->getFechaTerminoProgramaAproximado($modelSsProgramas->fecha_inicio_programa, $modelSsProgramas->id_periodo_programa);
				$modelSsProgramas->id_registro_fechas_ssocial = $id_reg_vig_ssocial; //Se toma el registro (vigente)

				//Revisar que el Programa no se Renueve en el mismo registro vigente de servicio social
				if($this->renovarProgramaDiferenteRegistroVigenteServicioSocial($rfcEmpleado, $id_reg_vig_ssocial))
				{
					//Se inserta los datos del Programa
					if($modelSsProgramas->save())
					{
						//Agregar al Supervisor al Programa Renovado su RFC
						$modelSsResponsableProgramaInterno = new SsResponsableProgramaInterno;
						$modelSsResponsableProgramaInterno->id_programa = $modelSsProgramas->id_programa;
						$modelSsResponsableProgramaInterno->rfcEmpleado = $rfcEmpleado;
						$modelSsResponsableProgramaInterno->fecha_asignacion = date('Y-m-d H:i:s');
						$modelSsResponsableProgramaInterno->superv_principal_int = true;

						if($modelSsResponsableProgramaInterno->save())
						{
							//Agregar un dia al Horario del nuevo programa renovado
							$modelSsHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;
							$modelSsHorarioDiasHabilesProgramas->id_programa = $modelSsProgramas->id_programa;
							$modelSsHorarioDiasHabilesProgramas->id_dia_semana = 1; //Lunes por DEFAULT
							$hor_ini = '09:00:00';
							$modelSsHorarioDiasHabilesProgramas->hora_inicio = date('H:i:s',strtotime($hor_ini));
							$hor_fin = '17:00:00';
							$modelSsHorarioDiasHabilesProgramas->hora_fin = date('H:i:s',strtotime($hor_fin));
							$modelSsHorarioDiasHabilesProgramas->horas_totales = $this->getTotalHorasXDia($modelSsHorarioDiasHabilesProgramas->hora_inicio, $modelSsHorarioDiasHabilesProgramas->hora_fin);

							if($modelSsHorarioDiasHabilesProgramas->save())
							{
								Yii::app()->user->setFlash('success', 'El Programa se renovó correctamente!!!');
								$transaction->commit();
								echo CJSON::encode( [ 'code' => 200 ] );

							}else{

								Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al Renovar el Horario del Programa.');
								//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
								$transaction->rollback();
								echo CJSON::encode( $modelSsHorarioDiasHabilesProgramas->getErrors() );
							}

						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al Agregar Supervisor al Programa.');
							//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
							$transaction->rollback();
							echo CJSON::encode( $modelSsResponsableProgramaInterno->getErrors() );
						}

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al Renovar el Programa.');
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
						echo CJSON::encode( $modelSsProgramas->getErrors() );
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al Renovar el Programa.');
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode( $modelSsProgramas->getErrors() );
				}

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! No hay registro de Servicio Social Vigente.');
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelSsProgramas->getErrors() );
			}

		}catch(Exception $e){

			Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al Renovar el Programa Seleccionado.');
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelSsProgramas->getErrors() );
		}

	}

	public function renovarProgramaDiferenteRegistroVigenteServicioSocial($rfcEmpleado, $id_registro_fechas_ssocial)
	{
		$qry_programa_sup = "select p.id_programa from pe_planeacion.ss_programas p
								join pe_planeacion.ss_responsable_programa_interno rpi
								on rpi.id_programa = p.id_programa
								where p.id_registro_fechas_ssocial = '$id_registro_fechas_ssocial' AND 
								rpi.\"rfcEmpleado\" = '$rfcEmpleado'
							";

		$curso_programa = Yii::app()->db->createCommand($qry_programa_sup)->queryAll();

		return (sizeof($curso_programa) > 0) ? false : true;

	}

	//Detalle del Servicio con todos los documentos digitales del Alumno
	public function actionDetalleAlumServSocialHistoricoSPrograma($id_servicio_social){ }
 
	//Historico de Programas del Supervisor
	public function actionListaHistoricoProgramasSupervisor($anio = null, $periodo = null)
	{
		//Obtener el no_ctrl del alumno del inicio de sesión
		//$rfc_empleado = Yii::app()->params['rfcSupervisor'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelSsProgramas = new SsProgramas_('search');
		$modelSsProgramas->unsetAttributes();  // clear any default values
		
		if(isset($_GET['SsProgramas']))
		{
			$modelSsProgramas->attributes = $_GET['SsProgramas'];
			$modelSsProgramas->anio = $_GET['SsProgramas']['anio'];
			$modelSsProgramas->periodo = $_GET['SsProgramas']['periodo'];
			$anio = $modelSsProgramas->anio;
			$periodo = $modelSsProgramas->periodo;
		}

        $this->render('listaHistoricoProgramasSupervisor',array(
					  'modelSsProgramas'=>$modelSsProgramas,
					  'rfcEmpleado' => $rfcEmpleado,
					  'anio' => $anio,
					  'periodo' => $periodo
        ));
	}

	public function actionDetalleHistoricoSPrograma($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';

		$modelSsProgramas = $this->loadModel($id_programa);

		//Datos Programa
		$data_supervisor = $this->getSupervisorPrincipal($modelSsProgramas->id_programa);
		$supervisor_principal = $data_supervisor[0]['name'];
		$periodo_programa = $this->getTipoPrograma($modelSsProgramas->id_periodo_programa);
		$tipo_servicio_social = $this->getTipoServicioSocialPrograma($modelSsProgramas->id_tipo_servicio_social);
		$empresa = $this->getEmpresaPerteneceSupervisor($supervisor_principal, $modelSsProgramas->id_tipo_programa);
		$prestador_apoyo = $this->getPrestadorApoyo($modelSsProgramas->id_apoyo_economico_prestador);
		$tipo_apoyo = $this->getTipoApoyosEconomicos($modelSsProgramas->id_tipo_apoyo_economico);
		$clasificacion_area = $this->getClasificacionArea($modelSsProgramas->id_clasificacion_area_servicio_social);
		$fec_registro_programa = GetFormatoFecha::getFechaLastUpdateProgramasReg($modelSsProgramas->id_programa);

		//periodo y Año
		$datos_per_anio = $this->getPeriodosAnios($modelSsProgramas->id_registro_fechas_ssocial);
		$periodo = $datos_per_anio->id_periodo;
		$anio = $datos_per_anio->anio;
		$fec_inicio = $this->getFormatoFecha($modelSsProgramas->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSsProgramas->fecha_fin_programa);
		$estatus_programa = $this->getEstatusprograma($modelSsProgramas->id_status_programa);

		$modelSsServicioSocial = new SsServicioSocial_('search');
		$modelSsServicioSocial->unsetAttributes();  // clear any default values
		
        if(isset($_GET['SsServicioSocial']))
            $modelSsServicioSocial->attributes=$_GET['SsServicioSocial'];

		$this->render('detalleHistoricoSPrograma',array(
					  'modelSsProgramas' => $modelSsProgramas,
					  'modelSsServicioSocial' => $modelSsServicioSocial,
					  'supervisor_principal' => $supervisor_principal,
					  'periodo_programa' => $periodo_programa,
					  'tipo_servicio_social' => $tipo_servicio_social,
					  'empresa' => $empresa,
					  'prestador_apoyo' => $prestador_apoyo,
					  'tipo_apoyo' => $tipo_apoyo,
					  'clasificacion_area' => $clasificacion_area,
					  'periodo' => $periodo,
					  'anio' => $anio,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'estatus_programa' => $estatus_programa,
					  'fec_registro_programa' => $fec_registro_programa
        ));
	}

	public function getEstatusprograma($id_status_programa)
	{
		$model = SsTipoStatus::model()->findByPk($id_status_programa);

		if($model === NULL)
			throw new CHttpException(404,'No existe estatus con ese ID.');

		return $model->descripcion_status;
	}

	public function getPeriodosAnios($id_registro_fechas_ssocial)
	{
		$model = SsRegistroFechasServicioSocial::model()->findByPk($id_registro_fechas_ssocial);

		if($model === NULL)
			throw new CHttpException(404,'No existen datos de ese Periodo y Año.');

		return $model;
	}

	public function getClasificacionArea($id_clasificacion_area_servicio_social)
	{
		$model = SsClasificacionArea::model()->findByPk($id_clasificacion_area_servicio_social);

		if($model === NULL)
			throw new CHttpException(404,'No existe esa Clasificación de Área.');

		return $model->clasificacion_area_servicio_social;
	}

	public function getPrestadorApoyo($id_apoyo_economico_prestador)
	{
		$modelSsApoyosEconomicos = SsApoyosEconomicos::model()->findByPk($id_apoyo_economico_prestador);

		if($modelSsApoyosEconomicos === NULL)
			throw new CHttpException(404,'No existe ese Tipo de Apoyo Económico.');

		return $modelSsApoyosEconomicos->descripcion_apoyo_economico;
	}

	public function getTipoApoyosEconomicos($id_tipo_apoyo_economico)
	{
		$model = SsTiposApoyosEconomicos::model()->findByPk($id_tipo_apoyo_economico);

		if($model === NULL)
			throw new CHttpException(404,'No existe ese Tipo de Apoyo Económico.');

		return $model->tipo_apoyo_economico;
	}

	public function getEmpresaPerteneceSupervisor($rfcEmpleado, $id_tipo_programa)
	{
		if($id_tipo_programa == 1)
		{
			$model = SsUnidadesReceptoras::model()->findByPk(1);
			if($model === NULL)
				throw new CHttpException(404,'No existe ese Tipo de Servicio Social.');

			return $model->nombre_unidad_receptora;

		}else{

			return "Empresa Externa";
		}
	}

	public function getTipoServicioSocialPrograma($id_tipo_servicio_social)
	{
		$modelSsTipoServicioSocial = SsTipoServicioSocial::model()->findByPk($id_tipo_servicio_social);

		if($modelSsTipoServicioSocial === NULL)
			throw new CHttpException(404,'No existe ese Tipo de Servicio Social.');

		return $modelSsTipoServicioSocial->tipo_servicio_social;
	}

	//RFC y nombre completo
	public function getSupervisorPrincipal($id_programa)
	{
		$qry_sup_princ = "select hemp.\"rfcEmpleado\", hemp.\"nmbCompletoEmp\" as name from pe_planeacion.ss_programas p
						join pe_planeacion.ss_responsable_programa_interno rpi
						on rpi.id_programa = p.id_programa
						join public.\"H_empleados\" hemp
						on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
						where p.id_programa = '$id_programa' AND rpi.superv_principal_int = true
					";

		$responsablePrograma = Yii::app()->db->createCommand($qry_sup_princ)->queryAll();

		if(!sizeof($responsablePrograma) > 0)
			throw new CHttpException(404,'No existe Supervisor para ese Programa.');

		return $responsablePrograma;
	}

	public function getTipoPrograma($id_periodo_programa)
	{
		$model = SsPeriodosProgramas::model()->findByPk($id_periodo_programa);

		if($model === NULL)
			throw new CHttpException(404,'No existe periodo con ese ID.');

		return $model->periodo_programa;
	}

	public function actionListaProgramasSinHorarioAsignado()
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  //clear any default values

		//Tipo Programa SEMESTRAL, ANUAL O UNICA VEZ
		$tipo_programa = $this->getPeriodosProgramas();

    if(isset($_GET['SsProgramas']))
    	$modelSSProgramas->attributes=$_GET['SsProgramas'];

		$this->render('listaProgramasSinHorarioAsignado',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'tipo_programa' => $tipo_programa
		));
	}

	public function actionListaProgramasSinSupervisorAsignado()
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  //clear any default values

		//Tipo Programa SEMESTRAL, ANUAL O UNICA VEZ
		$tipo_programa = $this->getPeriodosProgramas();

        if(isset($_GET['SsProgramas']))
            $modelSSProgramas->attributes=$_GET['SsProgramas'];

		$this->render('listaProgramasSinSupervisorAsignado',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'tipo_programa' => $tipo_programa
		));
	}

	/*Deshabilita los Programas vigentes y todos lo servicio sociales de ese programa pasan a estado CANCELADO*/
	public function actionCancelarProgramaServicioSocial($id_programa)
	{
		$modelSSProgramas = $this->loadModel($id_programa);
		$modelSSProgramas->id_status_programa = 4; // 4 - CANCELADO
		$modelSSProgramas->fecha_modificacion_programa = date('Y-m-d H:i:s');

		if($modelSSProgramas->save())
		{
			$this->cancelarServicioSocialesDelProgramaCancelado($modelSSProgramas->id_programa);
			echo CJSON::encode( [ 'code' => 200 ] );
		}else{
			echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
		}

	}
	/*Deshabilita los Programas vigentes y todos lo servicio sociales de ese programa pasan a estado CANCELADO*/

	public function cancelarServicioSocialesDelProgramaCancelado($id_programa)
	{
		//Actualizamos la tabla de servicio social
		$query = "Update pe_planeacion.ss_servicio_social
				  set id_estado_servicio_social = 7
				  where id_programa = '$id_programa' ";

		if(!Yii::app()->db->createCommand($query)->queryAll())
			throw new CHttpException(404,'Error!!! ocurrio un error al actualizar los datos de la tabla de ss_servicio_social.');

	}

	public function actionListaProgramasActivos()
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  //clear any default values

		//Lista de periodos
		$lista_periodos = $this->getPeriodosProgramas();

        if(isset($_GET['SsProgramas']))
            $modelSSProgramas->attributes=$_GET['SsProgramas'];

        $this->render('listaProgramasActivos',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'lista_periodos' => $lista_periodos
        ));
	}

	public function actionHistoricoProgramasServicioSocial($anio = null, $periodo = null)
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  //clear any default values

		//Lista de periodos
		$lista_periodos = $this->getPeriodosProgramas();
		$tipo_programa = $this->getPeriodosProgramas();

		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_GET['SsProgramas'];
			$modelSSProgramas->anio = $_GET['SsProgramas']['anio'];
			$modelSSProgramas->periodo = $_GET['SsProgramas']['periodo'];
			$anio = $modelSSProgramas->anio;
			$periodo = $modelSSProgramas->periodo;
		}

        $this->render('historicoProgramasServicioSocial',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'tipo_programa' => $tipo_programa,
					  'lista_periodos' => $lista_periodos,
					  'anio' => $anio,
					  'periodo' => $periodo
        ));
	}

	/*public function actionCambiarDeSupervisorDelPrograma($id_programa, $id_unidad_receptora)
	{
		$modelSSProgramas = $this->loadModel($id_programa);
		$modelSSSupervisoresProgramas = new SsSupervisoresProgramas;
		$modelSSResponsableProgramaExterno = new SsResponsableProgramaExterno;
		$modelSSResponsableProgramaInterno = new SsResponsableProgramaInterno;
		$modelHEmpleados = new HEmpleados;

		//Obtenemos el RFC del Supervisor del programa seleccionado
		$rfcSupervisor = $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$lista_supervisores = $this->getSupervisorXEmpresa($modelSSProgramas->id_programa, $modelSSProgramas->id_unidad_receptora, $rfcSupervisor);

		//Detalle del Programa
		$nombre_supervisor = $this->getNombreSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$periodo_programa = $this->getPeriodoPrograma($modelSSProgramas->id_periodo_programa);
		$fec_inicio = $this->getFormatoFecha($modelSSProgramas->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSSProgramas->fecha_fin_programa);

		//Lista de alumno asignados al Programa
		$criteria = new CDbCriteria();
		$criteria->condition = "id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSServicioSocial = SsServicioSocial::model()->findAll($criteria);

		if(isset($_POST['SsSupervisoresProgramas']) or isset($_POST['HEmpleados']))
        {
			if($modelSSProgramas->id_tipo_programa == 1)
			{
				$modelHEmpleados->attributes=$_POST['HEmpleados'];
				$rfc = $modelHEmpleados->rfcEmpleado;
				$modelSSResponsableProgramaInterno->rfcEmpleado = $rfc;
				$modelSSResponsableProgramaInterno->fecha_asignacion = date('Y-m-d H:i:s');

				if($modelSSResponsableProgramaInterno->save())
				{
					if($this->cambioDeSupervisorDelPrograma())
					{
						Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
						$this->redirect(array('listaProgramasActivos'));
					}


				}else{
					Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
				}

			}elseif($modelSSProgramas->id_tipo_programa == 2)
			{
				$modelSSSupervisoresProgramas->attributes=$_POST['SsSupervisoresProgramas'];
				$rfc = $modelSSSupervisoresProgramas->rfcSupervisor;
				$modelSSResponsableProgramaExterno->rfcSupervisor = $rfc;
				$modelSSResponsableProgramaExterno->fecha_asignacion = date('Y-m-d H:i:s');

				if($modelSSResponsableProgramaExterno->save())
				{
					if($this->cambioDeSupervisorDelPrograma())
					{
						Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
						$this->redirect(array('listaProgramasActivos'));
					}

				}else{
					Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
				}
			}
        }

		$this->render('cambiarDeSupervisorDelPrograma',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'modelHEmpleados' => $modelHEmpleados,
					  'modelSSSupervisoresProgramas' => $modelSSSupervisoresProgramas,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'lista_supervisores' => $lista_supervisores,
					  'nombre_supervisor' => $nombre_supervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'modelSSResponsableProgramaExterno' => $modelSSResponsableProgramaExterno,
					  'modelSSResponsableProgramaInterno' => $modelSSResponsableProgramaInterno
        ));
	}

	public function cambioDeSupervisorDelPrograma($id_programa)
	{
		$modelSSProgramas = $this->loadModel($id_programa);

		echo $modelProgramas->id_tipo_programa;
		die();
	}*/

	public function actionListaProgramasAdminServicioSocial($nombreSupervisor = null, $apePaterno = null, $apeMaterno = null)
	{
		$modelSSProgramas=new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  // clear any default values

		//Fecha de Inicio del Servicio Social
		$fec_inicio_servicio_social_actual = $this->getFechaInicioServicioSocialActual();
		//Lista de empresas para filtrar
		$tipo_programa = $this->getPeriodosProgramasTodos();
		//Periodo y año en que se registra el Programa
		$id_registro_fechas_ssocial = $this->getRegistroFechasServicioSocial();

		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes = $_GET['SsProgramas'];
			$modelSSProgramas->nombreSupervisor =  $_GET['SsProgramas']['nombreSupervisor']; //Busqueda por nombre
			$modelSSProgramas->apePaterno =  $_GET['SsProgramas']['apePaterno']; //Busqueda por apellido paterno
			$modelSSProgramas->apeMaterno =  $_GET['SsProgramas']['apeMaterno']; //Busqueda por apellido materno
			$nombreSupervisor = $modelSSProgramas->nombreSupervisor;
			$apePaterno = $modelSSProgramas->apePaterno;
			$apeMaterno = $modelSSProgramas->apeMaterno;

		}

		$this->render('listaProgramasAdminServicioSocial',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'tipo_programa' => $tipo_programa,
					  'id_registro_fechas_ssocial' => $id_registro_fechas_ssocial,
					  'fecha_actual' => date('Y-m-d'),
					  'fec_inicio_servicio_social_actual' => $fec_inicio_servicio_social_actual,
					  'nombreSupervisor' => $nombreSupervisor,
					  'apePaterno' => $apePaterno,
					  'apeMaterno' => $apeMaterno
		));
	}

	//Verificamos que la fecha de inicio del servicio social no sea mayor a la fecha actual y pueda agregar mas Programas
	public function getFechaInicioServicioSocialActual()
	{
		//Traemos la fecha de inicio del servicio social actual
		$bandera = false;
		$fec_actual = date('Y-m-d');
		$query = "select * from pe_planeacion.ss_registro_fechas_servicio_social where ssocial_actual = true";
		$datos = Yii::app()->db->createCommand($query)->queryAll();

		//Si hay servicio social activo entonces compara que la fecha de inicio de servicio social no haya pasado
		if($datos != NULL)
			$bandera = ($fec_actual < $datos[0]['fecha_limite_inscripcion']) ? true : false;

		return $bandera;
	}

	/*Para saber el registro de Servicio Social ACTUAL */
	public function getRegistroFechasServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'ssocial_actual = true';
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->find($criteria);

		return ($modelSSRegistroFechasServicioSocial != null) ? $modelSSRegistroFechasServicioSocial->id_registro_fechas_ssocial : 0;
	}
	/*Para saber el registro de Servicio Social ACTUAL */

	/*Nos traemos la fecha fin para saber si el programa ya caduco y pueda ser dado de baja*/
	public function getFechaFinPrograma()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'ssocial_actual = true';
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->find($criteria);

		return $modelSSRegistroFechasServicioSocial->fecha_limite_inscripcion;
	}
	/*Nos traemos la fecha fin para saber si el programa ya caduco y pueda ser dado de baja*/


	public function actionListaProgramasAlumnoServicioSocial($nombreSupervisor = null, $apePaterno = null, $apeMaterno = null)
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  // clear any default values

		//Obtener el no_ctrl del alumno del inicio de sesión
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;
		$alumnoAltaServicioSocial = $this->estaDadoDeAltaElAlumnoEnServicioSocial($no_ctrl);
		//Verificar que el alumno no haya tomado un servicio social en el mismo periodo y año
		$tomoSsocialMismoPeriodoYAnio = $this->getSSocialMismoPeriodoYAnio($no_ctrl);
		//Verificar si ya completo servicio social
		$completoServicioSocial = $this->completoServicioSocial($no_ctrl);

		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_GET['SsProgramas'];
			$modelSSProgramas->nombreSupervisor =  $_GET['SsProgramas']['nombreSupervisor']; //Busqueda por nombre
			$modelSSProgramas->apePaterno = $_GET['SsProgramas']['apePaterno']; //Busqueda por apellido paterno
			$modelSSProgramas->apePaterno = $_GET['SsProgramas']['apeMaterno']; //Busqueda por apellido paterno
			$nombreSupervisor = $modelSSProgramas->nombreSupervisor;
			$apePaterno = $modelSSProgramas->apePaterno;
			$apeMaterno = $modelSSProgramas->apeMaterno;
		}

        $this->render('listaProgramasAlumnoServicioSocial',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'no_ctrl' => $no_ctrl,
					  'alumnoAltaServicioSocial' => $alumnoAltaServicioSocial, //Verificamos si el alumno tiene activo el servicio social y pueda elegir programa
					  'tomoSsocialMismoPeriodoYAnio' => $tomoSsocialMismoPeriodoYAnio,
					  'servicioSocialFueCancelado' => $this->fueCanceladoServicioSocialDelAlumno($no_ctrl),
					  'hay_datos' => $this->getNoHaMandadoSolicitudPrograma($no_ctrl),
					  'cursando' => $this->estaCursandoServicioSocial($no_ctrl),
					  'fecha_limite' => $this->getValidarFechaLimiteSolicitudServicioSocial(),
					  'completoServicioSocial' => $completoServicioSocial,
					  'nombreSupervisor' => $nombreSupervisor,
					  'apePaterno' => $apePaterno,
					  'apeMaterno' => $apeMaterno
        ));
	}

	//Verificar si el alumno tiene dado de alta su servicio social
	public function estaDadoDeAltaElAlumnoEnServicioSocial($no_ctrl)
	{
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk($no_ctrl);
		$bandera = false;

		if($modelSSStatusServicioSocial != NULL)
			$bandera = ($modelSSStatusServicioSocial->val_serv_social == true) ? true : false;

		return $bandera;
	}

	public function completoServicioSocial($no_ctrl)
	{
		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);

		return ($modelSSHistoricoTotalesAlumnos != NULL) ? $modelSSHistoricoTotalesAlumnos->completo_servicio_social : NULL;

	}

	/*VERIFICAR QUE EL ALUMNO NO PUEDA TOMAR SERVICIO SOCIAL EN EL MISMO PERIODO */
	public function getSSocialMismoPeriodoYAnio($no_ctrl)
	{
		$_periodo = Yii::app()->db->createCommand("select periodoactual(0)")->queryAll();
		$periodo = $_periodo[0]['periodoactual'];
		$_anio = Yii::app()->db->createCommand("select periodoactual(1)")->queryAll();
		$anio = $_anio[0]['periodoactual'];

		$query = "select * from pe_planeacion.ss_servicio_social ss
							join pe_planeacion.ss_programas pss
							on pss.id_programa = ss.id_programa
							join pe_planeacion.ss_registro_fechas_servicio_social rfss
							on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial
							where rfss.id_periodo = '$periodo' AND rfss.anio = '$anio' and ss.no_ctrl = '$no_ctrl' AND ss.id_estado_servicio_social = 6 ";

		$data = Yii::app()->db->createCommand($query)->queryAll();
		$tamanio = sizeof($data);
		$bandera = ($tamanio > 0) ? 1 : 0; //Si regresa true entonces si hay registro de servicio social en ese periodo y año

		return $bandera;

	}
	/*VERIFICAR QUE EL ALUMNO NO PUEDA TOMAR SERVICIO SOCIAL EN EL MISMO PERIODO */

	//VERIFICAR QUE NO PUEDA LLEVAR SERVICIO SOCIAL SI FUE CANCELADO POR LA JEFA DE OFIC DE SERVICIO SOCIAL
	public function fueCanceladoServicioSocialDelAlumno($no_ctrl)
	{
		$_periodo = Yii::app()->db->createCommand("select periodoactual(0)")->queryAll();
		$periodo = $_periodo[0]['periodoactual'];
		$_anio = Yii::app()->db->createCommand("select periodoactual(1)")->queryAll();
		$anio = $_anio[0]['periodoactual'];

		$query = "select * from pe_planeacion.ss_servicio_social ss
							join pe_planeacion.ss_programas pss
							on pss.id_programa = ss.id_programa
							join pe_planeacion.ss_registro_fechas_servicio_social rfss
							on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial
							where rfss.id_periodo = '$periodo' AND rfss.anio = '$anio' and ss.no_ctrl = '$no_ctrl' AND ss.id_estado_servicio_social = 7 ";

		$data = Yii::app()->db->createCommand($query)->queryAll();
		$tamanio = sizeof($data);
		$bandera = ($tamanio > 0) ? 1 : 0; //Si regresa true entonces si hay registro de servicio social en ese periodo y año

		return $bandera;
	}
	//VERIFICAR QUE NO PUEDA LLEVAR SERVICIO SOCIAL SI FUE CANCELADO POR LA JEFA DE OFIC DE SERVICIO SOCIAL

	/*Para saber si el alumno ha mandado solicitud de Servicio Social*/
	public function getNoHaMandadoSolicitudPrograma($no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_solicitud_programa_servicio_social
		where (id_estado_solicitud_programa_alumno = 1 OR id_estado_solicitud_programa_alumno = 2)
		and no_ctrl = '$no_ctrl'
		";

		$hay_datos = Yii::app()->db->createCommand($query)->queryAll();

		return ($hay_datos != NULL ) ? true : false;
	}
	/*Para saber si el alumno ha mandado solicitud de Servicio Social*/

	/*verificamos que aun no se haya vencido la fecha limite de inscripcion */
	public function getValidarFechaLimiteSolicitudServicioSocial()
	{
		$bandera = false;
		$query = "select * FROM pe_planeacion.ss_registro_fechas_servicio_social WHERE ssocial_actual = true";

		$fecha_limite = Yii::app()->db->createCommand($query)->queryAll();
		$fecha_actual = date('Y-m-d H:i:s');

		if($fecha_limite != NULL)
			$bandera = ($fecha_actual < $fecha_limite[0]['fecha_limite_inscripcion']) ? true : false;

		return $bandera;
	}
	/*verificamos que aun no se haya vencido la fecha limite de inscripcion */

	public function estaCursandoServicioSocial($no_ctrl)
	{
		//No se toma id_estado_servicio_social 6 porque ya esta finalizado
		$query = "select * from pe_planeacion.ss_servicio_social
		where (id_estado_servicio_social > 0 AND id_estado_servicio_social < 6)
		and no_ctrl = '$no_ctrl'
		";

		$cursando = Yii::app()->db->createCommand($query)->queryAll();

		return ($cursando != NULL ) ? true : false;
	}

	public function actionListaProgramasSupervisorServicioSocial()
	{
		$modelSSProgramas = new SsProgramas_('searchProgramaSupervisor');
		$modelSSProgramas->unsetAttributes();  // clear any default values

		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		//Periodo y año en que se crea el nuevo Programa
		$id_registro_fechas_ssocial = $this->getRegistroFechasServicioSocial();
		//Id del tipo de programa que tiene, si es que tiene algun programa a su cargo
		$id_tipo_programa = $this->getIDprogramaVigenteSupervisor($rfcSupervisor);
		//Fecha de Inicio del Servicio Social
		$fec_inicio_servicio_social_actual = $this->getFechaInicioServicioSocialActual();

		/*VALIDAMOS QUE NO TENGA PROGRAMAS ASIGNADOS A MENOS QUE YA ESTEN EN STATUS FINALIZADO O CANCELADO*/
		if($id_tipo_programa != NULL)
			$asignado = $this->validarPuedaCrearPrograma($rfcSupervisor, $id_tipo_programa);
		else
			$asignado = false;

		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_GET['SsProgramas'];
		}

		//Veamos si ha creado un Programa en el Periodo actual el Supervisor
		$modelProgramas = $this->getDatosProgramaSupervisor($rfcSupervisor);

		$this->render('listaProgramasSupervisorServicioSocial',array(
					'modelSSProgramas'=>$modelSSProgramas,
					'rfcsupervisor' => $rfcSupervisor,
					'id_tipo_programa' => $id_tipo_programa,
					'asignado' => $asignado,
					'id_registro_fechas_ssocial' => $id_registro_fechas_ssocial,
					'fec_inicio_servicio_social_actual' => $fec_inicio_servicio_social_actual,
					'modelProgramas' => $modelProgramas
  		));

	}

	public function getDatosProgramaSupervisor($rfcSupervisor)
	{
		//Obtener si el Supervisor es interno o externo
		$modelEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSupervisor = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelEmpleados != NULL)
		{
			$query = "select * from public.\"H_empleados\" hemp
					join pe_planeacion.ss_responsable_programa_interno rpi
					on rpi.\"rfcEmpleado\" = hemp.\"rfcEmpleado\"
					join pe_planeacion.ss_programas pss
					on pss.id_programa = rpi.id_programa
					join pe_planeacion.ss_registro_fechas_servicio_social rfss
					on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial
					where rpi.\"rfcEmpleado\" = '$rfcSupervisor' and rfss.ssocial_actual = true
					and (pss.id_status_programa != 2 AND pss.id_status_programa != 4)";

			$datos = Yii::app()->db->createCommand($query)->queryAll();

		}elseif($modelSupervisor != NULL)
		{
			$query = "select * from pe_planeacion.ss_supervisores_programas sp
					join pe_planeacion.ss_responsable_programa_externo rpe
					on rpe.\"rfcSupervisor\" = sp.\"rfcSupervisor\"
					join pe_planeacion.ss_programas pss
					on pss.id_programa = rpe.id_programa
					join pe_planeacion.ss_registro_fechas_servicio_social rfss
					on rfss.id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial
					join pe_planeacion.ss_unidades_receptoras ur
					on ur.id_unidad_receptora = sp.id_unidad_receptora
					where rpe.\"rfcSupervisor\" = '$rfcSupervisor' and rfss.ssocial_actual = true
					and (pss.id_status_programa != 2 AND pss.id_status_programa != 4)";

			$datos = Yii::app()->db->createCommand($query)->queryAll();

		}else{
			$datos = array();
		}

		return $datos;

	}

	/*VERIFICAMOS QUE EL SUPERVISOR INTERNO/EXTERNO NO TENGA PROGRAMAS ASIGNADOS ACTUALMENTE*/
	public function validarPuedaCrearPrograma($rfcSupervisor, $id_tipo_programa)
	{
		if($id_tipo_programa == 1)
		{
			//Interno
			$query = "select * from pe_planeacion.ss_responsable_programa_interno rpi
					join pe_planeacion.ss_programas pss
					on pss.id_programa = rpi.id_programa
					where rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND 
					(pss.id_status_programa = 1 OR pss.id_status_programa = 6 OR pss.id_status_programa = 8)
				";
			
			$programas = Yii::app()->db->createCommand($query)->queryAll();
			$status = ($programas === NULL) ? false : true;

		}else
		if($id_tipo_programa == 2)
		{
			//Externo
			//$query = "select id_status_supervisor from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";
			$query = "select * from pe_planeacion.ss_programas pss
					join pe_planeacion.ss_responsable_programa_externo rpe
					on rpe.id_programa = pss.id_programa
					where rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND 
					(pss.id_status_programa = 1 OR pss.id_status_programa = 6 OR pss.id_status_programa = 8)
				";
			
			$programas = Yii::app()->db->createCommand($query)->queryAll();
			$status = ($programas === NULL) ? false : true;
		}


		return $status;
	}
	/*VERIFICAMOS QUE EL EMPLEADO NO TENGA PROGRAMAS ASIGNADOS ACTUALMENTE*/

	//Detalle del programa vista para el Admin
	public function actionDetalleAdminProgramaServicioSocial($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/ValidacionesServicioSocial.php';
		$modelSSProgramas = $this->loadModel($id_programa);
		$modelEDatosAlumno = new EDatosAlumno;

		//Detalle del Programa
		$nombre_supervisor = $this->getNombreSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$rfcSupervisor = $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$periodo_programa = $this->getPeriodoPrograma($modelSSProgramas->id_periodo_programa);
		$fec_inicio = $this->getFormatoFecha($modelSSProgramas->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSSProgramas->fecha_fin_programa);

		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		//Lista de Alumnos inscritos en el programa
		$SSServicioSocial = new SsServicioSocial_('searchListaAlumnossEnPrograma');
		$SSServicioSocial->unsetAttributes();  // clear any default values

		$criteria2 = new CDbCriteria();
		$criteria2->condition = "id_programa = '$id_programa' and id_estado_servicio_social != 7";
		//$modelSSServicioSocial = SsServicioSocial::model()->findAll($criteria2);

		if(isset($_GET['SsServicioSocial']))
			$SSServicioSocial->attributes=$_GET['SsServicioSocial'];

		/*CODIGO DE AGREGAR ALUMNOS AL PROGRAMA ACTUAL */
		$modelSSServicioSocial = new SsServicioSocial;

		//Fecha de creacion de la Solicitud
		$fec_inscripcion = date('Y-m-d H:i:s');

		//Vienen datos por POST
		if(isset($_POST['EDatosAlumno']))
		{
			$modelEDatosAlumno->attributes = $_POST['EDatosAlumno'];
			$modelEDatosAlumno = EDatosAlumno::model()->findByPk(trim($modelEDatosAlumno->nctrAlumno));

			//VERIFICAMOS QUE EL ALUMNO EXISTA EN EL SISTEMA
			if($modelEDatosAlumno != NULL)
			{
				//VERIFICAMOS QUE EL ALUMNO ESTE INSCRITO EN LA INSTITUCION
				if($modelEDatosAlumno->insAlumno != 'N')
				{
					//VERIFICAMOS QUE EL ALUMNO CUMPLA LOS CREDITOS MINIMOS PARA LLEVAR SERVICIO SOCIAL
					if($this->cumpleCreditos(trim($modelEDatosAlumno->nctrAlumno)))
					{
						//VERIFICAMOS QUE EL ALUMNO YA HAYA HECHO EL PREREGISTRO
						if($this->yaEstaRegistrado(trim($modelEDatosAlumno->nctrAlumno)))
						{
							//VERIFICAMOS QUE EL ALUMNO NO HAYA COMPLETADO YA SUS HORAS DE SERVICIO SOCIAL
							if(!$this->completoHorasServicioSocial(trim($modelEDatosAlumno->nctrAlumno)))
							{
								//VERIFICAMOS QUE EL ALUMNO NO ESTE LLEVANDO SERVICIO SOCIAL ACTUALMENTE (NO PUEDE LLEVAR DOS SERVICIOS SOCIALES AL MISMO TIEMPO)
								if(!$this->llevaServicioSocialActualmente($modelEDatosAlumno->nctrAlumno))
								{
									//DE NUEVO VERIFICAMOS QUE HAYA HECHO YA UN PREREGISTRO
									$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk(trim($modelEDatosAlumno->nctrAlumno));

									if($modelSSStatusServicioSocial != NULL)
									{
										//VERIFICAMOS QUE EL ALUMNO NO TENGA SOLICITUDES PENDIENTES A PROGRAMAS
										if(!ValidacionesServicioSocial::validarNoSolicitudesPendientesProgramasServicioSocial(trim($modelEDatosAlumno->nctrAlumno)))
										{
											//VERIFICAMOS QUE HAYA UN REGISTRO DE SERVICIO SOCIAL HABILITADO ACTUALMENTE, PERIODO Y AÑO
											if($this->hayRegistroHabilitadoServicioSocial())
											{
												//Instanciamos un objeto de SsSolicitudProgramaServicioSocial para insertar datos de la nueva solicitud
												$modelSSSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial;

												/*Se crea el registro para la Solicitud de Servicio Social*/
												$modelSSSolicitudProgramaServicioSocial->no_ctrl = trim($modelEDatosAlumno->nctrAlumno);
												$modelSSSolicitudProgramaServicioSocial->id_programa = $id_programa;
												$modelSSSolicitudProgramaServicioSocial->fecha_solicitud_programa = $fec_inscripcion;
												$modelSSSolicitudProgramaServicioSocial->valida_solicitud_alumno = $fec_inscripcion;
												$modelSSSolicitudProgramaServicioSocial->valida_solicitud_supervisor_programa = $fec_inscripcion;
												$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 2; //ACEPTADO
												$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 2; //ACEPTADO

												if($modelSSSolicitudProgramaServicioSocial->save())
												{
													/*Se crea su registro de Servicio Social al alumno*/
													$this->insertRegistroTablaServicioSocial($modelSSSolicitudProgramaServicioSocial->no_ctrl, $modelSSSolicitudProgramaServicioSocial->id_solicitud_programa, $modelSSSolicitudProgramaServicioSocial->id_programa);

													/*Crear los reportes bimestrales Servicio Social Semestral y Anual*/
													if($modelSSProgramas->id_periodo_programa == 1 or $modelSSProgramas->id_periodo_programa == 2)
													{
														if(Yii::app()->db->createCommand("select pe_planeacion.agregarReportesBimestralesAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll() &&
					    								Yii::app()->db->createCommand("select pe_planeacion.agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
					    								{
															/*Una vez registrado todo, sino hubo problemas ahora asi restar el campo lugares_disponibles en la tabla programas_servicio_social*/
															$this->restarLugarDisponibleEnPrograma($modelSSProgramas);

															$modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
															if($modelSSServicioSocial ===null)
																throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

															//Mandar observacion de bienvenida al ALUMNO por parte del Supervisor y el Jefe de Oficina de Servicio Social
															$this->ObservacionDeBienvenidaSupervisorAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa));
															$this->ObservacionDeBienvenidaDeptoAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getJefeOficinaServicioSocialActual());

														}else{

															Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
														}

													}elseif ($modelSSProgramas->id_periodo_programa == 3)
													{
														if(Yii::app()->db->createCommand("select pe_planeacion.agregarReportesBimestralesExtraTemporal('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa', '$modelSSProgramas->fecha_fin_programa')")->queryAll() &&
														Yii::app()->db->createCommand("select pe_planeacion.agregarActividadesServicioSocialAlumno('$modelSSProgramas->id_periodo_programa','$modelSSSolicitudProgramaServicioSocial->id_solicitud_programa','$modelSSProgramas->fecha_inicio_programa')")->queryAll())
														{
															/*Una vez registrado todo, sino hubo problemas ahora asi restar el campo lugares_disponibles en la tabla programas_servicio_social*/
															$this->restarLugarDisponibleEnPrograma($modelSSProgramas);

															$modelSSServicioSocial = SsServicioSocial::model()->findByPK($modelSSSolicitudProgramaServicioSocial->id_solicitud_programa);
															if($modelSSServicioSocial === null)
																throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

															//Mandar observacion de bienvenida al ALUMNO por parte del Supervisor y el Jefe de Oficina de Servicio Social
															$this->ObservacionDeBienvenidaSupervisorAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa));
															$this->ObservacionDeBienvenidaDeptoAlAlumno($modelSSProgramas->id_programa, $modelSSServicioSocial->id_servicio_social, $modelSSSolicitudProgramaServicioSocial->no_ctrl, $this->getJefeOficinaServicioSocialActual());

														}else{
															Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al generar la información del Servicio Social.');
														}
													}

												}else{

													Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al registrar los datos de la Solicitud.');
													die("Error!!! Ocurrio un error al registrar los datos de la Solicitud.");
												}

											}else{
												Yii::app()->user->setFlash('danger', 'Error!!! No hay Registro de Servicio Social HABILITADO actualmente.');
												die("Error!!! No hay Registro de Servicio Social HABILITADO actualmente.");
											}

										}else{
											Yii::app()->user->setFlash('danger', 'Error!!! Alumno con Solicitud Pendiente, debe CANCELAR la Solicitud el Supervisor del Programa.');
											die("Error!!! Alumno con Solicitud Pendiente, debe CANCELAR la Solicitud el Supervisor del Programa.");
										}

									}else{
										Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno NO HA REALIZADO el Preregistro de Servicio Social.');
										die("Error!!! Este Alumno NO HA REALIZADO el Preregistro de Servicio Social.");
									}

								}else{
									Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno YA ESTA INSCRITO ACTUALMENTE en un Programa de Servicio Social.');
									die("Error!!! Este Alumno YA ESTA INSCRITO ACTUALMENTE en un Programa de Servicio Social.");
								}

							}else{
								Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno YA COMPLETO sus 480 horas de Servicio Social.');
								die("Error!!! Este Alumno YA COMPLETO sus 480 horas de Servicio Social.");
							}

						}else{
							Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno NO tiene dado de ALTA el Servicio Social.');
							die("Error!!! Este Alumno NO tiene dado de ALTA el Servicio Social.");
						}

					}else{
						Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno NO CUMPLE con los creditos suficientes para llevar el Servicio Social.');
						die("Error!!! Este Alumno NO CUMPLE con los creditos suficientes para llevar el Servicio Social.");
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! Este Alumno no esta inscrito o vigente en la Institución');
					die("Error!!! Este Alumno no esta inscrito o vigente en la Institución");
				}

			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! No se encontraron datos del Alumno con ese No. de Control.');
				die("Error!!! No se encontraron datos del Alumno con ese No. de Control.");
			}

		}
		/*CODIGO DE AGREGAR ALUMNOS AL PROGRAMA ACTUAL*/

		$this->render('detalleAdminProgramaServicioSocial',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'SSServicioSocial' => $SSServicioSocial,
					  'modelEDatosAlumno' => $modelEDatosAlumno,
					  'nombre_supervisor' => $nombre_supervisor,
					  'rfcSupervisor' => $rfcSupervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'criteria2' => $criteria2

		));
	}

	//Verifiacamos que el Alumno no este llevando servicio social actualmente
	public function llevaServicioSocialActualmente($no_ctrl)
	{
		$noctrl = trim($no_ctrl);

		$query = "select * from pe_planeacion.ss_servicio_social ss
				join pe_planeacion.ss_estado_servicio_social ess
				on ess.id_estado_servicio_social = ss.id_estado_servicio_social
				where ess.id_estado_servicio_social > 0 and ess.id_estado_servicio_social < 6 and
				ss.no_ctrl = '$noctrl' ";

		$resultado = Yii::app()->db->createCommand($query)->queryAll();

		return ($resultado !=  NULL) ? true : false;
	}

	//Verificamos que el Alumno cumpla con minimo 135 creditos para llevar servicio social
	public function cumpleCreditos($no_ctrl)
	{
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk(trim($no_ctrl));

		return ($modelEDatosAlumno->crdAcumulaAlu >= 135) ? true : false;
	}

	//Detalle del programa vista para el supervisor
	public function actionDetalleSupervisorProgramaServicioSocial($id_programa)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarProgramaSupervisor($rfcSupervisor, $id_programa))
			throw new CHttpException(4044,'Este Programa No te corresponde.');

		$modelSSProgramas = $this->loadModel($id_programa);

		//Detalle del Programa
		$nombre_supervisor = $this->getNombreSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$rfcSupervisor = $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$periodo_programa = $this->getPeriodoPrograma($modelSSProgramas->id_periodo_programa);
		$fec_inicio = $this->getFormatoFecha($modelSSProgramas->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSSProgramas->fecha_fin_programa);
		$empresa = $this->getDatosEmpresa($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);

		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		$this->render('detalleSupervisorProgramaServicioSocial',array(
					  'modelSSProgramas' => $modelSSProgramas,
					  'nombre_supervisor' => $nombre_supervisor,
					  'rfcSupervisor' => $rfcSupervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'empresa' => $empresa,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,

		));
	}
	//Detalle del programa vista para el supervisor

	/*public function getnameEmpresa($rfcSupervisor)
	{
		$query = "
		select nombre_unidad_receptora from ss_supervisores_programas sp
		join ss_unidades_receptoras ur
		on ur.id_unidad_receptora = sp.id_unidad_receptora
		where sp.\"rfcSupervisor\" = '$rfcSupervisor'
		";

		$name = Yii::app()->db->createCommand($query)->queryAll();

		return $name[0]['nombre_unidad_receptora'];
	}*/

	//Detalle del programa vista para el Alumno
	public function actionDetalleAlumProgramaServicioSocial($id_programa)
	{
		if(!$this->validarProgramaDisponible($id_programa))
			throw new CHttpException(4044,'Este Programa No esta Disponible.');

		$modelSSProgramas = $this->loadModel($id_programa);

		//Detalle del Programa
		$nombre_supervisor = $this->getNombreSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa); //Mandamos el id del programa para ver quien es el supervisor
		$periodo_programa = $this->getPeriodoPrograma($modelSSProgramas->id_periodo_programa);
		$fec_inicio = $this->getFormatoFecha($modelSSProgramas->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSSProgramas->fecha_fin_programa);

		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		$this->render('detalleAlumProgramaServicioSocial',array(
					  'modelSSProgramas'=> $modelSSProgramas,
					  'nombre_supervisor' => $nombre_supervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,

		));
	}

	public function actionNuevoProgramaServicioSocialDepto($id_registro_fechas_ssocial)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';
		$modelSSProgramas = new SsProgramas;

		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramas();
		$lista_empresas = array(); //Empresas vacio, se filtrara dependiendo del tipo de programa que sea
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = array();//$this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_status = $this->getStatusPrograma();

		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_POST['SsProgramas'];

			//Verificar que las horas a liberar sea un número entero positivo
			if(RegularExpression::isEnteroPositivo($modelSSProgramas->horas_totales))
			{
				//Verificar que los alumnos asignados sean un número entero positivo
				if(RegularExpression::isEnteroPositivo($modelSSProgramas->numero_estudiantes_solicitados))
				{
					if($modelSSProgramas->id_apoyo_economico_prestador == 2 AND $modelSSProgramas->id_tipo_apoyo_economico == 4)
						$modelSSProgramas->apoyo_economico = "NINGUNO";

					$modelSSProgramas->fecha_registro_programa = date('Y-m-d H:i:s');
					$modelSSProgramas->fecha_modificacion_programa = $modelSSProgramas->fecha_registro_programa;

					/*Por obvias razones cuando se crea el programa el numero de alumnos solicitados es igual al numero de puestos disponibles*/
					$modelSSProgramas->lugares_disponibles = $modelSSProgramas->numero_estudiantes_solicitados;
					//El programa cuando se crea estara por default en status de NO SUPERVISOR, en editar programa se podra dar de baja
					$modelSSProgramas->id_status_programa = 7; //Una vez se asigne el supervisor y el horario pasa a ALTA
					//fecha_inicio_programa
					$modelSSProgramas->fecha_inicio_programa = $this->getFechaInicioPrograma($id_registro_fechas_ssocial);
					//fecha_fin_programa
					$modelSSProgramas->fecha_fin_programa = $this->getFechaTerminoProgramaAproximado($modelSSProgramas->fecha_inicio_programa, $modelSSProgramas->id_periodo_programa);
					//Ligar con el registro del servicio social actual
					$modelSSProgramas->id_registro_fechas_ssocial = $id_registro_fechas_ssocial;
					//Lugares ocupados del programa, default es cero cuando se crea el programa
					$modelSSProgramas->lugares_ocupados = 0;

					if($modelSSProgramas->save())
					{
						Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
						//Mandar a la vista de agregar Supervisores para agregar un supervisor al programa recien creado
						$this->redirect(array('asignacionSupervisoresProgramasDepto', 'id_programa' => $modelSSProgramas->id_programa, 'id_unidad_receptora' => $modelSSProgramas->id_unidad_receptora));

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! Los Alumnos deben ser un numero entero positivo.');
				}

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! Las Horas a Liberar deben ser un número entero positivo.');
			}

		}

		$this->render('nuevoProgramaServicioSocialDepto',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'lista_empresas' => $lista_empresas,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'lista_status' => $lista_status,
		));
	}

	/*Si se registro correctamente el programa cambia el status del Supervisor asigando al Programa*/
	public function statusSupervisorOcupado($rfcSupervisor)
	{
		$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findByPK($rfcSupervisor);

		if($modelSSSupervisoresProgramas === NULL)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese Supervisor.');

		$modelSSSupervisoresProgramas->id_status_supervisor = 5; //Ocupado en un programa
		$modelSSSupervisoresProgramas->save();
	}
	/*Si se registro correctamente el programa cambia el status del Supervisor asigando al Programa*/

	//Fecha estimada de fin del programa depdiendo el periodo del programa (Semestral, Anual, Unica vez)
	public function getFechaTerminoProgramaAproximado($fecha_inicio_programa, $id_periodo_programa)
	{
		$fecha = $fecha_inicio_programa;
		$nuevafecha;

		switch($id_periodo_programa)
		{
			case 1:
				$nuevafecha = strtotime ( '+6 month' , strtotime ($fecha));
				$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
			break;
			case 2:
				$nuevafecha = strtotime ( '+12 month' , strtotime ($fecha));
				$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
			break;
		}

		return $nuevafecha;
	}
	//Fecha estimada de fin del programa depdiendo el periodo del programa (Semestral, Anual, Unica vez)

	public function getFechaInicioPrograma($id_registro_fechas_ssocial)
	{
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->findByPK($id_registro_fechas_ssocial);

		if($modelSSRegistroFechasServicioSocial === null)
			throw new CHttpException(404,'Todos los campos deben ser completados.');

		return $modelSSRegistroFechasServicioSocial->fecha_inicio_ssocial;

	}

	public function actionNuevoProgramaServicioSocialSupervisor($id_registro_fechas_ssocial)
	{
		$modelSSProgramas = new SsProgramas;
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;
		$info_empleado = $this->infoEmpleado($rfcSupervisor);
		//Obtenemos si es un supervisor interno o externo
		$id_tipo_supervisor = $this->getTipoSupervisor($rfcSupervisor);
		//Obtenemos la empresa del supervisor
		$id_empresa = $this->getEmpresaSupervisor($rfcSupervisor);

		$lista_unidades_receptoras = $this->getUnidadesReceptoras();

		$lista_departamentos = $this->getDepartamentos();

		$lista_periodos_programas = $this->getPeriodosProgramas();

		$lista_supervisores = $this->getSupervisores();

		$lista_tipo_servicio_social = $this->getTipoServicioSocial();

		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();

		$lista_tipo_apoyo = array();//$this->getTipoApoyoEconomico();

		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();

		$lista_status = $this->getStatusPrograma();

		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_POST['SsProgramas'];

			if($modelSSProgramas->id_apoyo_economico_prestador == 2 AND $modelSSProgramas->id_tipo_apoyo_economico == 4)
				$modelSSProgramas->apoyo_economico = "NINGUNO";

			$modelSSProgramas->fecha_registro_programa = date('Y-m-d H:i:s');
			$modelSSProgramas->fecha_modificacion_programa = $modelSSProgramas->fecha_registro_programa;
			/*Se pone cero por default ya que el Admin es el que asigna el numero de puestos disponibles*/
			$modelSSProgramas->lugares_disponibles = $modelSSProgramas->numero_estudiantes_solicitados;
			//Se asigna por defualt cero hasta que el Admin asigne las horas que se van a liberar con el Programa
			$modelSSProgramas->horas_totales = 0;
			//El programa cuando se crea estara por default en status de PENDIENTE, en editar programa se podra dar de baja
			$modelSSProgramas->id_status_programa = 6; //PENDIENTE
			//fecha_inicio_programa
			$modelSSProgramas->fecha_inicio_programa = $this->getFechaInicioPrograma($id_registro_fechas_ssocial);
			//fecha_fin_programa
			$modelSSProgramas->fecha_fin_programa = $this->getFechaTerminoProgramaAproximado($modelSSProgramas->fecha_inicio_programa, $modelSSProgramas->id_periodo_programa);
			//Ligar con el registro del servicio social actual
			$modelSSProgramas->id_registro_fechas_ssocial = $id_registro_fechas_ssocial;
			//Lugares ocupados del programa, default es cero cuando se crea el programa
			$modelSSProgramas->lugares_ocupados = 0;
			//$modelSSProgramas->id_programa = 171;

			if($modelSSProgramas->save())
			{
				if($this->guardarDatosSupervisorPrograma($modelSSProgramas->id_programa, $modelSSProgramas->id_unidad_receptora, $rfcSupervisor))
				{
					Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					$this->redirect(array('listaProgramasSupervisorServicioSocial'));

				}else{
					die("1");
					Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos del Supervisor.');
				}

			}else{
				die("2");
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos del Programa.');
			}
		}

		$this->render('nuevoProgramaServicioSocialSupervisor',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'info_empleado' => $info_empleado,
					  'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
					  'cargo' => $this->getCargo($info_empleado->rfcEmpleado),
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'id_tipo_supervisor' => $id_tipo_supervisor,
					  'id_empresa' => $id_empresa,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'fecha_actualizacion' => null,
					  'lista_status' => $lista_status,
		));
	}

	public function infoEmpleado($rfcSupervisor)
	{
		$rfcEmpleado = trim($rfcSupervisor);

		$modelHEmpleados = HEmpleados::model()->findByPk($rfcEmpleado);
		if($modelHEmpleados === NULL)
			throw new CHttpException(404,'No hay datos del Empleado.');

		return $modelHEmpleados;
	}

	public function getDepartamentoEmpleado($cve_depto)
	{
		$cveDepto = trim($cve_depto);
		$qry_depto = "select * from public.\"H_departamentos\" where \"cveDepartamento\" = '$cveDepto' ";
		$rs = Yii::app()->db->createCommand($qry_depto)->queryAll();

		return ($rs[0]['dscDepartamento'] === Null ) ? '-' : $rs[0]['dscDepartamento'];
	}

	public function getCargo($rfc_empleado)
	{
		$rfcEmpleado = trim($rfc_empleado);
		$qry_cargo = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcEmpleado' ";

		$rs = Yii::app()->db->createCommand($qry_cargo)->queryAll();

		return ($rs[0]['nmbPuesto'] === NULL) ? '-' : $rs[0]['nmbPuesto'];
	}

	public function getTipoSupervisor($rfcSupervisor)
	{
		$data;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$data = array( '1' =>'INTERNO');
		}else
		if($modelSSSupervisoresProgramas != NULL){

			$data = array('2' =>'EXTERNO');
		}

		return $data;
	}

	//Detectamos la empresa a la que pertenece el supervisor
	public function getEmpresaSupervisor($rfcSupervisor)
	{

		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);
		$criteria = new CDbCriteria();

		if($modelHEmpleados != NULL)
		{
			$criteria->condition = "id_unidad_receptora = 1 ";
			$modelSSUnidadesReceptoras = SsUnidadesReceptoras::model()->findAll($criteria);

		}else
		if($modelSSSupervisoresProgramas != NULL)
		{
			$id = $modelSSSupervisoresProgramas->id_unidad_receptora;
			$criteria->condition = "id_unidad_receptora = '$id' ";
			$modelSSUnidadesReceptoras = SsUnidadesReceptoras::model()->findAll($criteria);
		}

		return CHtml::listData($modelSSUnidadesReceptoras, "id_unidad_receptora", "nombre_unidad_receptora");
	}

	//Selecciona el supervisor a cargo del programa (Vista del supervisor)
	public function guardarDatosSupervisorPrograma($id_programa, $id_unidad_receptora, $rfcSupervisor)
	{
		$bandera = false;
		$modelSSResponsableProgramaInterno = new SsResponsableProgramaInterno;
		$modelSSResponsableProgramaExterno = new SsResponsableProgramaExterno;

		$modelSSProgramas = $this->loadModel($id_programa);

			//Supervisor INTERNO
			if($modelSSProgramas->id_tipo_programa == 1)
			{

				$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
				if($modelHEmpleados === NULL)
					throw new CHttpException(404,'No existe usuario con ese RFC.');

				$rfc = $modelHEmpleados->rfcEmpleado;
				$modelSSResponsableProgramaInterno->id_programa = $id_programa;
				$modelSSResponsableProgramaInterno->rfcEmpleado = $rfc;
				$modelSSResponsableProgramaInterno->fecha_asignacion = date('Y-m-d H:i:s');
				$modelSSResponsableProgramaInterno->superv_principal_int = true;

				if($modelSSResponsableProgramaInterno->save())
				{
					//Una vez se asigna un Supervisor al Programa pasa a estatus NO HORARIO el Programa
					$modelSSProgramas->id_status_programa = 8;
					$modelSSProgramas->save();
					$bandera = true;

				}

			}else
			if($modelSSProgramas->id_tipo_programa == 2)
			{

				$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);
				if($modelSSSupervisoresProgramas === NULL)
					throw new CHttpException(404,'No existe usuario con ese RFC en la tabla de ss_supervisores.');

				$rfc = $modelSSSupervisoresProgramas->rfcSupervisor;
				$modelSSResponsableProgramaExterno->id_programa = $id_programa;
				$modelSSResponsableProgramaExterno->rfcSupervisor = $rfc;
				$modelSSResponsableProgramaExterno->fecha_asignacion = date('Y-m-d H:i:s');
				$modelSSResponsableProgramaExterno->superv_principal_ext = true;

				if($modelSSResponsableProgramaExterno->save())
				{
					//Una vez se asigna un Supervisor al Programa pasa a estatus NO HORARIO el Programa
					$modelSSProgramas->id_status_programa = 8;
					$modelSSProgramas->save();
					$bandera = true;
				}
			}

		return $bandera;

	}

	//Selecciona el supervisor a cargo del programa (Vista del jefe de servicio social)
	public function actionAsignacionSupervisoresProgramasDepto($id_programa, $id_unidad_receptora)
	{
		$modelSSSupervisoresProgramas = new SsSupervisoresProgramas;
		$modelSSResponsableProgramaExterno = new SsResponsableProgramaExterno;
		$modelSSResponsableProgramaInterno = new SsResponsableProgramaInterno;
		$modelHEmpleados = new HEmpleados;

		$lista_supervisores = $this->getSupervisoresXEmpresa($id_programa, $id_unidad_receptora);
		$modelSSProgramas = $this->loadModel($id_programa);
		$haySupervisorAsigando = $this->tieneSupervisorAsignado($id_programa);

		if(isset($_POST['SsSupervisoresProgramas']) or isset($_POST['HEmpleados']))
        {
			if($modelSSProgramas->id_tipo_programa == 1)
			{
				$modelHEmpleados->attributes=$_POST['HEmpleados'];
				$rfc = $modelHEmpleados->rfcEmpleado;

				if($this->supervisorInternoEstaDisponible($rfc))
				{
					$modelSSResponsableProgramaInterno->id_programa = $id_programa;
					$modelSSResponsableProgramaInterno->rfcEmpleado = $rfc;
					$modelSSResponsableProgramaInterno->fecha_asignacion = date('Y-m-d H:i:s');
					$modelSSResponsableProgramaInterno->superv_principal_int = true;

					if($modelSSResponsableProgramaInterno->save())
					{
						//Una vez se asigna un Supervisor al Programa pasa a estatus PENDIENTE el Programa
						if($modelSSProgramas->id_status_programa != 8)
						{
							$modelSSProgramas->id_status_programa = 8;
							$modelSSProgramas->save();
						}
						//Redireccionar a la misma Vista
						$this->redirect(array('asignacionSupervisoresProgramasDepto', 'id_programa'=>$id_programa, 'id_unidad_receptora'=>$id_unidad_receptora));

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.');
					//die("Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.");
				}

			}else
			if($modelSSProgramas->id_tipo_programa == 2){

				$modelSSSupervisoresProgramas->attributes=$_POST['SsSupervisoresProgramas'];
				$rfc = $modelSSSupervisoresProgramas->rfcSupervisor;

				if($this->supervisorExternoEstaDisponible($rfc))
				{
					$modelSSResponsableProgramaExterno->id_programa = $id_programa;
					$modelSSResponsableProgramaExterno->rfcSupervisor = $rfc;
					$modelSSResponsableProgramaExterno->fecha_asignacion = date('Y-m-d H:i:s');
					$modelSSResponsableProgramaExterno->superv_principal_ext = true;

					if($modelSSResponsableProgramaExterno->save())
					{
						//Una vez se asigna un Supervisor al Programa pasa a estatus PENDIENTE el Programa
						if($modelSSProgramas->id_status_programa != 8)
						{
							$modelSSProgramas->id_status_programa = 8;
							$modelSSProgramas->save();
						}
						//Redireccionar a la misma Vista
						$this->redirect(array('asignacionSupervisoresProgramasDepto', 'id_programa'=>$id_programa, 'id_unidad_receptora'=> $id_unidad_receptora));

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.');
					//die("Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.");
				}
			}
        }

		$this->render('asignacionSupervisoresProgramasDepto',array(
					  'modelSSSupervisoresProgramas'=>$modelSSSupervisoresProgramas,
					  'lista_supervisores' => $lista_supervisores,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelHEmpleados' => $modelHEmpleados,
					  'modelSSResponsableProgramaInterno' => $modelSSResponsableProgramaInterno,
					  'modelSSResponsableProgramaExterno' => $modelSSResponsableProgramaExterno,
					  'haySupervisorAsigando' => $haySupervisorAsigando

        ));
	}

	//Verificamos si el Programa ya tiene asigando un Supervisor
	public function tieneSupervisorAsignado($id_programa)
	{
		$bandera = false;
		$modelProgramas = $this->loadModel($id_programa);

		if($modelProgramas->id_tipo_programa == 1){ //INTERNO
			$query = "select * from pe_planeacion.ss_responsable_programa_interno where id_programa = '$modelProgramas->id_programa' ";
			$model = Yii::app()->db->createCommand($query)->queryAll();
			if($model != NULL)
				$bandera = true;

		}elseif($modelProgramas->id_tipo_programa == 2){ //EXTERNO
			$query = "select * from pe_planeacion.ss_responsable_programa_externo where id_programa = '$modelProgramas->id_programa' ";
			$model = Yii::app()->db->createCommand($query)->queryAll();
			if($model != NULL)
				$bandera = true;
		}

		return $bandera;
	}

	public function getSupervisorXEmpresa($id_programa, $id_unidad_receptora, $rfcSupervisor)
	{
		$modelSSProgramas = $this->loadModel($id_programa);

		$criteria = new CDbCriteria();

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$criteria->condition = " \"rfcEmpleado\" = '$rfcSupervisor' AND (\"statEmpleado\" = '02' OR \"statEmpleado\" = '06')";
			$modelProgramas = HEmpleados::model()->findAll($criteria);

			$lista_supervisores = CHtml::listData($modelProgramas, "rfcEmpleado", "rfcEmpleado");

		}else
		if($modelSSProgramas->id_tipo_programa == 2)
		{
			$criteria->condition = " \"rfcSupervisor\" = '$rfcSupervisor' AND id_status_supervisor = 1 AND id_unidad_receptora = '$id_unidad_receptora' ";
			$modelProgramas = SsSupervisoresProgramas::model()->findAll($criteria);

			$lista_supervisores = CHtml::listData($modelProgramas, "rfcSupervisor", "rfcSupervisor");
		}

		return $lista_supervisores;
	}

	public function getDeptos()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"cveDepartamento\" > 0  AND \"cveDepartamento\" < 40";
		$modelHDepartamentos = HDepartamentos::model()->find($criteria);

		return $lista_deptos = CHtml::listData($modelHDepartamentos, "cveDepartamento", "dscDepartamento");
	}

	public function getSupervisoresXEmpresa($id_programa, $id_unidad_receptora)
	{
		$modelSSProgramas = $this->loadModel($id_programa);

		$criteria = new CDbCriteria();
		$criteria2 = new CDbCriteria();

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$criteria->condition = " (\"statEmpleado\" = '02' OR \"statEmpleado\" = '06') AND \"rfcEmpleado\" is not null AND \"rfcEmpleado\" != 'PENDIENTE' ";
			$modelHEmpleados = HEmpleados::model()->findAll($criteria);

			//$lista_supervisores = CHtml::listData($lista, "rfcEmpleado", "rfcEmpleado");
			$lista_supervisores = CHtml::listData($modelHEmpleados, "rfcEmpleado", "rfcEmpleado");

		}else
		if($modelSSProgramas->id_tipo_programa == 2)
		{
			$criteria->condition = "id_status_supervisor = 1 AND id_unidad_receptora = '$id_unidad_receptora' ";
			$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findAll($criteria);

			$lista_supervisores = CHtml::listData($modelSSSupervisoresProgramas, "rfcSupervisor", "rfcSupervisor");
		}

		return $lista_supervisores;

	}

	public function actionMostrarDetalleSupervisorExterno()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$rfc_sup = Yii::app()->request->getParam('rfcSupervisor');

			//Verificamos que venga un rfc por ajax
			if($rfc_sup != null)
			{
				$criteria = new CDbCriteria();
				$criteria->condition = " \"rfcSupervisor\" = '$rfc_sup' AND id_status_supervisor = 1 ";
				$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->find($criteria);

				//$foto = $modelSSSupervisoresProgramas->rfcSupervisor;
				$nombre = $modelSSSupervisoresProgramas->nombre_supervisor;
				$apellido_pat = $modelSSSupervisoresProgramas->apell_paterno;
				$apellido_mat = $modelSSSupervisoresProgramas->apell_materno;
				$cargo = $modelSSSupervisoresProgramas->cargo_supervisor;
				$foto = $modelSSSupervisoresProgramas->foto_supervisor;

			}else{

				$nombre = "";
				$apellido_pat = "";
				$apellido_mat = "";
				$cargo = "";
				$foto = "";

			}

			// return data (JSON formatted)
			echo CJSON::encode(array(
				'nombre'=>$nombre,
				'apellido_pat' => $apellido_pat,
				'apellido_mat' => $apellido_mat,
				'cargo' => $cargo,
				'foto' => $foto,
				//'rfc_sup' => $rfc_sup
			));

			Yii::app()->end();

		}
	}

	public function actionMostrarDetalleSupervisorInterno()
	{
		if(Yii::app()->request->isAjaxRequest)
		{

			$rfc_sup = Yii::app()->request->getParam('rfcEmpleado');
			$departamento = "";

			//Verificamos que venga un rfc por ajax
			if($rfc_sup != null)
			{
				$criteria = new CDbCriteria();
				$criteria->condition = " \"rfcEmpleado\" = '$rfc_sup' ";
				$modelHEmpleados = HEmpleados::model()->find($criteria);

				//Departamento
				$id = $modelHEmpleados->cveDepartamentoEmp;
				$criteria2 = new CDbCriteria();
				$criteria2->condition = " \"cveDepartamento\" = '$id' ";
				$modelHDepartamentos = HDepartamentos::model()->findAll($criteria2);
				$lista_deptos = CHtml::listData($modelHDepartamentos, "cveDepartamento", "dscDepartamento");

				//$departamento = "<option value=''>--Departamento--</option>";
				foreach($lista_deptos as $value=>$dscDepartamento)
					$departamento .= CHtml::tag('option', array('value'=>$value),CHtml::encode($dscDepartamento),true);

				//$foto = $modelSSSupervisoresProgramas->rfcSupervisor;
				$nombre = $modelHEmpleados->nmbEmpleado;
				$apellido_pat = $modelHEmpleados->apellPaterno;
				$apellido_mat = $modelHEmpleados->apellMaterno;

			}else{

				$nombre = "";
				$apellido_pat = "";
				$apellido_mat = "";
				$departamento = "<option value=''>--Departamento--</option>";
			}

			// return data (JSON formatted)
			echo CJSON::encode(array(
				'nombre'=>$nombre,
				'apellido_pat' => $apellido_pat,
				'apellido_mat' => $apellido_mat,
				'departamento' => $departamento,
				//'rfc_sup' => $rfc_sup
			));
		}
	}

	/*SE ASIGNAN LOS DIAS DE LA SEMANA HABILES DEL PROGRAMA VISTA DEL SUPERVISOR*/
	public function actionNuevoHorarioDiasHabilesPrograma($id_programa)
	{
		/*Instancia del modelo SsHorarioDiasHabilesProgramas*/
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas_;

		/*Arreglo contiene dias insertados*/
		$dias_insertados = array();
		//Obtenemos los datos del programa actual a asignarle su horario
		$modelSSProgramas = $this->loadModel($id_programa);
		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramas();
		$lista_supervisores = $this->getSupervisores();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = $this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_dias = $this->getDiasSemana($id_programa);
		$hayRegHorario = $this->hayRegistroHorarioprograma($id_programa);

		if(isset($_POST['SsHorarioDiasHabilesProgramas']))
		{
			$modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];

			if($modelSSHorarioDiasHabilesProgramas->hora_inicio == null || $modelSSHorarioDiasHabilesProgramas->hora_fin == null || $modelSSHorarioDiasHabilesProgramas->id_dia_semana == null)
				throw new CHttpException(404,'Todos los campos deben ser completados.');

			if($modelSSHorarioDiasHabilesProgramas->hora_inicio >= $modelSSHorarioDiasHabilesProgramas->hora_fin)
				throw new CHttpException(404,'Error!!! La Hora de Inicio no debe ser mayor o igual a la Hora Fin.');

			$modelSSHorarioDiasHabilesProgramas->id_programa = $id_programa;
			$dias_insertados[] = $modelSSHorarioDiasHabilesProgramas->id_dia_semana;
			$modelSSHorarioDiasHabilesProgramas->horas_totales = $this->getTotalHorasXDia($modelSSHorarioDiasHabilesProgramas->hora_inicio, $modelSSHorarioDiasHabilesProgramas->hora_fin);

			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//Si se guardo al menos un registro de horario estatus del Programa pasa a PENDIENTE
				if($modelSSProgramas->id_status_programa != 1)
				{
					$modelSSProgramas->id_status_programa = 6; //Estatus PENDIENTE
					$modelSSProgramas->save();
				}
				$lista_dias = array_diff($lista_dias, $dias_insertados);
				$this->redirect(array('nuevoHorarioDiasHabilesPrograma', 'id_programa' => $id_programa));

			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevoHorarioDiasHabilesPrograma',array(
					  'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					  'modelSSProgramas' => $modelSSProgramas,
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_dias' => $lista_dias,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_supervisores' => $lista_supervisores,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'hayRegHorario' => $hayRegHorario
		));
	}
	/*SE ASIGNAN LOS DIAS DE LA SEMANA HABILES DEL PROGRAMA VISTA SUPERVISOR*/

	public function getTotalHorasXDia($_hora_inicio, $_hora_fin)
	{
		$f1 = new DateTime($_hora_inicio);
    	$f2 = new DateTime($_hora_fin);
		$d = $f1->diff($f2);
		
    	return $d->format('%H:%I:%S');
	}

	/*SE ASIGNAN LOS DIAS DE LA SEMANA HABILES DEL PROGRAMA VISTA ADMIN*/
	public function actionNuevoHorarioDiasHabilesProgramaDepto($id_programa)
	{
		/*Instancia del modelo SsHorarioDiasHabilesProgramas*/
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;

		/*Arreglo contiene dias insertados*/
		$dias_insertados = array();
		//Obtenemos los datos del programa actual a asignarle su horario
		$modelSSProgramas = $this->loadModel($id_programa);
		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramas();
		$lista_supervisores = $this->getSupervisores();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = $this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_dias = $this->getDiasSemana($id_programa);
		$hayRegHorario = $this->hayRegistroHorarioprograma($id_programa);

		if(isset($_POST['SsHorarioDiasHabilesProgramas']))
		{
			$modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];

			if($modelSSHorarioDiasHabilesProgramas->hora_inicio == null || $modelSSHorarioDiasHabilesProgramas->hora_fin == null || $modelSSHorarioDiasHabilesProgramas->id_dia_semana == null)
				throw new CHttpException(404,'Todos los campos deben ser completados.');

			if($modelSSHorarioDiasHabilesProgramas->hora_inicio > $modelSSHorarioDiasHabilesProgramas->hora_fin)
				throw new CHttpException(404,'Error!!! La Hora de Inicio debe ser menor a la Hora Fin.');

			$modelSSHorarioDiasHabilesProgramas->id_programa = $id_programa;
			$dias_insertados[] = $modelSSHorarioDiasHabilesProgramas->id_dia_semana;

			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//Si se guardo al menos un registro de horario estatus del Programa pasa a ALTA
				if($modelSSProgramas->id_status_programa != 1)
				{
					$modelSSProgramas->id_status_programa = 1; //Estatus LATA
					$modelSSProgramas->save();
				}
				$lista_dias = array_diff($lista_dias, $dias_insertados);
				$this->redirect(array('nuevoHorarioDiasHabilesProgramaDepto', 'id_programa' => $id_programa));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevoHorarioDiasHabilesProgramaDepto',array(
			'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
			'modelSSProgramas' => $modelSSProgramas,
			'lista_unidades_receptoras' => $lista_unidades_receptoras,
			'lista_dias' => $lista_dias,
			'lista_departamentos' => $lista_departamentos,
			'lista_periodos_programas' => $lista_periodos_programas,
			'lista_supervisores' => $lista_supervisores,
			'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
			'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
			'lista_tipo_apoyo' => $lista_tipo_apoyo,
			'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
			'hayRegHorario' => $hayRegHorario
		));

	}
	/*SE ASIGNAN LOS DIAS DE LA SEMANA HABILES DEL PROGRAMA VISTA ADMIN*/

	public function hayRegistroHorarioprograma($id_programa)
	{
		$criteria=new CDbCriteria;
		$criteria->condition = "id_programa = '$id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->find($criteria);

		return ($modelSSHorarioDiasHabilesProgramas === NULL) ? false : true;
	}

	/*EDITAR PROGRAMA DE PARTE DEL OFICINA DE SERVICIO SOCIAL */
	public function actionEditarDeptoProgramaServicioSocial($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';

		$modelSSProgramas=$this->loadModel($id_programa);
		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramas();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = $this->getTipoApoyoEconomicoE($modelSSProgramas->id_tipo_apoyo_economico); //$this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_dias = $this->getDiasSemana($id_programa);
		$lista_status = $this->getStatusPrograma();

		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_POST['SsProgramas'];
			$modelSSProgramas->fecha_modificacion_programa = date('Y-m-d H:i:s');

			if($modelSSProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;
				$this->redirect(array('listaProgramasAdminServicioSocial'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error al guardar los datos.');
			}

		}

		$this->render('nuevoProgramaServicioSocialDepto',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_status' => $lista_status,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'fecha_actualizacion' => GetFormatoFecha::getFechaLastUpdateProgramasMod($id_programa)
		));
	}
	/*EDITAR PROGRAMA DE PARTE DEL OFICINA DE SERVICIO SOCIAL */

	public function actionEditarSupervisorProgramaServicioSocial($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		$info_empleado = $this->infoEmpleado($rfcSupervisor);

		if(!$this->validarProgramaSupervisor($rfcSupervisor, $id_programa))
			throw new CHttpException(4044,'Este Programa No te corresponde.');

		$modelSSProgramas=$this->loadModel($id_programa);

		$id_tipo_supervisor = $this->getTipoSupervisor($rfcSupervisor);//Obtenemos si es un supervisor interno o externo
		$id_empresa = $this->getEmpresaSupervisor($rfcSupervisor);
		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramas();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = $this->getTipoApoyoEconomicoE($modelSSProgramas->id_tipo_apoyo_economico); //$this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_dias = $this->getDiasSemana($id_programa);
		$lista_status = $this->getStatusPrograma();

		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_POST['SsProgramas'];
			$modelSSProgramas->fecha_modificacion_programa = date('Y-m-d H:i:s');

			if($modelSSProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;
				$this->redirect(array('listaProgramasSupervisorServicioSocial'));

			}else{
				Yii::app()->user->setFlash('danger', 'Error al guardar los datos.');
			}

		}

		$this->render('nuevoProgramaServicioSocialSupervisor',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'info_empleado' => $info_empleado,
					  'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
					  'cargo' => $this->getCargo($info_empleado->rfcEmpleado),
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'id_tipo_supervisor'=> $id_tipo_supervisor,
					  'id_empresa' => $id_empresa,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'fecha_actualizacion' => GetFormatoFecha::getFechaLastUpdateProgramasMod($id_programa),
					  'lista_status' => $lista_status,
		));
	}

	/*El alumno selecciona el programa y se hacen las validaciones correspondientes antes
	de insertar el registro del alumno en la tabla solicitud_programa_servicio_social*/
	public function actionSeleccionarProgramaServicioSocial($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/ValidacionesServicioSocial.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoProgramas.php';

		//Instanciamos un objeto de SsSolicitudProgramaServicioSocial para insertar datos de la nueva solicitud
		$modelSSSolicitudProgramaServicioSocial = new SsSolicitudProgramaServicioSocial;

		//Obtener el no_ctrl del alumno
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		//Fecha de creacion de la Solicitud
		$fec_inscripcion = date('Y-m-d H:i:s');

		/*VERIFICAMOS QUE HAYA UN REGISTRO DEL HISTORICO DEL ALUMNO SOBRE EL SERVICIO SOCIAL, CONTEO DE SUS HORAS LIBERADAS*/
		if(ValidacionesServicioSocial::validaRegistroTotalDatosServicioSocial($no_ctrl))
		{
			/*VERIFICAMOS QUE LA SOLICITUD SE HAGA DENTRO DE UNA FECHA VALIDA*/
			if(ValidacionesServicioSocial::validarFechaLimiteInscripcion($fec_inscripcion))
			{
				/*VERIFICAMOS QUE EL ALUMNO TENGA DADO DE ALTA SU SERVICIO SOCIAL*/
				if(ValidacionesServicioSocial::validarAltaServicioSocialAlumno($no_ctrl))
				{
					/*VERIFICAMOS QUE SOLO PUEDA ENVIAR UNA SOLICITUD A UN PROGRAMA POR VEZ*/
					if(!ValidacionesServicioSocial::validarNoSolicitudesPendientesProgramasServicioSocial($no_ctrl))
					{
						/*Se crea el registro para la Solicitud de Servicio Social*/
						$modelSSSolicitudProgramaServicioSocial->no_ctrl = $no_ctrl;
						$modelSSSolicitudProgramaServicioSocial->id_programa = $id_programa;
						$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_supervisor = 1; //Default
						$modelSSSolicitudProgramaServicioSocial->id_estado_solicitud_programa_alumno = 1; //Default
						$modelSSSolicitudProgramaServicioSocial->fecha_solicitud_programa = $fec_inscripcion;

						/*Si llega hasta aca SE ENVIAN LOS DATOS DE LA SOLICITUD A LA TABLA solicitud_programa_servicio_social*/
						if($modelSSSolicitudProgramaServicioSocial->save())
						{
							//Redireccionamos a la vista de la aceptacion de la clasula de la Solicitud de Servicio Social
							$this->redirect(array('ssSolicitudProgramaServicioSocial/validaAlumnoClausulaSolicitudServicioSocial', 'id_solicitud_programa' => $modelSSSolicitudProgramaServicioSocial->id_solicitud_programa));

						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al guardar los datos.');
						}

					}else{

						Yii::app()->user->setFlash('danger', 'Solo puedes hacer una solicitud a un programa. Deben "Rechazar" tu solicitud enviada para poder enviar otra solicitud a otro programa.');
						//echo 'Solo puedes hacer una solicitud a un programa. Deben "Rechazar" tu solicitud enviada para poder enviar otra solicitud a otro programa.';
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Debes de dar de alta primero el Servicio Social. Acude al Depto. de Gestión y Vinculación.');
					//echo "Debes de dar de alta primero el Servicio Social. Acude al Depto. de Gestión y Vinculación.";
				}

			}else{

				Yii::app()->user->setFlash('danger', 'El tiempo de realizar solicitudes a los programas terminó.');
				//echo "El tiempo de realizar solicitudes a los programas terminó.";
			}

		}else{

			Yii::app()->user->setFlash('danger', 'No se encontró registro del historico de Servicio Social.');
			//echo "No se encontró registro del historico de Servicio Social.";
		}

	}

	public function actionSiHayApoyoEconomico()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$ngno = "";
			$valor = "";
			$criteria = new CDbCriteria();
			$tipos_apoyo = "";
			//$id_apoyo = $_POST['id_apoyo_economico_prestador'];
			$id_apoyo = Yii::app()->request->getParam('id_apoyo_economico_prestador');
			if($id_apoyo != NULL)
			{

				switch($id_apoyo)
				{
					case 1: //Cuando SI hay Apoyo
						$ngno = "-";
						$valor = "";
						$criteria->condition = " id_apoyo_economico_prestador_servicio_social = '$id_apoyo' ";
						$listOpcionesApoyosEconomicos = SsTiposApoyosEconomicos::model()->findAll($criteria);
						/*Devolveremos el dropdownlist el tipo de ayuda de servicio social y el texto */
						$lista_tipo = CHtml::listData($listOpcionesApoyosEconomicos, "id_tipo_apoyo_economico", "tipo_apoyo_economico");

						foreach($lista_tipo as $value=>$tipo_apoyo_economico)
							$tipos_apoyo .= CHtml::tag('option', array('value'=>$value),CHtml::encode($tipo_apoyo_economico),true);
					break;
					case 2: //Cuando NO hay Apoyo
						$ngno = "*";
						$valor = "NINGUNO";
						$criteria->condition = " id_apoyo_economico_prestador_servicio_social = '$id_apoyo' ";
						$listOpcionesApoyosEconomicos = SsTiposApoyosEconomicos::model()->findAll($criteria);
						/*Devolveremos el dropdownlist el tipo de ayuda de servicio social y el texto */
						$lista_tipo = CHtml::listData($listOpcionesApoyosEconomicos, "id_tipo_apoyo_economico", "tipo_apoyo_economico");

						foreach($lista_tipo as $value=>$tipo_apoyo_economico)
							$tipos_apoyo .= CHtml::tag('option', array('value'=>$value),CHtml::encode($tipo_apoyo_economico),true);
					break;
					case 3: //Cuando es OTRO tipo de Apoyo
						$ngno = "-";
						$valor = "";
						$criteria->condition = " id_apoyo_economico_prestador_servicio_social = '$id_apoyo' ";
						$listOpcionesApoyosEconomicos = SsTiposApoyosEconomicos::model()->findAll($criteria);
						/*Devolveremos el dropdownlist el tipo de ayuda de servicio social y el texto */
						$lista_tipo = CHtml::listData($listOpcionesApoyosEconomicos, "id_tipo_apoyo_economico", "tipo_apoyo_economico");

						foreach($lista_tipo as $value=>$tipo_apoyo_economico)
							$tipos_apoyo .= CHtml::tag('option', array('value'=>$value),CHtml::encode($tipo_apoyo_economico),true);
					break;
				}

			}else{
				$tipos_apoyo = "<option value=''>--Tipo Apoyo Económico--</option>";
				$ngno = "s";
				$valor = "";
			}

			echo CJSON::encode(array(
				'tipos_apoyo' => $tipos_apoyo,
				'ngno' => $ngno,
				'valor' => $valor
			));

			Yii::app()->end();
		}

	}

	public function actionListaProgramasAsignacionAlumnos()
	{
		$modelSSProgramas=new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  // clear any default values

		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_GET['SsProgramas'];
		}

		$this->render('listaProgramasAsignacionAlumnos', array(
					  'modelSSProgramas'=>$modelSSProgramas
		));
	}

	//Se asignan tanto las horas a liberar como el numero de alumnos para el Programa de Servicio Social
	public function actionAsignarAlumnosProgramaServicioSocial($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';
		$modelSSProgramas = new SsProgramas;
		$modelSSProgramasAnterior = $this->loadModel($id_programa);
		$bandera = false;

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		//Nombre Completo del supervisor
		$supervisor = $this->getNombreSupervisor($modelSSProgramasAnterior->id_programa, $modelSSProgramasAnterior->id_tipo_programa);
		//Periodo del Programa
		$periodo = $this->getPeriodoPrograma($modelSSProgramasAnterior->id_periodo_programa);
		//Formato de la fecha de inicio
		$fec_ini = $this->getFormatoFecha($modelSSProgramasAnterior->fecha_inicio_programa);
		//Formato de la fecha fin
		$fec_fin = $this->getFormatoFecha($modelSSProgramasAnterior->fecha_fin_programa);

		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_POST['SsProgramas'];

			try
			{
				//El numero de estudiantes debe ser un entero
				if(RegularExpression::isEnteroPositivo($modelSSProgramas->numero_estudiantes_solicitados))
				{
					if(RegularExpression::isEnteroPositivo($modelSSProgramas->horas_totales) AND $modelSSProgramas->horas_totales <= 480)
					{
						$modelSSProgramasAnterior->numero_estudiantes_solicitados = $modelSSProgramas->numero_estudiantes_solicitados;
						//Actualizar campo lugares_disponibles
						$modelSSProgramasAnterior->lugares_disponibles = $modelSSProgramas->numero_estudiantes_solicitados;
						$modelSSProgramasAnterior->horas_totales = $modelSSProgramas->horas_totales;

						/**Restriccion para permitir un maximo de 100 alumnos por programa */
						if($modelSSProgramas->numero_estudiantes_solicitados > 0 AND $modelSSProgramas->numero_estudiantes_solicitados <= 100)
						{
							$modelSSProgramasAnterior->fecha_modificacion_programa = date('Y-m-d H:i:s');
							//Si se asigno todo correctamente entonces pasa de PENDIENTE (6) a estado de NO HORARIO (8)
							$modelSSProgramasAnterior->id_status_programa = 8; //NO HORARIO 
							$modelSSProgramasAnterior->save();
							Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
							//Si se realizo correctamente el cambio
							$transaction->commit();
							$this->redirect(array('listaProgramasAsignacionAlumnos'));

						}else{

							Yii::app()->user->setFlash('danger', 'Solo se puede asignar un maximo de 100 alumnos por Programa.');
							//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
							$transaction->rollback();
							//die('Solo se puede asignar un maximo de 10 alumnos por programa.');
						}

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! Las Horas asignadas al Programa debe ser un entero Positivo Valido.');
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
						//die('Error!!! Las Horas a liberar deben ser un entero positivo y no mayor a 480 hrs.');
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! El Numero de estudiantes debe ser un entero positivo.');
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					//die('Error!!! El Numero de estudiantes debe ser un entero positivo.');
				}

			}catch(Exception $e)
			{
				Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al registrar la información del Programa');
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
			}

		}

		$this->render('asignarAlumnosProgramaServicioSocial', array(
					  'modelSSProgramas' => $modelSSProgramasAnterior,
					  'supervisor' => $supervisor,
					  'periodo' => $periodo,
					  'fec_ini' => $fec_ini,
					  'fec_fin' => $fec_fin,
					  'bandera' => $bandera

		));

	}

	/*Sumar lugares en caso de aumentar el espacio en algun programa*/
	public function getCambioLugaresPrograma($noEstAnt, $noEstAct)
	{
		$sum = $noEstAnt + $noEstAct;

		return $sum;
	}
	/*Sumar lugares en caso de aumentar el espacio en algun programa*/

	/*El programa se CANCELA de manera DEFINITIVA*/
	public function actionEliminarProgramaServicioSocial($id_programa)
	{
		//Nos traemos los datos de ese Programa
		$modelSSProgramas = $this->loadModel($id_programa);

		/*HAY DATOS DEL PROGRAMA ENTONCES SE PROCEDE A CANCELAR*/
		if ($modelSSProgramas)
		{
			//Asigna el status FINALIZAR
			$modelSSProgramas->id_status_programa = 4; //CANCELACION

			if ( $modelSSProgramas->save() )
				echo CJSON::encode( [ 'code' => 200 ] );
			else
				echo CJSON::encode( $modelSSProgramas->getErrors() );

		}
		/*HAY DATOS DEL PROGRAMA ENTONCES SE PROCEDE A CANCELAR*/
	}
	/*El programa se CANCELA de manera DEFINITIVA*/

	/*El programa FINALIZA, se llega su fecha fin*/
	public function actionFinalizarProgramaServicioSocial($id_programa)
	{

		$modelSSProgramas = $this->loadModel($id_programa);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			//Asigna el status FINALIZAR
			$modelSSProgramas->id_status_programa = 3; //FINALIZA

			if($modelSSProgramas->save())
			{
				Yii::app()->user->setFlash('success', "Programa Finalizado correctamente!!!");
				//Si se realizo correctamente el cambio
				$transaction->commit();
				echo CJSON::encode( [ 'code' => 200 ] );
			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Finalizar el Programa.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode($modelSSProgramas->getErrors());
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! no se pudo Finalizar el Programa.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode($modelSSProgramas->getErrors());
		}

	}
	/*El programa FINALIZA, se llega su fecha fin*/

	public function actionMostrarTipoEmpresas()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$criteria = new CDbCriteria();
			$tipo_empresa = Yii::app()->request->getParam('id_tipo_programa');

			if($tipo_empresa != NULL)
			{
				$criteria->condition = "id_tipo_empresa = '$tipo_empresa' ";
				$listaEmpresas = SsUnidadesReceptoras::model()->findAll($criteria);
				/*Devolveremos el dropdownlist el tipo de supervisor seleccionado segun el tipo de empresa (INTERNO O EXTERNO) y el texto */
				$lista_empresas = CHtml::listData($listaEmpresas, "id_unidad_receptora", "nombre_unidad_receptora");

				$empresas = "<option value=''>-- Empresas --</div>";
				foreach($lista_empresas as $value=>$nombre_unidad_receptora)
					$empresas .= CHtml::tag('option', array('value'=>$value), CHtml::encode($nombre_unidad_receptora),true);

			}else{

				$empresas = "<option value=''>-- Empresas --</div>";
			}

			// return data (JSON formatted)
			echo CJSON::encode(array(
				'empresas'=>$empresas,
			));

			Yii::app()->end();
		}

	}

	/*Si es programa externo o interno solo mostrar ese tipo de supervisores para elegir uno*/
	public function actionMostrarTipoSupervisor()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$criteria = new CDbCriteria();
			$tipo_supervisor = Yii::app()->request->getParam('id_tipo_programa'); //Si es Interno o Externo

			//Interno
			if($tipo_supervisor == 1)
			{
				$criteria->condition = " (\"rfcEmpleado\" is not null AND \"nmbEmpleado\" is not null) ";
				$listSupervisoresInternos = HEmpleados::model()->findAll($criteria);
				$lista_superviores_int = CHtml::listData($listSupervisoresInternos, "rfcEmpleado", "rfcEmpleado");

				if($listSupervisoresInternos != null)
				{
					//Si hay supervisores externos
					foreach ($listSupervisoresInternos as $model)
						$lista_tipo_supervisor[$model->rfcEmpleado] = $model->rfcEmpleado.' '.$model->nmbEmpleado.' '.$model->apellPaterno.' '.$model->apellMaterno;

					$supervisores = "<option value=''>-- Seleccionar Superviores --</option>";
					foreach($lista_tipo_supervisor as $value=>$rfcEmpleado)
					{
						$supervisores .= CHtml::tag('option', array('value'=>$value),CHtml::encode($rfcEmpleado),true);
					}

				}else{

					//Si no hay supervisores externos
					$supervisores = "<option value=''>-- Seleccionar Supervisores Internos --</option>";
					foreach($lista_superviores_int as $value=>$rfcEmpleado)
						$supervisores .= CHtml::tag('option', array('value'=>$value),CHtml::encode($rfcEmpleado),true);
				}

			}else
			if($tipo_supervisor == 2){ //Externo

				//status 1 son los supervisores externos que estan dados de alta y disponibles (no tienen programa a cargo)
				$criteria->condition = "id_status_supervisor = 1";
				$listSupervisores = SsSupervisoresProgramas::model()->findAll($criteria);
				$lista_superviores_ext = CHtml::listData($listSupervisores, "rfcSupervisor", "rfcSupervisor");

				if($listSupervisores != null)
				{
					//Si hay supervisores externos
					foreach ($listSupervisores as $model)
						$lista_tipo_supervisor[$model->rfcSupervisor] = $model->rfcSupervisor.' '.$model->nombre_supervisor . ' '. $model->apell_paterno. ' '.$model->apell_materno;

					$supervisores = "<option value=''>-- Seleccionar Superviores Externos--</option>";
					foreach($lista_tipo_supervisor as $value=>$rfcSupervisor)
					{
						$supervisores .= CHtml::tag('option', array('value'=>$value),CHtml::encode($rfcSupervisor),true);
					}

				}else{

					//Si no hay supervisores externos
					$supervisores = "<option value=''>-- Seleccionar Supervisores --</option>";
					foreach($lista_superviores_ext as $value=>$rfcSupervisor)
						$supervisores .= CHtml::tag('option', array('value'=>$value),CHtml::encode($rfcSupervisor),true);

				}

			}

			// return data (JSON formatted)
			echo CJSON::encode(array(
				'supervisores'=>$supervisores,
			));

			Yii::app()->end();
		}


	}
	/*Si es programa externo o interno solo mostrar ese tipo de supervisores para elegir */

	/*Lista de Programas ExtraTemporales*/
	public function actionListaProgramasExtraTemporales()
	{
		$modelSSProgramas = new SsProgramas_('search');
		$modelSSProgramas->unsetAttributes();  // clear any default values

		$id_registro_fechas_ssocial = $this->getRegistroFechasServicioSocial();

		$fecha_limite = $this->getFechaFinPrograma();

		if(isset($_GET['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_GET['SsProgramas'];
		}

        $this->render('listaProgramasExtraTemporales',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'id_registro_fechas_ssocial' => $id_registro_fechas_ssocial,
					  'fecha_limite' => $fecha_limite,
					  'fecha_actual' => date('Y-m-d')
        ));
	}
	/*Lista de Programas ExtraTemporales*/

	/*programas que se toman menos de seis meses, llamados Unica Vez */
	public function actionNuevoProgramaExtraTemporal($id_registro_fechas_ssocial)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';
		$modelSSProgramas = new SsProgramas;

		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramasExtratemporales();
		$lista_supervisores = array(); //$this->getSupervisores();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = array();//$this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_status = $this->getStatusPrograma();

		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramas->attributes=$_POST['SsProgramas'];

			if(RegularExpression::isEnteroPositivo($modelSSProgramas->horas_totales))
			{
				if(RegularExpression::isEnteroPositivo($modelSSProgramas->numero_estudiantes_solicitados))
				{
					if($modelSSProgramas->fecha_inicio_programa > $modelSSProgramas->fecha_fin_programa)
						throw new CHttpException(404,'Error!!! La Fecha de Inicio debe ser menor a la Fecha Fin del Programa ExtraTemporal.');

					if($modelSSProgramas->id_apoyo_economico_prestador == 2 AND $modelSSProgramas->id_tipo_apoyo_economico == 4)
						$modelSSProgramas->apoyo_economico = "NINGUNO";

					$modelSSProgramas->fecha_registro_programa = date('Y-m-d H:i:s');
					$modelSSProgramas->fecha_modificacion_programa = $modelSSProgramas->fecha_registro_programa;
					//El programa cuando se crea estara por default en status de ALTA, en editar programa se podra dar de baja
					$modelSSProgramas->id_status_programa = 7; //Pasa a estatus ALTA cuando se le agregue el horario al Programa
					//Ligar con el registro del servicio social actual
					$modelSSProgramas->id_registro_fechas_ssocial = $id_registro_fechas_ssocial;
					//Lugares ocupados por defualt es cero ya que apenas se creo el Programa
					$modelSSProgramas->lugares_ocupados = 0;
					//lugares disponibles
					$modelSSProgramas->lugares_disponibles = $modelSSProgramas->numero_estudiantes_solicitados;
					//Se calcula la fecha fin del programa Estratemporal sumandole 2 meses a la fecha de inicio
					//$modelSSProgramas->fecha_fin_programa = $this->getFechaFinProgramaExtraTemporal($modelSSProgramas->fecha_inicio_programa);

					if($modelSSProgramas->save())
					{
						Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
						$this->redirect(array('asignacionSupervisoresProgramaEstraTemporal', 'id_programa'=>$modelSSProgramas->id_programa, 'id_unidad_receptora' => $modelSSProgramas->id_unidad_receptora));

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
					}
				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! El número de estudiantes solicitados debe ser un número entero positivo.');
					//die('Error!!! El número de estudiantes solicitados debe ser un número entero positivo.');
				}
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! Las Horas a completar en el Programa debe ser un número entero positivo.');
				//die('Error!!! Las Horas a completar en el Programa debe ser un número entero positivo.');
			}

    	}

        $this->render('nuevoProgramaExtraTemporal',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_supervisores' => $lista_supervisores,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'lista_status' => $lista_status
        ));
	}
	/*programas que se toman menos de seis meses, llamados Unica Vez */

	public function actionAsignacionSupervisoresProgramaEstraTemporal($id_programa, $id_unidad_receptora)
	{
		$modelSSSupervisoresProgramas = new SsSupervisoresProgramas;
		$modelSSResponsableProgramaExterno = new SsResponsableProgramaExterno;
		$modelSSResponsableProgramaInterno = new SsResponsableProgramaInterno;
		$modelHEmpleados = new HEmpleados;

		$modelSSProgramas = $this->loadModel($id_programa);
		$lista_supervisores = $this->getSupervisoresXEmpresa($id_programa, $id_unidad_receptora);
		$haySupervisorAsigando = $this->tieneSupervisorAsignado($id_programa);

		if(isset($_POST['SsSupervisoresProgramas']) or isset($_POST['HEmpleados']))
        {
			if($modelSSProgramas->id_tipo_programa == 1)
			{
				$modelHEmpleados->attributes=$_POST['HEmpleados'];
				$rfc = $modelHEmpleados->rfcEmpleado;

				if($this->supervisorInternoEstaDisponible($rfc))
				{
					$modelSSResponsableProgramaInterno->id_programa = $id_programa;
					$modelSSResponsableProgramaInterno->rfcEmpleado = $rfc;
					$modelSSResponsableProgramaInterno->fecha_asignacion = date('Y-m-d H:i:s');
					$modelSSResponsableProgramaInterno->superv_principal_int = true;

					if($modelSSResponsableProgramaInterno->save())
					{
						$modelSSProgramas->id_status_programa = 8; //Estatus SIN HORARIO
						$modelSSProgramas->save();
						//Redireccionar a la misma Vista
						$this->redirect(array('asignacionSupervisoresProgramaEstraTemporal', 'id_programa'=>$id_programa, 'id_unidad_receptora'=>$id_unidad_receptora));

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
					}
				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.');
					//die("Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.");
				}

			}else
			if($modelSSProgramas->id_tipo_programa == 2){

				$modelSSSupervisoresProgramas->attributes=$_POST['SsSupervisoresProgramas'];
				$rfc = $modelSSSupervisoresProgramas->rfcSupervisor;

				if($this->supervisorExternoEstaDisponible($rfc))
				{
					$modelSSResponsableProgramaExterno->id_programa = $id_programa;
					$modelSSResponsableProgramaExterno->rfcSupervisor = $rfc;
					$modelSSResponsableProgramaExterno->fecha_asignacion = date('Y-m-d H:i:s');
					$modelSSResponsableProgramaExterno->superv_principal_ext = true;

					if($modelSSResponsableProgramaExterno->save())
					{
						//Redireccionar a la misma Vista
						$this->redirect(array('asignacionSupervisoresProgramaEstraTemporal', 'id_programa'=>$id_programa, 'id_unidad_receptora'=> $id_unidad_receptora));

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
					}
				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.');
					//die("Error!!! este Supervisor ya tiene un Programa a su cargo actualmente. Elige otro Supervisor.");
				}

			}
		}

		$this->render('asignacionSupervisoresProgramaEstraTemporal',array(
					  'modelSSSupervisoresProgramas'=>$modelSSSupervisoresProgramas,
					  'lista_supervisores' => $lista_supervisores,
					  'haySupervisorAsigando' => $haySupervisorAsigando,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelHEmpleados' => $modelHEmpleados,
					  'modelSSResponsableProgramaInterno' => $modelSSResponsableProgramaInterno,
					  'modelSSResponsableProgramaExterno' => $modelSSResponsableProgramaExterno

		));

	}

	public function supervisorInternoEstaDisponible($rfcSupervisor)
	{
		$bandera = false;
		$criteria = new CDbCriteria();
		$criteria->alias = "rpi";
		$criteria->select = "*";
		$criteria->join = "join pe_planeacion.ss_programas pss on pss.id_programa = rpi.id_programa";
		$criteria->condition = "rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_status_programa = 1";

		$modelSSResponsableProgramaInterno = SsResponsableProgramaInterno::model()->find($criteria);
		if($modelSSResponsableProgramaInterno === NULL)
			$bandera = true;

		return $bandera;
	}

	public function supervisorExternoEstaDisponible($rfcSupervisor)
	{
		$bandera = false;
		$criteria = new CDbCriteria();
		$criteria->alias = "rpe";
		$criteria->select = "*";
		$criteria->join = "JOIN pe_planeacion.ss_programas pss ON pss.id_programa = rpe.id_programa";
		$criteria->condition = "rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND (pss.id_status_programa != 2 AND pss.id_status_programa != 3 AND pss.id_status_programa != 4)";

		$modelSSResponsableProgramaExterno = SsResponsableProgramaExterno::model()->find($criteria);
		if($modelSSResponsableProgramaExterno === NULL)
			$bandera = true;

		return $bandera;
	}

	/*Se calcula la fecha de fin del programa extratemporal */
	public function getFechaFinProgramaExtraTemporal($fecha_inicio_programa)
	{

		$nuevafecha = strtotime ( '+2 month' , strtotime ($fecha_inicio_programa));
		$fecha_fin = date ( 'Y-m-d' , $nuevafecha );

		return $fecha_fin;
	}

	/*Editar el programa extratemporal, menos las fecha de inicio y fin del programa*/
	public function actionEditarProgramaExtraTemporal($id_programa)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
		$modelSSProgramas=$this->loadModel($id_programa);

		//Detalle del programa ExtraTemporal
		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramasExtratemporales();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = $this->getTipoApoyoEconomicoE($modelSSProgramas->id_tipo_apoyo_economico); //$this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_dias = $this->getDiasSemana($id_programa);
		$lista_status = $this->getStatusPrograma();

        if(isset($_POST['SsProgramas']))
        {
			$modelSSProgramas->attributes=$_POST['SsProgramas'];
			$modelSSProgramas->fecha_modificacion_programa = date('Y-m-d H:i:s');

			if($modelSSProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;
				$this->redirect(array('listaProgramasExtraTemporales'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error al guardar los datos.');
			}

        }

        $this->render('nuevoProgramaExtraTemporal',array(
					  'modelSSProgramas'=>$modelSSProgramas,
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_status' => $lista_status,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'fecha_actualizacion' => GetFormatoFecha::getFechaLastUpdateProgramasMod($id_programa)
		));
	}
	/*Editar el programa extratemporal, menos las fecha de inicio y fin del programa*/

	/*Horarios de los programas ExtraTemporales */
	public function actionNuevoHorarioDiasHabilesProgramaExtraTemporal($id_programa)
	{
		/*Instancia del modelo SsHorarioDiasHabilesProgramas*/
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;
		/*Arreglo contiene dias insertados*/
		$dias_insertados = array();
		//Obtenemos los datos del programa actual a asignarle su horario
		$modelSSProgramas = $this->loadModel($id_programa);
		$lista_unidades_receptoras = $this->getUnidadesReceptoras();
		$lista_departamentos = $this->getDepartamentos();
		$lista_periodos_programas = $this->getPeriodosProgramas();
		$lista_supervisores = $this->getSupervisores();
		$lista_tipo_servicio_social = $this->getTipoServicioSocial();
		$ofrece_apoyo_economico = $this->getOfreceApoyoEconomico();
		$lista_tipo_apoyo = $this->getTipoApoyoEconomico();
		$lista_clasificacion_areas_serv_social = $this->getClasificacionAreas();
		$lista_dias = $this->getDiasSemana($id_programa);
		$hayRegHorario = $this->hayRegistroHorarioprograma($id_programa);

		if(isset($_POST['SsHorarioDiasHabilesProgramas']))
		{
			$modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];

			if($modelSSHorarioDiasHabilesProgramas->hora_inicio == null || $modelSSHorarioDiasHabilesProgramas->hora_fin == null || $modelSSHorarioDiasHabilesProgramas->id_dia_semana == null)
				throw new CHttpException(404,'Todos los campos deben ser completados.');

			if($modelSSHorarioDiasHabilesProgramas->hora_inicio > $modelSSHorarioDiasHabilesProgramas->hora_fin)
				throw new CHttpException(404,'La Hora de Inicio debe ser menor a la Hora Fin.');

			$modelSSHorarioDiasHabilesProgramas->id_programa = $id_programa;
			$dias_insertados[] = $modelSSHorarioDiasHabilesProgramas->id_dia_semana;

			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//Si se guardo al menos un registro de horario estatus del Programa pasa a ALTA
				if($modelSSProgramas->id_status_programa != 1)
				{
					$modelSSProgramas->id_status_programa = 1; //Estatus LATA
					$modelSSProgramas->save();
				}
				$lista_dias = array_diff($lista_dias, $dias_insertados);
				$this->redirect(array('nuevoHorarioDiasHabilesProgramaExtraTemporal','id_programa' => $id_programa));

			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

		}

		$this->render('nuevoHorarioDiasHabilesProgramaExtraTemporal',array(
					  'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					  'modelSSProgramas' => $modelSSProgramas,
					  'lista_unidades_receptoras' => $lista_unidades_receptoras,
					  'lista_dias' => $lista_dias,
					  'lista_departamentos' => $lista_departamentos,
					  'lista_periodos_programas' => $lista_periodos_programas,
					  'lista_supervisores' => $lista_supervisores,
					  'lista_tipo_servicio_social' => $lista_tipo_servicio_social,
					  'ofrece_apoyo_economico' => $ofrece_apoyo_economico,
					  'lista_tipo_apoyo' => $lista_tipo_apoyo,
					  'lista_clasificacion_areas_serv_social' => $lista_clasificacion_areas_serv_social,
					  'hayRegHorario' => $hayRegHorario
		));

	}
	/*Horarios de los programas ExtraTemporales */

	/*Detalle de los programas extratemporales*/
	public function actionDetalleProgramaExtraTemporal($id_programa)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		$this->render('detalleProgramaExtraTemporal',array(
					'modelSSProgramas'=>$this->loadModel($id_programa),
					'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,

		));
	}
	/*Detalle de los programas extratemporales*/

	public function actionAddLugaresProgramaServicioSocial($id_programa)
	{
		//Para validar numero de lugares sea entero
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		$modelSSProgramasActual = SsProgramas::model()->findByPk($id_programa);

		if($modelSSProgramasActual === null)
			throw new CHttpException(404,'No se encontraron datos del Programa.');

		//Informacion del Programa y su Supervisor
		$nombre_supervisor = $this->getNombreSupervisor($modelSSProgramasActual->id_programa, $modelSSProgramasActual->id_tipo_programa);
		$rfcSupervisor = $this->getRFCSupervisor($modelSSProgramasActual->id_programa, $modelSSProgramasActual->id_tipo_programa);
		$periodo_programa = $this->getPeriodoPrograma($modelSSProgramasActual->id_periodo_programa);
		$fec_inicio = $this->getFormatoFecha($modelSSProgramasActual->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSSProgramasActual->fecha_fin_programa);
		$est_sol = $modelSSProgramasActual->numero_estudiantes_solicitados;

		$modelSSProgramasNuevo = new SsProgramas; //Guarda los lugares agregados al Programa

		//Vinen datos por POST
		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramasNuevo->attributes=$_POST['SsProgramas'];

			if(RegularExpression::isEnteroPositivo($modelSSProgramasNuevo->numero_estudiantes_solicitados))
			{
				//Aumentar los lugares disponibles dependiendo del numero de estudiantes solicitados
				$lugares = $modelSSProgramasActual->numero_estudiantes_solicitados + $modelSSProgramasNuevo->numero_estudiantes_solicitados; // 6 + 4 = 10
				$modelSSProgramasActual->lugares_disponibles =  $modelSSProgramasActual->lugares_disponibles + $modelSSProgramasNuevo->numero_estudiantes_solicitados;// 10 - 8 = 6
				$modelSSProgramasActual->numero_estudiantes_solicitados = $lugares;

				if($modelSSProgramasActual->save())
				{
					Yii::app()->user->setFlash('success', 'Lugares Agregados Correctamente!!!');
					$this->redirect(array('listaProgramasAdminServicioSocial'));

				}else{
					Yii::app()->user->setFlash('danger', 'Error!!! no se agregaron los lugares.');
				}
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! el valor debe ser un entero positivo.');
			}

		}else{
			$modelSSProgramasActual->numero_estudiantes_solicitados = 0;
		}

		$this->render('addLugaresProgramaServicioSocial',array(
					  'modelSSProgramasActual'=> $modelSSProgramasActual,
					  'nombre_supervisor' => $nombre_supervisor,
					  'rfcSupervisor' => $rfcSupervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'est_sol' => $est_sol

		));
	}

	//Eliminar Lugares disponibles a un determinado Programa
	public function actionDelLugaresProgramaServicioSocial($id_programa)
	{
		//Para validar numero de lugares sea entero
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		$modelSSProgramasActual = SsProgramas::model()->findByPk($id_programa);

		if($modelSSProgramasActual === null)
			throw new CHttpException(404,'No se encontraron datos del Programa.');

		//Informacion del Programa y su Supervisor
		$nombre_supervisor = $this->getNombreSupervisor($modelSSProgramasActual->id_programa, $modelSSProgramasActual->id_tipo_programa);
		$rfcSupervisor = $this->getRFCSupervisor($modelSSProgramasActual->id_programa, $modelSSProgramasActual->id_tipo_programa);
		$periodo_programa = $this->getPeriodoPrograma($modelSSProgramasActual->id_periodo_programa);
		$fec_inicio = $this->getFormatoFecha($modelSSProgramasActual->fecha_inicio_programa);
		$fec_fin = $this->getFormatoFecha($modelSSProgramasActual->fecha_fin_programa);
		$est_sol = $modelSSProgramasActual->numero_estudiantes_solicitados;

		$modelSSProgramasNuevo = new ssProgramas; //Para guardar los lugares que se va a aeliminar a un Programa

		//Vinen datos por POST
		if(isset($_POST['SsProgramas']))
		{
			$modelSSProgramasNuevo->attributes=$_POST['SsProgramas'];

			//Validar que el valor que viene por POST sea un valor enterno
			if(RegularExpression::isEnteroPositivo($modelSSProgramasNuevo->numero_estudiantes_solicitados))
			{
				//Verificar que haya lugares disponibles para eliminar
				if($modelSSProgramasActual->lugares_disponibles > 0)
				{
					//Validar los lugares que se pueden eliminar dependiendo del numero de lugares disponibles que tiene el Programa
					if($modelSSProgramasActual->lugares_disponibles >= $modelSSProgramasNuevo->numero_estudiantes_solicitados)
					{
						//Calcular los lugares disponibles en el programa y los lugares solicitados
						$modelSSProgramasActual->lugares_disponibles = $modelSSProgramasActual->lugares_disponibles - $modelSSProgramasNuevo->numero_estudiantes_solicitados;
						$modelSSProgramasActual->numero_estudiantes_solicitados = $modelSSProgramasActual->numero_estudiantes_solicitados - $modelSSProgramasNuevo->numero_estudiantes_solicitados;

						if($modelSSProgramasActual->save())
						{
							Yii::app()->user->setFlash('success', 'Lugares Elimnados Correctamente!!!');
							$this->redirect(array('listaProgramasAdminServicioSocial'));

						}else{
							Yii::app()->user->setFlash('danger', 'Error!!! no se agregaron los lugares.');
							//die("Error!!! no se agregaron los lugares.");

						}

					}else{
						Yii::app()->user->setFlash('danger', 'Error!!! No puedes Eliminar mas lugares de los que tienes Disponibles.');
						//die("Error!!! No puedes Eliminar mas lugares de los que tienes Disponibles.");

					}

				}else{
					Yii::app()->user->setFlash('danger', 'Error!!! El Programa debe tener lugares disponibles para que puedas Eliminar.');
					//die("Error!!! El Programa debe tener lugares disponibles para que puedas Eliminar.");

				}
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! el valor debe ser un entero positivo.');
				//die("Error!!! el valor debe ser un entero positivo.");

			}

		}else{
			$modelSSProgramasActual->numero_estudiantes_solicitados = 0;
		}

		$this->render('delLugaresProgramaServicioSocial', array(
					  'modelSSProgramasActual' => $modelSSProgramasActual,
					  'nombre_supervisor' => $nombre_supervisor,
					  'rfcSupervisor' => $rfcSupervisor,
					  'periodo_programa' => $periodo_programa,
					  'fec_inicio' => $fec_inicio,
					  'fec_fin' => $fec_fin,
					  'est_sol' => $est_sol

		));
	}

	//Mensaje de bienvenida del SUPERVISOR para el Alumno
	public function ObservacionDeBienvenidaSupervisorAlAlumno($id_programa, $id_servicio_social, $no_ctrl, $rfcSupervisor)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);

		//Observacion de Bienvenida del Supervisor
		$ObservacionesServicioSocial = new SsObservacionesServicioSocial;
		$ObservacionesServicioSocial->id_servicio_social = $id_servicio_social;
		$ObservacionesServicioSocial->observacion = "BIENVENIDO seas y mucha suerte te desea ".$this->getDatosSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa)." Supervisor del Programa.";
		$ObservacionesServicioSocial->fecha_registro = date('Y-m-d H:i:s');
		$ObservacionesServicioSocial->tipo_observacion_emisor = 2; //Quien la realiza
		$ObservacionesServicioSocial->tipo_observacion_receptor = 3; //Quien la recibe
		$ObservacionesServicioSocial->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
		$ObservacionesServicioSocial->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
		$ObservacionesServicioSocial->emisor_observ = $rfcSupervisor;
		$ObservacionesServicioSocial->receptor_observ = $no_ctrl;

		if(!$ObservacionesServicioSocial->save()){
			throw new CHttpException(404,'Ocurrio un error al guardar los datos.');
		}
	}
	//Mensaje de bienvenida del SUPERVISOR para el Alumno

	//Mensaje de bienvenida del JEFE OF SERVICIO SOCIAL para el Alumno
	public function ObservacionDeBienvenidaDeptoAlAlumno($id_programa, $id_servicio_social, $no_ctrl, $rfcJefDeptoVinc)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);

		//Observacion de Bienvenida del Supervisor
		$ObservacionesServicioSocial = new SsObservacionesServicioSocial;
		$ObservacionesServicioSocial->id_servicio_social = $id_servicio_social;
		$ObservacionesServicioSocial->observacion = "BIENVENIDO seas y mucha suerte te desea ".$this->getNombreJefeOfServicioSocial(). " Jefe de la Oficina de Servicio Social.";
		$ObservacionesServicioSocial->fecha_registro = date('Y-m-d H:i:s');
		$ObservacionesServicioSocial->tipo_observacion_emisor = 1; //Quien la realiza
		$ObservacionesServicioSocial->tipo_observacion_receptor = 3; //Quien la recibe
		$ObservacionesServicioSocial->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
		$ObservacionesServicioSocial->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
		$ObservacionesServicioSocial->emisor_observ = $rfcJefDeptoVinc;
		$ObservacionesServicioSocial->receptor_observ = $no_ctrl;

		if(!$ObservacionesServicioSocial->save()){
			throw new CHttpException(404,'Ocurrio un error al guardar los datos.');
		}
	}
	//Mensaje de bienvenida del JEFE OF SERVICIO SOCIAL para el Alumno

	public function getNombreJefeOfServicioSocial()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '04' AND \"cvePuestoParticular\" = '02' AND \"cveDepartamentoEmp\" = '06' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['nmbCompletoEmp'];
	}

	/*Restar lugar_disponible del programa donde se acaba de ACEPTAR al ALUMNO*/
	public function restarLugarDisponibleEnPrograma($modelSSProgramas)
	{
		if($modelSSProgramas->lugares_disponibles == 0)
		{
			$modelSSProgramas->numero_estudiantes_solicitados = $modelSSProgramas->numero_estudiantes_solicitados + 1;
			$modelSSProgramas->lugares_disponibles = $modelSSProgramas->lugares_disponibles + 1;
			$modelSSProgramas->save();
		}

		$modelSSProgramas->lugares_disponibles = $modelSSProgramas->lugares_disponibles - 1;

		if($modelSSProgramas->lugares_disponibles == 0 OR $modelSSProgramas->lugares_disponibles < 0)
			$modelSSProgramas->lugares_disponibles = 0;

		/*Se actualiza el campo de lugares disponibles para saber si el programa aun tiene lugares */
		if(!$modelSSProgramas->save()){
			Yii::app()->user->setFlash('danger', 'Error!!! Ocurrio un error al regisrar los datos del Servicio Social.');
			//die('Ocurrio un error al regisrar los datos del Servicio Social.');
		}
	}
	/*Restar lugar_disponible del programa donde se acaba de ACEPTAR al ALUMNO*/

	/*Una vex que el alumno sea ACEPTADO en el programa se creara su registro en la tabla servicio_social*/
	public function insertRegistroTablaServicioSocial($no_ctrl, $id_solicitud_programa, $id_programa)
	{
		//Inicializamos la instancia del modelo
		$modelSSServicioSocial = new SsServicioSocial;
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);

		/*REALIZAMOS LA INSERCION DEL REGISTRO EN LA TABLA servicio_social */
		/*Comprobamos que no haya registros repetidos del alumno en la tabla servicio_social*/
		if(!$this->evitarRegistroRepetidoTablaServicioSocial($no_ctrl, $modelSSProgramas->id_programa))
		{
			$modelSSServicioSocial->id_servicio_social = $id_solicitud_programa; //Mismo id de la solicitud_servicio_social
			$modelSSServicioSocial->no_ctrl = $no_ctrl;
			$modelSSServicioSocial->id_estado_servicio_social = 2; // En Curso
			$modelSSServicioSocial->id_programa = $modelSSProgramas->id_programa;
			$modelSSServicioSocial->fecha_registro = date('Y-m-d H:i:s');
			$modelSSServicioSocial->fecha_modificacion = date('Y-m-d H:i:s');
			$modelSSServicioSocial->calificacion_servicio_social = 0; //Calificacion default 0
			$modelSSServicioSocial->rfcDirector = $this->getDirectorActual(); //RFC del director del instituto
			$modelSSServicioSocial->rfcSupervisor = $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
			$modelSSServicioSocial->cargo_supervisor = $this->getCargoSupervisor($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa));
			$modelSSServicioSocial->empresaSupervisorJefe = $this->getEmpresaSupervisorYJefe($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa)); //Obtener la empresa a la que pertenece el supervisor
			$modelSSServicioSocial->nombre_jefe_depto = $this->getNombreJefeDepto($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa)); // Obtener nombre de jefe depto del depto del supervisor
			$modelSSServicioSocial->departamentoSupervisorJefe = $this->getDepartamentoSupervisorJefe($modelSSProgramas->id_programa, $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa)); //Nombre del jefe de depto. al que pertenece el supervisor
			$modelSSServicioSocial->rfcJefeOficServicioSocial = $this->getJefeOficinaServicioSocialActual(); //Jefe de Oficina de Servicio Social
			$modelSSServicioSocial->rfcJefeDeptoVinculacion = $this->getJefeDeptoVinculacionActual(); //Jefe de Depto. de Vinculacion
			$modelSSServicioSocial->nombre_supervisor = $this->getDatosSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);

			/*Si se inserto correctamente no mandara mensaje de error */
			if(!$modelSSServicioSocial->save()){
				//Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
				die('Ocurrio un error al querer guardar los datos del Servicio Social del Alumno.');
			}
		}
	}

	//Nombre del Supervisor del Programa Seleccionado
	public function getDatosSupervisor($id_programa, $id_tipo_programa)
	{
		//Si es 1 es Interno, si es 2 es Externo
		if($id_tipo_programa == 1)
		{
			$query =
			"
			select hemp.\"nmbCompletoEmp\" as name from pe_planeacion.ss_responsable_programa_interno rpi
			join public.\"H_empleados\" hemp
			on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
			where rpi.id_programa = '$id_programa' AND rpi.superv_principal_int = true
			";
			$name = Yii::app()->db->createCommand($query)->queryAll();
		}else
		if($id_tipo_programa == 2)
		{
			$query =
			"
			select (sp.nombre_supervisor || ' ' || sp.apell_paterno || ' ' || sp.apell_materno) as name from pe_planeacion.ss_responsable_programa_externo rpe
			join pe_planeacion.ss_supervisores_programas sp
			on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
			where rpe.id_programa = '$id_programa' AND rpe.superv_principal_ext = true
			";
			$name = Yii::app()->db->createCommand($query)->queryAll();
		}

		return $name[0]['name'];
	}

	//Obtenemnos el RFC del jefe de Vinculacion
	public function getJefeDeptoVinculacionActual()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '02' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['rfcEmpleado'];
	}

	//Obtenemos el RFC del jefe de la oficina de Servicio Social
	public function getJefeOficinaServicioSocialActual()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '04' AND \"cvePuestoParticular\" = '02' AND \"cveDepartamentoEmp\" = '06' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['rfcEmpleado'];
	}

	//Departamento al que pertenece el Jefe del Supervisor del Programa
	public function getDepartamentoSupervisorJefe($id_programa, $rfcSupervisor)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$departamento = "";

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$query = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$departamento = ($result != NULL) ? $result[0]['dscDepartamento'] : null;

		}else
		if($modelSSProgramas->id_tipo_programa == 2){//EXTERNO

			$query = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$departamento = $result[0]['dscDepartamento'];
		}

		return $departamento;
	}

	//Nombre del Jefe del Depto. al que pertenece el Supervisor
	public function getNombreJefeDepto($id_programa, $rfcSupervisor)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$nombre_jefe_depto = "";

		//Interno
		if($modelSSProgramas->id_tipo_programa == 1)
		{
			//Nos trae el nombre del jefe de depto.
			$nombre_jefe_depto = $this->getJefeDeptoXDepartamento($rfcSupervisor);

		}else
		if($modelSSProgramas->id_tipo_programa == 2)
		{
			$query = "select nombre_jefe_depto from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nombre_jefe_depto'];
		}

		return $nombre_jefe_depto;
	}

	public function getEmpresaSupervisorYJefe($id_programa, $rfcSupervisor)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_tipo_programa;
		$empresa = "";

		if($idPrograma == 1)
		{
			//id = 1 porque tec celaya debe ser la "empresa" default
			$query = "select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = 1";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$empresa = $result[0]['nombre_unidad_receptora'];

		}else
		if($idPrograma == 2)
		{
			$query = "select ur.nombre_unidad_receptora from pe_planeacion.ss_supervisores_programas sp
			join pe_planeacion.ss_unidades_receptoras ur
			on ur.id_unidad_receptora = sp.id_unidad_receptora
			where sp.\"rfcSupervisor\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$empresa = $result[0]['nombre_unidad_receptora'];

		}

		return $empresa;
	}

	public function getCargoSupervisor($id_programa, $rfcSupervisor)
	{
		//Obtenemos datos el programa
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		$idPrograma = $modelSSProgramas->id_programa;
		$cargoSupervisor = "";

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$query = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();

			$cargoSupervisor = ($result !=  NULL) ? $result[0]['nmbPuesto'] : null;
		}else
		if($modelSSProgramas->id_tipo_programa == 2)
		{
			$query = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

			$result = Yii::app()->db->createCommand($query)->queryAll();

			$cargoSupervisor = ($result != NULL) ? $result[0]['cargo_supervisor'] : null;
		}

		return $cargoSupervisor;
	}

	//Obtenemos el RFC del director del instituto
	public function getDirectorActual()
	{
		$query = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '01' AND \"cvePuestoParticular\" = '01' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();

		return $result[0]['rfcEmpleado'];

	}

	/*Revisamos si el alumno ya esta cursando su servicio social */
	public function evitarRegistroRepetidoTablaServicioSocial($no_ctrl, $id_programa)
	{
		$query ="select * from pe_planeacion.ss_servicio_social
				where no_ctrl = '$no_ctrl'
				and id_programa = '$id_programa'
				and id_estado_servicio_social != 6 and id_estado_servicio_social != 7";

		$datos_alumno = Yii::app()->db->createCommand($query)->queryAll();

		/*Si es mayor a cero ya hay registro del alumno en la tabla servicio_social y por
		lo tanto regresa true*/
		return ($datos_alumno != NULL ) ? true : false;
	}
	/*Revisamos si el alumno ya esta cursando su servicio social */

	public function hayRegistroHabilitadoServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'ssocial_actual = true';
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->find($criteria);

		return ($modelSSRegistroFechasServicioSocial === NULL) ? false : true;
	}

	//Si el alumno ya hizo preregistro para servicio social
	public function yaEstaRegistrado($no_ctrl)
	{
		$bandera1;
		$bandera2;
		//Activamos status de servicio social y sino existe lo creamos
		$modelSSStatusServicioSocial = SsStatusServicioSocial::model()->findByPk(trim($no_ctrl));

		if($modelSSStatusServicioSocial === NULL)
		{
			$periodo = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
			$anio = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual

			//Si insertar los datos del nuevo registro
			$SSStatusServicioSocial = new SsStatusServicioSocial;
			$SSStatusServicioSocial->no_ctrl = $no_ctrl;
			$SSStatusServicioSocial->val_serv_social = true;
			$SSStatusServicioSocial->fecha_preregistro = date('Y-m-d H:i:s');
			$SSStatusServicioSocial->periodo = $periodo[0]['periodoactual'];
			$SSStatusServicioSocial->anio = $anio[0]['periodoactual'];

			//Si no hubo problemas devuelve true
			if($SSStatusServicioSocial->save())
				$bandera1 = true;
			else
				$bandera1 = false;

		}else{
			//Si el servicio social del alumno esta desactivado entonces su activa para que pueda realizarlo
			if($modelSSStatusServicioSocial->val_serv_social == false or $modelSSStatusServicioSocial->val_serv_social == null)
			{
				$modelSSStatusServicioSocial->val_serv_social = true;
				$modelSSStatusServicioSocial->fecha_preregistro = date('Y-m-d H:i:s');
				$modelSSStatusServicioSocial->periodo = $periodo[0]['periodoactual'];
				$modelSSStatusServicioSocial->anio = $anio[0]['periodoactual'];

				//Si no hubo problemas devuelve true
				if($modelSSStatusServicioSocial->save())
					$bandera1 = true;
				else
					$bandera1 = false;

			}else{
				$bandera1 = true;
			}
		}

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK(trim($no_ctrl));

		if($modelSSHistoricoTotalesAlumnos === NULL)
		{
			$HistoricoTotalesAlumnos = new SsHistoricoTotalesAlumnos;
			$HistoricoTotalesAlumnos->no_ctrl = $this->getCadena($no_ctrl);
			$HistoricoTotalesAlumnos->horas_totales_servicio_social = 0;
			$HistoricoTotalesAlumnos->fecha_modificacion_horas_totales = date('Y-m-d H:i:s');
			$HistoricoTotalesAlumnos->calificacion_final_servicio_social = 0;
			$HistoricoTotalesAlumnos->completo_servicio_social = false; //Cuando se cumplen las 480 horas se valida que completo su Servicio Social
			$HistoricoTotalesAlumnos->firma_digital_alumno = null;
			$HistoricoTotalesAlumnos->calificacion_kardex = false; //Escolares la validara para que pase la calificacion de servicio social al kardex
			if($HistoricoTotalesAlumnos->save())
				$bandera2 = true;
			else
				$bandera2 = false;

		}else{
			$bandera2 = true;
		}

		return ($bandera1 == true AND $bandera2 == true) ? true : false;
	}

	public function completoHorasServicioSocial($no_ctrl)
	{

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPK(trim($no_ctrl));

		//Obtenemos el total de horas que se deben liberar de servicio social
		$id = 1;
		$modelSSConfiguracion = SsConfiguracion::model()->findByPk($id);
		return ($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social >= $modelSSConfiguracion->horas_max_servicio_social) ? true : false;
	}

	public function getCadena($no_ctrl)
	{
		$noctrl = "";

		//strlen: obtener la longitud de una cadena string
		for($i=0; $i<strlen($no_ctrl); $i++){ $noctrl = $noctrl.$no_ctrl[$i]; }

		return trim($noctrl);
	}

	public function actionValidarAddAlumnoPrograma()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$nocontrol = Yii::app()->request->getParam('nctrAlumno');

			if($nocontrol != NULL)
			{
				$alumnos = EDatosAlumno::model()->findByPk($nocontrol);

				if($alumnos === NULL)
				{
					$carrera = "";
					$inscrito = "";
					$status = "";
					$semestre = "";
					$creditos_acum = "";
					$sexo = "";
					$nombre_completo = "";
					$no_c = "*"; //No existe alumno con ese no. de control
					$msg_err = 'sad_512.png';
					$val_creditos = "";
				}else{
					$carreras = EEspecialidad::model()->find(" \"cveEspecialidad\"=?", array($alumnos->cveEspecialidadAlu));

					$carrera = $carreras->dscEspecialidad;
					$inscrito = $alumnos->insAlumno;
					$status = $alumnos->statAlumno;
					$semestre = $alumnos->semAlumno;
					$creditos_acum = $alumnos->crdAcumulaAlu;
					$sexo = $alumnos->sexoAlumno;
					$nombre_completo = $alumnos->nmbAlumno;
					$ape_paterno = $alumnos->apellPaternoAlu;
					$ape_materno = $alumnos->apellMaternoAlu;
					$no_c = $alumnos->nctrAlumno;
					$msg_err = "";
					$val_creditos = ($creditos_acum >= 135) ? true : false;
				}

			}else{

				$carrera = "";
				$inscrito = "";
				$status = "";
				$semestre = "";
				$ape_paterno = "";
				$ape_materno = "";
				$creditos_acum = "";
				$sexo = "";
				$nombre_completo = "";
				$no_c = "+"; //No se introdujo ningun caracter
				$msg_err = "";
				$val_creditos = "";
			}

			//datos enviados al form por AJAX
			echo CJSON::encode(array(
				'carrera' => $carrera,
				'inscrito' => $inscrito,
				'status' => $status,
				'semestre' => $semestre,
				'ape_paterno' => $ape_paterno,
				'ape_materno' => $ape_materno,
				'creditos_acum' => $creditos_acum,
				'sexo' => $sexo,
				'nombre_completo' => $nombre_completo,
				'no_c' => $no_c,
				'msg_err' => $msg_err,
				'val_creditos' => $val_creditos
			));

			Yii::app()->end();
		}
	}

	public function loadModel($id_programa)
	{
		$modelSSProgramas=SsProgramas::model()->findByPk($id_programa);

		if($modelSSProgramas===null)
			throw new CHttpException(404,'Registro no encontrado.');


		return $modelSSProgramas;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='programas-servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	//Obtener el nomnbre del jefe de depto del supervisor interno por medio del rfc del supervisor
	public function getJefeDeptoXDepartamento($rfcSupervisor)
	{
		$depto = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$result2 = Yii::app()->db->createCommand($depto)->queryAll();
		$cveDepto = ($result2 != NULL) ? $result2[0]['cveDepartamentoEmp'] : null; //Obtenemos la clave del depto. al cual pertenece ese supervisor
		$nombre_jefe_depto = "";

		if($cveDepto == 1) //Direccion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '01' AND \"cvePuestoParticular\" = '01')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 2) //Subdirector Planeacion y Vinculacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '01')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 3) //Subdireccion Academica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '02')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 4) //Subdireccion de Servicios Administrativos
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '03')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 5) //Depto Planeacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '01')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 6) //Vinculacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '02')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 7) //Comunicacion y difusion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '03')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 8)//Actividades Extraescolares
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '03')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 9) //Servicios Escolares
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '04');";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 10) //Centro de informacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '05')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 11)
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '06')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 12)
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '07')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 13) //Mecanica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '08')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 14) //Bioquimica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '09')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 15) //Quimica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '09')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 16) //Industrial
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '10')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 17) //Electronica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '11')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 18)// Ciencias Economico-Administrativas
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '12')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 19) // Desarrollo Academico
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '13')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 20) //Div. Estudios Profesionales
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '14')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 21) //Div. Est. Postgrado e Investigacion
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '15')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 22) //Recursos Humanos
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '16')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 23) //Recursos Financieros
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '17')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 24) //Recursos Materiales y de Servicios
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '18')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 25) //Centro de Computo
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '19')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 26) //Mantenimiento de equipo
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '20')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 29) //Ambiental
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '21')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 30) //Mecatronica
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '22')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else
		if($cveDepto == 32) //Calidad
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = '$cveDepto' AND (\"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '23')";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre_jefe_depto = $result[0]['nmbCompletoEmp'];
		}else{
			$nombre_jefe_depto = null;
		}

		return $nombre_jefe_depto;
	}

	/*********************************GETTERS Y SETTERS/*********************************/
	public function getIDprogramaVigenteSupervisor($rfcSupervisor)
	{
		$interno = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$externo = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

		$result1 = Yii::app()->db->createCommand($interno)->queryAll();
		$result2 = Yii::app()->db->createCommand($externo)->queryAll();

		if($result1 != NULL)
		{
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND 
			(pss.id_status_programa = 1 OR pss.id_status_programa = 6 OR pss.id_status_programa = 8)
			";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$ProgramaServicioSocial = ($result != NULL) ? $ProgramaServicioSocial = $result[0]['id_tipo_programa'] : null;

		}else
		if($result2 != NULL){
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			where rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND 
			(pss.id_status_programa = 1 OR pss.id_status_programa = 6 OR pss.id_status_programa = 8)
			";

			$result = Yii::app()->db->createCommand($query)->queryAll();
			$ProgramaServicioSocial = ($result != NULL) ? $result[0]['id_tipo_programa'] : null;

		}else{
			$ProgramaServicioSocial = array();
		}

		return $ProgramaServicioSocial;
	}

	//Obtenemos el RFC del Supervisor
	public function getRFCSupervisor($id_programa, $id_tipo_programa)
	{
		$rfc = "";

		if($id_tipo_programa == 1)
		{
			$query = "select * from pe_planeacion.ss_responsable_programa_interno where id_programa = '$id_programa' AND superv_principal_int = true";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcEmpleado'];

		}else
		if($id_tipo_programa == 2){
			$query = "select * from pe_planeacion.ss_responsable_programa_externo where id_programa = '$id_programa' AND superv_principal_ext = true";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcSupervisor'];
		}

		return $rfc;
	}

	public function getUnidadesReceptoras()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_unidad_receptora > 0 AND id_status_unidad_receptora = 1";
		$modelSSUnidadesReceptoras = SsUnidadesReceptoras::model()->findAll($criteria);

		$lista_unidades_receptoras = CHtml::listData($modelSSUnidadesReceptoras, "id_unidad_receptora", "nombre_unidad_receptora");

		return $lista_unidades_receptoras;
	}

	public function getDepartamentos()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = ' "cveDepartamento" > 0 ';
		$modelHDepatamentos = HDepartamentos::model()->findAll($criteria);

		$lista_departamentos = CHtml::listData($modelHDepatamentos, "cveDepartamento", "dscDepartamento");

		return $lista_departamentos;
	}

	public function getPeriodosProgramas()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = ' "id_periodo_programa" != 3';
		$modelSSPeriodosProgramas = SsPeriodosProgramas::model()->findAll($criteria);

		$lista_periodos_programas = CHtml::listData($modelSSPeriodosProgramas, "id_periodo_programa", "periodo_programa");

		return $lista_periodos_programas;
	}

	public function getPeriodosProgramasTodos()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = ' "id_periodo_programa" > 0';
		$modelSSPeriodosProgramas = SsPeriodosProgramas::model()->findAll($criteria);

		$lista_periodos_programas = CHtml::listData($modelSSPeriodosProgramas, "id_periodo_programa", "periodo_programa");

		return $lista_periodos_programas;
	}

	public function getPeriodosProgramasExtratemporales()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = ' "id_periodo_programa" = 3';
		$modelSSPeriodosProgramas = SsPeriodosProgramas::model()->findAll($criteria);

		$lista_periodos_programas = CHtml::listData($modelSSPeriodosProgramas, "id_periodo_programa", "periodo_programa");

		return $lista_periodos_programas;
	}

	public function getTipoServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = ' "id_tipo_servicio_social" > 0 ';
		$modelSSTipoServicioSocial = SsTipoServicioSocial::model()->findAll($criteria);

		$lista_tipo_servsocial = CHtml::listData($modelSSTipoServicioSocial, "id_tipo_servicio_social", "tipo_servicio_social");

		return $lista_tipo_servsocial;
	}

	public function getSupervisores()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tipo_supervisor = 2 AND (id_status_supervisor != 2 OR id_status_supervisor != 5)";
		$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findAll($criteria);

		//Si hay supervisores internos/externos
		foreach ($modelSSSupervisoresProgramas as $model)
		$lista_tipo_supervisor[$model->rfcSupervisor] = $model->rfcSupervisor.' '.$model->nombre_supervisor . ' '. $model->apell_paterno. ' '.$model->apell_materno;

		$supervisores = "<option value=''>-- Seleccionar Supervisores --</option>";
		foreach($lista_tipo_supervisor as $value=>$rfcSupervisor)
		{
			$supervisores .= CHtml::tag('option', array('value'=>$value),CHtml::encode($rfcSupervisor),true);
		}

		$lista_supervisores = CHtml::listData($modelSSSupervisoresProgramas, "rfcSupervisor", "nombre_supervisor");

		return $lista_supervisores;
	}

	public function getSupervisor($rfcSupervisor)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"rfcSupervisor\" = '$rfcSupervisor' ";
		//$criteria->order=' "rfcSupervisor" ASC';
		$modelSSSupervisoresProgramas = SsSupervisoresProgramas::model()->findAll($criteria);

		$lista_supervisores = CHtml::listData($modelSSSupervisoresProgramas, "rfcSupervisor", "nombre_supervisor");

		return $lista_supervisores;
	}

	public function getOfreceApoyoEconomico()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_apoyo_economico_prestador_servicio_social > 0 ';
		$modelSSApoyosEconomicos = SsApoyosEconomicos::model()->findAll($criteria);

		$lista_apoyos = CHtml::listData($modelSSApoyosEconomicos, "id_apoyo_economico_prestador_servicio_social", "descripcion_apoyo_economico");

		return $lista_apoyos;
	}

	public function getClasificacionAreas()
	{

		$criteria = new CDbCriteria();
		$criteria->condition = 'id_clasificacion_area_servicio_social > 0 AND status_area = true';
		$modelSSClasificacionArea = SsClasificacionArea::model()->findAll($criteria);

		$lista_clasificacion_aras = CHtml::listData($modelSSClasificacionArea, "id_clasificacion_area_servicio_social", "clasificacion_area_servicio_social");

		return $lista_clasificacion_aras;
	}

	public function getTipoApoyoEconomico()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_tipo_apoyo_economico > 0 ';
		$modelSSTiposApoyosEconomicos = SsTiposApoyosEconomicos::model()->findAll($criteria);

		$lista_tipos_apoyos = CHtml::listData($modelSSTiposApoyosEconomicos, "id_tipo_apoyo_economico", "tipo_apoyo_economico");

		return $lista_tipos_apoyos;
	}

	public function getTipoApoyoEconomicoE($id_tipo_apoyo)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tipo_apoyo_economico = '$id_tipo_apoyo' ";
		$modelSSTiposApoyosEconomicos = SsTiposApoyosEconomicos::model()->findAll($criteria);

		$lista_tipos_apoyos = CHtml::listData($modelSSTiposApoyosEconomicos, "id_tipo_apoyo_economico", "tipo_apoyo_economico");

		return $lista_tipos_apoyos;
	}

	public function getDiasSemana($id_programa)
	{
		$query =
		"
			SELECT id_dia_semana from pe_planeacion.ss_horario_dias_habiles_programas
			where id_programa = '$id_programa'
		";

		$dias_elegidos = Yii::app()->db->createCommand($query)->queryAll();

		$dias = array(1, 2, 3, 4, 5, 6, 7);
		$long = sizeof($dias);//7
		$arrays = array();
		$arrays2 = array();

		/*REINDEXAMOS VALORES DEL ARREGLO */
		for($j = 0; $j < sizeof($dias_elegidos); $j++)
		{
			$dia = $dias_elegidos[$j]['id_dia_semana'];//ji
			//echo $dia;
			$arrays2[] = $dia;
		}

		/*VERIFICAMOS CUALES DIAS YA HAN SIDO ELEGIDOS Y LOS QUITAMOS PARA QUE NO ELIJAN EL HORARIO
		DEL MISMO DIA DOS VECES EN EL PROGRAMA*/
		for($i = 0 ; $i < $long ; $i++)
		{
			if(in_array($dias[$i],$arrays2) === false)
			{
				$arrays[] = $dias[$i];
			}
		}

		/*SE ELIJEN LOS DIAS A MOSTRAR EN DROPDOWNLIST DEPENDIENDO DE QUE EL DIA AUN NO HAYA SIDO REGISTRADO, SI
		SE REGISTRO SIMPLEMENTE SE QUITA EL DIA DEL DROPDOWNLIST PARA YA NO MOSTRAR DIAS ELEGIDOS */
		$criteria = new CDbCriteria();
		switch(sizeof($arrays))
		{
			case 1:
				$criteria->condition = "id_dia_semana in ($arrays[0])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 2:
				$criteria->condition = "id_dia_semana in ($arrays[0],$arrays[1])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 3:
				$criteria->condition = "id_dia_semana in ($arrays[0],$arrays[1], $arrays[2])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 4:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 5:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 6:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4], $arrays[5])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 7:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4], $arrays[5], $arrays[6])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;

		}

		$lista_dias_semana = CHtml::listData($modelSSDiasSemana, "id_dia_semana", "dia_semana");

		return $lista_dias_semana;
	}

	public function getStatusPrograma()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_status > 0 ';
		$modelSSTipoStatus = SsTipoStatus::model()->findAll($criteria);

		$lista_status = CHtml::listData($modelSSTipoStatus, "id_status", "descripcion_status");

		return $lista_status;
	}

	/*Substraemos unicamente el rfcSupervisor de la cadena mostrada en el formulario de programas*/
	public function getSoloRFCSupervisor($rfcSupervisor)
	{
		$rfc = explode(" ", $rfcSupervisor);

		return trim($rfc[0]);
	}

	//Traemos los datos del Supervisor dependiendo si es interno o externo
	public function getNombreSupervisor($id_programa, $id_tipo_programa)
	{
		$name = "";

		if($id_tipo_programa == 1)
		{
			//INTERNO
			$query =
			"
			select hemp.\"nmbCompletoEmp\" as name from pe_planeacion.ss_responsable_programa_interno rpi
			join public.\"H_empleados\" hemp
			on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
			where rpi.id_programa = '$id_programa' AND rpi.superv_principal_int = true
			";
			$name = Yii::app()->db->createCommand($query)->queryAll();
		}else
		if($id_tipo_programa == 2)
		{
			//EXTERNO
			$query =
			"
			select (sp.nombre_supervisor || ' ' || sp.apell_paterno || ' ' || sp.apell_materno) as name
			from pe_planeacion.ss_responsable_programa_externo rpe
			join pe_planeacion.ss_supervisores_programas sp
			on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
			where rpe.id_programa = '$id_programa' AND rpe.superv_principal_ext = true
			";
			$name = Yii::app()->db->createCommand($query)->queryAll();
		}

		return $name[0]['name'];
	}
	//Traemos los datos del Supervisor dependiendo si es interno o externo

	public function getDatosEmpresa($id_programa, $id_tipo_programa)
	{
		//1 - Interno y 2 - Externo
		//TNM INSTITUTO TECNOLOGICO DE CELAYA DEBE SER LA EMPRESA CON EL ID 1 EN LA TABLA ss_unidades_receptoras
		if($id_tipo_programa == 1)
		{
			$query = "
			select ur.nombre_unidad_receptora as name_empresa
			from pe_planeacion.ss_responsable_programa_interno rpi
			join pe_planeacion.ss_programas pss
			on pss.id_programa = rpi.id_programa
			join pe_planeacion.ss_unidades_receptoras ur
			on ur.id_unidad_receptora = pss.id_unidad_receptora
			where rpi.id_programa = '$id_programa'  AND rpi.superv_principal_int = true AND ur.id_unidad_receptora = 1
			";
			$result = Yii::app()->db->createCommand($query)->queryAll();
		}else
		if($id_tipo_programa == 2)
		{
			$query = "
			select ur.nombre_unidad_receptora as name_empresa
			from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			join pe_planeacion.ss_supervisores_programas sp
			on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
			join pe_planeacion.ss_unidades_receptoras ur
			on ur.id_unidad_receptora = sp.id_unidad_receptora
			where rpe.id_programa = '$id_programa' AND superv_principal_ext = true
			";
			$result = Yii::app()->db->createCommand($query)->queryAll();
		}

		return $result[0]['name_empresa'];
	}

	public function getPeriodoPrograma($id_periodo_programa)
	{
		$query = "Select * from pe_planeacion.ss_periodos_programas where id_periodo_programa = '$id_periodo_programa' ";

		$periodo = Yii::app()->db->createCommand($query)->queryAll();

		return $periodo[0]['periodo_programa'];
	}

	/*Damos formato a la fecha*/
	public function getFormatoFecha($fecha)
	{
		$fechats = strtotime($fecha); //pasamos a timestamp

		//Obtenemos el dia, mes y año
		$dia = date('d', $fechats);
		$mes = $this->getMes(date('m', $fechats));
		$anio = date('Y', $fechats);

		return $dia." de ".$mes." de ".$anio;
	}
	/*Damos formato a la fecha*/

	public function getMes($mes)
	{
		$_mes="";

		switch ($mes)
		{
			case 1: $_mes = "ENERO"; break;
			case 2: $_mes = "FEBRERO"; break;
			case 3: $_mes = "MARZO"; break;
			case 4: $_mes = "ABRIL"; break;
			case 5: $_mes = "MAYO"; break;
			case 6: $_mes = "JUNIO"; break;
			case 7: $_mes = "JULIO"; break;
			case 8: $_mes = "AGOSTO"; break;
			case 9: $_mes = "SEPTIEMBRE"; break;
			case 10: $_mes = "OCTUBRE"; break;
			case 11: $_mes = "NOVIEMBRE"; break;
			case 12: $_mes = "DICIEMBRE"; break;
			default: $_mes = "DESCONOCIDO"; break;
		}

		return $_mes;
	}

	public function validarProgramaDisponible($id_programa)
	{
		$model = SsProgramas::model()->findByAttributes(
									array('id_programa'=>$id_programa),
									'id_status_programa=:status',
									array(':status'=>1)
		);

		return ($model === NULL) ? false : true;
	}

	/*Comprueba que el programa pertenece al supervisor logeado en el sistema */
	public function validarProgramaSupervisor($rfcSupervisor, $id_programa)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join public.\"H_empleados\" hemp
				on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
				where pss.id_programa = '$id_programa' and (pss.id_status_programa = 1 OR pss.id_status_programa = 8)
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and rpi.superv_principal_int = true";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_supervisores_programas sp
				on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
				where pss.id_programa = '$id_programa' and (pss.id_status_programa = 1 OR pss.id_status_programa = 8)
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and rpe.superv_principal_ext = true";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}
	/*********************************GETTERS Y SETTERS*********************************/

}
