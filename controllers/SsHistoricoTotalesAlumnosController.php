<?php

class SsHistoricoTotalesAlumnosController extends Controller
{

	//public $layout='//layouts/column2';


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaHorasTotalesAlumnosServicioSocial',
								 //'editarHorasTotalesAlumnosServicioSocial',
								 //'agregarFirmaDigitalAlumno', //Se deshabilito este opcion
								 'detalleProgramaHistoricoAlumno', //Detalle del programa realizado por el Alumno
								 'historialHorasTotalesAlumnosServicioSocial', //Historial de todos los Servicio Sociales que completo el alumno
								 'enviarCalificacionKardexAlumno', //Enviar la calificacion al Kardex del Alumno
								 'historicoHorasTotalesAlumnosServicioSocial', //Historico de Servicios Sociales Finalizados
								 'historialTotalAlumnoServicioSocial', //Detalle de todos los servicios sociales finalizados del Alumno
								 'deshabilitarServicioSocialAlumnoCompleto' //Deshabilita el Servicio Social porque ya lo completo
								 ),
				'roles'=>array('escolares_control_escolar','jefe_departamento_escolares'),
				//'users'=>array('@'),
				'actions'=>array('calificacionesAlumnosServicioSocialTodos' //Lista calificaciones todas servicio social
				),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users'=>array('@'),
			),
			
		);
	}

	public function actionCalificacionesAlumnosServicioSocialTodos()
	{
		$modelSsHistoricoTotalesAlumnos = new SsHistoricoTotalesAlumnos_('search');
		$modelSsHistoricoTotalesAlumnos->unsetAttributes();  // clear any default values
		
        if(isset($_GET['SsHistoricoTotalesAlumnos']))
            $modelSsHistoricoTotalesAlumnos->attributes=$_GET['SsHistoricoTotalesAlumnos'];

        $this->render('calificacionesAlumnosServicioSocialTodos',array(
            		  'modelSsHistoricoTotalesAlumnos'=>$modelSsHistoricoTotalesAlumnos,
        ));
	}

	public function actionEditCalificacionServicioSocial($no_ctrl)
	{
		$modelSsHistoricoTotalesAlumnos = $this->loadModel($no_ctrl);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['SsHistoricoTotalesAlumnos']))
        {
			$modelSsHistoricoTotalesAlumnos->attributes=$_POST['SsHistoricoTotalesAlumnos'];
			
			try
			{
				if($modelSsHistoricoTotalesAlumnos->save())
				{
					Yii::app()->user->setFlash('success', "Reporte Bimestral Eliminado correctamente!!!");
					//Si se realizo correctamente el cambio
					$transaction->commit();
					$this->redirect(array('calificacionesAlumnosServicioSocialTodos'));

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Eliminar el Reporte Bimestral.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					//echo CJSON::encode( $modelSsHistoricoTotalesAlumnos->getErrors() );
				}
			}catch(Exception $e){

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Eliminar el Reporte Bimestral.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
			}
        }

        $this->render('editCalificacionServicioSocial',array(
            		  'modelSsHistoricoTotalesAlumnos'=>$modelSsHistoricoTotalesAlumnos,
        ));
	}

	public function actionDeshabilitarServicioSocialAlumnoCompleto($no_ctrl)
	{
		$modelSSHistoricoTotalesAlumnos = $this->loadModel($no_ctrl);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		//Aqui se valida si se inserto la calificacion de Servicio Social del Alumno en el Kardex
		if($this->cumplioHorasServicioSocial($modelSSHistoricoTotalesAlumnos->horas_totales_servicio_social))
		{
			try
			{
				$modelSSHistoricoTotalesAlumnos->fecha_modificacion_horas_totales = date('Y-m-d H:i:s');
				$modelSSHistoricoTotalesAlumnos->completo_servicio_social = true;
				$modelSSHistoricoTotalesAlumnos->calificacion_kardex = true;

				if($modelSSHistoricoTotalesAlumnos->save())
				{
					Yii::app()->user->setFlash('success', "Acción realizada correctamente!!!");
					//Si se realizo correctamente el cambio
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! No se pudo realizar la acción.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode($modelSSHistoricoTotalesAlumnos->getErrors());
				}

			}catch(Exception $e)
			{
				Yii::app()->user->setFlash('danger', "Error!!! No se pudo realizar la acción.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode($modelSSHistoricoTotalesAlumnos->getErrors());
			}
		}else{

			Yii::app()->user->setFlash('danger', "Error!!! No ha cumplido el total de horas requeridas.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode($modelSSHistoricoTotalesAlumnos->getErrors());
		}
	}

	public function cumplioHorasServicioSocial($horas_totales_servicio_social)
	{
		return ($horas_totales_servicio_social >= $this->getHorasTotalesServicioSocial()) ? true : false;
	}

	public function actionHistorialTotalAlumnoServicioSocial($no_ctrl)
	{
		echo $no_ctrl;
		die("En terminación");
	}

	public function actionHistoricoHorasTotalesAlumnosServicioSocial($cve_especialidad = null)
	{

		$modelSsHistoricoTotalesAlumnos = new SsHistoricoTotalesAlumnos_('search');
		$modelSsHistoricoTotalesAlumnos->unsetAttributes();  // clear any default values

		//Lista carrera
		$lista_carreras = $this->getCarreras();
		
		if(isset($_GET['SsHistoricoTotalesAlumnos']))
		{
			$modelSsHistoricoTotalesAlumnos->attributes = $_GET['SsHistoricoTotalesAlumnos'];
			$modelSsHistoricoTotalesAlumnos->cve_especialidad = $_GET['SsHistoricoTotalesAlumnos']['cve_especialidad'];
			$cve_especialidad = $modelSsHistoricoTotalesAlumnos->cve_especialidad;
		}

        $this->render('historicoHorasTotalesAlumnosServicioSocial',array(
					  'modelSsHistoricoTotalesAlumnos'=>$modelSsHistoricoTotalesAlumnos,
					  'lista_carreras' => $lista_carreras,
					  'cve_especialidad' => $cve_especialidad
        ));
	}

	public function getCarreras()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " (\"tipoEspecialidad\" = 'L' OR \"tipoEspecialidad\" = 'I') AND ( \"cveEspecialidad\" != '9' AND \"cveEspecialidad\" != '11' ) ";
		$modelEEspecialidad = EEspecialidad::model()->findAll($criteria);

		$lista_carreras = CHtml::listData($modelEEspecialidad, "cveEspecialidad", "dscEspecialidad");

		return $lista_carreras;
	}

	public function actionListaHorasTotalesAlumnosServicioSocial()
	{
		$modelSSHistoricoTotalesAlumnos=new SsHistoricoTotalesAlumnos_('search');
		$modelSSHistoricoTotalesAlumnos->unsetAttributes();  // clear any default values

		$lista_estado = $this->getEstadosLiberacionServicioSocial();

		if(isset($_GET['SsHistoricoTotalesAlumnos']))
		{
			$modelSSHistoricoTotalesAlumnos->attributes=$_GET['SsHistoricoTotalesAlumnos'];
		}

		$this->render('listaHorasTotalesAlumnosServicioSocial',array(
					  'modelSSHistoricoTotalesAlumnos'=>$modelSSHistoricoTotalesAlumnos,
					  'lista_estado' => $lista_estado
		));
	}

	/*Devuelve solamente los estados para liberacion de servicio social */
	public function getEstadosLiberacionServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = 6";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_estados= CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_estados;
	}

	public function actionDetalleProgramaHistoricoAlumno($id_servicio_social, $no_ctrl)
	{
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);
		$modelSSProgramas = SsProgramas::model()->findByPK($modelSSServicioSocial->id_programa);
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($modelSSServicioSocial->no_ctrl);
		if($modelEDatosAlumno === null)
			throw new CHttpException(404,'No existen datos de ese Alumno con ese No. de Control.');

		$modelEEspecialidad = EEspecialidad::model()->findByPk($modelEDatosAlumno->cveEspecialidadAlu);
		if($modelEEspecialidad === null)
			throw new CHttpException(404,'No existen datos de esa Especialidad.');

		if($modelSSProgramas === null)
		{
			$modelSSProgramas=new SsServicioSocial;
		}else{
			$nombre_alumno = $this->getNameCompletoAlumno($modelSSServicioSocial->no_ctrl);
			$estado_servicio_social = $this->getEstadoServicioSocial($modelSSServicioSocial->id_estado_servicio_social);
		}

		//Obtenemos el horario del programa para mostrarlo
		$criteria = new CDbCriteria();
		$criteria->condition = " id_programa = '$modelSSProgramas->id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		if($modelSSHorarioDiasHabilesProgramas === null)
			$modelSSHorarioDiasHabilesProgramas=new SsHorarioDiasHabilesProgramas;
		

		$this->render('detalleProgramaHistoricoAlumno', array(
					  'nombre_alumno' => $nombre_alumno,
					  'no_ctrl' => $no_ctrl,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'estado_servicio_social' => $estado_servicio_social,
					  'modelSSProgramas' => $modelSSProgramas,
					  'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas,
					  'modelEEspecialidad' => $modelEEspecialidad
					  
   		));
	}

	public function actionEditarHorasTotalesAlumnosServicioSocial($no_ctrl)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
		$modelSSHistoricoTotalesAlumnos = $this->loadModel($no_ctrl);
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($no_ctrl);

		if($modelEDatosAlumno === null)
			throw new CHttpException(404,'No se encontraron datos del registro.');

		if(isset($_POST['HorasTotalesAlumnosServicioSocial']))
		{
			$modelSSHistoricoTotalesAlumnos->attributes=$_POST['HorasTotalesAlumnosServicioSocial'];
			$modelSSHistoricoTotalesAlumnos->fecha_modificacion_horas_totales = date('Y-m-d H:i:s');
			if($modelSSHistoricoTotalesAlumnos->save())
			{
				$this->redirect(array('listaHorasTotalesAlumnosServicioSocial'));
			}else{
				Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar tu firma digital.");
			}
		}

		$this->render('editarHorasTotalesAlumnosServicioSocial',array(
					'modelSSHistoricoTotalesAlumnos'=>$modelSSHistoricoTotalesAlumnos,
					'modelEDatosAlumno' => $modelEDatosAlumno,
					'fecha_actualizacion' => GetFormatoFecha::getFechaLastUpdateHorasTotalesAlumno($no_ctrl)
		));
	}

	public function actionAgregarFirmaDigitalAlumno()
	{
		//Tomar del inicio de sesion del alumno en el SII
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		$modelSSHistoricoTotalesAlumnos = SsHistoricoTotalesAlumnos::model()->findByPk($no_ctrl);

		if($modelSSHistoricoTotalesAlumnos === null)
		{
			$modelSSHistoricoTotalesAlumnos=new SsHistoricoTotalesAlumnos;
		}

		/*Tipos de FORMATO permitidos para la firma digital*/
		$extensiones = array("jpg");

        if(isset($_POST['SsHistoricoTotalesAlumnos']))
        {
            $modelSSHistoricoTotalesAlumnos->attributes=$_POST['SsHistoricoTotalesAlumnos'];

            $name  = $_FILES['SsHistoricoTotalesAlumnos']['name']['firma_digital_alumno'];
			$filename   = pathinfo($name, PATHINFO_FILENAME); //Nombre del archivo subido
			$ext        = pathinfo($name, PATHINFO_EXTENSION); //Nombre de la extension

            /*CARGAR FIRMA DIGITAL DEL ALUMNO*/
			//Devuelve una Instancia del archivo subido
			$modelSSHistoricoTotalesAlumnos->firma_digital_alumno = CUploadedFile::getInstance($modelSSHistoricoTotalesAlumnos, 'firma_digital_alumno');

			//Comparamos que el nombre de la imagen sea el no. de control del alumno logeado
			if(trim($filename) === trim($no_ctrl))
			{
				//Verificamos que no se haya subido imagen ya que solo permitiremos subirla una vez, cuando el campo firma_digital_alumno este vacio en la bd
				if($this->hayFirmaDigitalEnBD($no_ctrl) == true)
				{
					//1MB = 1048576
					if($modelSSHistoricoTotalesAlumnos->firma_digital_alumno->getSize() > '1048576')
					{
						$errors[] = 'El archivo debe pesar como maximo 1 MB.';
					}

					if(in_array($modelSSHistoricoTotalesAlumnos->firma_digital_alumno->getExtensionName(), $extensiones) === false)
					{
						$errors[] = 'La extensión del archivo debe ser .jpg';
					}

					if(strlen($modelSSHistoricoTotalesAlumnos->firma_digital_alumno->getName()) > 12)
					{
						$errors[] = 'El nombre de la imagen debe ser tu No. de Control. ';
					}

					//Si no hay errores entonces guardamos la imagen
					if(empty($errors))
					{
						if(!is_dir(Yii::getPathOfAlias('webroot').'/firmas_digitales_alumnos/'.$no_ctrl))
						{
							mkdir(Yii::getPathOfAlias('webroot').'/firmas_digitales_alumnos/'.$no_ctrl, 0, true);
							chmod(Yii::getPathOfAlias('webroot').'/firmas_digitales_alumnos/'.$no_ctrl, 0775);
						}

						//Ruta donde se guardara el logo de la empresa o unidad receptora
						//$path = Yii::app()->request->baseUrl.'/firmas_digitales_alumnos/';

						//Guardamos la imagen localmente
						$modelSSHistoricoTotalesAlumnos->firma_digital_alumno->saveAs(Yii::getPathOfAlias("webroot")."/firmas_digitales_alumnos/".$no_ctrl."/".$modelSSHistoricoTotalesAlumnos->firma_digital_alumno->getName());

						if($modelSSHistoricoTotalesAlumnos->save())
	            		{
	            			Yii::app()->user->setFlash('success', "Firma Digital guardada correctamente!");
	            			$this->redirect(array('agregarFirmaDigitalAlumno'));

	            		}else{

	            			Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar tu firma digital.");
	            			//die("Error!!! no se pudo guardar tu firma digital.");
	            		}

					}else{

						//Mostrar todos los errores que surgieron al tratar de subir la imagen
						$var = "";
						$esp="<br>";
						$contador = 0;

						foreach($errors as $key=>$value)
						{
							$contador++;
							if($contador == 1)
							{
								$var = $value;
							}else{
								$var .= $esp.$value;
							}
						}

						Yii::app()->user->setFlash('danger', trim($var));
						//Resfrescamos pantalla
						$this->refresh();
					}

				}else{

					//Si intenta subir otra firma digital cuando ya tiene una cargada en el servidor
					Yii::app()->user->setFlash('danger', "Lo siento, ya hay existe una firma digital. Si quieres editar la firma digital contacta al Administrador del Módulo.");
					//die("Lo siento, ya hay existe una firma digital. Si quieres editar la firma digital contacta al Administrador del Módulo.");
				}

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! El nombre de la imagen debe ser tu No. de Control.");
				//die("Error!!! El nombre de la imagen debe ser tu No. de Control.");
			}
            /*CARGAR FIRMA DIGITAL DEL ALUMNO*/
		}

    $this->render('agregarFirmaDigitalAlumno',array(
            		  'modelSSHistoricoTotalesAlumnos'=>$modelSSHistoricoTotalesAlumnos,
            		  'no_ctrl' => $no_ctrl
    ));
	}

	public function hayFirmaDigitalEnBD($no_ctrl)
	{
		$modelSSHistoricoTotalesAlumnos = $this->loadModel($no_ctrl);

		return ($modelSSHistoricoTotalesAlumnos->firma_digital_alumno === NULL) ? true : false;
	}

	public function actionHistorialHorasTotalesAlumnosServicioSocial($no_ctrl)
	{
		$modelSSHistoricoTotalesAlumnos = $this->loadModel($no_ctrl);
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($no_ctrl);
		if($modelEDatosAlumno === NULL)
			throw new CHttpException(404,'No se encontraron información del alumno con ese No. de Control.');

		//Verificamos si hay registros en servicios sociales pasados
		$hayRegServicioSocialPasados = $this->getServiciosSocialesPasados($no_ctrl);
		$modelSSServiciosSocialesAnteriores = new SsServiciosSocialesAnteriores_('search');
		$modelSSServiciosSocialesAnteriores->unsetAttributes();  // clear any default values
		
		$carrera = $this->getCarreraAlumno($modelEDatosAlumno->nctrAlumno);
		$nombre_alumno = $modelEDatosAlumno->nmbAlumno;
		$semestre = $modelEDatosAlumno->semAlumno;

		$modelSSServicioSocial = new SsServicioSocial_('searchXAlumnoCompletoServicioSocial');
		$modelSSServicioSocial->unsetAttributes();  // clear any default values
		
		if(isset($_GET['SsServicioSocial']))
		{
			$modelSSServicioSocial->attributes=$_GET['SsServicioSocial'];
		}
        
		$this->render('historialHorasTotalesAlumnosServicioSocial',array(
					  'modelSSHistoricoTotalesAlumnos'=>$this->loadModel($no_ctrl),
					  'nombre_alumno' => $nombre_alumno,
					  'semestre' => $semestre,
					  'carrera' => $carrera,
					  'no_ctrl' => $no_ctrl,
					  'horas_servicio_social' => $this->getHorasTotalesServicioSocial(),
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'modelSSServiciosSocialesAnteriores' => $modelSSServiciosSocialesAnteriores,
					  'hayRegServicioSocialPasados' => $hayRegServicioSocialPasados
		));
	}

	public function getServiciosSocialesPasados($no_ctrl)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "no_ctrl = '$no_ctrl' ";
		$modelSSServiciosSocialesAnteriores = SsServiciosSocialesAnteriores::model()->findAll($criteria);

		return ($modelSSServiciosSocialesAnteriores === NULL) ? false : true;
	}

	/*Devuelve el nombre completo del alumno */
	public function getNameCompletoAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";
		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		return $modelEDatosAlumno->nmbAlumno;
	}

	//Para mostrarlo como informacion del alumno
	public function getEstadoServicioSocial($id_estado_servicio_social)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social = '$id_estado_servicio_social' ";

		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->find($criteria);

		return strtoupper($modelSSEstadoServicioSocial->estado);
	}

	/*Devuelve la carrera del alumno */
	public function getCarreraAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"nctrAlumno\" = '$no_ctrl' ";
		$modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

		$criteria2 = new CDbCriteria();
		$criteria2->condition = " \"cveEspecialidad\" = '$modelEDatosAlumno->cveEspecialidadAlu' ";
		$modelEEspecialidad = EEspecialidad::model()->find($criteria2);

		return $modelEEspecialidad->dscEspecialidad;
	}

	/*Devuelve el total de horas a completar de Servicio Social */
	public function getHorasTotalesServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_configuracion = 1 ";

		$modelAlumnosVigentes = SsConfiguracion::model()->find($criteria);

		return $modelAlumnosVigentes->horas_max_servicio_social;
	}

	public function actionEnviarCalificacionKardexAlumno($no_ctrl)
	{
		$modelSSHistoricoTotalesAlumnos = $this->loadModel($no_ctrl);
		$modelSSHistoricoTotalesAlumnos->calificacion_kardex = true;

		//Aqui se valida si se inserto la calificacion de Servicio Social del Alumno en el Kardex
		if(true)
		{
			if($modelSSHistoricoTotalesAlumnos->save()){
				echo CJSON::encode( [ 'code' => 200 ] );
			}
			else{
				echo CJSON::encode($modelSSHistoricoTotalesAlumnos->getErrors());
			}
		}else{
			echo CJSON::encode($modelSSHistoricoTotalesAlumnos->getErrors());
		}
		
	}

	public function loadModel($id)
	{
		$modelSSHistoricoTotalesAlumnos=SsHistoricoTotalesAlumnos::model()->findByPk($id);

		if($modelSSHistoricoTotalesAlumnos===null)
			throw new CHttpException(404,'No se encontraron datos del alumno.');

		return $modelSSHistoricoTotalesAlumnos;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-historico-totales-alumnos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
