<?php

class SsSupervisoresProgramasController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaSupervisoresProgramas',
								 'nuevoSupervisorPrograma',
								 'editarSupervisorPrograma',
								 'detalleSupervisorPrograma',
								 'fotoPerfilSupervisorPrograma',
								 'mostrarTipoEmpresas',
								 'rfcEmpleadoReconocido', //Verificar si ya existe el rfc introducido por el usuario en la BD y nos traemos sus datos
								 'esJefeDepto', 
								 'actualizaNombreSupervisor',  //Si se actualiza el nombre tambien se debe de actualizar en el campo nombre_jefe_depto si es jefe depto
								 'actualizarMaxGradoAcademico' //Si se actualiza el grado acad. tambien se debe de actualizar en el campo gradoMaxEstudios si es jefe depto
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
				),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionListaSupervisoresProgramas()
	{
		
		$modelSSSupervisoresProgramas=new SsSupervisoresProgramas_('search');
		$modelSSSupervisoresProgramas->unsetAttributes();  // clear any default values

		//Lista de empresas para filtrar
		$empresas = $this->getEmpresas();

		if(isset($_GET['SsSupervisoresProgramas']))
		{
			$modelSSSupervisoresProgramas->attributes=$_GET['SsSupervisoresProgramas'];
		}

		$this->render('listaSupervisoresProgramas',array(
					  'modelSSSupervisoresProgramas'=>$modelSSSupervisoresProgramas,
					  'empresas' => $empresas
		));
	}

	public function actionNuevoSupervisorPrograma()
	{
		$modelSSSupervisoresProgramas = new SsSupervisoresProgramas;

		$lista_status = $this->getStatusSupervisor();
		$lista_empresas = $this->getEmpresas();

		if(isset($_POST['SsSupervisoresProgramas']))
		{
			$modelSSSupervisoresProgramas->attributes=$_POST['SsSupervisoresProgramas'];
			$modelSSSupervisoresProgramas->rfcSupervisor = strtoupper($modelSSSupervisoresProgramas->rfcSupervisor);
			$modelSSSupervisoresProgramas->nombre_supervisor = strtoupper($modelSSSupervisoresProgramas->nombre_supervisor);
			$modelSSSupervisoresProgramas->apell_paterno = strtoupper($modelSSSupervisoresProgramas->apell_paterno);
			$modelSSSupervisoresProgramas->apell_materno = strtoupper($modelSSSupervisoresProgramas->apell_materno);
			$modelSSSupervisoresProgramas->cargo_supervisor = strtoupper($modelSSSupervisoresProgramas->cargo_supervisor);
			$modelSSSupervisoresProgramas->nombre_jefe_depto = strtoupper($modelSSSupervisoresProgramas->nombre_jefe_depto);
			$modelSSSupervisoresProgramas->dscDepartamento = strtoupper($modelSSSupervisoresProgramas->dscDepartamento);
			$modelSSSupervisoresProgramas->gradoMaxEstudios = strtoupper($modelSSSupervisoresProgramas->gradoMaxEstudios);
			$modelSSSupervisoresProgramas->grMaxEstSup = strtoupper($modelSSSupervisoresProgramas->grMaxEstSup);
			$modelSSSupervisoresProgramas->id_status_supervisor = 1;	//Default status supervisor (1)Alta
			$modelSSSupervisoresProgramas->id_tipo_supervisor = 2;  //1 - INTERNO y 2 - EXTERNO

			//VALIDACION POR CODIGO DE NOMBRES EN CASO DE QUE EL SUPERVISOR TAMBIEN SEA UN JEFE DE DEPARTAMENTO
			//PENSADA POR SI FALLA AJAX Y JAVASCRIPT	
			if($modelSSSupervisoresProgramas->eres_jefe_depto == true)
				$modelSSSupervisoresProgramas->nombre_jefe_depto = $modelSSSupervisoresProgramas->nombre_supervisor.' '.$modelSSSupervisoresProgramas->apell_paterno.' '.$modelSSSupervisoresProgramas->apell_materno;
			
			/*ASIGNAR FOTO DE PERFIL DEFAULT DEPENDIENDO DE SU SEXO O GENERO*/
			if($modelSSSupervisoresProgramas->id_sexo == '1')
			{
				$modelSSSupervisoresProgramas->foto_supervisor = 'masculino.png';
			}else
			if($modelSSSupervisoresProgramas->id_sexo == '2'){
				$modelSSSupervisoresProgramas->foto_supervisor = 'femenino.png';
			}else{
				$modelSSSupervisoresProgramas->foto_supervisor = 'masculino.png';
			}
			/*ASIGNAR FOTO DE PERFIL DEFAULT DEPENDIENDO DE SU SEXO O GENERO*/

			/*SI TODO ESTA BIEN INSERTA Y NOS VAMOS A LA LISTA DE SUPERVISORES */
			if($modelSSSupervisoresProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaSupervisoresProgramas'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
			}
				
		}

		$this->render('nuevoSupervisorPrograma',array(
					  'modelSSSupervisoresProgramas'=>$modelSSSupervisoresProgramas,
					  'lista_status' => $lista_status,
					  'lista_empresas' => $lista_empresas,
		));
	}

	public function actionEditarSupervisorPrograma($rfcSupervisor)
	{
		$modelSSSupervisoresProgramas=$this->loadModel($rfcSupervisor);

		$lista_status = $this->getStatusSupervisor();

		$lista_empresas = $this->getEmpresas();

		$lista_deptos = $this->getDeptoSeleccionado($modelSSSupervisoresProgramas->id_tipo_supervisor); //array();

		/*ASIGNAR FOTO DE PERFIL DEFAULT DEPENDIENDO DE SU SEXO O GENERO*/
		if($modelSSSupervisoresProgramas->id_sexo == '1')
		{
			$modelSSSupervisoresProgramas->foto_supervisor = 'masculino.png';
		}else
		if($modelSSSupervisoresProgramas->id_sexo == '2'){
			$modelSSSupervisoresProgramas->foto_supervisor = 'femenino.png';
		}else{
			$modelSSSupervisoresProgramas->foto_supervisor = 'masculino.png';
		}
		/*ASIGNAR FOTO DE PERFIL DEFAULT DEPENDIENDO DE SU SEXO O GENERO*/

		//VALIDACION POR CODIGO DE NOMBRES EN CASO DE QUE EL SUPERVISOR TAMBIEN SEA UN JEFE DE DEPARTAMENTO
		//PENSADA POR SI FALLA AJAX Y JAVASCRIPT	
		if($modelSSSupervisoresProgramas->eres_jefe_depto == true)
			$modelSSSupervisoresProgramas->nombre_jefe_depto = $modelSSSupervisoresProgramas->nombre_supervisor.' '.$modelSSSupervisoresProgramas->apell_paterno.' '.$modelSSSupervisoresProgramas->apell_materno;
		

		if(isset($_POST['SsSupervisoresProgramas']))
		{
			$modelSSSupervisoresProgramas->attributes=$_POST['SsSupervisoresProgramas'];
			$modelSSSupervisoresProgramas->rfcSupervisor = strtoupper($modelSSSupervisoresProgramas->rfcSupervisor);
			$modelSSSupervisoresProgramas->nombre_supervisor = strtoupper($modelSSSupervisoresProgramas->nombre_supervisor);
			$modelSSSupervisoresProgramas->apell_paterno = strtoupper($modelSSSupervisoresProgramas->apell_paterno);
			$modelSSSupervisoresProgramas->apell_materno = strtoupper($modelSSSupervisoresProgramas->apell_materno);
			$modelSSSupervisoresProgramas->cargo_supervisor = strtoupper($modelSSSupervisoresProgramas->cargo_supervisor);
			$modelSSSupervisoresProgramas->nombre_jefe_depto = strtoupper($modelSSSupervisoresProgramas->nombre_jefe_depto);
			$modelSSSupervisoresProgramas->dscDepartamento = strtoupper($modelSSSupervisoresProgramas->dscDepartamento);
			$modelSSSupervisoresProgramas->gradoMaxEstudios = strtoupper($modelSSSupervisoresProgramas->gradoMaxEstudios);
			$modelSSSupervisoresProgramas->grMaxEstSup = strtoupper($modelSSSupervisoresProgramas->grMaxEstSup);

			if($modelSSSupervisoresProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaSupervisoresProgramas'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos.');
			}
				
		}

		$this->render('nuevoSupervisorPrograma',array(
				      'modelSSSupervisoresProgramas'=>$modelSSSupervisoresProgramas,
					  'lista_status' => $lista_status,
					  'lista_empresas' => $lista_empresas,
					  'lista_deptos' => $lista_deptos
		));
	}

	public function actionMostrarTipoEmpresas()
	{
		$criteria = new CDbCriteria();
		$id_tipo = $_POST["id_tipo_supervisor"];
		$criteria->condition = "id_tipo_empresa = '$id_tipo' AND id_status_unidad_receptora = 1";
		$listaEmpresas = SsUnidadesReceptoras::model()->findAll($criteria);
		$lista_empresas = CHtml::listData($listaEmpresas, "id_unidad_receptora", "nombre_unidad_receptora");

		$empresas = "<option value=''>--Seleccionar Empresa--</option>";
		foreach($lista_empresas as $value=>$nombre_unidad_receptora)
			$empresas .= CHtml::tag('option', array('value'=>$value),CHtml::encode($nombre_unidad_receptora),true);
		
		if($id_tipo == 1)
		{
			$criteria->condition = " \"cveDepartamento\" != 99 ";
			$listaDeptos = HDepartamentos::model()->findAll($criteria);
			$lista_deptos = CHtml::listData($listaDeptos, "cveDepartamento", "dscDepartamento");
		}else{
			$criteria->condition = " \"cveDepartamento\" = 99 ";
			$listaDeptos = HDepartamentos::model()->findAll($criteria);
			$lista_deptos = CHtml::listData($listaDeptos, "cveDepartamento", "dscDepartamento");
		}
		
		$deptos = "<option value='null'>--Seleccionar Departamento--</option>";
		foreach($lista_deptos as $value=>$dscDepartamento)
			$deptos .= CHtml::tag('option', array('value'=>$value),CHtml::encode($dscDepartamento),true);
		

		// return data (JSON formatted)
		echo CJSON::encode(array(
			'empresas'=>$empresas,
			'deptos' => $deptos
		));

		Yii::app()->end();
	}

	public function actionRfcEmpleadoReconocido()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$criteria = new CDbCriteria();
			$rfc_supervisor = $_POST["rfcSupervisor"];
			$criteria->condition = " \"rfcSupervisor\" = '$rfc_supervisor' ";
			$RFCSupervisor = SsSupervisoresProgramas::model()->findAll($criteria);

			if($RFCSupervisor != NULL)
			{
				$nombre_sup = $RFCSupervisor->nombre_supervisor;
			}

			echo CJSON::encode(array(
				'nombre_sup' => $nombre_sup,
				//'nombrecompleto' => $nombrecompleto,
				//'semestre' => $semestre
			));
			
			Yii::app()->end();
		}
		
	}

	public function actionEsJefeDepto()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$es_jefe = Yii::app()->request->getParam('eres_jefe_depto');

			if($es_jefe)
			{
				$nombre = Yii::app()->request->getParam('nombre_supervisor');
				$ape_pat = Yii::app()->request->getParam('apell_paterno');
				$ape_mat = Yii::app()->request->getParam('apell_materno');
				$name_completo = $nombre.' '.$ape_pat.' '.$ape_mat;
				$grado = Yii::app()->request->getParam('grMaxEstSup');

			}else{

				$name_completo = "";
				$grado = "";
			}

			echo CJSON::encode(array(
				'name_completo' => $name_completo,
				'es_jefe' => $es_jefe,
				'grado' => $grado
			));
			
			Yii::app()->end();
		}
	}

	public function actionActualizaNombreSupervisor()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$es_jefe = Yii::app()->request->getParam('eres_jefe_depto');

			if($es_jefe)
			{
				$nombre = Yii::app()->request->getParam('nombre_supervisor');
				$ape_pat = Yii::app()->request->getParam('apell_paterno');
				$ape_mat = Yii::app()->request->getParam('apell_materno');
				$name_completo = $nombre.' '.$ape_pat.' '.$ape_mat;
			}else{

				$name_completo = "";
			}
			
			echo CJSON::encode(array(
				'name_completo' => $name_completo,
				'es_jefe' => $es_jefe
			));
			
			Yii::app()->end();
		}
	}

	public function actionActualizarMaxGradoAcademico()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$es_jefe = Yii::app()->request->getParam('eres_jefe_depto');

			if($es_jefe)
				$cargo = Yii::app()->request->getParam('grMaxEstSup');
			else
				$cargo = "";
			
			
			echo CJSON::encode(array(
				'cargo' => $cargo,
				'es_jefe' => $es_jefe
			));
			
			Yii::app()->end();

		}
	}

	public function actionDetalleSupervisorPrograma($rfcSupervisor)
	{
		$modelSSSupervisoresProgramas = $this->loadModel($rfcSupervisor);

		$name_empresa = $this->getEmpresa($modelSSSupervisoresProgramas->id_unidad_receptora);

		$this->render('detalleSupervisorPrograma',array(
					  'modelSSSupervisoresProgramas'=>$this->loadModel($rfcSupervisor),
					  'name_empresa' => $name_empresa
		));
	}

	//Agregamos foto de perfil, esta parte es opcional
	public function actionFotoPerfilSupervisorPrograma($rfcSupervisor)
	{
		$modelSSSupervisoresProgramas = $this->loadModel($rfcSupervisor);

		/*Tipos de FORMATO permitidos */
		$extensiones = array("jpeg", "jpg", "png");

		if(isset($_POST['SsSupervisoresProgramas']))
		{
			$modelSSSupervisoresProgramas->attributes=$_POST['SsSupervisoresProgramas'];

			/***AGREGAR IMAGEN DEL SUPERVISOR***/
			$modelSSSupervisoresProgramas->foto_supervisor = CUploadedFile::getInstance($modelSSSupervisoresProgramas, 'foto_supervisor');

			//Verificamos que se haya subido alguna imagen
			if($modelSSSupervisoresProgramas->foto_supervisor != null)
			{
				//2MB = 2097152
				if($modelSSSupervisoresProgramas->foto_supervisor->getSize() > '2097152')
					$errors[] = 'El archivo debe pesar menos de 2MB.';
				
				if(in_array($modelSSSupervisoresProgramas->foto_supervisor->getExtensionName(), $extensiones) === false)
					$errors[] = 'La extensión del archivo debe ser png, jpeg o jpg';

				if(strlen($modelSSSupervisoresProgramas->foto_supervisor->getName()) > 13)
					$errors[] = 'El nombre del logo de perfil es demasiado largo. Debe ser de maximo 13 caracteres.';

				/*VERIFICAMOS QUE NO HAYA ERRORES Y SI HAY LOS MOSTRAMOS AL USUARIO*/
				if(empty($errors))
				{
					//Ruta donde se guardara el logo de la empresa o unidad receptora
					if(!is_dir(Yii::getPathOfAlias('webroot').'/images/supervisores/'.$rfcSupervisor)) 
					{
						mkdir(Yii::getPathOfAlias('webroot').'/images/supervisores/'.$rfcSupervisor, 0, true);
						chmod(Yii::getPathOfAlias('webroot').'/images/supervisores/'.$rfcSupervisor, 0775);
					}

					//Guardamos la imagen localmente
					$modelSSSupervisoresProgramas->foto_supervisor->saveAs(Yii::getPathOfAlias("webroot")."/images/supervisores/".$rfcSupervisor."/".$modelSSSupervisoresProgramas->foto_supervisor->getName());

					//Si no hubo error alguno entonces mandara devolvera true
					if($modelSSSupervisoresProgramas->save())
					{
						Yii::app()->user->setFlash('success', "Logo guardado correctamente!");
						$this->redirect(array('listaSupervisoresProgramas'));
					}else{

						Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar el logo.");
						die("Error al tratar de subir el documento.");
					}

				}else{

					//Mostrar todos los errores que surgieron al tratar de subir la imagen
					$var = "";
					$esp="<br>";
					$contador = 0;

					foreach($errors as $key=>$value)
					{
						$contador++;
						if($contador == 1)
						{
							$var = $value;
						}else{
							$var .= $esp.$value;
						}
					}

					//Mostramos los errores en pantalla
					Yii::app()->user->setFlash('danger', trim($var));
					die('Ocurrio un error al guardar la foto.');
					//Resfrescamos pantalla
					$this->refresh();

				}
				/*VERIFICAMOS QUE NO HAYA ERRORES Y SI HAY LOS MOSTRAMOS AL USUARIO*/
			}
			/***AGREGAR IMAGEN DEL SUPERVISOR***/
		}

		$this->render('fotoPerfilSupervisorPrograma', array(
					  'modelSSSupervisoresProgramas' => $modelSSSupervisoresProgramas
		));
	}
	//Agregamos foto de perfil, esta parte es opcional

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsSupervisoresProgramas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id_supervisor)
	{
		$modelSSSupervisoresProgramas=SsSupervisoresProgramas::model()->findByPk($id_supervisor);
		
		if($modelSSSupervisoresProgramas===null)
		{
			throw new CHttpException(404,'No existen datos de ese registro.');
		}
			
		return $modelSSSupervisoresProgramas;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='supervisores-programas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*****************************************GETTERS AND SETTERS*****************************************/
	public function getDeptoSeleccionado($id_tipo)
	{
		$criteria = new CDbCriteria();
		if($id_tipo == 1)
		{
			$criteria->condition = " \"cveDepartamento\" != 99";
			$modelDepto = HDepartamentos::model()->findAll($criteria);
		}else{

			$criteria->condition = " \"cveDepartamento\" = 99";
			$modelDepto = HDepartamentos::model()->findAll($criteria);
		}
		
		$lista_depto = CHtml::listData($modelDepto,"cveDepartamento","dscDepartamento");

		return $lista_depto;
	}

	public function getStatusSupervisor()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_status > 0";
		$modelSSTipoStatus = SsTipoStatus::model()->findAll($criteria);

		$lista_status = CHtml::listData($modelSSTipoStatus,"id_status","descripcion_status");

		return $lista_status;
	}

	public function getEmpresas()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_status_unidad_receptora = 1 AND id_tipo_empresa = 2";
		$listaEmpresas = SsUnidadesReceptoras::model()->findAll($criteria);

		$edo_servicio = CHtml::listData($listaEmpresas, "id_unidad_receptora", "nombre_unidad_receptora");

		return $edo_servicio;
	}

	public function getEmpresa($id_unidad_receptora)
	{
		$query = "select nombre_unidad_receptora from pe_planeacion.ss_unidades_receptoras
		where id_unidad_receptora = '$id_unidad_receptora' ";

		$name_empresa = Yii::app()->db->createCommand($query)->queryAll();

		return $name_empresa[0]['nombre_unidad_receptora'];
	}
	/*****************************************GETTERS AND SETTERS*****************************************/
}
