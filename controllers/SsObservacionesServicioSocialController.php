<?php

class SsObservacionesServicioSocialController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'nuevaObservacionDeptoServicioSocial', //form de la observacion al alumno por el depto
								 'responderObservacion' //Form para responder a una observacion hecha
								 ),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users' => array('@')
				),
			array('allow',
			   'actions'=>array('nuevaObservacionSupervisorServicioSocial',
			   					'responderObservacion'),
			   //'roles'=>array('vinculacion_supervisor_servicio_social'),
			   'users' => array('@')
			   ),
			array('allow',
			   'actions'=>array('historialAlumnoObservacionesServicioSocial',
								'verObservacionRecibidaAlumno',
								'responderObservacion',
								'historialObservacionesServicioSocial'
							),
			   //'roles'=>array('alumno'),
			   'users' => array('@')
           	),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	//Conversacion entre el alumno y el jefe de ofic de servicio social
	public function actionNuevaObservacionDeptoServicioSocial($id_servicio_social, $no_ctrl)
	{
		$rfcSupervisor = Yii::app()->params['rfcJefeOfServSocial'];
		//$rfcSupervisor = Yii::app()->user->name; //Jefe de oficina de servicio social
		$ObservacionesServicioSocial = new SsObservacionesServicioSocial;

		$servicioSocialAlumno = $this->getDatosServicioSocial($id_servicio_social);

		//Nos traemos toda la conversacion completa ENTRE EL JEFE DE OFIC SERVICIO SOCIAL Y EL ALUMNO para mostrarla en la vista
		$criteria = new CDbCriteria();
		$criteria->condition = "id_servicio_social = '$id_servicio_social' AND ((emisor_observ = '$rfcSupervisor' AND receptor_observ = '$no_ctrl') OR (emisor_observ = '$no_ctrl' AND receptor_observ = '$rfcSupervisor')) AND
		((tipo_observacion_emisor = 1 AND tipo_observacion_receptor = 3) OR (tipo_observacion_emisor = 3 AND tipo_observacion_receptor = 1))";
		$criteria->order = "id_observacion ASC";
		$modelSSObservacionesServicioSocial = SsObservacionesServicioSocial::model()->findAll($criteria);

		//Si entra a la vista entonces se la por visto la observacion
		$this->getCambiarEstadoObservacionALeidasJefeOfServicioSocial($rfcSupervisor, $id_servicio_social);

		//Obtenemos el nombre del emisor y su foto, es decir, el que manda el mensaje para mostrarla en la Vista de la conversacion
		$nombre_supervisor = $this->getNombreSupervisor($rfcSupervisor);
		$foto_emisor = $this->getFotoSupervisor($rfcSupervisor);

        if(isset($_POST['SsObservacionesServicioSocial']))
        {
			$ObservacionesServicioSocial->attributes=$_POST['SsObservacionesServicioSocial'];
			$ObservacionesServicioSocial->id_servicio_social = $id_servicio_social;
			$ObservacionesServicioSocial->fecha_registro = date('Y-m-d H:i:s');
			$ObservacionesServicioSocial->tipo_observacion_emisor = 1; //Quien la realiza
			$ObservacionesServicioSocial->tipo_observacion_receptor = 3; //Quien la recibe
			$ObservacionesServicioSocial->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
			$ObservacionesServicioSocial->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
			$ObservacionesServicioSocial->emisor_observ = $rfcSupervisor;
			$ObservacionesServicioSocial->receptor_observ = $no_ctrl;
			//$modelObservacion->hilo_observacion = $hilo_observacion;

			if($ObservacionesServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//Se guarda el hilo de la conversacion
				//$ObservacionesServicioSocial->hilo_observacion = $ObservacionesServicioSocial->id_observacion;
				//$ObservacionesServicioSocial->save();
				$this->redirect(array('ssServicioSocial/listaDeptoObservacionesServicioSocial'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

        }

        $this->render('nuevaObservacionDeptoServicioSocial',array(
					  'ObservacionesServicioSocial' => $ObservacionesServicioSocial,
					  'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
					  'foto_emisor' => $foto_emisor,
				      'nombre_supervisor' => $nombre_supervisor,
					  'rfcSupervisor' => $rfcSupervisor,
					  'no_ctrl' => $no_ctrl,
					  'servicioSocialAlumno' => $servicioSocialAlumno
        ));
	}

	//Para actualizar a "VISTO" las observaciones enviadas al JEFE DE OFICINA DE SERVICIO SOCIAL, sean uno o varias
	public function getCambiarEstadoObservacionALeidasJefeOfServicioSocial($rfcSupervisor, $id_servicio_social)
	{
		$fec_actual = date('Y-m-d H:i:s');

		$model = SsObservacionesServicioSocial::model()->findByAttributes(
										array('id_servicio_social'=>$id_servicio_social,'receptor_observ'=>$rfcSupervisor),
										'fue_leida=:status',
										array(':status'=>false)

		);

		//print_r($model);
		//die();

		if($model != NULL)
		{
			$query = "Update pe_planeacion.ss_observaciones_servicio_social
				set fue_leida = true, fecha_leida = '$fec_actual'
				where receptor_observ = '$rfcSupervisor' AND id_servicio_social = '$id_servicio_social' AND fue_leida = false
				AND tipo_observacion_receptor = 1";

			if(!Yii::app()->db->createCommand($query)->queryAll())
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');

		}

	}
	//Para actualizar a "VISTO" las observaciones enviadas al JEFE DE OFICINA DE SERVICIO SOCIAL, sean uno o varias

	//Vista conversacion ENTRE EL SUPERVISOR Y EL ALUMNO QUE ESTA EN SU PROGRAMA
	public function actionNuevaObservacionSupervisorServicioSocial($id_servicio_social, $no_ctrl)
	{
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarObservacionesSupervisor($rfcSupervisor, $id_servicio_social, $no_ctrl))
			throw new CHttpException(4045,'Esta información No esta disponible para ti.');

		$modelSSObservaciones = new SsObservacionesServicioSocial;

		//De aqui obtenemos los datos del alumno para mostrar en la vista
		$servicioSocialAlumno = $this->getDatosServicioSocial($id_servicio_social);

		//Nos traemos toda la conversacion completa ENTRE EL SUPERVISOR Y EL ALUMNO para mostrarla en la vista
		$criteria = new CDbCriteria();
		$criteria->condition = " id_servicio_social = '$id_servicio_social' AND ((emisor_observ = '$rfcSupervisor' AND receptor_observ = '$no_ctrl') OR (emisor_observ = '$no_ctrl' AND receptor_observ = '$rfcSupervisor')) AND
		((tipo_observacion_emisor = 2 AND tipo_observacion_receptor = 3) OR (tipo_observacion_emisor = 3 AND tipo_observacion_receptor = 2))";
		$criteria->order = "id_observacion ASC";
		$modelSSObservacionesServicioSocial = SsObservacionesServicioSocial::model()->findAll($criteria);

		//Obtenemos el nombre del emisor y su foto, es decir, el que manda el mensaje para mostrarla en la Vista de la conversacion
		$nombre_supervisor = $this->getNombreSupervisor($rfcSupervisor);
		$foto_emisor = $this->getFotoSupervisor($rfcSupervisor);
		$Programas = $this->getIDprogramaVigenteSupervisor($rfcSupervisor);

		if($Programas != NULL)
		{
			$id_tipo_programa = $Programas[0]['id_tipo_programa'];
		}else{
			$id_tipo_programa = 0;
		}

		//Si entra a la conversacion entonces muestra las observaciones como leidas
		$this->getCambiarEstadoObservacionALeidasSupervisor($rfcSupervisor, $id_servicio_social);

        if(isset($_POST['SsObservacionesServicioSocial']))
        {
			$modelSSObservaciones->attributes=$_POST['SsObservacionesServicioSocial'];

			$modelSSObservaciones->id_servicio_social = $id_servicio_social;
			$modelSSObservaciones->fecha_registro = date('Y-m-d H:i:s');
			$modelSSObservaciones->tipo_observacion_emisor = 2; //Quien la realiza 1-Admin, 2-Supervisor y 3-Alumno
			$modelSSObservaciones->tipo_observacion_receptor = 3; //Quien la recibe
			$modelSSObservaciones->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
			$modelSSObservaciones->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
			$modelSSObservaciones->emisor_observ = $rfcSupervisor;
			$modelSSObservaciones->receptor_observ = $no_ctrl;
			//$modelObservacion->hilo_observacion = $hilo_observacion;

			if($modelSSObservaciones->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('ssServicioSocial/listaSupervisorObservacionesServicioSocial'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
        }

        $this->render('nuevaObservacionSupervisorServicioSocial',array(
					  'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
					  'modelSSObservaciones' => $modelSSObservaciones,
					  'nombre_supervisor' => $nombre_supervisor,
					  'foto_emisor' => $foto_emisor,
					  'rfcSupervisor' => $rfcSupervisor,
					  'id_tipo_programa' => $id_tipo_programa,
					  'no_ctrl' => $no_ctrl,
					  'servicioSocialAlumno' => $servicioSocialAlumno

        ));
	}
	//Vista conversacion ENTRE EL SUPERVISOR Y EL ALUMNO QUE ESTA EN SU PROGRAMA

	//Para actualizar a "VISTO" las observaciones enviadas al SUPERVISOR, sean uno o varias
	public function getCambiarEstadoObservacionALeidasSupervisor($rfcSupervisor, $id_servicio_social)
	{
		$fec_actual = date('Y-m-d H:i:s');

		$model = SsObservacionesServicioSocial::model()->findByAttributes(

			array('id_servicio_social'=>$id_servicio_social,
				  'receptor_observ'=>$rfcSupervisor
			),
			array(
				'condition'=>'fue_leida=:status',
				'params'=>array(':status'=>false)
			)

		);

		if($model != NULL)
		{
			$query = "Update pe_planeacion.ss_observaciones_servicio_social
			set fue_leida = true, fecha_leida = '$fec_actual'
			where receptor_observ = '$rfcSupervisor' AND id_servicio_social = '$id_servicio_social' AND fue_leida = false
			AND tipo_observacion_receptor = 2";

			if(!Yii::app()->db->createCommand($query)->queryAll())
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');

		}

	}
	//Para actualizar a "VISTO" las observaciones enviadas al SUPERVISOR, sean uno o varias

	/*Muestra el historial de observaciones al servicio social actual de cada alumno */
	public function actionHistorialAlumnoObservacionesServicioSocial($id_servicio_social, $fue_leida = null)
	{
		$modelSSObservacionesServicioSocial = new SsObservacionesServicioSocial_('searchXHistorialAlumnoServicioSocial');
		$modelSSObservacionesServicioSocial->unsetAttributes();  // clear any default values

		$servicioSocialAlumno = $this->getDatosServicioSocial($id_servicio_social);

		$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

		if(isset($_GET['SsObservacionesServicioSocial']))
		{
			$modelSSObservacionesServicioSocial->attributes=$_GET['SsObservacionesServicioSocial'];
		}

		$this->render('historialAlumnoObservacionesServicioSocial',array(
					  'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
					  'id_servicio_social' => $id_servicio_social,
					  'fue_leida' => $fue_leida,
					  'servicioSocialAlumno' => $servicioSocialAlumno,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin

		));
	}
	/*Muestra el historial de observaciones al servicio social actual de cada alumno */

	/*Obtener el periodo de inicio y fin del servicio social */
	public function getPeriodoServicioSocial($fecha)
	{
		$dt = new DateTime($fecha);
		$mes = $dt->format("m");
		$anio = $dt->format("Y");
		$periodo="";

		if($mes <= 7)
		{
			$periodo = "ENERO-JULIO"." de ".$anio;
		}else
		if($mes > 7){
			$periodo = "AGOSTO-DICIEMBRE"." de ".$anio;
		}

		return $periodo;
	}
	/*Obtener el periodo de inicio y fin del servicio social */

	/*Datos para vista de la evaluacion del reporte bimestral de servicio social */
	public function getDatosServicioSocial($id_servicio_social)
	{
		$query="
		select
		eda.\"nmbSoloAlumno\" as name_alumno,
		ss.no_ctrl,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		pss.nombre_programa as programa,
		ss.id_estado_servicio_social,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		(select periodo_programa from pe_planeacion.ss_periodos_programas where id_periodo_programa = pss.id_periodo_programa) periodo_servsocial,
		ss.calificacion_servicio_social
		from pe_planeacion.ss_servicio_social ss
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social';
		";

		$datos_servsocial = Yii::app()->db->createCommand($query)->queryAll();

		return $datos_servsocial;
	}
	/*Datos para vista de la evaluacion del reporte bimestral de servicio social */

	public function actionHistorialObservacionesServicioSocial()
	{
		$modelSSObservacionesServicioSocial = new SsObservacionesServicioSocial_('searchXHistorialAlumnoServicioSocial');
		$modelSSObservacionesServicioSocial->unsetAttributes();  // clear any default values
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		$criteria=new CDbCriteria;
		$criteria->condition = "no_ctrl = '$no_ctrl' AND (id_estado_servicio_social > 0 AND id_estado_servicio_social < 6)";
		$modelSSServicioSocial = SsServicioSocial::model()->find($criteria);

		if($modelSSServicioSocial === null)
		{
			$modelSSServicioSocial = new SsServicioSocial;
			$modelSSServicioSocial->id_servicio_social = 0;
			//throw new CHttpException(404,'No existen datos de ese Servicio Social.');
		}

		if(isset($_GET['SsObservacionesServicioSocial']))
		{
			$modelSSObservacionesServicioSocial->attributes=$_GET['SsObservacionesServicioSocial'];
		}

        $this->render('historialObservacionesServicioSocial',array(
					  'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
					  'id_servicio_social' => $modelSSServicioSocial->id_servicio_social,
					 //'fue_leida' => $fue_leida
        ));
	}

	//Vista Conversacion del alumno con SUPERVISOR O JEFE OFICINA SERVICIO SOCIAL
	public function actionVerObservacionRecibidaAlumno($id_observacion, $id_servicio_social)
	{
		//Nos traemos el no. de control del alumno
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		if(!$this->validarObservacionesServicioSocial($id_observacion, $id_servicio_social, $no_ctrl))
			throw new CHttpException(404,'La información No esta disponible.');


		$model = SsObservacionesServicioSocial::model()->findByAttributes(
									array('id_observacion'=>$id_observacion,
										 'id_servicio_social'=>$id_servicio_social
									)
		);

		//De aqui obtenemos los datos del alumno para mostrar en la vista
		$servicioSocialAlumno = $this->getDatosServicioSocial($id_servicio_social);

		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);
		if($modelSSServicioSocial === NULL)
			throw new CHttpException(404,'No hay datos de ese Servicio Social.');

		//Nos traemos los datos del alumno
		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($modelSSServicioSocial->no_ctrl);
		if($modelEDatosAlumno === NULL)
			throw new CHttpException(404,'No hay datos de ese Alumno.');

		$criteria = new CDbCriteria;
		//Nos traemos toda la conversacion completa (ENTRE ALUMNO Y SUPERVISOR) de esa Observacion para mostrarla en la vista
		if($model->tipo_observacion_emisor == 2)
		{
			$criteria->condition = " id_servicio_social = '$id_servicio_social' AND
			((emisor_observ = '$model->emisor_observ' AND receptor_observ = '$no_ctrl') OR (emisor_observ = '$no_ctrl' AND receptor_observ = '$model->emisor_observ')) AND
			((tipo_observacion_emisor = 2 AND tipo_observacion_receptor = 3) OR (tipo_observacion_emisor = 3 AND tipo_observacion_receptor = 2))";
			$criteria->order = "id_observacion ASC";
			$modelSSObservacionesServicioSocial = SsObservacionesServicioSocial::model()->findAll($criteria);

			$nombre_supervisor = $this->getNombreSupervisor($model->emisor_observ);
			$foto_emisor = $this->getFotoSupervisor($model->emisor_observ);
			$Programas = $this->getIDprogramaVigenteSupervisor($model->emisor_observ);

			$id_tipo_programa = ($Programas != NULL) ? $Programas[0]['id_tipo_programa'] : 0;

		}else
		if($model->tipo_observacion_emisor == 1)
		{
			$criteria->condition = "id_servicio_social = '$id_servicio_social' AND
			((emisor_observ = '$model->emisor_observ' AND receptor_observ = '$no_ctrl') OR (emisor_observ = '$no_ctrl' AND receptor_observ = '$model->emisor_observ')) AND
			((tipo_observacion_emisor = 1 AND tipo_observacion_receptor = 3) OR (tipo_observacion_emisor = 3 AND tipo_observacion_receptor = 1))";
			$criteria->order = "id_observacion ASC";
			$modelSSObservacionesServicioSocial = SsObservacionesServicioSocial::model()->findAll($criteria);

			$nombre_supervisor = null;
			$foto_emisor = null;

			$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
			$id_tipo_programa = ($modelSSProgramas === NULL) ? 0 : $modelSSProgramas->id_tipo_programa;

		}

		$modelObservacion = new SsObservacionesServicioSocial;
		//Si entra a la conversacion entonces muestra las observaciones como leidas
		$this->getCambiarEstadoObservacionALeidasAlumno($id_observacion, $no_ctrl, $id_servicio_social);

		//ENTRA AQUI HASTA QUE VIENEN DATOS POR POST, CUANDO SE MANDA EL MENSAJE
		if(isset($_POST['SsObservacionesServicioSocial']))
        {
			$modelObservacion->attributes=$_POST['SsObservacionesServicioSocial'];

			if($no_ctrl != $model->emisor_observ)
			{
				$modelObservacion->id_servicio_social = $id_servicio_social;
				$modelObservacion->fecha_registro = date('Y-m-d H:i:s');
				$modelObservacion->tipo_observacion_emisor = 3; //Quien la realiza (3)-Alumno
				$modelObservacion->tipo_observacion_receptor = ($model->tipo_observacion_emisor == 2) ? 2 : 1; //Quien la recibe (3)-Alumno
				$modelObservacion->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
				$modelObservacion->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
				$modelObservacion->emisor_observ = $no_ctrl;
				$modelObservacion->receptor_observ = $model->emisor_observ;
				//$modelObservacion->hilo_observacion = $hilo_observacion;

				if($modelObservacion->save())
				{
					Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					$this->redirect(array('historialObservacionesServicioSocial'));

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
				}

			}else{
				throw new CHttpException(404,'Registro No valido.');
			}
        }

		$this->render('verObservacionRecibidaAlumno',array(
					  'modelObservacion' => $modelObservacion,
					  'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
					  'modelEDatosAlumno' => $modelEDatosAlumno,
					  'no_ctrl' => $no_ctrl,
					  'emisor_observ' => $model->emisor_observ,
					  'nombre_supervisor' => $nombre_supervisor,
					  'foto_emisor' => $foto_emisor,
					  'id_tipo_programa' => $id_tipo_programa,
					  'servicioSocialAlumno' => $servicioSocialAlumno
		));
	}
	//Vista Conversacion del alumno con SUPERVISOR O JEFE OFICINA SERVICIO SOCIAL

	//Para actualizar a "VISTO" las observaciones enviadas al SUPERVISOR, sean uno o varias
	public function getCambiarEstadoObservacionALeidasAlumno($id_observacion, $no_ctrl, $id_servicio_social)
	{
		$fec_actual = date('Y-m-d H:i:s');

		$model = SsObservacionesServicioSocial::model()->findByAttributes(

			array('id_observacion'=>$id_observacion,
					'id_servicio_social'=>$id_servicio_social,
					'receptor_observ'=>$no_ctrl
			),
			array(
				'condition'=>'fue_leida=:status',
				'params'=>array(':status'=>false)
			)

		);

		if($model != NULL)
		{
			$query = "Update pe_planeacion.ss_observaciones_servicio_social
					set fue_leida = true, fecha_leida = '$fec_actual'
					where receptor_observ = '$no_ctrl'
					AND id_observacion = '$id_observacion' AND id_servicio_social = '$id_servicio_social' AND
					fue_leida = false AND fecha_leida is null";

			if(!Yii::app()->db->createCommand($query)->queryAll())
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');

		}

	}
	//Para actualizar a "VISTO" las observaciones enviadas al SUPERVISOR, sean uno o varias

	//Repuesta del Alumno
	public function actionResponderObservacion($id_observacion, $emisor_observ)
	{
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;
		$modelObservacion = new SsObservacionesServicioSocial;

		//Nos traemos los datos del alumno y la observacion
		$modelSSObservacionesServicioSocial = $this->loadModel($id_observacion);
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($modelSSObservacionesServicioSocial->id_servicio_social);

		if($modelSSServicioSocial === NULL)
			throw new CHttpException(404,'No hay datos de ese Servicio Social.');

		$modelEDatosAlumno = EDatosAlumno::model()->findByPk($modelSSServicioSocial->no_ctrl);
		if($modelEDatosAlumno === NULL)
			throw new CHttpException(404,'No hay datos de esa observación.');

		if(isset($_POST['SsObservacionesServicioSocial']))
        {
			$modelObservacion->attributes=$_POST['SsObservacionesServicioSocial'];
			$modelObservacion->id_servicio_social = $modelSSObservacionesServicioSocial->id_servicio_social;
			$modelObservacion->fecha_registro = date('Y-m-d H:i:s');
			$modelObservacion->tipo_observacion_emisor = 3; //Quien la realiza (3)-Alumno
			$modelObservacion->tipo_observacion_receptor = 2; //Quien la realiza (3)-Alumno
			$modelObservacion->fue_leida = false; //false hasta que la lea el ALumno cambiara a true
			$modelObservacion->fecha_leida = null; //null hasta que el alumno la lea se guardara la fecha en que fue leida
			$modelObservacion->emisor_observ = $no_ctrl;
			$modelObservacion->receptor_observ = $emisor_observ;
			//$modelObservacion->hilo_observacion = $modelSSObservacionesServicioSocial->hilo_observacion;

			if($modelObservacion->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				//Se guarda el hilo de la conversacion
				$this->redirect(array('verObservacionRecibidaAlumno','id_observacion'=>$modelSSObservacionesServicioSocial->id_observacion));

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
        }

		$this->render('responderObservacion',array(
					  'modelObservacion' => $modelObservacion,
					  'modelSSObservacionesServicioSocial'=>$modelSSObservacionesServicioSocial,
					  'modelEDatosAlumno' => $modelEDatosAlumno
		));
	}

	public function getIDprogramaVigenteSupervisor($rfcSupervisor)
	{

		$interno = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$externo = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

		$result1 = Yii::app()->db->createCommand($interno)->queryAll();
		$result2 = Yii::app()->db->createCommand($externo)->queryAll();

		if($result1 != NULL)
		{
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_interno rpi
			on rpi.id_programa = pss.id_programa
			where rpi.\"rfcEmpleado\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$ProgramaServicioSocial = Yii::app()->db->createCommand($query)->queryAll();

		}else
		if($result2 != NULL){
			$query =
			"select * from pe_planeacion.ss_programas pss
			join pe_planeacion.ss_responsable_programa_externo rpe
			on rpe.id_programa = pss.id_programa
			where rpe.\"rfcSupervisor\" = '$rfcSupervisor' AND pss.id_status_programa = 1
			";

			$ProgramaServicioSocial = Yii::app()->db->createCommand($query)->queryAll();
		}else{

		}

		return $ProgramaServicioSocial;
	}

	public function getNombreSupervisor($rfcSupervisor)
	{
		$interno = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$externo = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

		$result1 = Yii::app()->db->createCommand($interno)->queryAll();
		$result2 = Yii::app()->db->createCommand($externo)->queryAll();

		//Es interno
		if($result1 != NULL)
		{
			$ProgramaServicioSocial = $result1[0]['nmbEmpleado'];

		}else
		if($result2 != NULL){

			$ProgramaServicioSocial = $result2[0]['nombre_supervisor'];
		}else{
			$ProgramaServicioSocial = null;
		}

		return $ProgramaServicioSocial;
	}

	public function getFotoSupervisor($rfcSupervisor)
	{
		$interno = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$externo = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";

		$result1 = Yii::app()->db->createCommand($interno)->queryAll();
		$result2 = Yii::app()->db->createCommand($externo)->queryAll();

		//Es interno
		if($result1 != NULL)
		{
			//$ProgramaServicioSocial = $result1[0]['nmbEmpleado'];
			//Se jala la foto de la BD la foto del supervisor INTERNO
			$foto = "supervisor_512.png";

		}else
		if($result2 != NULL){

			$foto = $result2[0]['foto_supervisor'];
		}else{

			$foto = "supervisor_512.png";
		}

		return $foto;
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new SsObservacionesServicioSocial;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsObservacionesServicioSocial']))
		{
			$model->attributes=$_POST['SsObservacionesServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_observacion));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsObservacionesServicioSocial']))
		{
			$model->attributes=$_POST['SsObservacionesServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_observacion));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsObservacionesServicioSocial');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$model=new SsObservacionesServicioSocial_('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SsObservacionesServicioSocial']))
			$model->attributes=$_GET['SsObservacionesServicioSocial'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$modelSSObservacionesServicioSocial=SsObservacionesServicioSocial::model()->findByPk($id);

		if($modelSSObservacionesServicioSocial===null)
		{
			throw new CHttpException(404,'No hay datos de esa observación.');
		}

		return $modelSSObservacionesServicioSocial;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-observaciones-servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function validarObservacionesServicioSocial($id_observacion, $id_servicio_social, $no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_servicio_social ss
				join pe_planeacion.ss_observaciones_servicio_social oss
				on oss.id_servicio_social = ss.id_servicio_social
				where ss.id_servicio_social = '$id_servicio_social' and oss.id_observacion = '$id_observacion' and ss.no_ctrl = '$no_ctrl'
				and (ss.id_estado_servicio_social > 0 and ss.id_estado_servicio_social < 5)
				";

		$datos = Yii::app()->db->createCommand($query)->queryAll();

		return ($datos != NULL) ? true : false;
	}

	public function validarObservacionesSupervisor($rfcSupervisor, $id_servicio_social, $no_ctrl)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_observaciones_servicio_social oss
				on oss.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and ss.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$no_ctrl'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_observaciones_servicio_social oss
				on oss.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and ss.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$no_ctrl'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}
}
