<?php

class SsEvaluacionBimestralController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'nuevaEvaluacionBimestral', //Duda
								 'evaluacionDeptoReporteBimestral', //form evaluacion bimestral del depto.
								 'evaluacionSupervisorExtReporteBimestral',
								 'editarCalificacionCriterioSupervisorExterno', //Evaluacion reporte bimestral servicio social externo
								 'editarCalificacionCriterioDepto', //Form para editar la calificacion del criterio seleccionado (depto)
								 'editarReporteBimestralTodo' //Editar Calificaciones del Reporte Bimestral
								),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
			),
			array('allow',
				   'actions'=>array(
						 			'evaluacionSupervisorReporteBimestral',
									'editarCalificacionCriterioSupervisor',
									'calificacionDeptoReporteBimestralExterno'
								),
				'roles'=>array('vinculacion_supervisor_servicio_social'),
				//'users' =>array('@')
           	),
			
		);
	}

	//Evaluacion Repportes Bimestrales Externos
	public function actionEvaluacionSupervisorExtReporteBimestral($id_reporte_bimestral)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcJefeOfServSocial'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarEvaluacionSupervisorExterno($id_reporte_bimestral, $rfcSupervisor))
			throw new CHttpException(4045,'Esta información No esta disponible.');

		$modelSSEvaluacionBimestral = new SsEvaluacionBimestral_('searchXEvaluacionBimestral');
		$modelSSEvaluacionBimestral->unsetAttributes();  // clear any default values
		$id_tipo = 1;
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPK($id_reporte_bimestral);

		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese reporte bimestral.');

		$servicioSocialAlumno = $this->getDatosServicioSocial($modelSSReportesBimestral->id_servicio_social, $id_reporte_bimestral);

		//Obtenemos la fecha_inicio y fecha_fin del programa
		$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

		if(isset($_GET['SsEvaluacionBimestral']))
			$modelSSEvaluacionBimestral->attributes=$_GET['SsEvaluacionBimestral'];

		$this->render('evaluacionSupervisorExtReporteBimestral',array(
					'modelSSEvaluacionBimestral'=>$modelSSEvaluacionBimestral,
					'id_reporte_bimestral' => $id_reporte_bimestral,
					'id_tipo' => $id_tipo,
					'servicioSocialAlumno' => $servicioSocialAlumno,
					'periodo_inicio' => $periodo_inicio,
					'periodo_fin' => $periodo_fin
		));
	}

	/*Evaluacion supervisor del Reporte Bimestral*/
	public function actionEvaluacionSupervisorReporteBimestral($id_reporte_bimestral)
	{

		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarEvaluacionSupervisor($id_reporte_bimestral, $rfcSupervisor))
			throw new CHttpException(4045,'Esta información No esta disponible.');

		$modelSSEvaluacionBimestral = new SsEvaluacionBimestral_('searchXEvaluacionBimestral');
		$modelSSEvaluacionBimestral->unsetAttributes();  // clear any default values
		$id_tipo = 1;
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPK($id_reporte_bimestral);

		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese reporte bimestral.');

		$servicioSocialAlumno = $this->getDatosServicioSocial($modelSSReportesBimestral->id_servicio_social, $id_reporte_bimestral);

		/*Obtenemos la fecha_inicio y fecha_fin del programa */
		$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

		if(isset($_GET['SsEvaluacionBimestral']))
		{
			$modelSSEvaluacionBimestral->attributes=$_GET['SsEvaluacionBimestral'];
		}

		$this->render('evaluacionSupervisorReporteBimestral',array(
					  'modelSSEvaluacionBimestral'=>$modelSSEvaluacionBimestral,
					  'id_reporte_bimestral' => $id_reporte_bimestral,
					  'id_tipo' => $id_tipo,
					  'servicioSocialAlumno' => $servicioSocialAlumno,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin
		));
	}
	/*Evaluacion supervisor del Reporte Bimestral*/

	//Editar Calificacion del Reporte Bimestral
	public function actionEditarReporteBimestralTodo($id_reporte_bimestral, $id_servicio_social)
	{
		if(!$this->validarReporteBimestral($id_reporte_bimestral, $id_servicio_social))
			throw new CHttpException(4045,'El Reporte No Existe.');

		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPK($id_reporte_bimestral);

		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese reporte bimestral.');

		$modelSSEvaluacionBimestral = new SsEvaluacionBimestral_('searchXEvaluacionBimestral');
		$modelSSEvaluacionBimestral->unsetAttributes();  // clear any default values

		$servicioSocialAlumno = $this->getDatosServicioSocial($modelSSReportesBimestral->id_servicio_social, $id_reporte_bimestral);

		/*Obtenemos la fecha_inicio y fecha_fin del programa */
		$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

		if(isset($_GET['SsEvaluacionBimestral']))
		{
			$modelSSEvaluacionBimestral->attributes=$_GET['SsEvaluacionBimestral'];
		}

		$this->render('editarReporteBimestralTodo',array(
					  'modelSSEvaluacionBimestral'=>$modelSSEvaluacionBimestral,
					  'modelSSReportesBimestral' => $modelSSReportesBimestral,
					  'id_reporte_bimestral' => $id_reporte_bimestral,
					  'servicioSocialAlumno' => $servicioSocialAlumno,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin
		));
	}

	public function validarReporteBimestral($id_reporte_bimestral, $id_servicio_social)
	{
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByAttributes(
			array('id_reporte_bimestral' => $id_reporte_bimestral,
				  'id_servicio_social' => $id_servicio_social
			)
		);

		return ($modelSSReportesBimestral === NULL) ? false : true;
	}

	/*Evaluacion depto. del Reporte Bimestral*/
	public function actionEvaluacionDeptoReporteBimestral($id_reporte_bimestral)
	{
		$modelSSEvaluacionBimestral=new SsEvaluacionBimestral_('searchXEvaluacionBimestral');
		$modelSSEvaluacionBimestral->unsetAttributes();  // clear any default values
		$id_tipo = 2;
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPK($id_reporte_bimestral);

		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese reporte bimestral.');

		$servicioSocialAlumno = $this->getDatosServicioSocial($modelSSReportesBimestral->id_servicio_social, $id_reporte_bimestral);

		$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

		if(isset($_GET['SsEvaluacionBimestral']))
		{
			$modelSSEvaluacionBimestral->attributes=$_GET['SsEvaluacionBimestral'];
		}

        $this->render('evaluacionDeptoReporteBimestral',array(
					  'modelSSEvaluacionBimestral'=>$modelSSEvaluacionBimestral,
					  'id_reporte_bimestral' => $id_reporte_bimestral,
					  'id_tipo' => $id_tipo,
					  'servicioSocialAlumno' => $servicioSocialAlumno,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin
        ));
	}
	/*Form de Evaluacion depto. del Reporte Bimestral*/

	/*Obtener el periodo de inicio y fin del servicio social */
	public function getPeriodoServicioSocial($fecha)
	{
		$dt = new DateTime($fecha);
		$mes = $dt->format("m");
		$anio = $dt->format("Y");
		$periodo="";

		if($mes <= 7)
		{
			$periodo = "ENERO-JULIO"." de ".$anio;
		}else
		if($mes > 7){
			$periodo = "AGOSTO-DICIEMBRE"." de ".$anio;
		}

		return $periodo;
	}
	/*Obtener el periodo de inicio y fin del servicio social */

	/*Datos para vista de la evaluacion del reporte bimestral de servicio social */
	public function getDatosServicioSocial($id_servicio_social, $id_reporte_bimestral)
	{
		$query = "
		select
		eda.\"nmbAlumno\" as name_alumno,
		ss.no_ctrl,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		pss.nombre_programa as programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		pss.id_periodo_programa as periodo_servicio_social,
		pss.id_tipo_programa,
		rb.bimestres_correspondiente,
		rb.bimestre_final,
		rb.fecha_inicio_rep_bim,
		rb.fecha_fin_rep_bim,
		rb.calificacion_reporte_bimestral,
		(select anio from pe_planeacion.ss_registro_fechas_servicio_social where id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial) as año_servicio_social
		from pe_planeacion.ss_servicio_social ss
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_reportes_bimestral rb
		on rb.id_servicio_social = ss.id_servicio_social
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
		";

		$datos_servsocial = Yii::app()->db->createCommand($query)->queryAll();

		return $datos_servsocial;
	}
	/*Datos para vista de la evaluacion del reporte bimestral de servicio social */

	/*Editar evaluacion depto. del reporte bimestral del alumno*/
	public function actionEditarCalificacionCriterioDepto()
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		if (isset($_POST['id_evaluacion_bimestral']) && (isset($_POST['evaluacion_b'])))
		{
			$id_evaluacion_bimestral = $_POST['id_evaluacion_bimestral'];
			$cal = (int)$_POST['evaluacion_b'];
			$modelSSEvaluacionBimestral = $this->loadModel($id_evaluacion_bimestral);

			if(RegularExpression::isEnteroPositivo($cal))
			{
				$modelSSEvaluacionBimestral->evaluacion_b = $cal;

				if($modelSSEvaluacionBimestral->save())
				{
					//Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					/*Actualizamos calificacion final del reporte bimestral servicio social INTERNO */
					$inserto = Yii::app()->db->createCommand("select pe_planeacion.getCalificacionReporteBimestral('$modelSSEvaluacionBimestral->id_reporte_bimestral')")->queryAll();

					if(!$inserto)
						die("Error!!!! Ocurrio un error al asignar la Calificación al criterio.");

					//$this->redirect(array('evaluacionSupervisorReporteBimestral','id_reporte_bimestral'=>$modelSSEvaluacionBimestral->id_reporte_bimestral));
				}else{	die("Error!!! no se actualizaron los datos."); }

			}else{ die("Error!!! La calificación debe ser un entero positivo."); }
		}

	}
	/*Editar evaluacion depto. del reporte bimestral del alumno*/

	/*Editar evaluacion supervisor del reporte bimestral del alumno*/
	public function actionEditarCalificacionCriterioSupervisor()
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		if (isset($_POST['id_evaluacion_bimestral']) && (isset($_POST['evaluacion_b'])))
		{
			$id_evaluacion_bimestral = $_POST['id_evaluacion_bimestral'];
			$cal = (int)$_POST['evaluacion_b'];
			$modelSSEvaluacionBimestral = $this->loadModel($id_evaluacion_bimestral);

			if(RegularExpression::isEnteroPositivo($cal))
			{

				$modelSSEvaluacionBimestral->evaluacion_b = $cal;

				if($modelSSEvaluacionBimestral->save())
				{
					//Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					/*Actualizamos calificacion final del reporte bimestral servicio social INTERNO */
					$inserto = Yii::app()->db->createCommand("select pe_planeacion.getCalificacionReporteBimestral('$modelSSEvaluacionBimestral->id_reporte_bimestral')")->queryAll();

					if(!$inserto)
						die("Error!!!! Ocurrio un error al asignar la Calificación al criterio.");

					//$this->redirect(array('evaluacionSupervisorReporteBimestral','id_reporte_bimestral'=>$modelSSEvaluacionBimestral->id_reporte_bimestral));
				}else{	die("Error!!! no se actualizaron los datos."); }

			}else{ die("Error!!! La calificación debe ser un entero positivo."); }

		}

	}
	/*Editar evaluacion supervisor del reporte bimestral del alumno*/

	/*Editar evaluacion jefe ofic. de servicio social como Supervisor Externo*/
	public function actionEditarCalificacionCriterioSupervisorExterno()
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		if (isset($_POST['id_evaluacion_bimestral']) && (isset($_POST['evaluacion_b'])))
		{
			$id_evaluacion_bimestral = $_POST['id_evaluacion_bimestral'];
			$cal = (int)$_POST['evaluacion_b'];
			$modelSSEvaluacionBimestral = $this->loadModel($id_evaluacion_bimestral);

			if(RegularExpression::isEnteroPositivo($cal))
			{

				$modelSSEvaluacionBimestral->evaluacion_b = $cal;

				if($modelSSEvaluacionBimestral->save())
				{
					//Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					/*Actualizamos calificacion final del reporte bimestral servicio social INTERNO */
					$inserto = Yii::app()->db->createCommand("select pe_planeacion.getCalificacionReporteBimestral('$modelSSEvaluacionBimestral->id_reporte_bimestral')")->queryAll();

					if(!$inserto)
						die("Error!!!! Ocurrio un error al asignar la Calificación al criterio.");

					//$this->redirect(array('evaluacionSupervisorReporteBimestral','id_reporte_bimestral'=>$modelSSEvaluacionBimestral->id_reporte_bimestral));
				}else{	die("Error!!! no se actualizaron los datos."); }

			}else{ die("Error!!! La calificación debe ser un entero positivo."); }
		}
		
	}
	/*Editar evaluacion jefe ofic. de servicio social como Supervisor Externo*/

	public function actionCalificacionDeptoReporteBimestralExterno($id_reporte_bimestral)
	{
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPK($id_reporte_bimestral);
		$modelSSExpedientesReportesBimestralesServicioSocialExterno = SsExpedientesReportesBimestralesServicioSocialExterno::model()->findByPK($id_reporte_bimestral);
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($modelSSReportesBimestral->id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese Servicio Social.');

		if($modelSSExpedientesReportesBimestralesServicioSocialExterno === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese Expediente.');

		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de ese Reporte Bimestral a evaluar.');


		if(isset($_POST['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes=$_POST['SsReportesBimestral'];
			//Una vez que actualizamos la calificacion del Reporte Bimestral, validamos
			//$modelSSReportesBimestral->valida_oficina_servicio_social = date('Y-m-d H:i:s');
			if(is_numeric($modelSSReportesBimestral->calificacion_reporte_bimestral))
			{
				if($modelSSReportesBimestral->calificacion_reporte_bimestral >= 0 && $modelSSReportesBimestral->calificacion_reporte_bimestral <= 100)
				{
					if($modelSSReportesBimestral->save())
					{
						Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
						$this->redirect(array('ssReportesBimestral/listaAdminReportesBimestralesExterno'));
					}else{
						Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
						//die('Error!!! no se guardaron los datos');
					}

				}else{
					Yii::app()->user->setFlash('danger', 'Error!!! La calificación del Reporte Bimestral debe ser un numero entero positivo.');
					//die('Error!!! La calificación del Reporte Bimestral debe ser un numero entero positivo entre 0 y 100.');
				}

			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! La calificación del Reporte Bimestral debe ser un numero entero positivo.');
				//die('Error!!! La calificación del Reporte Bimestral debe ser un numero entero positivo.');
			}

		}

		$this->render('calificacionDeptoReporteBimestralExterno',array(
					'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					'id_reporte_bimestral' => $id_reporte_bimestral,
					'modelSSExpedientesReportesBimestralesServicioSocialExterno' => $modelSSExpedientesReportesBimestralesServicioSocialExterno,
					'modelSSServicioSocial' => $modelSSServicioSocial

		));
	}

	public function compararCriterio($id_evaluacion_bimestral, $id_reporte_bimestral, $id_criterio)
	{
		$query =
		"select ce.valor_a from pe_planeacion.ss_evaluacion_bimestral as eb
		join pe_planeacion.ss_criterios_a_evaluar as ce
		on ce.id_criterio = eb.id_criterio
		where eb.id_evaluacion_bimestral = '$id_evaluacion_bimestral'
		and eb.id_reporte_bimestral= '$id_reporte_bimestral'
		and ce.id_criterio = '$id_criterio';
		";

		$valor_a = Yii::app()->db->createCommand($query)->queryAll();

		return $valor_a[0]['valor_a'];
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function loadModel($id)
	{
		$modelSSEvaluacionBimestral = SsEvaluacionBimestral::model()->findByPk($id);

		if($modelSSEvaluacionBimestral===null)
			throw new CHttpException(404,'Error!!! no se encontraron datos de esa evaluación.');

		return $modelSSEvaluacionBimestral;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsEvaluacionBimestral $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='evaluacion-bimestral-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function validarEvaluacionSupervisor($id_reporte_bimestral, $rfcSupervisor)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query="select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_reportes_bimestral rb
				on rb.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and rb.id_reporte_bimestral = '$id_reporte_bimestral'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query="select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_reportes_bimestral rb
				on rb.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and rb.id_reporte_bimestral = '$id_reporte_bimestral'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}

	public function validarEvaluacionSupervisorExterno($id_reporte_bimestral, $rfcSupervisor)
	{
		$bandera1;
		$bandera2;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		if($modelHEmpleados != NULL)
		{
			$query = "select * from public.h_ocupacionpuesto where \"cveDepartamentoEmp\" = 6 and \"cvePuestoGeneral\" = '04' and
							\"cvePuestoParticular\" = '02' and \"rfcEmpleado\" = '$rfcSupervisor' ";
			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else{
			$bandera1 = false;
			$bandera2 = false;
		}
		$query2 = "select * from pe_planeacion.ss_reportes_bimestral where id_reporte_bimestral = '$id_reporte_bimestral' ";

		$datos2 = Yii::app()->db->createCommand($query2)->queryAll();
		$bandera2 = ($datos2 != NULL) ? true : false;

		return ($bandera == true AND $bandera2 == true) ? true : false;

	}

	public function validarEdicionCalCriterioSupervisor($id_evaluacion_bimestral, $id_reporte_bimestral, $rfcSupervisor)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{

			$query ="select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_reportes_bimestral rb
				on rb.id_servicio_social = ss.id_servicio_social
				join pe_planeacion.ss_evaluacion_bimestral eb
				on eb.id_reporte_bimestral = rb.id_reporte_bimestral
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
				and eb.id_evaluacion_bimestral = '$id_evaluacion_bimestral'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_reportes_bimestral rb
				on rb.id_servicio_social = ss.id_servicio_social
				join pe_planeacion.ss_evaluacion_bimestral eb
				on eb.id_reporte_bimestral = rb.id_reporte_bimestral
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
				and eb.id_evaluacion_bimestral = '$id_evaluacion_bimestral'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}
}
