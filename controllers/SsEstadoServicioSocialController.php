<?php

class SsEstadoServicioSocialController extends Controller
{
	
	//public $layout='//layouts/column2';

	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaEstadoServicioSocial', 
								 'editarEstadoServicioSocial'
								),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users' => array('@')
		    ),
			array('allow',
				'actions'=>array('listaEstadoServicioSocial'),
   				'roles'=>array('vinculacion_supervisor_servicio_social', 'alumno'),
			),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionListaEstadoServicioSocial()
	{
		$modelSSEstadoServicioSocial =new SsEstadoServicioSocial_('search');
		$modelSSEstadoServicioSocial->unsetAttributes();  // clear any default values

		//Acceso
		$oficina = $this->tipoAcceso();

		if(isset($_GET['SsEstadoServicioSocial']))
		{
			$modelSSEstadoServicioSocial->attributes=$_GET['SsEstadoServicioSocial'];
		}
			
		$this->render('listaEstadoServicioSocial',array(
					  'modelSSEstadoServicioSocial'=>$modelSSEstadoServicioSocial,
					  'oficina' => $oficina
		));
	}

	public function tipoAcceso()
	{
		$bandera = true;
		if(Yii::app()->user->checkAccess('vinculacion_oficina_servicio_social'))
		{
			$bandera = true;
		}elseif(Yii::app()->user->checkAccess('vinculacion_supervisor_servicio_social') or Yii::app()->user->checkAccess('alumno'))
		{
			$bandera = false;
		}

		return $bandera;
	}

	public function actionEditarEstadoServicioSocial($id_estado_servicio_social)
	{
		$modelSSEstadoServicioSocial=$this->loadModel($id_estado_servicio_social);

		if(isset($_POST['SsEstadoServicioSocial']))
		{
			$modelSSEstadoServicioSocial->attributes=$_POST['SsEstadoServicioSocial'];
			if($modelSSEstadoServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaEstadoServicioSocial'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}	
		}

		$this->render('nuevoEstadoServicioSocial',array(
					'modelSSEstadoServicioSocial'=>$modelSSEstadoServicioSocial,
		));
	}
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new SsEstadoServicioSocial;

		if(isset($_POST['SsEstadoServicioSocial']))
		{
			$model->attributes=$_POST['SsEstadoServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_estado_servicio_social));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['SsEstadoServicioSocial']))
		{
			$model->attributes=$_POST['SsEstadoServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_estado_servicio_social));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsEstadoServicioSocial');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$model=new SsEstadoServicioSocial_('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SsEstadoServicioSocial']))
			$model->attributes=$_GET['SsEstadoServicioSocial'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$modelSSEstadoServicioSocial=SsEstadoServicioSocial::model()->findByPk($id);
		
		if($modelSSEstadoServicioSocial===null)
		{
			throw new CHttpException(404,'No hay datos del estado.');
		}
			
		return $modelSSEstadoServicioSocial;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-estado-servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
