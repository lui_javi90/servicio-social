<?php

class SsCodigosCalidadController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaCodigosCalidadServicioSocial',
								 'editarCodigosCalidadServicioSocial',
								 'nuevoCodigosCalidadServicioSocial'
								),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionListaCodigosCalidadServicioSocial()
	{
		$modelSSCodigosCalidad = new SsCodigosCalidad_('search');

		$modelSSCodigosCalidad->unsetAttributes();  // clear any default values

		if(isset($_GET['SsCodigosCalidad']))
		{
			$modelSSCodigosCalidad->attributes=$_GET['SsCodigosCalidad'];
		}
			
		$this->render('listaCodigosCalidadServicioSocial',array(
			'modelSSCodigosCalidad'=>$modelSSCodigosCalidad,
		));
	}

	public function actionNuevoCodigosCalidadServicioSocial()
	{
		$modelSSCodigosCalidad = new SsCodigosCalidad;

		if(isset($_POST['SsCodigosCalidad']))
		{
			$modelSSCodigosCalidad->attributes=$_POST['SsCodigosCalidad'];
			
			if($modelSSCodigosCalidad->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaCodigosCalidadServicioSocial'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('nuevoCodigosCalidadServicioSocial',array(
						'modelSSCodigosCalidad'=>$modelSSCodigosCalidad,
		));
	}

	public function actionEditarCodigosCalidadServicioSocial($id_codigo_calidad)
	{
		$modelSSCodigosCalidad = $this->loadModel($id_codigo_calidad);

		if(isset($_POST['SsCodigosCalidad']))
		{
			$modelSSCodigosCalidad->attributes=$_POST['SsCodigosCalidad'];
			if($modelSSCodigosCalidad->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaCodigosCalidadServicioSocial'));

			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
				
		}

		$this->render('nuevoCodigosCalidadServicioSocial',array(
					  'modelSSCodigosCalidad'=>$modelSSCodigosCalidad,
		));
	}

	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate()
	{
		$model = new SsCodigosCalidad;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsCodigosCalidad']))
		{
			$model->attributes=$_POST['SsCodigosCalidad'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_codigo_calidad));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsCodigosCalidad']))
		{
			$model->attributes=$_POST['SsCodigosCalidad'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_codigo_calidad));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsCodigosCalidad');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	public function actionAdmin()
	{
		$model=new SsCodigosCalidad_('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SsCodigosCalidad']))
			$model->attributes=$_GET['SsCodigosCalidad'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$modelSSCodigosCalidad = SsCodigosCalidad::model()->findByPk($id);
		
		if($modelSSCodigosCalidad===null)
		{
			throw new CHttpException(404,'No se encontraron datos de ese registro.');
		}
			
		return $modelSSCodigosCalidad;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-codigos-calidad-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
