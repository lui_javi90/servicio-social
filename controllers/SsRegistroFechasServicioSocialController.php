<?php

class SsRegistroFechasServicioSocialController extends Controller
{

	//public $layout='//layouts/column2';


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}


	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaFechasRegistroServicioSocial',
								 'nuevasFechasRegistroServicioSocial',
								 'editarRegistroFechasServSocial',
								 'statusRegistroServicioSocial'
								 ),
				//'roles'=>array('vinculacion_oficina_servicio_social'),
				'users' => array('@')
				),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}

	public function actionListaFechasRegistroServicioSocial()
	{
		
		$modelSSRegistroFechasServicioSocial = new SsRegistroFechasServicioSocial_('search');
		$modelSSRegistroFechasServicioSocial->unsetAttributes();  // clear any default values

		//Estatus de registro de servicio social
		$status_registro = $this->getRegistroFechasServicioSocial();
		//Lista de años
		$lista_anios = $this->getAnios();
		//Periodos
		$lista_periodos = $this->getPeriodosEscolares();

		if(isset($_GET['SsRegistroFechasServicioSocial']))
		{
			$modelSSRegistroFechasServicioSocial->attributes=$_GET['SsRegistroFechasServicioSocial'];
		}

		$this->render('listaFechasRegistroServicioSocial',array(
					  'modelSSRegistroFechasServicioSocial'=>$modelSSRegistroFechasServicioSocial,
					  'lista_anios' => $lista_anios,
					  'lista_periodos' => $lista_periodos,
					  'status_registro' => $status_registro
		));
	}

	/*Para saber el registro de Servicio Social ACTUAL */
	public function getRegistroFechasServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'ssocial_actual = true';
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->find($criteria);

		return ($modelSSRegistroFechasServicioSocial === NULL ) ? false : true;
	}
	/*Para saber el registro de Servicio Social ACTUAL */

	public function actionNuevasFechasRegistroServicioSocial()
	{
		$modelSSRegistroFechasServicioSocial = new SsRegistroFechasServicioSocial;

		$periodos_escolares = $this->getPeriodosEscolares();
		$anio_actual = $this->getAnio(); // Obtenemos el año actual

		if(isset($_POST['SsRegistroFechasServicioSocial']))
		{
			$modelSSRegistroFechasServicioSocial->attributes=$_POST['SsRegistroFechasServicioSocial'];
			//Si se inserta un llena un registro quiere decir que sera el servicio social actual
			$modelSSRegistroFechasServicioSocial->ssocial_actual = true;
			$modelSSRegistroFechasServicioSocial->anio = $anio_actual;
			$modelSSRegistroFechasServicioSocial->cambiar_fech_limite_insc = false;

			/*Validar que no haya mas de un registro con el mismo periodo y año*/
			if($this->validarPeriodoNoRepetido($modelSSRegistroFechasServicioSocial->id_periodo, $modelSSRegistroFechasServicioSocial->anio))
			{
				if($modelSSRegistroFechasServicioSocial->save())
				{
					//Si se inserta correctamente entonces se pone como actual el ultimo registro insertado (servicio social actual)
					$this->actualizarRegistroServicioSocialActual($modelSSRegistroFechasServicioSocial->id_registro_fechas_ssocial);
					Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					$this->redirect(array('listaFechasRegistroServicioSocial'));
				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al guardar los datos');
				}
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! ya existe un registro con ese periodo y año.');
				//die('Error!!! ya existe un registro con ese periodo y año.');
			}
			
		}

		$this->render('nuevasFechasRegistroServicioSocial',array(
					  'modelSSRegistroFechasServicioSocial'=>$modelSSRegistroFechasServicioSocial,
					  'periodos_escolares' => $periodos_escolares,
		));
	}

	/*Validar que no se quiera dar de ALTA un registro de Servicio Social el mismo periodo escolar y año */
	public function validarPeriodoNoRepetido($periodo, $anio)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_periodo = '$periodo' AND anio = '$anio' ";
		$modelSSRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->find($criteria);

		//Si no hay registro con ese periodo y año entonces devuelve TRUE, caso contrario devuelve FALSE
		$bandera = ($modelSSRegistroFechasServicioSocial === NULL) ? true : false;

		return $bandera;
	}
	/*Validar que no se quiera dar de ALTA un registro de Servicio Social el mismo periodo escolar y año */

	public function actionEditarRegistroFechasServSocial($id_registro_fechas_ssocial)
	{
		$modelSSRegistroFechasServicioSocial=$this->loadModel($id_registro_fechas_ssocial);

		$periodos_escolares = $this->getPeriodosEscolares();
		$lista_anio = $this->getAnio();

		if(isset($_POST['SsRegistroFechasServicioSocial']))
		{
			$modelSSRegistroFechasServicioSocial->attributes=$_POST['SsRegistroFechasServicioSocial'];
			$modelSSRegistroFechasServicioSocial->cambiar_fech_limite_insc = true;
			if($modelSSRegistroFechasServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaFechasRegistroServicioSocial'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se encontro registro.');
			}
		}

		$this->render('editarRegistroFechasServSocial',array(
					  'modelSSRegistroFechasServicioSocial'=>$modelSSRegistroFechasServicioSocial,
					  'periodos_escolares' => $periodos_escolares,
					  'lista_anio' => $lista_anio
		));
	}

	//Cuando se deshabilite el registro de fechas de Servicio Social Verificar cuantos Programa hay vigentes aun
	public function actionStatusRegistroServicioSocial($id_registro_fechas_ssocial, $oper)
	{
		$modelSSRegistroFechasServicioSocial = $this->loadModel($id_registro_fechas_ssocial);

		//Valida que no haya Programas Semestrales o Especiales Vigentes en este registro de fechas de servicio social
		if(!$this->validaProgramasSemestralesEspecialesFueronFinalizados($modelSSRegistroFechasServicioSocial->id_registro_fechas_ssocial))
		{
			try
			{
				if($modelSSRegistroFechasServicioSocial)
				{ 

					$modelSSRegistroFechasServicioSocial->ssocial_actual = $oper;

					if($modelSSRegistroFechasServicioSocial->save())
					{
						Yii::app()->user->setFlash('success', "Se deshabilitó correctamente el Registro de Fechas de Servicio Social!!!");
						//Si se realizo correctamente el cambio
						$transaction->commit();
						echo CJSON::encode( [ 'code' => 200 ] );

					}else{

						Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al deshabilitar el Registro de Fechas de Servicio Social.");
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
						echo CJSON::encode( $modelSSRegistroFechasServicioSocial->getErrors() );
					}
				}

			}catch(Exception $e)
			{
				Yii::app()->user->setFlash('danger', "Upps!!! ocurrio un error al deshabilitar el Registro de Fechas de Servicio Social.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelSSRegistroFechasServicioSocial->getErrors() );
			}

		}else{

			Yii::app()->user->setFlash('danger', 'Error!!! Existen Programas VIGENTES que son Semestrales o Especiales y no han sido FINALIZADOS.');
		}
		
	}

	public function loadModel($id)
	{
		$modelSsRegistroFechasServicioSocial = SsRegistroFechasServicioSocial::model()->findByPk($id);

		if($modelSsRegistroFechasServicioSocial === null)
			throw new CHttpException(404,'No existe Registro de ese Servicio Social.');
			
		return $modelSsRegistroFechasServicioSocial;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ss-registro-fechas-servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*****************************GETTERS AND SETTERS*****************************/
	public function validaProgramasSemestralesEspecialesFueronFinalizados($id_registro_fechas_ssocial)
	{
		/*
		--Programas Semestrales y Especiales vigentes en el registro de fechas vigente de servicio social
		--id_periodo_programa = 1 -> SEMESTRAL
		--id_periodo_programa = 3 -> ESPECIAL
		--id_status_programa = 1 -> ALTA
		*/

		$qry_prog_vig_reg_vig = "select count(p.id_programa) as programas_vigentes
								from pe_planeacion.ss_programas p
								join pe_planeacion.ss_registro_fechas_servicio_social rf
								on rf.id_registro_fechas_ssocial = p.id_registro_fechas_ssocial
								where (p.id_periodo_programa = 1 OR p.id_periodo_programa = 3) AND
								p.id_registro_fechas_ssocial = '$id_registro_fechas_ssocial' AND p.id_status_programa = 1 AND
								rf.ssocial_actual = true ";

		$total_prog_vig_reg_vig = Yii::app()->db->createCommand($qry_prog_vig_reg_vig)->queryAll();

		return ($total_prog_vig_reg_vig[0]['programas_vigentes'] > 0) ? true : false;
	}

	public function actualizarRegistroServicioSocialActual($id_registro_fechas_ssocial)
	{
		$query = "Update pe_planeacion.ss_registro_fechas_servicio_social
		set ssocial_actual = false
		where id_registro_fechas_ssocial != '$id_registro_fechas_ssocial' ";

		Yii::app()->db->createCommand($query)->queryAll();

	}

	public function getPeriodosEscolares()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = " \"numPeriodo\" != '3' "; //Son 32 estados en el pais

		$modelEPeriodos = EPeriodos::model()->findAll($criteria);

		$lista_periodos = CHtml::listData($modelEPeriodos, "numPeriodo", "dscPeriodo");

		return $lista_periodos;
	}

	//Obtenemos el año actual
	public function getAnio()
	{
		$hoy = getdate();
		
		return trim($hoy['year']);
	}

	public function getAnios()
	{
		$criteria = new CDbCriteria();
		$criteria->select = "distinct(anio) as anio";
		
		$Anios = SsRegistroFechasServicioSocial::model()->findAll($criteria);

		$lista_anios = CHtml::listData($Anios, "anio", "anio");

		return $lista_anios;

	}

	/*****************************GETTERS AND SETTERS*****************************/
}
