<?php

class SsHorarioDiasHabilesProgramasController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()//ListaHorariosProgramas
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaHorariosProgramas',
								 'detalleHorarioPrograma',
								 'editarHorariosProgramas',//Edita el Horario del Programa
								 'nuevoDiaHorarioPrograma',//Agregar nuevo dia habil al horario
								 'editarDiaHorarioPrograma',//Edita los dias del horario
								 'nuevoHorariosProgramas',
								 'eliminarDiaHorarioPrograma',//elimina los dias del horario
								 //'editarHorarioProgramaServicioSocial', //Editar el Horario del programa
								 'eliminarDiaProgramaSupervisor'
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
				),
				array('allow',  // allow all users to perform 'index' and 'view' actions
					'actions'=>array(
								  'listaHorarioProgramaSupervisor',
								  'nuevoDiaProgramaSupervisor',
								  'editarHorarioProgramaSupervisor', //Editar el Horario del programa
								  'eliminarDiaProgramaSupervisor' //Eliminar dia del programa
								 ),
				'roles'=>array('vinculacion_supervisor_servicio_social'),
				//'users' => array('@')
				),
				
		);
	}

	public function actionEliminarDiaProgramaSupervisor($id_horario, $id_programa)
	{
		$modelSSHorarioDiasHabilesProgramas = $this->loadModel($id_horario);

		//Validar que minimo haya un dia agregado en el horario del Programa del supervisor
		if($this->validarExistaMinimoUnDiaDiaHorarioProgramaSupervisor($id_programa))
		{
			if($modelSSHorarioDiasHabilesProgramas->delete())
			{
				Yii::app()->user->setFlash('success', 'Día eliminado correctamente!!!');
				$this->redirect(array('listaHorarioProgramaSupervisor','id_programa'=>$id_programa));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al eliminar el día del horario del Programa.');
				//die('Error!!! ocurrio un error al eliminar el día del horario del Programa.');
			}
		}else{
			Yii::app()->user->setFlash('danger', 'Error!!! Debes tener mínimo un día registrado en el horario del Programa.');
			$this->redirect(array('listaHorarioProgramaSupervisor','id_programa'=>$id_programa));
			//die('Error!!! Debes tener mínimo un día registrado en el horario del Programa.');
		}
	}

	public function validarExistaMinimoUnDiaDiaHorarioProgramaSupervisor($id_programa)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_programa = '$id_programa' ";
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAll($criteria);

		$bandera = (sizeof($modelSSHorarioDiasHabilesProgramas) >= 2) ? true : false;

		return $bandera;
	}

	//Agregamos el Horario del Programa
	public function actionNuevoDiaProgramaSupervisor($id_programa)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarProgramaSupervisor($rfcSupervisor, $id_programa))
			throw new CHttpException(4044,'Este Programa No te corresponde.');

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;
      	$lista_dias = $this->getDias($id_programa);

        if(isset($_POST['SsHorarioDiasHabilesProgramas']))
        {
            $modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];
			//Asiganos el ID del Programa para saber a quien pertenece el Horario
			$modelSSHorarioDiasHabilesProgramas->id_programa = $id_programa;

			try
			{
				if($modelSSHorarioDiasHabilesProgramas->hora_inicio != null AND $modelSSHorarioDiasHabilesProgramas->hora_fin != null AND $modelSSHorarioDiasHabilesProgramas->id_dia_semana != null)
				{
					//throw new CHttpException(404,'Todos los campos deben ser completados.');

					if($modelSSHorarioDiasHabilesProgramas->hora_inicio < $modelSSHorarioDiasHabilesProgramas->hora_fin)
					{
						//Se inserta el dia al horario del Programa
						if($modelSSHorarioDiasHabilesProgramas->save())
						{
							//cuando se agregue el primer dia al Horario del Programa pasar Programa a ALTA
							$qrys = "select * from pe_planeacion.ss_horario_dias_habiles_programas
									where id_programa = '$id_programa' ";

							$model1 = Yii::app()->db->createCommand($qrys)->queryAll();

							//Si es el primer dia que se agrega al Horario del Programa entra al IF a cambiar el Estatus del Programa a ALTA
							if(count($model1) == 1)
							{
								$model = SsProgramas::model()->findByPk($id_programa);
								$model->id_status_programa = 1; //ALTA

								if($model->save())
								{
									Yii::app()->user->setFlash('success', 'Dia Agregado correctamente al Horario del Programa!!!');
									//Si se realizo correctamente el cambio
									$transaction->commit();
									//die("1");
									$this->redirect(array('listaHorarioProgramaSupervisor','id_programa'=>$id_programa));

								}else{

									Yii::app()->user->setFlash('danger', 'Error!!! no se pudo actualizar el Estatus del Programa.');
									//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
									$transaction->rollback();
									die("1");
								}

							}else{

								Yii::app()->user->setFlash('success', 'Dia Agregado correctamente al Horario del Programa!!!');
								//Si se realizo correctamente el cambio
								$transaction->commit();
								//die("1");
								$this->redirect(array('listaHorarioProgramaSupervisor','id_programa'=>$id_programa));
							}
							
						}else{

							Yii::app()->user->setFlash('danger', 'Error!!! no se pudo Agregar el dia al Horario del Programa.');
							//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
							$transaction->rollback();
							die("2");
						}

					}else{

						Yii::app()->user->setFlash('danger', 'Error!!! La Hora de Inicio no debe ser mayor a la Hora Fin.');
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
						die("3");
					}

				}else{

					Yii::app()->user->setFlash('danger', 'Error!!! Todos los campos deben ser completados.');
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					die("4");
				}

			}catch(Exception $e)
			{
				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al generar el Horario del Programa.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
			}

        }

        $this->render('nuevoDiaProgramaSupervisor',array(
            		  'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					  'lista_dias' => $lista_dias,
					  'id_programa' => $id_programa
        ));
	}

	public function actionListaHorarioProgramaSupervisor($id_programa)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarProgramaSupervisor($rfcSupervisor, $id_programa))
			throw new CHttpException(4044,'Este Programa No te corresponde.');

		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas_('search');
		$modelSSHorarioDiasHabilesProgramas->unsetAttributes();  // clear any default values

		if(isset($_GET['SsHorarioDiasHabilesProgramas']))
			$modelSSHorarioDiasHabilesProgramas->attributes=$_GET['SsHorarioDiasHabilesProgramas'];

		$this->render('listaHorarioProgramaSupervisor',array(
									'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
									'id_programa' => $id_programa
		));

	}

	public function actionEditarHorarioProgramaSupervisor($id_programa, $id_horario)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarProgramaSupervisor($rfcSupervisor, $id_programa))
			throw new CHttpException(4044,'Este Programa No te corresponde.');

		$modelSSHorarioDiasHabilesProgramas = $this->loadModel($id_horario);
		if($modelSSHorarioDiasHabilesProgramas === NULL)
			throw new CHttpException(404,'No existe programa con ese dia.');

		$lista_dias = $this->getDia($modelSSHorarioDiasHabilesProgramas->id_dia_semana);

      	if(isset($_POST['SsHorarioDiasHabilesProgramas']))
      	{
      		$modelSSHorarioDiasHabilesProgramas->attributes = $_POST['SsHorarioDiasHabilesProgramas'];
        	if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!!!');
				$this->redirect(array('listaHorarioProgramaSupervisor','id_programa'=>$modelSSHorarioDiasHabilesProgramas->id_programa));
			}else{
					
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los cambios.');
			}
      }

      $this->render('editarHorarioProgramaSupervisor',array(
             		'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					'lista_dias' => $lista_dias,
					'id_programa' => $id_programa
      ));
	}

	public function getDia($id_dia_semana)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_dia_semana = '$id_dia_semana' ";
		$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);

		$lista_dias_semana = CHtml::listData($modelSSDiasSemana, "id_dia_semana", "dia_semana");

		return $lista_dias_semana;
	}

	public function getDias($id_programa)
	{
		$query =
		"
			SELECT id_dia_semana from pe_planeacion.ss_horario_dias_habiles_programas
			where id_programa = '$id_programa'
		";

		$dias_elegidos = Yii::app()->db->createCommand($query)->queryAll();

		$dias = array(1, 2, 3, 4, 5, 6, 7);
		$long = sizeof($dias);//7
		$arrays = array();
		$arrays2 = array();

		/*REINDEXAMOS VALORES DEL ARREGLO */
		for($j = 0; $j < sizeof($dias_elegidos); $j++)
		{
			$dia = $dias_elegidos[$j]['id_dia_semana'];//ji
			//echo $dia;
			$arrays2[] = $dia;
		}

		/*VERIFICAMOS CUALES DIAS YA HAN SIDO ELEGIDOS Y LOS QUITAMOS PARA QUE NO ELIJAN EL HORARIO
		DEL MISMO DIA DOS VECES EN EL PROGRAMA*/
		for($i = 0 ; $i < $long ; $i++)
		{
			if(in_array($dias[$i],$arrays2) === false)
			{
				$arrays[] = $dias[$i];
			}
		}

		/*SE ELIJEN LOS DIAS A MOSTRAR EN DROPDOWNLIST DEPENDIENDO DE QUE EL DIA AUN NO HAYA SIDO REGISTRADO, SI
		SE REGISTRO SIMPLEMENTE SE QUITA EL DIA DEL DROPDOWNLIST PARA YA NO MOSTRAR DIAS ELEGIDOS */
		$criteria = new CDbCriteria();
		switch(sizeof($arrays))
		{
			case 1:
				$criteria->condition = "id_dia_semana in ($arrays[0])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 2:
				$criteria->condition = "id_dia_semana in ($arrays[0],$arrays[1])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 3:
				$criteria->condition = "id_dia_semana in ($arrays[0],$arrays[1], $arrays[2])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 4:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 5:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 6:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4], $arrays[5])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 7:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4], $arrays[5], $arrays[6])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;

		}

		$lista_dias_semana = CHtml::listData($modelSSDiasSemana, "id_dia_semana", "dia_semana");

		return $lista_dias_semana;
	}

	public function actionListaHorariosProgramas()
	{
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas_('searchNoRepetidosXPrograma');
		$modelSSHorarioDiasHabilesProgramas->unsetAttributes();  // clear any default values

		if(isset($_GET['SsHorarioDiasHabilesProgramas']))
		{
			$modelSSHorarioDiasHabilesProgramas->attributes=$_GET['SsHorarioDiasHabilesProgramas'];
		}

		$this->render('listaHorariosProgramas',array(
					  'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
		));
	}


	public function actionDetalleHorarioPrograma($id_programa)
	{

		$modelSSProgramas = SsProgramas::model()->findByPK($id_programa);
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findAllByAttributes(
																	array('id_programa'=>$id_programa)
															);

		/*INFORMACION DEL PROGRAMA*/
		$rfcSupervisor = $this->getRFCSupervisor($modelSSProgramas->id_programa, $modelSSProgramas->id_tipo_programa);
		$empresa = $this->getNameEmpresa($modelSSProgramas->id_unidad_receptora);
		$departamento = $this->getDepartamentoPrograma($modelSSProgramas->id_programa, $rfcSupervisor);
		$supervisor = $this->getNameSupervisor($modelSSProgramas->id_programa, $rfcSupervisor);
		$clasif_area = $this->getClasificacionAreaPrograma($modelSSProgramas->id_clasificacion_area_servicio_social);
		/*INFORMACION DEL PROGRAMA*/

		$this->render('detalleHorarioPrograma',array(
					'modelSSProgramas'=>$modelSSProgramas,
					'empresa' => $empresa,
					'departamento' => $departamento,
					'supervisor' => $supervisor,
					'clasif_area' => $clasif_area,
					'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas
		));
	}


	/*public function actionNuevoHorariosProgramas()
	{
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;

		$lista_dias = $this->getDiasSemana();

		if(isset($_POST['SsHorarioDiasHabilesProgramas']))
		{
			$modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];
			$modelSSHorarioDiasHabilesProgramas->id_programa = 1;
			$modelSSHorarioDiasHabilesProgramas->horas_totales = 0;
			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaHorariosProgramas'));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

		}

		$this->render('nuevoHorariosProgramas',array(
					'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					'lista_dias' => $lista_dias
		));
	}*/


	public function actionEditarHorariosProgramas($id_programa)
	{
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas_('searchEditarHorarioPrograma');
		$modelSSHorarioDiasHabilesProgramas->unsetAttributes();  // clear any default values

		if(isset($_POST['SsHorarioDiasHabilesProgramas']))
		{
			$modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];

			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaHorariosProgramas'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
		}

		$this->render('editarHorariosProgramas',array(
					'id_programa'=>$id_programa,
					'modelSSHorarioDiasHabilesProgramas' => $modelSSHorarioDiasHabilesProgramas
		));
	}

	public function actionEditarDiaHorarioPrograma($id_horario, $id_programa)
	{
		$modelSSHorarioDiasHabilesProgramas = $this->loadModel($id_horario);
		/*Dias semana */
		$lista_dias_semana = $this->getDiasSemana();

		if(isset($_POST['SsHorarioDiasHabilesProgramas']))
        {
            $modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];
			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('editarHorariosProgramas','id_programa'=>$id_programa));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
        }

        $this->render('editarDiaHorarioPrograma',array(
					'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					'lista_dias_semana' => $lista_dias_semana,
					'id_programa' => $id_programa
        ));
	}

	public function actionEliminarDiaHorarioPrograma($id_horario, $id_programa)
	{
		$modelSSHorarioDiasHabilesProgramas = SsHorarioDiasHabilesProgramas::model()->findByAttributes(
			array('id_horario'=>$id_horario,
				  'id_programa'=>$id_programa
			)
		);

		if($modelSSHorarioDiasHabilesProgramas != NULL)
		{
			if($modelSSHorarioDiasHabilesProgramas->delete())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('editarHorariosProgramas','id_programa'=>$id_programa));
			}
		}else{
			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
		}
	}

	public function actionNuevoDiaHorarioPrograma($id_programa)
	{
		//echo 'id:'.$id_programa;
		$modelSSHorarioDiasHabilesProgramas = new SsHorarioDiasHabilesProgramas;

		/*Dias semana, deben aparecer solo los dias que no han sido elegidos*/
		$lista_dias_semana = $this->getDiasSemanaRestantes($id_programa);

        if(isset($_POST['SsHorarioDiasHabilesProgramas']))
        {
			$modelSSHorarioDiasHabilesProgramas->attributes=$_POST['SsHorarioDiasHabilesProgramas'];
			$modelSSHorarioDiasHabilesProgramas->id_programa = $id_programa;
			//$modelSSHorarioDiasHabilesProgramas->horas_totales = 0;

			if($modelSSHorarioDiasHabilesProgramas->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('editarHorariosProgramas','id_programa'=>$id_programa));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}
        }

        $this->render('nuevoDiaHorarioPrograma',array(
					  'modelSSHorarioDiasHabilesProgramas'=>$modelSSHorarioDiasHabilesProgramas,
					  'lista_dias_semana' => $lista_dias_semana,
					  'id_programa' => $id_programa
        ));
	}

	public function loadModel($id_horario)
	{
		$modelSSHorarioDiasHabilesProgramas=SsHorarioDiasHabilesProgramas::model()->findByPk($id_horario);

		if($modelSSHorarioDiasHabilesProgramas===null)
		{
			throw new CHttpException(404,'No existen datos de ese registro.');
		}

		return $modelSSHorarioDiasHabilesProgramas;
	}

	
	/*******************************GETTERS AND SETTERS*******************************/

	/*Comprueba que el programa pertenece al supervisor logeado en el sistema */
	public function validarProgramaSupervisor($rfcSupervisor, $id_programa)
	{
		$bandera = false;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join public.\"H_empleados\" hemp
				on hemp.\"rfcEmpleado\" = rpi.\"rfcEmpleado\"
				where pss.id_programa = '$id_programa' and (pss.id_status_programa = 1 OR pss.id_status_programa = 8)
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and rpi.superv_principal_int = true";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_supervisores_programas sp
				on sp.\"rfcSupervisor\" = rpe.\"rfcSupervisor\"
				where pss.id_programa = '$id_programa' and (pss.id_status_programa = 1 OR pss.id_status_programa = 8)
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and rpe.superv_principal_ext = true";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}

	/*SOLO SE USA PARA CUANDO SE EDITA EL DIA HABIL DEL HORARIO DEL PROGRAMA*/
	public function getDiasSemana()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_dia_semana > 0 ';
		$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);

		$lista_dias_semana = CHtml::listData($modelSSDiasSemana, "id_dia_semana", "dia_semana");

		return $lista_dias_semana;
	}

	public function getNameEmpresa($id_unidad_receptora)
	{
		$query = "select * from pe_planeacion.ss_unidades_receptoras where id_unidad_receptora = '$id_unidad_receptora' ";

		$empresa = Yii::app()->db->createCommand($query)->queryAll();

		return $empresa[0]['nombre_unidad_receptora'];

	}

	public function getRFCSupervisor($id_programa, $id_tipo_programa)
	{
		$rfc = "";

		if($id_tipo_programa == 1)
		{
			$query = "select * from pe_planeacion.ss_responsable_programa_interno where id_programa = '$id_programa' AND superv_principal_int = true";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcEmpleado'];

		}else
		if($id_tipo_programa == 2){
			$query = "select * from pe_planeacion.ss_responsable_programa_externo where id_programa = '$id_programa' AND superv_principal_ext = true";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$rfc = $result[0]['rfcSupervisor'];
		}

		return $rfc;
	}

	public function getNameSupervisor($id_programa, $rfcSupervisor)
	{
		$modelSSProgramas = SsProgramas::model()->findByPK($id_programa);
		$nombre = "";

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$query = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre = $result[0]['nmbCompletoEmp'];

		}else
		if($modelSSProgramas->id_tipo_programa == 2){
			$query = "select (nombre_supervisor || ' ' || apell_paterno || ' ' || apell_materno) as name
			from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$nombre = $result[0]['name'];
		}

		return $nombre;
	}

	public function getDepartamentoPrograma($id_programa, $rfcSupervisor)
	{
		$modelSSProgramas = SsProgramas::model()->findByPK($id_programa);
		$departamento = "";

		if($modelSSProgramas->id_tipo_programa == 1)
		{
			$query = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcSupervisor' ";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$departamento = $result[0]['dscDepartamento'];
		}else
		if($modelSSProgramas->id_tipo_programa == 2){

			$query = "select * from pe_planeacion.ss_supervisores_programas where \"rfcSupervisor\" = '$rfcSupervisor' ";
			$result = Yii::app()->db->createCommand($query)->queryAll();
			$departamento = $result[0]['dscDepartamento'];
		}

		return $departamento;
	}

	public function getSupervisorPrograma($rfcSupervisor)
	{
		$query = "select (nombre_supervisor||' '||apell_paterno||' '||apell_materno) as name
		from ss_supervisores_programas
		where \"rfcSupervisor\" = '$rfcSupervisor'
		";

		$supervisor = Yii::app()->db->createCommand($query)->queryAll();

		return ($supervisor != NULL) ? $supervisor[0]['name'] : "NO ESPECIFICADO";
	}

	public function getClasificacionAreaPrograma($id_clasificacion_area_servicio_social)
	{
		$query = "select clasificacion_area_servicio_social
		from pe_planeacion.ss_clasificacion_area
		where id_clasificacion_area_servicio_social = '$id_clasificacion_area_servicio_social'
		";

		$clasificacion = Yii::app()->db->createCommand($query)->queryAll();

		return ($clasificacion != NULL) ? $clasificacion[0]['clasificacion_area_servicio_social'] : "NO ESPECIFICADO";
	}

	public function getDiasSemanaRestantes($id_programa)
	{

		$query = "
		SELECT id_dia_semana from pe_planeacion.ss_horario_dias_habiles_programas
		where id_programa = '$id_programa'
		";

		$dias_elegidos = Yii::app()->db->createCommand($query)->queryAll();

		$dias = array(1, 2, 3, 4, 5, 6, 7);
		$long = sizeof($dias);//7
		$arrays = array();
		$arrays2 = array();

		/*REINDEXAMOS VALORES DEL ARREGLO */
		for($j = 0; $j < sizeof($dias_elegidos); $j++)
		{
			$dia = $dias_elegidos[$j]['id_dia_semana'];//ji
			//echo $dia;
			$arrays2[] = $dia;
		}

		/*VERIFICAMOS CUALES DIAS YA HAN SIDO ELEGIDOS Y LOS QUITAMOS PARA QUE NO ELIJAN EL HORARIO
		DEL MISMO DIA DOS VECES EN EL PROGRAMA*/
		for($i = 0 ; $i < $long ; $i++)
		{
			if(in_array($dias[$i],$arrays2) === false)
			{
				$arrays[] = $dias[$i];
			}
		}

		/*SE ELIJEN LOS DIAS A MOSTRAR EN DROPDOWNLIST DEPENDIENDO DE QUE EL DIA AUN NO HAYA SIDO REGISTRADO, SI
		SE REGISTRO SIMPLEMENTE SE QUITA EL DIA DEL DROPDOWNLIST PARA YA NO MOSTRAR DIAS ELEGIDOS */
		$criteria = new CDbCriteria();
		switch(sizeof($arrays))
		{
			case 1:
				$criteria->condition = "id_dia_semana in ($arrays[0])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 2:
				$criteria->condition = "id_dia_semana in ($arrays[0],$arrays[1])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 3:
				$criteria->condition = "id_dia_semana in ($arrays[0],$arrays[1], $arrays[2])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 4:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 5:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 6:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4], $arrays[5])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;
			case 7:
				$criteria->condition = "id_dia_semana in ($arrays[0], $arrays[1], $arrays[2], $arrays[3], $arrays[4], $arrays[5], $arrays[6])";
				$modelSSDiasSemana = SsDiasSemana::model()->findAll($criteria);
			break;

		}

		$lista_dias_semana = CHtml::listData($modelSSDiasSemana, "id_dia_semana", "dia_semana");

		return $lista_dias_semana;
	}
	/*******************************GETTERS AND SETTERS*******************************/

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='horario-dias-habiles-programas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
