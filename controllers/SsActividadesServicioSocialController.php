<?php

class SsActividadesServicioSocialController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaSupervisorActividadesServicioSocial',
								 'editarSupervisorActividadServicioSocial',
								 'imprimirPlanDeTrabajoServicioSocial', //Reporte
								 'imprimirSPlanDeTrabajoServicioSocial',
								 'imprimirPlanDeTrabajoHistoricoServicioSocial',
								 'detalleActividadesServicioSocial',
								 'asignarMismasActividadesAVariosAlumnos' //Agregar mismas actividades a varios alumnos
								 ),
					'roles' => array('vinculacion_supervisor_servicio_social'),
					//'users' => array('@')
				),
				array('allow',
					   'actions'=>array('listaAlumnoActividadesServicioSocial',
										'imprimirPlanDeTrabajoServicioSocial',
										'imprimirPlanDeTrabajoHistoricoServicioSocial'
									),
					'roles'=>array('alumno'),
					//'users' => array('@')
				   ),
				array('allow',
					   'actions'=>array('imprimirPlanDeTrabajoServicioSocial',
										'imprimirSPlanDeTrabajoServicioSocial', //Supervisor
										'imprimirAPlanDeTrabajoServicioSocial', //Admin
										'listaAdminActividadesServicioSocial',
										'imprimirPlanDeTrabajoHistoricoServicioSocial',
										'editarAdminActividadServicioSocial'
									),
					'roles'=>array('vinculacion_oficina_servicio_social'),
					//'users' => array('@')
           		),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
		);
	}

	public function actionListaAdminActividadesServicioSocial($id_servicio_social)
	{
		$modelSSActividadesServicioSocial = new SsActividadesServicioSocial_('searchXActividadesAlumno');
        $modelSSActividadesServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsActividadesServicioSocial']))
		{
			$modelSSActividadesServicioSocial->attributes=$_GET['SsActividadesServicioSocial'];
		}

		$this->render('listaAdminActividadesServicioSocial',array(
					'modelSSActividadesServicioSocial'=>$modelSSActividadesServicioSocial,
					'id_servicio_social' => $id_servicio_social,
		));
	}

	public function actionListaSupervisorActividadesServicioSocial($id_servicio_social)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarActividadesAlumnoServicioSocial($rfcSupervisor, $id_servicio_social))
			throw new CHttpException(4044,'Estas actividades No te corresponden.');

		$modelSSActividadesServicioSocial = new SsActividadesServicioSocial_('search');
		$modelSSActividadesServicioSocial->unsetAttributes();  // clear any default values

		if(isset($_GET['SsActividadesServicioSocial']))
		{
			$modelSSActividadesServicioSocial->attributes=$_GET['SsActividadesServicioSocial'];
		}

		$criteria=new CDbCriteria;
		$criteria->condition = "id_servicio_social = '$id_servicio_social' ";
		$criteria->order = "id_actividad_servicio_social ASC";

		$this->render('listaSupervisorActividadesServicioSocial',array(
			'modelSSActividadesServicioSocial'=>$modelSSActividadesServicioSocial,
			'criteria' => $criteria,
			'id_servicio_social' => $id_servicio_social
		));
	}

	public function actionListaAlumnoActividadesServicioSocial()
	{

		$modelSSActividadesServicioSocial=new SsActividadesServicioSocial_('searchXActividadesAlumno');
		$modelSSActividadesServicioSocial->unsetAttributes();  // clear any default values
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;
		$ver_reporte;

		$criteria=new CDbCriteria;
		$criteria->condition = "no_ctrl = '$no_ctrl' AND id_estado_servicio_social < 6";
		$SSServicioSocial = SsServicioSocial::model()->find($criteria);

		if($SSServicioSocial === null)
		{
			//throw new CHttpException(404,'No existen datos del alumno con ese no. de Control.');
			$SSServicioSocial = new SsServicioSocial;
			$modelSSProgramas = new SsProgramas;
			$SSServicioSocial->id_servicio_social = 0;
			//$modelSSProgramas->id_tipo_programa = 0;
		}else{
			$modelSSProgramas = SsProgramas::model()->findByPk($SSServicioSocial->id_programa);
		}

		if(isset($_GET['SsActividadesServicioSocial']))
		{
			$modelSSActividadesServicioSocial->attributes=$_GET['SsActividadesServicioSocial'];
		}

		$id_serv = $SSServicioSocial->id_servicio_social;
		$ver_reporte = $this->puedeVerReporte($no_ctrl);

		$this->render('listaAlumnoActividadesServicioSocial',array(
					  'modelSSActividadesServicioSocial'=>$modelSSActividadesServicioSocial,
					  'id_servicio_social' => $id_serv,
					  'ver_reporte' => $ver_reporte,
					  'no_ctrl' => $no_ctrl
		));


	}

	public function actionEditarAdminActividadServicioSocial($id_actividad_servicio_social, $id_servicio_social)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetMeses.php';
		$modelSSActividadesServicioSocial=$this->loadModel($id_actividad_servicio_social);
		$mes = GetMeses::getMes($modelSSActividadesServicioSocial->id_mes);

		if(isset($_POST['SsActividadesServicioSocial']))
		{
			$modelSSActividadesServicioSocial->attributes=$_POST['SsActividadesServicioSocial'];
			if($modelSSActividadesServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('ssActividadesServicioSocial/listaAdminActividadesServicioSocial','id_servicio_social'=>$id_servicio_social));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
				//die('Error!!! no se guardaron los datos');
			}
		}

		$this->render('editarAdminActividadServicioSocial',array(
									'modelSSActividadesServicioSocial'=>$modelSSActividadesServicioSocial,
									'mes' => $mes
		));
	}

	public function actionEditarSupervisorActividadServicioSocial($id_actividad_servicio_social, $id_servicio_social)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarActividadesAlumno($rfcSupervisor, $id_servicio_social, $id_actividad_servicio_social))
			throw new CHttpException(4044,'Estas actividades No te corresponden.');

		$modelSSActividadesServicioSocial=$this->loadModel($id_actividad_servicio_social);

		if(isset($_POST['SsActividadesServicioSocial']))
		{
			$modelSSActividadesServicioSocial->attributes=$_POST['SsActividadesServicioSocial'];
			if($modelSSActividadesServicioSocial->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('ssActividadesServicioSocial/listaSupervisorActividadesServicioSocial','id_servicio_social'=>$id_servicio_social));
			}else{
				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
				die('Error!!! no se guardaron los datos');
			}
		}

		$this->render('editarSupervisorActividadServicioSocial',array(
					'modelSSActividadesServicioSocial'=>$modelSSActividadesServicioSocial,

		));
	}

	public function actionImprimirPlanDeTrabajoHistoricoServicioSocial($id_servicio_social)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Tomar del inicio de sesion del alumno en el SII
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		//if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			//throw new CHttpException(4044,'La Información No se encuentra disponible.');

		//Construimos la consulta (Datos del servicio social en el reporte actividades)
		$query = "
		select
		eda.\"nmbAlumno\" as namealumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		ss.no_ctrl,
		pss.nombre_programa,
		pss.descripcion_objetivo_programa,
		pss.fecha_inicio_programa
		from public.\"E_datosAlumno\" eda
		join pe_planeacion.ss_servicio_social ss
		on ss.no_ctrl = eda.\"nctrAlumno\"
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social'
		";

		//Ejecutamos la consulta, informacion del alumno
		$datos_alum_plan_trabajo = Yii::app()->db->createCommand($query)->queryAll();

		//Damos formato a la fecha de inicio
		$fecha_ini = $this->getFormatoFecha($datos_alum_plan_trabajo[0]['fecha_inicio_programa']);

		//Construimos la consulta (Informacion de las actividades del Servicio Social del Alumno)
		$query2 = "
		select
		ass.id_actividad_servicio_social ,ass.id_mes, ass.actividad_mensual
		from pe_planeacion.ss_actividades_servicio_social ass
		join pe_planeacion.ss_servicio_social ss
		on ss.id_servicio_social = ass.id_servicio_social
		where ass.id_servicio_social = '$id_servicio_social'
		order by ass.id_actividad_servicio_social ASC
		";

		$datos_actividades = Yii::app()->db->createCommand($query2)->queryAll();

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad = 2
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class='bold' style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Plan de Trabajo');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirPlanDeTrabajoServicioSocial', array(
						  'datos_alum_plan_trabajo' => $datos_alum_plan_trabajo,
						  'banners' => $banners,
						  'fecha_inicio' => $fecha_ini,
						  'datos_actividades' => $datos_actividades
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Plan Trabajo.pdf', 'I');
	}


	public function actionImprimirSPlanDeTrabajoServicioSocial($id_servicio_social)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		if(!$this->validarActividadesAlumnoServicioSocial($rfcSupervisor, $id_servicio_social))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		//Construimos la consulta (Datos del servicio social en el reporte actividades)
		$query = "
		select
		eda.\"nmbAlumno\" as namealumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		ss.no_ctrl,
		pss.nombre_programa,
		pss.descripcion_objetivo_programa,
		pss.fecha_inicio_programa,
		pss.id_unidad_receptora as no_empresa
		from public.\"E_datosAlumno\" eda
		join pe_planeacion.ss_servicio_social ss
		on ss.no_ctrl = eda.\"nctrAlumno\"
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social' ";

		//Ejecutamos la consulta, informacion del alumno
		$datos_alum_plan_trabajo = Yii::app()->db->createCommand($query)->queryAll();

		//Damos formato a la fecha de inicio
		$fecha_ini = $this->getFormatoFecha($datos_alum_plan_trabajo[0]['fecha_inicio_programa']);

		//Construimos la consulta (Informacion de las actividades del Servicio Social del Alumno)
		$query2 = "select ass.id_actividad_servicio_social ,ass.id_mes, ass.actividad_mensual
			from pe_planeacion.ss_actividades_servicio_social ass
			join pe_planeacion.ss_servicio_social ss
			on ss.id_servicio_social = ass.id_servicio_social
			where ass.id_servicio_social = '$id_servicio_social'
			order by ass.id_actividad_servicio_social ASC";

		$datos_actividades = Yii::app()->db->createCommand($query2)->queryAll();

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad = 2
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class='bold' style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Plan de Trabajo');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirPlanDeTrabajoServicioSocial', array(
						  'datos_alum_plan_trabajo' => $datos_alum_plan_trabajo,
						  'banners' => $banners,
						  'fecha_inicio' => $fecha_ini,
						  'datos_actividades' => $datos_actividades
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Plan Trabajo.pdf', 'I');
	}

	public function actionImprimirAPlanDeTrabajoServicioSocial($id_servicio_social)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Construimos la consulta (Datos del servicio social en el reporte actividades)
		$query = "
		select
		eda.\"nmbAlumno\" as namealumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		ss.no_ctrl,
		pss.nombre_programa,
		pss.descripcion_objetivo_programa,
		pss.fecha_inicio_programa
		from public.\"E_datosAlumno\" eda
		join pe_planeacion.ss_servicio_social ss
		on ss.no_ctrl = eda.\"nctrAlumno\"
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social'
		";

		//Ejecutamos la consulta, informacion del alumno
		$datos_alum_plan_trabajo = Yii::app()->db->createCommand($query)->queryAll();

		//Damos formato a la fecha de inicio
		$fecha_ini = $this->getFormatoFecha($datos_alum_plan_trabajo[0]['fecha_inicio_programa']);

		//Construimos la consulta (Informacion de las actividades del Servicio Social del Alumno)
		$query2 = "
		select
		ass.id_actividad_servicio_social ,ass.id_mes, ass.actividad_mensual
		from pe_planeacion.ss_actividades_servicio_social ass
		join pe_planeacion.ss_servicio_social ss
		on ss.id_servicio_social = ass.id_servicio_social
		where ass.id_servicio_social = '$id_servicio_social'
		order by ass.id_actividad_servicio_social ASC
		";

		$datos_actividades = Yii::app()->db->createCommand($query2)->queryAll();

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad = 2
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class='bold' style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Plan de Trabajo');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirPlanDeTrabajoServicioSocial', array(
						  'datos_alum_plan_trabajo' => $datos_alum_plan_trabajo,
						  'banners' => $banners,
						  'fecha_inicio' => $fecha_ini,
						  'datos_actividades' => $datos_actividades
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Plan Trabajo.pdf', 'I');

	}

	public function actionImprimirPlanDeTrabajoServicioSocial($id_servicio_social)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Tomar del inicio de sesion del alumno en el SII
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		if(!$this->validarServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		//Construimos la consulta (Datos del servicio social en el reporte actividades)
		$query = "
		select
		eda.\"nmbAlumno\" as namealumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		ss.no_ctrl,
		pss.id_unidad_receptora as no_empresa,
		pss.nombre_programa,
		pss.descripcion_objetivo_programa,
		pss.fecha_inicio_programa
		from public.\"E_datosAlumno\" eda
		join pe_planeacion.ss_servicio_social ss
		on ss.no_ctrl = eda.\"nctrAlumno\"
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social'
		";

		//Ejecutamos la consulta, informacion del alumno
		$datos_alum_plan_trabajo = Yii::app()->db->createCommand($query)->queryAll();

		//Damos formato a la fecha de inicio
		$fecha_ini = $this->getFormatoFecha($datos_alum_plan_trabajo[0]['fecha_inicio_programa']);

		//Construimos la consulta (Informacion de las actividades del Servicio Social del Alumno)
		$query2 = "
		select
		ass.id_actividad_servicio_social ,ass.id_mes, ass.actividad_mensual
		from pe_planeacion.ss_actividades_servicio_social ass
		join pe_planeacion.ss_servicio_social ss
		on ss.id_servicio_social = ass.id_servicio_social
		where ass.id_servicio_social = '$id_servicio_social'
		order by ass.id_actividad_servicio_social ASC
		";

		$datos_actividades = Yii::app()->db->createCommand($query2)->queryAll();

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad = 2
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class='bold' style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Plan de Trabajo');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirPlanDeTrabajoServicioSocial', array(
						  'datos_alum_plan_trabajo' => $datos_alum_plan_trabajo,
						  'banners' => $banners,
						  'fecha_inicio' => $fecha_ini,
						  'datos_actividades' => $datos_actividades
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Plan Trabajo.pdf', 'I');
	}

	//La institucion local debe ser la primera empresa en registrar, debe tener el id 1
	public function getDatosBanners()
	{
		$modelEmpresas = SsUnidadesReceptoras::model()->findByPk(1);

		if($modelEmpresas === NULL)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelEmpresas;
	}

	/*Damos formato a la fecha*/
	public function getFormatoFecha($fecha)
	{
		$fechats = strtotime($fecha); //pasamos a timestamp

		//Obtenemos el dia, mes y año
		$dia = date('d', $fechats);
		$mes = $this->getMes(date('m', $fechats));
		$anio = date('Y', $fechats);

		return $dia." de ".$mes." de ".$anio;
	}
	/*Damos formato a la fecha*/

	public function getMes($mes)
	{
		$_mes="";

		switch ($mes)
		{
			case 1: $_mes = "Enero"; break;
			case 2: $_mes = "Febrero"; break;
			case 3: $_mes = "Marzo"; break;
			case 4: $_mes = "Abril"; break;
			case 5: $_mes = "Mayo"; break;
			case 6: $_mes = "Junio"; break;
			case 7: $_mes = "Julio"; break;
			case 8: $_mes = "Agosto"; break;
			case 9: $_mes = "Septiembre"; break;
			case 10: $_mes = "Octubre"; break;
			case 11: $_mes = "Noviembre"; break;
			case 12: $_mes = "Diciembre"; break;
			default: $_mes = "Desconocido"; break;
		}

		return $_mes;
	}

	public function actionAsignarMismasActividadesAVariosAlumnos()
	{

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionCreate()
	{
		$model = new SsActividadesServicioSocial;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsActividadesServicioSocial']))
		{
			$model->attributes=$_POST['SsActividadesServicioSocial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_actividad_servicio_social));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SsActividadesServicioSocial');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$model=SsActividadesServicioSocial::model()->findByPk($id);

		if($model===null)
		{
			throw new CHttpException(404,'No existen datos del No. de Control con esa actividad.');
		}

		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='actividades-servicio-social-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function puedeVerReporte($no_ctrl)
	{
		$bandera;

		$criteria = new CDbCriteria;

		if($no_ctrl != null)
		{
			$criteria->condition = "no_ctrl = '$no_ctrl' AND (id_estado_servicio_social < 6 AND id_estado_servicio_social !=7 ) ";
			$SSServicioSocial = SsServicioSocial::model()->find($criteria);
			$bandera = ($SSServicioSocial === null) ? false : true;
		}else{
			$bandera = false;
		}

		return $bandera;
	}

	//Validar que el servicio social pertenezca al usuario actualmente logeado en el SII
	public function validarServicioSocial($id_servicio_social, $no_ctrl)
	{
		/*$model = SsServicioSocial::model()->findByAttributes(
			array('id_servicio_social'=>$id_servicio_social,
				  'no_ctrl'=>$no_ctrl
			)
		);*/

		$query = "select * from pe_planeacion.ss_servicio_social
		where id_servicio_social = '$id_servicio_social' and no_ctrl = '$no_ctrl'
		and (id_estado_servicio_social != 6 or id_estado_servicio_social != 7)";

		$model = Yii::app()->db->createCommand($query)->queryAll();

		return ($model != NULL) ? true : false;
	}

	public function validarHistoricoServicioSocial($id_servicio_social, $no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_servicio_social
				where ss.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$no_ctrl'
				and id_estado_servicio_social < 6";

		$model = Yii::app()->db->createCommand($query)->queryAll();

		return ($model !=  NULL ) ? true : false;
	}

	public function validarActividadesAlumnoServicioSocial($rfcSupervisor, $id_servicio_social)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query="select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_actividades_servicio_social ass
				on ass.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and ass.id_servicio_social = '$id_servicio_social' ";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_actividades_servicio_social ass
				on ass.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and ass.id_servicio_social = '$id_servicio_social' ";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;

	}

	public function validarActividadesAlumno($rfcSupervisor, $id_servicio_social, $id_actividad_servicio_social)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query="select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_actividades_servicio_social ass
				on ass.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and ass.id_servicio_social = '$id_servicio_social' and
				ass.id_actividad_servicio_social = '$id_actividad_servicio_social' ";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_actividades_servicio_social ass
				on ass.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and ass.id_servicio_social = '$id_servicio_social' and
				ass.id_actividad_servicio_social = '$id_actividad_servicio_social'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;
	}
}
