<?php

class SsReportesBimestralController extends Controller
{
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array
		(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'listaAdminReportesBimestrales', //vista y evaluación de reportes bimestrales de Servicio Social Internos por el depto.
								 'listaAdminReportesBimestralesExterno', //Lista de Reportes Bimestrales Servicio Social Externo
								 'imprimirReporteBimestral', //se muestra el reporte bimestral en PDF
								 'imprimirAReporteBimestral',
								 'validarDeptoEvaluacionReporteBimestral', //validar depto. evaluacion reporte bimestral
								 'nuevaAdminObservacionReporteBimestral', //Observaciones a los repotes bimestrales de servicio social interno (Admin)
								 'repBimHistoricoAdminServicioSocial', // Vista Admin Historial de reportes bimestrales de los servicios sociales terminados
								 'validarDeptoEvaluacionReporteBimestralExterno', //Valida reporte bimestral externo, despues de asignar calificacion
								 'evaluacionSupervisorExtReporteBimestral', //Evaluar los reportes bimestrales de servicio social externo
								 'editarCalificacionesCriterios', //Editar Reportes Bimestrales
								 'repBimHistoricoEscolaresAlumnoServicioSocial',
								 'listaReportesBimestralesValidadosTodos', //Reportes Bimestrales validados por el jefe de oficina
								 'listaReportesBimestralesTodos', //Reportes Bimestrales Todos
								 'editReporteBimestral',
								 'delReporteBimestralServicioSocial'
								 ),
				'roles'=>array('vinculacion_oficina_servicio_social'),
				//'users' => array('@')
				),
			array('allow',
			   'actions'=>array('listaSupervisorReportesBimestrales',
								'imprimirSReporteBimestral',
								'validarSupervisorEvaluacionReporteBimestral', //validar supervisor evaluacion reporte bimestral
								'listaReportesBimestralesValidadosSupervisor' //Lista de reportes Bimestrales validados por el Supervisor
							),
			   'roles'=>array('vinculacion_supervisor_servicio_social'),
			   //'users' => array('@'),
			),
			array('allow',
				'actions'=>array('listaAlumnoReportesBimestrales',
								 'imprimirReporteBimestral',
								 'envioAlumnoReporteBimestral',
								 'repBimHistoricoAlumnoServicioSocial'
								),
				'roles'=>array('alumno'),
				//'users' => array('@')
			),
			array('allow',
               'actions' => array('repBimHistoricoEscolaresAlumnoServicioSocial'),
			   'roles'=>array('jefe_departamento_escolares'),
			   //'users' => array('@')
			),
		);
	}

	public function actionListaReportesBimestralesTodos()
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('search');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values
		
        if(isset($_GET['SsReportesBimestral']))
            $modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];

        $this->render('listaReportesBimestralesTodos',array(
            		  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
        ));
	}

	//Eliminar Reportes Bimestrales (Repetidos)
	public function actionDelReporteBimestralServicioSocial($id_reporte_bimestral)
	{

		//die;
		$modelSSReportesBimestral = $this->loadModel($id_reporte_bimestral);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			$_id_reporte_bimestral = $modelSSReportesBimestral->id_reporte_bimestral;

			//$this->eliminaDatos($_id_reporte_bimestral);

			if($modelSSReportesBimestral->delete())
			{
				
				Yii::app()->user->setFlash('success', "Reporte Bimestral Eliminado correctamente!!!");
				//Si se realizo correctamente el cambio
				$transaction->commit();
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Eliminar el Reporte Bimestral.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Eliminar el Reporte Bimestral.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
		}
	}

	public function eliminaDatos($_id_reporte_bimestral)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " id_reporteBimestral = '$_id_reporte_bimestral' ";
		$modelSsEvaluacionBimestral = SsEvaluacionBimestral::model()->find($criteria);

		print_r($modelSsEvaluacionBimestral);
		die;

		if(!$modelSsEvaluacionBimestral->delete())
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Eliminar el Reporte Bimestral.");
		}

	}

	public function actionEditReporteBimestral($id_reporte_bimestral)
	{
		
		$modelSSReportesBimestral = $this->loadModel($id_reporte_bimestral);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['SsReportesBimestral']))
        {
			try
			{

				$modelSSReportesBimestral->attributes = $_POST['SsReportesBimestral'];
			
				if($modelSSReportesBimestral->save())
				{
					//die;
					Yii::app()->user->setFlash('success', "Reporte Bimestral Eliminado correctamente!!!");
					$transaction->commit();
					$this->redirect(array('listaReportesBimestralesTodos'));

				}else{
					//die;
					$transaction->rollback();
					Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Eliminar el Reporte Bimestral.");
				}

			}catch(Exception $e)
			{
				$transaction->rollback();
				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Eliminar el Reporte Bimestral.");
			}
        }

        $this->render('editReporteBimestral',array(
            		  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
        ));
	}

	//Reportes Bimestrales que hayan sido validados por el jefe de oficina de servicio social
	public function actionListaReportesBimestralesValidadosTodos($nocontrol = null, $anio = null, $periodo = null)
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('searchXReportesBimnestralesValidadosOficServicioSocial');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values
		
		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes = $_GET['SsReportesBimestral'];
			$modelSSReportesBimestral->nocontrol = $_GET['SsReportesBimestral']['nocontrol'];
			$modelSSReportesBimestral->anio = $_GET['SsReportesBimestral']['anio'];
			$modelSSReportesBimestral->periodo = $_GET['SsReportesBimestral']['periodo'];
			$nocontrol = $modelSSReportesBimestral->nocontrol;
			$anio = $modelSSReportesBimestral->anio;
			$periodo = $modelSSReportesBimestral->periodo;
		}
			
		$this->render('listaReportesBimestralesValidadosTodos',array(
					  'modelSSReportesBimestral' => $modelSSReportesBimestral,
					  'nocontrol' => $nocontrol,
					  'anio' => $anio,
					  'periodo' => $periodo
			  
		));
	}

	/*Evaluacion de Reportes Bimestrales de Servicio Social Externo*/
	public function actionEvaluacionSupervisorExtReporteBimestral()
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/RegularExpression.php';

		if(isset($_POST['id_evaluacion_bimestral']) && $_POST['evaluacion_b'])
		{
			$id_evaluacion_bimestral = $_POST['id_evaluacion_bimestral'];
			$modelSSEvaluacionBimestral = $this->loadModel($id_evaluacion_bimestral);

			if(RegularExpression::isEnteroPositivo($_POST['evaluacion_b']))
			{
				$modelSSEvaluacionBimestral->evaluacion_b = $_POST['evaluacion_b'];

				if($modelSSEvaluacionBimestral->save())
				{
					//Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
					/*Actualizamos calificacion final del reporte bimestral servicio social INTERNO */
					$inserto = Yii::app()->db->createCommand("select pe_planeacion.getCalificacionReporteBimestral('$modelSSEvaluacionBimestral->id_reporte_bimestral')")->queryAll();
					/*Actualizamos calificacion final del reporte bimestral servicio social INTERNO */
					if(!$inserto)
						die("Error!!!! Ocurrio un error al asignar calificación al criterio.");

					//$this->redirect(array('evaluacionSupervisorReporteBimestral','id_reporte_bimestral'=>$modelSSEvaluacionBimestral->id_reporte_bimestral));
				}else{	die("Error!!! no se actualizaron los datos."); }

			}else{ die("Error!!! La calificación debe ser un entero positivo."); }
		}

	}
	/*Evaluacion de Reportes Bimestrales de Servicio Social Externo*/

	//Todos los reportes que ha valdado el supervisor de servicio social
	public function actionListaReportesBimestralesValidadosSupervisor($nocontrol = null, $anio = null, $periodo = null)
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('searchReportesBimestralesAlumno');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		//Tomamos de su inicio de sesion del alumno
		//$rfc_supervisor = Yii::app()->params['rfcSupervisor'];
		$rfc_supervisor = Yii::app()->user->name;
		$rfcSupervisor = trim($rfc_supervisor);

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes = $_GET['SsReportesBimestral'];
			$modelSSReportesBimestral->nocontrol = $_GET['SsReportesBimestral']['nocontrol'];
			$modelSSReportesBimestral->anio = $_GET['SsReportesBimestral']['anio'];
			$modelSSReportesBimestral->periodo = $_GET['SsReportesBimestral']['periodo'];
			$anio = $modelSSReportesBimestral->anio;
			$periodo = $modelSSReportesBimestral->periodo;
			$nocontrol = $modelSSReportesBimestral->nocontrol;
		}


		$this->render('listaReportesBimestralesValidadosSupervisor',array(
					  'modelSSReportesBimestral' => $modelSSReportesBimestral,
					  'rfcSupervisor' => $rfcSupervisor,
					  'nocontrol' => $nocontrol,
					  'anio' => $anio,
					  'periodo' => $periodo
						
		));
	}

	//Vista Modal del Servicio Social
	public function actionShowDetServSocialValidado($id_reporte_bimestral, $id_servicio_social)
	{
		$this->layout='//layouts/mainVacio';

		//detalle del Servicio Social del Alumno
		$modelSSReportesBimestral = $this->loadModel($id_reporte_bimestral);
		$modelSSServicioSocial = SsServicioSocial::model()->findbyPk(trim($id_servicio_social));
		if($modelSSServicioSocial === NULL)
			throw new CHttpException(404,'No existe Servicio Social con ese ID.');

		//info servicio social
		$name_programa = $this->getPrograma($modelSSServicioSocial->id_programa);
		$estado_servicio = $this->getEstadoServicio($modelSSServicioSocial->id_estado_servicio_social);

		$this->render('showDetServSocialValidado',array(
					  'modelSSReportesBimestral' => $modelSSReportesBimestral,
					  'modelSSServicioSocial' => $modelSSServicioSocial,
					  'name_programa' => $name_programa,
					  'estado_servicio' => $estado_servicio
			  
		));

	}

	public function getEstadoServicio($id_estado_servicio_social)
	{
		$model = SsEstadoServicioSocial::model()->findByPk($id_estado_servicio_social);
		if($model === NULL)
			throw new CHttpException(404,'No existe Estado de Servicio Social con ese ID.');

		return $model->estado;
	}

	public function getPrograma($id_programa)
	{
		$modelSSProgramas = SsProgramas::model()->findByPk($id_programa);
		if($modelSSProgramas === NULL)
			throw new CHttpException(404,'No existe Programa con ese ID.');

		return ($modelSSProgramas === NULL) ? "Desconocido" : $modelSSProgramas->nombre_programa; 
	}

	public function actionListaAlumnoReportesBimestrales()
	{
		$modelSSReportesBimestral=new SsReportesBimestral_('searchReportesBimestralesAlumno');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		//Tomamos de su inicio de sesion del alumno
		$no_ctrl = Yii::app()->params['no_ctrl'];
		//$no_ctrl = Yii::app()->user->name;

		//Nos traemos los datos del servicio social actual del alumno para ver si es INTERNO o EXTERNO
		//$programas = $this->getIdServicioSocial($no_ctrl);

		//Datos de los reportes bimestrales
		//$meses = $this->getMesesReportesBimestrales($no_ctrl);
		//$calReportesBimestrales = $this->getCalificaciones($no_ctrl);

		if(isset($_GET['SsReportesBimestral']))
			$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];


		$this->render('listaAlumnoReportesBimestrales',array(
					  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					  'no_ctrl' => $no_ctrl
						//'meses' => $meses
						//'calReportesBimestrales' => $calReportesBimestrales
		));

	}

	public function getMesesReportesBimestrales($no_ctrl)
	{
		$meses = array();
		$id_servicio_social = $this->getServicioSocialActualAlumno($no_ctrl);
		$modelReportesBimestrales = SsReportesBimestral::model()->findAll(array(
																																				'select'=>'*',
																																				'condition'=>"id_servicio_social = '$id_servicio_social' ",
																																				'order'=>'id_reporte_bimestral'
																																			));

		if($modelReportesBimestrales != NULL)
		{

			$tamanio = (int) sizeof($modelReportesBimestrales);

			for($j = 0; $j < $tamanio; $j++)
			{

				//$mes1 = date("n", strtotime($modelReportesBimestrales[$j]['fecha_inicio_rep_bim']));
				$mes2 = date("n", strtotime($modelReportesBimestrales[$j]['fecha_fin_rep_bim']));

				switch ($mes2)
				{
					case 1:
					$meses[] = 'Enero';
						break;
						case 2:
						$meses[] = 'Febrero';
							break;
							case 3:
							$meses[] = 'Marzo';
								break;
								case 4:
								$meses[] = 'Abril';
									break;
									case 5:
									$meses[] = 'Mayo';
										break;
										case 6:
										$meses[] = 'Junio';
											break;
											case 7:
											$meses[] = 'Julio';
												break;
												case 8:
												$meses[] = 'Agosto';
													break;
													case 9:
													$meses[] = 'Septiembre';
														break;
														case 10:
														$meses[] = 'Octubre';
															break;
															case 11:
															$meses[] = 'Noviembre';
																break;
																case 12:
																$meses[] = 'Diciembre';
																	break;
				}

			}//Fin de for
			return $meses;
		}else{
			return $meses;
		}

	}

	public function getServicioSocialActualAlumno($no_ctrl)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "no_ctrl = '$no_ctrl' AND (id_estado_servicio_social > 1 AND id_estado_servicio_social < 6)";
		$modelServicioSocialActualAlumno = SsServicioSocial::model()->find($criteria);

		return ($modelServicioSocialActualAlumno != NULL) ? $modelServicioSocialActualAlumno->id_servicio_social : 0;

	}

	public function getCalificaciones($no_ctrl)
	{
		return 0;
	}

	public function actionListaAdminReportesBimestrales($nocontrol=null)
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('searchReportesXServicioSocialInternoAdmin');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes = $_GET['SsReportesBimestral'];
			$modelSSReportesBimestral->nocontrol = $_GET['SsReportesBimestral']['nocontrol'];
			$nocontrol = $modelSSReportesBimestral->nocontrol;

		}

        $this->render('listaAdminReportesBimestrales',array(
					  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					  'nocontrol' => $nocontrol
        ));
	}

	//Lista de Reportes Bimestrales Externos
	public function actionListaAdminReportesBimestralesExterno($nocontrol = null)
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('searchReportesXServicioSocialExternoAdmin');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes = $_GET['SsReportesBimestral'];
			$modelSSReportesBimestral->nocontrol = $_GET['SsReportesBimestral']['nocontrol'];
			$nocontrol = $modelSSReportesBimestral->nocontrol;
			//print_r($modelSSReportesBimestral);
		}

		$this->render('listaAdminReportesBimestralesExterno',array(
					  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					  'nocontrol' => $nocontrol

		));
	}
	//Lista de Reportes Bimestrales Externos

	//El Jefe de Ofic. de Servicio Social valida el reporte bimestral como Supervisor Externo
	public function actionValidarDeptoEvaluacionReporteBimestralExterno($id_reporte_bimestral)
	{
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPk($id_reporte_bimestral);
		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'No existen datos de ese Reportes Bimestral.');

		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($modelSSReportesBimestral->id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'No existen datos de ese Servicio Social.');

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			//Obtenemos el Supervisor del Servicio Social del Reporte Bimestral que se esta evaluando
			$rfc = $this->getRFCSupervisorExterno($modelSSServicioSocial->id_servicio_social);

			if($modelSSReportesBimestral)
			{
				/*Se valida el reporte bimestral por la Oficina de Servicio Social*/
				$modelSSReportesBimestral->valida_responsable = date('Y-m-d H:i:s');
				$modelSSReportesBimestral->rfcJefeOfiServicioSocial = Yii::app()->user->name;
				$modelSSReportesBimestral->rfcResponsable = $rfc;

				if ($modelSSReportesBimestral->save())
				{
					/*Si pone estatus de "Entrega de Reporte" (2) */
					$modelSSServicioSocial->id_estado_servicio_social = 2; //Por simple inspeccion, ya que desde que lo envia el alumno a evaluacion cambia a estatus (2)
					//Guardamos los datos
					$modelSSServicioSocial->save();
					Yii::app()->user->setFlash('success', "Reporte Bimestral Validado correctamente!!!");
					//Si se realizo correctamente el cambio
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Validar el Reporte Bimestral.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
				}
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! No se pudo Validar el Reporte Bimestral.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
		}
	}
	//El Jefe de Ofic. de Servicio Social valida el reporte bimestral como Supervisor Externo

	public function getRFCSupervisorExterno($id_servicio_social)
	{
		$query = "select rpe.\"rfcSupervisor\" as rfc from pe_planeacion.ss_servicio_social ss
							join pe_planeacion.ss_programas pss
							on pss.id_programa = ss.id_programa
							join pe_planeacion.ss_responsable_programa_externo rpe
							on rpe.id_programa = pss.id_programa
							where pss.id_status_programa = 1 and ss.id_servicio_social = '$id_servicio_social' ";

		$rfc = Yii::app()->db->createCommand($query)->queryAll();

		return ($rfc != NULL) ? $rfc[0]['rfc'] : null;
	}

	public function getIdServicioSocial($no_ctrl)
	{
		$query = "
		select * from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.no_ctrl = '$no_ctrl' and (ss.id_estado_servicio_social > 1 AND
		ss.id_estado_servicio_social < 6)
		";

		$programas = Yii::app()->db->createCommand($query)->queryAll();

		return $programas;
	}

	public function actionListaSupervisorReportesBimestrales($nocontrol = null)
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('searchReportesXServicioSocialInterno');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		//RFC tomado del inicio se sesion del empleado
		//$rfc_supervisor = Yii::app()->params["rfcSupervisor"];
		$rfc_supervisor = Yii::app()->user->name;

		$id_tipo_programa = $this->getTipoSupervisor($rfc_supervisor);
		$id_programa = $this->getIDProgramaVigente($rfc_supervisor);

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];
			$modelSSReportesBimestral->nocontrol = $_GET['SsReportesBimestral']['nocontrol'];
			$nocontrol = $modelSSReportesBimestral->nocontrol;

		}

		$this->render('listaSupervisorReportesBimestrales',array(
					 'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					 'nocontrol' => $nocontrol,
					 'id_tipo_programa' => $id_tipo_programa,
					 'rfc_supervisor' => $rfc_supervisor,
					 'id_programa' => $id_programa

		));
	}

	//Obtenemos el ID del programa Vigente del usuario logeado en el SII
	public function getIDProgramaVigente($rfcSupervisor)
	{
		$id_programa = 0;
		
		$query = "select pss.id_programa from pe_planeacion.ss_responsable_programa_interno rpi
				join pe_planeacion.ss_programas pss
				on pss.id_programa = rpi.id_programa
				where pss.id_status_programa = 1 and rpi.\"rfcEmpleado\" = '$rfcSupervisor' ";

		$query2 = "select pss.id_programa from pe_planeacion.ss_responsable_programa_externo rpe
				join pe_planeacion.ss_programas pss
				on pss.id_programa = rpe.id_programa
				where pss.id_status_programa = 1 and rpe.\"rfcSupervisor\" = '$rfcSupervisor' ";

		$result = Yii::app()->db->createCommand($query)->queryAll();
		$result2 = Yii::app()->db->createCommand($query2)->queryAll();

		if(!empty($result)) //Interno
		{
			$id_programa = $result[0]['id_programa'];
		}else
		if(!empty($result2)) //Externo
		{
			$id_programa = $result[0]['id_programa'];
		}

		return $id_programa;
	}

	public function getTipoSupervisor($rfcSupervisor)
	{
		$tipo = 0;
		$query = "select * from pe_planeacion.ss_responsable_programa_interno where \"rfcEmpleado\" = '$rfcSupervisor' ";
		$query2 = "select * from pe_planeacion.ss_responsable_programa_externo where \"rfcSupervisor\" = '$rfcSupervisor' ";
		$result = Yii::app()->db->createCommand($query)->queryAll();
		$result2 = Yii::app()->db->createCommand($query2)->queryAll();

		if(!empty($result))
		{
			$tipo = 1;
		}else
		if(!empty($result2))
		{
			$tipo = 2;
		}

		return $tipo;
	}

	public function actionImprimirAReporteBimestral($id_reporte_bimestral, $id_servicio_social)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		//$rfcSupervisor = Yii::app()->user->name;

		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';

		//Construimos la consulta
		$query_reporte =
		"
		select
		eda.\"nmbAlumno\" as nombre_alumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera_alumno,
		ss.no_ctrl,
		pss.id_unidad_receptora as no_empresa,
		pss.nombre_programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		rb.bimestres_correspondiente,
		(case
		when (rb.bimestre_final = false) then ''
		when (rb.bimestre_final = true) then 'X'
		end) as bimestre_final,
		ss.\"rfcSupervisor\",
		ss.nombre_supervisor,
		ss.cargo_supervisor,
		rb.valida_responsable,
		ss.\"rfcJefeOficServicioSocial\",
		(select \"nmbCompletoEmp\" from public.h_ocupacionpuesto where \"rfcEmpleado\" = ss.\"rfcJefeOficServicioSocial\") as namejefeserviciosocial,
		rb.valida_oficina_servicio_social,
		rb.calificacion_reporte_bimestral,
		rb.observaciones_reporte_bimestral
		from pe_planeacion.ss_servicio_social ss
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_reportes_bimestral rb
		on rb.id_servicio_social = ss.id_servicio_social
		where rb.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$modelSSServicioSocial->no_ctrl' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
		";

		$datos_reporte_bimestral = Yii::app()->db->createCommand($query_reporte)->queryAll();

		//Obtenemos la fecha en que valido el reporte bimestral del alumno
		$fec_val_supervisor = GetFormatoFecha::getFechaValidaSupervisorReporteBimestral($id_reporte_bimestral, $id_servicio_social);

		//Obtenemos los criterios a evaluar por el supervisor del programa y que esten en status = true
		$query_crierios_sup =
		"
		select eb.id_criterio, ce.descripcion_criterio, ce.valor_a, eb.evaluacion_b from pe_planeacion.ss_criterios_a_evaluar as ce
		join pe_planeacion.ss_evaluacion_bimestral as eb
		on eb.id_criterio = ce.id_criterio
		where ce.id_tipo_criterio = 1 and ce.status_criterio = true and eb.id_reporte_bimestral = '$id_reporte_bimestral'
		order by ce.posicion_criterio
		";
		$lista_criterios_sup = Yii::app()->db->createCommand($query_crierios_sup)->queryAll();

		//Obtenemos los criterios a evaluar por gestion tecnologica y vinculacion y que esten en status = true
		$query_crierios_vinc =
		"
		select eb.id_criterio, ce.descripcion_criterio, ce.valor_a, eb.evaluacion_b from pe_planeacion.ss_criterios_a_evaluar as ce
		join pe_planeacion.ss_evaluacion_bimestral as eb
		on eb.id_criterio = ce.id_criterio
		where ce.id_tipo_criterio = 2 and ce.status_criterio = true and eb.id_reporte_bimestral = '$id_reporte_bimestral'
		order by ce.posicion_criterio ASC
		";

		//Sumatoria de los Valores A de los Criterios
		$totalB = $this->getValorATotalCriterios($id_servicio_social, $id_reporte_bimestral);

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Ejecutamos la consulta criterios para reporte bimestral
		$lista_criterios_vinc = Yii::app()->db->createCommand($query_crierios_vinc)->queryAll();
		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad=1
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Reporte Bimestral');
		# render (full page)
    	$mPDF1->WriteHTML(
		$this->render('imprimirReporteBimestral', array(
					  'lista_criterios_sup' => $lista_criterios_sup,
					  'lista_criterios_vinc' => $lista_criterios_vinc,
					  'banners' => $banners,
					  'datos_reporte_bimestral' => $datos_reporte_bimestral,
					  'fec_inicio' => $this->getFormatoFechaInicio($id_reporte_bimestral),
					  'fec_fin' => $this->getFormatoFechaFin($id_reporte_bimestral),
					  'fec_val_supervisor' => $fec_val_supervisor,
					  'totalB' => $totalB
					), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Reporte Bimestral.pdf', 'I');

	}

	public function actionImprimirSReporteBimestral($id_reporte_bimestral, $id_servicio_social)
	{
		//SE OBTIENE DE LA SESIÓN DEL EMPLEADO
		//$rfcSupervisor = Yii::app()->params['rfcSupervisor'];
		$rfcSupervisor = Yii::app()->user->name;

		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		if(!$this->validarSupervisorServicioSocial($id_servicio_social, $id_reporte_bimestral, $rfcSupervisor, $modelSSServicioSocial->no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';

		//Construimos la consulta
		$query_reporte =
		"
		select
		eda.\"nmbAlumno\" as nombre_alumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera_alumno,
		ss.no_ctrl,
		pss.id_unidad_receptora as no_empresa,
		pss.nombre_programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		rb.bimestres_correspondiente,
		(case
		when (rb.bimestre_final = false) then ''
		when (rb.bimestre_final = true) then 'X'
		end) as bimestre_final,
		ss.\"rfcSupervisor\",
		ss.nombre_supervisor,
		ss.cargo_supervisor,
		rb.valida_responsable,
		ss.\"rfcJefeOficServicioSocial\",
		(select \"nmbCompletoEmp\" from public.h_ocupacionpuesto where \"rfcEmpleado\" = ss.\"rfcJefeOficServicioSocial\") as namejefeserviciosocial,
		rb.valida_oficina_servicio_social,
		rb.calificacion_reporte_bimestral,
		rb.observaciones_reporte_bimestral
		from pe_planeacion.ss_servicio_social ss
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_reportes_bimestral rb
		on rb.id_servicio_social = ss.id_servicio_social
		where rb.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$modelSSServicioSocial->no_ctrl' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
		";

		$datos_reporte_bimestral = Yii::app()->db->createCommand($query_reporte)->queryAll();

		//Obtenemos la fecha en que valido el reporte bimestral del alumno
		$fec_val_supervisor = GetFormatoFecha::getFechaValidaSupervisorReporteBimestral($id_reporte_bimestral, $id_servicio_social);

		//Obtenemos los criterios a evaluar por el supervisor del programa y que esten en status = true
		$query_crierios_sup =
		"
		select eb.id_criterio, ce.descripcion_criterio, ce.valor_a, eb.evaluacion_b from pe_planeacion.ss_criterios_a_evaluar as ce
		join pe_planeacion.ss_evaluacion_bimestral as eb
		on eb.id_criterio = ce.id_criterio
		where ce.id_tipo_criterio = 1 and ce.status_criterio = true and eb.id_reporte_bimestral = '$id_reporte_bimestral'
		order by ce.posicion_criterio
		";
		$lista_criterios_sup = Yii::app()->db->createCommand($query_crierios_sup)->queryAll();

		//Obtenemos los criterios a evaluar por gestion tecnologica y vinculacion y que esten en status = true
		$query_crierios_vinc =
		"
		select eb.id_criterio, ce.descripcion_criterio, ce.valor_a, eb.evaluacion_b from pe_planeacion.ss_criterios_a_evaluar as ce
		join pe_planeacion.ss_evaluacion_bimestral as eb
		on eb.id_criterio = ce.id_criterio
		where ce.id_tipo_criterio = 2 and ce.status_criterio = true and eb.id_reporte_bimestral = '$id_reporte_bimestral'
		order by ce.posicion_criterio ASC
		";

		//Sumatoria de los Valores A de los Criterios
		$totalB = $this->getValorATotalCriterios($id_servicio_social, $id_reporte_bimestral);

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Ejecutamos la consulta criterios para reporte bimestral
		$lista_criterios_vinc = Yii::app()->db->createCommand($query_crierios_vinc)->queryAll();
		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', '', '', 15, 15, 5, 5, 5, 5,'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad=1
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Reporte Bimestral');
		# render (full page)
    	$mPDF1->WriteHTML(
		$this->render('imprimirReporteBimestral', array(
					  'lista_criterios_sup' => $lista_criterios_sup,
					  'lista_criterios_vinc' => $lista_criterios_vinc,
					  'banners' => $banners,
					  'datos_reporte_bimestral' => $datos_reporte_bimestral,
					  'fec_inicio' => $this->getFormatoFechaInicio($id_reporte_bimestral),
					  'fec_fin' => $this->getFormatoFechaFin($id_reporte_bimestral),
					  'fec_val_supervisor' => $fec_val_supervisor,
					  'totalB' => $totalB
					), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Reporte Bimestral.pdf', 'I');
	}

	public function actionImprimirReporteBimestral($id_reporte_bimestral, $id_servicio_social)
	{

		//Tomar del inicio de sesion del alumno en el SII
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		if(!$this->validarServicioSocial($id_servicio_social, $id_reporte_bimestral, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');
		
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';

		//Construimos la consulta
		$query_reporte =
		"
		select
		eda.\"nmbAlumno\" as nombre_alumno,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera_alumno,
		ss.no_ctrl,
		pss.id_unidad_receptora as no_empresa,
		pss.nombre_programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		rb.bimestres_correspondiente,
		(case
		when (rb.bimestre_final = false) then ''
		when (rb.bimestre_final = true) then 'X'
		end) as bimestre_final,
		ss.\"rfcSupervisor\",
		ss.nombre_supervisor,
		ss.cargo_supervisor,
		rb.valida_responsable,
		ss.\"rfcJefeOficServicioSocial\",
		(select \"nmbCompletoEmp\" from public.h_ocupacionpuesto where \"rfcEmpleado\" = ss.\"rfcJefeOficServicioSocial\") as namejefeserviciosocial,
		rb.valida_oficina_servicio_social,
		rb.calificacion_reporte_bimestral,
		rb.observaciones_reporte_bimestral
		from pe_planeacion.ss_servicio_social ss
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		join pe_planeacion.ss_reportes_bimestral rb
		on rb.id_servicio_social = ss.id_servicio_social
		where rb.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$no_ctrl' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
		";

		$datos_reporte_bimestral = Yii::app()->db->createCommand($query_reporte)->queryAll();

		//Obtenemos la fecha en que valido el reporte bimestral del alumno
		$fec_val_supervisor = GetFormatoFecha::getFechaValidaSupervisorReporteBimestral($id_reporte_bimestral, $id_servicio_social);

		//Obtenemos los criterios a evaluar por el supervisor del programa y que esten en status = true
		$query_crierios_sup =
		"
		select eb.id_criterio, ce.descripcion_criterio, ce.valor_a, eb.evaluacion_b from pe_planeacion.ss_criterios_a_evaluar as ce
		join pe_planeacion.ss_evaluacion_bimestral as eb
		on eb.id_criterio = ce.id_criterio
		where ce.id_tipo_criterio = 1 and ce.status_criterio = true and eb.id_reporte_bimestral = '$id_reporte_bimestral'
		order by ce.posicion_criterio
		";
		$lista_criterios_sup = Yii::app()->db->createCommand($query_crierios_sup)->queryAll();

		//Obtenemos los criterios a evaluar por gestion tecnologica y vinculacion y que esten en status = true
		$query_crierios_vinc =
		"
		select eb.id_criterio, ce.descripcion_criterio, ce.valor_a, eb.evaluacion_b from pe_planeacion.ss_criterios_a_evaluar as ce
		join pe_planeacion.ss_evaluacion_bimestral as eb
		on eb.id_criterio = ce.id_criterio
		where ce.id_tipo_criterio = 2 and ce.status_criterio = true and eb.id_reporte_bimestral = '$id_reporte_bimestral'
		order by ce.posicion_criterio ASC
		";

		//Sumatoria de los Valores A de los Criterios
		$totalB = $this->getValorATotalCriterios($id_servicio_social, $id_reporte_bimestral);

		//Datos de los banners
		$banners = $this->getDatosBanners();

		//Ejecutamos la consulta criterios para reporte bimestral
		$lista_criterios_vinc = Yii::app()->db->createCommand($query_crierios_vinc)->queryAll();
		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', '', '', 15, 15, 5, 5, 5, 5);
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//$mPDF1 = new \Mpdf\Mpdf(array('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L'));
		/*$mPDF1 = new \Mpdf\Mpdf(array(
						'margin_left'	=> '5',
						'margin_right'      =>  '5',
						'mode'              =>  'utf-8',     //Codepage Values OR Codepage Values
						'format'            =>  'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
						'orientation'       =>  'L'          //"L" for Landscape orientation, "P" for Portrait orientation
		));*/
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad=1
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table class=\"bold\" style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Reporte Bimestral');
		# render (full page)
    	$mPDF1->WriteHTML(
		$this->render('imprimirReporteBimestral', array(
					  'lista_criterios_sup' => $lista_criterios_sup,
					  'lista_criterios_vinc' => $lista_criterios_vinc,
					  'banners' => $banners,
					  'datos_reporte_bimestral' => $datos_reporte_bimestral,
					  'fec_inicio' => $this->getFormatoFechaInicio($id_reporte_bimestral),
					  'fec_fin' => $this->getFormatoFechaFin($id_reporte_bimestral),
					  'fec_val_supervisor' => $fec_val_supervisor,
					  'totalB' => $totalB
					), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Reporte Bimestral.pdf', 'I');

	}

	//La institucion local debe ser la primera empresa en registrar, debe tener el id 1
	public function getDatosBanners()
	{
		$modelEmpresas = SsUnidadesReceptoras::model()->findByPk(1);

		if($modelEmpresas === NULL)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelEmpresas;
	}

	/*Sumatoria del valor B asigando a cada criterio */
	public function getValorATotalCriterios($id_servicio_social, $id_reporte_bimestral)
	{
		$query = "select SUM(evaluacion_b) as \"totalB\" from pe_planeacion.ss_reportes_bimestral rb
		join pe_planeacion.ss_evaluacion_bimestral eb
		on eb.id_reporte_bimestral = rb.id_reporte_bimestral
		where rb.id_servicio_social = '$id_servicio_social' AND rb.id_reporte_bimestral = '$id_reporte_bimestral' ";

		$totalA = Yii::app()->db->createCommand($query)->queryAll();

		return $totalA[0]['totalB'];
	}
	/*Sumatoria del valor A asigando a cada criterio activado */

	public function actionImprimirReporteBimestralExterno($id_reporte_bimestral, $no_ctrl, $id_servicio_social)
	{
		//Construimos la consulta
		$query_reporte =
		"
		select (select (nombres||' '||apellido_paterno || ' ' || apellido_materno) from alumnos_vigentes_inscritos_creditos where no_ctrl = ss.no_ctrl) as namealumno,
		(select \"dscEspecialidad\" from alumnos_vigentes_inscritos_creditos where no_ctrl= ss.no_ctrl) as carrera,
		ss.no_ctrl,
		pss.id_unidad_receptora as no_empresa,
		pss.nombre_programa as programa,
		(select (nombre_supervisor || ' ' || apell_paterno||' '||apell_materno) from ss_supervisores_programas where \"rfcSupervisor\" = pss.\"rfcSupervisor\") as namesupervisor,
		(select cargo_supervisor from ss_supervisores_programas where \"rfcSupervisor\" = pss.\"rfcSupervisor\") as cargosupervisor,
		rb.bimestres_correspondiente,
		rb.observaciones_reporte_bimestral,
		rb.bimestre_final,
		rb.fecha_inicio_rep_bim,
		rb.fecha_fin_rep_bim
		from ss_servicio_social as ss
		join ss_reportes_bimestral as rb
		on rb.id_servicio_social = ss.id_servicio_social
		join ss_programas as pss
		on pss.id_programa = ss.id_programa
		where rb.id_servicio_social = '$id_servicio_social' and ss.no_ctrl = '$no_ctrl' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
		";

		$datos_reporte_bimestral = Yii::app()->db->createCommand($query_reporte)->queryAll();

		//Obtenemos los criterios a evaluar por el supervisor del programa y que esten en status = true
		$query_crierios_sup =
		"
		select id_criterio, descripcion_criterio, valor_a from ss_criterios_a_evaluar
                where id_tipo_criterio = 1 and status_criterio = true
                order by id_criterio
		";
		$lista_criterios_sup = Yii::app()->db->createCommand($query_crierios_sup)->queryAll();

		//Obtenemos los criterios a evaluar por gestion tecnologica y vinculacion y que esten en status = true
		$query_crierios_vinc =
		"
		select id_criterio, descripcion_criterio, valor_a from ss_criterios_a_evaluar
                where id_tipo_criterio = 2 and status_criterio = true
                order by id_criterio
		";
		//Ejecutamos la consulta criterios para reporte bimestral
		$lista_criterios_vinc = Yii::app()->db->createCommand($query_crierios_vinc)->queryAll();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		// mode
		// margin_left
		// margin right
		// margin top
		// margin bottom
		// margin header
		// margin footer
		// L - Landscape / P - Portrait
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4', 0, '', 15, 15, 7, 10, 0, 15, 'P');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/*ASIGNAR CODIGOS CALIDAD A REPORTE BIMESTRAL*/
		$query_cc = "
		Select codigo_calidad, revision
		from pe_planeacion.ss_codigos_calidad
		where id_codigo_calidad = 1
		";
		$codigos_calidad = Yii::app()->db->createCommand($query_cc)->queryAll();
		/*ASIGNAR CODIGOS CALIDAD QUE LE TOCAN*/
		/************************************Footer************************************/
		$c_calidad = $codigos_calidad[0]['codigo_calidad'];
		$revision = "Rev. ".$codigos_calidad[0]['revision'];
		$footer =
		"<table style='border-top: 7px solid #1B5E20; border-collapse: separate; border-spacing: 3px;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border: 0px solid #FFFFFF; font-size: 16px; padding-bottom: 5px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Reporte Bimestral');
		# render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirReporteBimestral', array(
						  'lista_criterios_sup' => $lista_criterios_sup,
						  'lista_criterios_vinc' => $lista_criterios_vinc,
						  'datos_reporte_bimestral' => $datos_reporte_bimestral,
						  'fec_inicio' => $this->getFormatoFechaInicio($id_reporte_bimestral),
						  'fec_fin' => $this->getFormatoFechaFin($id_reporte_bimestral),
						  'porte_tipo' => $porte_tipo = 2 //REPORTE BIMESTRAL SERVICIO SOCIAL EXTERNO
			), true)
		);
		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Reporte Bimestral.pdf', 'I');
	}

	public function getFormatoFechaInicio($id_reporte_bimestral)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoReportesBimestral.php';
		$fecha_ini = InfoReportesBimestral::getFechaInicioReporteBimestral($id_reporte_bimestral);

		return $fecha_ini[0]['fecha_ini'];
	}

	public function getFormatoFechaFin($id_reporte_bimestral)
	{
		require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/InfoReportesBimestral.php';
		$fecha_fin = InfoReportesBimestral::getFechaFinReporteBimestral($id_reporte_bimestral);

		return $fecha_fin[0]['fecha_fin'];
	}

	/*VALIDACIÓN DE ENVIO DE REPORTE BIMESTRAL A REVISIÓN (INTERNO)*/
	public function actionEnvioAlumnoReporteBimestral($id_reporte_bimestral, $oper=null)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			$modelSSReportesBimestral = SsReportesBimestral::model()->findByPk($id_reporte_bimestral);

			$modelSSServicioSocial = SsServicioSocial::model()->findByPk($modelSSReportesBimestral->id_servicio_social);

			if($modelSSServicioSocial === null)
				throw new CHttpException(404,'No existen datos de ese Servicio Social.');

			if ($modelSSReportesBimestral)
			{

				$modelSSReportesBimestral->envio_alum_evaluacion = date('Y-m-d H:i:s');

				if ($modelSSReportesBimestral->save())
				{
					/*Se genera el registro de la evaluacion del reporte bimestral en la tabla evaluacion_bimestral*/
					if(!$this->getValidarRegistroNoRepetedio($modelSSReportesBimestral))
					{
						$insert = Yii::app()->db->createCommand("select pe_planeacion.crearRegistroEvaluacionBimestral('$id_reporte_bimestral')")->queryAll();
						
						/*Agregar el cambio de estado de Servicio Social a 3 (Entrega Reporte)*/
						if($insert == true)
						{
							$this->cambioEstadoServicioSocial($modelSSReportesBimestral->id_servicio_social);
							/*Se genera el registro de la evaluacion del reporte bimestral en la tabla evaluacion_bimestral*/
							echo CJSON::encode( [ 'code' => 200 ] );
							$transaction->commit();
							Yii::app()->user->setFlash('success', 'Reporte Bimestral validado correctamente!!!');

						}else{

							echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
							$transaction->rollback();
							Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al validar el Reporte Bimestral.');
						}
					}
					
				}else{
					echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
					$transaction->rollback();
					Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al validar el Reporte Bimestral.');
				}
			}

		}catch(Exception $e)
		{
			echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
			$transaction->rollback();
			Yii::app()->user->setFlash('danger', 'Error!!! ocurrio un error al validar el Reporte Bimestral.');
		}

	}
	/*VALIDACION DE ENVIO DE REPORTE BIMESTRAL A REVISION (INTERNO)*/

	/*VALIDACION DE ENVIO DE REPORTE BIMESTRAL A REVISION (EXTERNO)*/
	public function actionEnvioAlumnoReporteBimestralExterno($id_reporte_bimestral, $oper=null)
	{
		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPk($id_reporte_bimestral);

		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($modelSSReportesBimestral->id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'No existen datos de ese Servicio Social.');

		if ($modelSSReportesBimestral)
		{
			//Valor default de false del campo envio_alum_evaluacion
			if($modelSSReportesBimestral->envio_alum_evaluacion == null)
			{
				$modelSSReportesBimestral->envio_alum_evaluacion = 0; //false
			}

			/*Una vez enviado el reporte a revision cambia estado a true, ya no se permite revocar esa validacion, siempre sera true*/
			$var = ($modelSSReportesBimestral->envio_alum_evaluacion == $oper) ? 1 : 1 ;
			$modelSSReportesBimestral->envio_alum_evaluacion  = $var; // 0 - No Enviado, 1 - Enviado
			$modelSSReportesBimestral->valida_responsable = date('Y-m-d H:i:s');
			//$modelSSReportesBimestral->valida_oficina_servicio_social = date('Y-m-d H:i:s');

			if ($modelSSReportesBimestral->save())
			{
				/*Agregar el cambio de estado de Servicio Social a 3 (Entrega Reporte)*/
				$this->cambioEstadoServicioSocial($modelSSReportesBimestral->id_servicio_social);
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{
				echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
			}
		}
	}
	/*VALIDACION DE ENVIO DE REPORTE BIMESTRAL A REVISION (EXTERNO)*/

	public function getValidarRegistroNoRepetedio($modelSSReportesBimestral)
	{
		$query = "Select * from pe_planeacion.ss_evaluacion_bimestral
					where id_reporte_bimestral = '$modelSSReportesBimestral->id_reporte_bimestral'
				";

		$lista_reporte_bim = Yii::app()->db->createCommand($query)->queryAll();

		//Podemos ser mas exactos y verificar el numero de criterios esten completos
		return ($lista_reporte_bim != NULL) ? true : false;
	}

	public function cambioEstadoServicioSocial($id_servicio_social)
	{
		$modelSSServicioSocial = SsServicioSocial::model()->findByPK($id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'No se encontraron datos del Servicio Social.');

		$modelSSServicioSocial->id_estado_servicio_social = 3;

		/*Si se inserto correctamente no mandara mensaje de error */
		if(!$modelSSServicioSocial->save()){
			Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			die('Ocurrio un error!!!');
		}

	}

	/*Se valida o no el reporte por parte del depto. de Servico Social */
	public function actionValidarDeptoEvaluacionReporteBimestral($id_reporte_bimestral)
	{

		//$rfcJefeOfServSocial = Yii::app()->params["rfcJefeOfServSocial"];
		$rfcJefeOfServSocial = Yii::app()->user->name;

		$modelSSReportesBimestral = SsReportesBimestral::model()->findByPk($id_reporte_bimestral);

		if($modelSSReportesBimestral === null)
			throw new CHttpException(404,'No existen datos de ese Reportes Bimestral.');

		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($modelSSReportesBimestral->id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'No existen datos de ese Servicio Social.');

		if($modelSSReportesBimestral)
		{

			//Si no se ha validado el reporte se valida por parte del depto. de Vinculacion
			$modelSSReportesBimestral->valida_oficina_servicio_social =  date('Y-m-d H:i:s');
			//$modelSSReportesBimestral->rfcJefeOfiServicioSocial = Yii::app()->user->name;
			$modelSSReportesBimestral->rfcJefeOfiServicioSocial = $rfcJefeOfServSocial;

			if($modelSSReportesBimestral->save())
			{

				/*Si es el ultimo Reporte Bimestral, entonces pase a a entrega de carta de terminacion*/
				if($modelSSReportesBimestral->bimestre_final == true)
				{
					$modelSSServicioSocial->id_estado_servicio_social = 4;

				}else{
					/*De vuelta al estado "Entrega Reporte" y Cambio de reporte_bimestral_actual*/
					$modelSSServicioSocial->id_estado_servicio_social = 2;
					$this->cambioReporteBimestralActual($modelSSReportesBimestral->id_reporte_bimestral, $modelSSReportesBimestral->id_servicio_social);
				}

				$modelSSServicioSocial->save();
				echo CJSON::encode( [ 'code' => 200 ] );
			}
			else{
				echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
			}
		}
	}

	public function cambioReporteBimestralActual($id_reporte_bimestral, $id_servicio_social)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_reporte_bimestral = '$id_reporte_bimestral' AND id_servicio_social = '$id_servicio_social' ";
		$modelSSReportesBimestral = SsReportesBimestral::model()->find($criteria);

		if($modelSSReportesBimestral->bimestre_final == false)
		{
			//Pasamos al siguiente Reporte Bimestral
			$bimestre = $modelSSReportesBimestral->bimestres_correspondiente + 1;
			$criteria2 = new CDbCriteria();
			$criteria2->condition = "id_servicio_social = '$id_servicio_social' AND bimestres_correspondiente = '$bimestre' ";
			$modelReportesBimestral = SsReportesBimestral::model()->find($criteria2);
			$modelReportesBimestral->reporte_bimestral_actual = true;
			$modelReportesBimestral->save();
		}

		//El Reporte Bimestral Actual pasa a deja de ser el Actual
		$modelSSReportesBimestral->reporte_bimestral_actual = false;
		$modelSSReportesBimestral->save();

	}

	public function actionValidarSupervisorEvaluacionReporteBimestral($id_reporte_bimestral)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		//$rfc_supervisor = Yii::app()->params["rfcSupervisor"];
		$rfc_supervisor = Yii::app()->user->name;

		try
		{
			$modelSSReportesBimestral = SsReportesBimestral::model()->findByPk($id_reporte_bimestral);

			if($modelSSReportesBimestral)
			{
				//die;
				$modelSSReportesBimestral->valida_responsable = date('Y-m-d H:i:s');
				$modelSSReportesBimestral->rfcResponsable = trim($rfc_supervisor);

				if($modelSSReportesBimestral->save())
				{
					Yii::app()->user->setFlash('success', "Reporte Bimestral Validado correctamente!!!");
					//Si se realizo correctamente el cambio
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar el Reporte Bimestral.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
				}
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar el Reporte Bimestral.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelSSReportesBimestral->getErrors() );
		}
	}

	public function actionNuevaAdminObservacionReporteBimestral($id_reporte_bimestral)
	{
		$modelSSReportesBimestral=$this->loadModel($id_reporte_bimestral);

		$servicioSocialAlumno = $this->getDatosServicioSocial($modelSSReportesBimestral->id_servicio_social, $id_reporte_bimestral);

		//$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		//$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

      if(isset($_POST['SsReportesBimestral']))
      {
			$modelSSReportesBimestral->attributes=$_POST['SsReportesBimestral'];

			if($modelSSReportesBimestral->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaAdminReportesBimestrales'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

        }

        $this->render('nuevaAdminObservacionReporteBimestral',array(
						'modelSSReportesBimestral'=>$modelSSReportesBimestral,
						'servicioSocialAlumno' => $servicioSocialAlumno,
						//'periodo_inicio' => $periodo_inicio,
						//'periodo_fin' => $periodo_fin
        ));
	}

	public function actionNuevaSupervisorObservacionReporteBimestral($id_reporte_bimestral)
	{
		$modelSSReportesBimestral=$this->loadModel($id_reporte_bimestral);

		$servicioSocialAlumno = $this->getDatosServicioSocial($modelSSReportesBimestral->id_servicio_social, $id_reporte_bimestral);

		$periodo_inicio = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_inicio_programa']);
		$periodo_fin = $this->getPeriodoServicioSocial($servicioSocialAlumno[0]['fecha_fin_programa']);

        if(isset($_POST['SsReportesBimestral']))
        {
			$modelSSReportesBimestral->attributes=$_POST['SsReportesBimestral'];

			if($modelSSReportesBimestral->save())
			{
				Yii::app()->user->setFlash('success', 'Datos guardados correctamente!');
				$this->redirect(array('listaAdminReportesBimestrales'));
			}else{

				Yii::app()->user->setFlash('danger', 'Error!!! no se guardaron los datos.');
			}

		}

		$this->render('nuevaSupervisorObservacionReporteBimestral',array(
				      'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					  'servicioSocialAlumno' => $servicioSocialAlumno,
					  'periodo_inicio' => $periodo_inicio,
					  'periodo_fin' => $periodo_fin
));
	}

	/*Funcion para Obtener el periodo de inicio y fin del servicio social */
	public function getPeriodoServicioSocial($fecha)
	{
		$dt = new DateTime($fecha);
		$mes = $dt->format("m");
		$anio = $dt->format("Y");
		$periodo="";

		if($mes <= 7)
		{
			$periodo = "ENERO-JULIO"." de ".$anio;
		}else
		if($mes > 7){
			$periodo = "AGOSTO-DICIEMBRE"." de ".$anio;
		}

		return $periodo;
	}
	/*Funcion para Obtener el periodo de inicio y fin del servicio social */

	/*Datos para vista de la evaluacion del reporte bimestral de servicio social */
	public function getDatosServicioSocial($id_servicio_social, $id_reporte_bimestral)
	{
		$query="
		select
		eda.\"nmbAlumno\" as name_alumno,
		ss.no_ctrl,
		(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\") as carrera,
		pss.nombre_programa as programa,
		pss.fecha_inicio_programa,
		pss.fecha_fin_programa,
		pss.id_periodo_programa as periodo_servicio_social,
		pss.id_tipo_programa,
		rb.bimestres_correspondiente,
		rb.bimestre_final,
		rb.fecha_inicio_rep_bim,
		rb.fecha_fin_rep_bim,
		rb.calificacion_reporte_bimestral,
		(select anio from pe_planeacion.ss_registro_fechas_servicio_social where id_registro_fechas_ssocial = pss.id_registro_fechas_ssocial) as anio_servicio_social
		from pe_planeacion.ss_servicio_social ss
		join public.\"E_datosAlumno\" eda
		on eda.\"nctrAlumno\" = ss.no_ctrl
		join pe_planeacion.ss_reportes_bimestral rb
		on rb.id_servicio_social = ss.id_servicio_social
		join pe_planeacion.ss_programas pss
		on pss.id_programa = ss.id_programa
		where ss.id_servicio_social = '$id_servicio_social' and rb.id_reporte_bimestral = '$id_reporte_bimestral'
		";

		$datos_servsocial = Yii::app()->db->createCommand($query)->queryAll();

		return $datos_servsocial;
	}
	/*Datos para vista de la evaluacion del reporte bimestral de servicio social */

	/*Historico Reportes Bimestrales Vista Alumno*/
	public function actionRepBimHistoricoAlumnoServicioSocial($id_servicio_social)
	{
		//Filtrara solo los de is_estado_servicio_social = 6 FINALIZADO
		$modelSSReportesBimestral  = new SsReportesBimestral_('search');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		//Tomar del inicio de sesion del alumno en el SII
		//$no_ctrl = Yii::app()->params['no_ctrl'];
		$no_ctrl = Yii::app()->user->name;

		if(!$this->validarHistoricoServicioSocial($id_servicio_social, $no_ctrl))
			throw new CHttpException(4044,'La Información No se encuentra disponible.');

		//Obtener el tipo de servicio social
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Servicio Social.');

		$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Programa.');

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];
		}

    	$this->render('repBimHistoricoAlumnoServicioSocial',array(
				  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
				  'id_servicio_social' => $id_servicio_social,
				  'no_ctrl' => $no_ctrl,
				  'tipo_servicio' => $modelSSProgramas->id_tipo_programa
		));

	}
	/*Historico Reportes Bimestrales Vista Alumno*/

	/*Historico Reportes Bimestrales Vista Servicios Escolares*/
	public function actionRepBimHistoricoEscolaresAlumnoServicioSocial($id_servicio_social, $no_ctrl)
	{
		//Filtrara solo los de is_estado_servicio_social = 6 FINALIZADO
		$modelSSReportesBimestral  = new SsReportesBimestral_('search');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		//Obtener el tipo de servicio social
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Servicio Social.');

		$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Programa.');

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];
		}

    	$this->render('repBimHistoricoEscolaresAlumnoServicioSocial',array(
				  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
				  'id_servicio_social' => $id_servicio_social,
				  'no_ctrl' => $no_ctrl,
				  'tipo_servicio' => $modelSSProgramas->id_tipo_programa
		));

	}
	/*Historico Reportes Bimestrales Vista Servicios Escolares*/

	/*Historico Reportes Bimestrales Vista Admin*/
	public function actionRepBimHistoricoAdminServicioSocial($id_servicio_social, $no_ctrl)
	{
		//Filtrara solo los de is_estado_servicio_social = 6 FINALIZADO
		$modelSSReportesBimestral  = new SsReportesBimestral_('search');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		//Obtener el tipo de servicio social
		$modelSSServicioSocial = SsServicioSocial::model()->findByPk($id_servicio_social);

		if($modelSSServicioSocial === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Servicio Social.');

		$modelSSProgramas = SsProgramas::model()->findByPk($modelSSServicioSocial->id_programa);
		if($modelSSProgramas === null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Programa.');

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];
		}

        $this->render('repBimHistoricoAdminServicioSocial',array(
					 'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					 'id_servicio_social' => $modelSSServicioSocial->id_servicio_social,
					 'no_ctrl' => $no_ctrl,
					 'tipo_servicio' => $modelSSProgramas->id_tipo_programa
		));
	}
	/*Historico Reportes Bimestrales Vista Admin*/

	public function loadModel($id)
	{
		$model=SsReportesBimestral::model()->findByPk($id);

		if($model===null)
			throw new CHttpException(404,'Error!!! no existen datos de ese Reporte Bimestral.');

		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reportes-bimestral-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	//Modificar las calificaciones de Servicio Social
	public function actionEditarCalificacionesCriterios($nocontrol = null)
	{
		$modelSSReportesBimestral = new SsReportesBimestral_('search');
		$modelSSReportesBimestral->unsetAttributes();  // clear any default values

		if(isset($_GET['SsReportesBimestral']))
		{
			$modelSSReportesBimestral->attributes=$_GET['SsReportesBimestral'];
			$modelSSReportesBimestral->nocontrol = $_GET['SsReportesBimestral']['nocontrol'];
			$nocontrol = $modelSSReportesBimestral->nocontrol;
		}

		$this->render('editarCalificacionesCriterios',array(
					  'modelSSReportesBimestral'=>$modelSSReportesBimestral,
					  'nocontrol' => $nocontrol
		));
	}

	/*****************GETTERS AND SETTERS**********************/
	public function getEstadosServicioSocial()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_estado_servicio_social > 0 ";
		$modelSSEstadoServicioSocial = SsEstadoServicioSocial::model()->findAll($criteria);

		$lista_edos_servicio = CHtml::listData($modelSSEstadoServicioSocial, "id_estado_servicio_social", "estado");

		return $lista_edos_servicio;
	}

	//Validar que el servicio social en curso pertenezca al usuario actualmente logeado en el SII
	public function validarServicioSocial($id_servicio_social, $id_reporte_bimestral, $no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_servicio_social ss
		join pe_planeacion.ss_reportes_bimestral rb
		on rb.id_servicio_social = ss.id_servicio_social
		where ss.no_ctrl = '$no_ctrl' and rb.id_servicio_social = '$id_servicio_social'
		and rb.id_reporte_bimestral = '$id_reporte_bimestral' and (id_estado_servicio_social != 6 or id_estado_servicio_social != 7)";

		$datos = Yii::app()->db->createCommand($query)->queryAll();

		return ($datos != NULL) ? true : false;
	}

	public function validarHistoricoServicioSocial($id_servicio_social, $no_ctrl)
	{
		$query = "select * from pe_planeacion.ss_servicio_social
		where no_ctrl = '$no_ctrl' and id_servicio_social = '$id_servicio_social'
		and id_estado_servicio_social = 6";

		$datos = Yii::app()->db->createCommand($query)->queryAll();

		return ($datos != NULL) ? true : false;
	}

	public function validarSupervisorServicioSocial($id_servicio_social, $id_reporte_bimestral, $rfcSupervisor, $no_ctrl)
	{
		$bandera;
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcSupervisor);
		$modelSSSupervisores = SsSupervisoresProgramas::model()->findByPk($rfcSupervisor);

		if($modelHEmpleados != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_interno rpi
				on rpi.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_reportes_bimestral rb
				on rb.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpi.superv_principal_int = true
				and rpi.\"rfcEmpleado\" = '$rfcSupervisor' and ss.id_servicio_social = '$id_servicio_social' and ss.no_ctrl ='$no_ctrl'
				and rb.id_reporte_bimestral = '$id_reporte_bimestral'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}else
		if($modelSSSupervisores != NULL)
		{
			$query = "select * from pe_planeacion.ss_programas pss
				join pe_planeacion.ss_responsable_programa_externo rpe
				on rpe.id_programa = pss.id_programa
				join pe_planeacion.ss_servicio_social ss
				on ss.id_programa = pss.id_programa
				join pe_planeacion.ss_reportes_bimestral rb
				on rb.id_servicio_social = ss.id_servicio_social
				where pss.id_status_programa = 1 and rpe.superv_principal_ext = true
				and rpe.\"rfcSupervisor\" = '$rfcSupervisor' and ss.id_servicio_social = '$id_servicio_social'
				and ss.no_ctrl='$no_ctrl' and rb.id_reporte_bimestral = '$id_reporte_bimestral'";

			$datos = Yii::app()->db->createCommand($query)->queryAll();
			$bandera = ($datos != NULL) ? true : false;
		}

		return $bandera;

	}
	/*****************GETTERS AND SETTERS**********************/
}
