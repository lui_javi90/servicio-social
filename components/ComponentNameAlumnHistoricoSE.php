<?php
class ComponentNameAlumnHistoricoSE extends CButtonColumn
{
    public $header = "Alumno";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        $model2 = EDatosAlumno::model()->findByPk($data->no_ctrl);

        $name = $model2->nmbAlumno;
        
        //No se usan validaciones porque los campos son NOT NULL en la BD
        echo CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".$data->no_ctrl, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

        echo "<br>";
        /*Se crea la tabla*/
        echo '<table style="width:100%" class="table">
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Nombre Alumno</th>
                </tr>
                <tr>
                    <td style="width:70%" rowspan="1" colspan="1" class="text-center">'.'<b>'.$name.'</b>'.'</td>
                </tr>
            </table>';
        /*Se crea la tabla*/
    }
}

?>