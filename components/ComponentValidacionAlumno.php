<?php
class ComponentValidacionAlumno extends CButtonColumn
{
    public function init() {}

        public function renderDataCellContent($row, $data)
        {
            /*Para llamar el metodo estatico*/
            require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';
    
            $modelSSReportesBimestral = SsReportesBimestral::model()->findByPk($data->id_reporte_bimestral);
    
            if($modelSSReportesBimestral != NULL)
            {
                echo "<br>";
                echo ($modelSSReportesBimestral->envio_alum_evaluacion != NULL) ? '<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/aprobado_32.png"/>' : '<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/cancelado_32.png"/>';
                echo "<br><br>";
                //Formato a las fecha de validacion del Alumno
                $fecha_val_alum = GetFormatoFecha::getFechaValidaAlumnoReporteBimestral($data->id_reporte_bimestral, $data->id_servicio_social);
                echo ($fecha_val_alum[0]['fecha_act'] != 'SIN EVALUAR') ? "<b>".$fecha_val_alum[0]['fecha_act']." a las ".$fecha_val_alum[0]['hora']."<b>" : '<b>'.'--'.'<b>';
                echo "<br><br>";
            }else{
                
                return "<b>"."No hay Datos"."<b>";
            }
    
        }
}

?>