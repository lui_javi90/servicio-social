<?php
/*Componente para manejar las fecha de evaluacion de los Reportes Bimestrales*/
class ComponentTiemposEvaluacion extends CButtonColumn
{
  public function init(){}

  protected function renderDataCellContent($row, $data)
  {
    /*Para llamar el metodo estatico*/
    require_once Yii::app()->basePath.'/modules/serviciosocial/staticClasses/GetFormatoFecha.php';

    /*Para guardar las fecha de validacion de los reportes bimestrales del alumno, supervisor y admin*/
    $val_alum; $val_sup; $val_admin;
    $mi_color_a; $mi_color_s; $mi_color_v;

    /*Obtener las fechas y los dias que se tardan en evaluar los reportes bimestrales desde que fueron enviados
    por el alumno a revision*/
    $eval_alumno = GetFormatoFecha::getFechaValidaAlumnoReporteBimestral($data->id_reporte_bimestral, $data->id_servicio_social);
    $eval_supervisor = GetFormatoFecha::getFechaValidaSupervisorReporteBimestral($data->id_reporte_bimestral, $data->id_servicio_social);
    $eval_admin = GetFormatoFecha::getFechaValidaAdminReporteBimestral($data->id_reporte_bimestral, $data->id_servicio_social);

    /*Damos formato a las fechas de validacion de los reporte bimestrales*/
    $val_alum = ($eval_alumno[0]['fecha_act'] != "SIN EVALUAR") ? $eval_alumno[0]['fecha_act']." a las ".$eval_alumno[0]['hora'] : $eval_alumno[0]['fecha_act'];
    $val_sup = ($eval_supervisor[0]['fecha_act'] != "SIN EVALUAR") ? $eval_supervisor[0]['fecha_act']." a las ".$eval_supervisor[0]['hora'] : $eval_supervisor[0]['fecha_act'];   ;
    $val_admin = ($eval_admin[0]['fecha_act'] != "SIN EVALUAR") ? $eval_admin[0]['fecha_act']." a las ".$eval_admin[0]['hora'] : $eval_admin[0]['fecha_act'];

    /*Aumentamos 7 dias a la fecha fin del reporte bimestral que seran los que se den para validar el reporte bimestral*/
    //$nuevafecha = strtotime ( '+7 day' , strtotime ($data->fecha_fin_rep_bim)) ;
    //$nuevafecha = date ( 'Y-m-d' , $nuevafecha );

    /*Agregamos formato a la fecha limite de evaluacion del reporte bimestral*/
    //$fec_lim_eva = $this->getFechaFormato($nuevafecha);

    /*Colores que definen si se envio a tiempo el reporte bimestral*/
    //$mi_color_a = $this->getColorEvaluacionBimestral($data->envio_alum_evaluacion, $nuevafecha);
    $mi_color_a = $this->fueEvaluado($data->envio_alum_evaluacion);
    //$mi_color_s = $this->getColorEvaluacionBimestral($data->valida_responsable, $nuevafecha);
    $mi_color_s = $this->fueEvaluado($data->valida_responsable);
    //$mi_color_v = $this->getColorEvaluacionBimestral($data->valida_oficina_servicio_social, $nuevafecha);
    $mi_color_v = $this->fueEvaluado($data->valida_oficina_servicio_social);
    /*Colores que definen si se envio a tiempo el reporte bimestral*/

    /*Dias transcurridos entre las fechas de evaluacion y validacion de los reportes bimestrales*/
    $dias_trans_as = $this->diasTranscurridos($data->envio_alum_evaluacion, $data->valida_responsable);
    $dias_trans_sv = $this->diasTranscurridos($data->valida_responsable, $data->valida_oficina_servicio_social);

    /*Dias transcurridos desde que el alumno envio su reporte bimestral a evaluacion y validacion y lo evaluo y valido el jefe de ofic de servicio social */
    $dias_trans_aj = $this->diasTranscurridos($data->envio_alum_evaluacion, $data->valida_oficina_servicio_social);
    $dias = ($dias_trans_aj != NULL) ? ' días' : '-';
     
    /** Fecha limite evaluacion (experimental)
     * <tr style="width:100%">
              <th style="width:40%" colspan="1">Fecha Limite Evaluación</th>
              <td style="width:60%" colspan="1" class="text-center">'.$fec_lim_eva.'</td>
        </tr>
     */

    /*Se crea la tabla*/
    echo '<table style="width:100%" class="table">';
    echo '<tr bgcolor='.$mi_color_a.' style="width:100%">
              <th style="width:40%" colspan="1">Alumno</th>
              <td style="width:60%" colspan="1" class="text-center">'.$val_alum.'</td>
          </tr>
          <tr bgcolor='.$mi_color_s.' style="width:100%">
              <th style="width:40%" colspan="1">Supervisor del Programa</th>
              <td style="width:60%" colspan="1" class="text-center">'.$dias_trans_as.'  '.$val_sup.'</td>
          </tr>
          <tr bgcolor='.$mi_color_v.' style="width:100%">
              <th style="width:40%" colspan="1">Depto. de Vinculación</th>
              <td style="width:60%" colspan="1" class="text-center">'.$dias_trans_sv.'  '.$val_admin.'</td>
          </tr>
          <tr bgcolor='.$mi_color_v.' style="width:100%">
              <th style="width:40%" colspan="1">Días Transcurridos</th>
              <td style="width:60%" colspan="1" class="text-center">'.$dias_trans_aj.' '.$dias.'</td>
          </tr>';
    echo '</table>';
    /*Se crea la tabla*/

  }

  //Por ahora nada mas
  public function fueEvaluado($fec_eval) {  return ($fec_eval != NULL) ? '#64DD17' : '#CCC' ; }

  /*Se validan las fecha para ver si se evaluo el reporte bimestral en las fecha defindas */
  public function getColorEvaluacionBimestral($fec_eval, $fec_limite)
  {
    if($fec_eval == NULL)
    {
      return "#CCC";
    }else{
      if($fec_eval <= $fec_limite)
      {
        return "#64DD17"; //Verde

      }else{
        return "#DD2C00"; //Rojo
      }
    }
  }
  /*Se validan las fecha para ver si se evaluo el reporte bimestral en las fecha defindas */

  /*Agregar formato legible a las fechas de evaluación*/
  public function getFechaFormato($fecha)
  {
    $mes;
    $dia = date("d", strtotime($fecha)); //dia
    $_mes = date("n", strtotime($fecha)); //Suprime los ceros de los meses de un digito 1 a 12
    $anio = date("Y", strtotime($fecha)); //año

    switch($_mes)
    {
      case 1: $mes = "Enero"; break;
      case 2: $mes = "Febrero"; break;
      case 3: $mes = "Marzo"; break;
      case 4: $mes = "Abril"; break;
      case 5: $mes = "Mayo"; break;
      case 6: $mes = "Junio"; break;
      case 7: $mes = "Julio"; break;
      case 8: $mes = "Agosto"; break;
      case 9: $mes = "Septiembre"; break;
      case 10: $mes = "Octubre"; break;
      case 11: $mes = "Noviembre"; break;
      case 12: $mes = "Diciembre"; break;
    }

    return $dia." de ".$mes." de ".$anio;
  }
  /*Agregar formato legible a las fechas de evaluación*/

  /*Dias transcurridos entre cada evluacion del report bimestral*/
  public function diasTranscurridos($fec_al, $fec_sup)
  {
    $diasTranscurridos;

    if($fec_al != NULL && $fec_sup != NULL)
    {
      $fechaAlumno = strtotime($fec_al);
      $fechaSupervisor = strtotime($fec_sup);
      $segundosTranscurridos = $fechaSupervisor - $fechaAlumno;
      $diasTranscurridos = $segundosTranscurridos / 86400; //Segundos por dia.

      return "( + ".intval($diasTranscurridos)." )";

    }else{

      return null;
    }

  }
  /*Dias transcurridos entre cada evluacion del report bimestral*/

}//Fin de la clase

/*
* Para saber cuántos días han pasado, podemos dividir los segundos transcurridos entre 86,400
* Un día = 24 horas * 60 minutos * 60 segundos = 86,400 segundos
*/

?>
