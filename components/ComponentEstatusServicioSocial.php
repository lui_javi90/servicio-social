<?php
class ComponentEstatusServicioSocial extends CButtonColumn
{
    public function init(){}

    protected function renderDataCellContent($row, $data)
    {
        $no_ctrl = trim($data->nctrAlumno);
        $query = "select * from pe_planeacion.ss_historico_totales_alumnos where no_ctrl = '$no_ctrl' ";
        $datos = Yii::app()->db->createCommand($query)->queryAll();

        $control = (count($datos) > 0) ? $datos[0]['completo_servicio_social'] : 3;

		if(trim($control) != 3)
		{
			if(trim($control) == 1){

			    echo '<span style="font-size:14px" class="label label-success">COMPLETADO</span>';
            }else{
        
                echo '<span style="font-size:14px" class="label label-info">POR COMPLETAR</span>';
            }

		}else{
            echo '<span style="font-size:14px" class="label label-default">NO HAY DATOS</span>';
        }
    }

}?>