<?php
/**
* Componente para Evaluacion Reporte Bimestral de los Supervisores Externos
**/
class ComponentSupEAsignarCalificacionCriterio extends CButtonColumn
{
    public $header = "Evaluación <br>B<br>";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        $model = SsCriteriosAEvaluar::model()->findByPK($data->id_criterio);
        $valores = array();
        $length = (int)$model->valor_a;

        for($i = 0; $i <= $length; $i++){   $valores[$i] = $i; }
                
        echo CHtml::dropDownList('evaluacion_b' . $data->id_evaluacion_bimestral, $data->evaluacion_b,
                                $valores,
                            array(
                                'class'=>'form-control',
                                'ajax' => array(
                                    'type' => 'POST',
                                    'beforeSend' => 'function(){$(".bar").show();}',
                                    'complete' => 'function(){$(".bar").hide();}',
                                    'url' => ('?r=serviciosocial/ssEvaluacionBimestral/editarCalificacionCriterioSupervisorExterno'),
                                    'data' => array(
                                        'evaluacion_b' => 'js:this.value', 'id_evaluacion_bimestral' => $data->id_evaluacion_bimestral
                                    ),
                                ),
                            )
        );
    }

}

?>