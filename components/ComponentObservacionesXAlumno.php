<style>
.badge-success {
  background-color: #468847;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-warning {
  background-color: #f89406;
}
.badge-error {
  background-color: #b94a48;
}
.badge-inverse {
  background-color: #333333;
}
</style>
<?php
/*Componente para mostrar si hay observaciones enviadas por cada alumno y esten en estatus de NO VISTAS*/
class ComponentObservacionesXAlumno extends CButtonColumn
{
    public function init(){}

    protected function renderDataCellContent($row, $data)
    {
        $noCtrl = $data->no_ctrl;

        //Nos traemos todas las observaciones que le ha hecho el alumno con ese numero de control al Supervisor que no han sido leidas (fue_leida = false)
        $query = 
        "select COUNT(oss.id_observacion) as observ_no_vistas from pe_planeacion.ss_observaciones_servicio_social oss
        join pe_planeacion.ss_servicio_social ss
        on ss.id_servicio_social = oss.id_servicio_social
        where oss.emisor_observ = '$noCtrl' AND oss.fue_leida = false AND oss.tipo_observacion_receptor = 2
        ";

        $result = Yii::app()->db->createCommand($query)->queryAll();

        //Si devuelve null entonces no hay observaciones pendientes y ponemos un cero
        $observaciones = ($result[0]['observ_no_vistas'] == NULL) ? "0" : $result[0]['observ_no_vistas'];

        echo "<span class=\"badge badge-info\">".$observaciones."</span>";
    }
}
?>