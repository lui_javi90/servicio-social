<style>
.badge-success {
  background-color: #468847;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-warning {
  background-color: #f89406;
}
.badge-error {
  background-color: #b94a48;
}
.badge-inverse {
  background-color: #333333;
}
</style>
<?php
/*Componente para mostrar si hay observaciones enviadas por cada alumno y esten en estatus de NO VISTAS*/
class ComponentObservacionesXSupervisor extends CButtonColumn
{
    public function init(){}

    protected function renderDataCellContent($row, $data)
    {
        $noCtrl = Yii::app()->params['no_ctrl'];
        $idServicioSocial = $data->id_servicio_social;

        //Nos traemos todas las observaciones que le han hecho al alumno por parte del Supervisor con ese rfc y que no han sido leidas (fue_leida = false)
        if($data->tipo_observacion_emisor == 1)
        {
            $query = 
            "select COUNT(oss.id_observacion) as observ_no_vistas 
            from pe_planeacion.ss_observaciones_servicio_social oss
            join pe_planeacion.ss_servicio_social ss
            on ss.id_servicio_social = oss.id_servicio_social
            where oss.receptor_observ = '$noCtrl' AND ss.id_servicio_social = '$idServicioSocial' AND oss.fue_leida = false 
            AND oss.tipo_observacion_emisor = 1
            ";
    
            $result = Yii::app()->db->createCommand($query)->queryAll();
        }else
        if($data->tipo_observacion_emisor == 2)
        {
            $query = 
            "select COUNT(oss.id_observacion) as observ_no_vistas 
            from pe_planeacion.ss_observaciones_servicio_social oss
            join pe_planeacion.ss_servicio_social ss
            on ss.id_servicio_social = oss.id_servicio_social
            where oss.receptor_observ = '$noCtrl' AND ss.id_servicio_social = '$idServicioSocial' AND oss.fue_leida = false 
            AND oss.tipo_observacion_emisor = 2
            ";
    
            $result = Yii::app()->db->createCommand($query)->queryAll();
        }
       

        //Si devuelve null entonces no hay observaciones pendientes y ponemos un cero
        $observaciones = ($result[0]['observ_no_vistas'] == NULL) ? "0" : $result[0]['observ_no_vistas'];

        echo "<span class=\"badge badge-info\">".$observaciones."</span>";
    }
}
?>